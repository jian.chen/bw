<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.bw.adv.module.sys.model.SysUser" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<meta charset="UTF-8">
		<title>SAGE AIR</title>
		<%@ include file="base.jsp"%>
		<link rel="shortcut icon" href="../img/sage.png">
		<style type="text/css">
			.loading-div{
			  width: 100%;
			  height: 100%;
			  display: block;
			  position: fixed;
			  z-index: 3333;
			  top: 0rem;
			/*   opacity:0.80; */
			  background-color: rgba(0, 0, 0, 0.5);
			}
			.loading-div td{
			  color:black;
			  font-weight:900;
			}
			.summary {
				width:100%;
			}
			.line{
				font-size:0px;
			}
			.line > div{
				display:inline-block;
				*zoom:1;
				*display:inline;
				text-align:center;
				padding: 15px 10px;
				vertical-align:top;
				width:315px;
			}
			.summaryTd{
			    border: solid 1px #000;
			    text-align: center;
			}
			.title{
			    font-size: 16px;
			    line-height: 35px;
			    background-color: rgba(24,138,113,0.5);
			    color: #fff;
			}
			.count-panel{
			    color: #fff;
			    background-color: rgba(24,138,113,0.2);
			    padding: 10px 0;
			}
			.count{
			    font-size: 35px;
			    font-weight: bold;
			    line-height: 45px;
			    min-height: 45px;
			    color: #fff;
			}
			.change{
				font-size:12px;
				color: #fff;
			}
			.date{
			    background-color: rgba(24,138,113,0.5);
			    color: #fff;
			    min-height: 40px;
			    line-height:40px;
			    font-size:14px;
			}
			
			.line > div > li[draggable="false"] {
			    border-style: dashed;
			}
			
			.line > div > li {
			    border: 1px solid #d8d8d8;
			    box-sizing: border-box;
			    float: left;
			    height: 153px;
			    margin: 0px;
			    opacity: 1;
			    position: relative;
			    width: 295px;
			}
			
			.line > div > li > a {
			    color: #3c3c3c;
			    display: block;
			    height: 100%;
			    text-decoration: none;
			    width: 100%;
			}
			
			.line > div > li[draggable="false"] > a > .thumb {
			    background: url(../img/plus.png) no-repeat scroll center center, rgba(242, 242, 242, 0.49) url(../img/noise.png) repeat scroll 0 0;
			}
			
			.line > div > li > a > .thumb {
			    background: white none no-repeat scroll 0 0;
			    display: block;
			    height: 100%;
			    width: 100%;
			}
			
			.line > div > li:hover {
			    border-color: #9a9fa4;
			    box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.85);
			}
	</style>	
	</head>
	
	<body  class="bg_panel">
		<div id="mainPanel" style="height:150px;">
			<div data-options="region:'north',border:false" class="common_top">
				<a href="index.jsp"><div class="icon sage"></div></a>
				<ul class="common_top_tools">
					<li onclick="loginOut()" class="icon-small exit"></li>
					<a href="#">
						<li id="sys-li" name="sys-li" class="icon-small system">
						</li>
					</a>
					<li>${sysUser.cn}</li>
				</ul>
			</div>
			<div data-options="region:'west',border:false" style="width:190px;">
				<div class="common_menu" style="height:100%; width:100%; padding-bottom:50px; box-sizing:border-box; overflow:auto;">
					<ul id="menu_ul">
						${menu}
					</ul>
				</div>
			</div>
			<div data-options="region:'center',border:false" style="padding:20px">
				<div style="padding-top:30px;padding-left:80px;">
					<span style="font-size:20px;">
						${sysUser.cn} , <span id="welcom"></span>好！欢迎登录SAGE AIR 消费者互动营销平台！
					</span>
					<input id="hour" type="hidden" value="" />
					<span class="h_text" style="font-size:15px;">如需修改系统设置，</span>
					<span style="font-size:15px;margin-top:4px;">请点击</span>
					<button name="sys-li" type="button" class="btn btn-success btn-block choose_btn" style="width: 83px;margin-top: -2px;height: 30px;display:inline-block; padding:0px 0px;">系统设置</button>
				</div>
				<div style="display:block;margin-top:65px;margin-left:75px;">
				<div class="summary">
					<div class="line" id="dashShow">
						<!-- <div>
							<p class="title">昨日新增会员</p>
							<div class="count-panel">
								<p class="count" id="memberDayNew"></p>
								<p class="change" id="memberDayChange"></p>
							</div>
							<p class="date" id="memberDay"></p>
						</div>
						<div>
							<p class="title">过去7天新增会员</p>
							<div class="count-panel">
								<p class="count" id="memberWeekNew"></p>
								<p class="change" id="memberWeekChange"></p>
							</div>
							<p class="date" id="memberWeek"></p>
						</div>
						<div>
							<p class="title">上月新增会员</p>
							<div class="count-panel">
								<p class="count" id="memberMonthNew">123</p>
								<p class="change" id="memberMonthChange"></p>
							</div>
							<p class="date" id="memberMonth"></p>
						</div>
						<div>
							<p class="title">至昨天为止所有会员</p>
							<div class="count-panel">
								<p class="count" id="memberAllNew"></p>
								<p class="change" id="memberAllChange"></p>
							</div>
							<p class="date" id="memberAll"></p>
						</div>
						<div>
							<p class="title">昨日优惠券发放数</p>
							<div class="count-panel">
								<p class="count" id="couponDayNew"></p>
								<p class="change" id="couponDayChange"></p>
							</div>
							<p class="date"  id="couponDay"></p>
						</div>
						<div>
							<p class="title">上月优惠券发放数</p>
							<div class="count-panel">
								<p class="count" id="couponMonthNew"></p>
								<p class="change" id="couponMonthChange"></p>
							</div>
							<p class="date" id="couponMonth"></p>
						</div>
						<div>
							<li id="add_dashboard" draggable="false" data-index="8" data-fid="" title="点击添加新的仪表盘">
								<a draggable="false">
								<span class="thumb" style="background-position: center center;"></span>
								</a>
							</li>
						</div> -->
					</div>	
				</div>
				</div>
			</div>
		</div>
		<div id="maskPanel" class="easyui-panel mask none" style="width:100%;padding:10px; position: absolute;"  data-options="">
			
		</div>
		
		<div id="dataLoad" class="loading-div"><!--页面载入显示-->
		   <table width=100% height=100% border="0" align=center valign=middle>
		    <tr height=45%><td align=center>&nbsp;</td></tr>
		    <tr><td align=center><img src="<c:url value='/scrm/img/loading.gif'/>"/></td></tr>
		    <tr><td align=center></td></tr>
		    <tr height=50%><td align=center>&nbsp;</td></tr>
		   </table>
		 </div>
		
		<script type="text/javascript">
			var hour = new Date().getHours(); //获取系统时间
			$("#hour").val(hour);
			var TimeString = ""
			if (hour >= 0 & hour < 6) {
				TimeString = "凌晨";
			} else if (hour >= 6 & hour < 8) {
				TimeString = "早上";
			} else if (hour >= 8 & hour < 13) {
				TimeString = "上午";
			} else if (hour >= 13 & hour < 18) {
				TimeString = "下午";
			}
			if (hour >= 18 & hour <= 23) {
				TimeString = "晚上";
			}
			$("#welcom").html(TimeString);
		</script>
		
		<script type="text/javascript">
			require(['index']);
			
			//隐藏遮罩层
			$("#dataLoad").hide();
			//显示遮罩层
			//$("#dataLoad").show();
		</script>
		
	</body>
</html>