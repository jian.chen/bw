<%
	String baseUrl = request.getContextPath();
	request.setAttribute("baseUrl", baseUrl);
	request.setAttribute("urlArgs", "v2.0_2016-08-12");
%>

<script>
	 var require = {
	      urlArgs: '${urlArgs}'
	 };
	//get server date��
	 function getServerDate(){
	 	return new Date(<%=System.currentTimeMillis() %>);
	 }
	
	var baseUrl = "${baseUrl}";
	var baseUrl_html = "scrm/view/";
</script>


<link rel="stylesheet" type="text/css" href="${baseUrl}/scrm/css/bootstrap.min.css?v=${urlArgs}" />
<link rel="stylesheet" type="text/css" href="${baseUrl}/scrm/css/bootstrap-multiselect.css?v=${urlArgs}" />
<link rel="stylesheet" type="text/css" href="${baseUrl}/scrm/css/easyui.css?v=${urlArgs}" />
<link rel="stylesheet" type="text/css" href="${baseUrl}/scrm/css/icon.css?v=${urlArgs}" />
<link rel="stylesheet" type="text/css" href="${baseUrl}/scrm/css/common.css?v=${urlArgs}" />
<link rel="stylesheet" type="text/css" href="${baseUrl}/scrm/css/city.css?v=${urlArgs}" />
<link rel="stylesheet" type="text/css" href="${baseUrl}/scrm/css/ztree/zTreeStyle.css?v=${urlArgs}" />
<link rel="stylesheet" type="text/css" href="${baseUrl}/scrm/css/ztree/zTreeStyle.css?v=${urlArgs}" />

<script type="text/javascript" src="${baseUrl}/scrm/js/lib/jquery.min.js?v=${urlArgs}"></script>
<script type="text/javascript" src="${baseUrl}/scrm/js/lib/bootstrap.min.js?v=${urlArgs}"></script>
<script type="text/javascript" src="${baseUrl}/scrm/js/lib/jquery.easyui.min.js?v=${urlArgs}"></script>
<script type="text/javascript" src="${baseUrl}/scrm/js/lib/ckeditor.js?v=${urlArgs}"></script>
<script type="text/javascript" src="${baseUrl}/scrm/js/lib/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="${baseUrl}/scrm/js/lib/underscore-min.js"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm//js/lib/easyui-lang-zh_CN.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/contants.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/lib/bootstrap-datetimepicker.min.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/lib/bootstrap-datetimepicker.zh-CN.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/lib/require.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/main.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/common.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/serializeObject.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/lib/jquery.ztree.all-3.5.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/lib/uploadPreview.js?v=${urlArgs}"></script>
<script type="text/javascript" charset="utf-8" src="${baseUrl}/scrm/js/lib/DatePicker/My97DatePicker/WdatePicker.js?v=${urlArgs}"></script>
<%-- <script src="${baseUrl}/scrm/js/layer.js" type="text/javascript" charset="utf-8"></script> --%>
 


