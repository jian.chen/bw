<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>

	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="renderer" content="webkit">
		<link rel="shortcut icon" href="scrm/img/sage.png">
		<title>sage</title>
		<link rel="stylesheet" type="text/css" href="scrm/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="scrm/css/bootstrap-datetimepicker.min.css" />
		<link rel="stylesheet" type="text/css" href="scrm/css/easyui.css" />
		<link rel="stylesheet" type="text/css" href="scrm/css/common.css" />
		<script src="scrm/js/lib/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="scrm/js/lib/jquery.easyui.min.js"></script>
		<script src="scrm/js/contants.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="scrm/js/lib/jquery.easyui.min.js"></script>
		<style type="text/css">
			.loginBg{
				position: absolute;
				width: 100%;
				height: 100%;
				background: url(scrm/img/loginbg.png);
				top: 0;
			}
			.loginBg img{
				width: 100%;
				height: 100%;
			}
			.common_top{
				position: absolute;
				top: 0;
				z-index: 10;
				width: 100%;
			}
			.loginBox{
				width: 410px;
				height: 360px;
				background-image: url(scrm/img/loginBox.png);
				position: absolute;
				top: 50%;
				left: 50%;
				margin-left: -205px;
				margin-top: -180px;
			}
			.loginForm{
				position: absolute;
				top: 90px;
				left: 50%;
				width: 270px;
				margin-left: -135px;
				font-size: 16px;
			}
			.loginForm span{
				display: inline-block;
				width: 70px;
				line-height: 30px;
			}
			.loginForm input{
				width: 195px;
				height: 30px;
				line-height: 30px;
				display: inline-block;
				margin-bottom: 25px;
			}
			.loginCheck{
				padding-left: 70px;
				margin-top: -15px;
			}
			.loginCheck img{
				display: block;
				margin: auto;
			}
			.loginBtn{
				height: 55px;
				font-size: 16px;
				margin-top: 10px;
			}
			.loginBottom{
				font-size: 12px;
				color: #666666;
				position: absolute;
				top: 50%;
				left: 50%;
				margin-left: -65px;
				margin-top: 200px;
				text-align: center;
			}
		</style>
		
<script type="text/javascript">
$(function() {
	<c:if test="${!empty sessionScope.message}">
		alert('${sessionScope.message}');
	</c:if>	
	$('input:text:first').focus();
	bindLoginButtonClick();
	bindInputKeyDown(); 
}); 
function bindInputKeyDown() { 
	$("#code").keyup(function(){
		$("#tooltipspan").text("");
		});
	$inp = $(':input'); 
	$inp.bind('keydown', function (e) { 
		if (e.which == 13) { 
			e.preventDefault(); 
			var nxtIdx = $inp.index(this) + 1; 
			$(":input:eq(" + nxtIdx + ")").focus(); 
			if($inp.index(this) == 2) 
				$('#activeButton').click(); 
		} 
	}); 
}
function bindLoginButtonClick() {
	$('#activeButton').click(function() {
		var code = $("#code").val();
		if(code != null && code != ""){
			 $.ajax({
				type: "post", 
		        url: $.baseUrl+'/onActiveSys', 
		        dataType: "json",
		        data : {
		        	code : code
		        },success : function(data){
		        	if(data.message == "N"){
		        		$("#tooltipspan").text("激活码错误，请重试");
		        	}else if(data.message == "Y"){
		        		if (confirm("您已激活！点击确定进入登录页面")) {
		        		    window.location.href = $.baseUrl+'/login';
		        		}
		        	}else{
		        		alert(data.message);
		        	}
		        }
			 })
		}else{
			$("#tooltipspan").text("激活码不能为空");
		}
		
	});
}

</script>
		
	</head>
		
	<body>
		<!--头部信息-->
		<div class="common_top clearfix">
			<div class="main-width clearfix">
				<div class="common_logo_box icon sage"></div>
			</div>
		</div>
		<div class="loginBg">
			<img src="scrm/img/loginbg.png"/>
			<div class="loginBox">
					<div class="loginForm">
						<div style="text-align:center">
							<br><h1>欢迎使用本系统，请输入激活码</h1><br><br><br>
						</div>
						<div>
							<span>激活码</span>
							<input id="code" name="code" data-options="required:true" class="form-control" style="margin-bottom: 15px;"/>
							<span id="tooltipspan" style="color:red;width:150px;height:30px;margin:0 75px;display:block;"></span>
						</div>
						<div>
							<button type="button" id="activeButton" class="btn btn-success btn-block loginBtn">激活</button>
						</div>
					</div>
			</div>
			<p class="loginBottom">Sage赛捷软件提供授权</p>
		</div>
	</body>

</html>