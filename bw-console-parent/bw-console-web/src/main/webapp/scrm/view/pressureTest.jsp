<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>test</title>
		<script type="text/javascript" src="../js/lib/jquery.min.js"></script>
	</head>
	<body>
	<hr/>
	<span>功能</span><br/>
	<span>同步订单</span><button id="handleOrder">click me</button><span id="orderResult"></span><br/><br/><br/><br/>
	<span>会员注册</span><button id="memberReg">click me</button><span id="regResult"></span>
	</body>
	<script type="text/javascript">
	$("#handleOrder").click(function(){
		var orderXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    		+ "<orderHeaderDto>"
      		+ "<externalId>ESB4JE914RHFC9SZ</externalId>"
      		+ "<cardNo>10000000000035</cardNo>"
      		+ "<memberCode>M0000000293</memberCode>"
      		+ "<channelId>104</channelId>"
      		+ "<productStoreId>{F0EE8FFC-2160-4099-A18A-6239104E548A}</productStoreId>"
      		+ "<productStoreName>test</productStoreName>"
      		+ "<orderDate>20140218</orderDate>"
      		+ "<createTime>20140218144419</createTime>"
      		+ "<orderAmount>33</orderAmount>"
      		+ "<payAmount>33</payAmount>"
      		+ "<tableFor>1</tableFor>"
      		+ "<optType>i</optType>"
      		+ "<ActiveCode>ESB6KE994EBFC9BE</ActiveCode>"
      		+ "<statusId>4</statusId>"
      		+ "<dataVersion>1</dataVersion>"
      		+ "<mobile/>"
      		+ "<orderItemDto>"
      		+ "<externalItemId>1</externalItemId>"
      		+ "<name>product</name>"
      		+ "<productCode>206</productCode>"
      		+ "<quantity>1.00</quantity>"
      		+ "<unitPrice>33.00</unitPrice>"
      		+ "<amount>33.00</amount>"
      		+ "<groupId>15</groupId>"
      		+ "<isCoupon>N</isCoupon>"
      		+ "</orderItemDto>"
      		+ "<orderItemDto>"
      		+ "<externalItemId>2</externalItemId>"
      		+ "<name>cake</name>"
      		+ "<productId>20045</productId>"
      		+ "<quantity>1.00</quantity>"
      		+ "<unitPrice>0.00</unitPrice>"
      		+ "<amount>0.00</amount>"
      		+ "<groupId>11</groupId>"
      		+ "<isCoupon>Y</isCoupon>"
      		+ "</orderItemDto>"
      		+ "<orderCouponDto>"
      		+ "<externalItemId>2</externalItemId>"
      		+ "<couponInstanceCode>000166112</couponInstanceCode>"
      		+ "<couponAmount>0.00</couponAmount>"
      		+ "</orderCouponDto>"
      		+ "<orderPayDto>"
      		+ "<externalItemId>1</externalItemId>"
      		+ "<name>cash</name>"
      		+ "<PayId>1</PayId>"
      		+ "<Amount>33.00</Amount>"
      		+ "</orderPayDto>"
      		+ "</orderHeaderDto>";
      		
      		$("#orderXml").val(orderXml);
      		
      		$("#orderForm").submit();
		}); 
	
	$("#memberReg").click(function(){
		$("#memberForm").submit();
	})
	</script>
	
	
	<form id="orderForm" action="http://10.174.141.169/scrm-web-main/pressureTest/testHandleOrder.json" method="post">
		<input type="hidden" id="orderXml" name="orderXml" value=""/>
	</form>
	
	<form id="memberForm" action="http://10.174.141.169/scrm-web-main/pressureTest/testRegisterMember.json" method="post">
		
	</form>
	
	</html>