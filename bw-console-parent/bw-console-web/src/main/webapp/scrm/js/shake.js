$(document).ready(function(){
	
	var pathName=window.document.location.pathname;
	var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
	$.baseUrl = projectName;//初始化项目根路径
	
	$("#submit").click(function(){
		var teleNumber =$("#name").val();
		var userName =$("#mobile").val();
		
		if(teleNumber ==""||teleNumber==null){
			alert("电话号码不能为空!");
		}else if(userName==""||userName==null){
			alert("用户名不能为空!");
		}else{
			$.ajax({
				type : "get",
				contentType:"application/x-www-form-urlencoded; charset=utf-8",
				url : $.baseUrl+'/shakeController/savePersonInfo.do',
				data : "teleNumber="+teleNumber+"&userName="+userName,
				dataType : "json",
				async: false,
				success : function(result) {
					if(result.status =='success'){
						// 跳转到获奖详情页面
						jumpPrizeLink(result.data);
					}
				},
				error : function() {
					
				}
			});
		}
	});

	valiForm();
	
	
});


function jumpPrizeLink(obj){
	window.location.href="prize.html?userId="+obj;
}

function valiForm(){
	$(".userPanel").validator({
		rules:{
			valiName:function(el){
				var nameOk = new RegExp("^[\u4E00-\u9FA5]").test($('#name').val());
				if(nameOk){
					return true;
				}else{
					return "请输入中文";
				}
			},
			valiMobile:function(el){
	    		var isMobile = new RegExp("^13[0-9]{9}$|15[0-9]{9}$|18[0-9]{9}$|147[0-9]{8}$").test($('#mobile').val());
	      	  	if(!isMobile) {
	      	  		return "手机号不合法";
	      	  	}
	        }
		},
		fields: {
		  '#name':"姓名:required;valiName",
	      '#mobile':"手机号:required;valiMobile"
		}
	}).on("click", ".btn", function(){
		$(".btn").trigger("submit");
	}).on('valid.field invalid.field', ':input', function(e){
		$(this).parent(".line")[ e.type === "valid" ? "removeClass" : "addClass" ]('n-invalid');
	}).on('valid.form',function(){
		alert("可以通过");
	});
}