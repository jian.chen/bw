var complainIds;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initSelect();
		initDate();
		initInput();
		initDataGrid();
	}
	$("#search_btn").click(function(){
		$("#complainTable").datagrid('load',serializeObject($("#complainSearchForm")));
	});
	
	return {
		init: init
	}
});

//初始化数据
function initDataGrid(){
	//初始化table
	$('#complainTable').datagrid( {
		url : $.baseUrl+'/complainController/showComplainList.do',
		queryParams : serializeObject($("#complainSearchForm")),
		idField : 'roleId',
		pageNumber : 1,
		pageSize : 10,
		pageList : [ 1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 ],
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}
//DateGrid Date时间格式化
function dateDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}
//DateGrid Time时间格式化
function timeDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + ' ' +val.substring(8, 10)+':'+val.substring(10,12)+':'+val.substring(12,14) ;
	}
}

//Date时间格式化
function dateDateformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}

function dateTimeformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + " " + val.substring(8, 10) + ":" + val.substring(10, 12) + ":" + val.substring(12, 14);
	}
}
//获取当前时间(yyyymmdd)
function getDate(){
	var date = new Date();
	var str = '';
	if(date.getMonth()+1<10){
		str = date.getFullYear()+"0"+(date.getMonth()+1);
		
	}else{
		str = date.getFullYear()+""+(date.getMonth()+1);
		
	}
	if(date.getDate()<10){
		str += "0"+date.getDate();
	}else{
		str += date.getDate();
	}
	return str;
}