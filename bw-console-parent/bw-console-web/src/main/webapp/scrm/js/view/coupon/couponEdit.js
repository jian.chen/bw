var couponRange;
define(['common'], function(common) {
	function init() {
		//设置下一步上一步
		$("#nextSetting").click(function(){
			$("#coupon-add-first-setting").hide();
			$("#coupon-add-second-setting").show();
			$("#div-title-first").hide();
			$("#div-title-second").show();
		});
		
		$("#beforeSetting").click(function(){
			$("#coupon-add-first-setting").show();
			$("#coupon-add-second-setting").hide();
			$("#div-title-first").show();
			$("#div-title-second").hide();
		});
		
		$("#goback").click(function(){
			common.Page.loadCenter('coupon/couponManage.html');
		})
		
		if(editStatus == "N"){
			$("[name='couponName']").attr("readonly","readonly");
			$("[name='expireType']").attr("disabled","disabled");
			$("[name='fromDate']").attr("readonly","readonly");
			$("[name='fromDate']").removeAttr("onfocus");
			$("[name='thruDate']").attr("readonly","readonly");
			$("[name='thruDate']").removeAttr("onfocus");
			$("[name='afterDate']").attr("readonly","readonly");
			$("[name='expireValue']").attr("readonly","readonly");
			$("[name='equalOrgName']").attr("readonly","readonly");
			$("[name='extField1']").attr("readonly","readonly");
			$("#equalOrgId").attr("disabled","disabled");
			$("#couponTypeName").attr("disabled","disabled");
			$("#couponEdit_comments").attr("readonly","readonly");
			$("#couponEdit_commentsEn").attr("readonly","readonly");
			$("#couponEdit_remark").attr("readonly","readonly");
			$("#couponEdit_remarkEn").attr("readonly","readonly");
			$("#couponEdit_webchatRemark").attr("readonly","readonly");
			$("#coupon_type").attr("disabled","disabled");
			$("#couponTYpe_type").attr("disabled","disabled");
			$("#couponMer_type").attr("disabled","disabled");
//			
		}
		
		//如果是已下发的状态，可允许修改以下字段
		if(couponStatus=='1402'){
			$("[name='couponName']").removeAttr("readonly");
			$("#couponEdit_comments").removeAttr("readonly");
			$("#couponEdit_commentsEn").removeAttr("readonly");
			$("#couponEdit_remark").removeAttr("readonly");
			$("#couponEdit_remarkEn").removeAttr("readonly");
			$("#couponEdit_webchatRemark").removeAttr("readonly");
			$("#couponEdit_extField1En").removeAttr("readonly");
			$("#couponEdit_extField1").removeAttr("readonly");
			$("#couponEdit_image").removeAttr("disabled");
			$("#couponEdit2_image").removeAttr("disabled");
			$("#coupon_type").attr("disabled","disabled");
			$("#couponTYpe_type").attr("disabled","disabled");
			$("#couponMer_type").attr("disabled","disabled");
			$("#equalOrgId").removeAttr("disabled")
		}
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		$("#couponEdit_image").uploadPreview({ Img: "couponEdit_img3_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		$("#couponEdit2_image").uploadPreview({ Img: "couponEdit_img2_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		initSelect();
		initDate();
		initInput();
		couponRange = '';
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
		//区域门店弹框
		$('#equalOrgId').click(function() {
			initTreeData("areaChoose","areaTree","/orgController/testTree.do","1");
			checkNode(stringToArr(couponRange),"areaTree");
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTree",1);
			var selectNames= getSelectName("areaTree",1);
			if(selectIds){
				$("#couponEdit_extField1").attr("data-options","length:'50',required:true");
			}else{
				$("#couponEdit_extField1").attr("data-options","");
			}
			$("[name='equalOrgId']").val(selectIds);
			$("[name='equalOrgName']").val(selectNames);
			$('#areaChoose').find(".detailed_close2").click();
		});
		$('#couponEdit_image').change(function() {
			$("#couponEdit_fileSpan").text($(this).val());
		});
		initCouponEditDataGrid();
		
		
		$("input:radio[name='expireType']").change(function (){
			if(this.value=='1'){
				$("#couponEdit_afterDate").val("");
				$("#couponEdit_expireValue").val("");
				$("#couponEdit_thruDate").attr("data-options","required:true");
				$("#couponEdit_afterDate").attr("data-options","length:'3',validType:'rtZero'");
				$("#couponEdit_expireValue").attr("data-options","length:'3',validType:'rtZero'");
			}else if(this.value=='2'){
				$("#couponEdit_thruDate").val("");
				$("#couponEdit_expireValue").val("");
				$("#couponEdit_thruDate").attr("data-options","");
				$("#couponEdit_afterDate").attr("data-options","required:true,length:'3',validType:'rtZero'");
				$("#couponEdit_expireValue").attr("data-options","required:true,length:'3',validType:'rtZero'");
			}else{
				$("#couponEdit_afterDate").val("");
				$("#couponEdit_expireValue").val("");
				$("#couponEdit_thruDate").attr("data-options","");
				$("#couponEdit_afterDate").attr("data-options","");
				$("#couponEdit_expireValue").attr("data-options","");
			}
		 });
		
		$("input:radio[name='couponTypeId']").change(function (){
			if(this.value=='1'){
				$("#couponEdit_useSatisfyAmount").attr("data-options","length:'16',validType:'rtNZero'");
				$("#couponEdit_amount").attr("data-options","length:'16',validType:'rtZero'");
				$("#couponEdit_discount").attr("data-options","required:true,validType:'ishundred'");
				$("#couponEdit_useSatisfyAmount").val("");
				$("#couponEdit_amount").val("");
			}else if(this.value=='2'){
				$("#couponEdit_discount").attr("data-options","validType:'ishundred'");
				$("#couponEdit_useSatisfyAmount").attr("data-options","required:true,length:'16',validType:'rtNZero'");
				$("#couponEdit_amount").attr("data-options","required:true,length:'16',validType:'rtZero'");
				$("#couponEdit_discount").val("");
			}else if(this.value=='3'){
				$("#couponEdit_discount").attr("data-options","validType:'ishundred'");
				$("#couponEdit_useSatisfyAmount").attr("data-options","length:'16',validType:'rtNZero'");
				$("#couponEdit_amount").attr("data-options","length:'16',validType:'rtZero'");
				$("#couponEdit_discount").val("");
				$("#couponEdit_useSatisfyAmount").val("");
				$("#couponEdit_amount").val("");
			}
		});
		
		if($('#couponOriginId').val()=='1'){
			$('#merchat').hide();
			$('#externalMerchantId').val('0');
		}
		
		
		$("#couponEditSave").click(function(){
			if($("input:radio[name='expireType']:checked").val()=='1'){
				if($("#couponEdit_fromDate").val()==''){
					alert("请选择开始时间!");
					return;
				}
				if($("#couponEdit_thruDate").val()==''){
					alert("请选择结束时间!");
					return;
				}
			}
			if($("#isReceiveCK").is(':checked')){
				$("#isReceive").val("N");
			}else{
				$("#isReceive").val("Y");
			}
			if($("#isRemindCK").is(':checked')){
				$("#isRemind").val("Y");
			}else{
				$("#isRemind").val("N");
			}
			if($("#isOwnerUsedCK").is(':checked')){
				$("#isOwnerUsed").val("N");
			}else{
				$("#isOwnerUsed").val("Y");
			}
			if(checkForm('couponEdit')){
				$.ajax({
					type: "POST",
					url : $.baseUrl+'/couponController/editCoupon.do',
					data:$('#couponEdit_form').serialize(),
					async: false,
					dataType : "json",
					success : function(result) {
						result = eval('(' + result + ')');
						$.messager.alert("提示",result.message);
						common.Page.loadCenter('coupon/couponManage.html');
					},
					error : function() {
						parent.layer.alert("出错了:(");
					}
				});
				
			}
		})
		
	}
	
	return {
		init: init
	}
});
function showname(conid,value,targetid)
{
	
$("#"+targetid).val($("#"+conid).children("li[data-id='"+value+"']").find("a").html());	
}
function initCouponEditDataGrid(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/couponController/queryCouponDetail.do',
		data : "couponid="+couponIds,
		dataType : "json",
		async: false,
		success : function(result) {
			couponRange = result.couponRange.orgIds;
			$("#couponEdit_couponId").val(couponIds);
			$("#couponEdit_couponName").val(result.coupon.couponName);
			if(result.coupon.expireType=='1'){
				$("input[name='expireType']:eq(0)").attr("checked",'checked');
				$("#couponEdit_fromDate").val(dateDateformat(result.coupon.fromDate));
				$("#couponEdit_thruDate").val(dateDateformat(result.coupon.thruDate));
			}else if(result.coupon.expireType=='2'){
				$("input[name='expireType']:eq(1)").attr("checked",'checked');
				$("#couponEdit_afterDate").val(result.coupon.afterDate);
				$("#couponEdit_expireValue").val(result.coupon.expireValue);
			}
			$("#couponEdit_orgId").val(result.coupon.orgId);
			$("#couponEdit_extField1").val(result.coupon.extField1);
			
			$("#coupon_type").val(result.couponType.couponTypeName);
			$("#couponType").val(result.couponType.couponTypeId);
			$("#equalOrgId").val(result.couponRange.orgName);
			//新修改参数值
			//$('#coupon_type').val(result.couponType.couponTypeName);
			$('#couponEdit_couponType').val(result.coupon.couponTypeId);
			$("#couponTYpe_type").val(result.coupon.couponTypeName);
			$('#couponOriginId').val(result.coupon.originId);
			$('#externalMerchantId').val(result.coupon.externalMerchantId);
			
			showname("ul",result.coupon.originId,"couponTYpe_type");
			showname("ul1",result.coupon.externalMerchantId,"couponMer_type");
			showname("ul2",result.couponType.couponTypeId,"coupon_type");
			
			
			$("input[name='equalOrgId']").val(result.couponRange.orgIds);
			$("#couponEdit_img3_path").attr("src",result.coupon.couponImg3);
			$("#couponEdit_img3").val(result.coupon.couponImg3);
			$("#couponEdit_img2_path").attr("src",result.coupon.couponImg2);
			$("#couponEdit_img2").val(result.coupon.couponImg2);
			$("#couponEdit_comments").val(result.coupon.comments);
			$("#couponEdit_remark").val(result.coupon.remark);
			
			$("#couponEdit_couponNameEn").val(result.coupon.couponNameEn);
			$("#couponEdit_commentsEn").val(result.coupon.commentsEn);
			$("#couponEdit_remarkEn").val(result.coupon.remarkEn);
			$("#couponEdit_webchatRemark").val(result.coupon.webchatRemark);
			$("#couponEdit_extField1En").val(result.coupon.extField1En);
			
			if(result.coupon.isReceive == 'N'){
				$("#isReceiveCK").attr("checked","checked")
			}
			if(result.coupon.isRemind == 'Y'){
				$("#isRemindCK").attr("checked","checked")
			}
			if(result.coupon.isOwnerUsed == 'N'){
				$("#isOwnerUsedCK").attr("checked","checked")
			}
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});
}

function stringToArr(str){
	var arr = new Array();
	arr = str.split(",");
	return arr;
}

