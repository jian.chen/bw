define(['common', 'view/base/mealManage'], function(common,mealManage) {
	function init() {
		common.Page.resetMain();
		var savetype;
		
		//初始化输入框验证
		initInput();
		
		//时间组件文本不能编辑
		$('#startTime').spinner({ editable: false });
		$('#endTime').spinner({ editable: false });

		if(mealManage.getType() == 'show'){
			$("#meal_title").html('时段详情');
			$("#meal_title_name").html('时段详情');
			
			$("#savea").hide();
			show();
			//表单禁用
			$("#mealPeriodName").attr("disabled","disabled");
			$("#remark").attr("disabled","disabled");
			$(".weeksList").find("input[name='weeks']").prop("disabled",'disabled');
			$('#startTime').spinner({ disabled: true });
			$('#endTime').spinner({ disabled: true });
		}else if(mealManage.getType() == 'edit'){
			$("#meal_title").html('时段编辑');
			$("#meal_title_name").html('时段编辑');
			
			$("#mealPeriodId").val(mealManage.getMealPeriodId());
			$("#savea").show();
			savetype = 'edit';
			show();
		}else {
			$("#meal_title").html('时段新建');
			$("#meal_title_name").html('时段新建');
			savetype = 'save';
		}
		
		//保存按钮
		$("#savea").click(function(){
			var ck = formCheck($("#mealPeriodForm"));
			if(ck){
				if(savetype == 'edit'){
					editMealPeriod();
				}else{
					saveMealPeriod();
				}
			}else{
				return;
			}
		});
		
		//返回按钮
		$("#goback").click(function(){
			common.Page.loadCenter('base/mealManage.html');
		});
		
		var mealPeriodId;
		//时段名称不能重复
		$("[name='mealPeriodName']").blur(function(){
			if($("[name='mealPeriodName']") != ""){
				if(savetype == 'save'){
					mealPeriodId = null;
				}else{
					mealPeriodId = mealManage.getMealPeriodId();
				}

				$.ajax({ 
			        type: "post", 
			        url: $.baseUrl+'/mealController/showCountByNameById.do', 
			        dataType: "json",
			        data : {
			        	mealPeriodName: $(this).val(),
			        	mealPeriodId: mealPeriodId
			        },
			        async: false,
			        success: function (result) {
			        	if(result.status=='300'){
			        		alert(result.message);
			        	}
			        }, 
			        error: function () { 
			        	parent.layer.alert("出错了:(");
			        }
			    });	
			}	  	
		});
		
	}
	
	var weeksArr = new Array();
	//查看
	function show(){
		$.ajax({
			url : $.baseUrl+'/mealController/showMealPeriodById.do',
			type:"POST",
			data:"mealPeriodId="+mealManage.getMealPeriodId(),
			datatype:"json",
			success:function(result){
				if(result){
					result = eval('(' + result + ')');
					
					$("#mealPeriodId").val(result.obj.meal.mealPeriodId);
					$("#mealPeriodName").val(result.obj.meal.mealPeriodName);						
					$('#startTime').timespinner('setValue', result.obj.meal.startTime);
					$('#endTime').timespinner('setValue', result.obj.meal.endTime);
					$("#remark").val(result.obj.meal.remark);
					
					weeksArr = result.obj.meal.weeks;
					if(weeksArr){
						for(var i=0;i<weeksArr.length;i++){
							value=weeksArr.split(",")[i];
							$(".weeksList").find("input[name='weeks'][value='" + value + "']").prop("checked",'true');
						}
					}
				}else{
					alert('出错了！');
				}
			}
		});
	}
	
	//开始时间和结束时间不能相同
	function checkTime(){
		var startTime=$("#startTime").val();
		var endTime=$("#endTime").val();
		if(startTime==endTime){
			alert("开始时间和结束时间不能相同！！！");
			return false;
		}else{
			return true;
		}
	}
	
	//新建时段
	function saveMealPeriod(){
		if(checkTime()){
			//$("#dataLoad").show();
			$.ajax({
				url:$.baseUrl+'/mealController/saveMealPeriod.do',
				type:"POST",
				data:serializeObject($('#mealPeriodForm')),
				datatype:"json",
				success:function(result){
					//$("#dataLoad").hide();
					result = $.parseJSON(result);
					if(result.status == '200'){
						alert(result.message);
						common.Page.loadCenter('base/mealManage.html');
					}else{
						alert(result.message);
					}				
				}
			});			
		}
	}
	
	//编辑时段
	function editMealPeriod(){
		if(checkTime()){
			//$("#dataLoad").show();
			$.ajax({
				url:$.baseUrl+'/mealController/editMealPeriod.do',
				type:"POST",
				data:serializeObject($('#mealPeriodForm')),
				datatype:"json",
				success:function(result){
					//$("#dataLoad").hide();
					result = $.parseJSON(result);
					if(result.status == '200'){
						alert(result.message);
						common.Page.loadCenter('base/mealManage.html');
					}else{					
						alert(result.message);
					}
				}
			});		
		}
	}
	
	return {
		init: init
	}
});



