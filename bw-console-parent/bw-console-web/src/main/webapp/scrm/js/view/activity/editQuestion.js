define(['common'], function(common) {
	
	function init() {
		showEditQuestion();
		$("#updateQuestion").click(function(){
			var questionName = $("#questionNameEdit").val();
			var explains = $("#explainsEdit").val();
			var endExplain = $("#endExplainEdit").val();
			var startDate = $("#startDateEdit").val();
			var endDate = $("#endDateEdit").val();
			if(questionName == ""){
				$("#questionName").alert("不能为空");
				return;
			}
			if(explains == ""){
				$("#explains").alert("不能为空");
				return;
			}
			$.ajax({ 
		        type: "post", 
		        url: $.baseUrl+'/QuestionController/updateQuestion', 
				dataType : "json",
		        data : {
		        	questionnaireId : editQuestionId,
		        	questionName: questionName, 
		        	explains: explains,
		        	endExplain : endExplain,
		        	startDate : startDate,
		        	endDate : endDate
		        },
		        success: function (data) {
		    		alert(data.message);
		    		$(".detailed_close").closest('.mask, .mask_in').hide();
		    		$('#questionTable').datagrid('reload');
		        }
		    });
			
		});
	}
	
	return {
		init: init
	}
});

function showEditQuestion(){
	$.ajax({
		type: "post", 
        url: $.baseUrl+'/QuestionController/showEditQuestion', 
		dataType : "json",
        data : {
        	editQuestionId : editQuestionId
        },
        success : function (data){
        	$("#questionNameEdit").val(data.editResult.questionName);
        	$("#explainsEdit").val(data.editResult.explains);
        	$("#endExplainEdit").val(data.editResult.endExplain);
        	$("#startDateEdit").val(getDateString(data.editResult.startDate));
        	$("#endDateEdit").val(getDateString(data.editResult.endDate));
        }
	})
}


