var groupIdParam="";

define(['common'], function(common) {
	// 全局变量  闭包
	/*window.formatProgress = function(value,row,index){
		var s = '<div class="relative"><input type="hidden" name="" id="" value="">'+
				'<input size="16" type="text" class="form-control" value="- 请选择 -" input-type="select" readonly="">'+
				'<ul id="dataGridRow" class="dropdown-menu" url-options="url:'+"'/wechatFansGroupController/showWechatFansGroup.do'"+',value:'+"'wechatFansGroupId'"+',text:'+"'groupName'"+' ">'+
				'</ul>'+
				'<i class="input_icon input_down"></i>'+
				'</div>';
		return s;
	}*/
	function init() {
		common.Page.resetMain();
		initSelect();
		//初始化页面布局
		initPage();
		//初始化表格
		initDataGrid();
		//筛选显示
		$("#fenzuxianshiul").delegate("li",'click',function(){
			$("#showGroupName").val($(this).text());
			groupIdParam = $(this).attr("data-id");
			if(groupIdParam!=null&&groupIdParam!=""){
				$("#reName").css("display","inline-block");
				$("#delete").css("display","inline-block");
			}
			initDataGrid();
		});
		
		//粉丝添加显示
		$("#fenzutianjiaul").delegate("li","click",function(){
			var groupId = $(this).attr("data-id");
			var wechatFansIds ="";
			var openIds="";
			var getSelections  = $('#fensifenzuTable').datagrid('getSelections');
			for(var i=0;i<getSelections.length;i++){
				if(i<getSelections.length-1){
					wechatFansIds+=getSelections[i].wechatFansId+",";
					openIds +=getSelections[i].openid+",";
				}else{
					wechatFansIds+=getSelections[i].wechatFansId;
					openIds +=getSelections[i].openid;
				}
			}
			$.ajax({
		        type: "post",
		        url: $.baseUrl+'/wechatFansController/batchRemoveGroup.do',
		        data:{groupId:groupId,wechatFansIds:wechatFansIds,openIds:openIds},
		        async: true,
		        success: function (data) {
		        	// 刷新页面
	        		$("#wechatFansGroup").click();
		        }, 
		        error: function (XMLHttpRequest, textStatus, errorThrown) { 
		                alert(errorThrown); 
		        } 
			});
			
		});
		
		// 按昵称查询 
		$("#icon_search_btn").click(function(){
			var nickName =$("#nickNameSearch").val();
			$('#fensifenzuTable').datagrid({
				url : $.baseUrl+'/wechatFansGroupController/showWechatFansGroupList.do',
				method :'post',// 请求类型 
				singleSelect : false,// 允许多行
				loadMsg:"数据加载中...",
				queryParams:{nickName:nickName},
				onLoadSuccess : function(result) {
					
				},
				onLoadError : function() {
						
					}
				});
		});
		
		// 新建分组框
		$("#xinjianfenzuBtn").click(function(){
			$("#xinjianfenzukuang").show();
		});
		// 重命名
		$("#reName").click(function(){
			$("#updatefenzukuang").show();
		});
		
		// 修改分组名称
		$("#updateGroupName").click(function(){
			var updateGroupNameStr =$("#updateGroupNameStr").val();
			$("#groupNameForm").form('submit',{
				url: $.baseUrl+'/wechatFansController/updateWechatFansGroup.do',
				onSubmit:function(param){
					param.wechatGroupId= groupIdParam;
					param.groupNameStr= updateGroupNameStr;
				},
			    success: function(data){
				var data = eval('(' + data + ')');  // change the JSON string to javascript object
		        if (data.status == 'success'){
		        		alert("修改分组名称成功!");
		        		//清除表单数据
		        		$("#groupNameForm").form('clear');
		        		$("#xinjianfenzukuang").hide();
		        		// 刷新页面
		        		// window.location.reload();
		        		$("#wechatFansGroup").click();
		        	}    
			    }
			});
		});
		
		// 删除分组
		$("#delete").click(function(){
			$.ajax({
		        type: "post",
		        url: $.baseUrl+'/wechatFansController/deleteWechatFansGroup.do',
		        data:{wechatGroupId:groupIdParam},
		        async: true,
		        success: function (data) {
		        	// 刷新页面
	        		$("#wechatFansGroup").click();
		        }, 
		        error: function (XMLHttpRequest, textStatus, errorThrown) { 
		                alert(errorThrown); 
		        } 
			});
		});
		
		$("#saveGroupName").click(function(){
			$("#groupNameForm").form('submit',{
				url: $.baseUrl+'/wechatFansController/saveWechatFansGroup.do',
				success: function(data){
					var data = eval('(' + data + ')');  // change the JSON string to javascript object
					if (data.status == 'success'){
						alert("新建分组成功!");
						//清除表单数据
						$("#groupNameForm").form('clear');
						$("#xinjianfenzukuang").hide();
						// 刷新页面
						// window.location.reload();
						$("#wechatFansGroup").click();
					}    
				}
			});
		});
		
	}
	return {
		init: init
	}
	
	// 初始化页面布局
	function initPage(){
		$("#showGroupName").val("---全部---");
	}
	
	//初始化表格数据
	function initDataGrid(){
		$('#fensifenzuTable').datagrid({
			url : $.baseUrl+'/wechatFansGroupController/showWechatFansGroupList.do',
			method :'post',
			singleSelect : false,
			queryParams:{wechatGroupId:groupIdParam},
			loadMsg:"数据加载中...",
			onLoadSuccess : function(result) {
			},
			onLoadError : function() {
				}
			});
		}
	
	function initSelectedCell(value){
		$.ajax({
	        type: "get",
	        url: $.baseUrl+'/wechatFansGroupController/showWechatFansGroup.do',
	        async: true,
	        success: function (data) {
	        	var data = eval('(' + data + ')');  // change the JSON string to javascript object
	        	
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	                alert(errorThrown); 
	        } 
		});
	}
	
	
	function onClickRow(index){
		if (editIndex != index){
			if (endEditing()){
				$('#dg').datagrid('selectRow', index)
						.datagrid('beginEdit', index);
				editIndex = index;
			} else {
				$('#dg').datagrid('selectRow', editIndex);
			}
		}
	}
});