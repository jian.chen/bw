define(['common'], function(common) {
	
	function init() {
		instanceAllMemberVote();
		showVoteCount();
		$('#goback').click(function(){
			backUrl = 'activity/voteTitle.html';
			clickPoints = 'vote';
			common.Page.loadCenter('activity/publicManage.html');
		});
		
	}
	
	return {
		init: init
	}
	
});

function showVoteCount(){
	$.ajax({
		type : 'post',
		url : $.baseUrl + '/VoteController/showVoteCount',
		data : {
			titleId : voteCountId
		},
		dataType : 'json',
		success : function(data){
			var sum = 0;
			for(var x = 0;x < data.result.length;x++){
				sum = sum + data.result[x].memberSum;
			}
			
			var tempHtml = '';
			tempHtml += "<table class='question-table'>";
			for(var i = 0,j = 1;i<data.result.length,j<data.result.length+1;i++,j++){
				var rate = (data.result[i].memberSum*100/sum).toFixed(2);
				tempHtml += "<tr>";
				tempHtml += "<td class='score'>选项" + j + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + data.result[i].optionName + "</td>";
				tempHtml += "<td class='person-total'>选择<span>" + data.result[i].memberSum + "</span>人</td>";
				tempHtml += "<td class='percent'><span class='percent-total'><span class='percent-length' style='width:" + rate + "%'></span></span><span>" + rate + "%</span></td>";
				tempHtml += "</tr>";
			}
			tempHtml += "</table>";
			$('#countListVote').append(tempHtml);
		},error : function(){
			
		}
	})
}

function instanceAllMemberVote(){
	$.ajax({
		type : 'post',
		url : $.baseUrl + '/VoteController/voteInstanceAllMember',
		data : {
			titleId : voteCountId
		},
		dataType : 'json',
		success : function(data){
			$("#voteMemberAllCount").text(data.sum);
		},error : function(){
			
		}
	})
}


