var productId;
define(['common'], function(common) {
	function init() {
		var sortList = new Array();
		var prdSortArr = [];
		var prdSortArrUpdate = [];
		getSortList(prdSortArr,prdSortArrUpdate);
		
		common.Page.resetMain();
		initDataGrid();
		initInput();
		
		//productSort
		$('#fileInput').change(function() {
			$("#fileSpan").text($(this).val());
		});
		//下载模板
		$('#downImportTemp').click(function() {
			downTemp();
		});
		//导入提交
		$('#importBtn').click(function() {
			importData();
		});
		
		$('#importProductBtn').click(function() {
			$('#daoruchanpin').show();
		});
		
		//查看
		$('#showProductBtn').click(function(){
			if(checkSingle()){
				productId = getSingleCheckedId();
				var url=$.baseUrl+"/productController/productDetail.do?productId="+productId;
				$.ajax({
					type:"POST",
					url:url,
					dataType:"JSON",
					success:function(data){
						var itemList = data.itemList;
						var itemIds =[];
						for(var i = 0;i < itemList.length; i++){
							itemIds.push(itemList[i].productCategoryId);
						}
						$('#showProductId').val(data.productId);
						$('#showProductCode').val(data.productCode);
						$('#showProductName').val(data.productName);
						$('#prdSortShow-multiple').multiselect('select', itemIds);
					}
				});
				$('#chakanchanpin').show();
			}else{
				$.messager.alert("提示","请勾选一条记录查询!");
			}			
			
		});
		$('#editProductBtn').click(function(){
			if(checkSingle()){
				productId = getSingleCheckedId();
				var url=$.baseUrl+"/productController/productDetail.do?productId="+productId;
				$.ajax({
					type:"POST",
					url:url,
					dataType:"JSON",
					success:function(data){
						var itemList = data.itemList;
						var itemIds =[];
						for(var i = 0;i < itemList.length; i++){
							itemIds.push(itemList[i].productCategoryId);
						}
						$('#updateProductId').val(data.productId);
						$('#updateProductCode').val(data.productCode);
						$('#updateProductName').val(data.productName);
						$('#prdSortUpdate-multiple').multiselect('select', itemIds);
					}
				});
				$('#bianjichanpin').show();
			}else{
				$.messager.alert("提示","请勾选一条记录查询!");
			}			
			
		});
		//删除
		$('#deleteProductBtn').click(function(){
			if(checkSingles()){
				$.messager.confirm("确认","确认删除？",function(){
					productId = getSingleCheckedId();
					$.ajax({
						type:"POST",
						url:$.baseUrl+"/productController/removeProduct.do",
						dataType:"JSON",
						data : {
							productId : productId
						},
						success:function(result){
							$.messager.alert("提示",result.message);
							if(result.status == '200'){								
								$("#productTable").datagrid('clearChecked');
								$("#productTable").datagrid('reload');
							}
						}
					});
				})
				
			}else{
				$.messager.alert("提示","请勾选一条记录查询!");
			}			
			
		});
		
		$('#addProductBtn').click(function(){
			$('#prdSort-multiple option:selected').each(function() {
                $(this).prop('selected', false);
            });
			$('#prdSort-multiple').multiselect('refresh');
			
        	$('#saveProductCode').val("");
        	$('#saveProductName').val("");
			$('#xinjianchanpin').show();
		});
		
		//新建保存
		$('#save_btn').click(function(){
			var ck = formCheck($("#save_form"));
			if(prdSortArr.length == 0){
				$.messager.alert("提示","请选择产品类别");
				return;
			}
			if(ck){
	    		var url=$.baseUrl+"/productController/saveProduct.do";
	        	$.ajax({
	                type:"POST",
	                url:url,
	                dataType:"JSON",
	                data : {
	                	productCode : $("#saveProductCode").val(),
	                	productName : $("#saveProductName").val(),
	                	sortList : JSON.stringify(prdSortArr)
	                },
	                success:function(result){
						$.messager.alert("提示",result.message);
						if(result.status == '200'){
							$('#xinjianchanpin').hide();
							$("#productTable").datagrid('load');
						}
	                }
	            });				
			}
		});
		
		//编辑保存
		$('#update_btn').click(function(){
			var ck = formCheck($("#update_form"));
			if(prdSortArrUpdate.length == 0){
				$.messager.alert("提示","请选择产品类别");
				return;
			}
			if(ck){
				var url=$.baseUrl+"/productController/editProduct.do";
				$.ajax({
					type:"POST",
					url:url,
					dataType:"JSON",
					data : {
						productId : $("#updateProductId").val(),
	                	productCode : $("#updateProductCode").val(),
	                	productName : $("#updateProductName").val(),
	                	sortList : JSON.stringify(prdSortArrUpdate)
	                },
					success:function(result){
						$.messager.alert("提示",result.message);
						if(result.status == '200'){
							$('#bianjichanpin').hide();
							$("#productTable").datagrid('load');
						}
					}
				});
			}else{
				alert("请输入正确格式的数据");
			}
		});		
		
		//导入批量保存
		$('#import_save_btn').click(function() {
			alert("批量保存！");
		});
		
	}
	
	function initDataGrid(){
		$('#productTable').datagrid( {
			url : $.baseUrl+'/productController/showProduct.do',
			idField : 'productId',
			onLoadSuccess : function(result) {
					if(result) {
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	

	function getSortList(prdSortArr,prdSortArrUpdate){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/scrm/view/showProductSortForJS",
			dataType : "json",
			success : function(data){
				sortList = data.sortList;
				initMultiple("prdSort-multiple",prdSortArr,sortList);
				initMultiple("prdSortShow-multiple",prdSortArr,sortList);
				initMultiple("prdSortUpdate-multiple",prdSortArrUpdate,sortList);
			}
		})
	}

	
	function checkSingle(){
		var selected = $('#productTable').datagrid('getChecked');
		if(selected.length==1){
			return true;
		}else{
			return false;
		}

	}

	function checkSingles(){
		var selected = $('#productTable').datagrid('getChecked');
		if(selected.length>0){
			return true;
		}else{
			return false;
		}

	}

	function getCheckedIds(){
		var selected = $('#productTable').datagrid('getChecked');
		var pIds='';
		for(var i=0; i<selected.length; i++){
		    if (pIds != ''){ 
		    	pIds += ',';
		    }
		    pIds += selected[i].productId;
		}
		return pIds;
	}
	
	function getSingleCheckedId(){
		var selected = $('#productTable').datagrid('getChecked');
		var pId = selected[0].productId;
		return pId;
	}

	function importData() {
			$('#importForm').form('submit', {    
			    url:$.baseUrl+'/productController/importData.do',  
			    onSubmit: function(){    
			    },    
			    success:function(result){
			    	var result = $.parseJSON(result);
			    	$.messager.alert("提示",result.message);
			    	if(result.status == '200'){
			    		$('#daoruchanpin').find(".detailed_close").click();
			    		$("#productTable").datagrid('clearChecked');
			    		$("#productTable").datagrid('reload');
			    	}
				   }    
				});  
		}
	
	//下载模板
	function downTemp() {
		window.location=$.baseUrl+'/productController/downTemp.do';
	}
	
	return {
		init: init
	}
});