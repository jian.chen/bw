define(['common', 'view/base/industryManage','view/member/areaLinkage'], function(common, industryManage,areaLinkage) {
	function init() {
		common.Page.resetMain();
		var savetype;
		var edit4originalCouponTotal;
		var merchantIds;
		//初始化下拉框&输入框验证
		initSelect();
		initInput();
		
		$('#mendian_tree_btn').click(function(){
			$('#mendian_tree').show();
		});
		
		if(industryManage.getType() == 'show'){
			$("#sto_title").html('异业商户详情');
			$("#savea").hide();
			show();
			//表单禁用
			disableForm('MerchantForm',true);
			$("#mendian_tree_btn").hide();
		    }
		
		else if(industryManage.getType() == 'edit'){
			$("#sto_title").html('编辑异业商户信息');
			$("#MerchantId1").val(industryManage.getMerchantId());
			$("#savea").show();
			savetype = 'edit';
			show();
		}
		
		else{
			$("#sto_title").html('新建异业商户');
			$("#savea").show();
			savetype = 'save';
		}
		
		$("#savea").click(function(){
			var ck = formCheck($("#MerchantForm"));
			if(ck){
				if(savetype == 'edit'){
					editMerchant();
				}else{
					saveMerchant();
				}
			}else{
				return;
			}
		});	
	}
	
	function show(){
		$.ajax({
			url : $.baseUrl+'/couponController/getExternalCouponMerchant.do',
			type:"get",
			data:"externalMerchantId="+industryManage.getMerchantId(),
			datatype:"json",
			success:function(result){
				if(result){
					result = eval('(' + result + ')');
					$("#MerchantCode1").val(result.merchantCode);
					$("#MerchantName1").val(result.merchantName);
				}else{
					alert('出错了！');
				}
			}
		});
	}


	function saveMerchant(){
		$("#dataLoad").show();
			$.ajax({
				url:$.baseUrl+'/couponController/saveExternalCouponMerchants.do',
				type:"POST",
				data:serializeObject($('#MerchantForm')),
				success:function(result){
					$("#dataLoad").hide();
					result = $.parseJSON(result);
					if(result.status == '200'){
						$("#closeButton").click();
						$("#MerchantTable").datagrid('load');
					}
					alert(result.message);
				}
			
			});
	}

	function editMerchant(){
		$("#dataLoad").show();
			$.ajax({
				url:$.baseUrl+'/couponController/editExternalCouponMerchants.do',
				type:"POST",
				data:serializeObject($('#MerchantForm')),
				datatype:"json",
				success:function(result){
					$("#dataLoad").hide();
					result = $.parseJSON(result);
					if(result.status == '200'){
						$("#closeButton").click();
						$("#MerchantTable").datagrid('load');
					}
					alert(result.message);
				}
			});
		
	}
	
	
	return {
		init: init
	}
});



