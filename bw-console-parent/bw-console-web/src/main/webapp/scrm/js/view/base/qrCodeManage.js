define(['common'],function(common){
	var type;
	var qrCodeId;
	var qrCodeIds;
	var dlPath;
	
	function init(){
		common.Page.resetMain();
		initDataGrid();
		
		$('#showTemp').click(function(){
			var mm = check();
			
			if(mm == 1){
				type = 'show';
				isAuth('/base/qrCodeAdd.html',function(){
					common.Page.loadMask('base/qrCodeAdd.html');
				});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		
		$('#saveTemp').click(function() {
			type = 'save';
			isAuth('/qrCodeController/saveQrCode.do',function(){
				common.Page.loadMask('base/qrCodeAdd.html');
			});
		});
		
		$('#editTemp').click(function() {
			var mm = check();
			
			if(mm == 1){
				type = 'edit';
				isAuth('/qrCodeController/saveQrCode.do',function(){
					common.Page.loadMask('base/qrCodeAdd.html');
				});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});	
		
		
		//删除选中条码
		$('#deletedTemp').click(function() {
			var mm = check();
			
			if(mm != 2){
				if(confirm("确认删除？")){
					if(mm == 1){
						qrCodeIds = qrCodeId;
					}
					deleted();
				}
			}else{
				alert("请选择条目！");
			}	
		});
	};
	
	function initDataGrid(){
		$('#qrCodeTable').datagrid( {
			url : $.baseUrl+'/qrCodeController/showQrCode.json',
			
			idField : 'qrCodeId',
			onLoadSuccess : function(result) {
				if(result) {
					
				}
			},
			onLoadError : function() {
				
			}
		});
	}
	
	function deleted(){
		var url=$.baseUrl+"/qrCodeController/removeQrCode.do";
		isAuthForAjax('/qrCodeController/removeQrCode.do','POST',"",'JSON',{qrCodeIds:qrCodeIds},function(result){
			var data = eval('(' + result + ')');
			qrCodeIds="";
			qrCodeId="";
			alert(data.message);
			$("#qrCodeTable").datagrid('clearChecked');
			$("#qrCodeTable").datagrid('reload');
		},function(data){
			
		});
//		$.ajax({
//			type:"POST",
//			url:url,
//			dataType:"JSON",
//			contentType: "application/x-www-form-urlencoded; charset=utf-8", 
//			success:function(result){
//				var data = eval('(' + result + ')');
//				qrCodeIds="";
//				qrCodeId="";
//				alert(data.message);
//				$("#qrCodeTable").datagrid('clearChecked');
//				$("#qrCodeTable").datagrid('reload');
//			},
//			error:function(){
//				alert("request error");
//			}
//		});
	}
	
	function check(){
		var selected = $('#qrCodeTable').datagrid('getChecked');
		qrCodeIds = '';
		qrCodeId = '';
		
		if(selected.length==1){
			qrCodeId = selected[0].qrCodeId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			for(var i=0; i<selected.length; i++){
				if (qrCodeIds != ''){ 
					qrCodeIds += ',';
			    }
				qrCodeIds += selected[i].qrCodeId;
			}
			return 3;
		}

	}

	window.downLoadBtn = function downLoadBtn(value,row,index){
		return "<a href='javascript:doDownLoad(&apos;"+row.qrCodeId+"&apos;,&apos;"+row.qrTypeId+"&apos;);' target='_blank'>下载</a>";
	}

	window.doDownLoad = function doDownLoad(rowId,typeId){
		location.href=$.baseUrl+"/qrCodeController/downQrCodePic?qrCodeId="+rowId;
	}
	
	
	return{
		init: init,
		getType : function(){
			return type;
		},
		getQrCodeId : function(){
			return qrCodeId;
		},
		getQrCodeIds : function(){
			return qrCodeIds;
		}
	}
	
});


