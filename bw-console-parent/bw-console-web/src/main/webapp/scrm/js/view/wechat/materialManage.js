// 全局变量
var wechatTemplateId=null;
define(['common'], function(common) {
	window.checkImageText =function(id,i){
		// 将样式加上
		var strDiv="<div class='wechat_sucai_check'><i class='glyphicon glyphicon-ok'></i></div>";
		$("#index"+i).append(strDiv);
		
	}
	// 编辑图文消息
	window.editImageText =function(id){
		wechatTemplateId =id;
		common.Page.loadCenter('wechat/materialAdd.html');
	}
	
	// 删除图文消息
	window.deleteImageText =function(id){
		$.ajax({
	        type: "post",
	        url: $.baseUrl+'/wechatFansImageTextController/deleteImageText.do',
	        data:{id:id},
	        async: true,
	        success: function (data){
	        	var data = eval('(' + data + ')');
	        	if(data.status =='success'){
	        		alert("删除成功！");
	        		// 刷新
	        		$("#materialManage").click();
	        	}
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	                alert(errorThrown); 
	        } 
		});
	}
	
	function init() {
		common.Page.resetMain();
		$('#xinjiansucai').click(function(){
			common.Page.loadCenter('wechat/materialAdd.html');
		});
		initImageTextInfo();
	};
	
	return {
		init: init
	}
	
	/**
	 * 初始化加载图文列表
	 */
	function initImageTextInfo(){
		$.ajax({
	        type: "post",
	        url: $.baseUrl+'/wechatFansImageTextController/showImageTextList.do',
	        async: true,
	        success: function (data){
	        	initPackageIMageTextInfo(data);
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	                alert(errorThrown); 
	        } 
		});
	}
	
	// 初始化图文列表
	function initPackageIMageTextInfo(data){
		var data = eval('(' + data + ')');
    	var list =data.data;
    	var trasferData = JSON.stringify(list);
    	// list.length 是总的图文条数
    	$("#imageTextCount").html(list.length);
    	for(var i=0;i<list.length;i++){
    		var imageTextHtml="<li id='index"+i+"'><div class='wechat_caidan_right_icon'><a href='javascript:void(0)' onclick='editImageText("+list[i][0].wechatMessageTemplateId+")'><i class='table_icon icon_upd'></i></a>&nbsp;&nbsp;&nbsp;" +
    				"<a href='javascript:void(0)' onclick='deleteImageText("+list[i][0].wechatMessageTemplateId+")'><i class='table_icon icon_del'></i></a></div><div class='wechat_sucai_add'><div id='fengmian"+i+"' class='fengmian'>" +
    				"<div><img id='ImgPrFengmian"+i+"' style='width:50%;height: 50%'/></div><span>标题</span></div>" +
    				"<div id='itemList"+i+"'></div></div></li>";
    		
/*    		var imageTextHtml="<a href='javascript:void(0)' onclick='checkImageText("+list[i][0].wechatMessageTemplateId+","+i+")'>" +
    		"<li id='index"+i+"'><div class='wechat_caidan_right_icon'><i class='table_icon icon_upd'></i>&nbsp;&nbsp;&nbsp;" +
    		"<i class='table_icon icon_del'></i></div><div class='wechat_sucai_add'><div id='fengmian"+i+"' class='fengmian'>" +
    		"<div><img id='ImgPrFengmian"+i+"' style='width:50%;height: 50%'/></div><span>标题</span></div>" +
    		"<div id='itemList"+i+"'></div></div></li></a>";
*/    		
    		$("#wechat_sucai_ul").append(imageTextHtml);
    		for(var j=0;j<list[i].length;j++){
    			// 如果是单条文
    			if(j==0){
    				var str="#ImgPrFengmian"+i;
    				// 封面图片
    				$(str).attr("src",list[i][j].pictureUrl);
    				// 封面title
    				var strFengmian="#fengmian"+i;
    				$(strFengmian).children().eq(1).html(list[i][j].title);
    			}else{
    				var itemHtml="<div class='appmsgItem' ><span>"+list[i][j].title+"</span>" +
					"<div class='appmsgItem_img'><img style='width:100%;height:100%' src='"+list[i][j].pictureUrl+"'/></div></div>";
    				var strItem="#itemList"+i;
    				$(strItem).append(itemHtml);
    			}
    		}
    	}
	}
	
	
});