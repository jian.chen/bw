var complainIds;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initSelect();
		initDate();
		initInput();
		initDataGrid();
		
		$("#search_btn").click(function(){
			$("#complainTable").datagrid('load',serializeObject($("#complainSearchForm")));
		});
		$('#searchComplainDetail').click(function(){
			if(checkSingle()){
				complainIds = getCheckedId();
				common.Page.loadMask('complain/complainDetail.html');
			}
		});
		$("#closeComplainDetail").click(function(){
			if(checkSingle() && checkDel()){
				$('#dealComplainResult').show();
				var selected = $('#complainTable').datagrid('getSelected');
				var complainCode = selected.complainCode;
				$("#complainServiceId").text(complainCode);
			}
		});
		$("#save_complainService_btn").click(function(){
			$.ajax({
	            type:"POST",
	            url:$.baseUrl+'/complainController/closeComplain.do',
	            dataType:"JSON",
	            data : {
	            	complainId:$('#complainTable').datagrid('getSelected').complainId,
	            	remark: $("#complainContent").val()
	            },
	            success:function(result){
	            	if(result.code =='200'){
	            		$.messager.alert("提示","服务单关闭成功!");
	            		$('#dealComplainResult').hide();
	            		$("#complainContent").val('');
	            		$("#complainTable").datagrid('load',serializeObject($("#complainSearchForm")));
	            	}
	            }
	        });	
			
		});
	}
	
	return {
		init: init
	}
});
//检查是否单选
function checkSingle(){
	var selected = $('#complainTable').datagrid('getChecked');
	console.log(selected);
	if(selected.length==1){
		return true;
	}else{
		alert("请勾选一条记录!");
		return false;
	}

}

function checkDel(){
	var selected = $('#complainTable').datagrid('getChecked');
	if(selected[0].statusId == 7001){
		$.messager.alert("提示","该记录还未查看,不能直接关闭");
		return false;
	}else if(selected[0].statusId == 7003){
		$.messager.alert("提示","该记录已经被关闭,不能重复操作");
		return false;
	}else{
		return true;
	}
}
//获取选择Id
function getCheckedId(){
	var selected = $('#complainTable').datagrid('getChecked');
	var complainIds='';
	for(var i=0; i<selected.length; i++){
	    if (complainIds != ''){ 
	    	complainIds += ',';
	    }
	    complainIds += selected[i].complainId;
	}
//	alert(couponIds);
	return complainIds;
}
//初始化数据
function initDataGrid(){
	//初始化table
	$('#complainTable').datagrid( {
		url : $.baseUrl+'/complainController/showComplainList.do',
		idField : 'roleId',
		pageNumber : 1,
		pageSize : 10,
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}
//DateGrid Date时间格式化
function dateDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}
//DateGrid Time时间格式化
function timeDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + ' ' +val.substring(8, 10)+':'+val.substring(10,12)+':'+val.substring(12,14) ;
	}
}

//Date时间格式化
function dateDateformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}

function dateTimeformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + " " + val.substring(8, 10) + ":" + val.substring(10, 12) + ":" + val.substring(12, 14);
	}
}
//获取当前时间(yyyymmdd)
function getDate(){
	var date = new Date();
	var str = '';
	if(date.getMonth()+1<10){
		str = date.getFullYear()+"0"+(date.getMonth()+1);
		
	}else{
		str = date.getFullYear()+""+(date.getMonth()+1);
		
	}
	if(date.getDate()<10){
		str += "0"+date.getDate();
	}else{
		str += date.getDate();
	}
	return str;
}