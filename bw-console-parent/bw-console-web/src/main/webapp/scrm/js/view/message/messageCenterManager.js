var messageIds;
define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		$('#messageDetailBtn').click(function(){
			if(checkSingle()){
				messageIds = getCheckedId();
				common.Page.loadCenter('message/messageCenterDetail.html');
				
			}
			
		});
		
		$('#messageAddBtn').click(function(){
			common.Page.loadCenter('message/messageCenterAdd.html');
		});
		$('#messageDeleteBtn').click(function(){
			if(checkSingle()){
				messageIds = getCheckedId();
				if(checkStatus()){
					if(confirm("确定删除吗？")){
						deleteMessage();
					}
				}else{
					alert("不可删除已发送消息！");
				}
			}
			
		});
		$('#messageReturnBtn').click(function(){
			if(checkSingle()){
				messageIds = getCheckedId();
				if(!checkStatus()){
					if(confirm("确定撤回吗？")){
						recallMessage();
					}
					
				}else{
					alert("该消息未发送！")
				}
			}
			
		});
		$('#messageEditBtn').click(function(){
			if(checkSingle()){
				messageIds = getCheckedId();
				if(checkStatus()){
					common.Page.loadCenter('message/messageCenterEdit.html');
				}else{
					alert("不可编辑已发送消息！")
				}
				
			}
		});
		$('#messageSendBtn').click(function(){
			if(checkSingle()){
				messageIds = getCheckedId();
				if(checkStatus()){
					common.Page.loadMask('message/messsageIssue.html');
				}else{
					alert("该消息已发送过，不可再次发送!")
				}
			}
		});
		
		initMessageDataGrid();
		
		function deleteMessage(){
		    $.ajax({
				type: "POST",
				url : $.baseUrl+'/memberMessageController/deleteMessage.do',
				data: "id="+messageIds,
				dataType : "json",
				success : function(result) {
					if(result.status==200){
						alert(result.message);
						common.Page.loadCenter('message/messageCenterManager.html');
						
					}else{
						alert(result.message);
					}
				},
				error : function() {
					parent.layer.alert("出错了:(");
				}
			});; 
		}
		
		function recallMessage(){
		    $.ajax({
				type: "POST",
				url : $.baseUrl+'/memberMessageController/recallMessage.do',
				data: "messageId="+messageIds,
				dataType : "json",
				success : function(result) {
					if(result.status==200){
						alert(result.message);
						common.Page.loadCenter('message/messageCenterManager.html');
						
					}else{
						alert(result.message);
					}
				},
				error : function() {
					parent.layer.alert("出错了:(");
				}
			});; 
		}
	}

	return {
		init: init
	}

});

function checkSingle(){
	var selected = $('#memberMessageTable').datagrid('getChecked');
	if(selected.length==1){
		return true;
	}else{
		alert("请勾选一条记录!");
		return false;
	}

}
//获取选择Id
function getCheckedId(){
	var selected = $('#memberMessageTable').datagrid('getChecked');
	var ids='';
	for(var i=0; i<selected.length; i++){
	    if (ids != ''){ 
	    	ids += ',';
	    }
	    ids += selected[i].memberMessageId;
//	    edit4originalCouponTotal = selected[i].issueProportion.split("/")[0];
	}
	return ids;
}
	
function initMessageDataGrid(){
	//初始化table
	$('#memberMessageTable').datagrid( {
		url : $.baseUrl+'/memberMessageController/showList.do',
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

window.rowformater_img = function rowformater_img(value){
	if(value == null || value == ''){
		return "<img src='../img/memberCard.png' style='width: 100%;height: 133px;' />";
	}
	return "<img src='"+value+"' style='width: 100%;height: 133px;' />";
}
//DateGrid Time时间格式化
window.rowformater_time = function timeDateformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + ' ' +val.substring(8, 10)+':'+val.substring(10,12)+':'+val.substring(12,14) ;
	}
}

//清空图片
function clearImg(page){
	$("#"+page+"_image").val("");
	$("#"+page+"_img3").val("");
	$("#"+page+"_img1_path").attr("src","");
	$("#"+page+"_fileSpan").text("未选择文件...");
}
//上传大图片
function uploadmessageimage(page){
//	var loading = '';
//	loading = parent.layer.load('正在导入，请稍侯...');
	$("#dataLoad").show();
	$('#'+page+'_messageForm').form('submit', {    
	    url : $.baseUrl+'/memberMessageController/uploadMessageImage.do',   //加loading 
	    success:function(result){
	    	$("#dataLoad").hide();
//	    	parent.layer.close(loadi);
	    	result = eval('('+result+')');
	    	if(result=="不被允许上传的文件格式!"){
	    		alert("不被允许上传的文件格式!");
	    	}else{
	    		$("#"+page+"_img1").val($.rootPath+result);
	    		$("#"+page+"_img1_path").attr("src",$.rootPath+result);
	    		$.messager.alert("提示","上传成功");
	    	}
	    }    
	}); 
}

function checkStatus(){
	var selected = $('#memberMessageTable').datagrid('getChecked');
	var messageStatus = '';
	messageStatus = selected[0].statusId;
	if(messageStatus=='8003'){
		return false;
	}else{
		return true;
	}
}
