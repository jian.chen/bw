define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		$("#couponUseSearch").click(function(){
			if($("#couponUse_instanceCode").val() == ''){
				$.messager.alert("提示","请输入券码!");
				return;
			}
			$('#couponUseTable').datagrid( {
				url : $.baseUrl+'/couponController/queryCouponInstanceForUse',
				queryParams:{
					couponInstanceCode : $("#couponUse_instanceCode").val()
				}
			});
		})
		
		$("#verificationCoupon").click(function(){
			var instanceCode = $("#couponUse_instanceCode").val();
			var action = '1603';
			if(instanceCode == ""){
				$.messager.alert("提示","请输入券码！");
				return;
			}
			$.messager.confirm("确认","您确认要核销优惠券:"+instanceCode+"吗?",function(){
				isAuthForAjax('/couponIssueController/editCouponInstance.do',"post","","json","instanceCode="+instanceCode+"&action="+action,function(data) {
					if(data.status != null){
						$.messager.alert("提示",data.result);
						$('#couponUseTable').datagrid('reload');
					}else{
						$.messager.alert("提示","券码错误,请重新输入");
					}
				}, function() {
					parent.layer.alert("出错了:(");
				});
			})
		})
		
	}
	
	return {
		init: init
	}
});
