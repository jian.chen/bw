var downdata;
var gradedata;
var enableBoolean;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initInput();
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		initDataGrid();
	}
	//初始化数据
	function initDataGrid(){
		//初始化table
		$('#gradeTable').datagrid( {
			url : $.baseUrl+'/gradeController/showGrade.do',
			queryParams : {
				gradeConfId : 1
			},
			onLoadSuccess : function(result) {
				gradedata = result;
				$("#gradeManage_changeTypeId").val(result.obj.changeType);
				//判断是否启用
				enableChange(result.obj.isActive);
				//判断变动方式
				if(result.obj.changeType==1) {
					$("#gradeManage_changeTypeName").val("自动等级变化");
                }else if(result.obj.changeType==2){
                	$("#gradeManage_changeTypeName").val("手动等级变化");
                	$("#auto").hide();
					$("#manual").show();
                }
				//判断影响因素
				if(result.obj.gradeFactorsId==1){
					$("input[name='gradeFactorsId']:eq(0)").attr("checked",'checked');
					
				}else{
					$("input[name='gradeFactorsId']:eq(1)").attr("checked",'checked');
					
				}
				if(!result.obj.upPeriod){
					$("#gradeManage_permanent_accumulation").attr('checked',true)
					$("#gradeManage_cycle").attr('readonly',true);
					$("#gradeManage_cycle").attr("data-options","length:'3',validType:'rtNZero'");
				}
				$("#gradeManage_cycle").val(result.obj.upPeriod);
				initgradeSelect();
				initGradeData(gradedata);
			},
			onLoadError : function() {
				
			}
		});
		$("#gradeManage_permanent_accumulation").click(function(){
			if(this.checked==true){
				$("#gradeManage_cycle").val('');
				$("#gradeManage_cycle").attr('readonly',true);
				$("#gradeManage_cycle").attr("data-options","length:'3',validType:'rtNZero'");
			}else{
				$("#gradeManage_cycle").attr('readonly',false);
				$("#gradeManage_cycle").attr("data-options","required:true,length:'3',validType:'rtNZero'");
			}
		});
	}
	//初始化table2
	function initGradeData(gradedata){
		$('#gradeTable2').datagrid( {
			data : gradedata.rows,
			onLoadSuccess : function(result) {
				if(gradedata.obj.changeType==1){
					$("#auto").show();
					$("#manual").hide();
				}
				
			}
		});
	}
	
	$('#gradeAdjustIcon').tooltip({
		position: 'right',
		content: '<span style="color:#456BF0">注:<br/>1.每次积分累计值变动时判断是否升级，不降级<br/>2.每次结算日判断前一周期内累计值进行降级</span>',
		onShow: function(){
			$(this).tooltip('tip').css({
				backgroundColor: '#B4B4B4',
				borderColor: '#666'
			});
		}
	});
	
	return {
		init: init
	}
});
//保存等级配置信息
function saveGrade(){
	if(enableBoolean){
		alert("请停用后保存!");
		return;
	}
	if(formCheck($("#gradeForm"))){
		var arr;
		var gradeFactorsId;
		var changeType = $("#gradeManage_changeTypeId").val();
		gradeFactorsId = $("input[name='gradeFactorsId']:checked").val();
		if($("#gradeManage_changeTypeId").val()==1){
			endEditing();
			arr=$('#gradeTable').datagrid('getChanges');
			var upPeriod = $("#gradeManage_cycle").val();
		}else if($("#gradeManage_changeTypeId").val()==2){
			endEditing1();
			arr=$('#gradeTable2').datagrid('getChanges');
		}
		
		isAuthForAjax('/gradeController/saveGrade.do',"POST","","json",{
			gradelist:JSON.stringify(arr),
			changeType:changeType,
			gradeFactorsId:gradeFactorsId,
			upPeriod:upPeriod,
		},function(result){
			result = eval('('+result+')');
			if(result.code==1){
				alert(result.message);
			}else if(result.code==0){
				alert(result.message);
			}
			getGradeList();//更新contants里的等级菜单
		},function(data){
			parent.layer.alert("出错了:(");
		});
	}
}
//启用、停用等级
function enableGrade(active){
	isAuthForAjax('/gradeController/enableGrade.do',"get","","json","gradeConfId=1&isActive="+active,function(result){
		enableChange(active);
		result = eval('('+result+')');
	},function(data){
		parent.layer.alert("出错了:(");
	});
}

//升级下拉框函数
function selectId(){
	var upid = $("#gradeManage_upId").val();
	if(upid==1){
		downdata="$.Grade_Down_Rule1";
		$("#gradeManage_up_month_day").hide();
	}else if(upid==2){
		downdata="$.Grade_Down_Rule2";
		$("#gradeManage_up_month_day").show();
	}
	if($("#gradeManage_downId").val()!=upid){
		$("#gradeManage_downId").val("3");
		$("#gradeManage_downName").val("不降级");
		$("#gradeManage_down_month_day").hide();
		$('#gradeTable').datagrid('hideColumn','retainValue');
	}
	updategradeSelect(downdata);
}
//变动方式下拉框函数
function changeType(){
	var changeTypeId = $("#gradeManage_changeTypeId").val();
	if(changeTypeId==1){
		$("#auto").show();
		$("#manual").hide();
		if($("#gradeManage_permanent_accumulation").checked==true){
			$("#gradeManage_cycle").attr("data-options","length:'3',validType:'rtNZero'");
		}else{
			$("#gradeManage_cycle").attr("data-options","required:true,length:'3',validType:'rtNZero'");
		}
	}else if(changeTypeId==2){
		$("#auto").hide();
		$("#manual").show();
		$("#gradeManage_cycle").attr("data-options","length:'3',validType:'rtNZero'");
	}
}
//降级下拉框函数
function showRetainValue(){
	var downTypeId = $("#gradeManage_downId").val();
	if(downTypeId==2){
		$('#gradeTable').datagrid('showColumn','retainValue');
		$("#gradeManage_down_month_day").show();
	}else{
		$('#gradeTable').datagrid('hideColumn','retainValue');
		$("#gradeManage_down_month_day").hide();
	}
}

//根基升级方式修改降级下拉框数据
function updategradeSelect(downdata){
	$('.dropdown-menu').each(function(i,v){
	var _pp = $("#gradeManage_downType");
	var s=$.trim($(this).attr("data"));
	if(s=="$.Grade_Up_Rule"){
		_pp.find("li").remove();
		eval("var data = "+downdata);
		$.each(data,function(i,v){
			_pp.append("<li data-id='"+v.value+"'><a href='javascript:showRetainValue();'>"+v.text+"</a></li>");
		});
	}
});
}

//下拉框初始化
function initgradeSelect(){
	$('.dropdown-menu').each(function(i,v){
	var _pp = $(this);
	var s=$.trim($(this).attr("data"));
	var url_options = $.trim($(this).attr("url-options"));
	var addfun = $.trim($(this).attr("addfun"));
	if(s){
		if(s=="$.Grade_Up_Rule"){
			eval("var data = "+s);
			$.each(data,function(i,v){
				_pp.append("<li data-id='"+v.value+"'><a href='javascript:selectId();'>"+v.text+"</a></li>");
			});
			
		}else if(s=="$.Grade_Change_Type"){
			eval("var data = "+s);
			$.each(data,function(i,v){
				_pp.append("<li data-id='"+v.value+"'><a href='javascript:changeType();'>"+v.text+"</a></li>");
			});
		}else if(addfun){
			eval("var data = "+s);
			$.each(data,function(i,v){
				_pp.append("<li data-id='"+v.value+"'><a href='javascript:showRetainValue();'>"+v.text+"</a></li>");
			});
		}else{
			eval("var data = "+s);
			$.each(data,function(i,v){
				_pp.append("<li data-id='"+v.value+"'><a href='javascript:;'>"+v.text+"</a></li>");
			});
			
		}
	}else if(url_options){
		eval("var urlData = {"+url_options+"}");
		var url = urlData.url;
		var value = urlData.value;
		var text = urlData.text;
		$.ajax({ 
	        type: "get", 
	        url: encodeURI($.baseUrl+url), 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$.each(data, function(i, v){
	        		eval("var aa = v."+value+"");
	        		eval("var bb = v."+text+"");
	        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
			    });
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        }
	    });
	}
});
}

//编辑表格
$.extend($.fn.datagrid.methods, {
	editCell: function(jq,param){
		return jq.each(function(){
			var opts = $(this).datagrid('options');
			var fields = $(this).datagrid('getColumnFields',true).concat($(this).datagrid('getColumnFields'));
			for(var i=0; i<fields.length; i++){
				var col = $(this).datagrid('getColumnOption', fields[i]);
				col.editor1 = col.editor;
				if (fields[i] != param.field){
					col.editor = null;
				}
			}
			$(this).datagrid('beginEdit', param.index);
			for(var i=0; i<fields.length; i++){
				var col = $(this).datagrid('getColumnOption', fields[i]);
				col.editor = col.editor1;
			}
		});
	}
});

var editIndex = undefined;
//table编辑结束
function endEditing(){
	if (editIndex == undefined){return true}
	if ($('#gradeTable').datagrid('validateRow', editIndex)){
		$('#gradeTable').datagrid('endEdit', editIndex);
		editIndex = undefined;
		return true;
	} else {
		return false;
	}
}
//table点击函数
function onClickCell(index, field){
	if (endEditing()){
		$('#gradeTable').datagrid('selectRow', index)
				.datagrid('editCell', {index:index,field:field});
		editIndex = index;
	}
}
//table2编辑结束函数
function endEditing1(){
	if (editIndex == undefined){return true}
	if ($('#gradeTable2').datagrid('validateRow', editIndex)){
		$('#gradeTable2').datagrid('endEdit', editIndex);
		editIndex = undefined;
		return true;
	} else {
		return false;
	}
}
//table2点击函数
function onClickCell1(index, field){
	if (endEditing1()){
		$('#gradeTable2').datagrid('selectRow', index)
		.datagrid('editCell', {index:index,field:field});
		editIndex = index;
	}
}

function enableChange(isActive){
	if(isActive=='Y'){
		$("#gradeManage_enable").attr("class","btn btn-block");
		$("#gradeManage_disable").attr("class","btn btn-success btn-block");
		$("#gradeManage_enable").attr("onclick","");
		$("#gradeManage_disable").attr("onclick","javascript:enableGrade('N')");
		$("#gradeManage_enable").text("已启用");
		$("#gradeManage_disable").text("停用");
		enableBoolean = true;
	}else if(isActive=='N'){
		$("#gradeManage_enable").attr("class","btn btn-success btn-block");
		$("#gradeManage_disable").attr("class","btn btn-block");
		$("#gradeManage_disable").attr("onclick","");
		$("#gradeManage_enable").attr("onclick","javascript:enableGrade('Y')");
		$("#gradeManage_enable").text("启用");
		$("#gradeManage_disable").text("已停用");
		enableBoolean = false;
	}
}