define(['common','activityManage'], function(common,activityManage) {
	var activityInstanceId = null;
	//初始化table
	function init(){
		
		initLoad();
		
		$("#seachRecord").click(function(){
			$('#activity-instance-record-table').datagrid('load', serializeObject($('#recordForm')));
		});
		
		//返回
		$("#backBtn-form-activityInstanceManage").click(function () {
			if(activityManage.typeId=='0'){
				 common.Page.loadCenter('activity/activityManage.html');
			 }else{
				 common.Page.loadCenter('activity/consumeActivity.html');
			 }
		});
		
	}
	
	function initLoad(){
		$('#activity-instance-table').datagrid( {
			url : $.baseUrl+'/activityController/showActivityInstanceList.json',
			method:'post',
			queryParams:{'activityId': activityManage.getActivityId()},
			onLoadSuccess : function(result) {
				if(result) {
                }
			},
			onLoadError : function() {
			}
		});
	}
	
	window.activity_formatProgress = function (value,row,index){
		
		var s = '<button onclick="report('+value+')" type="button" class="solid-btn">报告</button>';
		
		s = s + '<button onclick="settingNoSave('+value+')" type="button" class="border-btn">查看</button>';
		
		//中止活动(只有已创建和进行中的活动可以中止&设置)
		if(row.statusId == '2102' || row.statusId == '2101'){
			s = s + '<button onclick="setting('+value+')" type="button" class="border-btn">设置</button>';
	    	s = s + '<button  type="button" onclick="activity_stop('+value+')" class="solid-btn">终止 </button>';
	    }else if(row.statusId == '2103' || row.statusId == '2104'){
	    	s = s + '<button  type="button" style="background: #cdcdcd;" class="solid-btn">设置</button>';
	    	s = s + '<button  type="button" style="background: #cdcdcd;" class="solid-btn">终止 </button>';
	    }
	    
		return s;
	};
	
	window.setting = function setting(instanceId){
		activityInstanceId = instanceId;
		saveType = 'edit';
		if(activityManage.typeId=='0'){
			common.Page.loadCenter('activity/activitySetting.html');
		}else{
			common.Page.loadCenter('activity/activitySettingDetail.html');
		}
	};
	
	window.settingNoSave = function setting(instanceId){
		activityInstanceId = instanceId;
		saveType = 'show';
//		common.Page.loadCenter('activity/activitySetting.html');
		if(activityManage.typeId=='0'){
			common.Page.loadCenter('activity/activitySetting.html');
		}else{
			common.Page.loadCenter('activity/activitySettingDetail.html');
		}
	};
	
	window.report = function report(instanceId){
		activityInstanceId = instanceId;
		common.Page.loadCenter('activity/activityInstanceReport.html');
	};
	
	//结束时间
	window.thruDate_setting = function(value, row, index){
		var s;
		if(row.validityType == 'forever'){
			s = '永久有效';
		}else{
			s = getDateString(value);
		}
		return s;
	}
	
	//终止活动
	window.activity_stop = function(instanceId){
		$.messager.confirm('确认','您确认想要终止此活动吗？终止后将不能再启用',function(r){    
		    if (r){    
		    	$.ajax({
					url:$.baseUrl+'/activityController/updateInstanceStatus.json',
					type:"POST",
					data:"activityInstanceId="+instanceId,
					datatype:"json",
					success:function(result){
						result = eval('(' + result + ')');
						if(result==1){
							alert('活动已中止！');
							$("#activity-instance-table").datagrid('load');
						}else{
							alert('中止失败，请重试！');
						}
					}
					
				});
		    }
		});
	};
	
	return{
		init:init,
		getActivityInstanceId : function(){
			return activityInstanceId;
		},
		getIsEdit : function(){
			return isEdit;
		}
	};
});

