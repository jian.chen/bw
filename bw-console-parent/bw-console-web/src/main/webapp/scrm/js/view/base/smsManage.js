var type;
var smsTempId;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		//查看
		$('#showTemp').click(function(){
			if(check()==1){
				type = 'show';
				common.Page.loadCenter('base/smsEdit.html');
			}else if(check()==2){
				$.messager.alert("提示","请选择条目！");
			}
		});
		
		//编辑
		$('#editTemp').click(function(){
			if(check()==1){
				type = 'edit';
				common.Page.loadCenter('base/smsEdit.html');
			}else if(check()==2){
				$.messager.alert("提示","请选择条目！");
			}
		});
		
		//新建
		$('#addTemp').click(function(){
			type = 'save';
			common.Page.loadCenter('base/smsEdit.html');
		})
		
		//查看已发送短信
		$('#showSmsInstance').click(function() {
			if(check()==1){
				isAuth('/smsTempController/showSmsInstanceByExampleList.do',function(){
					common.Page.loadCenter('base/smsInstance.html');
				});
			}else if(check()==2){
				$.messager.alert("提示","请选择条目！");
			}
		});
		
		//启用
		$("#startTemp").click(function(){
			if(check()==1){
				$.ajax({
					url : $.baseUrl+'/smsTempController/updateSmsTempStatus.do',
					type : "post",
					dataType : "json",
					data :{
						smsTempId : smsTempId,
						status : 'Y'
					},
					success : function(result){
						if(result.status = '200'){
							$.messager.alert("提示",result.message);
							$('#smsTempTable').datagrid('reload');
						}else{
							$.messager.alert("提示",result.message);
						}
					}
				})
			}else if(check()==2){
				$.messager.alert("提示","请选择条目！");
			}
		})
		
		//停用
		$("#stopTemp").click(function(){
			if(check()==1){
				$.ajax({
					url : $.baseUrl+'/smsTempController/updateSmsTempStatus.do',
					type : "post",
					dataType : "json",
					data :{
						smsTempId : smsTempId,
						status : 'N'
					},
					success : function(result){
						if(result.status = '200'){
							$.messager.alert("提示",result.message);
							$('#smsTempTable').datagrid('reload');
						}else{
							$.messager.alert("提示",result.message);
						}
					}
				})
			}else if(check()==2){
				$.messager.alert("提示","请选择条目！");
			}
		})
		
	}
	
	function initDataGrid(){
		$('#smsTempTable').datagrid( {
			url : $.baseUrl+'/smsTempController/showSmsList.do',
			idField : 'smsTempId',
				onLoadSuccess : function(result) {
				},
				onLoadError : function() {
					
				}
			});
	}
	
	function check(){
		var selected = $('#smsTempTable').datagrid('getChecked');
		if(selected.length==1){
			smsTempId = selected[0].smsTempId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}
	}
	
	return {
		init: init,
		getSmsTempId : function(){
			return smsTempId;
		},
		getType : function(){
			return type;
		}
	}
});