define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		isAuthManage("/goods/couCateItemManage.html", "goods/couCateItemManage.html");
		$('#xitongguanli_tab li').click(function(){
			var url = $(this).data('url');
			var authUrl = $(this).attr('auth-url');
			isAuthManage(authUrl, url, $(this));
		});
	};


	function isAuthManage(url, goToUrl, dom){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/sysRoleController/isAuth",
			async : false,
			dataType: "json",
			data : {
				url : url
			},
			success : function(data){
				if(data.flag == false){
					$.messager.alert("访问受限","抱歉您没有此操作权限");
					return;
				}
				if(data.flag == true){
					if(dom != 'undefined') {
						$(dom).addClass('active').siblings().removeClass('active');
					}
					$('#xitongguanliPanel').show().panel({
						href: goToUrl
					});
				}
			}
		})
	}
	return {
		init: init
	}
});