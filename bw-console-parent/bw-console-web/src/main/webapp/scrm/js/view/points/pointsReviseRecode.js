define(['common'], function(common) {
	function init() {
		common.Page.resetMain();

		initPointsReviseDataGrid();												
	    
		$('#seachTask').click(function() {
			$('#pointsReviseTable').datagrid('load', serializeObject($('#pointsReviseForm')));
		});
		
	};
	return {
		init: init
	}
});

function initPointsReviseDataGrid(){
	//初始化table
	$('#pointsReviseTable').datagrid( {
		url : $.baseUrl+'/pointsItemController/showPointsReviseList.do',
		queryParams : serializeObject($('#pointsReviseForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}