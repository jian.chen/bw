define(['common','citySelect'], function(common,citySelect) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelectDetailed();
		initDate();
		initInput();
		memberIdAdd='';
		addTagIdArrAdd = [];
		addTagNameArrAdd = [];
		//使用区域选择
		citySelect.inputsCitys['city_qr_member'] = [];
		citySelect.inputsNames['city_qr_member'] = [];
		new citySelect.initCity({input:'city_qr_member',inputType:true});
		//绑定切换
		$('#userDetailedTabBtnList').bindTab('#userDetailedTabContent');
		
		$("#languageSelect").change(function(){
			$("#language").val($(this).children('option:selected').val());
		})
		
		//标签切换查询
		/*
		 $("#userDetailedTabBtnList li").each(function() {
             $(this).click(function() {
                 var title = $(this).text();
                 if(title.trim()=='基本信息'){
                 	
                 }else if(title.trim()=='联系信息'){
                 	if(memberId==''){
                 		alert('请先录入基本信息');
                 	}
                 }else if(title.trim()=='标签分组'){
                 	if(memberId==''){
                 		alert('请先录入基本信息');
                 	}
                 }else if(title.trim()=='其他信息'){
                 	if(memberId==''){
                 		alert('请先录入基本信息');
                 	}
                 }
　　　　　　		});
		});
*/
		
		$('#interactionTabList').bindTab('#interactionTabContent');

		//标签弹出层
		$('#labelChooseBtn2').click(function() {
			$('#labelChooseAdd').show();
			appendTagAdd();
			queryTagAdd($('#likeMemberTagName2').val());
		});
		$('#queryTag_bt2').click(function() {
			queryTagAdd($('#likeMemberTagName2').val());
		});
		$('#tagSaveBtn2').click(function() {
			//关闭弹出层
			$('#labelChooseAdd').find(".detailed_close").click();
			addSelectTagAdd();
		});

		//分组弹出层
		$('#addFenZuBtn').click(function() {
			$('#addFenZu').show();
			queryGroup();
		});
		//分组弹出层保存
		$('#groupSaveBtn').click(function() {
			//关闭弹出层
			$('#addFenZu').find(".detailed_close2").click();
			addSelectGroupToArray();
			addSelectGroup();
		});
		
		//添加联系地址弹出层
		$('#contactDetailedBtn').click(function() {
			$('#contactDetailed').show();
		});
		
		
		//基本信息保存
		$('#memberBaseSaveBtn').click(function() {
			/*
			if(!citySelect.inputsCitys['city_qr_member']){
				alert("请选择正确的城市");
				return;
			}
			*/
			
			if($("#channelName").val()==""){
				$("#channelName").alert("不能为空");
				return;
			}
			if($("[name='mobile']").val()==""){
				$("[name='mobile']").alert("不能为空");
				return;
			}
			$("[name='geoId']").val(citySelect.inputsCitys['city_qr_member']);
			saveMemberBaseInfoAdd();
		});
		
		//联系信息保存
		$('#saveMemberExtAccountBtn').click(function() {
			if(memberIdAdd ==''){
				alert('请先录入基本信息');
				return;
			}
			saveMemberExtAccountAdd();
		});
  		
		//保存会员的标签和分组
		$('#saveMemberTagAndGroupBtn').click(function() {
			if(memberIdAdd ==''){
				alert('请先录入基本信息');
				return;
			}
			saveMemberTagAndGroupAdd();
		});
		
		//保存会员的其他信息
		$('#memberExtInfoSaveBtn').click(function() {
			if(memberIdAdd ==''){
				alert('请先录入基本信息');
				return;
			}
			saveMemberExtInfoAdd();
		});
		
		//添加联系地址弹出层
		$('#memberAddressAddBtn').click(function() {
			if(memberIdAdd ==''){
				alert('请先录入基本信息');
				return;
			}
			$('#contactDetailed').show();
			initAddressData("provinceName","20001");
			//表单清空
			clearForm($('#memberAddressForm'));
		});
		//编辑地址
		$('#memberAddressEditBtn').click(function() {
			if(memberIdAdd ==''){
				alert('请先录入基本信息');
				return;
			}
			var selected  = $('#memberAddressTable').datagrid('getSelected');
			var memberAddressId;
			if(selected){
				memberAddressId = selected.memberAddressId;
			}else{
				alert("请选择地址");
				return;
			}
			$('#contactDetailed').show();
			initAddressData("provinceName","20001");
			//查询详情页面赋值
			queryMemberAddressDetailAdd(memberAddressId);
		});
		//删除地址
		$('#memberAddressDeleteBtn').click(function() {
			if(memberIdAdd ==''){
				alert('请先录入基本信息');
				return;
			}
			var selected  = $('#memberAddressTable').datagrid('getSelected');
			var memberAddressId;
			if(selected){
				memberAddressId = selected.memberAddressId;
			}else{
				alert("请选择地址");
				return;
			}
			//查询详情页面赋值
			deleteMemberAddressAdd(memberAddressId);
		});
		
		
		//保存会员联系地址
		$('#memberAddressSaveBtn').click(function() {
			saveMemberAddressAdd();
		});
		
		//默认地址勾选框勾选赋值
		$('#isDefaultCheckbox').click(function() {
			if($(this).prop("checked")){
				$("[name='isDefault']").val("1");
			}else{
				$("[name='isDefault']").val("0");
			}
		});
		
		//区域门店弹框
		$('#orgName').click(function() {
			$('#areaChooseAdd').show();
			initTreeData("areaChooseAdd","areaTreeAdd","/orgController/testTree.do","2");
			selectOrg($("[name='orgId']").val(),"areaTreeAdd");
		});
		//区域门店保存
		$('#areaSaveBtnAdd').click(function() {
			var selectIds= getSelectId("areaTreeAdd","2");
			var selectNames= getSelectName("areaTreeAdd","2");
			$("[name='orgId']").val(selectIds);
			$("[name='orgName']").val(selectNames);
			$('#areaChooseAdd').find(".detailed_close2").click();
		});
		
	}
	
	return {
		init: init
	}
});

var memberIdAdd='';
//保存会员基本信息
function saveMemberBaseInfoAdd(){
	//if($("[name='memberCode']").val() == ''){
		$('#memberBaseInfoForm').form('submit', {    
		    url:$.baseUrl+'/memberController/saveMember.do',
		    onSubmit: function(){
		    	var isValid = formCheck($(this));
		    	if(isValid){
		    		$("#dataLoad").show();
		    	}
				return isValid;
		    },    
		    success:function(data){  
		    	$("#dataLoad").hide();
		    	var result = $.parseJSON(data);
		    	$('#huiyuandingdanTable').datagrid('reload');
		    	alert(result.message);
		    	if(result.status==200){
		    		$("[name='memberCode']").val(result.obj.memberCode);
		    		$("[name='memberId']").val(result.obj.memberId);
		    		memberIdAdd = result.obj.memberId;
		    	}
		    }    
		}); 
	//}
}

//保存会员联系信息
function saveMemberExtAccountAdd(){
		$('#memberExtAccountForm').form('submit', {    
		    url:$.baseUrl+'/memberController/saveMemberExtAccount.do?memberId='+memberIdAdd,
		    method : "post",
		    onSubmit: function(){
		    	var isValid = formCheck($(this));
		    	if(isValid){
		    		$("#dataLoad").show();
		    	}
				return isValid;
		    },    
		    success:function(data){  
		    	$("#dataLoad").hide();
		    	var result = $.parseJSON(data);
		    	$('#huiyuandingdanTable').datagrid('reload');
		    	alert(result.message);
		    }    
		}); 
}

//保存会员标签和分组
function saveMemberTagAndGroupAdd(){
	$("#dataLoad").show();
		 $.ajax({ 
	        type: "post", 
	        url: $.baseUrl+'/memberController/saveMemberTagAndGroup.do?memberId='+memberIdAdd, 
	        dataType: "json",
	        data : {
	        	memberTagIds: addTagIdArrAdd.toString(), 
	        	memberGroupIds: addGroupIdArr.toString()
	        },
	        async: false,
	        success: function (data) {
	        	$("#dataLoad").hide();
			    //var result = $.parseJSON(data);
	    		alert(data.message);
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        }
	    });
}

//保存会员其他信息
function saveMemberExtInfoAdd(){
		$('#saveMemberExtInfoForm').form('submit', {    
		    url:$.baseUrl+'/memberController/saveMemberExtInfo.do?memberId='+memberIdAdd,
		    method : "post",
		    onSubmit: function(){
		    	var isValid = formCheck($(this));
		    	if(isValid){
		    		$("#dataLoad").show();
		    	}
				return isValid;
		    },    
		    success:function(data){  
		    	$("#dataLoad").hide();
		    	var result = $.parseJSON(data);
		    	alert(result.message);
		    }    
		}); 
}


//保存会员联系地址
function saveMemberAddressAdd(){
	if($("#provinceName").val()==""){
		$("#provinceName").alert("不能为空");
		return;
	}
	if($("#cityName").val()==""){
		$("#cityName").alert("不能为空");
		return;
	}
	if($("#districtName").val()==""){
		$("#districtName").alert("不能为空");
		return;
	}
	$('#memberAddressForm').form('submit', {    
    url:$.baseUrl+'/memberController/saveMemberAddress.do?memberId='+memberIdAdd,
    method : "post",
    onSubmit: function(){
    	var isValid = formCheck($(this));
    	if(isValid){
    		$("#dataLoad").show();
    	}
		return isValid;
    },    
    success:function(data){  
    	$("#dataLoad").hide();
    	var result = $.parseJSON(data);
    	if(result.status=='200'){
    		$('#contactDetailed').find(".detailed_close2").click();
    		$('#memberAddressTable').datagrid( {
				url: $.baseUrl+'/memberController/showMemberContactInfo.do?memberId='+memberIdAdd, 
				singleSelect : true,
				onLoadSuccess : function(result) {
				},
				onLoadError : function() {
					
				}
			});
    		//$('#memberAddressTable').datagrid('load',{});
    	}else{
    		alert(result.message);
    	}
    }    
}); 
}

//查询地址详情
function queryMemberAddressDetailAdd(memberAddressId){
	
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/showMemberAddressDetail.do?memberAddressId='+memberAddressId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("[name='memberAddressId']").val(data.memberAddressId);
        	$("[name='personName']").val(data.personName);
        	$("[name='provinceId']").val(data.provinceId);
        	$("#provinceName").val(data.provinceName);
        	 $("[name='cityId']").val(data.cityId);
        	$("#cityName").val(data.cityName);
        	$("[name='districtId']").val(data.districtId);
        	$("#districtName").val(data.districtName);
        	$("[name='addressDetail']").val(data.addressDetail);
        	$("[name='personMobile']").val(data.personMobile);
        	$("[name='personPhoneZone']").val(data.personPhoneZone);
        	$("[name='personPhoneNum']").val(data.personPhoneNum);
        	$("[name='isDefault']").val(data.isDefault);
        	
        	if(data.isDefault==1){
        		$("#isDefaultCheckbox").prop("checked",true); 
        	}else{
        		$("#isDefaultCheckbox").prop("checked",false); 
        	}
        	
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

function deleteMemberAddressAdd(memberAddressId){
	$("#dataLoad").show();
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/removeMemberAddress.do?memberAddressId='+memberAddressId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("#dataLoad").hide();
	        if(data.status=='200'){
	    		$('#memberAddressTable').datagrid('load', {});
	    	}else{
	    		alert(data.message);
	    	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}


/*
 * 标签js开始
 */

//存储勾选的标签id
var addTagIdArrAdd = new Array();
//存储勾选的标签name
var addTagNameArrAdd = new Array();
//将标签添加到已选标签
function addTagAdd(obj){
	var id = $(obj).val();
	var name = $(obj).parent().text();
	if($(obj).prop("checked")){
		if(!addTagIdArrAdd.in_array(id)){
			addTagIdArrAdd.push(id);
			addTagNameArrAdd.push(name);
		}
		/*if(!addTagNameArrAdd.in_array(name)){
			addTagNameArrAdd.push(name);
		}*/
		
	}else{
		addTagIdArrAdd.remove(id);
		addTagNameArrAdd.remove(name);
	}
	appendTagAdd();
	
}
//删除已选标签
function delAddTagAdd(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArrAdd.remove(id);
	addTagNameArrAdd.remove(name);
	appendTagAdd();
}

//查询标签
function queryTagAdd(likeMemberTagName){
	$.ajax({ 
            type: "post", 
            url: $.baseUrl+'/memberTagController/showMemberTagList.do', 
            data: {"likeMemberTagName":likeMemberTagName},
            dataType: "json",
            async: false,
            success: function (data) {
            	$("#labelChooseAdd").find(".labelChoose_list ul").html("");
            	
            	$.each(data.rows, function(index, value){
			      $("#labelChooseAdd").find(".labelChoose_list ul").append("<li><input type='checkbox' onchange='javascript:addTagAdd(this);' name='' value='"+value.memberTagId+"' />" + value.memberTagName+ "</li>");
			    });
            }, 
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    alert(errorThrown); 
            } 
        });
}


function appendTagAdd(){
	$("#labelChooseAdd").find(".labelChoose_title").html("<span>已选标签:</span>");
	$.each(addTagIdArrAdd, function(index, value){
      	$("#labelChooseAdd").find(".labelChoose_title").append("<span class='labelChoose_span'>"+addTagNameArrAdd[index]+
      		"<span class='icon icon_close_active' onclick='delAddTagAdd(this)' >" +
      		"<input type='hidden' value='"+addTagIdArrAdd[index]+"' /><input type='hidden' value='"+addTagNameArrAdd[index]+"' />" +
      				"</span></span>");
    });
}

//页面添加选择的标签
function addSelectTagAdd(){
	$("#memberTagList").html("");
	$.each(addTagIdArrAdd, function(index, value){
	  	$("#memberTagList").append("<span class='labelChoose_span'>"+addTagNameArrAdd[index]+
	  		"<span class='icon icon_close_active' onclick='delSelectTagAdd(this)' >" +
	  		"<input type='hidden' value='"+addTagIdArrAdd[index]+"' /><input type='hidden' value='"+addTagNameArrAdd[index]+"' />" +
	  				"</span></span>");
	});
}

//删除已选标签
function delSelectTagAdd(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArrAdd.remove(id);
	addTagNameArrAdd.remove(name);
	addSelectTagAdd();
}
/*
 * 标签js结束
 */



/*
 * 分组js开始
 */

//存储勾选的分组id
var addGroupIdArr = new Array();
//存储勾选的分组name
var addGroupNameArr = new Array();

function addSelectGroupToArray(){
	var id = $("#selectMemberGroupId").val();
	var name = $("#selectMemberGroupName").val();
	if(id!='' && !addGroupIdArr.in_array(id)){
		addGroupIdArr.push(id);
	}
	if(name!='' && !addGroupNameArr.in_array(name)){
		addGroupNameArr.push(name);
	}
}

//页面添加选择的分组
function addSelectGroup(){
	$("#memberGroupList").html("");
	$.each(addGroupIdArr, function(index, value){
	  	$("#memberGroupList").append("<span class='labelChoose_span'>"+addGroupNameArr[index]+
	  		"<span class='icon icon_close_active' onclick='delSelectGroup(this)' >" +
	  		"<input type='hidden' value='"+addGroupIdArr[index]+"' /><input type='hidden' value='"+addGroupNameArr[index]+"' />" +
	  				"</span></span>");
	});
}

//删除已选分组
function delSelectGroup(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addGroupIdArr.remove(id);
	addGroupNameArr.remove(name);
	addSelectGroup();
}


//查询分组
function queryGroup(){
	$.ajax({ 
            type: "get", 
            url: $.baseUrl+'/memberGroupController/showMemberGroupListSelect.do', 
            dataType: "json",
            async: false,
            success: function (data) {
            	$("#addFenZu").find(".dropdown-menu").html("");
            	$.each(data, function(index, value){
			      $("#addFenZu").find(".dropdown-menu").append("<li data-id='"+value.memberGroupId+"'><a href='javascript:;'>"+value.memberGroupName+"</a></li>");
			    });
            }, 
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    alert(errorThrown); 
            } 
        });
}
/*
 * 分组js结束
 */

