//判断最多增加图文的条数
var subTextImageindex=1;
//封装图文消息
var ImageTextInfo =new Array();
// 子图文消息index
var subIndex =0;
// 图文列表项图片显示
var ImageShow ='ImgPrFengmian0';
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		// 初始化图文消息列表
		initImageTextInfo(wechatTemplateId);
		
		$('#tianjiaItem').click(function(){
			subTextImageindex++;
			if(subTextImageindex<9){
				var $dom = '<div id="subTextImage'+subTextImageindex+'" class="appmsgItem"><span>标题</span><div class="appmsgItem_img"><img id="ImgPrFengmian'+(subTextImageindex-1)+'" style="width:100%;height: 100%"/></div></div>';
				var $domInput='<input type="file" name="fengmianImage'+subTextImageindex+'" id="fengmianImage'+subTextImageindex+'" style="display:none"/>';
				$('#itemList').append($dom);
				$('#multipatPictureForm').append($domInput);
			}else{
				alert("你最多只可以增加8条图文消息");
			}
		});
		
		$('#itemList').delegate('.appmsgItem', 'click', function() {
			// 点击下一个之前，将当前的子图文信息保存
			saveImageTextInfo();
			// 清空form表单记录
			refreshForm();
			// 获取当前的缩略图的id
			ImageShow = $(this).children().eq(1).children().eq(0).attr("id");
			// preAll()查找当前元素之前所有的同辈元素
			var count = $(this).prevAll().length+1;
			subIndex =count;
			// 首先隐藏之前的input 其次显示当前的input
			refreshInputTypeFile(count);
			$('#sucaiOption').css('margin-top', (count-1) * 100 + 160);
			$("#ImageTextDigest_Div").css("display","none");
			
			// 多图文缩略图预览
			var fengmianImage ="#fengmianImage"+(count+1);
			$(fengmianImage).uploadPreview({ Img:ImageShow, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		});
		
		$('.fengmian').click(function(){
			subIndex =0;
			refreshInputTypeFile(0);
			$("#ImageTextDigest_Div").css("display","");
			$('#sucaiOption').css('margin-top', 15);
		});
		
		// 第一个图的上传
		$("#fengmianImage1").uploadPreview({ Img:ImageShow, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		
		$('#return_sucai').click(function(){ 
			common.Page.loadCenter('wechat/materialManage.html');
		});
		
		/*// 上传图片预览
		$("#saveImageTextInfo").click(function(){});*/
		
		// 点击单条图文，对应的素材填写详情---显示和隐藏对应的input<type="file">选项
		function refreshInputTypeFile(value){
			var count=value;
			for(var i=0;i<$("#multipatPictureForm").children().length;i++){
				$("#multipatPictureForm").children().eq(i).css("display","none");
			}
			$("#multipatPictureForm").children().eq(count).css("display","");
		}
		// 预览
		$("#priviewButton").click(function(){
			var data = JSON.stringify(ImageTextInfo);
			$("#ImageTextInfoInput").attr("value",data);
			$('#uploadForm').form('submit', {    
				url:$.baseUrl+'/wechatFansImageTextController/uploadTextImageInfo.do',  
				onSubmit: function(){
				},   
				success:function(result){    
				}    
			});
		});
	};

	return {
		init: init
	}
	
	/**
	 * 初始化图文信息列表
	 */
	function initImageTextInfo(id){
		$.ajax({
	        type: "post",
	        url: $.baseUrl+'/wechatFansImageTextController/initImageTextDetail.do',
	        data:{wechatTemplateId:id},
	        async: true,
	        success: function (data) {
	        	var data = eval('(' + data + ')');
	        	// 数据返回成功
	        	if(data.status =='success'){
	        		
	        	}
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	                alert(errorThrown); 
	        } 
		});
	}
	
	/**
	 * 清空Form的数据
	 */
	function refreshForm(){
		$("#ImageTextTitle").val('');
		$("#ImageTextAuthor").val('');
		$("#ImageTextImage").val('');
		$("#ImageTextCover").val('');
		$("#ImageTextDigest").val('');
		$("#ImageTextBody").val('');
		$("#ImageTextLink").val('');
	}
	
	function saveImageTextInfo(){
		// 标题
		var ImageTextTitle = $("#ImageTextTitle").val();
		// 作者
		var ImageTextAuthor = $("#ImageTextAuthor").val();
		// 图片path
		var ImageTextImagePath = $("#ImageTextImage").val();
		// 摘要
		var ImageTextDigest = $("#ImageTextDigest").val();
		// 正文
		var ImageTextBody = $("#ImageTextBody").val();
		// 原文链接
		var ImageTextLink = $("#ImageTextLink").val();
		// 是否封面
		var ImageTextCover = $("input[type='checkbox']").prop("checked");
		// 封面显示在正文中
		if(ImageTextCover==true){
			ImageTextCover =1;
		}else{
			ImageTextCover =0;
		}
		var ImageTextSingle ={
				'index':subIndex,
				'ImageTextTitle':ImageTextTitle,
				'ImageTextAuthor':ImageTextAuthor,
				'ImageTextCover':ImageTextCover,
				'ImageTextDigest':ImageTextDigest,
				'ImageTextBody':ImageTextBody,
				'ImageTextLink':ImageTextLink
		};
		// 将填写的字段显示在封面上--标题   第一个封面标题
		if(subIndex==0){
			$("#mianItem").children().eq(1).html(ImageTextTitle);
		}else{
			var id ="#subTextImage"+(subIndex+1);
			$(id).children().eq(0).html(ImageTextTitle);
		}
		ImageTextInfo.push(ImageTextSingle);
	
	}
	
	
});