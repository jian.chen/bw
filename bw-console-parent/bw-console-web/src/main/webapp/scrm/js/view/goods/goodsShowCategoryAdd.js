define(['common','view/goods/goodsShowCategoryManage'],function(common,goodsShowCategoryManage){
	
	function init(){
		
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		
		if(goodsShowCategoryManage.getType() == "show"){
			$("#goodsShow_title").html('商品类型详情');
			$("#areaSaveBtn").hide();
			show();
			
			//表单禁用
			disableForm('goodsShowCategoryAddForm',true);
		}else if(goodsShowCategoryManage.getType() == "edit"){
			$("#goodsShow_title").html('商品类型编辑');
			$("#goodsShowCategoryId").val(goodsShowCategoryManage.getGoodsShowCategoryId());
			$("#areaSaveBtn").show();
			
			show();
		}else{
			$("#goodsShow_title").html('新建商品类型');
		}
		

	}
	
	function show(){
		isAuthForAjax("/goodsShowController/showCategoryDetail.do?goodsShowCategoryId="+goodsShowCategoryManage.getGoodsShowCategoryId(),
			"POST", "", "JSON", {}, function (data) {
				$("#goodsShowCategoryId").val(data.goodsShowCategoryId);
				$("#goodsShowCategoryName").val(data.goodsShowCategoryName);
			}, function (){
				alert("request error");
			});
	}
	
	window.goodsShowCategoryAdd = function(){
		var isValid = formCheck($("#goodsShowCategoryAddForm"));
		if(isValid){
			isAuthForAjax("/goodsShowController/saveGoodsShowCategory.do",
				"POST", "", "JSON", serializeObject($('#goodsShowCategoryAddForm')), function (result) {
					var data = eval('(' + result + ')');
					alert(data.message);
					$('#close_btn').click();
					$("#goodsShowCategoryTable").datagrid('load');
				}, function (){
					alert("request error");
				});
		}
	}
	
	return {
		init:init
	}
	
});

