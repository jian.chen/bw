define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		$("#equityAdd_image").uploadPreview({ Img: "equityAdd_img_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		$('#equityAdd_image').change(function() {
			if($(this).val() != ""){
				$("#equityAdd_fileSpan").text($(this).val());
				//选择的图片存放在隐藏域，用于图片上传验证
				$("#equityAdd_filePath").val($(this).val());
				//进行提示
				$("#equityAdd_hint").text("未上传");
				//清空当前存在的图片路径
				$("#equityAdd_img").val("");
			}
		});
		
		//等级查询
		showMemberGradeDetail();
		
	}
	return {
		init: init
	}
});


function showMemberGradeDetail(){
	$.ajax({ 
        type: "get", 
        url : $.baseUrl+'/gradeController/showGradeList.do',
        dataType: "json",
        async: false,
        success: function (data) {
        	var span = "";
        	for(var i = 0; i < data.length; i++){
        		span += "<span>";
                span += "<input type='checkbox' name='gradeId_"+i+"' value='"+data[i].gradeId+"' checked='checked' > "+data[i].gradeName;
                span += "</span>";
            }
        	$("#grade_div").append("");
        	$("#grade_div").append(span);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

//保存会员权益信息
function equitySave(page){

	if($("#equity_Starttime").val() != ""){
		if($("#equity_Endtime").val() == ""){
			alert("请选择权益的截止日期");
			return;			
		}
	}
	if($("#equity_Endtime").val() != ""){
		if($("#equity_Starttime").val() == ""){
			alert("请选择权益的开始日期");
			return;			
		}
	}
	
	//选择图片未上传验证
	if($("#equityEdit_filePath").val() != ""){
		if($("#"+page+"_img").val() == ""){
			alert("选择的图片未进行上传");
			return;			
		}
	}
	
	//获取checkbox选择的值
	var gradeids;
	var i = 0;
	$("input[name^='grade']:checked").each(function(){
		if(i==0){
			gradeids = this.value;
		}else{
			gradeids = gradeids+","+this.value;			
		}
		i++;
	});
    $("#gradeIds").val(gradeids);
	
	if(checkForm(page)){
		$.ajax({
			type: "POST",
			url : $.baseUrl+'/memberEquityController/saveEquity.do',
			data:$('#equityAdd_form').serialize(),
			async: false,
			dataType : "json",
			success : function(result) {
				//result = eval('(' + result + ')');
				alert(result.message);
				$('#memberEquityTable').datagrid('reload'); 
				$('.mask, .mask_in').hide();
			},
			error : function() {
				parent.layer.alert("出错了:(");
			}
		});
	}
	
}
