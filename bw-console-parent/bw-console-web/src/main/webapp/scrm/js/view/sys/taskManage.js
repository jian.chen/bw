define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格&下拉框
		initDataGrid();
		initSelect();
		
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
		$('#seachTask').click(function() {
			
			$('#comTaskTable').datagrid('load', serializeObject($('#comTaskForm')));
		});
	}

	function initDataGrid(){
		$('#comTaskTable').datagrid( {
			url : $.baseUrl+'/comTaskController/showComTaskList.do',
			idField : 'taskId',
				onLoadSuccess : function(result) {
					if(result) {
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	return {
		init: init
	}

});



