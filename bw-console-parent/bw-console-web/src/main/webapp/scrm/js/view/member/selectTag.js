$(function() { 
	//标签弹出框
	/*
		$('#labelChooseBtn').click(function() {
			$('#labelChoose').show();
			queryTag();
		});
		
		$('#queryTag_bt').click(function() {
			queryTag();
		});
		
		$('#tagSaveBtn').click(function() {
			//关闭弹出层
			$(".detailed_close").closest('.mask, .mask_in').hide();
			if(addTagIdArr.length>0){
				$("#memberTagIds").val(addTagIdArr.toString());
				$("#labelChooseBtn").val(addTagNameArr[0]+"...");
			}
			
		});
		*/
});

//将标签添加到已选标签
function addTag(obj){
	var id = $(obj).val();
	var name = $(obj).parent().text();
	if($(obj).prop("checked")){
		if(!addTagIdArr.in_array(id)){
			addTagIdArr.push(id);
			addTagNameArr.push(name);
		}
		
	}else{
		addTagIdArr.remove(id);
		addTagNameArr.remove(name);
	}
	appendTag();
	
}
//删除已选标签
function delAddTag(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArr.remove(id);
	addTagNameArr.remove(name);
	appendTag();
}

//查询标签
function queryTag(likeMemberTagName){
	$.ajax({ 
            type: "post", 
            url: $.baseUrl+'/memberTagController/showMemberTagList.do', 
            data: {"likeMemberTagName":likeMemberTagName},
            dataType: "json",
            async: false,
            success: function (data) {
            	$(".labelChoose_list ul").html("");
            	$.each(data.rows, function(index, value){
			      $(".labelChoose_list ul").append("<li><input type='checkbox' onclick='javascript:addTag(this);' name='' value='"+value.memberTagId+"' />" + value.memberTagName+ "</li>");
			    });
            }, 
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    alert(errorThrown); 
            } 
        });
}


function appendTag(){
	$(".labelChoose_title").html("<span>已选标签:</span>");
	$.each(addTagIdArr, function(index, value){
      	$(".labelChoose_title").append("<span class='labelChoose_span'>"+addTagNameArr[index]+
      		"<span class='icon icon_close_active' onclick='delAddTag(this)' >" +
      		"<input type='hidden' value='"+addTagIdArr[index]+"' /><input type='hidden' value='"+addTagNameArr[index]+"' />" +
      				"</span></span>");
    });
}