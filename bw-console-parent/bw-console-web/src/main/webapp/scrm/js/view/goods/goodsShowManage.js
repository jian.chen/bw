define(['common'], function(common) {
	
	var type;
	var goodsShowId;
	var goodsShowIds;
	var statusId;

	function init(){
		initPage();
	}
	
	function initPage() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		//查看
		$("#showGoods").click(function(){
			var mm = check();
			if(mm == 1){
				type = 'show';
				isAuth("/goods/goodsShowAdd.html", function() {common.Page.loadMask('goods/goodsShowAdd.html')});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		
		//编辑
		$("#editGoods").click(function(){
			var mm = check();
			if(mm == 1){
				if(statusId == '2202'){
					alert("上架状态下,兑换不可修改");
					return;
				}
				type = 'edit';
				isAuth("/goodsShowController/saveGoodsShow.do", function() {common.Page.loadMask('goods/goodsShowAdd.html')});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});

		//新建
		$("#saveGoods").click(function(){
			type = 'save';
			isAuth("/goodsShowController/saveGoodsShow.do", function() {common.Page.loadMask('goods/goodsShowAdd.html')});
		});

		//删除
		$("#deletedGoods").click(function(){
			var mm = check();
			if(mm != 2){
				if(confirm("确认删除？")){
					if(mm == 1){
						goodsShowIds = goodsShowId;
					}
					deleted();
				}
			}else{
				alert("请选择条目！");
			}	
		});
		
		
	}
	
	function initDataGrid(){
		$('#goodsShowTable').datagrid( {
			url : $.baseUrl+'/goodsShowController/showList.do',
			idField : 'goodsShowId',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	function check(){
		var selected = $('#goodsShowTable').datagrid('getChecked');
		goodsShowIds = '';
		goodsShowId = '';
		
		if(selected.length==1){
			goodsShowId = selected[0].goodsShowId;
			statusId = selected[0].statusId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			for(var i=0; i<selected.length; i++){
				if (goodsShowIds != ''){ 
					goodsShowIds += ',';
			    }
				goodsShowIds += selected[i].goodsShowId;
			}
			return 3;
		}

	}
	
	function deleted(){
		isAuthForAjax("/goodsShowController/removeList.do?goodsShowIds="+goodsShowIds,
			"POST", "", "JSON", {}, function (result) {
				var data = eval('(' + result + ')');
				goodsShowIds="";
				goodsShowId="";
				alert(data.message);
				$("#goodsShowTable").datagrid('clearChecked');
				$("#goodsShowTable").datagrid('reload');
			}, function (){
				alert("request error");
			});
		//var url=$.baseUrl+"/goodsShowController/removeList.do?goodsShowIds="+goodsShowIds;
		//$.ajax({
		//	type:"POST",
		//	url:url,
		//	dataType:"JSON",
		//	contentType: "application/x-www-form-urlencoded; charset=utf-8",
		//	success:function(result){
		//		var data = eval('(' + result + ')');
		//		goodsShowIds="";
		//		goodsShowId="";
		//		alert(data.message);
		//		$("#goodsShowTable").datagrid('clearChecked');
		//		$("#goodsShowTable").datagrid('reload');
		//	},
		//	error:function(){
		//		alert("request error");
		//	}
		//});
	}
	
	window.shelves = function shelves(value, row, index){
		var statusTemp = $.GOODS_SHOW_STATUS.getText(row.statusId);
		
		if(statusTemp == '上架'){
			
			return "<button id='"+row.goodsShowId+"_start' type='button' onclick='javascript:updateGoodsStatus(&apos;"+row.goodsShowId+"&apos;,&apos;"+row.statusId+"&apos;)' class='border-btn'>下架</a>";
		}else{
			return "<button id='"+row.goodsShowId+"_start' type='button' onclick='javascript:updateGoodsStatus(&apos;"+row.goodsShowId+"&apos;,&apos;"+row.statusId+"&apos;)' class='solid-btn'>上架</a>";
		}
	}
	
	window.updateGoodsStatus = function(goodsShowId,statusId){
		isAuthForAjax("/goodsShowController/updateStatus.do",
			"POST", "", "JSON", {goodsShowId : goodsShowId, statusId : statusId}, function (result) {
				var data = eval('(' + result + ')');
				alert(data.message);
				$("#goodsShowTable").datagrid('load');
			}, function (){
				alert("request error");
			});
	}

	return {
		init: init,
		getType : function(){
			return type;
		},
		getGoodsShowId : function(){
			return goodsShowId;
		}
	}

});

