define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		initDate();
		initInput();
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
		
	}
	return {
		init: init
	}
});


function couponBTAdd(page){
//	if(checkForm(page)){
		$.ajax({
//        cache: true,
			type: "POST",
			url : $.baseUrl+'/couponBusinessTypeController/insertCouponBusinessType.do',
			data:$('#couponBTAdd_form').serialize(),
			async: false,
			dataType : "json",
			success : function(result) {
				result = eval('(' + result + ')');
				alert(result.message);
				$('#couponTable').datagrid('reload'); 
				$('.mask, .mask_in').hide();
			},
			error : function() {
				parent.layer.alert("出错了:(");
			}
		});
		
	}
//}
	
