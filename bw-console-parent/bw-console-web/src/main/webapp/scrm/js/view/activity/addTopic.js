define(['common'], function(common) {
	//页面init
	function init(){
		initSelect();
		var sec_num =2;
		$("#addSelect").on("click",function(){
			var num = $(".del").length;
			if(num>4){
				alert("最多只能5个选项");  
				return
			}
			var nextOne = $("#topicModel > li:first").clone().attr("id","choose"+sec_num);
			nextOne.find(".fl").text("选项");
			nextOne.find("input").attr("id","content"+sec_num).val("");
			$("#topicModel").append(nextOne);
			sec_num++;
			innitIndex();
		});
		$(".questionDetail").on("click",".del",function(){
			if($(".del").length!=1){
				$(this).parent("li").remove();
				innitIndex();
			}else{
				$.messager.alert("提示","最少为1个");
			}
		});
		
		$('.dropdown-menu > li').click(function(){
			var type = $(this).attr("data-id");
			if(type == 1){
				$("#topicModel").show();
				$("#newSelect").show();
			}else{
				$("#topicModel").hide();
				$("#newSelect").hide();
			}
		});
		
		$("#saveTopic").click(function(){
			var type=$("[name='topicType']").val();
			var explains = $("#explains").val();
			var selectNum = $("#selectNum").val();
			var selectList = [];
			var templateId = manageQuestionId;
			$(".topicSelect").each(function(e){
				selectList[e]=$(this).val();
			});
			if(type == ""){
				$("#topicType").alert("不能为空");
				return;
			}
			if(explains == ""){
				$("#explains").alert("不能为空");
				return;
			}
			if(type == "1" && (isNaN(selectNum) || selectNum < 0.1 || selectNum=='')){
				$("#selectNum").alert("请输入正确的数字!");
				return;
			}
			$.ajax({
				type : "post",
				url : $.baseUrl + "/QuestionController/saveTemplate",
				dataType : "json",
				data : {
					selectList : selectList,
					type : type,
					explains : explains,
					selectNum : selectNum,
					questionnaireId : templateId
				},success : function(data){
					alert(data.message);
					manageTemplateCount ++;
					$(".detailed_close").closest('.mask, .mask_in').hide();
					$('#questionTemplateTable').datagrid('reload');
				}
			})
		})
	}
	
	function innitIndex(){
		var num_list =['a','b','c','d','e'];
		$("#topicModel li span").filter(".fl").each(function(e){
			$(this).text("选项" + num_list[e]);
		});
	}
	
	return {
		init: init
	}
	
	
});
