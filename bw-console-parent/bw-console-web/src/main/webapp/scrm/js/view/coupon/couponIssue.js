var memberIds;
//存储勾选的标签id
var addTagIdArr = new Array();
//存储勾选的标签name
var addTagNameArr = new Array();

var availableNum;
var couponmemberArr;
var importcouponMemberArr;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initSelect();
		initDate();
		initInput();
		//保存勾选的会员id
		couponmemberArr =new Array();
		importcouponMemberArr = new Array();
		memberIds = 'SOME';
		
		//返回
		 $("#goback").click(function(){
			common.Page.loadCenter('coupon/couponManage.html');
		})
		
		/*
		 * 表格查询绑定
		 */
		$('#search_btn').click(function() {
			$('#memberTable').datagrid({
				url : $.baseUrl+'/memberController/showMemberList.do',
				idField : 'memberId',
				singleSelect : true,
				pagination : true,
			    queryParams : serializeObject($('#memberSearchForm')),
			    onLoadSuccess : function(result) {
			    	setIssueNum(result.total);
				}
				/*onCheck : function(value,row,edit) {
					couponmemberArr.push(row.memberId);
					setIssueNum(couponmemberArr.length);
				},
				onUncheck : function(value,row,edit) {
					couponmemberArr.remove(row.memberId);
					setIssueNum(couponmemberArr.length);
				},
				onCheckAll:function(rows) {
					$.each(rows, function(index, item){
						if(!couponmemberArr.in_array(item.memberId)){
							couponmemberArr.push(item.memberId);
						}
		            });
					setIssueNum(couponmemberArr.length);
				},
				onUncheckAll:function(rows) {
					$.each(rows, function(index, item){
						couponmemberArr.remove(item.memberId);
		            });
					setIssueNum(couponmemberArr.length);
				}*/
			});
		});
		
		/*
		 * 标签弹出层开始
		 */
		$('#labelChooseBtn').click(function() {
			$('#labelChoose').show();
			queryTag($('#likeMemberTagName').val());
		});
		$('#queryTag_bt').click(function() {
			queryTag($('#likeMemberTagName').val());
		});
		$('#tagSaveBtn').click(function() {
			//$(".detailed_close").closest('.mask, .mask_in').hide();
			$('#labelChoose').find(".detailed_close").click();
			$("#memberTagIds").val(addTagIdArr.toString());
			$("#labelChooseBtn").val(addTagNameArr);
		});
		//下载模板
		$('#downloadExcel').click(function() {
			downTemp();
		});
		//区域门店弹框
		$('#equalOrgId').click(function() {
			initTreeData("areaChoose","areaTree","/orgController/testTree.do","1");
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTree",1);
			var selectNames= getSelectName("areaTree",1);
			$("[name='equalOrgId']").val(selectIds);
			$("[name='equalOrgName']").val(selectNames);
			$('#areaChoose').find(".detailed_close2").click();
		});
		$("[name='memberRadio']").click(function() {
			var val = $(this).val();
			$("#issue_num").val('');
			if(val==1){
				memberIds = "SOME";
				$("#personalAmount").attr("readonly",false);
				$("#queryMemberDiv").show();
				$("#importMemberDiv").hide();
				$('#couponInstances_num').hide();
				//$('#peopleCount').val(couponmemberArr.length);
				//$("#issue_num").val(couponmemberArr.length);
			}else if(val==2){
				memberIds = "IMPORT";
				$("#personalAmount").val("1");
				$("#personalAmount").attr("readonly",true);
				$("#queryMemberDiv").hide();
				$('#couponInstances_num').hide();
				$("#importMemberDiv").show();
				$('#peopleCount').val(couponmemberArr.length);
				$("#issue_num").val(importcouponMemberArr.length);
			}else if(val==3){
				memberIds = "ALL";
				$("#personalAmount").val("1");
				$("#personalAmount").attr("readonly",true);
				$("#queryMemberDiv").hide();
				$('#couponInstances_num').hide();
				$("#importMemberDiv").hide();
				showMemberNum();
			}else if(val==4){
				memberIds = "NONE";
				$("#personalAmount").val("1");
				$("#personalAmount").attr("disabled",true);
				$('#peopleCount').attr("disabled",true);
				$("#queryMemberDiv").hide();
				$("#importMemberDiv").hide();
				$('#couponInstances_num').show();
				$("#instance_num").focus();
			}
		});
		$("#importMemberDiv").hide();
		$('#couponInstances_num').hide();
		$('#fileInput').change(function() {
			$("#fileSpan").text($(this).val());
		});
		
		$('#memberImportBtn').click(function() {
			$('#daoruhuiyuan').show();
		});
		
		$('#importBtn').click(function() {
			importData();
		});
		
		//发放
		$('#saveBtn').click(function() {


			if(checkCouponIssueNum()){
				$('#comfirmSendBox').show();
				$("#refusSend").click(function(){
					$('#comfirmSendBox').hide();
				});
				$('#comfirmSendBtn').click(function(){
					var isSendTemplate;
					if($('#isSendTemplate').is(':checked')){
						isSendTemplate=$('#isSendTemplate').val();
					}
					else{
						isSendTemplate=0;
					}
					saveCouponIssue(isSendTemplate);
				});
			}
			// $.messager.confirm("确认","确认下发此优惠券?",function(r){
			// 	 if (r){
			// 		 saveCouponIssue();
			// 	 }
			// })
		});
		$('#chooseUserDetailedBtn').click(function() {
			common.Page.loadMask('memberGroup/groupAddBatch.html');
		});
		
		initCouponInstanceDataGrid();
		//输入优惠券张数事件
		$('#instance_num').blur(function() {
			$("#issue_num").val($("#instance_num").val());
		});
		$('#personalAmount').blur(function() {
			$("#issue_num").val($("#personalAmount").val()*$("#peopleCount").val());
		});
	}
	return {
		init: init
	}
});
//初始化优惠券数据
function initCouponInstanceDataGrid(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/couponController/queryCouponDetail.do',
		data : "couponid="+couponIds,
		dataType : "json",
		async: false,
		success : function(result) {
			$("#couponIssue_couponName").val(result.coupon.couponName);
			if(result.coupon.totalQuantity==0){
				$("#available_issue_num").html("/无限");
				availableNum = -1;
			}else{
				$("#available_issue_num").html("/"+result.coupon.availableQuantity);
				availableNum = result.coupon.availableQuantity;
			}
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});
}
//发放优惠券
function saveCouponIssue(val){
	if(checkCouponIssueNum()){
		if(memberIds == "SOME" || memberIds == undefined){
			//memberIds = couponmemberArr.toString();
			//$("#issue_num").val(couponmemberArr.length);
		}else if(memberIds == "IMPORT"){
			memberIds = importcouponMemberArr.toString();
			$("#issue_num").val(importcouponMemberArr.length);
			
		}
		var couponIssueNum = $("#issue_num").val();
		var forms = serializeObject($('#memberSearchForm'));
		$.messager.alert("提示","发放开始，可在优惠券管理列表查看发放情况");
		$('#comfirmSendBox').hide();
		$.ajax({
			type : "post",
			url : $.baseUrl+'/couponIssueController/saveCouponIssues.do',
			data : {
				isSendTemplate:val,
				couponId : couponIds,
				couponIssueNum : couponIssueNum,
				memberIds : memberIds,
				personalAmount : $("#personalAmount").val(),
				search : JSON.stringify(forms)
			},
			dataType : "json",
			success : function(result) {
				if(result.code==200){
					$('#couponTable').datagrid('reload');
				}else{
					$.messager.alert("提示",result.message);
				}
				
			},
			error : function() {
				parent.layer.alert("出错了:(");
			}
		});

	}
}
//查询系统会员的数量
function showMemberNum(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/couponIssueController/showMemberCount.do',
		dataType : "json",
		async: false,
		success : function(result) {
			$('#peopleCount').val(result);
			$("#issue_num").val($("#personalAmount").val()*result);
		},
		error : function() {
			alert("出错了:(");
		}
	});
}
//初始化会员表数据
function initDataGrid(){
	//初始化table
	$('#memberTable').datagrid( {
		url : $.baseUrl+'/memberController/showMemberList.do',
		idField : 'memberId',
		singleSelect : true,
		pagination : true,
	    queryParams : serializeObject($('#memberSearchForm')),
		onLoadSuccess : function(result) {
		}
		/*onCheck : function(value,row,edit) {
			couponmemberArr.push(row.memberId);
			setIssueNum(couponmemberArr.length);
		},
		onUncheck : function(value,row,edit) {
			couponmemberArr.remove(row.memberId);
			setIssueNum(couponmemberArr.length);
		},
		onCheckAll:function(rows) {
			$.each(rows, function(index, item){
				if(!couponmemberArr.in_array(item.memberId)){
					couponmemberArr.push(item.memberId);
				}
            });
			setIssueNum(couponmemberArr.length);
		},
		onUncheckAll:function(rows) {
			$.each(rows, function(index, item){
				couponmemberArr.remove(item.memberId);
            });
			setIssueNum(couponmemberArr.length);
		}*/
	});
}


//导入会员
function importData() {
	$('#importForm').form('submit', {    
	    url:$.baseUrl+'/memberGroupController/importData.do',  
	    onSubmit: function(){    
	    },    
	    success:function(result){    
	    	var data = $.parseJSON(result);
	    	$('#daoruhuiyuan').find(".detailed_close").click();
	    	$('#memberImportTable').datagrid({
				idField : 'memberId',
				data : data,
				singleSelect : false,
				pagination : true,
				pageNumber : 1,
				pageSize : 100,
				pageList : [  10, 30, 50, 100, 200, 500],
				onCheck : function(value,row,edit) {
					if(row.memberId && !importcouponMemberArr.in_array(row.memberId)){
						importcouponMemberArr.push(row.memberId);
					}
					setIssueNum(importcouponMemberArr.length);
				},
				onUncheck : function(value,row,edit) {
					if(row.memberId){
						importcouponMemberArr.remove(row.memberId);
					}
					setIssueNum(importcouponMemberArr.length);
				},
				onCheckAll:function(rows) {
					$.each(rows, function(index, item){
						if(item.memberId && !importcouponMemberArr.in_array(item.memberId)){
							importcouponMemberArr.push(item.memberId);
						}
		            });
					setIssueNum(importcouponMemberArr.length);
				},
				onUncheckAll:function(rows) {
					$.each(rows, function(index, item){
						if(item.memberId){
							importcouponMemberArr.remove(item.memberId);
						}
		            });
					setIssueNum(importcouponMemberArr.length);
				}
			});
	   }    
	});  
}

//下载模板
function downTemp() {
	window.location=$.baseUrl+'/memberGroupController/downTemp.do';
}

//控制发放券的数量
function setCouponIssueNum(val){
	var num = $("#issue_num").val();
	if(num==undefined || num==''){
		$("#issue_num").val(val);
	}else{
		$("#issue_num").val(Number(num)+Number(val));
	}
}
//设置发放券数量
function setIssueNum(val){
	$("#peopleCount").val(val);
	$("#issue_num").val($("#personalAmount").val()*val);
}
//校验券是否可以发放
function checkCouponIssueNum(){
	if($("#personalAmount").val() == '' || $("#personalAmount").val() == '0'){
		$.messager.alert("提示","请输入正确的发放数量!");
		return false;
	}
	
	var couponNum = $("#issue_num").val();
	if(couponNum == '' || couponNum == '0'){//未选择发放对象或数量
		$.messager.alert("提示","请选择发放对象!");
		return false;
	}else if(!rtNZero(couponNum)){//输入错误字符
		$.messager.alert("提示","请输入合法字符");
		return false;
	}else if(availableNum==-1){//无限数量优惠券
		return true;
	}else if(couponNum>availableNum){//发放数量大于可用数量
		$.messager.alert("提示","优惠券可用数量不够!");
		return false;
	}else{
		return true;
	}
}