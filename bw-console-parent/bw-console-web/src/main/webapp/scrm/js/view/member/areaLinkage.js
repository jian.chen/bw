//初始化下拉数据,下拉框id,pid上层id
var addressUrl = $.baseUrl+'/geoController/showGeoByParent.do?geoId=';
function initAddressData(id,pid){
	var obj = $("#"+id).next("ul");
	var url = addressUrl+ pid;
	obj.html("");
	$.ajax({ 
	        type: "get", 
	        url: url, 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$.each(data, function(i, v){
	        		obj.append("<li data-id='"+v.geoId+"'><a href='#' onclick='javascript:selectNode(this);'>"+v.geoName+"</a></li>");
			    });
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        }
	    });
}

function selectNode(obj){
	//获取id作为pid
	var id = $(obj).parent().attr("data-id");
	//获取下级id
	 var next_id = $.trim($(obj).parent().parent().attr("next-id"));
	 if(next_id){
		 clearNext(next_id);
	 	initAddressData(next_id,id);
	 }
	
}

function clearNext(next_id){
	 if(next_id){
	 	$("#"+next_id).val("");
	 	$("#"+next_id).prev().val("");
	 	var new_next = $("#"+next_id).next("ul").attr("next-id");
	 	clearNext(new_next);
	 }
}