define([ 'common', 'view/sys/sysManage', 'contants' ], function(common,
		sysManage, constants) {
	var type;
	var sysUserId;
	var _active_status = 4001;
	var _unactive_status = 4002;
	var docu = $("div.wechatPanel")[0];

	function init() {
		common.Page.resetMain();
		initDataGrid();
		initBindEvent();

		window.timestampToDate = function(value) {
			return new Date(parseInt(value)).toLocaleString().replace(/:\d{1,2}$/,' '); 
		}

	}
	;

	function initDataGrid() {
		isAuth('/sysUserController/showSysUserList.do', function() {
			$('#sysUserTable').datagrid({
				url : baseUrl + '/sysUserController/showSysUserList.do',
				idField : 'userId',
				pageNumber : 1,
				pageSize : 10,
				pageList : [ 1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 ]
			});
		});
	}

	function initBindEvent() {
		$('#showSys').click(function() {
			if (check() == 1) {
				type = 'show';
				isAuth('/sys/sysUserAdd.html', function() {
					$("#xitongguanliPanel").panel({
						href : 'sys/sysUserAdd.html'
					});
				});
			}
		});
		$('#editSys').click(function() {
			if (check() == 1) {
				type = 'edit';
				isAuth('/sys/sysUserAdd.html', function() {
					$("#xitongguanliPanel").panel({
						href : 'sys/sysUserAdd.html'
					});
				});
			}
		});

		$('#saveSys').click(function() {
			type = 'save';
			isAuth('/sys/sysUserAdd.html', function() {
				$("#xitongguanliPanel").panel({
					href : 'sys/sysUserAdd.html'
				});
			});
		});
		$('#stopSys').click(function() {
			isAuth('/sysUserController/changeStatus.do', function() {
				if (getChecked() != 0) {
					if (confirm("确认停用？")) {
						switchStatus(_unactive_status);
					}
				} else {
					$.messager.alert("提示", "请选择条目！");
				}
			});
		});
		$('#enabledSys').click(function() {
			isAuth('/sysUserController/changeStatus.do', function() {
				if (getChecked() != 2) {
					if (confirm("确认启用？")) {
						switchStatus(_active_status);
					}
				} else {
					$.messager.alert("提示", "请选择条目！");
				}
			})
		});
	}

	function getChecked() {
		return $('#sysUserTable').datagrid('getChecked').length;
	}

	function check() {
		var selected = $('#sysUserTable').datagrid('getChecked');
		if (selected.length == 1) {
			sysUserId = selected[0].userId;
		} else if (selected.length == 0) {
			$.messager.alert("提示", "请选择条目！");
		} else {
			$.messager.alert("提示", "不能同时选择多个条目！");
		}
		return selected.length;
	}

	function switchStatus(status) {
		var selected = $('#sysUserTable').datagrid('getChecked');
		var sysUserIds = '';
		if (selected.length == 0) {
			$.messager.alert("提示", '请选择条目！');
			return;
		} else {
			for (var i = 0; i < selected.length; i++) {
				if (sysUserIds != '') {
					sysUserIds += ',';
				}
				sysUserIds += selected[i].userId;
			}
		}
		var param = {};
		param.sysUserIds = sysUserIds;
		param.type = status;
		isAuthForAjax("/sysUserController/changeStatus.do", "POST", "", "JSON",
				param, function(data) {
					var msg;
					if (status == _active_status) {
						msg = "启用";
					} else if (status == _unactive_status) {
						msg = "停用";
					}
					if (data.status == "200") {
						$.messager.alert("提示", msg + "成功");
						$("#sysUserTable").datagrid('load');
						$("#sysUserTable").datagrid('clearChecked');
					} else if (data.status == "300") {
						$.messager.alert("提示", msg + "失败");
					} else {
						$.messager.alert("提示", "出错");
					}
					;
				}, function(result) {
					$.messager.alert("提示", "出错");
				});
	}

	function back() {
		sysManage.setUrl('sys/sysUserManage.html');
		common.Page.loadCenter('sys/sysManage.html');
	}

	return {
		init : init,
		getSysUserId : function() {
			return sysUserId;
		},
		getType : function() {
			return type;
		},
		back : back
	}
});
