define(['common'], function(common) {
	var type;
	var mealPeriodId;
	function init() {
		common.Page.resetMain();
		initDataGrid();
		
		//查看
		$('#show').click(function(){
			if(check()==1){
				type = 'show';
				isAuth('/base/mealEdit.html',function(){
					common.Page.loadCenter('base/mealEdit.html');
				});
			}else if(check()==2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		
		//编辑
		$('#edit').click(function(){
			if(check()==1){
				type = 'edit';
				isAuth('/mealController/editMealPeriod.do',function(){
					common.Page.loadCenter('base/mealEdit.html');
				});
			}else if(check()==2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		
		//新建
		$('#save').click(function(){
			type = 'save';
			isAuth('/mealController/saveMealPeriod.do',function(){
				common.Page.loadCenter('base/mealEdit.html');
			});
		});
		
		//删除
		$('#deleted').click(function(){
			if(check() != 2){
				if(confirm("确认删除？")){
					deleted();
				}
			}else{
				alert("请选择条目！");
			}
		});
		
	}
	
	//时段时间
	window.rowformater_date = function (value,row,index){
		return row.mealPeriodTime = row.startTime + "-" + row.endTime;
	}
	
	//时段时间
	window.rowformater_weeks = function (value,row,index){
		return row.weeks == null || row.startDate == "" ? "不限" : row.weeks;
	}
	
	//初始化时段列表
	function initDataGrid(){
		$('#mealPeriodTable').datagrid( {
			type: "POST", 
			url : $.baseUrl+'/mealController/showMealPeriodList.do',
			idField : 'mealPeriodId',
			onLoadSuccess : function(result) {
				if(result) {
					
				}
			},
			onLoadError : function() {
				
			}
		});
	}
	
	function check(){
		var selected = $('#mealPeriodTable').datagrid('getChecked');
		if(selected.length==1){
			mealPeriodId = selected[0].mealPeriodId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			return 3;
		}
		
	}
	
	//删除
	function deleted(){
		var selected = $('#mealPeriodTable').datagrid('getChecked');
		var mealPeriodIds = '';
		
		if(selected.length==0){
			alert('请选择条目！');
		}else{
			for(var i=0; i<selected.length; i++){
				if (mealPeriodIds != ''){ 
					mealPeriodIds += ',';
				}
				mealPeriodIds += selected[i].mealPeriodId;
			}
		}
		
		isAuthForAjax('/mealController/removeMealPeriod.do',"POST","","json","mealPeriodIds="+mealPeriodIds,function(result){
			if(result.status=='200'){
				alert(result.message);
				$("#mealPeriodTable").datagrid('load');
				//清除所有勾选的行
				$("#mealPeriodTable").datagrid('clearChecked');
			}else{
				alert(result.message);
			}
		
		},function(data){
			
		})		
	}
	
	return {
		init: init,
		getMealPeriodId : function(){
			return mealPeriodId;
		},
		getType : function(){
			return type;
		}
	}
});
