define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格&下拉框&日期&输入框验证
		initDataGrid();
		initSelect();
		initDate();
		initInput();
		
		$("#seachRelay").click(function(){
			var ck = formCheck($("#couponInstanceRelayForm"));
			if(ck){
				$('#couponInstanceRelayTable').datagrid({
					url : $.baseUrl+'/couponController/showCouponInstanceRelayList.do',
					queryParams : serializeObject($('#couponInstanceRelayForm')),
					onLoadSuccess : function(result) {
						if(result) {
								
				        }
					},
					onLoadError : function() {
							
					}
				});
			}
		});	
		
		//退回
		$('#rollbackBtn').click(function() {
			var selected  = $('#couponInstanceRelayTable').datagrid('getSelected');
			if(selected){
				$("#dataLoad").show();
				var statusId = $("[name='statusId']").val();
				if(statusId==1){					
					$.ajax({
						type: "post", 
						url:$.baseUrl+'/couponController/couponrollback.do',
						dataType: "json",
						data : {
							couponInstanceId : selected.couponInstanceId,
							srcId : selected.srcId				
						},
						success:function(result){
							$("#dataLoad").hide();
							if(result.status=='200'){
								alert(result.message);
								$("#couponInstanceRelayTable").datagrid('reload');
							}else{
								alert(result.message);
							}
						}  
					})
				}else{
					alert("只能退回转发中的优惠券！");
				}
			}else{
				alert("请选择一张优惠券");
				return;
			}			
		});

	}
	
	return {
		init: init
	}

});

//初始化table
function initDataGrid(){
	$('#couponInstanceRelayTable').datagrid({
		url : $.baseUrl+'/couponController/showCouponInstanceRelayList.do',
		queryParams : serializeObject($('#couponInstanceRelayForm')),
		idField : 'couponInstanceRelayId',
		onLoadSuccess : function(result) {
			if(result) {
					
	        }
		},
		onLoadError : function() {
				
		}
	});
}


/*window.row_Status = function(value,row,index){
var statusId = $("[name='statusId']").val();
if(statusId==1){
	return value = '转发中';
}else if(statusId==2){
	return value = '已转发';
}else {
	return value = '已退回';
}
}*/

