define(['common'], function(common) {
	var originalUrl;
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		//验证表格上传格式
		$("#card_image").uploadPreview({Callback: function () { }});
		
		//上传图片按钮
		$('#card_upload').click(function() {
			uploadcardimage();
		});
		//取消按钮
		$('#card_cansle').click(function() {
			$("#closeCardButton").click();
		});
		//保存按钮
		$('#saveCardDetail').click(function() {
			if(originalUrl == $('#imgUrl_Detail').val()){
				if (confirm('您没有上传新的图片,要离开此页吗？')){
					$("#closeCardButton").click();
					return;
				}
				return;
			}
			savecard();
		});
		
		$('#card_image').change(function() {
			$("#memberCard_fileSpan").text($(this).val());
		});
	}
	

	function initDataGrid(){
		$('#memberCardTable').datagrid( {
			url : $.baseUrl+'/memberCardGradeController/showMemberCardList.do',
			idField : 'memberCardGradeId',
			pageNumber : 1,
			pageSize : 10,
			pageList : [ 1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 ],
				onLoadSuccess : function(result) {
					if(result) {
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	//上传图片
	function uploadcardimage(){
		if($("#card_image").val() == null || $("#card_image").val() == ""){
			alert("请选择文件！");
			return;
		}
		$('#cardDetailForm').form('submit', {    
		    url : $.baseUrl+'/memberCardGradeController/uploadCardImage.do',    
		    success:function(result){
		    	result = eval('('+result+')');
		    	if(result=="不被允许上传的文件格式!"){
		    		alert("此文件格式不允许上传!");
		    	}else{
		    		$("#imgUrl_Detail").val($.rootPath+result);
			    	$("#memberCard_img_path").attr("src",$.rootPath+result);
			    	$("#memberCardStyle").val('');
			    	$("#memberCardStyle").next().val('');
		    	}
		    	
		    }    
		}); 
	}
	//保存设置
	function savecard(){
		$("#dataLoad").show();
		$.ajax({
			type:"POST",
			url : $.baseUrl+'/memberCardGradeController/editMemberCard.do',
			data:serializeObject($('#cardDetailForm')),
			datatype:"json",
			success:function(result){
				$("#dataLoad").hide();
				if(result != 0){
					$("#closeCardButton").click();
					alert('设置成功！');
					$('#memberCardTable').datagrid('load');
				}else{
					alert('设置失败，请重试！');
				}
			}
		});
	}
	
	window.showCardDetail = function showCardDetail(memberCardStyleId,memberCardGradeId,imgUrl){
		
		isAuthForAjax('/memberCardGradeController/editMemberCard.do',"POST","","json",serializeObject($('#cardDetailForm')),function(result){
			if(imgUrl == null || imgUrl == ''){
				imgUrl = '../img/memberCard.png';
			}
			$("#memberCard_img_path").attr("src",imgUrl);
			originalUrl = imgUrl;
			$("#memberCardGradeId_Detail").val(memberCardGradeId);
			$("#imgUrl_Detail").val(imgUrl);
			
			$("#card_image").val('');
			$("#memberCard_fileSpan").text('未选择文件...');
			if(memberCardStyleId != null && memberCardStyleId != 0){
				$("#memberCardStyle").val(memberCardStyleId);
				$("#memberCardStyle").next().val($.MEMBER_CARD_STYLE_ID.getText(memberCardStyleId));
			}
			$("#card_Detail").show();
		},function(data){
			parent.layer.alert("出错了:(");
		});
		
	}
	
	window.rowformater_card = function rowformater_card(value,row,index){
		return "<a href='javascript:showCardDetail("+row.memberCardStyleId+","+row.memberCardGradeId+",&quot;"+row.imgUrl+"&quot;);' target='_blank'>设置</a>";
	}
	window.rowformater_img = function rowformater_img(value){
		if(value == null || value == ''){
			return "<img src='../img/memberCard.png' style='width: 100%;height: 133px;' />";
		}
		return "<img src='"+value+"' style='width: 100%;height: 133px;' />";
	}
	
	return {
		init: init
	}

});






/*function Card_selectImg(url){
	if(url == 'src1'){
		url = 'http://codeorg.cn:8080/scrm-web-main/business/shops/upload/card/20150914151841/2302c32d-297c-4051-9a1f-cabd32e669bb.jpg';
	}else if(url == 'src2'){
		url = 'http://codeorg.cn:8080/scrm-web-main/business/shops/upload/card/20150914151900/389f4e50-a1e9-4dcf-b943-4b262f9e0f6d.jpg';
	}else if(url == 'src3'){
		url = 'http://codeorg.cn:8080/scrm-web-main/business/shops/upload/card/20150914151911/860df47c-ec88-4b83-8ad6-4ee1ac4415de.jpg';
	}
	
	$("#memberCard_img_path").attr("src",url);
	$("#imgUrl").val(url);
}*/


