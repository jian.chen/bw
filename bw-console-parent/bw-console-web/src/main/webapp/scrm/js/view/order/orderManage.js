define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格&下拉框&日期&输入框验证
		//initDataGrid();
		initSelect();
		initDate();
		initInput();
		
		$("#seachOrder").click(function(){
			var ck = formCheck($("#orderForm"));
			if(ck){
				$('#orderTable').datagrid({
					url : $.baseUrl+'/orderController/showListByExample.do',
					queryParams : serializeObject($('#orderForm')),
					idField : 'orderId',
						onLoadSuccess : function(result) {
							if(result) {
								
				            }
						},
						onLoadError : function() {
							
						}
					});
			}
		});
		
		
		//订单明细
		/*$('#orderItemDetailBtn').click(function() {
			var selected  = $('#orderTable').datagrid('getSelected');
			if(selected){
				order=selected;
				orderId = selected.orderId;
				isAll = selected.isAll;
				isAuth("/orderController/showOrderItemList.do",function(){
					common.Page.loadMask('order/orderItem.html');
				});
			}else{
				$.messager.alert("提示","请选择一条记录");
				return;
			}
			
		});*/
		
		window.getTicketNo = function(row){
			return row.externalId.split("-")[2];
		}
		
	}

	function initDataGrid(){
		$('#orderTable').datagrid({
			url : $.baseUrl+'/orderController/showListByExample.do',
			idField : 'orderId',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	return {
		init: init
	}

});

