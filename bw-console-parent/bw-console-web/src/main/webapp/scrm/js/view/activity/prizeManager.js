var sceneGameId;
var sceneGameCode;
var sceneGamePageUrl;
var imgUrl;
var pageTag = null;

define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化表格
		initDataGrid();
		
	}
	
	function initDataGrid(){
		$('#activityTable').datagrid( {
			url : $.baseUrl+'/sceneGameController/showSceneGameList.json',
			idField : 'sceneGameId',
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
			}
		});
	}
	
	window.bangding_game = function(value,id,imgUrl,pageUrl){
		sceneGameId = id;
		this.imgUrl = imgUrl;
		this.sceneGamePageUrl = pageUrl;
		common.Page.loadCenter('activity/prizeSetting.html');
	};
	
	window.bangding_game_manage = function(value,id,imgUrl,pageUrl){
		sceneGameId = id;
		this.imgUrl = imgUrl;
		this.sceneGamePageUrl = pageUrl;
		common.Page.loadCenter('activity/prizeInstanceManage.html');
	};
	
	return {
		init: init
	};
	
});

function rowformater_game(value,row,index){
	return "<button id='"+value+"_start' type='button' onclick='javascript:bangding_game(&quot;"+value+"&quot;,"+row.sceneGameId+",&quot;"+row.imgUrl+"&quot;,&quot;"+row.pageUrl+"&quot;)' class='solid-btn'>发起</a>" +
			"<button id='"+value+"_manage' type='button' onclick='javascript:bangding_game_manage(&quot;"+value+"&quot;,"+row.sceneGameId+",&quot;"+row.imgUrl+"&quot;,&quot;"+row.pageUrl+"&quot;)' class='border-btn'>管理</a>";
}

