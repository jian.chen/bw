define(['common'], function(common) {
	var mobile;
	function init() {
		common.Page.resetMain();
		
		//初始化表格
		initSmsInstanceDataGrid();
		
		$('#seachSmsInstance').click(function(){
			mobile = $('#instanceMobile').val();
			$('#smsInstanceTable').datagrid( {
				url : $.baseUrl+'/smsTempController/showSmsInstanceByExampleList.do',
				queryParams : {
					smsTempId : smsTempId,
					mobile : mobile
				},
				idField : 'smsTempId',
				onLoadSuccess : function(result) {
				},
				onLoadError : function() {
					
				}
				});
		});
		
		$("#goback").click(function(){
			common.Page.loadCenter('base/smsManage.html');
		})
		
		
	}
	
	function initSmsInstanceDataGrid(){
		$('#smsInstanceTable').datagrid( {
			url : $.baseUrl+'/smsTempController/showSmsInstanceByExampleList.do',
			queryParams : {
				smsTempId : smsTempId
			},
			idField : 'smsTempId',
			onLoadSuccess : function(result) {
			},
			onLoadError : function() {
				
			}
			});
	}
	
	return {
		init: init
	}
});