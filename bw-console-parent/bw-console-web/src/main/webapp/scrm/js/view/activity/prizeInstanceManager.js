var sceneGameInstanceId;
var optStatus;
define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化表格
		initDataGrid();
		pageTag = "prizeInstanceManager";
		
		$("#seachRecord").click(function(){
			$('#scenegame-instance-record-table').datagrid('load', serializeObject($('#recordForm')));
		});
		
		//返回
		$("#backBtn-form-prizeInstanceManager").click(function () {
			common.Page.loadCenter('activity/prizeManager.html');
		});
	}
	
	//加载数据
	function initDataGrid(){
		$('#game-instance-record-table').datagrid( {
			url : $.baseUrl+'/sceneGameInstanceController/showSceneGameInstanceList.json',
			queryParams:{'sceneGameId': sceneGameId},
			idField : 'sceneGameId',
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
			}
		});
	}
	
	window.showPrizeInstance = function(id,sta){
		sceneGameInstanceId = id;
		optStatus = sta;
		common.Page.loadCenter('activity/prizeSettingDetail.html');
	};
	
	//查看中奖纪录
	window.showWinningItem = function(id){
		sceneGameInstanceId = id;
		common.Page.loadCenter('activity/prizeInstanceReport.html');
	};
	
	return {
		init: init
	};
});


function dateDateformatByGame(val) {
	
	if(val==undefined || val ==''){
		return '';
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + ' ' + val.substring(8, 10) + ':' + val.substring(10, 12) + ":" +val.substring(12, 14);
	}
	
}

//加载活动展示时间
function showInstanceTime(value,row,index){
	return dateDateformatByGame(row.startTime)+" ~ "+dateDateformatByGame(row.endTime);
}

//加载状态
function showStatus(value,row,index){
	var status = "";
	
	if(value=="6101"){
		status="已保存";
	}else if(value=="6102"){
		status="已激活";
	}else if(value=="6103"){
		status="已过期";
	}else if(value=="6104"){
		status="已关闭";
	}
	
	return status;
}

//加载操作
function showOperation(value,row,index){
	
	var optHtml = "<button  type='button' onclick=\"javascript:showWinningItem('"+row.sceneGameInstanceId+"')\" class='solid-btn'>报告</button>";
	 optHtml += "<button  type='button' onclick=\"javascript:showPrizeInstance('"+row.sceneGameInstanceId+"','show')\" class='border-btn'>查看</button>";
	if(row.statusId=="6101"){
		optHtml += "<button  type='button' onclick=\"javascript:showPrizeInstance('"+row.sceneGameInstanceId+"','edit')\" class='border-btn'>编辑</button>";
		optHtml += "<button  type='button' onclick=\"javascript:updateSceneGameInstanceStatus('"+row.sceneGameInstanceId+"','open','"+row.sceneGameId+"')\" class='solid-btn'>激活</button>";
	}
	if(row.statusId=="6104"){
		optHtml += "<button  type='button' onclick=\"javascript:updateSceneGameInstanceStatus('"+row.sceneGameInstanceId+"','open','"+row.sceneGameId+"')\" class='solid-btn'>激活</button>";
	}
	if(row.statusId=="6102"){
		optHtml += "<button  type='button' onclick=\"javascript:showPrizeInstance('"+row.sceneGameInstanceId+"','editIsUse')\" class='border-btn'>编辑</button>";
		optHtml += "<button  type='button' onclick=\"javascript:updateSceneGameInstanceStatus('"+row.sceneGameInstanceId+"','close','"+row.sceneGameId+"')\" class='solid-btn'>关闭</button>";
	}
	return optHtml;
	
}


function updateSceneGameInstanceStatus(id,status,sceneGameId){
	$.ajax({
        url: $.baseUrl+'/sceneGameInstanceController/editStatus.do', 
        data:"sceneGameInstanceId="+id+"&status="+status+"&sceneGameId="+sceneGameId,
        dataType: "json",
        async: false,
        success: function (data) {
        	if(data.status=="SUCCESS"){
        		alert(data.message);
        		$("#game-instance-record-table").datagrid('reload');
        	}else{
        		alert(data.message);
        	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

