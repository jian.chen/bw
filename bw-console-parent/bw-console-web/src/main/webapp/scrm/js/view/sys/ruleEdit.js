define(['common', 'view/sys/ruleManage'], function(common, ruleManage) {
	function init() {
		common.Page.resetMain();
		initLoad();
		initInput();
		ruleManage.addEditEventBind();
	}

	function initLoad(){
		$("#roleId").val(ruleManage.getRoleId());
		$("#roleName").val(ruleManage.getRoleName());
		if(ruleManage.getType() == "show"){
			$("#title").text("角色查看");
			$("#save_btn").hide();
			$("#roleName").attr("readonly","readonly");
		}else{
			if(ruleManage.getType() == 'edit'){
				$("#title").text("角色编辑");
			}
			$("#save_btn").show();
			//$("#roleName").attr("readonly","readonly");
			$("#roleName").removeAttr("readonly");
		}
		var params = {};
		params.roleId = ruleManage.getRoleId();
		params.systemId = 1;
		$('#huiyuandingdanTable').treegrid( {
			url : baseUrl + "/sysRoleController/showSysRoleDetail.do",
			method:'post',
			queryParams: params});
		params.systemId = 2;
		$('#airTable').treegrid({
			url : baseUrl + "/sysRoleController/showSysRoleDetail.do",
			method:'post',
			queryParams: params});
		$('#shujuquanxian').datagrid({
			url : baseUrl+"/sysRoleController/showSysRoleData",
			method:'post',
			queryParams:{'roleId': params.roleId},
			onLoadSuccess : function(result) {
			},
			onLoadError : function() {
				$.messager.alert("提示", "onLoadError!");
			}
		});
	}

	return {
		init: init
	}
});




