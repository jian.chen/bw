define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		loadThisMask('points/pointsRevise.html');
		$('#pointsRevise_tab li').click(function(){
			var url = $(this).data('url');
			$(this).addClass('active').siblings().removeClass('active');
			loadThisMask(url);
		});
	};

	function loadThisMask(url) {
		$('#pointsRevisePanel').show().panel({
			href: url
		});
	};
	return {
		init: init
	}
});