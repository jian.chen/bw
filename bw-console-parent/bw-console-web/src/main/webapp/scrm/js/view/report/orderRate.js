define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		
	}
	
	function initDataGrid(){
		$('#orderRateTable').datagrid({
			url : $.baseUrl+'/ReportController/showOrderRate',
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
				
			}
		})
	}
	
	window.rowRate = function (value,row,index){
		if(row.sum == 0){
			return "0%";
		}else{
			var a = row.memberCount * 100 / row.sum;
			var b = a.toFixed(2);
			var rate = b + "%";
			return rate;
		}
	}
	
	return {
		init: init
	}
	
});