define(['common','view/base/qrCodeManage'],function(common,qrCodeManage){
	
	function init(){
		
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		//初始化二维码类型下拉框
		initSelectById('channelSelect','/qrCodeController/showChannelQrCodeList.do','channelId','channelName');
		
		//$("#qrCodeDescDiv").hide();
		//区域门店弹框
		$('#orgName').click(function() {
			initTreeData("areaChoose","areaTree","/orgController/testTree.do","2");
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTree",2);
			var selectNames= getSelectName("areaTree",2);
			$("[name='orgId']").val(selectIds);
			$("[name='orgName']").val(selectNames);
			$('#areaChoose').find(".detailed_close2").click();
		});
		
		if(qrCodeManage.getType() == "show"){
			$("#qrCode_title").html('二维码详情');
			$("#codeSave").hide();
			show();
			
			//表单禁用
			disableForm('qrCodeAdd_form',true);
		}else if(qrCodeManage.getType() == "edit"){
			$("#qrCode_title").html('二维码编辑');
			$("#codeSave").show();
			
			show();
		}else{
			$("#qrCode_title").html('新建二维码');
			$("#codeSave").show();
		}

	}
	
	function show(){
		var url=$.baseUrl+"/qrCodeController/qrCodeDetail.do?qrCodeId="+qrCodeManage.getQrCodeId();
		$.ajax({
			type:"POST",
			url:url,
			dataType:"JSON",
			contentType: "application/x-www-form-urlencoded; charset=utf-8", 
			success:function(data){
				$("#qrCodeId").val(data.qrCodeId);
				$("#qrCodeName").val(data.qrCodeName);
				
				$("#qrTypeId").val(data.qrTypeId);
				$("#qrTypeName").next("ul").find("[data-id="+data.qrTypeId+"]").find("a").click();
				
				$("#qrCodeDesc").val(data.qrCodeDesc);
				
				$("#channelId").val(data.channelId);
				$("#channelSelect").val($("#channelSelect").next("ul").find("[data-id="+data.channelId+"]").find("a").text());
				
				$("#orgId").val(data.orgId);
				$("#orgName").val(data.orgName);
				
				$("#remark").val(data.remark);
			},
			error:function(){
				alert("request error");
			}
		});
	}
	
	window.qrCodeAdd = function(){
		
		var isValid = formCheck($("#qrCodeAdd_form"));
		if(isValid){
			if($("#qrTypeId").val() == ''){
				$("#qrTypeName").alert('不能为空！');
				return;
			}else if($("#qrTypeId").val() == 2){
				if($("#qrCodeDesc").val() == ''){
					$("#qrCodeDesc").alert('不能为空！');
					return;
				}
			}
			if($("#channelId").val() == ''){
				$("#channelSelect").alert('不能为空！');
				return;
			}
			
			var url=$.baseUrl+"/qrCodeController/saveQrCode.do";
			$.ajax({
				type:"POST",
				url:url,
				dataType:"JSON",
				data:serializeObject($('#qrCodeAdd_form')),
				contentType: "application/x-www-form-urlencoded; charset=utf-8", 
				success:function(result){
					var data = eval('(' + result + ')');
					alert(data.message);
					$('#close_btn').click();
					$("#qrCodeTable").datagrid('load');
				},
				error:function(){
					alert("request error");
				}
			});
		}
	}
	
	window.showDesc = function(value){
//		if(value == '1'){
//			$("#qrCodeDescDiv").hide();
//		}else{
//			$("#qrCodeDescDiv").show();
//		}
	}
	
	return {
		init:init
	}
	
});

