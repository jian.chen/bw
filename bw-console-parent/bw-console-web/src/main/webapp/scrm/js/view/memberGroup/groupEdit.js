define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		$('#SQLEditBtn').click(function() {
			showsql("sqlArea",2);
			$('#SQLEdit').show();
		});
		
		$("[name='executeTypeRadio']").click(function() {
			$("[name='executeType']").val($(this).val());
		});
		
		$("#memberGroupSaveBtn").click(function() {
			saveMemberGroupEdit();
		});
		
		
		showMemberGroupDetail();
	}
	return {
		init: init
	}
});

//保存会员分组
function saveMemberGroupEdit(){
	$('#memberGroupEditForm').form('submit', {    
    url:$.baseUrl+'/memberGroupController/editMemberGroup.do?memberGroupId='+memberGroupId,
    onSubmit: function(){
    	var isValid = formCheck($(this));
    	if(isValid){
    		$("#dataLoad").show();
    	}
		return isValid;
    },    
    success:function(data){  
    	$("#dataLoad").hide();
    	var result = $.parseJSON(data);
    	//alert(result.message);
    	if(result.status=='200'){
    		$(".detailed_close").closest('.mask, .mask_in').hide();
    		$('#memberGroupTable').datagrid('load',{});
    		alert(result.message);
    	}else{
    		alert(result.message);
    	}
    }    
}); 
}

//查询分组详情
function showMemberGroupDetail(){
	
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberGroupController/showMemberGroupDetail.do?memberGroupId='+memberGroupId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("[name='memberGroupCode']").val(data.memberGroupCode);
        	$("#memberGroupCode").text(data.memberGroupCode);
        	$("[name='memberGroupName']").val(data.memberGroupName);
        	$("[name='remark']").val(data.remark);
        	$("[name='executeType']").val(data.executeType);
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}