define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initSelect();
		showEditTag();
		
		//区域门店弹框
		$(document).on("click","#equalOrgIdTag", function(){
			initTreeData("areaChooseManager","areaTreeManager","/orgController/testTree.do","1");
			var ids = $("[name='equalOrgIdTag']").val();
			if(ids){
				var idArr = ids.split(",");
				checkNode(idArr,"areaTreeManager");
			}
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTreeManager","1");
			var selectNames= getSelectName("areaTreeManager","1");
			$("[name='equalOrgIdTag']").val(selectIds);
			$("[name='equalOrgName']").val(selectNames);
			$('#areaChooseManager').find(".detailed_close2").click();
		});
		
		$("#updateNode").click(function(){
			if($(this).val() == 1){
				$("#tag_member").attr("class","tag_line none");
				$("#tag_order").attr("class","tag_line none");
				removeAllSelect();
				return;
			}
			if($(this).val() == 2){
				$("#tag_member").attr("class","tag_line");
				$("#tag_order").attr("class","tag_line none");
				return;
			}
			if($(this).val() == 3){
				$("#tag_member").attr("class","tag_line");
				$("#tag_order").attr("class","tag_line");
				return;
			}
		})
		
		$("[name='executeTypeRadio']").click(function() {
			$("[name='executeType']").val($(this).val());
			if($(this).val() == 1){
				$("#setUpdate").attr("class","clearfix none");
				$("#tagSelect").attr("class","clearfix none");
				$("#setNode").attr("class","clearfix none");
				$("[name='updateType']").val(""),
				$("[name='updateNode']").val(""),
				$("input[name='updateTypeRadio'][value='1']").prop("checked",'true');
				$("#updateNode option[value='0']").attr("selected",true);
				removeAllSelect();
			}
			if($(this).val() == 2){
				$("#setUpdate").attr("class","clearfix");
				$("#tagSelect").attr("class","clearfix");
			}
		});
		
		$("[name='updateTypeRadio']").click(function() {
			$("[name='updateType']").val($(this).val());
			if($(this).val() == 1){
				$("#setNode").attr("class","clearfix none");
			}
			if($(this).val() == 2){
				$("#setNode").attr("class","clearfix");
			}
		});
		
		$("#updateNode").click(function(){
			$("[name='updateNode']").val($(this).val());
		})
		
		$("[name='memberTagName']").blur(function(){
			if($("[name='memberTagName']") != ""){
				$.ajax({ 
			        type: "post", 
			        url: $.baseUrl+'/memberTagController/showCountByName.do', 
			        dataType: "json",
			        data : {
			        	memberTagName: $(this).val()
			        },
			        async: false,
			        success: function (data) {
			        	if(data>0){
			        		alert("标签名称已被使用");
			        	}
			        }, 
			        error: function (XMLHttpRequest, textStatus, errorThrown) { 
			            alert(errorThrown); 
			        }
			    });
			}
	  	
	  });
		
	}
	return {
		init: init
	}
});

function removeAllSelect(){
	$(".tag_line-ele > span").removeClass("active");
	$(".tag_view-panel div:last").siblings().remove();
}

function showTagCondtion(button){
	$("#select_"+button).toggleClass("active");
	var title = $("#select_"+button).text();
	$(".tag_item-title").each(function(){
		if($(this).text() == title){
			$(this).parent().clone().insertBefore(".tag_view-panel > div:first");
		}
	});

	
}

function showEditTag(){
	$.ajax({
		type: "post", 
        url: $.baseUrl+'/memberTagController/showEditTag', 
		dataType : "json",
        data : {
        	memberTagId : memberTagId
        },
        success : function (data){
        	//显示标签
        	$("[name='memberTagName']").val(data.tag.memberTagName);
			$("[name='remark']").val(data.tag.remark);
			$("#executeType").val(data.tag.executeType);
			$("input[name='executeTypeRadio'][value='"+data.tag.executeType+"']").prop("checked",'true');
			if(data.tag.updateType != ""){
				$("input[name='updateTypeRadio'][value='"+data.tag.updateType+"']").prop("checked",'true');
			}else{
				$("#setNode").attr("class","clearfix none");
			}
			
			if(data.tag.updateNode != ""){
				$("[name='updateNode']").val(data.tag.updateNode);
				$("#updateNode option[value='"+data.tag.updateNode+"']").attr("selected",true);
			}
			if(data.tag.executeType == "1"){
				$("#setUpdate").attr("class","clearfix none");
				$("#tagSelect").attr("class","clearfix none");
				$("#setNode").attr("class","clearfix none");
				$("[name='updateType']").val(""),
				$("[name='updateNode']").val(""),
				$("input[name='updateTypeRadio'][value='1']").prop("checked",'true');
				$("#updateNode option[value='0']").attr("selected",true);
				removeAllSelect();
			}
			if(data.tag.updateType == "1"){
				$("#setNode").attr("class","clearfix none");
				$("#updateNode option[value='0']").attr("selected",true);
			}
        	//显示标签选项
			for(var i = 0;i < data.tagCondition.length;i++){
				var condition_single = data.tagCondition[i];
				showTagCondtion(condition_single.distinctConditionRule);
				var value = condition_single.distinctConditionValue;
				//基本资料
				if("select_"+condition_single.distinctConditionRule == "select_mobile"){
					$(".tag_view-panel").find("input[name='mobileRadio'][value='" + value + "']").prop("checked",'true');
				}
				if("select_"+condition_single.distinctConditionRule == "select_gender"){
					$(".tag_view-panel").find("input[name='genderRadio'][value='" + value + "']").prop("checked",'true');
				}
				if("select_"+condition_single.distinctConditionRule == "select_age"){
					$("#ageStart").val(value.split(",")[0]);
					$("#ageEnd").val(value.split(",")[1]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_birthday"){
					$("#birthdayStart").val(value.split(",")[0]);
					$("#birthdayEnd").val(value.split(",")[1]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_register"){
					$("#registerDateStart").val(value.split(",")[0]);
					$("#registerDateEnd").val(value.split(",")[1]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_channel"){
					$("#channelId").val(value);
					$("#channelId").next().val($.Member_Channel.getText(value));
				}
				if("select_"+condition_single.distinctConditionRule == "select_grade"){
					$("#equalGradeId").val(value);
					$("#equalGradeId").next().val($.Member_Grade.getText(value));
				}
				//会员资料
				if("select_"+condition_single.distinctConditionRule == "select_points"){
					$("#pointsStart").val(value.split(",")[0]);
					$("#pointsEnd").val(value.split(",")[1]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_couponFree"){
					$("#couponFreeStart").val(value.split(",")[0]);
					$("#couponFreeEnd").val(value.split(",")[1]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_couponUsed"){
					$("#couponUsedStart").val(value.split(",")[0]);
					$("#couponUsedEnd").val(value.split(",")[1]);
				}
				//交易数据
				if("select_"+condition_single.distinctConditionRule == "select_orderCount"){
					$("#orderCountDateStart").val(value.split(",")[0]);
					$("#orderCountDateEnd").val(value.split(",")[1]);
					$("#orderCountStart").val(value.split(",")[2]);
					$("#orderCountEnd").val(value.split(",")[3]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_orderLast"){
					$("#orderLastStart").val(value.split(",")[0]);
					$("#orderLastEnd").val(value.split(",")[1]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_orderGrand"){
					$("#orderGrandAmountDateStart").val(value.split(",")[0]);
					$("#orderGrandAmountDateEnd").val(value.split(",")[1]);
					$("#orderGrandAmountStart").val(value.split(",")[2]);
					$("#orderGrandAmountEnd").val(value.split(",")[3]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_orderOne"){
					$("#orderOneAmountDateStart").val(value.split(",")[0]);
					$("#orderOneAmountDateEnd").val(value.split(",")[1]);
					$("#orderOneAmountStart").val(value.split(",")[2]);
					$("#orderOneAmountEnd").val(value.split(",")[3]);
				}
				if("select_"+condition_single.distinctConditionRule == "select_orderStore"){
					$("#editOrgTag").val(value.split(",")[0]);
				}
			}
        }
	})

}

function tagCondition(distinctConditionRule,distinctConditionValue,distinctConditionId)
{
	this.distinctConditionRule=distinctConditionRule;
	this.distinctConditionValue=distinctConditionValue;
	this.memberTagConditionId= null;
	this.memberTagId= null;
	this.distinctConditionId= distinctConditionId;
}

var tagCoditionList = new Array();
