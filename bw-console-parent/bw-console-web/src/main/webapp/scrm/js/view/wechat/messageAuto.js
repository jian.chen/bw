define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		$('.wechat_msg_top_row').delegate('[data-for]', 'click', function(){
			$($(this).data('for')).show().siblings().hide();
			$(this).addClass('active').siblings().removeClass('active');
		});
		$('.wechat_zidong_tab').delegate('[data-for]', 'click', function(){
			$($(this).data('for')).show().siblings().hide();
			$(this).addClass('active').siblings().removeClass('active');
		});
		$("#up").uploadPreview({ Img: "ImgPr", ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		$("#xiaoxi_up").uploadPreview({ Img: "xiaoxi_ImgPr", ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	};

	return {
		init: init
	}
});


function doSendGuanzhu(){
	var wechatMessageType;
	if($("#wenzi").is(":visible")){
		wechatMessageType="text";
	}
	if($("#tupian").is(":visible")){
		wechatMessageType="image";
		$("#guanzhu_content").val("");
	}
	$(".guanzhu_wechatMessageType").val(wechatMessageType);
	
		var url=$.baseUrl+"/WechatMessageAutoReplyController/sendWechatMessageAutoReply.do";
		$('#guanzhu_form').form('submit', {    
			url:url,  
			onSubmit: function(){
				alert("ok");
			},   
			success:function(result){    
			}    
		});     		
}	

function doSendXiaoxi(){
	var wechatMessageType;
	if($("#xiaoxi_wenzi").is(":visible")){
		wechatMessageType="text";
	}
	if($("#xiaoxi_tupian").is(":visible")){
		wechatMessageType="image";
		$("#xiaoxi_content").val("");
	}
	$(".xiaoxi_wechatMessageType").val(wechatMessageType);
	
	 	var url=$.baseUrl+"/WechatMessageAutoReplyController/sendWechatMessageAutoReply.do";
		$('#xiaoxi_form').form('submit', {    
			url:url,  
			onSubmit: function(){
				alert("ok");
			},   
			success:function(result){    
			}    
		});     	
}	

function showNewGuanjian(){
   	$("#new_guanjian").show();
}
function showAddKey(){
   	$("#addKey").show();
}
function doSaveKye(){
	 $("[name='matchType']").each(function(){
		 if($(this).is(':checked')){
			 $(this).next(".checkedValue").val("1");
		 }else{
			 $(this).next(".checkedValue").val("0");
		 }
     })
	 
   	$("#new_guanjian").hide();
   	
}
function doGetKyeName(){
	alert("本次关键字名称："+$("#keyName").val());
//	var keyNames=$("#keyNames").val();
//	keyNames+=","+$("#keyName").val();
//	$("#keyNames").val(keyNames);
//	alert("所有关键字名称:"+keyNames);
	
	var html="<input  type=\"text\" name=\"keyNames\" value=\""+$("#keyName").val()+"\"  readonly > <input  name=\"matchType\" type=\"checkbox\" value=\"\"><input  class=\"checkedValue\"  name=\"checkedValue\" type=\"hidden\"  >&nbsp;完全匹配 &nbsp;&nbsp;<br/> ";
	$("#showKeyNamesDiv").append(html);
	
	$("#keyName").val("");
	$("#addKey").hide();
}