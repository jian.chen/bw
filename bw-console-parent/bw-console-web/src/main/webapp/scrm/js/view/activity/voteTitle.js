define(['common'], function(common) {
	
	function init() {
		$("#createVote").click(function(){
			common.Page.loadMask('activity/addVoteTitle.html');
		});
		
		//初始化表格
		initDataGridVote();
	}
	
	window.rowformater_voteTable = function (value,row,index){
		var tempHtml = '';
		tempHtml += "<button id='manage' type='button' class='solid-btn' style='font-size: 14px;' onClick='manageVote("+row.titleId+","+ row.optionCount + ")'>选项管理</button>";
		tempHtml += "<button id='edit' type='button' class='solid-btn' style='font-size: 14px;' onClick='editVote("+row.titleId+")'>编辑</button>";
		tempHtml += "<button id='delete' type='button' class='solid-btn' style='font-size: 14px;' onClick='deleteVote("+row.titleId+")'>删除</button>";
		tempHtml += "<button id='count' type='button' class='solid-btn' style='font-size: 14px;' onClick='countVote("+row.titleId+")'>统计</button>";
		if(row.statusId == '1902'){
			tempHtml += "<button id='status' type='button' class='solid-btn' style='font-size: 14px;' onClick='stopVote("+row.titleId+")'>停用</button>";
		}
		if(row.statusId == '1905'){
			tempHtml += "<button id='status' type='button' class='solid-btn' style='font-size: 14px;' onClick='startVote("+row.titleId+")'>启用</button>";
		}
		return tempHtml;
	}
	
	window.rowformater_date = function (value,row,index){
		return row.startDate == null || row.startDate == "" ? "未发布" : getDateString(row.startDate);
	}
	
	return {
		init: init
	}
	
});

var editVoteId;//存储选择的问卷id
var manageVoteId;//存储管理的问卷id
var voteOptionCount;//存储管理问卷的题目数量
var voteCountId;//查看统计问卷的id

function initDataGridVote(){
	$('#voteTable').datagrid({
		url : $.baseUrl+'/VoteController/showVote',
		idField : 'titleId',
		onLoadSuccess : function(result) {
			
		},
		onLoadError : function() {
			
		}
	})
}

function editVote(titleId){
	editVoteId = titleId;
	$('#maskPanel').show().panel({
		href: 'activity/editVote.html'
	});
}

function manageVote(titleId,count){
	manageVoteId = titleId;
	voteOptionCount = count;
	$('#mainPanel').layout('panel', 'center').panel({
		href: 'activity/voteManage.html'
	});
}

function countVote(titleId){
	voteCountId = titleId;
	$('#mainPanel').layout('panel', 'center').panel({
		href: 'activity/voteCount.html'
	});
}

function deleteVote(titleId){
	if(confirm("确认要删除吗？")){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/VoteController/deleteVoteTitle",
			dataType : "json",
			data : {
				titleId : titleId
			},
			success : function(data){
				alert(data.code);
				$('#voteTable').datagrid('reload');
			}
		})
	
	}
}

function stopVote(titleId){
	$.ajax({
		type : "post",
		url : $.baseUrl + "/VoteController/changeVoteStatus",
		dataType : "json",
		data : {
			titleId : titleId,
			status : "N"
		},
		success : function(data){
			$.messager.alert("提示",data.message);
			$('#voteTable').datagrid('reload');
		}
	})
}

function startVote(titleId){
	$.ajax({
		type : "post",
		url : $.baseUrl + "/VoteController/changeVoteStatus",
		dataType : "json",
		data : {
			titleId : titleId,
			status : "Y"
		},
		success : function(data){
			$.messager.alert("提示",data.message);
			$('#voteTable').datagrid('reload');
		}
	})
}



