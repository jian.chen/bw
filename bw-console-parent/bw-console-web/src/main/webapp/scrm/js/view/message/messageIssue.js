var memberIds;
//存储勾选的标签id
var addTagIdArr = new Array();
//存储勾选的标签name
var addTagNameArr = new Array();

var availableNum;
var couponmemberArr;
var importcouponMemberArr;
var type;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initSelect();
		initDate();
		initInput();
		//保存勾选的会员id
		couponmemberArr =new Array();
		importcouponMemberArr = new Array();
		memberIds = undefined;
		/*
		 * 表格查询绑定
		 */
		$('#search_btn').click(function() {
			$('#memberTable').datagrid('load', serializeObject($('#memberSearchForm')));
		});
		
		/*
		 * 标签弹出层开始
		 */
		$('#labelChooseBtn').click(function() {
			$('#labelChoose').show();
			queryTag($('#likeMemberTagName').val());
		});
		$('#queryTag_bt').click(function() {
			queryTag($('#likeMemberTagName').val());
		});
		$('#tagSaveBtn').click(function() {
			//$(".detailed_close").closest('.mask, .mask_in').hide();
			$('#labelChoose').find(".detailed_close").click();
			$("#memberTagIds").val(addTagIdArr.toString());
			$("#labelChooseBtn").val(addTagNameArr);
		});
		//下载模板
		$('#downloadExcel').click(function() {
			downTemp();
		});
		//区域门店弹框
		$('#equalOrgId').click(function() {
			initTreeData("areaChoose","areaTree","/orgController/testTree.do","1");
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTree",1);
			var selectNames= getSelectName("areaTree",1);
			$("[name='equalOrgId']").val(selectIds);
			$("[name='equalOrgName']").val(selectNames);
			$('#areaChoose').find(".detailed_close2").click();
		});
		initDataGrid();
		$("[name='memberRadio']").click(function() {
			var val = $(this).val();
			$("#issue_num").val('');
			if(val==1){
				memberIds = "SOME";
				type = "2";
				$("#queryMemberDiv").show();
				$("#importMemberDiv").hide();
				$('#couponInstances_num').hide();
				$("#issue_num").val(couponmemberArr.length);
			}else if(val==2){
				memberIds = "IMPORT";
				$("#queryMemberDiv").hide();
				$('#couponInstances_num').hide();
				$("#importMemberDiv").show();
				$("#issue_num").val(importcouponMemberArr.length);
			}else if(val==3){
				memberIds = "ALL";
				type = "1";
				$("#queryMemberDiv").hide();
				$('#couponInstances_num').hide();
				$("#importMemberDiv").hide();
				showMemberNum();
			}else if(val==4){
				memberIds = "NONE";
				$("#queryMemberDiv").hide();
				$("#importMemberDiv").hide();
				$('#couponInstances_num').show();
				$("#instance_num").focus();
			}
		});
		$("#importMemberDiv").hide();
		$('#couponInstances_num').hide();
		$('#fileInput').change(function() {
			$("#fileSpan").text($(this).val());
		});
		
		$('#memberImportBtn').click(function() {
			$('#daoruhuiyuan').show();
		});
		
		$('#importBtn').click(function() {
			importData();
		});
		
		//选择会员保存
		$('#saveBtn').click(function() {
			couponIssueIng();
		});
		$('#chooseUserDetailedBtn').click(function() {
			common.Page.loadMask('memberGroup/groupAddBatch.html');
		});
		
//		initCouponInstanceDataGrid();
		//输入优惠券张数事件
		$('#instance_num').blur(function() {
			$("#issue_num").val($("#instance_num").val());
		});
	}
	return {
		init: init
	}
});
//初始化优惠券数据
//function initCouponInstanceDataGrid(){
//	$.ajax({
//		type : "get",
//		url : $.baseUrl+'/couponController/queryCouponDetail.do',
//		data : "couponid="+couponIds,
//		dataType : "json",
//		async: false,
//		success : function(result) {
//			$("#couponIssue_couponName").val(result.coupon.couponName);
//			if(result.coupon.totalQuantity==0){
//				$("#available_issue_num").html("/无限");
//				availableNum = -1;
//			}else{
//				$("#available_issue_num").html("/"+result.coupon.availableQuantity);
//				availableNum = result.coupon.availableQuantity;
//			}
//		},
//		error : function() {
//			parent.layer.alert("出错了:(");
//		}
//	});
//}
//发放优惠券
//function saveCouponIssue(){
//	if(checkCouponIssueNum()){
//		
//		$.ajax({
//			type : "post",
//			url : $.baseUrl+'/couponIssueController/editCoupon.do',
//			data : "couponId="+couponIds,
//			dataType : "json",
//			success : function(result) {
//				$('#couponTable').datagrid('reload');
//				$('.mask, .mask_in').hide();
//				couponIssueIng();
//				
//			},
//			error : function() {
//				parent.layer.alert("出错了:(");
//				$("#dataLoad").hide();
//			}
//		});
//		$('#couponTable').datagrid('reload');
//		$('.mask, .mask_in').hide();
//		$("#dataLoad").show();
		
//	}
//}
//发送消息
function couponIssueIng(){
	if(memberIds == "SOME" || memberIds == undefined || memberIds == ''){
		type = "2";
		memberIds = couponmemberArr.toString();
	}
	if(memberIds.length==0){
		alert("请选择会员!");
		return;
	}
	$("#dataLoad").show();
	$.ajax({
		type : "post",
		url : $.baseUrl+'/memberMessageController/sendMessage.do',
		data : "messageId="+messageIds+"&memberIds="+memberIds+"&type="+type,
		dataType : "json",
		//async: false,
		success : function(result) {
//			$("#dataLoad").hide();
//			result = eval('('+result+')');
			if(result.status==200){
				alert(result.message);
				$('.mask, .mask_in').hide();
//				$("#dataLoad").show();
				$('#memberMessageTable').datagrid('reload');
			}else{
				alert(result.message);
			}
			$("#dataLoad").hide();
		},
		error : function() {
//			parent.layer.alert("出错了:(");
			$("#dataLoad").hide();
		}
	});
}
//查询系统会员的数量
function showMemberNum(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/couponIssueController/showMemberCount.do',
		dataType : "json",
		async: false,
		success : function(result) {
			$("#issue_num").val(result);
		},
		error : function() {
			alert("出错了:(");
		}
	});
}
//初始化会员表数据
function initDataGrid(){
	//初始化table
	$('#memberTable').datagrid( {
		url : $.baseUrl+'/memberController/showMemberList.do',
		idField : 'memberId',
		singleSelect : false,
		pagination : true,
		pageNumber : 1,
		pageSize : 100,
		pageList : [  10, 30, 50, 100, 200, 500],
	    queryParams : serializeObject($('#memberSearchForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		
		onLoadError : function() {
			
		},
		onCheck : function(value,row,edit) {
			if(!couponmemberArr.in_array(row.memberId)){
				couponmemberArr.push(row.memberId);
//				setIssueNum(couponmemberArr.length);
				
			}
		},
		onUncheck : function(value,row,edit) {
			couponmemberArr.remove(row.memberId);
//			setIssueNum(couponmemberArr.length);
		},
		onCheckAll:function(rows) {
			$.each(rows, function(index, item){
				if(!couponmemberArr.in_array(item.memberId)){
					couponmemberArr.push(item.memberId);
				}
            });
//			setIssueNum(couponmemberArr.length);
		},
		onUncheckAll:function(rows) {
			$.each(rows, function(index, item){
				couponmemberArr.remove(item.memberId);
            });
//			setIssueNum(couponmemberArr.length);
		}
	});
}


//导入会员
function importData() {
	$('#importForm').form('submit', {    
	    url:$.baseUrl+'/memberGroupController/importData.do',  
	    onSubmit: function(){    
	    },    
	    success:function(result){    
	    	var data = $.parseJSON(result);
	    	$('#daoruhuiyuan').find(".detailed_close").click();
	    	$('#memberImportTable').datagrid({
				idField : 'memberId',
				data : data,
				singleSelect : false,
				pagination : true,
				pageNumber : 1,
				pageSize : 100,
				pageList : [  10, 30, 50, 100, 200, 500],
				onCheck : function(value,row,edit) {
					if(row.memberId && !importcouponMemberArr.in_array(row.memberId)){
						importcouponMemberArr.push(row.memberId);
					}
					setIssueNum(importcouponMemberArr.length);
				},
				onUncheck : function(value,row,edit) {
					if(row.memberId){
						importcouponMemberArr.remove(row.memberId);
					}
					setIssueNum(importcouponMemberArr.length);
				},
				onCheckAll:function(rows) {
					$.each(rows, function(index, item){
						if(item.memberId && !importcouponMemberArr.in_array(item.memberId)){
							importcouponMemberArr.push(item.memberId);
						}
		            });
					setIssueNum(importcouponMemberArr.length);
				},
				onUncheckAll:function(rows) {
					$.each(rows, function(index, item){
						if(item.memberId){
							importcouponMemberArr.remove(item.memberId);
						}
		            });
					setIssueNum(importcouponMemberArr.length);
				}
			});
	   }    
	});  
}

//下载模板
function downTemp() {
	window.location=$.baseUrl+'/memberGroupController/downTemp.do';
}

//控制发放券的数量
function setCouponIssueNum(val){
	var num = $("#issue_num").val();
	if(num==undefined || num==''){
		$("#issue_num").val(val);
	}else{
		$("#issue_num").val(Number(num)+Number(val));
	}
}
//设置发放券数量
function setIssueNum(val){
	$("#issue_num").val(val);
}
//校验券是否可以发放
function checkCouponIssueNum(){
	var couponNum = $("#issue_num").val();
	if(couponNum=='' || couponNum== 0){//未选择发放对象或数量
		alert("请选择发放对象!");
		return false;
	}else if(!rtNZero(couponNum)){//输入错误字符
		alert("请输入合法字符");
		return false;
	}else if(availableNum==-1){//无限数量优惠券
		return true;
	}else if(couponNum>availableNum){//发放数量大于可用数量
		alert("优惠券可用数量不够!");
		return false;
	}else{
		return true;
	}
}