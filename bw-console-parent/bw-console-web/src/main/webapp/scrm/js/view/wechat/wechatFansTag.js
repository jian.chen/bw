//存储勾选的标签id
var addTagIdArr = new Array();
//存储勾选的标签name
var addTagNameArr = new Array();
//粉丝的主键wchatId
var wechatFansIdLabel =null;
var nickNameParam ="";
var tagIdParam ="";

define(['common','selectWechatTag'], function(common) {
	// 全局变量  闭包
	window.addLabelToFan= function(value,row,index){
		 return "<a href='javacript:void(0)' onclick='showAddLabel("+row.wechatFansId+");';>自定义标签</a>";
	}
	
	window.showAddLabel = function(value){
		wechatFansIdLabel =value;
		initFansTag(value);
	}
	
	function init() {
		common.Page.resetMain();
		//初始化表格
		initDataGrid();
		initSelect();
		/*showAddLabel();*/
		
		// 新建标签框显示 
		$("#xinjianbiaoqianBtn").click(function(){
			$("#xinjianbiaoqiankuang").show();
		});
		
		// 新建标签保存按钮
		$("#saveXinjianBiaoQian").click(function(){
			$("#TagNameForm").form('submit',{
				url: $.baseUrl+'/wechatFansController/saveWechatFansLabel.do',
			    success: function(data){
			    var data = eval('(' + data + ')');
		        if (data.status == 'success'){
		        		alert("新建标签成功!");
		        		//清除表单数据
		        		$("#TagNameForm").form('clear');
		        		$("#xinjianbiaoqiankuang").hide();
		        		// 刷新页面
		        		$("#wechatFansTag").click();
		        	}    
			    }
			});
		});
		
		// 昵称查询
		$('#nickName_search_btn').click(function() {
			nickNameParam=$("#nickName").val();
			initDataGrid();
		});
		
		//标签查询
		$('#queryTag_bt-activitySetting').click(function() {
			queryTag($('#likeMemberTagName-activitySetting').val());
		});
		
		
		//保存粉丝 标签
		$('#tagSaveBtn-activitySetting').click(function() {
			var addTagIdString =null;
			for(var i=0;i<addTagIdArr.length;i++){
				if(i<addTagIdArr.length-1){
					addTagIdString +=addTagIdArr[i]+",";
				}else{
					addTagIdString +=addTagIdArr[i]
				}
			}
			addTagIdString =addTagIdString.substr(4,addTagIdString.length-1);
			// 保存粉丝标签
			$.ajax({ 
	            type: "post", 
	            url: $.baseUrl+'/wechatFansLabelController/saveFansLabel.do',
	            data:{addTagIdArr:addTagIdString,wechatFansIdLabel:wechatFansIdLabel},
	            async: false,
	            success: function (data) {
	            	var data = eval('(' + data + ')');
	            	if(data.status =='success'){
	            		// 刷新页面
		        		$("#wechatFansTag").click();
	            	}
	            }, 
	            error: function (XMLHttpRequest, textStatus, errorThrown) { 
	                    alert(errorThrown); 
	            } 
	        });
			
			//关闭弹出层
			$('#labelChoose-activitySetting').find(".detailed_close").click();
			$('#labelChooseBtn-activitySetting').val(addTagNameArr);
		});
		
		// 批量添加粉丝标签
		$("#biaoqiantianjiaul").delegate("li","click",function(){
			var tagId = $(this).attr("data-id");
			var wechatFansIds ="";
			var getSelections  = $('#fensibiaoqianTable').datagrid('getSelections');
			for(var i=0;i<getSelections.length;i++){
				if(i<getSelections.length-1){
					wechatFansIds+=getSelections[i].wechatFansId+",";
				}else{
					wechatFansIds+=getSelections[i].wechatFansId;
				}
			}
			$.ajax({ 
		        type: "post",
		        url: $.baseUrl+'/wechatFansLabelController/batchAddLabelToFans.do',
		        data:{wechatFansIds:wechatFansIds,tagId:tagId},
		        async: true,
		        success: function (data) {
		        	// 刷新页面
	        		$("#wechatFansTag").click();
		        }, 
		        error: function (XMLHttpRequest, textStatus, errorThrown) { 
		                alert(errorThrown); 
		        } 
		    });
			
		});
		
		// 标签筛选
		$("#biaoqianshuaixuanul").delegate("li","click",function(){
			$("#showTagName").val($(this).text());
			tagIdParam = $(this).attr("data-id");
			initDataGrid();
		});
		
	}
	return {
		init: init
	}
	
	// 初始化dataGrid
	function initDataGrid(){
		$('#fensibiaoqianTable').datagrid({
			url : $.baseUrl+'/wechatFansLabelController/showWechatFansLabelList.do',
			method :'post',// 请求类型 
			queryParams:{nickName:nickNameParam,LabelId:tagIdParam},
			singleSelect : false,
			loadMsg:"数据加载中...",
			onLoadSuccess : function(result) {
			},
			onLoadError : function() {
				
			}
		});
	}
	
});