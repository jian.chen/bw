define(['common'], function(common) {
	
	function init(){
		initEdit();
		$("#editTopic").click(function(){
			var optionName = $("#optionNameEdit").val();
			var optionImg = $("#optionImgEdit").val();
			var optionDetail = $("#optionDetailEdit").val();
			var optionsId = selectVoteOptionsId;
			if(optionName == ""){
				$("#optionName").alert("不能为空");
				return;
			}
			$.ajax({
				type : "post",
				url : $.baseUrl + "/VoteController/updateOptions",
				dataType : "json",
				data : {
					optionName : optionName,
					optionImg : optionImg,
					optionDetail : optionDetail,
					optionsId : optionsId
				},success : function(data){
					alert(data.message);
					$(".detailed_close").closest('.mask, .mask_in').hide();
					$('#voteOptionsTable').datagrid('reload');
				}
			})
		})
		
	}
	
	return {
		init: init
	}
	
});

function initEdit(){
	$.ajax({
		type : "post",
		async:false,
		url : $.baseUrl + "/VoteController/findOptionsById",
		dataType : "json",
		data : {
			optionsId : selectVoteOptionsId
		},success : function(data){
			$("#optionNameEdit").val(data.result.optionName);
			$("#optionImgEdit").val(data.result.optionImg);
			$("#optionDetailEdit").val(data.result.optionDetail);
		}
	})
	
}
