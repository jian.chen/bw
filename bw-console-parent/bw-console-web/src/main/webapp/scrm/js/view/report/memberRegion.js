define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		$("#daochu").click(function(){
			location.href = $.baseUrl + '/ReportController/leadToExcelMemberRegion';
		});
		
	}
	
	function initDataGrid(){
		$('#regionRateTable').datagrid({
			url : $.baseUrl+'/ReportController/showMemberRegion',
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
				
			}
		})
	}
	
	window.rowformater_region = function (value,row,index){
		var a = row.memberId * 100 / row.allMemberAccount;
		var b = a.toFixed(2);
		var rate = b + "%";
		return rate;
	}
	
	window.rowformater_city = function (value,row,index){
		if(row.geoName == null){
			return "其他";
		}else{
			return row.geoName;
		}
	}
	
	return {
		init: init
	}
	
});