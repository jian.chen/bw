define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		var editor = CKEDITOR.replace('editor01');
//		$("#add_image").uploadPreview({ Img: "add_img1_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
//		$('#summernote').summernote();
		$("#back").click(function(){
			common.Page.loadCenter('message/messageCenterManager.html');
		});
		
		initData();
		
		function initData(){
			$.ajax({
//		      cache: true,
				type: "get",
				url : $.baseUrl+'/memberMessageController/showObj.do',
				data: "id="+messageIds,
				dataType : "json",
				success : function(result) {
					$('#detail_messageName').val(result.message.title);
					$('#detail_img1_path').attr('src',result.message.messageImg1);
//					$('.note-editable').html(result.message.messageContent);
					setTimeout(function(){
						editor.setData(result.message.messageContent);
					 },500);
					console.log(result.message.messageContent);
//					$('#editor01').show();
				},
				error : function() {
				}
			});
		}
	}
	return {
		init: init
	}
	
});



