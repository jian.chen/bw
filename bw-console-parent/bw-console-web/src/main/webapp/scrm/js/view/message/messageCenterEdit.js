define(['common'], function(common) {
	function init() {
		var editor = CKEDITOR.replace('editor02');
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		$("#edit_image").uploadPreview({ Img: "edit_img1_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		
		$("#messageSaveBtn").click(function(){
			// 获取富文本编辑器的值
			console.log("data:"+editor.getData());
			$('#edit_messageContent').val(editor.getData());
			messageEdit();
		});
		
		$("#back").click(function(){
			common.Page.loadCenter('message/messageCenterManager.html');
		});
		
		$('#edit_image').change(function() {
			$("#edit_fileSpan").text($(this).val());
		});
		initData();
		
		function messageEdit(){
			if(checkMessageForm()){
			$("#dataLoad").show();
				$.ajax({
					type: "POST",
					url : $.baseUrl+'/memberMessageController/updateMessage.do',
					data:$('#edit_messageForm').serialize(),
					dataType : "json",
					success : function(result) {
						$("#dataLoad").hide();
						if(result.status==200){
							alert(result.message);
							common.Page.loadCenter('message/messageCenterManager.html');
							
						}else{
							alert(result.message);
						}
					},
					error : function() {
					}
				});
				
			}
		}
		
		function initData(){
			$.ajax({
//		      cache: true,
				type: "get",
				url : $.baseUrl+'/memberMessageController/showObj.do',
				data: "id="+messageIds,
				dataType : "json",
				success : function(result) {
					$('#edit_messageName').val(result.message.title);
					$('#memberMessageId').val(result.message.memberMessageId);
					$('#edit_img1').val(result.message.messageImg1);
					$('#edit_img1_path').attr('src',result.message.messageImg1);
//					$("textarea[name='editor01']").val(result.message.messageContent);
					setTimeout(function(){
						editor.setData(result.message.messageContent);
					 },500);
					console.log(result.message.messageContent);
//					$('#editor01').show();
				},
				error : function() {
				}
			});
		}
	}
	return {
		init: init
	}
	
});


function checkMessageForm(){
	if($('#edit_messageName').val()==null || $('#edit_messageName').val()==''){
		alert("标题不能为空");
		return false;
	}else if($('#edit_img1').val()==null || $('#edit_img1').val()==''){
		alert("封面图像不能为空");
		return false;
	}else{
		return true;
	}
}
