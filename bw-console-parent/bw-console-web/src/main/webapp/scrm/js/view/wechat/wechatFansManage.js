define(['common'], function(common) {
	// 全局变量  
	window.showFansDetail= function(value,row,index){
		 return "<a href='javascript:void(0)' onclick='showFansDetailFrame("+row.wechatFansId+");';>查看</a>";
	}
	
	window.timeformatstr =function(val,row,index) {
		if(val==undefined || val ==''){
			return ''
		}else{
			return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + ' ' +val.substring(8, 10)+':'+val.substring(10,12)+':'+val.substring(12,14) ;
		}
	}
	
	// 弹出框
	window.showFansDetailFrame = function(value){
		$('#fensixiangqingform').form('submit',{
			url: $.baseUrl+'/wechatFansController/showWechatFansDetail.do',
			onSubmit: function(param){
			        param.wechatFansId = value;    
			    },  
			success: function(data){
				var data = eval('(' + data + ')');  // change the JSON string to javascript object
		        if (data.status == 'success'){
		        	// 填充form表单
		        	showFansDetailForm(data);
		        }    
			}
		});
		$('#fensixiangqing').show();
	}
	
	function init() {
		common.Page.resetMain();
		initDate();
		initSelect();
		initInput();
		//初始化表格
		initDataGrid();
		// 查看
		$('#fensixiangqingBtn').click(function() {
			// 清除表单数据
			var selected  = $('#fensiguanliTable').datagrid('getSelected');
			if(selected){
				wechatFansId = selected.wechatFansId;
				$('#fensixiangqingform').form('submit',{
					url: $.baseUrl+'/wechatFansController/showWechatFansDetail.do',
					onSubmit: function(param){
					        param.wechatFansId = wechatFansId;    
					    },  
					success: function(data){
						var data = eval('(' + data + ')');  // change the JSON string to javascript object
				        if (data.status == 'success'){
				        	// 填充form表单
							showFansDetailForm(data);
				        }    
					}
				});
						
			}else{
				alert("请选择粉丝");
			}
			$('#fensixiangqing').show();
		});
		
		// 查询
		$("#queryButton").click(function(){
			initDataGrid();
		});
		
		//区域门店弹框
		$('#equalOrgId').click(function() {
			initTreeData("areaChoose","areaTree","/orgController/testTree.do","1");
			checkNode(stringToArr(couponRange),"areaTree");
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTree",1);
			var selectNames= getSelectName("areaTree",1);
			$("[name='equalOrgId']").val(selectIds);
			$("[name='orgName']").val(selectNames);
			$('#areaChoose').find(".detailed_close2").click();
		});
		

		function stringToArr(str){
			var arr = new Array();
			arr = str.split(",");
			return arr;
		}
		
	}
	return {
		init: init
	}
	
	
	var wechatFansId =null;
	//初始加载表格
	function initDataGrid(){
		$('#fensiguanliTable').datagrid({
			url : $.baseUrl+'/wechatFansController/showWechatFansList.do',
			method :'post',// 请求类型 
			singleSelect : true,
			loadMsg:"数据加载中...",
		    queryParams :serializeObject($('#fensiguanliForm')),
				onLoadSuccess : function(result) {
						
				},
				onLoadError : function() {
					
				}
			});
		}
	/**
	 * 展示粉丝详情
	 */
	function showFansDetailForm(data){
		$("#headImgUrl").attr("src",data.wechatInfo.headImgUrl);
		$("#openId").html(data.wechatInfo.openid);
		$("#nickName").html(data.wechatInfo.nickName);
		$("#sex").html(data.wechatInfo.sex);
		$("#country").html(data.wechatInfo.country);
		$("#province").html(data.wechatInfo.province);
		$("#city").html(data.wechatInfo.cty);
		$("#isFans").html(data.wechatInfo.isFans);
		$("#attentionTime").html(getDateString(data.wechatInfo.attentionTime));
		$("#cancelAttentionTime").html(getDateString(data.wechatInfo.cancelAttentionTime));
	}
	
	// 
	function showFansInfo(id){
		alert(id);
	}
	
	
	
});