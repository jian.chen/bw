define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initMemberPrismDataGrid();
		
		
	}
	
	return {
		init: init
	}
	
});
var selectPrismId;

function initMemberPrismDataGrid(){
	$('#memberPrismTable').datagrid( {
		url : $.baseUrl+'/PrismController/showMemberPrismList',
		idField : 'prismId',
		onLoadSuccess : function(result) {
			
		},
		onLoadError : function() {
			
		}
	});
}