define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		//初始化表格&&下拉框&&日期&&输入框验证
		initSelect();
		initDate();
		initInput();
		showSmsTemp();
		
		//通道下拉框初始化&&会员分组下拉框初始化&初始化参数下拉框
		initParams();
		
		$("#goback").click(function(){
			common.Page.loadCenter('base/smsManage.html');
		})
		
		//保存编辑or新建信息
		$('#saveTemp_Detail').click(function(){
			if(type == 'save'){
				saveSmsTemp();
			}else{
				editSmsTemp();
			}
		});
		
		
	}
	
	function initParams(){
		var _pp = $('#paramsId').next("ul");
		var value = 'smsTempParamsCode';
		var text = 'smsTempParamsName';
		$.ajax({ 
	        type: "POST", 
	        url: $.baseUrl+"/smsTempController/showParsmsList.do", 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$.each(data.obj, function(i, v){
	        		eval("var aa = v."+value+"");
	        		eval("var bb = v."+text+"");
	        		_pp.append("<li data-id='"+aa+"'><a href='#' onclick='javascript:selectParams(&quot;"+aa+"&quot;);'>"+bb+"</a></li>");
			    });
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        }
	    });
	}
	
	function showSmsTemp(){
		if(type == 'show'){
			$("#smsTemp_title").html('短消息模板详情');
			$("#saveTemp_Detail").hide();
			disableForm('smsTempDetailForm',true);//表单禁用
		}
		if(type == 'edit'){
			$("#smsTemp_title").html('短消息模板编辑');
			$("#saveTemp_Detail").show();
			disableForm('smsTempDetailForm',false);//表单启用
		}
		if(type == 'save'){
			$("#smsTemp_title").html('新建短消息模板');
			$(':input','#smsTempDetailForm').val('');//清空表单
			$("#saveTemp_Detail").show();
			disableForm('smsTempDetailForm',false);//表单启用
			return;
		}
		
		$.ajax({
			async:false,
			url:$.baseUrl+'/smsTempController/showSmsTempDetail.do',
			type:"POST",
			data:"smsTempId="+smsTempId,
			dataType: "json",
			success:function(result){
				if(result.status == '200'){
					result = result.obj;
					$('#smsTempName_detail').val(result.smsTempName);
					$("#smsTempName_detail").attr('readonly',true);
					
					$('#statusId_Detail').val(result.statusId);
					$('#statusId_Detail').next().val($.Sms_Temp_Status.getText(result.statusId));
					$('#content_Detail').val(result.content);
					
					$('#smsTempId_detail').val(smsTempId);
					
					$('#smsTempDetail').show();
				}else{
					alert('出错了！');
				}
			}
		});
	}
	

	//编辑保存
	function editSmsTemp(){
		$.ajax({
			url:$.baseUrl+'/smsTempController/editSmsTemp.do',
			type:"POST",
			data:serializeObject($('#smsTempDetailForm')),
			dataType: "json",
			success:function(result){
				if(result.status == '200'){
					$.messager.alert("提示","编辑成功");
					common.Page.loadCenter('base/smsManage.html');
				}
			}
		});
	}

	//新建保存
	function saveSmsTemp(){
		$.ajax({
			url:$.baseUrl+'/smsTempController/saveSmsTemp.do',
			type:"POST",
			data:serializeObject($('#smsTempDetailForm')),
			dataType: "json",
			success:function(result){
				if(result.status == '200'){
				}
			}
		});
	}

	return {
		init: init,
		getSmsTempId : function(){
			return smsTempId;
		},
		getType : function(){
			return type;
		}
	}
});

function selectParams(code){
	var content = $("#content_Detail").val();
	$("#content_Detail").val(content+"\{"+code+"\}");
}

