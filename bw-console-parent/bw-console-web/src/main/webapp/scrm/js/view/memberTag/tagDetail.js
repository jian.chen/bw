define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		if(isAll=='1'){
			$('#memberTagItemRemoveBtn').hide();
		}else{
			$('#memberTagItemRemoveBtn').show();
		}
		
		$('#memberTagDetailSearchBtn').click(function() {
			$('#memberTagDetailTable').datagrid('load', serializeObject($('#memberTagDetailForm')));
		});
		
		//删除
		$('#memberTagItemRemoveBtn').click(function() {
			var selected  = $('#memberTagDetailTable').datagrid('getSelected');
			var memberId = '';
			if(selected){
				memberId = selected.memberId;
			}else{
				alert("请选择标签");
				return;
			}
			//查询详情页面赋值
			removeMemberTagItem(memberTagId,memberId);
		});
		
		initTagDataGrid();
		
	}
	return {
		init: init
	}
});

var memberTagId;

function initTagDataGrid(){
	//初始化table
	$('#memberTagDetailTable').datagrid( {
		url : $.baseUrl+'/memberTagController/showMemberListOfTag.do?memberTagId='+memberTagId,
		/*
		queryParams : {
		},
		*/
		idField : 'memberId',
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [ 1, 10, 20, 30, 40, 50 ],
	    queryParams : serializeObject($('#memberTagDetailForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
	
}

function removeMemberTagItem(memberTagId,memberId){
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberTagController/removeMemberTagItem.do?memberTagId='+memberTagId+'&memberId='+memberId, 
        dataType: "json",
        async: false,
        success: function (data) {
	        if(data.status=='200'){
	    		$('#memberTagDetailTable').datagrid('load', {});
	    	}else{
	    		alert(data.message);
	    	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}