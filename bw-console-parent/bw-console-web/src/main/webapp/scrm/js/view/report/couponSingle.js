define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格&下拉框&日期&输入框验证
		initDataGrid();
		initSelect();
		
		$("#searchCS").click(function(){
			
			if($("#seachTime").val() == null || $("#seachTime").val() == ''){
				$("#seachTime").alert("请选择时间！");
			}else{
				$('#couponSingleTable').datagrid('load', serializeObject($('#couponSingleForm')));
			}
		});
		
		$("#daochuCS").click(function(){
			location.href = $.baseUrl + '/CouponSingleSummaryDayController/leadToExcelCouponSingle?startDate='+$("#seachTime").val();
		})
		
	}

	function initDataGrid(){
		$('#couponSingleTable').datagrid( {
			url : $.baseUrl+'/CouponSingleSummaryDayController/showListByMonth.do',
				onLoadSuccess : function(result) {
				},
				onLoadError : function() {
				}
			})
	}
	
	window.accountForCS = function(value,row,index){
		if(row.issueCount == '0'){
			return '--';
		}else if(row.issueCount == row.useCount){
			return '100%';
		}
		var a = row.useCount / row.issueCount;
		var b = a.toFixed(4);
		var rate2 = b.slice(2,4)+"."+b.slice(4,6)+"%";
		return rate2;
	}
	
	
	return {
		init: init
	}

});

