define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initDashBoard();
		
		$("#dashSave").click(function(){
			var str = [];
            $("input[name='dashCheck']:checkbox").each(function(){ 
                if($(this).prop("checked")){
                    str.push($(this).val());
                }
            })
			$.ajax({
				type : "post",
				url : $.baseUrl + "/sysUserController/updateUserChart",
				dataType : "json",
				data : {
					selectList : JSON.stringify(str)
				},
				success : function(data){
					$(".detailed_close").closest('.mask, .mask_in').hide();
					$.messager.alert("提示",data.result);
					reloadDashBoard(function(){
						$("#add_dashboard").click(function(){
							common.Page.loadMask('sys/dashboardAdd.html');
						})
					});
				}
			})
		})
		
		function initDashBoard(){
			for(var i = 0;i < selectDash.length; i++){
				$("input[name='dashCheck']:checkbox").each(function(){ 
	                if($(this).attr("value") == selectDash[i]){
	                	$(this).prop("checked","checked")
	                }
	            })
			}
		}
		
	}
	return {
		init: init
	}
});