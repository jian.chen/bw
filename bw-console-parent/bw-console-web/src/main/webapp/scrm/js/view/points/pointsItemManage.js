define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格&下拉框&日期&输入框验证
		initDataGrid();
		initDate();
		initSelect();
		
		//初始化积分类型下拉框
		initSelectById('selectId','/pointsItemController/showPointsTypeList.do','pointsTypeId','pointsTypeName');
		
		$("#seachPoints").click(function(){
			$('#pointsItemTable').datagrid('load', serializeObject($('#pointsItemForm')));
		});


	}
	
	function initDataGrid(){
		$('#pointsItemTable').datagrid( {
			url : $.baseUrl+'/pointsItemController/showListByExample.do',
			idField : 'memberPointsItemId',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}

	return {
		init: init
	}

});

