define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		loadThisMask('wechat/messageInteract.html');
		$('#gongzhonghaoguanlixiaoxi_tab li').click(function(){
			var url = $(this).data('url');
			$(this).addClass('active').siblings().removeClass('active');
			loadThisMask(url);
		});
	};

	function loadThisMask(url) {
		$('#gongzhonghaoguanlixiaoxiPanel').show().panel({
			href: url
		});
	};
	return {
		init: init
	}
});