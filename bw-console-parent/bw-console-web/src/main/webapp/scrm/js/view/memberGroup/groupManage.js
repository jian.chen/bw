define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		//initSelectById("testSelect","/memberGroupController/showMemberGroupListSelect.do","memberGroupId","memberGroupName");
		initDate();
		initMemberGroupDataGrid();
		$('#memberGroupDetailBtn').click(function() {
			var selected  = $('#memberGroupTable').datagrid('getSelected');
			if(selected){
				memberGroupId = selected.memberGroupId;
				isAll = selected.isAll;
				isAuth("/memberGroupController/showMemberListOfGroup.do",function(){
					common.Page.loadMask('memberGroup/groupDetail.html');
				});
			}else{
				alert("请选择分组");
				return;
			}
			
		});
		$('#memberGroupAddBtn').click(function() {
			isAuth("/memberGroupController/saveMemberGroup.do",function(){
				common.Page.loadMask('memberGroup/groupAdd.html');
			});
		});
		$('#memberGroupEditBtn').click(function() {
			var selected  = $('#memberGroupTable').datagrid('getSelected');
			if(selected){
				memberGroupId = selected.memberGroupId;
				isAuth("/memberGroupController/editMemberGroup.do",function(){
					common.Page.loadMask('memberGroup/groupEdit.html');
				});
			}else{
				alert("请选择分组");
				return;
			}
		});
		
		//删除
		$('#memberGroupDeleteBtn').click(function() {
				var selected  = $('#memberGroupTable').datagrid('getSelected');
				if(selected){
					memberGroupId = selected.memberGroupId;
				}else{
					alert("请选择分组");
					return;
				}
				//查询详情页面赋值
				if (confirm('您确认想要删除选中记录吗？')){ 
					removeMemberGroup(memberGroupId);
				}
		});
		
		//执行
		$('#memberGroupExcuteBtn').click(function() {
			var selected  = $('#memberGroupTable').datagrid('getSelected');
			if(selected){
				memberGroupId = selected.memberGroupId;
			}else{
				alert("请选择分组");
				return;
			}
			if(!selected.conditionSql){
				alert("该分组未设置计算规则，不能执行！");
				return;
			}else{
				if(confirm("确定执行分组吗？")){
					 excuteMemberGroupSql(memberGroupId);
				 }
			}
			
		});
		
		$('#memberGroupAddBatchBtn').click(function() {
			var selected  = $('#memberGroupTable').datagrid('getSelected');
			if(selected){
				memberGroupId = selected.memberGroupId;
				isAll = selected.isAll;
				if(isAll=="1"){
					alert("该分组已包含全部会员");
					return;
				}
				isAuth("/memberGroupController/saveMemberGroupItem.do",function(){
					common.Page.loadMask('memberGroup/groupAddBatch.html');
				});
			}else{
				alert("请选择分组");
				return;
			}
			
		});
		
		$('#memberGroupSearchBtn').click(function() {
			$('#memberGroupTable').datagrid('load', serializeObject($('#memberGroupForm')));
		});
		
	}
	return {
		init: init
	}
});

var memberGroupId;
var isAll;

function initMemberGroupDataGrid(){
	//初始化table
	$('#memberGroupTable').datagrid( {
		url : $.baseUrl+'/memberGroupController/showMemberGroupList.do',
		/*
		queryParams : {
		},
		*/
		idField : 'memberGroupId',
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [ 1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 ],
	    queryParams : serializeObject($('#memberGroupForm')),
			onLoadSuccess : function(result) {
				if(result) {
	            }
			},
			onLoadError : function() {
				
			}
		});
}

function removeMemberGroup(memberGroupId){
	var url = '/memberGroupController/removeMemberGroup.do?memberGroupId='+memberGroupId;
	isAuthForAjax(url,"get","","json",{},function(data){
        if(data.status=='200'){
    		$('#memberGroupTable').datagrid('load', {});
    		//清除所有勾选的行
			$("#memberGroupTable").datagrid('clearChecked');
    	}else{
    		alert(data.message);
    	}
	},function(data){
		$.messager.alert("提示","error!");
	});
	
	/*$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberGroupController/removeMemberGroup.do?memberGroupId='+memberGroupId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("#dataLoad").hide();
	        if(data.status=='200'){
	    		$('#memberGroupTable').datagrid('load', {});
	    		//清除所有勾选的行
				$("#memberGroupTable").datagrid('clearChecked');
	    	}else{
	    		alert(data.message);
	    	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });*/
}

function excuteMemberGroupSql(memberGroupId){
	var url = '/memberGroupController/excuteMemberGroupSql.do?memberGroupId='+memberGroupId;
	isAuthForAjax(url,"post","","json",{},function(data){
		$.messager.alert("提示",data.message);
	},function(){
		$.messager.alert("提示","error!"); 
	});
	
	/*$.ajax({ 
        type: "post", 
        url: $.baseUrl+'/memberGroupController/excuteMemberGroupSql.do?memberGroupId='+memberGroupId, 
        dataType: "json",
        async: false,
        beforeSend: function (xhr) {
		},
        success: function (data) {
	        alert(data.message);
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });*/
}