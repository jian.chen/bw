define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		//initSelectById("testSelect","/memberTagController/showMemberTagListSelect.do","memberTagId","memberTagName");
		initDate();
		initMemberTagDataGrid();
		$('#memberTagDetailBtn').click(function() {
			var selected  = $('#memberTagTable').datagrid('getSelected');
			if(selected){
				memberTagId = selected.memberTagId;
				isAll = selected.isAll;
				isAuth("/memberTagController/showMemberListOfTag.do",function(){
					common.Page.loadMask('memberTag/tagDetail.html');
				});
			}else{
				alert("请选择标签");
				return;
			}
			
		});
		$('#memberTagAddBtn').click(function() {
			isAuth("/memberTagController/saveMemberTag.do",function(){
				common.Page.loadMask('memberTag/tagAdd.html');
			});
		});
		$('#memberTagCheckBtn').click(function() {
			var selected  = $('#memberTagTable').datagrid('getSelected');
			if(selected){
				memberTagId = selected.memberTagId;
				isAuth("/memberTag/tagCheck.html",function(){
					common.Page.loadMask('memberTag/tagCheck.html');
				});
			}else{
				alert("请选择标签");
				return;
			}
		});
		$('#memberTagEditBtn').click(function() {
			var selected  = $('#memberTagTable').datagrid('getSelected');
			if(selected){
				memberTagId = selected.memberTagId;
				if(selected.executeType == 1){
					isAuth("/memberTagController/updateMemberTag.do",function(){
						common.Page.loadMask('memberTag/tagEdit.html');
					});
				}else{
					alert("此标签有条件，不能编辑");
				}
			}else{
				alert("请选择标签");
				return;
			}
		});
		
		//删除
		$('#memberTagDeleteBtn').click(function() {
			    var selected  = $('#memberTagTable').datagrid('getSelected');
			    if(selected){
			    	memberTagId = selected.memberTagId;
			    }else{
			    	alert("请选择标签");
			    	return;
			    }
			    //查询详情页面赋值
			    if (confirm('您确认想要删除选中记录吗？')){
			    	removeMemberTag(memberTagId);
				}    
		});
		
		//执行
		$('#memberTagExcuteBtn').click(function() {
			var selected  = $('#memberTagTable').datagrid('getSelected');
			if(selected){
				memberTagId = selected.memberTagId;
			}else{
				alert("请选择标签");
				return;
			}
			if(selected.executeType != "2"){
				alert("该标签没有条件,不能执行！");
				return;
			}else if(selected.updateType != "1"){
				alert("该标签不能手动执行！");
				return;
			}else{
				if(confirm("确定执行标签吗？")){
					excuteMemberTagSql(memberTagId);
				 }
			}
			
			$('#memberTagTable').datagrid('load');
		});
		
		$('#memberTagAddBatchBtn').click(function() {
			isAuth("/memberTagController/saveMemberTagItem.do",function(){
				var selected  = $('#memberTagTable').datagrid('getSelected');
				if(selected){
					memberTagId = selected.memberTagId;
					
					isAll = selected.isAll;
					if(isAll=="1"){
						alert("该标签已包含全部会员");
						return;
					}
					if(selected.executeType != "1"){
						alert("该标签有条件，不能手动添加会员");
						return;
					}
					common.Page.loadMask('memberTag/tagAddBatch.html');
				}else{
					alert("请选择标签");
					return;
				}
			})
		});
		
		$('#memberTagSearchBtn').click(function() {
			$('#memberTagTable').datagrid('load', serializeObject($('#memberTagForm')));
		});
		
	}
	return {
		init: init
	}
});

var memberTagId;
var isAll;

function initMemberTagDataGrid(){
	//初始化table
	$('#memberTagTable').datagrid( {
		url : $.baseUrl+'/memberTagController/showMemberTagList.do',
		/*
		queryParams : {
		},
		*/
		idField : 'memberTagId',
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [ 1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 ],
	    queryParams : serializeObject($('#memberTagForm')),
			onLoadSuccess : function(result) {
				if(result) {
	            }
			},
			onLoadError : function() {
				
			}
		});
}

function removeMemberTag(memberTagId){
	var url = '/memberTagController/removeMemberTag.do?memberTagId='+memberTagId;
	isAuthForAjax(url,"get","","json",{},function(data){
        if(data.status=='200'){
    		$('#memberTagTable').datagrid('load', {});
    		//清除所有勾选的行
			$("#memberTagTable").datagrid('clearChecked');
    	}else{
    		alert(data.message);
    	}
	},function(data){
		alert("error!");
	});
	
	/*$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberTagController/removeMemberTag.do?memberTagId='+memberTagId, 
        dataType: "json",
        async: false,
        success: function (data) {
	        if(data.status=='200'){
	    		$('#memberTagTable').datagrid('load', {});
	    		//清除所有勾选的行
				$("#memberTagTable").datagrid('clearChecked');
	    	}else{
	    		alert(data.message);
	    	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });*/
}

function excuteMemberTagSql(memberTagId){
	/*var url = '/memberTagController/excuteMemberTagManel.do?memberTagId='+memberTagId;
	isAuthForAjax(url,"post","","json",{},function (data) {
        $.messager.alert("提醒",data.message);
    },function(){
    	$.messager.alert("提醒","error!");
    });*/
	$.ajax({ 
        type: "post", 
        url: $.baseUrl+'/memberTagController/excuteMemberTagManel.do?memberTagId='+memberTagId, 
        dataType: "json",
        async: true,
        success: function (data) {
	        $.messager.alert("提醒",data.message);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
        	$.messager.alert(errorThrown); 
        } 
    });
}