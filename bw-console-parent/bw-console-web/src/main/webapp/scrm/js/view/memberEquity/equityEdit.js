define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		$("#equityEdit_image").uploadPreview({ Img: "equityEdit_img_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		$('#equityEdit_image').change(function() {
			if($(this).val() != ""){	
				$("#equityEdit_fileSpan").text($(this).val());
				//选择的图片存放在隐藏域，用于图片上传验证
				$("#equityEdit_filePath").val($(this).val());
				//进行提示
				$("#equityEdit_hint").text("未上传");
				//清空当前存在的图片路径
				$("#equityEdit_img").val("");
			}
		});
		
		//权益基本信息
		showMemberEquityDetail();
	}
	return {
		init: init
	}
});


function showMemberEquityDetail(){
	$.ajax({ 
        type: "post", 
        url: $.baseUrl+'/memberEquityController/showMemberEquityDetail.do?', 
        dataType: "json",
        async: false,
        data : {
        	memberEquityId : memberEquityId
        },
        success: function (data) {
        	var span = "";
        	
        	$("#member_EquityId").val(data.memberEquity.memberEquityId);
        	$("[name='equityName']").val(data.memberEquity.equityName);
        	$("[name='equityContent']").val(data.memberEquity.equityContent);
        	$("#equity_Starttime").val(dateDateformat(data.memberEquity.equityStarttime));
			$("#equity_Endtime").val(dateDateformat(data.memberEquity.equityEndtime));
        	
        	for(var i = 0; i < data.gradeList.length; i++){
        		if(data.egRelationList.length > 0){
        			for(var j = 0; j< data.egRelationList.length; j++){
        				if(data.egRelationList[j].gradeId == data.gradeList[i].gradeId){
        					span += "<span>";
        					span += "<input type='checkbox' name='gradeId_"+i+"' value='"+data.gradeList[i].gradeId+"' checked='checked'> "+data.gradeList[i].gradeName;
        					span += "</span>";
        					break;
        				}
        				if(j==data.egRelationList.length-1){
        					span += "<span>";
        					span += "<input type='checkbox' name='gradeId_"+i+"' value='"+data.gradeList[i].gradeId+"' > "+data.gradeList[i].gradeName;
        					span += "</span>"; 
        				}
        			}        			
        		}else{
        			span += "<span>";
					span += "<input type='checkbox' name='gradeId_"+i+"' value='"+data.gradeList[i].gradeId+"' > "+data.gradeList[i].gradeName;
					span += "</span>";
        		}
        	}
        	$("#grade_div").append("");
        	$("#grade_div").append(span);
        	
        	$("#equityEdit_img").val(data.memberEquity.equityImg);
        	$("#equityEdit_img_path").attr("src",data.memberEquity.equityImg);
        	
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

//检查表单
function checkForm(page){
	var isValid = formCheck($('#'+page+'_form'));
	return isValid;
}

//保存
function equityEdit(page){
	
	if($("#equity_Starttime").val() != ""){
		if($("#equity_Endtime").val() == ""){
			alert("请选择权益的截止日期");
			return;			
		}
	}
	if($("#equity_Endtime").val() != ""){
		if($("#equity_Starttime").val() == ""){
			alert("请选择权益的开始日期");
			return;			
		}
	}
	
	//选择图片未上传验证
	if($("#equityEdit_filePath").val() != ""){
		if($("#"+page+"_img").val() == ""){
			alert("选择的图片未进行上传");
			return;			
		}
	}
	
	
	//获取checkbox选择的值
	var gradeids ;
	var i = 0;
	$("input[name^='grade']:checked").each(function(){
		if(i==0){
			gradeids = this.value;
		}else{
			gradeids = gradeids+","+this.value;			
		}
		i++;
	});
    $("#gradeIds").val(gradeids); 
	
	if(checkForm(page)){
		$.ajax({
			type: "POST",
			url : $.baseUrl+'/memberEquityController/editEquity.do',
			data:$('#equityEdit_form').serialize(),
			async: false,
			dataType : "json",
			success : function(result) {
				//result = eval('(' + result + ')');
				alert(result.message);
				$('#memberEquityTable').datagrid('reload'); 
				$('.mask, .mask_in').hide();
			},
			error : function() {
				parent.layer.alert("出错了:(");
			}
		});	
	}
	
}	

