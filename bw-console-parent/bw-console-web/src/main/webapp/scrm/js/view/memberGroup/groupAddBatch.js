define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		addTagIdArr = [];
		addTagNameArr = [];
		importMemberArr = [];
		memberArr = [];
		
		$("[name='memberRadio']").click(function() {
			var val = $(this).val();
			if(val==1){
				$("#queryMemberDiv").show();
				$("#importMemberDiv").hide();
			}else if(val==2){
				$("#queryMemberDiv").hide();
				$("#importMemberDiv").show();
			}else if(val==3){
				$("#queryMemberDiv").hide();
				$("#importMemberDiv").hide();
			}
		});
		$("#importMemberDiv").hide();
		
		initSelect();
		initInput();
		
		/*
		 * 表格查询绑定
		 */
		$('#search_btn').click(function() {
			$('#memberTable').datagrid( {
				url : $.baseUrl+'/memberController/showMemberList.do',
				idField : 'memberId',
				singleSelect : false,
				pagination : true,
				pageNumber : 1,
				pageSize : 100,
				pageList : [  10, 30, 50, 100, 200, 500],
			    queryParams : serializeObject($('#memberSearchForm')),
				onLoadSuccess : function(result) {
					if(result) {
		            }
				},
				
				onLoadError : function() {
					
				},
				onCheck : function(value,row,edit) {
					if(!memberArr.in_array(row.memberId)){
						memberArr.push(row.memberId);
					}
				},
				onUncheck : function(value,row,edit) {
					memberArr.remove(row.memberId);
				},
				onCheckAll:function(rows) {
					$.each(rows, function(index, item){
						if(!memberArr.in_array(item.memberId)){
							memberArr.push(item.memberId);
						}
		            });
				},
				onUncheckAll:function(rows) {
					$.each(rows, function(index, item){
						memberArr.remove(item.memberId);
		            });
				}
			});
			
			
		});
		
		/*
		 * 标签弹出层开始
		 */
		$('#labelChooseBtn').click(function() {
			$('#labelChoose').show();
			queryTag($('#likeMemberTagName').val());
		});
		$('#queryTag_bt').click(function() {
			queryTag($('#likeMemberTagName').val());
		});
		$('#tagSaveBtn').click(function() {
			//$(".detailed_close").closest('.mask, .mask_in').hide();
			$('#labelChoose').find(".detailed_close").click();
			if(addTagIdArr.length>0){
				$("#memberTagIds").val(addTagIdArr.toString());
				$("#labelChooseBtn").val(addTagNameArr.toString());
			}else{
				$("#memberTagIds").val('');
				$("#labelChooseBtn").val('');
			}
		});
		
		//区域门店弹框
		$('#equalOrgId').click(function() {
			initTreeData("areaChoose","areaTree","/orgController/testTree.do","1");
			var ids = $("[name='equalOrgId']").val();
			if(ids){
				var idArr = ids.split(",");
				checkNode(idArr,"areaTree");
			}
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTree","1");
			var selectNames= getSelectName("areaTree","1");
			$("[name='equalOrgId']").val(selectIds);
			$("[name='equalOrgName']").val(selectNames);
			$('#areaChoose').find(".detailed_close2").click();
		});
		
		$('#fileInput').change(function() {
			$("#fileSpan").text($(this).val());
		});
		
		$('#memberImportBtn').click(function() {
			$('#daoruhuiyuan').show();
		});
		
		$('#importBtn').click(function() {
			importData();
		});
		
		//选择会员保存
		$('#saveBtn').click(function() {
			saveMemberGroupItem();
		});
		
		$('#downImportTemp').click(function() {
			downTemp();
		});
		
	}
	return {
		init: init
	}
});

//保存勾选的会员id
var memberArr =new Array();
//$("#memberTagIds").val(memberArr.toString());
//存储勾选的标签id
var addTagIdArr = new Array();
//存储勾选的标签name
var addTagNameArr = new Array();

function initDataGrid(){
	//初始化table
	$('#memberTable').datagrid( {
		url : $.baseUrl+'/memberController/showMemberList.do',
		/*
		queryParams : {
		},
		*/
		idField : 'memberId',
		singleSelect : false,
		pagination : true,
		pageNumber : 1,
		pageSize : 100,
		pageList : [  10, 30, 50, 100, 200, 500],
	    queryParams : serializeObject($('#memberSearchForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		
		onLoadError : function() {
			
		},
		onCheck : function(value,row,edit) {
			if(!memberArr.in_array(row.memberId)){
				memberArr.push(row.memberId);
			}
		},
		onUncheck : function(value,row,edit) {
			memberArr.remove(row.memberId);
		},
		onCheckAll:function(rows) {
			$.each(rows, function(index, item){
				if(!memberArr.in_array(item.memberId)){
					memberArr.push(item.memberId);
				}
            });
		},
		onUncheckAll:function(rows) {
			$.each(rows, function(index, item){
				memberArr.remove(item.memberId);
            });
		}
	});
}

function saveMemberGroupItem(){
	var val = $("[name='memberRadio']:checked").val();
	if(val==1){
		if(memberArr.length==0){
			alert("请选择会员");
			return;
		}
		$.ajax({ 
        type: "post", 
        url: $.baseUrl+'/memberGroupController/saveMemberGroupItem.do?memberGroupId='+memberGroupId +'&memberIds='+memberArr.toString(), 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("#dataLoad").hide();
	        if(data.status=='200'){
	    		alert(data.message);
	    	}else{
	    		alert(data.message);
	    	}
	    	$('#memberGroupTable').datagrid('load',{});
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
		
	}else if(val==2){
		//判断勾选的
		$.ajax({ 
	        type: "post", 
	        url: $.baseUrl+'/memberGroupController/saveMemberGroupItem.do?memberGroupId='+memberGroupId +'&memberIds='+importMemberArr.toString(), 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$("#dataLoad").hide();
		        if(data.status=='200'){
		    		alert(data.message);
		    	}else{
		    		alert(data.message);
		    	}
		    	$('#memberGroupTable').datagrid('load',{});
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        } 
    	});
		
	}else if(val==3){
		$.ajax({ 
	        type: "post", 
	        url: $.baseUrl+'/memberGroupController/saveAllMemberToGroup.do?memberGroupId='+memberGroupId, 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$("#dataLoad").hide();
		        if(data.status=='200'){
		    		alert(data.message);
		    	}else{
		    		alert(data.message);
		    	}
		    	$('#memberGroupTable').datagrid('load',{});
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        } 
    	});
	}
}
var importMemberArr = new Array();
function importData() {
	$("#dataLoad").show();
		$('#importForm').form('submit', {    
		    url:$.baseUrl+'/memberGroupController/importData.do',  
		    onSubmit: function(){    
		    },    
		    success:function(result){ 
		    	$("#dataLoad").hide();
		    	var data = $.parseJSON(result);
		    	//$('#cigfunctioncategory_index_grid').datagrid('loadData',{total:0,rows:[]});
		    	//$("#saveButton").show();
		    	$('#daoruhuiyuan').find(".detailed_close").click();
		    	//$('#memberImportTable').datagrid('loadData',data);
		    	
		    	$('#memberImportTable').datagrid({
					idField : 'memberId',
					data : data,
					singleSelect : false,
					pagination : true,
					pageNumber : 1,
					pageSize : 100,
					pageList : [  10, 30, 50, 100, 200, 500],
					onCheck : function(value,row,edit) {
						if(row.memberId && !importMemberArr.in_array(row.memberId)){
							importMemberArr.push(row.memberId);
						}
					},
					onUncheck : function(value,row,edit) {
						if(row.memberId){
							importMemberArr.remove(row.memberId);
						}
					},
					onCheckAll:function(rows) {
						$.each(rows, function(index, item){
							if(item.memberId && !importMemberArr.in_array(item.memberId)){
								importMemberArr.push(item.memberId);
							}
			            });
					},
					onUncheckAll:function(rows) {
						$.each(rows, function(index, item){
							if(item.memberId){
								importMemberArr.remove(item.memberId);
							}
			            });
					}
				});
			   }    
			});  
		
	}
//下载模板
function downTemp() {
	window.location=$.baseUrl+'/memberGroupController/downTemp.do';
}

