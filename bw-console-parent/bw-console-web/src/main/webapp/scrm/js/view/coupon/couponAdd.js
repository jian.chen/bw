define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		initDate();
		initInput();
		$('#merchat').hide();
		$("#couponAdd_image").uploadPreview({ Img: "couponAdd_img3_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		$("#couponAdd2_image").uploadPreview({ Img: "couponAdd_img2_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		//设置下一步上一步
		$("#nextSetting").click(function(){
			if($("#couponName").val() == ''){
				$.messager.alert("提示","优惠券名称不能为空");
				return;
			}
			if($("input:radio[name='expireType']:checked").val()=='1'){
				if($("#couponAdd_startDate").val()==''){
					$.messager.alert("提示","请选择过期日期开始时间!");
					return;
				}
				if($("#couponAdd_endDate").val()==''){
					$.messager.alert("提示","请选择过期日期结束时间!");
					return;
				}
			}
			
			//couponTYpe_type
			if($("#couponTYpe_type").val()==''){
				$.messager.alert("提示","请选择来源类型!");
				return;
			}
		
			$("#coupon-add-first-setting").hide();
			$("#coupon-add-second-setting").show();
			$("#div-title-first").hide();
			$("#div-title-second").show();
		});
		
		$("#beforeSetting").click(function(){
			$("#coupon-add-first-setting").show();
			$("#coupon-add-second-setting").hide();
			$("#div-title-first").show();
			$("#div-title-second").hide();
		});
		
		//区域门店弹框
		$('#equalOrgId').click(function() {
			initTreeData("areaChoose","areaTree","/orgController/testTree.do","1");
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTree",1);
			var selectNames= getSelectName("areaTree",1);
			$("[name='equalOrgId']").val(selectIds);
			$("[name='equalOrgName']").val(selectNames);
			$('#areaChoose').find(".detailed_close2").click();
		});
		$('#couponAdd_image').change(function() {
			$("#couponAdd_fileSpan").text($(this).val());
		});
		$('#couponAdd2_image').change(function() {
			$("#couponAdd2_fileSpan").text($(this).val());
		});
		
		$("input:radio[name='expireType']").change(function (){
			if(this.value=='1'){
				$("#couponAdd_afterDate").val("");
				$("#couponAdd_expireValue").val("");
				$("#couponAdd_thruDate").attr("data-options","required:true");
				$("#couponAdd_afterDate").attr("data-options","length:'3',validType:'rtZero'");
				$("#couponAdd_expireValue").attr("data-options","length:'3',validType:'rtZero'");
			}else if(this.value=='2'){
				$("#couponAdd_expireValue").val("");
				$("#couponAdd_thruDate").val("");
				$("#couponAdd_thruDate").attr("data-options","");
				$("#couponAdd_afterDate").attr("data-options","required:true,length:'3',validType:'rtZero'");
				$("#couponAdd_expireValue").attr("data-options","required:true,length:'3',validType:'rtZero'");
			}else{
				$("#couponAdd_expireValue").val("");
				$("#couponAdd_thruDate").val("");
				$("#couponAdd_thruDate").attr("data-options","");
				$("#couponAdd_afterDate").attr("data-options","");
				$("#couponAdd_expireValue").attr("data-options","");
			}
		 });
		
		$("input:radio[name='couponTypeId']").change(function (){
			if(this.value=='1'){
				$("#couponAdd_useSatisfyAmount").val("");
				$("#couponAdd_amount").val("");
				$("#couponAdd_discount").attr("data-options","required:true,validType:'ishundred'");
				$("#couponAdd_useSatisfyAmount").attr("data-options","length:'16',validType:'rtNZero'");
				$("#couponAdd_amount").attr("data-options","length:'16',validType:'rtZero'");
			}else if(this.value=='2'){
				$("#couponAdd_discount").val("");
				$("#couponAdd_discount").attr("data-options","validType:'ishundred'");
				$("#couponAdd_useSatisfyAmount").attr("data-options","required:true,length:'16',validType:'rtNZero'");
				$("#couponAdd_amount").attr("data-options","required:true,length:'16',validType:'rtZero'");
			}else if(this.value=='3'){
				$("#couponAdd_discount").val("");
				$("#couponAdd_useSatisfyAmount").val("");
				$("#couponAdd_amount").val("");
				$("#couponAdd_discount").attr("data-options","validType:'ishundred'");
				$("#couponAdd_useSatisfyAmount").attr("data-options","length:'16',validType:'rtNZero'");
				$("#couponAdd_amount").attr("data-options","length:'16',validType:'rtZero'");
			}
		});
		
		$("#goback").click(function(){
			common.Page.loadCenter('coupon/couponManage.html');
		})
		
		$("#couponAddSave").click(function(){
			if($("#isReceiveCK").is(':checked')){
				$("#isReceive").val("N");
			}else{
				$("#isReceive").val("Y");
			}
			if($("#isRemindCK").is(':checked')){
				$("#isRemind").val("Y");
			}else{
				$("#isRemind").val("N");
			}
			if($("#isOwnerUsedCK").is(':checked')){
				$("#isOwnerUsed").val("N");
			}else{
				$("#isOwnerUsed").val("Y");
			}
			if(checkForm('couponAdd')){
				$.ajax({
					type: "POST",
					url : $.baseUrl+'/couponController/insertCoupon.do',
					data:$('#couponAdd_form').serialize(),
					async: false,
					dataType : "json",
					success : function(result) {
						if(result.status == '200'){
							$.messager.alert("提示","新增成功");
							common.Page.loadCenter('coupon/couponManage.html');
						}else{
							$.messager.alert("提示","新增失败");
						}
					},
					error : function() {
						parent.layer.alert("出错了:(");
					}
				});
				
			}
		})
		
	}
	$("#begindatetime").click(function(){  
		   alert('Changed!')  
	});  
	return {
		init: init
	}
});