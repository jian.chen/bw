define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		initDate();
		initInput();
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		initCouponBusinessData();
		
		
	}
	return {
		init: init
	}
});
function initCouponBusinessData(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/couponBusinessTypeController/queryCouponBusinessTypeDetail.do',
		data : "couponBusinessTypeId="+couponIds,
		dataType : "json",
		async: false,
		success : function(result) {
			$("#couponBusinessTypeId").val(couponIds);
			$("#couponBusinessTypeName").val(result.couponBusinessType.couponBusinessTypeName);
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});
}

function couponBTEdit(page){
//	if(checkForm(page)){
		$.ajax({
//        cache: true,
			type: "POST",
			url : $.baseUrl+'/couponBusinessTypeController/editCouponBusinessType.do',
			data:$('#couponBTAdd_form').serialize(),
			async: false,
			dataType : "json",
			success : function(result) {
				result = eval('(' + result + ')');
				alert(result.message);
				$('#couponTable').datagrid('reload'); 
				$('.mask, .mask_in').hide();
			},
			error : function() {
				parent.layer.alert("出错了:(");
			}
		});
		
	}
//}
	
