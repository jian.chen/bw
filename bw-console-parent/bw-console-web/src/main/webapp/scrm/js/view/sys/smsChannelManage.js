define(['common', 'view/sys/sysManage',], function(common, sysManage) {
    var smsChannelId = null;
    var smsChannelItem = null;
	function init() {
        smsChannelItem = _.template($("#smsChannelItem").html());
		common.Page.resetMain();
		initDataGrid();
	};
	
	function initDataGrid(){
        $.ajax({
            url: baseUrl+'/smsChannelController/showSmsChannelList.do',
            type:"GET",
            dataType:"json",
            success:function(result){
                if (result != null && result.status == "200") {
                    $("#smsChannelContent").html(smsChannelItem({datas:result.rows}));
                    $("button.confBtn").click(function(){
                        var channelId = $(this).attr("data_id");
                        if (channelId == "") return;
                        isAuth('/smsChannelController/showSmsChannelById.do',function(){
                            smsChannelId = channelId;
                            $("#xitongguanliPanel").show().panel({
                                href: 'sys/smsChannelEdit.html'
                            });
                        });
                    });
                } else {
                    $.messager.alert("提示","加载失败");
                }
            }
        });
	}

    function back() {
        sysManage.setUrl('sys/smsChannelManage.html');
        common.Page.loadCenter('sys/sysManage.html');
    }

	return {
		init: init,
        getSmsChannelId: function() {
            return smsChannelId;
        },
        back: back
	}
});
