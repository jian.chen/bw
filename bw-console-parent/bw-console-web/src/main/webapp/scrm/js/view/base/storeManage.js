define(['common','view/member/areaLinkage'], function(common) {
	var type;
	var storeId;
	function init() {
		common.Page.resetMain();
		initDataGrid();
		
		$('#show').click(function(){
			if(check()==1){
				type = 'show';
				isAuth('/base/storeEdit.html',function(){
					common.Page.loadMask('base/storeEdit.html');
				});
			}else if(check()==2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		$('#edit').click(function(){
			if(check()==1){
				type = 'edit';
				isAuth('/storeController/editStore.do',function(){
					common.Page.loadMask('base/storeEdit.html');
				});
			}else if(check()==2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		$('#save').click(function(){
			type = 'save';
			isAuth('/storeController/saveStore.do',function(){
				common.Page.loadMask('base/storeEdit.html');
			});
		});
		$('#deleted').click(function(){
			if(check() != 2){
				if(confirm("确认删除？")){
					deleted();
				}
			}else{
				alert("请选择条目！");
			}
		});
		
		
	}
	
	function initDataGrid(){
		$('#storeTable').datagrid( {
			url : $.baseUrl+'/storeController/showStoreList.do',
			idField : 'storeId',
			onLoadSuccess : function(result) {
				if(result) {
					
				}
			},
			onLoadError : function() {
				
			}
		});
	}
	
	function check(){
		var selected = $('#storeTable').datagrid('getChecked');
		if(selected.length==1){
			storeId = selected[0].storeId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			return 3;
		}
		
	}
	
	function deleted(){
		var selected = $('#storeTable').datagrid('getChecked');
		var storeIds = '';
		
		if(selected.length==0){
			alert('请选择条目！');
		}else{
			for(var i=0; i<selected.length; i++){
				if (storeIds != ''){ 
					storeIds += ',';
				}
				storeIds += selected[i].storeId;
			}
		}
		
		isAuthForAjax('/storeController/removeStoreList.do',"POST","","json","storeIds="+storeIds,function(result){
			result = eval('(' + result + ')');
			if(result==1){
				alert('删除成功！');
				$("#storeTable").datagrid('load');
				//清除所有勾选的行
				$("#storeTable").datagrid('clearChecked');
			}else{
				alert('删除失败，请重试！');
			}
		
		},function(data){
			
		})
		
		/*$.ajax({
			url:$.baseUrl+'/storeController/removeStoreList.do',
			type:"POST",
			data:"storeIds="+storeIds,
			datatype:"json",
			success:function(result){
				result = eval('(' + result + ')');
				if(result==1){
					alert('删除成功！');
					$("#storeTable").datagrid('load');
					//清除所有勾选的行
					$("#storeTable").datagrid('clearChecked');
				}else{
					alert('删除失败，请重试！');
				}
			}
		
		});*/
		
	}
	return {
		init: init,
		getStoreId : function(){
			return storeId;
		},
		getType : function(){
			return type;
		}
	}
});
