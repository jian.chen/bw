var errerMessage="";
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		//初始化下拉框;
		initSelect("pointsTypeIdSelect");
		initSelect("pointsRuleTypeSelect");
		initSelect("invalidTimeTypeSelect");
		initSelect("dateType");
		//初始化日期
		initDate();
		initInput();

	
		//保存
		$('#save_btn').click(function(){
			var timeValue=$("#timeValue").val();
			var dateTypeValue=$("#dateTypeValue").val();
			$("#editInvalidTimeValue").val(timeValue+"/"+dateTypeValue);
			var editCleanTime = $("#editCleanTime_md").val()+$("#editCleanTime_ymd").val();
			editCleanTime=editCleanTime.replace(/-/g, "");
			$("#editCleanTime").val(editCleanTime);
			
			if($("#editInvalidTimeType").val() == '1'){
				$("#editInvalidTimeValue").val(timeValue+"/"+dateTypeValue+"/"+editCleanTime);
			}
			if($("#editInvalidTimeType").val() == '2'){
				$("#editInvalidTimeValue").val("//"+editCleanTime);
			}
			if($("#editInvalidTimeType").val() == '3'){
				$("#editInvalidTimeValue").val("/");
			}
			if($("#editInvalidTimeType").val() == '4'){
				$("#editInvalidTimeValue").val(timeValue+"/"+dateTypeValue);
			}
			
			var formParams = $('#save_points_rule').serialize();
			
			var ck = formCheck($("#save_points_rule"));
			if(ck){
				if($("#editPointsTypeId").val() == ''){
					$("#pointsTypeText").alert('请选择积分规则类型！');
					return;
				}
				if($("#editPointsRuleType").val() == ''){
					$("#pointsRuleTypeText").alert('请选择发放比例！');
					return;
				}
				if($("#editInvalidTimeType").val() == ''){
					$("#invalidTimeTypeText").alert('请选择积分有效类型！');
					return;
				}
				
				// 根据积分有效类型的值判定日期的选择
				if($("#editInvalidTimeType").val() !=''){
					// 固定日期+滚动日期
					if($("#editInvalidTimeType").val()==1){
						if($("#editCleanTime_md").val() == ''){
							$("#editCleanTime_md").alert("请选择积分清理日!");
							return;
						}
					}
					// 固定日期
					if($("#editInvalidTimeType").val()==2){
						if($("#editCleanTime_ymd").val()== ''){
							$("#editCleanTime_ymd").alert("请选择积分清理日!");
							return;
						}
					}
				}
				if($("#dateTypeValue").val() == '' && $("#editInvalidTimeType").val() == '4' ){
					
					$("#dateTypeText").alert('请选择时间单位！');
					return;
				}
				
				var url=$.baseUrl+"/pointsRuleController/savePointsRule.do";
				$.ajax({
					type:"POST",
					url:url,
					dataType:"JSON",
					data:formParams,
					contentType: "application/x-www-form-urlencoded; charset=utf-8", 
					success:function(result){
						var data = eval('(' + result + ')');
						alert(data.message);
						if(data.code==1){
							$("#close_btn").click();
							$("#pointsRuleTable").datagrid('load');
						}
					},
					error:function(){
						alert("request error");
					}
				});	
			}
			
		});		
		
		if(!isNulls(pointsRuleId)){
    		var url=$.baseUrl+"/pointsRuleController/pointsRuleDetail.do?pointsRuleId="+pointsRuleId;
        	$.ajax({
                type:"GET",
                url:url,
                dataType:"JSON",
                contentType: "application/x-www-form-urlencoded; charset=utf-8", 
                success:function(data){
                	$("#editPointsRuleId").val(data.pointsRuleId);
                	$("#editPointsRuleName").val(data.pointsRuleName);
                	$("#editPointsTypeId").val(data.pointsTypeId);
                	$("#pointsTypeText").val($.POINTS_TYPE.getText(data.pointsTypeId));
                	$("#editPointsRuleType").val(data.pointsRuleType);
                	$("#pointsRuleTypeText").val($.POINTS_RULE_TYPE.getText(data.pointsRuleType));
                	$("#editPointsAmount").val(data.pointsAmount);
                	$("#editInvalidTimeType").val(data.invalidTimeType);
                	$("#invalidTimeTypeText").val($.INVALID_TIME_TYPE.getText(data.invalidTimeType));
                	$("#editInvalidTimeValue").val(data.invalidTimeValue);
             		if(!isNulls(data.invalidTimeValue)){
            			var invalidTimeValues=data.invalidTimeValue.split("/");
                    	$("#timeValue").val(invalidTimeValues[0]);
                    	$("#dateTypeValue").val(invalidTimeValues[1]);
                    	$("#dateTypeText").val($.POINT_TIME_TYPE.getText(invalidTimeValues[1]));
            		}
             		$("#editCleanTime").val(data.cleanTime);
             		$("#pointsRuleDesc").val(data.pointsRuleDesc);
                	if(data.invalidTimeType==1){//滚动日期+固定日期
                		$("#rollDate").show();
                		$("#editInvalidTimeValue").attr("data-options","required:true,length:'50'");
                		$("#dateTypeText").attr("disabled","disabled");
                    	$("#dateType_i").hide();
                		$("#cleanDate").show();
                    	$("#cleanTime_div1").show();
                    	var ct=data.cleanTime;
                    	var newct= insert_flg(ct,"-",2);
                    	$("#editCleanTime_md").val(newct.substring(0,newct.length-1));
                    	$("#cleanTime_div2").hide();
   
                	}else if(data.invalidTimeType==2){//固定日期
                		$("#rollDate").hide();
                		$("#editInvalidTimeValue").removeAttr("data-options");
                		$("#cleanDate").show();	
                		$("#cleanTime_div1").hide();
                		$("#cleanTime_div2").show();
                    	var ct=data.cleanTime;
                    	var newct= insert_flg(ct,"-",2);
                    	newct=newct.substring(0,newct.length-1).replace("-", "");
                    	$("#editCleanTime_ymd").val(newct);
                    	
                	}else if(data.invalidTimeType==3){//永久生效
                		$("#rollDate").hide();
                		$("#editInvalidTimeValue").removeAttr("data-options");
                		$("#cleanDate").hide();		
                	}else{//滚动日期
                		$("#rollDate").show();
                		$("#editInvalidTimeValue").attr("data-options","required:true,length:'50'");
                		$("#cleanDate").hide();		
                	}
                	
                	
                	if(openType=="show"){
                		$('#save_btn').hide();
                		$("#editPointsRuleName").attr("readonly","readonly");
                		$("#pointsTypeText").attr("disabled","disabled");
                		$("#pointsTypeIdSelect_i").hide(); 
                		$("#pointsRuleTypeText").attr("disabled","disabled");
                		$("#pointsRuleTypeText_i").hide(); 
                		$("#editPointsAmount").attr("readonly","readonly");
                		$("#invalidTimeTypeText").attr("disabled","disabled");
                		$("#invalidTimeTypeText_i").hide();
                		$("#dateTypeText").attr("disabled","disabled");
                    	$("#dateType_i").hide();  
                    	$("#timeValue").attr("readonly","readonly");
                    	$("#pointsRuleDesc").attr("readonly","readonly");
                    	$("#editCleanTime_md").attr("disabled","disabled");
                    	$("#editCleanTime_ymd").attr("disabled","disabled");
                	}else{
            			$("#save_btn").show();
            			$("#editPointsRuleName").removeAttr("readonly");
            			$("#pointsTypeText").removeAttr("disabled");
            			$("#pointsTypeIdSelect_i").show(); 
            			$("#pointsRuleTypeText").removeAttr("disabled");
            			$("#pointsRuleTypeText_i").show(); 
            			$("#editPointsAmount").removeAttr("readonly");
            			$("#invalidTimeTypeText").removeAttr("disabled");
            			$("#invalidTimeTypeText_i").show();
            			if(data.invalidTimeType!=1){
	            			$('#dateTypeText').removeAttr("disabled");
	            			$("#dateType_i").show(); 
            			}
            			$("#timeValue").removeAttr("readonly");
            			$("#pointsRuleDesc").removeAttr("readonly");
            			$('#editCleanTime_md').removeAttr("disabled");
            			$('#editCleanTime_ymd').removeAttr("disabled");
            		}
                },
                error:function(){
                    alert("request error");
                }
            });	
		}else{
	    	$("#editPointsRuleId").val("");
	    	$("#editPointsRuleName").val("");
	    	$("#editPointsTypeId").val("");
	    	$("#pointsTypeText").val("");
	    	$("#editPointsRuleType").val("");
	    	$("#pointsRuleTypeText").val("");
	    	$("#editPointsAmount").val("");
	    	$("#editInvalidTimeType").val("");
	    	$("#invalidTimeTypeText").val("");
	    	$("#editInvalidTimeValue").val("");
	        $("#timeValue").val("");
	        $("#invalidTimeType").val("");
	        $("#dateTypeText").val("");            			
	 		$("#editCleanTime").val("");
	 		$("#pointsRuleDesc").val("");	
		}

	};

	return {
		init: init
	}
});
//下拉框初始化
function initSelect(id){
	$('#'+id).each(function(i,v){
		var _pp = $(this);
		var s=$.trim($(this).attr("data"));
		var url_options = $.trim($(this).attr("url-options"));
		
		var js="";
		if(id=="invalidTimeTypeSelect"){
			js="showView";
		}
		
		if(s){
			eval("var data = "+s);
			$.each(data,function(i,v){
				_pp.append("<li data-id='"+v.value+"'><a href='javascript:"+js+"("+v.value+")"+";'>"+v.text+"</a></li>");
			});
		}else if(url_options){
			eval("var urlData = {"+url_options+"}");
			var url = urlData.url;
			var value = urlData.value;
			var text = urlData.text;
			$.ajax({ 
		        type: "get", 
		        url: encodeURI($.baseUrl+url), 
		        dataType: "json",
		        async: false,
		        success: function (data) {
		        	$.each(data, function(i, v){
		        		eval("var aa = v."+value+"");
		        		eval("var bb = v."+text+"");
		        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
				    });
		        }, 
		        error: function (XMLHttpRequest, textStatus, errorThrown) { 
		            alert(errorThrown); 
		        }
		    });
		}
	});
}
function showView(obj){
	//alert(obj);
	if(obj==1){//滚动日期+固定日期
		$("#rollDate").show();
		$("#timeValue").attr("data-options","required:true,validType:'number',length:'50'");
		$("#cleanDate").show();
    	$("#dateTypeValue").val("year");
    	$("#dateTypeText").val("年");  
    	$("#dateTypeText").attr("disabled","disabled");
    	$("#dateType_i").hide();  
    	$("#cleanTime_div1").show();
    	$("#cleanTime_div2").hide();
    	$("#editCleanTime_ymd").val("");
    	
	}else if(obj==2){//固定日期
		$("#rollDate").hide();
		$("#timeValue").removeAttr("data-options");
		
		$("#cleanDate").show();	
		$("#dateTypeValue").val("");
    	$("#dateTypeText").val("");  
    	$("#dateTypeText").removeAttr("disabled");
    	$("#dateType_i").show();
    	$("#cleanTime_div1").hide();
    	$("#editCleanTime_md").val("");
    	$("#cleanTime_div2").show();
    	
	}else if(obj==3){//永久生效
		$("#rollDate").hide();
		$("#timeValue").removeAttr("data-options");
		
		$("#cleanDate").hide();		
		$("#editCleanTime_md").val("");
		$("#editCleanTime_ymd").val("");
    	$("#dateTypeValue").val("");
    	$("#dateTypeText").val("");  
		$('#dateTypeText').removeAttr("disabled");
		$("#dateType_i").show();  		
	}else{//滚动日期
		$("#rollDate").show();
		$("#timeValue").attr("data-options","required:true,validType:'number',length:'50'");
		
		$("#cleanDate").hide();		
		$("#editCleanTime_md").val("");
		$("#editCleanTime_ymd").val("");
    	$("#dateTypeValue").val("");
    	$("#dateTypeText").val("");  
		$('#dateTypeText').removeAttr("disabled");
		$("#dateType_i").show();  
	}
}

//str表示原字符串变量，flg表示要插入的字符串，sn表示要插入的位置 
function insert_flg(str,flg,sn){
    var newstr="";
    for(var i=0;i<str.length;i+=sn){
        var tmp=str.substring(i, i+sn);
        newstr+=tmp+flg;
    }
    return newstr;
}