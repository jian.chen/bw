//页面标题
var title;
define(['common'], function(common) {
	//var saveType;
	//赠送优惠券的信息
	var couponInfoMap;
	//指定产品信息
	var productTemp_id;
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化下拉框&输入框验证
		initSelect();
		initInput();

		//判断活动类型
		showByType();
		
		couponInfoMap = {};
		addTagIdArr = new Array();
		addTagNameArr = new Array();
		productTemp_id = new Array();
		if(saveType == 'show'){
			$("#saveBtn-form-activitySetting").remove();
			showInstance();
			
			loadCouponTable();
		}else if(saveType == 'edit'){
			showInstance();
			
			loadCouponTable();
		}else{
			loadCouponTable();
		}

		//发起活动
		 $("#saveBtn-form-activitySetting").click(function () {
			 var ck = checkActivity('save');
			 
			 if(ck){
				 $('#channelId').val(channelId);
				 startActivity();
			 }else{
				 return;
			 }
		 });
		
		$(":checkbox[name='pointsOrcoupon']").click(function () {
	         if ($(this).val() == "points") {
	        	 $("#coupon-div").hide();
	        	 $("#li-points").show();
	         } else {
	        	 $("#li-points").hide();
	        	 $("#coupon-div").show();
	        	 $("#channelSetting-coupon-table").datagrid({"height":"200"});
	         }
	     });
		//showInstance();
		initBtn();
		 //生成二维码
		$(function(){
			var code;
			if(channelId==401){
				code="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx506f4b12c6e6292a&redirect_uri=http%3a%2f%2fscrm.pizzaexpress.cn%2fscrm-pe-wechat%2fmemberController%2fwechatEntrance%3fkey%3d5&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
			}else{
				code="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx506f4b12c6e6292a&redirect_uri=http%3a%2f%2fscrm.pizzaexpress.cn%2fscrm-pe-wechat%2fmemberController%2fwechatEntrance%3fkey%3d2%26channelId%3d"+channelId+"&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
			}
			var str = toUtf8(code);
			$("#linkCode").qrcode({
				render: "table",
				width: 170,
				height:200,
				text: str
			});

		})
	}
	
	
	function showByType(){
		
		//新会员奖励
		title = '新会员奖励设置';

		$('#title').html(title);
	}
	window.selectPointsOrcoupon = function(value){
		if (value == "points") {
	       	 $("#coupon-div").hide();
	       	 $("#li-points").show();
        } else {
	       	 $("#li-points").hide();
	       	 $("#coupon-div").show();
	       	 $("#channelSetting-coupon-table").datagrid({"height":"200"});
        }
		
	}
	//表单验证
	function checkActivity(step){
		//公用输入框方法验证
		var ckActivity = formCheck($("#form-activitySetting"));
		if(ckActivity){
			 var formParam = serializeObject($('#form-activitySetting'));
			 if(step=='save'){
				 // 新会员奖励
				// 校验活动开始日期
					if($("#fromDate").val() ==''){
						ckActivity =false;
						$("#fromDate").focus();
						$("#fromDate").alert('活动开始日期不能为空！');
					}
					
				    if(formParam.validityType){
						 if(formParam.validityType == "fixed"){
							 if($("#fixedDate").val() == '' || $("#fixedDate").val() == null){
								 ckActivity =false;
								 $("#fixedDate").alert('请设置结束时间！');
							 }
						 }else if(formParam.validityType == "period"){
							 if($("#periodDays").val() == '' || $("#periodDays").val() == null){
								 ckActivity =false;
								 $("#periodDays").alert('请设置结束时间！');
							 }
						 }
				   }else{
					     ckActivity =false;
						 alert('请选择结束时间！');
				   }
			 }
			 // 需完善信息
//			 奖励方式不能为空
			 if($("#give_points").val() == '' && $("#give_coupons").val() == ''&& $('#member_grade').val()==''){
				 alert("请至少选择一种奖励方式！");
				 ckActivity = false;
				 return;
			 }
			 if($('#pointsOrcoupon_coupons').is(':checked')){
				 var couponIds = getCheckedId();
				 if(couponIds.length==0){
					 alert("优惠券必须至少选中一条记录！");
					 ckActivity = false;
					 return;
				 }
				 //判断以勾选的优惠券必须填写数量
				 for (var i = 0; i < couponIds.length; i++) {
					 if (!couponInfoMap[couponIds[i]]) {
						 alert("已勾选的优惠券，必须填写优惠券数量！");
						 ckActivity = false;
						 return;
					 }
				 }
				 for(var key in couponInfoMap){
					 var isExists=false;
					 for (var i = 0; i < couponIds.length; i++) {
						 if (couponIds[i]==key) {
							 isExists=true;
							 break;
						 }
					 }
					 if(!isExists){
						 alert("存在已输入的数量没有勾选！");
						 ckActivity = false;
						 return;
					 }
				 }
			 }
			 if($('#pointsOrcoupon_points').is(':checked')){
				 if($('#give_points').val()==''){
					 alert("请填写积分！");
					 ckActivity = false;
					 return;
				 }
			 }
		 }else{
			 ckActivity = false;
			 return;
		 }
		
		return ckActivity;
	}
	
	function loadCouponTable(isSeachCoupon){
		//初始化优惠券table
		$('#channelSetting-coupon-table').datagrid( {
			url : $.baseUrl+'/couponController/queryCouponList.json',
			method:'get',
			queryParams : {
				isSeachStatus : isSeachCoupon
			},
			onLoadSuccess : function(result) {
				if(result) {
             }
			},
			onLoadError : function() {
			}
		});
	}
	
	 
	 //查询活动实例详情
	 function showInstance(){
		 $.ajax({
			 type: "GET",   //访问WebService使用Post方式请求
			 url: $.baseUrl+'/channelController/showChannelCondition',
			 async: true,
			 dataType : "json",
			 data:  {
				 channelCfgId : channelCfgId
			 },
			 success: function (res) {//回调函数，result，返回值
				 instance(res);
			 }
		 });
		
	 }
	 
	 //发起活动
	 function startActivity(){
		 var formParam = serializeObject($('#form-activitySetting'));
		 var conditionMap = {};
		 var saveUrl;
		 
		 //判断新建or修改活动
		 conditionMap.MEMBER_GRADE = formParam.member_grade;
		 
		 //送优惠券or送积分
		 if($("#pointsOrcoupon_coupons").prop('checked') == true){
			 conditionMap.GIVE_COUPONS = formParam.give_coupons;
		 }
		 if($("#pointsOrcoupon_points").prop('checked') == true){
			 conditionMap.GIVE_POINTS = formParam.give_points;
			 conditionMap.GIVE_POINTS_TYPE = formParam.give_points_type;
			 conditionMap.GIVE_POINTS_AMOUNT = formParam.give_points_amount;
		 }
		 var params = {};
		 params = formParam;
		 params.conditionMap = conditionMap;
		 //结束时间(有效期)
		 if(params.validityType == "fixed"){
			 params.validityValue = $("#fixedDate").val();
		 }else if(params.validityType == "period"){
			 params.validityValue = $("#periodDays").val();
		 }else if(params.validityType == "forever"){
			 params.validityValue = "";
		 }else{
			 params.validityValue = "";
		 }
		
		 /*$("#activityInstanceName").alert("sdfsdf");
		 return;*/
		 $("#dataLoad").show();
		 $.ajax({
             type: "POST",   //访问WebService使用Post方式请求
             url: $.baseUrl+'/channelController/editChannelConditionValue',
             data:  {
				params : $.trim(JSON.stringify(params))
             },
             dataType: 'json',
             success: function (res) {//回调函数，result，返回值
            	 if(res.code!=200){
            		 alert(res.msg);
            		 $("#dataLoad").hide();
            	 }else{
            		 $("#dataLoad").hide();
            		 if(saveType == 'save'){
            			 alert("保存成功！");
            		 }else if(saveType == 'edit'){
            			 alert("修改成功！");
            		 }
            		 $("#activity-instance-table").datagrid('reload');
            		 $("#backBtn-form-activitySetting").trigger('click');
            	 }
             }
         });
	 }
	 
	 
	
		//二维码的字符转换
		function toUtf8(str) {   
		    var out, i, len, c;   
		    out = "";   
		    len = str.length;   
		    for(i = 0; i < len; i++) {   
		    	c = str.charCodeAt(i);   
		    	if ((c >= 0x0001) && (c <= 0x007F)) {   
		        	out += str.charAt(i);   
		    	} else if (c > 0x07FF) {   
		        	out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));   
		        	out += String.fromCharCode(0x80 | ((c >>  6) & 0x3F));   
		        	out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));   
		    	} else {   
		        	out += String.fromCharCode(0xC0 | ((c >>  6) & 0x1F));   
		        	out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));   
		    	}   
		    }   
		    return out;   
		}  
	
	 //查看编辑回显
	 function instance(res){
//		 var obj = JSON.parse(res);
		 var obj = res;
		 var conditionMap = obj.conditionMap;
		 var instance = obj.instance;
		 
		//会员等级名称赋值
		 if(conditionMap.MEMBER_GRADE != null){
			 $("#member_grade").val(conditionMap.MEMBER_GRADE);
			 $("#member_grade_view").val($.Member_Grade.getText(conditionMap.MEMBER_GRADE));
		 }
		//如果积分不为空，则赋值
		 if(conditionMap.GIVE_POINTS){
			 $("#give_points").val(conditionMap.GIVE_POINTS);
			 $("#pointsOrcoupon_points").click();
		 }
		//如果优惠券不为空，则赋值
		 if(conditionMap.GIVE_COUPONS){
			 $("#give_coupons").val(conditionMap.GIVE_COUPONS);
			 couponInfoMap = JSON.parse(conditionMap.GIVE_COUPONS);
			 $("#pointsOrcoupon_coupons").click();
		 }else{
			 couponInfoMap = {};
		 }
		
		 $("#give_points_amount").val(conditionMap.GIVE_POINTS_AMOUNT);
		 
		 
		 //会员等级名称赋值
		 if(conditionMap.MEMBER_GRADE != null){
			 $("#member_grade").val(conditionMap.MEMBER_GRADE);
			 $("#member_grade_view").val($.Member_Grade.getText(conditionMap.MEMBER_GRADE));
		 }
		 $("#channelDesc").val(instance.comments);
		 $("#channelCfgId").val(instance.channelCfgId);
		 $("#channelId").val(instance.channelId);
		 $("#channelInstanceName").val(instance.channelInstanceName);
		 $("#fromDate").val(getDateString(instance.fromDate));
		//有效期
		 if(instance.validityType == 'fixed'){
			 $("#fixedDate").val(getDateString(instance.validityValue));
			 $("#fixedDate_check").click();
		 }else if(instance.validityType == 'period'){
			 $("#periodDays").val(instance.validityValue);
			 $("#periodDays_check").click();
		 }else{
			 $("#last_check").click();
		 }
		 
	 }
	 
 	//失去焦点事件，保存优惠券的数量
	window.onBlurByChannelSetting = function(obj,couponId,availableQuantity,totalQuantity){
		if(obj.value != null && obj.value != ''){
			if (!(/^[0-9]*$/.test(obj.value))) { 
				$(obj).alert("请输入大于等于0的整数！");
				return; 
			} 
		}
		if(totalQuantity != 0){
			if(obj.value > availableQuantity){
				$(obj).alert("券数量不足！");
				$(obj).focus();
				$(obj).val('');
				return;
			}
		}
		if(obj.value){
			couponInfoMap[couponId] = obj.value;
		}else{
			delete couponInfoMap[couponId];
		}
		var params = JSON.stringify(couponInfoMap);
		$("#give_coupons").val(params);
	};
	
	//送券操作列
	window.channelSettingformatProgressCouponBtn = function(value,row,index){
		var couponId = row.couponId;
		var amount = couponInfoMap[couponId];
		if (amount){
			$('#channelSetting-coupon-table').datagrid("selectRecord", row.couponId);
	    	var s = '<input value="'+ amount +'" onblur="onBlurByChannelSetting(this,'+couponId+','+row.availableQuantity+','+row.totalQuantity+')" style="width:50px;border: solid 1px #000; margin-right: 5px;" />张/每次';
	    	return s;
		} else {
	    	var s = '<input value="" onblur="onBlurByChannelSetting(this,'+couponId+','+row.availableQuantity+','+row.totalQuantity+')" style="width:50px;border: solid 1px #000; margin-right: 5px;"  />张/每次';
	    	return s;
		}
	};
	//券剩余
	window.availableChannelSettingsshow = function(value,row,index){
		if(row.totalQuantity == '0'){
			return '无限';
		}
		return value;
	};
	
	
	
	
	function initBtn(){
		//返回
		 $("#backBtn-form-activitySetting").click(function () {
			 //common.Page.loadCenter('channel/channelManager.html');
			 if(pageTag=="instanceManage"){
				common.Page.loadCenter('channel/channelInstanceManage.html');
			 }else{
				common.Page.loadCenter('channel/channelManager.html');
			 }
		 });
		 
		//下一步
		 $("#nextBtn-form-activitySetting").click(function () {
			 var ck = checkActivity("1");
			 if(ck){
				 $("#backBtn-form-activitySetting").hide();
				 $("#nextBtn-form-activitySetting").hide();
				 $("#ul-show-first").hide();
				 $("#div-title-first").hide();
				 
				 $("#ul-show-second").show();
				 $("#aboveBtn-form-activitySetting").show();
				 $("#saveBtn-form-activitySetting").show();
				 $("#div-title-second").show();
			 }
		 });
		 
		//上一步
		 $("#aboveBtn-form-activitySetting").click(function () {
			 $("#backBtn-form-activitySetting").show();
			 $("#nextBtn-form-activitySetting").show();
			 $("#ul-show-first").show();
			 $("#div-title-first").show();
			 
			 $("#ul-show-second").hide();
			 $("#aboveBtn-form-activitySetting").hide();
			 $("#saveBtn-form-activitySetting").hide();
			 $("#div-title-second").hide();
		 });
	}
	function getCheckedId(){
		var selected = $('#channelSetting-coupon-table').datagrid('getChecked');
		var couponId=[];
		for(var i=0; i<selected.length; i++){
			couponId[i] = selected[i].couponId;
		}
		return couponId;
	}
	
	return {
		init: init
	};
});





