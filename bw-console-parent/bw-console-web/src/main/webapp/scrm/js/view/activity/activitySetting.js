//存储勾选的标签id
var addTagIdArr;
//存储勾选的标签name
var addTagNameArr;
//页面标题
var title;

define(['common','selectTag','activityManage','activityInstanceManage'], function(common,selectTag,activityManage,activityInstanceManage) {
	//var saveType;
	//赠送优惠券的信息
	var couponInfoMap;
	//指定产品信息
	var productTemp_id;
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化下拉框&输入框验证
		initSelect();
		initInput();
		
		//初始化会员分组
		initSelectById('member_group_view','/memberGroupController/showMemberGroupListSelect.json','memberGroupId','memberGroupName');
		
		//初始化短信模板
		initSelectById('sms_temp_view','/activityController/showSmsTempList.json','smsTempId','smsTempName');
		
		//初始化积分规则下拉框
		initSelectById('points_view','/activityController/showActivityPointsRule.json','pointsRuleId','pointsRuleName');
		
		//判断活动类型
		showByType(activityType);
		
		couponInfoMap = {};
		addTagIdArr = new Array();
		addTagNameArr = new Array();
		productTemp_id = new Array();
		
		if(saveType == 'save'){
			//初始化优惠券
			loadCouponTable('Y');
		}else if(saveType == 'show'){
			$("#saveBtn-form-activitySetting").remove();
			showInstance();
			
			loadCouponTable();
		}else if(saveType == 'edit'){
			showInstance();
			
			loadCouponTable();
		}
		
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
		//积分赠送类型
		$("#give_points_type").change(function(){
			if(this.value == "1"){
				$("#font-points-desc1").show();
				$("#font-points-desc2").show();
			}else{
				$("#font-points-desc1").hide();
				$("#font-points-desc2").hide();
			}
		});
		
		
		//标签选中启用
		/*$("#setting-memberTages").click(function(){
			if($(this).prop('checked') == true){
				$("#labelChooseBtn-activitySetting").attr("disabled",false);
			}else{
				$("#labelChooseBtn-activitySetting").attr("disabled",true);
			}
		});*/
		//分组选中启用
		$("#memberGroupsCheck").click(function(){
			if($(this).prop('checked') == true){
				$("#member_group_view").attr("disabled",false);
				$("#memberGroupsCheck_icon").css("z-index","0");
			}else{
				$("#member_group_view").attr("disabled",true);
				$("#member_group").val('');
				$("#member_group_view").val('');
				$("#memberGroupsCheck_icon").css("z-index","-1");
			}
		});
		
		//结束时间各项选中，输入框启用与禁用
		$("#fixedDate_check").click(function(){
			$("#fixedDate").attr("disabled",false);
			$("#periodDays").attr("disabled",true);
		});
		$("#periodDays_check").click(function(){
			$("#periodDays").attr("disabled",false);
			$("#fixedDate").val('');
			$("#fixedDate").attr("disabled",true);
		});
		$("#last_check").click(function(){
			$("#fixedDate").val('');
			$("#fixedDate").attr("disabled",true);
			$("#periodDays").attr("disabled",true);
		});
		
		
		//产品弹窗
		$('#appoint_productName').click(function() {
			$('#chanPingTable').show();
			//初始化产品列表
			showProduct();
			
		});
		
		//产品查询
		$('#seach_productBtn').click(function() {
			$('#productTable').datagrid('load',{
				productName : $("#seach_product_name").val(),
				productCode : $("#seach_product_code").val()
			} );
		});
		
		//产品确定
		$('#saveProductForActivity').click(function() {
			//获取产品ids&names&codes
			var selected = $('#productTable').datagrid('getChecked');
			var productIds = '';
			var productName = '';
			var productCode = '';
			
			if(selected.length!=0){
				for(var i=0; i<selected.length; i++){
					if (productIds != ''){ 
						productIds += ',';
				    }
					if(productName != ''){
						productName +=','
					}
					if(productCode != ''){
						productCode +=','
					}
					productIds += selected[i].productId;
					productName += selected[i].productName;
					productCode += selected[i].productCode;
				}
				var product_Detail = {};
				product_Detail.id = productIds;
				product_Detail.name = productName;
				product_Detail.code = productCode;
				$('#appoint_product').val(JSON.stringify(product_Detail));
				$('#appoint_productName').val(productName);
			}
			//关闭弹出层
			$('#chanPingTable').find(".detailed_close").click();
		});
		
		
		
		//标签弹窗
		$('#labelChooseBtn-activitySetting').click(function() {
			$('#labelChoose-activitySetting').show();
			appendTag();
			queryTag($('#likeMemberTagName-activitySetting').val());
		});
		
		//标签查询
		$('#queryTag_bt-activitySetting').click(function() {
			queryTag($('#likeMemberTagName-activitySetting').val());
		});
		
		//标签确定
		$('#tagSaveBtn-activitySetting').click(function() {
//			addTagNameArr;
//			addTagIdArr;
			//关闭弹出层
			$('#labelChoose-activitySetting').find(".detailed_close").click();
			$('#member_tag_id').val(addTagIdArr);
			$('#labelChooseBtn-activitySetting').val(addTagNameArr);
			//addSelectTag();
		});
		
		
		//发起活动
		 $("#saveBtn-form-activitySetting").click(function () {
			 var ck = checkActivity('save');
			 
			 if(ck){
				 
				 $('#activityInstanceId').val(activityInstanceManage.getActivityInstanceId());
				 startActivity();
			 }else{
				 return;
			 }
		 });
		
		$(":radio[name='pointsOrcoupon']").click(function () {
	         if ($(this).val() == "points") {
	        	 $("#coupon-div").hide();
	        	 $("#li-points").show();
	         } else {
	        	 $("#li-points").hide();
	        	 $("#coupon-div").show();
	        	 $("#activitySetting-coupon-table").datagrid({"height":"200"});
	         }
	     });
		//showInstance();
		
		initBtn();
	}
	

	window.showSendDate = function showSendDate(sendDateViewType){
		if(sendDateViewType == '1' || sendDateViewType == '3'){
			$("#faFangShiJian_div").hide();
		}else{
			$("#faFangShiJian_div").show();
		}
	}
	
	function showByType(activityType){
		
		//新会员奖励
		if(activityType == 'ACTIVIY_NEW_MEMBER'){
			
			title = '新会员奖励设置';
			$("#teDingRiQi").hide();
			$("#faFangShiJian").hide();
			$("#zhiDingChanPin").hide();
			$("#zhiDingXiaoFei").hide();
			$("#faFangShiJian").hide();
			$("#dengJiMingChen").hide();
			$("#huanXinSheZhi").hide();
			$("#reward").hide();
			$("#wanShangXinxi").hide();
			
		}
		//会员信息完善奖励
		if(activityType == 'ACTIVIY_PERFECT_MEMBER'){
			
			title = '会员信息完善奖励设置';
			$("#teDingRiQi").hide();
			$("#faFangShiJian").hide();
			$("#zhiDingChanPin").hide();
			$("#zhiDingXiaoFei").hide();
			$("#faFangShiJian").hide();
			$("#dengJiMingChen").hide();
			$("#huanXinSheZhi").hide();
			$("#reward").hide();
			
		}
		//指定日期奖励
		else if(activityType == 'ACTIVIY_SPECIAL_DATE'){
			
			//初始化特定日期
			initSelectById('appoint_date_view','/activityController/showActivityDateList.json','activityDateId','activityDateName');
			
			title = '指定日期奖励设置';
			$("#zhiDingChanPin").hide();
			$("#zhiDingXiaoFei").hide();
			$("#faFangShiJian").hide();
			$("#dengJiMingChen").hide();
			$("#huanXinSheZhi").hide();
			$("#wanShangXinxi").hide();
			
		}
		//等级会员奖励
		else if(activityType == 'ACTIVIY_MEMBER_GRADE_UP'){

			//初始化会员等级下拉框
			initSelectById('member_grade_view','/activityController/showGrade.json','gradeId','gradeName');
			
			title = '等级会员奖励设置';
			$("#teDingRiQi").hide();
			$("#zhiDingChanPin").hide();
			$("#zhiDingXiaoFei").hide();
			$("#huanXinSheZhi").hide();
			
			//发放时间固定为“升级日期”,不可选
			$("#send_date_view").val('升级日期');
			$("#send_date_view").attr('disabled',true);
			$("#send_date_view").css('background','#cdcdcd');
			$("#send_date_view").next().next().hide();
			$("#faFangShiJian_div").hide();
			$("#wanShangXinxi").hide();

		}
		//会员唤醒奖励
		else if(activityType == 'ACTIVIY_MEMBER_WAKE'){
			
			title = '会员唤醒奖励设置';
			$("#teDingRiQi").hide();
			$("#faFangShiJian").hide();
			$("#dengJiMingChen").hide();
			$("#zhiDingChanPin").hide();
			$("#zhiDingXiaoFei").hide();
			$("#wanShangXinxi").hide();
		}
		//会员生日奖励
		else if(activityType == 'ACTIVIY_MEMBER_BIRTHDAY'){
			
			title = '会员生日奖励设置';
			$("#zhiDingChanPin").hide();
			$("#zhiDingXiaoFei").hide();
			$("#huanXinSheZhi").hide();
			$("#wanShangXinxi").hide();
			
			//特定日期固定为“会员生日”,不可选
			$("#appoint_date_view").val('会员生日');
			$("#appoint_date_view").attr('disabled',true);
			$("#appoint_date_view").css('background','#cdcdcd');
			$("#appoint_date_view").next().next().hide();
			//发放时间默认当日
			$("#send_date_view").next("ul").find("[data-id='1']").find("a").click();
		}
		//指定消费奖励
		else if(activityType == 'ACTIVIY_SPECIAL_SALE'){
			
			title = '指定消费奖励设置';
			$("#teDingRiQi").hide();
			$("#faFangShiJian").hide();
			$("#dengJiMingChen").hide();
			$("#huanXinSheZhi").hide();
			$("#wanShangXinxi").hide();
			
		}
		//首笔消费奖励
		else if(activityType == 'ACTIVIY_FIRST_SALE'){
			
			title = '首笔消费奖励设置';
			$("#teDingRiQi").hide();
			$("#faFangShiJian").hide();
			$("#zhiDingChanPin").hide();
			$("#zhiDingXiaoFei").hide();
			$("#huanXinSheZhi").hide();
			$("#dengJiMingChen").hide();
			$("#wanShangXinxi").hide();
			
		}
		//消费即送奖励
		else if(activityType == 'ACTIVITY_EVERY_SALE'){
			
			title = '消费即送奖励设置';
			$("#teDingRiQi").hide();
			$("#faFangShiJian").hide();
			$("#zhiDingChanPin").hide();
			$("#zhiDingXiaoFei").hide();
			$("#huanXinSheZhi").hide();
			$("#dengJiMingChen").hide();
			$("#wanShangXinxi").hide();
			
			$("#font-points-desc1").show();
			$("#font-points-desc2").show();
			$("#give_points_type").show();
		}
		
		$('#activityCode').val(activityType);
		$('#title').html(title);
	}
	
	//表单验证
	function checkActivity(step){
		//公用输入框方法验证
		var ckActivity = formCheck($("#form-activitySetting"));
		console.log("activityType:"+activityType);
		if(ckActivity){
			 var formParam = serializeObject($('#form-activitySetting'));
			 if(step=='save'){
				 // 会员生日
				 if(activityType == 'ACTIVIY_MEMBER_BIRTHDAY'){
					// 校验活动开始日期
					if($("#fromDate").val() ==''){
						ckActivity =false;
						$("#fromDate").focus();
						$("#fromDate").alert('活动开始日期不能为空！');
					}
						
				    if(formParam.validityType){
						 if(formParam.validityType == "fixed"){
							 if($("#fixedDate").val() == '' || $("#fixedDate").val() == null){
								 ckActivity =false;
								 $("#fixedDate").alert('请设置结束时间！');
							 }
						 }else if(formParam.validityType == "period"){
							 if($("#periodDays").val() == '' || $("#periodDays").val() == null){
								 ckActivity =false;
								 $("#periodDays").alert('请设置结束时间！');
							 }
						 }
				   }else{
					     ckActivity =false;
						 alert('请选择结束时间！');
				   }
				 }
				 // 会员完善信息奖励
				 else if(activityType == 'ACTIVIY_PERFECT_MEMBER'){
						// 校验活动开始日期
						if($("#fromDate").val() ==''){
							ckActivity =false;
							$("#fromDate").focus();
							$("#fromDate").alert('活动开始日期不能为空！');
						}
						
					    if(formParam.validityType){
							 if(formParam.validityType == "fixed"){
								 if($("#fixedDate").val() == '' || $("#fixedDate").val() == null){
									 ckActivity =false;
									 $("#fixedDate").alert('请设置结束时间！');
								 }
							 }else if(formParam.validityType == "period"){
								 if($("#periodDays").val() == '' || $("#periodDays").val() == null){
									 ckActivity =false;
									 $("#periodDays").alert('请设置结束时间！');
								 }
							 }
					   }else{
						     ckActivity =false;
							 alert('请选择结束时间！');
					   }
				 }
				 // 新会员奖励
				 else if(activityType == 'ACTIVIY_NEW_MEMBER'){
						// 校验活动开始日期
						if($("#fromDate").val() ==''){
							ckActivity =false;
							$("#fromDate").focus();
							$("#fromDate").alert('活动开始日期不能为空！');
						}
						
					    if(formParam.validityType){
							 if(formParam.validityType == "fixed"){
								 if($("#fixedDate").val() == '' || $("#fixedDate").val() == null){
									 ckActivity =false;
									 $("#fixedDate").alert('请设置结束时间！');
								 }
							 }else if(formParam.validityType == "period"){
								 if($("#periodDays").val() == '' || $("#periodDays").val() == null){
									 ckActivity =false;
									 $("#periodDays").alert('请设置结束时间！');
								 }
							 }
					   }else{
						     ckActivity =false;
							 alert('请选择结束时间！');
					   }
				 }
			 }
			 /*新会员奖励
				if(activityType == 'ACTIVIY_NEW_MEMBER'){
						
				}*/
				//var activityType = activityManage.getActivityType();
				//指定日期奖励
				if(activityType == 'ACTIVIY_SPECIAL_DATE'){
					if($("#appoint_date").val() == ''){
						$("#appoint_date_view").focus();
						$("#appoint_date_view").alert('请选择特定日期！');
						ckActivity = false;
						return;
					}
				}
				//等级会员奖励
				else if(activityType == 'ACTIVIY_MEMBER_GRADE_UP'){
					if($("#member_grade").val() == ''){
						$("#member_grade_view").focus();
						$("#member_grade_view").alert('请选择会员等级！');
						 ckActivity = false;
						 return;
					}
					
				}
				//会员唤醒奖励
				else if(activityType == 'ACTIVIY_MEMBER_WAKE'){
					if($("#wake_date").val() == '' || $("#wake_date").val() == null){
						$("#wake_date").focus();
						$("#wake_date").alert('请填写唤醒时间！');
						 ckActivity = false;
						 return;
					}
					
				}
				//会员生日奖励
				else if(activityType == 'ACTIVIY_MEMBER_BIRTHDAY'){
					var sendDateType = $("#send_date_type").val();
					if(sendDateType == 2){
						/*$("#send_date_view").focus();
						$("#send_date_view").alert('请选择发放时间！');*/
						var sendDateValue = $("#send_date_value").val();
						if(sendDateValue == ''){
							$("#send_date_value").focus();
							$("#send_date_value").alert('请填写天数！');
							ckActivity = false;
							return;
						}
					}	
				}
				//指定消费奖励
				else if(activityType == 'ACTIVIY_SPECIAL_SALE'){
					if($("#appoint_product").val() == '' || $("#appoint_product").val() == null){
						$("#appoint_productName").focus();
						$("#appoint_productName").alert('请指定产品！');
						ckActivity = false;
						return;
					}
					if($("#appoint_productNum").val() == '' || $("#appoint_product").val() == null){
						$("#appoint_productNum").focus();
						$("#appoint_productNum").alert('请设置消费产品数！');
						ckActivity = false;
						return;
					}
					if($("#appoint_amount").val() == '' || $("#appoint_product").val() == null){
						$("#appoint_amount").focus();
						$("#appoint_amount").alert('请指定消费额！');
						ckActivity = false;
						return;
					}
					/*//指定消费额
					var a = $("#consumption").val() != '' && ($("#appoint_amount").val() != '' && $("#appoint_amount").val() != null);
					//指定产品
					var b = $("#appoint_product").val() != '' && ($("#appoint_productNum").val() != '' && $("#appoint_productNum").val() != null);
					if(a == false && b == false){
						alert('请指定产品或设置消费额度！');
						ckActivity = false;
						return;
					}*/
				}
				//会员完善信息
				else if(activityType == 'ACTIVIY_PERFECT_MEMBER'){
					if($("#give_points").val() == '' && $("#give_coupons").val() == ''){
						 alert("请选择奖励方式！");
						 ckActivity = false;
						 return;
					 }
					
					if(formParam.member_info ==undefined || formParam.member_info ==''){
						 alert("请完善信息！");
						 ckActivity = false;
						 return;
					}
				}
				/*首笔消费奖励
				else if(activityType == 'ACTIVIY_FIRST_SALE'){
						
				}
				消费即送奖励
				else if(activityType == 'ACTIVITY_EVERY_SALE'){
						
				}*/
			 // 需完善信息
			 //奖励方式不能为空
			 if($("#give_points").val() == '' && $("#give_coupons").val() == ''){
				 alert("请选择奖励方式！");
				 ckActivity = false;
				 return;
			 }
			 
			/* if($("#fromDate").val() == ''){
				 $("#fromDate").focus();
				 $("#fromDate").alert("开始时间不能为空！");
				 ckActivity = false;
				 return;
			 }
			 //结束时间不能为空
			 if(formParam.validityType){
				 if(formParam.validityType == "fixed"){
					 if($("#fixedDate").val() == '' || $("#fixedDate").val() == null){
						 alert('请设置结束时间！');
						 ckActivity = false;
						 return;
					 }
				 }else if(formParam.validityType == "period"){
					 if($("#periodDays").val() == '' || $("#periodDays").val() == null){
						 alert('请设置结束时间！');
						 ckActivity = false;
						 return;
					 }
				 }
			 }else{
				 alert('请选择结束时间类型！');
				 ckActivity = false;
				 return;
			 }*/
			 
		 }else{
			 ckActivity = false;
			 return;
		 }
		
		return ckActivity;
	}
	
	function loadCouponTable(isSeachCoupon){
		//初始化优惠券table
		$('#activitySetting-coupon-table').datagrid( {
			url : $.baseUrl+'/couponController/queryCouponList.json',
			method:'get',
			queryParams : {
				isSeachStatus : isSeachCoupon,
				isBrowse: "N"
			},
			onLoadSuccess : function(result) {
				if(result) {
             }
			},
			onLoadError : function() {
			}
		});
	}
	
	 
	 //查询活动实例详情
	 function showInstance(){
		 $.ajax({
			 type: "GET",   //访问WebService使用Post方式请求
			 url: $.baseUrl+'/activityController/showActivityInstance.json',
			 async: true,
			 dataType : "json",
			 data:  {
				 activityInstanceId : activityInstanceManage.getActivityInstanceId()
			 },
			 success: function (res) {//回调函数，result，返回值
				 instance(res);
			 }
		 });
		
	 }
	 
	 //发起活动
	 function startActivity(){
		 var formParam = serializeObject($('#form-activitySetting'));
		 var conditionMap = {};
		 var saveUrl;
		 
		 //判断新建or修改活动
		 if(saveType == 'save'){
			 saveUrl = '/activityController/startActivity.json';
		 }else if(saveType == 'edit'){
			 saveUrl = '/activityController/editActivity.json';
		 }
		 
		 //会员标签
		 if($("#setting-memberTages").prop('checked') == true){
			 var tageParams = {};
			 tageParams.id = formParam.member_tag_id;
			 tageParams.name = formParam.member_tag_name;
			 conditionMap.MEMBER_TAG = JSON.stringify(tageParams);
		 }//会员分组
		 if($("#memberGroupsCheck").prop('checked') == true){
			 conditionMap.MEMBER_GROUP = formParam.member_group;
		 }
		 //需完善信息内容
		 var member_info="";
		 $("input[name='member_info']").each(function(index,element){
			 if(this.checked==true){
				 if(member_info == ""){
					 member_info = $(this).val();
				 }else{
					 member_info += ","+ $(this).val();
				 }
			 }
		 });
		 if(member_info != ""){
			 conditionMap.MEMBER_PERFECT_INFO = member_info;
		 }
		 conditionMap.MEMBER_GRADE = formParam.member_grade;
		 
		 //送优惠券or送积分
		 if($("#pointsOrcoupon_coupons").prop('checked') == true){
			 conditionMap.GIVE_COUPONS = formParam.give_coupons;
		 }
		 if($("#pointsOrcoupon_points").prop('checked') == true){
			 conditionMap.GIVE_POINTS = formParam.give_points;
			 conditionMap.GIVE_POINTS_TYPE = formParam.give_points_type;
			 conditionMap.GIVE_POINTS_AMOUNT = formParam.give_points_amount;
		 }
		 
		 conditionMap.WAKE_DATE = formParam.wake_date;
		 conditionMap.APPOINT_PRODUCT = formParam.appoint_product;
		 conditionMap.APPOINT_PRODUCT_NUM = formParam.appoint_productNum;
		 conditionMap.APPOINT_AMOUNT = formParam.appoint_amount;
		 conditionMap.APPOINT_DATE = formParam.appoint_date;
		 
		 //发放时间
		 if(formParam.send_date_type == '2'){
			 conditionMap.SEND_DATE = formParam.send_date_type + '/'+formParam.send_date_value;
		 }else{
			 conditionMap.SEND_DATE = formParam.send_date_type;
		 }
		 
		 var params = {};
		 params = formParam;
		 params.conditionMap = conditionMap;
		 
		 //短信模板
		 if($("#smsTmp").prop('checked') == true){
			 params.templateUrl = $("#sms_temp").val();
		 }
		 
		 //结束时间(有效期)
		 if(params.validityType == "fixed"){
			 params.validityValue = $("#fixedDate").val();
		 }else if(params.validityType == "period"){
			 params.validityValue = $("#periodDays").val();
		 }else if(params.validityType == "forever"){
			 params.validityValue = "";
		 }else{
			 params.validityValue = "";
		 }
		
		 /*$("#activityInstanceName").alert("sdfsdf");
		 return;*/
		 $("#dataLoad").show();
		 $.ajax({
             type: "POST",   //访问WebService使用Post方式请求
             url: $.baseUrl+saveUrl,
             data:  {
				params : $.trim(JSON.stringify(params))
             },
             dataType: 'json',
             success: function (res) {//回调函数，result，返回值
            	 if(res.code!=200){
            		 alert(res.msg);
            		 $("#dataLoad").hide();
            	 }else{
            		 $("#dataLoad").hide();
            		 if(saveType == 'save'){
            			 alert("新建活动成功！");
            		 }else if(saveType == 'edit'){
            			 alert("编辑活动成功！");
            		 }
            		 $("#activity-instance-table").datagrid('reload');
            		 $("#backBtn-form-activitySetting").trigger('click');
            	 }
             }
         });
	 }
	 
	
	 //查看编辑回显
	 function instance(res){
//		 var obj = JSON.parse(res);
		 var obj = res;
		 var instance = obj.instance;
		 var conditionMap = obj.conditionMap;
		 var tmpType = instance.templateType;
		 
		 //如果优惠券不为空，则赋值
		 if(conditionMap.GIVE_COUPONS){
			 $("#give_coupons").val(conditionMap.GIVE_COUPONS);
			 couponInfoMap = JSON.parse(conditionMap.GIVE_COUPONS);
			 $("#pointsOrcoupon_coupons").click();
		 }else{
			 couponInfoMap = {};
		 }
		 
		 if(conditionMap.MEMBER_PERFECT_INFO){
			 var member_info_codes = conditionMap.MEMBER_PERFECT_INFO.split(",");
			 $("input[name='member_info']").each(function(index,element){
				 for (var i=0;i<member_info_codes.length ;i++ ){   
					 if($(this).val()==member_info_codes[i]){
						 $(this).prop("checked",true);
					 }
				 }
			 });
		 }
		 
		//积分规则赋值
		 /*
		 $("#give_points").val(conditionMap.GIVE_POINTS);
		 if(conditionMap.GIVE_POINTS != null && conditionMap.GIVE_POINTS != ""){
			 $("#points_view").val($("#points_view").next("ul").find("[data-id="+conditionMap.GIVE_POINTS+"]").find("a").text());
		 }
		 conditionMap.GIVE_POINTS_TYPE = formParam.give_points_type;
			 conditionMap.GIVE_POINTS_AMOUNT = formParam.give_points_amount;
		 */
		 
		 $("#give_points").val(conditionMap.GIVE_POINTS);
		 $("#give_points_amount").val(conditionMap.GIVE_POINTS_AMOUNT);
		 $("#give_points_type").val(conditionMap.GIVE_POINTS_TYPE);
		 
		 
		 
		 
		 $("#activityInstanceName").val(instance.activityInstanceName);
		 $("#activityInstanceDesc").val(instance.activityInstanceDesc);
		 $("#fromDate").val(getDateString(instance.fromDate));
		 
		 //奖励人员(分组&标签)
		 if(conditionMap.MEMBER_TAG){
			 member_tag = eval('(' + conditionMap.MEMBER_TAG + ')');
			 addTagIdArr = member_tag.id.split(',');
			 addTagNameArr = member_tag.name.split(',');
			 $("#labelChooseBtn-activitySetting").val(member_tag.name);
			 appendTag();
		 }
		 
		 $("#member_group").val(conditionMap.MEMBER_GROUP);
		 //$("#member_group_view").val($("#member_group_view").next("ul").find("[data-id="+conditionMap.MEMBER_GROUP+"]").find("a").text());
		 if(conditionMap.MEMBER_TAG){
		 	 $("#setting-memberTages").attr('checked',true);
		 }
		 if(conditionMap.MEMBER_GROUP){
		 	$("#memberGroupsCheck").click();
		 }
		 
		 //会员等级名称赋值
		 if(conditionMap.MEMBER_GRADE != null){
			 $("#member_grade").val(conditionMap.MEMBER_GRADE);
			 $("#member_grade_view").val($.Member_Grade.getText(conditionMap.MEMBER_GRADE));
		 }
		 
		 //唤醒设置赋值
		 if(conditionMap.WAKE_DATE != null){
			 $("#wake_date").val(conditionMap.WAKE_DATE);
		 }
		 
		 //特定日期赋值
		 if(conditionMap.APPOINT_DATE != null){
			 $("#appoint_date").val(conditionMap.APPOINT_DATE);
			 $("#appoint_date_view").val($("#appoint_date_view").next("ul").find("[data-id="+conditionMap.APPOINT_DATE+"]").find("a").text());
		 }
		 
		 //发放时间
		 if(conditionMap.SEND_DATE){
			 if(conditionMap.SEND_DATE.indexOf('/')>=0){
				 var dv = conditionMap.SEND_DATE.split('/');
				 $("#send_date_type").val(dv[0]);
				 $('#send_date_value').val(dv[1]);
				 $("#send_date_view").next("ul").find("[data-id="+dv[0]+"]").find("a").click();
			 }else{
				 $("#send_date_type").val(conditionMap.SEND_DATE);
				 //$("#send_date_view").val($("#send_date_view").next("ul").find("[data-id="+conditionMap.SEND_DATE+"]").find("a").text());
				 $("#send_date_view").next("ul").find("[data-id="+conditionMap.SEND_DATE+"]").find("a").click();
			 }
		 }
		 
		 //指定产品赋值
		 if(conditionMap.APPOINT_PRODUCT != null){
			 var productTemp = eval('(' + conditionMap.APPOINT_PRODUCT + ')');
			 $("#appoint_productName").val(productTemp.name);
			 $("#appoint_product").val(conditionMap.APPOINT_PRODUCT);
			 productTemp_id = productTemp.id.split(',');;
			 
		 }
		 $("input[name=appoint_productNum]").val(conditionMap.APPOINT_PRODUCT_NUM);
		 
		 //指定消费额
		 $("input[name=appoint_amount]").val(conditionMap.APPOINT_AMOUNT);
		 $("#consumption_view").val('大于等于');
		 $("#consumption").val('1');
		 
		
		 
		 $("input[name=validityType][value='"+instance.validityType+"']").attr("checked",true);
		 
		 //有效期
		 if(instance.validityType == 'fixed'){
			 $("#fixedDate").val(getDateString(instance.validityValue));
			 $("#fixedDate_check").click();
		 }else if(instance.validityType == 'period'){
			 $("#periodDays").val(instance.validityValue);
			 $("#periodDays_check").click();
		 }else{
			 $("#last_check").click();
		 }
		 
		 //模板赋值
		 if(tmpType == "msg"){
			 $("#smsTmp").attr('checked', true);
			 if(instance.templateUrl != null && instance.templateUrl != ""){
				 $("#sms_temp").val(instance.templateUrl);
				 $("#sms_temp_view").val($("#sms_temp_view").next("ul").find("[data-id="+instance.templateUrl+"]").find("a").text());
			 }
		 }else if(tmpType == "wechat"){
			 $("#wehcatTmp").attr('checked', true);
		 }else if(tmpType == "both"){
			 $("#smsTmp").attr('checked', true);
			 $("#wehcatTmp").attr('checked', true);
		 }
		 
	 }
	 
 	//失去焦点事件，保存优惠券的数量
	window.onBlur = function(obj,couponId,availableQuantity,totalQuantity){
		if(obj.value != null && obj.value != ''){
			if (!(/^[0-9]*$/.test(obj.value))) { 
				$(obj).alert("请输入大于等于0的整数！");
				return; 
			} 
		}
		if(totalQuantity != 0){
			if(obj.value > availableQuantity){
				$(obj).alert("券数量不足！");
				$(obj).focus();
				$(obj).val('');
				return;
			}
		}
		if(obj.value){
			couponInfoMap[couponId] = obj.value;
		}else{
			delete couponInfoMap[couponId];
		}
		var params = JSON.stringify(couponInfoMap);
		$("#give_coupons").val(params);
	};
	
	//送券操作列
	window.formatProgress_coupon = function(value,row,index){
		var couponId = row.couponId;
		var amount = couponInfoMap[couponId];
		if (amount){
			$('#activitySetting-coupon-table').datagrid("selectRecord", row.couponId);
	    	var s = '<input value="'+ amount +'" onblur="onBlur(this,'+couponId+','+row.availableQuantity+','+row.totalQuantity+')" style="width:50px;border: solid 1px #000; margin-right: 5px;" />张/每次';
	    	return s;
		} else {
	    	var s = '<input value="" onblur="onBlur(this,'+couponId+','+row.availableQuantity+','+row.totalQuantity+')" style="width:50px;border: solid 1px #000; margin-right: 5px;"  />张/每次';
	    	return s;
		}
	};
	//券剩余
	window.available_show = function(value,row,index){
		if(row.totalQuantity == '0'){
			return '无限';
		}
		return value;
	};
	
	
	//查询产品
	window.showProduct = function showProduct(){
		$('#productTable').datagrid( {
			url : $.baseUrl+'/productController/showProductByExample.json',
			idField : 'productId',
			onLoadSuccess : function(result) {
				if(result) {
					if(productTemp_id.length != 0){
						$.each(productTemp_id,function(index,value){
							$('#productTable').datagrid('selectRecord',value);
						});
					}
				}
			},
			onLoadError : function() {
				
			}
		});
	};
	
	
	function initBtn(){
		//返回
		 $("#backBtn-form-activitySetting").click(function () {
			 if(pageTag=="instanceManage"){
				common.Page.loadCenter('activity/activityInstanceManage.html');
			 }else{
				 if(parentTag == "0"){
					 common.Page.loadCenter('activity/activityManage.html');
				 }else{
					 common.Page.loadCenter('activity/consumeActivity.html');
				 }
			 }
		 });
		 
		//下一步
		 $("#nextBtn-form-activitySetting").click(function () {
			 var ck = checkActivity("1");
			 if(ck){
				 $("#backBtn-form-activitySetting").hide();
				 $("#nextBtn-form-activitySetting").hide();
				 $("#ul-show-first").hide();
				 $("#div-title-first").hide();
				 
				 $("#ul-show-second").show();
				 $("#aboveBtn-form-activitySetting").show();
				 $("#saveBtn-form-activitySetting").show();
				 $("#div-title-second").show();
			 }
		 });
		 
		//上一步
		 $("#aboveBtn-form-activitySetting").click(function () {
			 $("#backBtn-form-activitySetting").show();
			 $("#nextBtn-form-activitySetting").show();
			 $("#ul-show-first").show();
			 $("#div-title-first").show();
			 
			 $("#ul-show-second").hide();
			 $("#aboveBtn-form-activitySetting").hide();
			 $("#saveBtn-form-activitySetting").hide();
			 $("#div-title-second").hide();
		 });
	}
	
	return {
		init: init
	};
});





