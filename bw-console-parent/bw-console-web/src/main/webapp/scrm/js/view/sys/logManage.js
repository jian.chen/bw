define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		initSelect();
		//初始化操作人下拉框
		initSelectById('selectId','/sysLogController/showSysUserList.do','userId','cn');
		
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
		$('#seachLog').click(function() {
			$('#sysLogTable').datagrid('load', serializeObject($('#sysLogForm')));
		});
	}

	function initDataGrid(){
		$('#sysLogTable').datagrid( {
			url : $.baseUrl+'/sysLogController/showSysLogList.do',
			idField : 'logId',
				onLoadSuccess : function(result) {
					if(result) {
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	return {
		init: init
	}

});

function rowformater(value,row,index){
	return "<a href='javascript:showLogDetail("+row.logId+");' target='_blank'>查看</a>";
}

//转换hhiiss为hh:ii:ss
function getLogTime(value){
	var str;
	str = value.substring(0,2)+":"+value.substring(2,4)+":"+value.substring(4,6);
	return str;
}

function showLogDetail(logId){
	$.ajax({
		async:false,
		url:$.baseUrl+'/sysLogController/showLogById.do',
		type:"POST",
		data:"logId="+logId,
		datatype:"json",
		success:function(result){
			if(result){
				alert(result);
				//result = eval('(' + result + ')');
			}else{
				alert('出错了！');
			}
		}
	});
}
