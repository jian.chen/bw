define(['common'], function(common) {
	var originalUrl;
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		$("#upPhoto").click(function(){
			$("#memberCard_img_path").attr("src","");
			$("#card_Detail").show();
		})
		
		$("#score").click(function(){
			if($("#memberCard_img_path").attr("src") == ""){
				alert("请先上传图片");
				return;
			}
			confirm("开始分析请稍后，如果长时间无结果请再次点击分析 ");
		    var api = new FacePP('a71b4abc0e513c8cd9ce663b40cd3f5d', 'GrH2YhUFR02c4u7KnVkMQMPv-yACHcfj');
		    api.request('detection/detect', {
		      url: 'http://scrm.codeorg.cn:8086/scrm-web-main/scrm/img/face/face.jpg',
		      attribute : 'glass,pose,gender,age,race,smiling'
		    }, function(err, result) {
		    	$.ajax({
					type : "post",
					url : $.baseUrl + "/activityController/savePhotoResult",
					dataType : "json",
					data : {
						photoResult : JSON.stringify(result, null, 2),
						error : err
					},
					success : function(data){
						
					}
				})
				
		      if (err) {
		        return;
		      }
		      $("#response").html("");
		      var obj = JSON.parse(JSON.stringify(result, null, 2));
		      var tempHtml = '';
		      if(obj.face[0] == null){
		    	  tempHtml += '<div><span>您的照片未能识别出，请重新上传，谢谢！</span></div>'
		      }else{
			      tempHtml += '<table>';
			      tempHtml += '<tr><td><span>照片分析结果:</span></td></tr>';
			      for(var i=0,j=1;i<obj.face.length;i++,j++){
			    	  tempHtml += '<tr><td><span>照片中第'+ j +'位人物:</span></td></tr>';
			    	  tempHtml += '<tr><td><span>年龄：</span></td><td>' + obj.face[i].attribute.age.value + '±' + obj.face[i].attribute.age.range +'</td></tr>';
				      tempHtml += '<tr><td><span>性别：</span></td><td>' + obj.face[i].attribute.gender.value + '</td></tr>';
				      tempHtml += '<tr><td><span>眼镜：</span></td><td>' + obj.face[i].attribute.glass.value + '</td></tr>';
				      tempHtml += '<tr><td><span>人种：</span></td><td>' + obj.face[i].attribute.race.value + '</td></tr>';
				      tempHtml += '<tr><td><span>微笑度(0~100)：</span></td><td>' + obj.face[i].attribute.smiling.value + '</td></tr>';
			      }
			      tempHtml += '</table>';
		      }
		      $("#response").append(tempHtml);
		    });
			
		})
		
		//验证表格上传格式
		$("#card_image").uploadPreview({Callback: function () { }});
		
		//上传图片按钮
		$('#card_upload').click(function() {
			uploadcardimage();
		});
		//取消按钮
		$('#card_cansle').click(function() {
			$("#closeCardButton").click();
		});
		
		$('#card_image').change(function() {
			$("#memberCard_fileSpan").text($(this).val());
		});
	}
	
	//上传图片
	function uploadcardimage(){
		if($("#card_image").val() == null || $("#card_image").val() == ""){
			alert("请选择文件！");
			return;
		}
		$('#cardDetailForm').form('submit', {    
		    url : $.baseUrl+'/activityController/uploadPhoto.do',    
		    success:function(result){
		    	result = eval('('+result+')');
		    	if(result=="不被允许上传的文件格式!"){
		    		alert("此文件格式不允许上传!");
		    	}if(result=="请使用.jpg的图片格式!"){
		    		alert("请使用.jpg的图片格式!");
		    	}
		    	else{
		    		$("#imgUrl_Detail").val($.rootPath+result);
			    	$("#memberCard_img_path").attr("src",$.rootPath+result+"?"+Math.random()*10000);
			    	alert("上传成功!");
		    	}
		    	
		    }    
		}); 
	}
	
	return {
		init: init
	}

});


