/*
 var zNodes =[
 { id:1, pId:0, name:"ordinary parent", t:"I am ordinary, just click on me", open:true},
 { id:11, pId:1, name:"leaf node 1-1", t:"I am ordinary, just click on me"},
 { id:12, pId:1, name:"leaf node 1-2", t:"I am ordinary, just click on me"},
 { id:13, pId:1, name:"leaf node 1-3", t:"I am ordinary, just click on me"},
 { id:2, pId:0, name:"strong parent", t:"You can click on me, but you can not click on the sub-node!", open:true},
 { id:21, pId:2, name:"leaf node 2-1", t:"You can't click on me..", click:false},
 { id:22, pId:2, name:"leaf node 2-2", t:"You can't click on me..", click:false},
 { id:23, pId:2, name:"leaf node 2-3", t:"You can't click on me..", click:false},
 { id:3, pId:0, name:"weak parent", t:"You can't click on me, but you can click on the sub-node!", open:true, click:false },
 { id:31, pId:3, name:"leaf node 3-1", t:"please click on me.."},
 { id:32, pId:3, name:"leaf node 3-2", t:"please click on me.."},
 { id:33, pId:3, name:"leaf node 3-3", t:"please click on me.."}
 ];
 */

//初始化下拉数据
function initTreeData(divId, id, url, type) {
	var setting1 = {
		data: {
			simpleData: {
				enable: true
			}
		},
		treeNodeKey: "id", //在isSimpleData格式下，当前节点id属性
		treeNodeParentKey: "pid", //在isSimpleData格式下，当前节点的父节点id属性
		showLine: true, //是否显示节点间的连线
		check: {
			enable: true,
			chkboxType: {"Y": "s", "N": "s"}
		}
	};

	var setting2 = {
		data: {
			simpleData: {
				enable: true
			}
		},
		treeNodeKey: "id", //在isSimpleData格式下，当前节点id属性
		treeNodeParentKey: "pid", //在isSimpleData格式下，当前节点的父节点id属性
		showLine: true, //是否显示节点间的连线
		check: {
			enable: false
		}
	};

	$('#' + divId).show();
	$.ajax({
		type: "get",
		url: $.baseUrl + url,
		dataType: "json",
		data: {
			//memberTagIds: addTagIdArrEdit.toString(),
			//memberGroupIds: addGroupIdArr.toString()
		},
		async: false,
		success: function (data) {
			zNodes = data;
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
	});
	if (type == 1) {
		$.fn.zTree.init($("#" + id), setting1, zNodes);
	} else if (type == 2) {
		$.fn.zTree.init($("#" + id), setting2, zNodes);
	}

}

//treeType:1/多选;2/单选
function getSelectId(id, treeType) {
	var selectIds = "";
	var treeObj = $.fn.zTree.getZTreeObj(id);
	var nodes;
	if (treeType == 1) {
		nodes = treeObj.getCheckedNodes();
	} else if (treeType == 2) {
		nodes = treeObj.getSelectedNodes();
	}

	for (var i = 0; i < nodes.length; i++) {
		selectIds += nodes[i].id + ",";
	}
	if (selectIds.length > 0) {
		selectIds = selectIds.substring(0, selectIds.length - 1);
	}
	return selectIds;
}

function getSelectName(id, treeType) {
	var selectNames = "";
	var treeObj = $.fn.zTree.getZTreeObj(id);
	var nodes;
	if (treeType == 1) {
		nodes = treeObj.getCheckedNodes();
	} else if (treeType == 2) {
		nodes = treeObj.getSelectedNodes();
	}
	for (var i = 0; i < nodes.length; i++) {
		selectNames += nodes[i].name + ",";
	}
	if (selectNames.length > 0) {
		selectNames = selectNames.substring(0, selectNames.length - 1);
	}
	return selectNames;
}

function getSelectAttr(id, attrName, treeType) {
	var attrs = "";
	var treeObj = $.fn.zTree.getZTreeObj(id);
	var nodes;
	if (treeType == 1) {
		nodes = treeObj.getCheckedNodes();
	} else if (treeType == 2) {
		nodes = treeObj.getSelectedNodes();
	}
	for (var i = 0; i < nodes.length; i++) {
		attrs += nodes[i][attrName] + ",";
	}
	if (attrs.length > 0) {
		attrs = attrs.substring(0, attrs.length - 1);
	}
	return attrs;
}

function getSelectNode(id, treeType) {
	var treeObj = $.fn.zTree.getZTreeObj(id);
	if (treeType == 1) {
		return treeObj.getCheckedNodes();
	}
	return treeObj.getSelectedNodes()[0];
}

//传入区域id&区域树id,选中该区域
function selectOrg(orgId, treeId) {
	var treeObj = $.fn.zTree.getZTreeObj(treeId);
	var node = treeObj.getNodeByParam("id", orgId);
	treeObj.selectNode(node);
}

//设置勾选
function checkNode(ids, treeId) {
	var tree = $.fn.zTree.getZTreeObj(treeId);
	for (var i = 0; i < ids.length; i++) {
		tree.checkNode(tree.getNodeByParam("id", ids[i]), true);
	}
}
 
