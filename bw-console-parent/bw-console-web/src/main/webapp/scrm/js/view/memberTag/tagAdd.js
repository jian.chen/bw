define(['common'], function(common) {
	function init() {
		
		common.Page.resetMain();
		initSelect();
		
		//区域门店弹框
		$(document).on("click","#equalOrgIdTag", function(){
			initTreeData("areaChooseManager","areaTreeManager","/orgController/testTree.do","1");
			var ids = $("[name='equalOrgIdTag']").val();
			if(ids){
				var idArr = ids.split(",");
				checkNode(idArr,"areaTreeManager");
			}
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTreeManager","1");
			var selectNames= getSelectName("areaTreeManager","1");
			$("[name='equalOrgIdTag']").val(selectIds);
			$("[name='equalOrgName']").val(selectNames);
			$('#areaChooseManager').find(".detailed_close2").click();
		});
		
		$("#updateNode").change(function(){
			if($(this).val() == 1){
				$("#tag_member").attr("class","tag_line none");
				$("#tag_order").attr("class","tag_line none");
				removeAllSelect();
				return;
			}
			if($(this).val() == 2){
				$("#tag_member").attr("class","tag_line");
				$("#tag_order").attr("class","tag_line");
				return;
			}
			if($(this).val() == 3){
				$("#tag_member").attr("class","tag_line");
				$("#tag_order").attr("class","tag_line");
				return;
			}
		})
		
		$("[name='executeTypeRadio']").click(function() {
			$("[name='executeType']").val($(this).val());
			if($(this).val() == 1){
				$("#setUpdate").attr("class","clearfix none");
				$("#tagSelect").attr("class","clearfix none");
				$("#setNode").attr("class","clearfix none");
				$("[name='updateType']").val(""),
				$("[name='updateNode']").val(""),
				$("input[name='updateTypeRadio'][value='1']").prop("checked",'true');
				$("#updateNode option[value='0']").attr("selected",true);
				removeAllSelect();
			}
			if($(this).val() == 2){
				$("#setUpdate").attr("class","clearfix");
				$("#tagSelect").attr("class","clearfix");
			}
		});
		
		$("[name='updateTypeRadio']").click(function() {
			$("[name='updateType']").val($(this).val());
			if($(this).val() == 1){
				$("#setNode").attr("class","clearfix none");
			}
			if($(this).val() == 2){
				$("#setNode").attr("class","clearfix");
			}
		});
		
		$("#updateNode").change(function(){
			$("[name='updateNode']").val($(this).val());
		})
		
		$("#memberTagSaveBtn").click(function() {
			saveMemberTagAdd();
		});
		
		$("[name='memberTagName']").blur(function(){
			if($("[name='memberTagName']") != ""){
				$.ajax({ 
			        type: "post", 
			        url: $.baseUrl+'/memberTagController/showCountByName.do', 
			        dataType: "json",
			        data : {
			        	memberTagName: $(this).val()
			        },
			        async: false,
			        success: function (data) {
			        	if(data>0){
			        		alert("标签名称已被使用");
			        	}
			        }, 
			        error: function (XMLHttpRequest, textStatus, errorThrown) { 
			            alert(errorThrown); 
			        }
			    });
			}
	  	
	  });
		
		$("#delete_all").click(function(){
			removeAllSelect();
			$("#tag_del").prop("class","tag_line none");
		})
		
	}
	return {
		init: init
	}
});

function removeAllSelect(){
	$(".tag_line-ele > span").removeClass("active");
	$(".tag_view-panel div:last").siblings().remove();
}

function tagCondition(distinctConditionRule,distinctConditionValue,distinctConditionId)
{
	this.distinctConditionRule=distinctConditionRule;
	this.distinctConditionValue=distinctConditionValue;
	this.memberTagConditionId= null;
	this.memberTagId= null;
	this.distinctConditionId= distinctConditionId;
}

var tagCoditionList = new Array();

function getAllselect(){
	$(".tag_line-ele > span").filter(".active").each(function(i){
		//基本资料
		if($(this).prop('id') == "select_mobile"){
			select = new tagCondition("mobile",$(".tag_view-panel").find("input[name='mobileRadio']:checked").val(),10);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_gender"){
			select = new tagCondition("gender",$(".tag_view-panel").find("input[name='genderRadio']:checked").val(),10);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_age"){
			select = new tagCondition("age",$("#ageStart").val()+","+$("#ageEnd").val(),10);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_birthday"){
			select = new tagCondition("birthday",$("#birthdayStart").val().replaceAll("-", "")+","+$("#birthdayEnd").val().replaceAll("-", ""),10);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_register"){
			select = new tagCondition("register",$("#registerDateStart").val().replaceAll("-", "")+","+$("#registerDateEnd").val().replaceAll("-", ""),10);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_channel"){
			select = new tagCondition("channel",$("#channelId").val(),10);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_grade"){
			select = new tagCondition("grade",$("#equalGradeId").val(),10);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_orderLast"){
			select = new tagCondition("orderLast",$("#orderLastStart").val()+","+$("#orderLastEnd").val(),10);
			tagCoditionList[i] = select;
			return true;
		}
		//会员资料
		if($(this).prop('id') == "select_points"){
			select = new tagCondition("points",$("#pointsStart").val()+","+$("#pointsEnd").val(),20);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_couponFree"){
			select = new tagCondition("couponFree",$("#couponFreeStart").val()+","+$("#couponFreeEnd").val(),30);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_couponUsed"){
			select = new tagCondition("couponUsed",$("#couponUsedStart").val()+","+$("#couponUsedEnd").val(),31);
			tagCoditionList[i] = select;
			return true;
		}
		//交易数据
		if($(this).prop('id') == "select_orderStore"){
			select = new tagCondition("orderStore",$(".tag_view-panel").find("[name='equalOrgIdTag']").val(),40);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_orderCount"){
			var orderCountDateStart = $("#orderCountDateStart").val().replaceAll("-", "");
			var orderCountDateEnd = $("#orderCountDateEnd").val().replaceAll("-", "");
			select = new tagCondition("orderCount",orderCountDateStart+","+orderCountDateEnd+","+$("#orderCountStart").val()+","+$("#orderCountEnd").val(),41);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_orderGrand"){
			var orderGrandAmountDateStart = $("#orderGrandAmountDateStart").val().replaceAll("-", "");
			var orderGrandAmountDateEnd = $("#orderGrandAmountDateEnd").val().replaceAll("-", "");
			select = new tagCondition("orderGrand",orderGrandAmountDateStart+","+orderGrandAmountDateEnd+","+$("#orderGrandAmountStart").val()+","+$("#orderGrandAmountEnd").val(),42);
			tagCoditionList[i] = select;
			return true;
		}
		if($(this).prop('id') == "select_orderOne"){
			var orderOneAmountDateStart = $("#orderOneAmountDateStart").val().replaceAll("-", "");
			var orderOneAmountDateEnd = $("#orderOneAmountDateEnd").val().replaceAll("-", "");
			select = new tagCondition("orderOne",orderOneAmountDateStart+","+orderOneAmountDateEnd+","+$("#orderOneAmountStart").val()+","+$("#orderOneAmountEnd").val(),43);
			tagCoditionList[i] = select;
			return true;
		}
	});
}


//保存会员标签
function saveMemberTagAdd(){
	
	if($("[name='memberTagName']").val() == ''){
		$("[name='memberTagName']").alert("不能为空");
		return;
	}
	if($("[name='remark']").val() == ''){
		$("[name='remark']").alert("不能为空");
		return;
	}
	getAllselect();
	if(tagCoditionList.length > 0){
		if(confirm("您的标签附带条件,一旦保存不可修改,确定要保存吗？")){
			$.ajax({
				type: "post", 
				url:$.baseUrl+'/memberTagController/saveMemberTag.do',
				dataType: "json",
				data : {
					memberTagName : $("[name='memberTagName']").val(),
					remark : $("[name='remark']").val(),
					executeType : $("[name='executeType']").val(),
					updateType : $("[name='updateType']").val(),
					updateNode : $("[name='updateNode']").val(),
					selectList : JSON.stringify(tagCoditionList)
				},
				success:function(data){  
			    	if(data.status=='200'){
			    		$(".detailed_close").closest('.mask, .mask_in').hide();
			    		alert(data.message);
			    		$('#memberTagTable').datagrid('load');
			    	}else{
			    		alert(data.message);
			    	}
			    	tagCoditionList = [];
			    }  
			})
		}else{
			return;
		}
	}else{
		if($("[name='executeType']").val() == '2'){
			alert("请至少选择一个条件");
			return;
		}
		$.ajax({
			type: "post", 
			url:$.baseUrl+'/memberTagController/saveMemberTag.do',
			dataType: "json",
			data : {
				memberTagName : $("[name='memberTagName']").val(),
				remark : $("[name='remark']").val(),
				executeType : $("[name='executeType']").val(),
				updateType : $("[name='updateType']").val(),
				updateNode : $("[name='updateNode']").val(),
				selectList : JSON.stringify(tagCoditionList)
			},
			success:function(data){  
		    	if(data.status=='200'){
		    		$(".detailed_close").closest('.mask, .mask_in').hide();
		    		alert(data.message);
		    		$('#memberTagTable').datagrid('load');
		    	}else{
		    		alert(data.message);
		    	}
		    	tagCoditionList = [];
		    }  
		})
	
	}
	
	
}