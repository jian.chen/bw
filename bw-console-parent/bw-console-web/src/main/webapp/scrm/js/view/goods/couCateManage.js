define(['common'], function(common) {
	
	var type;
	var couponCategoryId;
	var couponCategoryIds;
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		//查看
		$("#showCouponCategory").click(function(){
			var mm = check();
			
			if(mm == 1){
				type = 'show';
				isAuth("/goods/couCateAdd.html", function() {common.Page.loadMask('goods/couCateAdd.html')});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		
		//编辑
		$("#editCouponCategory").click(function(){
			
			var mm = check();
			
			if(mm == 1){
				type = 'edit';
				isAuth("/couponCategoryController/saveCouponCategory.do", function() {common.Page.loadMask('goods/couCateAdd.html')});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
			
		});

		//新建
		$("#saveCouponCategory").click(function(){
			type = 'save';
			isAuth("/couponCategoryController/saveCouponCategory.do", function() {common.Page.loadMask('goods/couCateAdd.html')});
		});

		//删除
		$("#deletedCouponCategory").click(function(){
			var mm = check();
			
			if(mm != 2){
				if(confirm("确认删除？")){
					if(mm == 1){
						couponCategoryIds = couponCategoryId;
					}
					deleted();
				}
			}else{
				alert("请选择条目！");
			}	
		});
		
		
	}
	
	function initDataGrid(){
		$('#couponCategoryTable').datagrid( {
			url : $.baseUrl+'/couponCategoryController/showCouponCategoryList.do',
			idField : 'couponCategoryId',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	function check(){
		var selected = $('#couponCategoryTable').datagrid('getChecked');
		couponCategoryIds = '';
		couponCategoryId = '';
		
		if(selected.length==1){
			couponCategoryId = selected[0].couponCategoryId;
			statusId = selected[0].statusId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			for(var i=0; i<selected.length; i++){
				if (couponCategoryIds != ''){ 
					couponCategoryIds += ',';
			    }
				couponCategoryIds += selected[i].couponCategoryId;
			}
			return 3;
		}

	}
	
	function deleted(){
		isAuthForAjax("/couponCategoryController/removeCouponCategoryList.do?couponCategoryIds="+couponCategoryIds,
			"POST", "", "JSON", {}, function (result) {
				var data = eval('(' + result + ')');
				couponCategoryIds="";
				couponCategoryId="";
				alert(data.message);
				$("#couponCategoryTable").datagrid('clearChecked');
				$("#couponCategoryTable").datagrid('reload');
			}, function (){
				alert("request error");
			});
	}
	
	window.shelves_couCate = function shelves_couCate(value, row, index){
		var statusTemp = $.COUPON_CATEGORY_STATUS.getText(row.statusId);
		
		if(statusTemp == '上架'){
			
			return "<button id='"+row.couponCategoryId+"_start' type='button' onclick='javascript:updateStatus(&apos;"+row.couponCategoryId+"&apos;,&apos;"+row.statusId+"&apos;)' class='border-btn'>下架</a>";
		}else{
			return "<button id='"+row.couponCategoryId+"_start' type='button' onclick='javascript:updateStatus(&apos;"+row.couponCategoryId+"&apos;,&apos;"+row.statusId+"&apos;)' class='solid-btn'>上架</a>";
		}
	}
	
	window.updateStatus = function(couponCategoryId,statusId){
		
		var url=$.baseUrl+"/couponCategoryController/updateCouponCategoryStatus.do";
		$.ajax({
			type:"POST",
			url:url,
			dataType:"JSON",
			data:{couponCategoryId : couponCategoryId, statusId : statusId},
			success:function(result){
				var data = eval('(' + result + ')');
				alert(data.message);
				$("#couponCategoryTable").datagrid('load');
			},
			error:function(){
				alert("request error");
			}
		});
	}	
	

	return {
		init: init,
		getType : function(){
			return type;
		},
		getCouponCategoryId : function(){
			return couponCategoryId;
		}
	}

});

