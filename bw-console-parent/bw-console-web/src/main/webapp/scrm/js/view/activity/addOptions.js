define(['common'], function(common) {
	
	function init(){
		$("#saveOptions").click(function(){
			var optionName = $("#optionName").val();
			var optionImg = $("#optionImg").val();
			var optionDetail = $("#optionDetail").val();
			var titleId = manageVoteId;
			if(optionName == ""){
				$("#optionName").alert("不能为空");
				return;
			}
			$.ajax({
				type : "post",
				url : $.baseUrl + "/VoteController/saveOptions",
				dataType : "json",
				data : {
					optionName : optionName,
					optionImg : optionImg,
					optionDetail : optionDetail,
					titleId : titleId
				},success : function(data){
					alert(data.message);
					$(".detailed_close").closest('.mask, .mask_in').hide();
					$('#voteOptionsTable').datagrid('reload');
				}
			})
		})
	}
	
	return {
		init: init
	}
	
	
});
