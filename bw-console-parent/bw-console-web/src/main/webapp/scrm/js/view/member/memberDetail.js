define(['common'], function(common) {
	function init() {
		disableForm("memberBaseInfoForm","true");
		disableForm("saveMemberExtInfoForm","true");
		disableForm("memberExtAccountForm","true");
		
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelectDetailed();
		initDate();
		initInput();
		//disableForm("memberBaseInfoForm");
		
		//绑定切换
		$('#userDetailedTabBtnList').bindTab('#userDetailedTabContent');
		
		//标签切换查询
		 $("#userDetailedTabBtnList li").each(function() {
             $(this).click(function() {
                 var title = $(this).text();
                 if(title.trim()=='基本信息'){
                 	queryMemberBaseInfo();
                 }else if(title.trim()=='联系信息'){
                 	queryMemberContantInfo();
                 }else if(title.trim()=='标签分组'){
                 	queryMemberTagAndGroup();
                 }else if(title.trim()=='其他信息'){
                 	queryMemberExtInfo();
                 }else if(title.trim()=='积分历史'){
                 	queryMemberPoints();
                 }else if(title.trim()=='消费记录'){
                 	queryMemberOrderHeader();
                 }else if(title.trim()=='优惠券'){
                 	queryMemberCouponInstance();
                 }else if(title.trim()=='互动历史'){
                 	queryMemberWechatMessage();
                 }
			});
		});

		
		$('#interactionTabList').bindTab('#interactionTabContent');

		
		//查询会员积分记录
		$('#pointsSearchBtn').click(function() {
			$('#memberPointsTable').datagrid('load', serializeObject($('#pointsSearchForm')));
		});
		
		//查询会员消费记录
		$('#memberOrderHeaderBtn').click(function() {
			$('#memberOrderHeaderTable').datagrid('load', serializeObject($('#memberOrderHeaderForm')));
		});
		
		//查询会员优惠劵
		$('#memberCouponInstanceBtn').click(function() {
			$('#memberCouponInstanceTable').datagrid('load', serializeObject($('#memberCouponInstanceForm')));
		});
		
		//查询会员微信互动消息
		$('#memberWechatMessageBtn').click(function() {
			queryMemberWechatMessage();
		});
		
		//查询会员短信消息
		$('#memberSmsInstanceBtn').click(function() {
			queryMemberSmsInstance();
		});
		
		queryMemberBaseInfo();
	}
	return {
		init: init
	}
});

//查询会员基本信息
function queryMemberBaseInfo(){
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/showMemberDetail.do?memberId='+memberId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	data = data.obj;
        	if(data.memberPhoto){
        		$("#memberPhoto").attr('src',data.memberPhoto); 
        	}
        	
        	$("#lastLoginTime").text(getDateString(data.lastLoginTime));
        	$("#channelId").text($.Member_Channel.getText(data.channelId));
        	$("#orgId").text(data.orgName==null?"":data.orgName);
        	$("#registerTime").text(getDateString(data.registerTime));
        	$("#registerIp").text(data.registerIp==null?'':data.registerIp);
        	$("#status").text($.Member_Status.getText(data.statusId));
        	
        	$("[name='memberId']").val(data.memberId);
        	$("[name='memberCode']").val(data.memberCode);
        	
        	$("[name='channelId']").val(data.channelId);
        	$("[name='channelId']").next().val($.Member_Channel.getText(data.channelId));
        	
        	$("[name='orgName']").val(data.orgName);
        	
        	$("[name='memberName']").val(data.memberName);
        	$("[name='userName']").val(data.userName);
        	
        	$("[name='gender']").val(data.gender);
        	$("[name='gender']").next().val($.gender.getText(data.gender));
        	
        	$("[name='mobile']").val(data.mobile);
        	$("[name='birthday']").val(getDateString(data.birthday));
        	$("[name='cardNo']").val(data.cardNo);
        	
        	$("[name='gradeId']").val(data.gradeId);
        	$("[name='gradeId']").next().val($.Member_Grade.getText(data.gradeId));
        	
        	$("[name='geoName']").val(data.geoName);
        	$("[name='email']").val(data.email);
        	$("#languageSelect").find("option[value='" + data.language + "']").attr("selected",true);
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
    initGradeConvert();
}


//初始化会员等级变动记录
function initGradeConvert(){
	$('#memberGradeConvertTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberGradeConvert.do?memberId='+memberId, 
		singleSelect : true,
		onLoadSuccess : function(result) {
			if(result) {
				
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询会员联系信息
function queryMemberContantInfo(){
    $('#memberAddressTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberContactInfo.do?memberId='+memberId, 
		singleSelect : true,
		onLoadSuccess : function(result) {
			if(result) {
				$("[name='wechat']").val(result.memberContact.wechat);
        		$("[name='qq']").val(result.memberContact.qq);
        		$("[name='sina']").val(result.memberContact.sina);
        		//$("[name='msn']").val(result.memberContact.msn);
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询会员标签和分组
function queryMemberTagAndGroup(){
    $.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/showMemberTagAndGroup.do?memberId='+memberId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("#memberTagList").html("");
        	$("#memberGroupList").html("");
        	$.each(data.tagList, function(index, value){
		      	$("#memberTagList").append("<span class='labelChoose_span'>"+value.memberTagName+"<span class='icon icon_close_active' >" 
		      	+"</span></span>");
		    });
		    
		    $.each(data.groupList, function(index, value){
        		$("#memberGroupList").append("<span class='labelChoose_span'>"+value.memberGroupName+"<span class='icon icon_close_active' >" 
		       +"</span></span>");
		    });
		    
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        }
    });
}

//查询会员其他信息
function queryMemberExtInfo(){
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/showMemberExtInfo.do?memberId='+memberId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("[name='profession']").val(data.profession);
        	$("[name='profession']").next().val($.Profession.getText(data.profession));
        	$("[name='zodiac']").val(data.zodiac);
        	$("[name='zodiac']").next().val($.Member_Zodiac.getText(data.zodiac));
        	
        	$("[name='chineseZodiac']").val(data.chineseZodiac);
        	$("[name='chineseZodiac']").next().val($.Member_Chinese_Zodiac.getText(data.chineseZodiac));
        	$("[name='blood']").val(data.blood);
        	$("[name='blood']").next().val($.Member_Blood.getText(data.blood));
        	
        	$("[name='isMarried']").val(data.isMarried);
        	$("[name='isMarried']").next().val($.IsMarried.getText(data.isMarried));
        	
        	$("[name='remark']").val(data.remark);
        	
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

//查询会员积分历史
function queryMemberPoints(){
     $('#memberPointsTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberPoints.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		queryParams : serializeObject($('#pointsSearchForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询会员消费记录
function queryMemberOrderHeader(){
     $('#memberOrderHeaderTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberOrderHeader.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		method : 'post',
		queryParams : serializeObject($('#memberOrderHeaderForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询会员优惠劵
function queryMemberCouponInstance(){
     $('#memberCouponInstanceTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberCouponInstance.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		method : 'post',
		queryParams : serializeObject($('#memberCouponInstanceForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询微信互动消息
function queryMemberWechatMessage(){
     $('#memberWechatMessageTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberWechatMessage.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		rownumbers:true,
		method : 'get',
		//queryParams : '',
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询短信消息
function queryMemberSmsInstance(){
     $('#memberSmsInstanceTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberSmsInstance.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		rownumbers:true,
		method : 'get',
		//queryParams : '',
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

