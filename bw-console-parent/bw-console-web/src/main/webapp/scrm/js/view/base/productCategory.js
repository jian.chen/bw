var productCategoryId;
var cateType;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		//初始化表格
		initCateGrid();
		
		//查看
		$("#cateView").click(function(){
			if(check()==1){
				cateType = 'show';
				common.Page.loadCenter('base/categoryShowAndEdit.html');
			}else if(check()==2){
				$.messager.alert("提示","请选择条目");
			}
		})
		
		//编辑
		$("#cateEdit").click(function(){
			if(check()==1){
				cateType = 'edit';
				common.Page.loadCenter('base/categoryShowAndEdit.html');
			}else if(check()==2){
				$.messager.alert("提示","请选择条目");
			}
		})
		
		//新增
		$("#cateNew").click(function(){
			cateType = 'new';
			common.Page.loadCenter('base/categoryShowAndEdit.html');
		})
		
		//删除
		$("#cateDel").click(function(){
			if(check()==1){
				deleteCate();
			}else if(check()==2){
				$.messager.alert("提示","请选择条目");
			}
		})
		
		//查看类别中的产品
		$("#CateForProduct").click(function(){
			if(check()==1){
				
			}else if(check()==2){
				$.messager.alert("提示","请选择条目");
			}
		})
		
		//导入
		$("#cateImport").click(function(){
			$('#importCateMask').show();
		})
		
		//下载模板
		$('#downImportTemp').click(function() {
			downTemp();
		});
		
		//文件路径
		$('#fileInput').change(function() {
			$("#fileSpan").text($(this).val());
		});
		
		//导入提交
		$('#importBtn').click(function() {
			importData();
		});
	}
	return {
		init: init
	}
	//表格初始化
	function initCateGrid(){
		$('#productCategoryliTable').datagrid({
			url : $.baseUrl+'/productCategoryController/showProductCategory.do',
			method :'post',// 请求类型 
			singleSelect : true,
			loadMsg:"数据加载中...",
			onLoadSuccess : function(result) {
			},
			onLoadError : function() {
			}
			});
		}
	
	function check(){
		var selected = $('#productCategoryliTable').datagrid('getChecked');
		if(selected.length==1){
			productCategoryId = selected[0].productCategoryId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}
	}
	
	function deleteCate(){
		$.ajax({
			url:$.baseUrl+'/productCategoryController/removeCategory.do',
			type:"POST",
			data:{
				productCategoryId : productCategoryId
			},
			dataType:"json",
			success:function(result){
				if(result.status == '200'){
					$.messager.alert('提示','删除成功！');
					$("#productCategoryliTable").datagrid('reload');
				}else{
					$.messager.alert('提示','删除失败，请重试！');
				}
			}
		});
	}
	
	//下载模板
	function downTemp() {
		window.location=$.baseUrl+'/productController/downCategoryTemp.do';
	}
	
	function importData() {
		$('#importForm').form('submit', {    
		    url:$.baseUrl+'/productController/importCategoryData.do',  
		    onSubmit: function(){    
		    },    
		    success:function(result){
		    	var result = $.parseJSON(result);
		    	$.messager.alert("提示",result.message);
		    	if(result.status == '200'){
		    		$('#importCateMask').find(".detailed_close").click();
		    		$("#productCategoryliTable").datagrid('reload');
		    	}
		   }    
		});  
	}
	
});