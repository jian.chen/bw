define(['common','view/sys/smsChannelManage'], function(common,smsChannelManage) {
	var smsName;
	function init() {
		
		//初始化输入框验证
		initInput();
		
		var savetype;
		if(smsChannelManage.getType() == 'show'){
			$("#sc_title").html('SMS网关详情');
			$("#saveSms_Detail").hide();
			show();
			//表单禁用
			disableForm('smsChannelForm',true);
		}else if(smsChannelManage.getType() == 'edit'){
			$("#sc_title").html('SMS网关编辑');
			$("#smsChannelId1").val(smsChannelManage.getSmsChannelId());
			savetype = 'edit';
			show();
		}else{
			$("#sc_title").html('SMS网关新建');
			savetype = 'save';
		}
		
		//通道名称不允许重复
		$("#smsChannelName1").blur(function(){
			if($("#smsChannelName1").val() != smsName){
				if($.inArray($("#smsChannelName1").val(),smsChannelManage.getSmsNameList()) >= 0){
					$("#smsChannelName1").alert('此通道名称已存在！');
				}
			}
		});
		
		$("#saveSms_Detail").click(function(){
			var ck = formCheck($("#smsChannelForm"));
			if(ck){
				if(savetype == 'edit'){
					if($("#smsChannelName1").val() != smsName){
						if($.inArray($("#smsChannelName1").val(),smsChannelManage.getSmsNameList()) >= 0){
							$("#smsChannelName1").alert('此通道名称已存在！');
							return;
						}
					}
					editSmsChannel();
				}else{
					if($.inArray($("#smsChannelName1").val(),smsChannelManage.getSmsNameList()) >= 0){
						$("#smsChannelName1").alert('此通道名称已存在！');
						return;
					}
					saveSmsChannel();
				}
			}else{
				return;
			}
		});
	};

	function show(){
		$.ajax({
			url : $.baseUrl+'/smsChannelController/showSmsChannelById.do',
			type:"POST",
			data:"smsChannelId="+smsChannelManage.getSmsChannelId(),
			datatype:"json",
			success:function(result){
				if(result){
					result = eval('(' + result + ')');
					
					$("#smsChannelName1").val(result.smsChannelName);
					smsName = result.smsChannelName;
					$("#sms_appId1").val(result.appId);
					$("#sms_appSecret1").val(result.appSecret);
					$("#url1").val(result.url);
					$("#smsChannelDesc1").val(result.smsChannelDesc);
					
					
				}else{
					alert('出错了！');
				}
			}
		});
	}


	function saveSmsChannel(){
		$("#dataLoad").show();
		$.ajax({
			url:$.baseUrl+'/smsChannelController/saveSmsChannel.do',
			type:"POST",
			data:serializeObject($('#smsChannelForm')),
			datatype:"json",
			success:function(result){
				if(result == 1){
					$("#dataLoad").hide();
					alert('新建成功！');
					$("#closeSmsButton").click();
					$("#smsChannelTable").datagrid('load');
				}else{
					alert('新建失败，请重试！');
				}
			}
			
		});
	}

	function editSmsChannel(){
		$("#dataLoad").show();
		$.ajax({
			url:$.baseUrl+'/smsChannelController/editSmsChannel.do',
			type:"POST",
			data:serializeObject($('#smsChannelForm')),
			datatype:"json",
			success:function(result){
				if(result == 1){
					$("#dataLoad").hide();
					$("#closeSmsButton").click();
					alert('修改成功！');
					$('#smsChannelTable').datagrid('reload');
				}else{
					alert('修改失败，请重试！');
				}
			}
		});
		
	}
	
	return {
		init: init
	}
});

