define(['common'], function(common) {
	
	function init() {
		instanceAllMember();
		showCount();
		$('#goback').click(function(){
			backUrl = 'activity/question.html';
			clickPoints = 'question';
			common.Page.loadCenter('activity/publicManage.html');
		});
		
	}
	
	return {
		init: init
	}
	
});

function showCount(){
	$.ajax({
		type : 'post',
		url : $.baseUrl + '/QuestionController/showQuestionCount',
		data : {
			questionnaireId : countQuestionId
		},
		dataType : 'json',
		success : function(data){
			var tempHtml = '';
			var num_list =['a','b','c','d','e'];
			for(var i = 0,j = 1;i<data.length,j<data.length+1;i++,j++){
				for(var key in data[i]){
					if(key == 1){
						var sum = 0;
						for(var s = 0;s < data[i][key].length;s++){
							sum += data[i][key][s].count;
						}
						tempHtml += "<div class='title-category'>" + j + ".选择题测试</div>";
						tempHtml += "<table class='question-table'>";
						for(var x = 0;x < data[i][key].length;x++){
							tempHtml += "<tr>";
							tempHtml += "<td class='person-total'>选择<span>" + data[i][key][x].count + "</span>人</td>";
							tempHtml += "<td class='percent'><span class='percent-total'><span class='percent-length' style='width:" + data[i][key][x].count*100/sum + "%'></span></span><span>" + data[i][key][x].count*100/sum + "%</span></td>";
							tempHtml += "<td class='score'>选项" + num_list[x] + "</td>";
							tempHtml += "</tr>";
						}
						tempHtml += "</table>";
					}
					if(key == 2){
						var sum = 0;
						for(var s = 0;s < data[i][key].length;s++){
							sum += data[i][key][s].count;
						}
						tempHtml += "<div class='title-category'>" + j + ".打分测试</div>";
						tempHtml += "<table class='question-table'>";
						for(var x = 0;x<data[i][key].length;x++){
							tempHtml += "<tr>";
							tempHtml += "<td class='person-total'>选择<span>" + data[i][key][x].count + "</span>人</td>";
							tempHtml += "<td class='percent'><span class='percent-total'><span class='percent-length' style='width:" + data[i][key][x].count*100/sum + "%'></span></span><span>" + data[i][key][x].count*100/sum + "%</span></td>";
							tempHtml += "<td class='score'><div style='width:" + 16.8*data[i][key][x].answer + "'></div></td>";
							tempHtml += "</tr>";
						}
						tempHtml += "</table>";
					}
					if(key == 3){
						tempHtml += "<div class='title-category'>" + j + ".问答测试</div>";
						tempHtml += "<table class='question-table'>";
						for(var x = 0;x<data[i][key].length;x++){
							tempHtml += "<tr>";
							tempHtml += "<td class='person-total'>" + data[i][key][x].answer + "</td>";
							tempHtml += "</tr>";
						}
						tempHtml += "</table>";
					}
				}				
			}
			$('#countList').append(tempHtml);
		},error : function(){
			
		}
	})
}

function instanceAllMember(){
	$.ajax({
		type : 'post',
		url : $.baseUrl + '/QuestionController/instanceAllMember',
		data : {
			questionnaireId : countQuestionId
		},
		dataType : 'json',
		success : function(data){
			$("#memberAllCount").text(data.sum);
		},error : function(){
			
		}
	})
}


