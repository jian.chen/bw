define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		//初始化日期
		initDate();
		
		
		var rollNo = null;
		var rollType =null; 
		var fixedDate = null;
		var invalidTimeType = null;
		initPointsConf();
		$('#editBtn').click(function() {
			$("#points-rule-ul-detail").hide();
			$("#points-rule-ul-edit").show();
			
			$("input:radio[name='invalidTimeType'][value='"+invalidTimeType+"']").prop("checked",true);
			$("input:radio[name='invalidTimeType'][value='"+invalidTimeType+"']").trigger('change');
			
			$("#timeValue").val(rollNo);
			if(invalidTimeType=="2"){
				$("#dateTypeText").combobox("setValue",rollType);
				//年
				if(rollType=="1"){
					$("#editCleanTime_md").val(fixedDate);
					$("#cleanTime_div1").show();
					$("#cleanTime_day").hide();
				}//月
				else if(rollType=="2"){
					$("#cleanTime_div1").hide();
					$("#cleanTime_day").show();
					$("#daySelect").combobox('setValue',fixedDate);
				}
			}else if(invalidTimeType=="3"){
				$("#rollDate").show();
				$("#cleanDate").hide();
				$("#div-yearMonthDay").show();
				$("#div-yearMonth").hide();
				$("#dateTypeText-day").combobox('setValue',rollNo);
			}
   		 
			
		});
		
		$('#backBtn').click(function() {
			$("#points-rule-ul-detail").show();
			$("#points-rule-ul-edit").hide();
		});
		
		
		
		$("#dateTypeText").combobox({
			onSelect: function (obj,o) {
				if(obj.value=="1"){
					$("#cleanTime_div1").show();
					$("#cleanTime_day").hide();
				}else if(obj.value=="2"){
					$("#cleanTime_div1").hide();
					$("#cleanTime_day").show();
				}
			}
		});
		
		
		$("input:radio[name='invalidTimeType']").change(function (){
			
			if($(this).val() == 1){
				$("#rollDate").hide();
				$("#cleanDate").hide();
			}else if($(this).val() == 2){
				$("#rollDate").show();
				$("#cleanDate").show();
				$("#div-yearMonthDay").hide();
				$("#div-yearMonth").show();
			}else if($(this).val() == 3){
				$("#rollDate").show();
				$("#cleanDate").hide();
				$("#div-yearMonthDay").show();
				$("#div-yearMonth").hide();
			}
		});
		
		
		$("#pointsRate").prop("disabled", true);
		
		$("#discountIsActive").change(function() { 
			if(!$(this).prop("checked")){
				$("#pointsRate").val("");
				$("#pointsRate").prop("disabled", true);
			}else{
				$("#pointsRate").prop("disabled", false);
			}
		}); 
		
		$('#saveBtn').click(function() {
			var type = $('input[name="invalidTimeType"]:checked').val();
			
			if(type == '1'){
				$("#invalidTimeValue").val("last");
			}
			if(type == '2'){
				//年
				if($("#dateTypeText").combobox('getValue')=="1"){
					$("#invalidTimeValue").val($("#timeValue").val()+"/"+$("#dateTypeText").combobox('getValue')+"/"+$("#editCleanTime_md").val());
				}
				//月
				if($("#dateTypeText").combobox('getValue')=="2"){
					$("#invalidTimeValue").val($("#timeValue").val()+"/"+$("#dateTypeText").combobox('getValue')+"/"+$("#daySelect").combobox('getValue'));
				}
			}
			if(type == '3'){
				$("#invalidTimeValue").val($("#timeValue").val()+"/"+$("#dateTypeText-day").combobox('getValue'));
			}
			
			var ckActivity = formCheck($("#form-pointsConf"));
			if(ckActivity){
				/*if($("#discountIsActive").prop("checked") && !$("#pointsRate").val()){
					alert("请输入积分抵现折扣值");
					$("#pointsRate").focus();
					return;
				}*/
				
				$('#form-pointsConf').form('submit', {
					url:$.baseUrl+'/pointsConfController/savePointsConf.json',
					success:function(data){
						var result = $.parseJSON(data);
						alert(result.message);
						initPointsConf();
						$("#points-rule-ul-detail").show();
						$("#points-rule-ul-edit").hide();
					}
				});
			}
		});
		
		
		function initPointsConf(){
			 $.ajax({
	             type: "POST",   //访问WebService使用Post方式请求
	             url: $.baseUrl+'/pointsConfController/showPointsConf.json',
	             data: {},
	             dataType: 'json',
	             success: function (data) {
	            	 
	            	 if(!data.obj){
	            		 $("#pointsConf-status-show").text("未设置");
	            		 $("#pointsConf-invalidTimeType-show").text("未设置");
	            		 $("#pointsRate-show").text("未设置");
	            		 return;
	            	 }
	            	 
	            	 $("input[name='isActive'][value='"+data.obj.isActive+"']").attr("checked",true); 
	            	 $("input[name='invalidTimeType'][value='"+data.obj.invalidTimeType+"']").attr("checked",true);
	            	 if(data.obj.discountIsActive=="Y"){
	            		 $("#pointsRate-show").text("顾客消费时可用积分抵用现金，"+data.obj.pointsRate+"积分可抵用1元");
	            		 $("#pointsRate").val(data.obj.pointsRate);
	            		 $("#discountIsActive").prop("checked", true);
	            		 $("#pointsRate").prop("disabled", false);
	            	 }else{
	            		 $("#discountIsActive").prop("checked", false);
	            		 $("#pointsRate").prop("disabled", true);
	            		 $("#pointsRate-show").text("未设置");
	            		 $("#pointsRate").val("");
	            	 }
	            	 
	            	 if(data.obj.isActive == "Y"){
	            		 $("#pointsConf-status-show").text("已开启");
	            	 }else{
	            		 $("#pointsConf-status-show").text("已关闭");
	            	 }
	            	 
	            	 invalidTimeType = data.obj.invalidTimeType;
	            	 if(invalidTimeType == "1"){
	            		 $("#div-roll-show").hide();
	            		 $("#div-clearDate-show").hide();
	            		 $("#pointsConf-invalidTimeType-show").text("永久有效");
	            	 }else if(invalidTimeType == "2"){
	            		 $("#pointsConf-invalidTimeType-show").text("滚动日期+固定日期");
	            		 $("#div-roll-show").show();
	            		 $("#div-clearDate-show").show();
	            		 rollNo = data.obj.invalidTimeValue.split("/")[1];
	            		 rollType = data.obj.invalidTimeValue.split("/")[1];
	            		 fixedDate = data.obj.invalidTimeValue.split("/")[2];
	            		 $("#dateTypeText-show").text(rollNo+"/"+(rollType=="1"?"年":"月"));
	            		 $("#editCleanTime-show").text(fixedDate);
	            		
	            	 }else if(invalidTimeType == "3"){
	            		 $("#pointsConf-invalidTimeType-show").text("滚动日期");
	            		 $("#div-roll-show").show();
	            		 rollNo = data.obj.invalidTimeValue.split("/")[1];
	            		 rollType = data.obj.invalidTimeValue.split("/")[1];
	            		 $("#dateTypeText-show").text(rollNo+"/"+(rollType=="1"?"年":rollType=="2"?"月":"日ze"));
	            	 }
	            	 
	             }
	         });
		}
		
	}
	return {
		init: init
	};
});

