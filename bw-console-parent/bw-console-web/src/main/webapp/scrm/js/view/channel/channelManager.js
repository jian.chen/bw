var channelId;
var channelName;
var saveType;
var pageTag;
define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		$("#search").click(function(){
			initDataGrid();
		});
		
		//渠道编辑
		
		$('#chanelEdit').click(function(){
			if(checkSingle()){
				channelId=getCheckedId();
				$('#importCateMask').show();
				initChannelEditDataGrid();
				
			}
		});
		
		//编辑时现实的内容
		function initChannelEditDataGrid(){
			$.ajax({
				type : "get",
				url : $.baseUrl+"/channelController/showChannelDetail",
				data : "channelId="+channelId,
				dataType : "json",
				success : function(result){
					$('#channelCode').val(result.channel.channelCode);
					$('#channelName').val(result.channel.channelName);
					$('#channelId').val(result.channel.channelId);
				},
				error : function() {
					parent.layer.alert("出错了:(");
				}
				
			});
		}
		
		//渠道新建
		
		$('#chanelAdd').click(function(){
			$('#channelCode').val('');
			$('#channelName').val('');
			$('#channelId').val('');
			$('#importCateMask').show();
		});
		
		//保存按钮
		$("#channelEditSave").click(function(){
			var isValid = formCheck($("#channelEditForm"));
			if(isValid){
				isAuthForAjax("/channelController/saveOrUpdateChannel",
						"POST", "", "JSON", serializeObject($('#channelEditForm')), function (result) {
						if(result.status == '200'){
							alert(result.message);
							$("#searchChannelName").val(result.obj);
							$('#close_btn').click();
							initDataGrid();
						}else{
							alert(result.message);
						}
						}, function (){
							alert("request error");
						});
			}
		});
		
	}
	
	function initDataGrid(){
		$('#memberChannelTable').datagrid({
			url : $.baseUrl+'/channelController/queryChannelList',
			queryParams : {
				channelName : $("#searchChannelName").val()
			},
			method:'get',
			idField : 'channelId',
			singleSelect : true,
			pagination : true,
			pageNumber : 1,
			pageSize : 10,
			pageList : [10, 20,30, 40, 50 ],
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
				
			}
		})
	}
	window.channelManagerFormatProgressBtn = function (value,row,index){
		
		
		var s = '<button onclick="lookChannelManagerSettingBtn('+value+',\''+row.channelName+'\')" type="button" class="border-btn">发起</button>';
		//
		s = s + '<button onclick="setChannelManagerSettingBtn('+value+',\''+row.channelName+'\')" type="button" class="border-btn">管理</button>';
		return s;
	};
	//频道查看
	window.lookChannelManagerSettingBtn=function(channelId,channelName){
		this.channelId=channelId;
		this.channelName=channelName;
		saveType="save";
		pageTag='channelManage';
		common.Page.loadCenter('channel/channelSetting.html');
	}
	//频道设置
	window.setChannelManagerSettingBtn = function(channelId,channelName){
		this.channelId=channelId;
		this.channelName=channelName;
		common.Page.loadCenter('channel/channelInstanceManage.html');
	};
	
	//检查是否单选
	function checkSingle(){
		var selected = $('#memberChannelTable').datagrid('getChecked');
		if(selected.length==1){
			return true;
		}else{
			$.messager.alert("提示","请勾选一条记录!");
			return false;
		}

	}
	//获取选择Id
	function getCheckedId(){
		var selected = $('#memberChannelTable').datagrid('getChecked');
		var channelId='';
		for(var i=0; i<selected.length; i++){
		    if (channelId != ''){ 
		    	channelId += ',';
		    }
		    channelId += selected[i].channelId;
		}
		return channelId;
	}
	return {
		init: init
	}
	
});