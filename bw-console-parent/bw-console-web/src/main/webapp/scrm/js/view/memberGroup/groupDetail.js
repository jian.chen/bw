define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		if(isAll=='1'){
			$('#memberGroupItemRemoveBtn').hide();
		}else{
			$('#memberGroupItemRemoveBtn').show();
		}
		
		$('#memberGroupDetailSearchBtn').click(function() {
			$('#memberGroupDetailTable').datagrid('load', serializeObject($('#memberGroupDetailForm')));
		});
		
		
		//删除
		$('#memberGroupItemRemoveBtn').click(function() {
			var selected  = $('#memberGroupDetailTable').datagrid('getSelected');
			var memberId = '';
			if(selected){
				memberId = selected.memberId;
			}else{
				alert("请选择会员");
				return;
			}
			//查询详情页面赋值
			removeMemberGroupItem(memberGroupId,memberId);
		});
		
		initGroupDataGrid();
		
	}
	return {
		init: init
	}
});


function initGroupDataGrid(){
	//初始化table
	$('#memberGroupDetailTable').datagrid( {
		url : $.baseUrl+'/memberGroupController/showMemberListOfGroup.do?memberGroupId='+memberGroupId,
		/*
		queryParams : {
		},
		*/
		idField : 'memberId',
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [ 1, 10, 20, 30, 40, 50 ],
	    queryParams : serializeObject($('#memberGroupDetailForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
	
}

function removeMemberGroupItem(memberGroupId,memberId){
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberGroupController/removeMemberGroupItem.do?memberGroupId='+memberGroupId+'&memberId='+memberId, 
        dataType: "json",
        async: false,
        success: function (data) {
	        if(data.status=='200'){
	    		$('#memberGroupDetailTable').datagrid('load', {});
	    	}else{
	    		alert(data.message);
	    	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}