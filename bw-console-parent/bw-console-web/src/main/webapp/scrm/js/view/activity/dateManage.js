define(['common'], function(common) {
	var type;
	var activityDateId;
	function init() {
		common.Page.resetMain();
		
		//初始化表格&&下拉框&&日期&&输入框验证
		initDataGrid();
		initSelect();
		initDate();
		initInput();
		
		
		//查看
		$("#showDate").click(function(){
			if(check()==1){
				type = 'show';
				$("#date_title").html('查看日期');
				$("#save_Detail").hide();
				isAuth("/base/dateManageShow.html",function(){
					showDate();
				});
				//表单禁用
				disableForm('activityDateForm',true);
			}else if(check()==2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		//编辑
		$('#editDate').click(function(){
			if(check()==1){
				type = 'edit';
				$("#date_title").html('编辑日期');
				$("#save_Detail").show();
				isAuth("/activityDateController/editActivityDate.do",function(){
					showDate();
				})

				//表单启用
				disableForm('activityDateForm',false);
			}else if(check()==2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		//新建
		$('#saveDate').click(function(){
			isAuth('/activityDateController/saveActivityDate.do',function(){
				type = 'save';
				$("#date_title").html('新建日期');
				$(':input','#activityDateForm').val('');//清空表单
				$("#save_Detail").show();
				$("#date_Detail").show();
				
				//表单启用
				disableForm('activityDateForm',false);
				//滚动周期默认年
				$("#dateValueTypeSelect").next("ul").find("[data-id='year']").find("a").click();
			});
		});
		//删除
		$('#deletedDate').click(function(){
			if(check() != 2){
				if(confirm("确认删除？")){
					deleted();
				}
			}else{
				alert("请选择条目！");
			}
		});
		//滚动周期限制只能输入正整数(不包含0)
		$("#dateValue1").blur(function(){
			if (!(/^[1-9][0-9]*$/.test($("#dateValue1").val()))) { 
				$("#dateValue1").alert("请输入正整数！");
				return; 
			}
		});
		
		//保存编辑or新建信息
		$('#save_Detail').click(function(){
			var isValid = formCheck($("#activityDateForm"));
			
			if(isValid){
				if($("#activityDateType").val() == '3'){
					if (!(/^[1-9][0-9]*$/.test($("#dateValue1").val()))) { 
						$("#dateValue1").alert("请输入正整数！");
						return; 
					}
				}
				if($("#activityDateType").val() == ''){
					$("#dateTypeSelect").alert('请选择日期类型！');
					return;
				}else{
					if($("#activityDateType").val() == '3'){
						//滚动类型
						$("#dateValue").val($("#dateValue1").val()+'/'+$("#dateValue2").val());
						if($("#dateValue2").val() == 'week'){
							//滚动周期为周时，验证日期不能为空
							if($("#fromDate3").val() == null || $("#fromDate3").val() == ''){
								$("#weekValueSelect").alert('请选择日期时间！');
								return;
							}
							$("#fromDate_detail").val($("#fromDate3").val());
						}else if($("#dateValue2").val() == 'mouth'){
							//滚动周期为月时，验证日期不能为空
							if($("#fromDate2").val() == null || $("#fromDate2").val() == ''){
								$("#fromDate2").alert('请选择日期时间！');
								return;
							}
							$("#fromDate_detail").val($("#fromDate2").val());
						}else{
							//滚动周期为年时，验证日期不能为空
							if($("#fromDate1").val() == null || $("#fromDate1").val() == ''){
								$("#fromDate1").alert('请选择日期时间！');
								return;
							}
							$("#fromDate_detail").val($("#fromDate1").val());
						}
					}else{
						//固定日期,验证日期不能为空
						if($("#fromDate4").val() == null || $("#fromDate4").val() == ''){
							$("#fromDate4").alert('请选择日期时间！');
							return;
						}
						$("#fromDate_detail").val($("#fromDate4").val());
					}
					
				}
				if(type == 'save'){
					saveDate();
				}else{
					editDate();
				}
			}else{
				return;
			}
		});
		
	}
	
	function initDataGrid(){
		$('#activityDateTable').datagrid( {
			url : $.baseUrl+'/activityDateController/showActivityDateList.do',
			idField : 'activityDateId',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}

	function check(){
		var selected = $('#activityDateTable').datagrid('getChecked');
		if(selected.length==1){
			activityDateId = selected[0].activityDateId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			return 3;
		}

	}

	function showDate(){
		$.ajax({
			async:false,
			url:$.baseUrl+'/activityDateController/showActivityDateDetail.do',
			type:"POST",
			data:"activityDateId="+activityDateId,
			datatype:"json",
			success:function(result){
				setTimeout(function(){
					if(result){
						result = eval('(' + result + ')');
						var dv = result.dateValue.split('/');
						
						$('#activityDateName1').val(result.activityDateName);
						
						//日期类型赋值
						$('#activityDateType').val(result.activityDateType);
						$("#dateTypeSelect").next("ul").find("[data-id="+result.activityDateType+"]").find("a").click();
						
						//日期赋值
						if(result.activityDateType == '1'){
							$('#fromDate4').val(getDateString(result.fromDate));
						}else if(result.activityDateType == '3'){
							//滚动周期赋值
							$("#dateValue1").val(dv[0]);
							
							if(dv[1] == 'year'){
								$("#dateValueTypeSelect").next("ul").find("[data-id='year']").find("a").click();
								$('#fromDate1').val(getDateString(result.fromDate));
							}else if(dv[1] == 'mouth'){
								$("#dateValueTypeSelect").next("ul").find("[data-id='mouth']").find("a").click();
								$('#fromDate2').val(result.fromDate);
							}else if(dv[1] == 'week'){
								$("#dateValueTypeSelect").next("ul").find("[data-id='week']").find("a").click();
								$("#weekValueSelect").next("ul").find("[data-id="+result.fromDate+"]").find("a").click();
							}
						}
						
						$('#date_Detail').show();
					}else{
						alert('出错了！');
					}
					
				},100);
			}
		});
	}

	function saveDate(){
		$("#dataLoad").show();
		$("#activityDateForm").form('submit',{
			url:$.baseUrl+'/activityDateController/saveActivityDate.do',
			onSubmit: function(){
		    },    
		    success:function(result){  
		    	$("#dataLoad").hide();
		    	result = eval('(' + result + ')');
		    	if(result == 1){
		    		alert('新建成功！');
		    		$("#closeDateButton").click();
					$("#activityDateTable").datagrid('load');
		    	}else{
		    		alert('出错了，请重试！');
		    	}
		    }    
		});
	}

	function editDate(){
		$("#dataLoad").show();
		$("#activityDateForm").form('submit',{
			url:$.baseUrl+'/activityDateController/editActivityDate.do?activityDateId='+activityDateId,
			onSubmit: function(){
		    },    
		    success:function(result){  
		    	$("#dataLoad").hide();
		    	result = eval('(' + result + ')');
		    	if(result == 1){
		    		alert('保存成功！');
		    		$("#closeDateButton").click();
					$("#activityDateTable").datagrid('load');
		    	}else{
		    		alert('修改出错了，请重试！');
		    	}
		    }    
			
		});
	}

	function deleted(){
		var selected = $('#activityDateTable').datagrid('getChecked');
		var activityDateIds = '';

		if(selected.length==0){
			alert('请选择条目！');
		}else{
			for(var i=0; i<selected.length; i++){
				if (activityDateIds != ''){ 
					activityDateIds += ',';
			    }
				activityDateIds += selected[i].activityDateId;
			}
		}
		isAuthForAjax('/activityDateController/removeActivityDateList.do','POST','','json','activityDateIds='+activityDateIds,function(result){
			result = eval('(' + result + ')');
			if(result==1){
				alert('删除成功！');
				$("#activityDateTable").datagrid('load');
				//清除所有勾选的行
				$("#activityDateTable").datagrid('clearChecked');
			}else{
				alert('删除失败，请重试！');
			}
		},function(){
			
		});
//		$.ajax({
//			url:$.baseUrl+'/activityDateController/removeActivityDateList.do',
//			type:"POST",
//			data:"activityDateIds="+activityDateIds,
//			datatype:"json",
//			success:function(result){
//				result = eval('(' + result + ')');
//				if(result==1){
//					alert('删除成功！');
//					$("#activityDateTable").datagrid('load');
//					//清除所有勾选的行
//					$("#activityDateTable").datagrid('clearChecked');
//				}else{
//					alert('删除失败，请重试！');
//				}
//			}
//			
//		});
		
	}
	
	//日期类型js切换
	window.activity_selectDate = function activity_selectDate(value){
		if(value == '3'){
			$('#roll').show();
			//滚动周期默认年
			$("#dateValueTypeSelect").next("ul").find("[data-id='year']").find("a").click();
		}
		if(value == '1'){
			$("#fixed_show4").show();
			$("#fixed_show1").hide();
			$("#fixed_show2").hide();
			$("#fixed_show3").hide();
			
			$('#roll').hide();
		}
	}
	
	//滚动周期的日期div切换
	window.display_date = function display_date(value){
		if(value == 'year'){
			$("#fixed_show1").show();
			$("#fixed_show2").hide();
			$("#fixed_show3").hide();
			$("#fixed_show4").hide();
			
			//滚动周期为年时，默认周期值为1，不可输入
			$("#dateValue1").val('1');
			$("#dateValue1").attr("disabled",true);
			$("#dateValue1").css("background",'#cdcdcd');
		}else if(value == 'mouth'){
			$("#fixed_show2").show();
			$("#fixed_show1").hide();
			$("#fixed_show3").hide();
			$("#fixed_show4").hide();
			
			//滚动周期可输入
			$("#dateValue1").attr('disabled',false);
			$("#dateValue1").css('background','#fff');
		}else{
			$("#fixed_show3").show();
			$("#fixed_show1").hide();
			$("#fixed_show2").hide();
			$("#fixed_show4").hide();
			$("#fixed").hide();
			
			//滚动周期可输入
			$("#dateValue1").attr('disabled',false);
			$("#dateValue1").css('background','#fff');
		}
	}
	
	//开始时间
	window.getFromDate = function getFromDate(value, row, index){
		//开始时间
		var date;
		//滚动周期
		var dateValue;
		if(row.activityDateType == '1'){
			//固定日期
			return getDateString(value);
		}else if(row.activityDateType == '3'){
			dateValue = row.dateValue.split('/');
			if(dateValue[1] == 'year'){
				date = value.split('-');
				return date[0]+'月'+date[1]+'日'+'/1年';
			}else if(dateValue[1] == 'mouth'){
				return value+'号'+'/'+dateValue[0]+'月';
			}else if(dateValue[1] == 'week'){
				if(value == '1') value = '一';
				if(value == '2') value = '二';
				if(value == '3') value = '三';
				if(value == '4') value = '四';
				if(value == '5') value = '五';
				if(value == '6') value = '六';
				if(value == '7') value = '日';
				
				return '周'+value+'/'+dateValue[0]+'周';
			}
		}
		
	}
	
	return {
		init: init
	}
});


