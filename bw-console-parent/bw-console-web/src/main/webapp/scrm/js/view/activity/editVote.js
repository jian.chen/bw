define(['common'], function(common) {
	
	function init() {
		showEditVote();
		$("#updateVote").click(function(){
			var voteNameEdit = $("#voteNameEdit").val();
			var voteDetailEdit = $("#voteDetailEdit").val();
			var startDateEdit = $("#startDateEdit").val();
			var endDateEdit = $("#endDateEdit").val();
			if(voteNameEdit == ""){
				$("#voteNameEdit").alert("不能为空");
				return;
			}
			$.ajax({ 
		        type: "post", 
		        url: $.baseUrl+'/VoteController/updateVoteTitle', 
				dataType : "json",
		        data : {
		        	titleId : editVoteId,
		        	voteName: voteNameEdit, 
		        	voteDetail : voteDetailEdit,
		        	startDate : startDateEdit,
		        	endDate : endDateEdit
		        },
		        success: function (data) {
		    		alert(data.message);
		    		$(".detailed_close").closest('.mask, .mask_in').hide();
		    		$('#voteTable').datagrid('reload');
		        }
		    });
			
		});
	}
	
	return {
		init: init
	}
});

function showEditVote(){
	$.ajax({
		type: "post", 
        url: $.baseUrl+'/VoteController/showEditVote', 
		dataType : "json",
        data : {
        	editVoteId : editVoteId
        },
        success : function (data){
        	$("#voteNameEdit").val(data.editResult.voteName);
        	$("#voteDetailEdit").val(data.editResult.voteDetail);
        	$("#startDateEdit").val(getDateString(data.editResult.startDate));
        	$("#endDateEdit").val(getDateString(data.editResult.endDate));
        }
	})
}


