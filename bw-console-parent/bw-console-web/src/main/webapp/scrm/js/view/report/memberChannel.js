define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//默认查询当前日期前一天数据
		$("#startDate").val(getDate());
		$("#endDate").val(getDate());
		
		//初始化表格&下拉框&日期&输入框验证
		initDataGrid();
		initSelect();
		initDate();
		initInput();
		
		$("#cmImport").click(function(){
			location.href = $.baseUrl + '/MemberChannelSummaryDayController/leadToExcelMemberChannel?startDate='+$("#startDate").val()+'&endDate='+$("#endDate").val();
		});
		
		$("#searchMC").click(function(){
			var ck = formCheck($("#memberChannelForm"));
			if(ck){
				$('#memberChannelTable').datagrid('load', serializeObject($('#memberChannelForm')));
			}else{
				alert("请输入合法数据！");
			}
		});
		
	}

	function initDataGrid(){
		$('#memberChannelTable').datagrid( {
			url : $.baseUrl+'/MemberChannelSummaryDayController/showListByExample.do',
				onLoadSuccess : function(result) {
				},
				onLoadError : function() {
				}
			})
	}
	
	//获取当前日期的前一天(返回字符串yyyy-mm-dd)
	function getDate(){
		var date = new Date();
		var yesterday_milliseconds=date.getTime()-1000*60*60*24;        
	    var yesterday = new Date(); 
	    yesterday.setTime(yesterday_milliseconds);  
       
        var year = yesterday.getFullYear();       //年
        var month = yesterday.getMonth() + 1;     //月
        var day = yesterday.getDate();            //日
       
        var clock = year + "-";
       
        if(month < 10){
        	month = "0" + month;
        }
        if(day < 10){
        	day = "0" + day;
        }
        
        clock += month + "-"+day;
       
        return(clock); 
    } 
	
	//激活会员/会员总数
	window.accountForMC = function accountForMC(value, row, index){
		if(row.totalActivaeNumber==0 || row.totalNumber==0 ){
			return '--';
		}
		if(row.totalActivaeNumber == row.totalNumber){
			return '100%';
		}
		var a = row.totalActivaeNumber / row.totalNumber;
		var b = a.toFixed(4);
		var rate = b.slice(2,4)+"."+b.slice(4,6)+"%";
		return rate;
	}
	
	//渠道会员/总会员
	window.accountForMC2 = function(value,row,index){
		memNum = row.memNum;
		if(row.totalNumber==0 || row.memNum==0 ){
			return '--';
		}
		if(row.totalNumber == memNum){
			return '100%';
		}
		var a = row.totalNumber / memNum;
		var b = a.toFixed(4);
		var rate2 = b.slice(2,4)+"."+b.slice(4,6)+"%";
		return rate2;
	}
	
	//新增激活会员/新增会员总数
	window.accountForMC3 = function(value, row, index){
		if(row.dailyAddActivaeNumber==0 || row.dailyAddNumber==0 ){
			return '--';
		}
		if(row.dailyAddActivaeNumber == row.dailyAddNumber){
			return '100%';
		}
		var a = row.dailyAddActivaeNumber / row.dailyAddNumber;
		var b = a.toFixed(4);
		var rate = b.slice(2,4)+"."+b.slice(4,6)+"%";
		return rate;
	}
	
	window.accountForMC3 = function(value, row, index){
		if(row.dailyAddActivaeNumber==0 || row.dailyAddNumber==0 ){
			return '--';
		}
		if(row.dailyAddActivaeNumber == row.dailyAddNumber){
			return '100%';
		}
		var a = row.dailyAddActivaeNumber / row.dailyAddNumber;
		var b = a.toFixed(4);
		var rate = b.slice(2,4)+"."+b.slice(4,6)+"%";
		return rate;
	}
	
	window.getChannelName = function(value){
		if(value){
			return value;
		}
		return '未知';
	}
	
	window.getMinus = function(value, row, index){
		return value;
	}
	
	return {
		init: init
	}

});

