define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		isAuthManage("/goods/goodsShowManage.html", "goods/goodsShowManage.html");
		$('#xitongguanli_tab li').click(function(){
			var url = $(this).data('url');
			var authUrl = $(this).attr("auth-url");
			isAuthManage(authUrl, url, $(this));
		});
	};

	function isAuthManage(url, goToUrl, dom){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/sysRoleController/isAuth",
			async : false,
			dataType: "json",
			data : {
				url : url
			},
			success : function(data){
				if(data.status == '401'){
					$.messager.alert("访问受限","抱歉您没有此操作权限");
					return;
				}
				if(data.status == '200'){
					$(dom).addClass('active').siblings().removeClass('active');
					$('#xitongguanliPanel').show().panel({
						href: goToUrl
					});
				}
			}
		})
	}

	return {
		init: init
	}
});


