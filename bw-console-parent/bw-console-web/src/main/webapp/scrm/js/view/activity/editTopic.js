define(['common'], function(common) {
	
	function init(){
		initSelect();
		
		initEdit();
		
		var sec_num = $(".del").length;
		$("#addSelect").on("click",function(){
			var num = $(".del").length;
			if(num>4){
				alert("最多只能5个选项");
				return
			}
			sec_num++;
			var nextOne = $("#topicModel li:first").clone().attr("id","choose"+sec_num);
			nextOne.find(".fl").text("选项"+sec_num);
			nextOne.find("input").attr("id","content"+sec_num).val("");
			$("#topicModel").append(nextOne);
			innitIndex();
		});
		$(".questionDetail").on("click",".del",function(){
			if($(".del").length!=1){
				$(this).parent("li").remove();
				innitIndex();
			}else{
				$.messager.alert("提示","最少为1个");
			}
		});
		/*$("#selectNum").keyup(function(){
				if($(".del").length<$("#selectNum").val()){
					$("#selectNum").val($(".del").length);
				}
			});*/
		$("#saveTopic").click(function(){
			var type=$("#topicType").val();
			var explains = $("#explains").val();
			var selectNum = $("input[name='selectNum']:checked").val();
			var selectList = [];
			var templateId = manageQuestionId;
			if(type == ""){
				$("#topicType").alert("不能为空");
				return;
			}
			if(explains == ""){
				$("#explains").alert("不能为空");
				return;
			}
			if(selectNum == undefined){
				selectNum == '1';
			}
			$(".topicSelect").each(function(e){
				selectList[e]=$(this).val();
			});
			$.ajax({
				type : "post",
				url : $.baseUrl + "/QuestionController/updateTemplate",
				dataType : "json",
				data : {
					selectList : selectList,
					type : type,
					explains : explains,
					selectNum : selectNum,
					questionnaireId : templateId,
					selectTemplateId : selectTemplateId
				},success : function(data){
					alert(data.message);
					$(".detailed_close").closest('.mask, .mask_in').hide();
					$('#questionTemplateTable').datagrid('reload');
				}
			})
		})
		
		$('.dropdown-menu > li').click(function(){
			var type = $(this).attr("data-id");
			if(type == 1){
				$("#topicModel").show();
				$("#newSelect").show();
			}else{
				$("#topicModel").hide();
				$("#newSelect").hide();
			}
			
			
		});
		
	}
	
	function innitIndex(){
		var num_list =['a','b','c','d','e'];
		$("#topicModel li span").filter(".fl").each(function(e){
			$(this).text("选项" + num_list[e]);
		});
	}
	
	return {
		init: init
	}
	
});

function initEdit(){
	$.ajax({
		type : "post",
		url : $.baseUrl + "/QuestionController/showTemplateAndInfo",
		dataType : "json",
		data : {
			selectTemplateId : selectTemplateId
		},success : function(data){
			$("#topicType").val(data.template.type);
        	$("#topicTypeText").val($.topicType.getText(data.template.type));
			$("#explains").val(data.template.title);
			if(data.template.selectNum == '1'){
				$("input[name='selectNum']:eq(0)").attr("checked",'checked');
			}
			if(data.template.selectNum == '2'){
				$("input[name='selectNum']:eq(1)").attr("checked",'checked');
			}
			if(data.template.type == 1){
				$("#topicModel").show();
				$("#newSelect").show();
			}
			var tempHtml = "";
			var list = data.infoList;
			var num_list =['','a','b','c','d','e'];
			for(var i=1,j=0;i<list.length+1;i++,j++){
				tempHtml += "<li id='choose" + i + "'>";
				tempHtml += "<span class='fl'>选项" + num_list[i] + "</span>";
				tempHtml += "<input id='content" + i + "' data-options='required:true,length:'300'' size='50' type='text' class='form-control input140 topicSelect' value='" + list[j].content + "'><span class='del' style='margin-left: 10px'>删除</span>";
				tempHtml += "</li>";
			}
			$("#topicModel").append(tempHtml);
		}
	})
	
}
