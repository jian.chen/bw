define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		$("#goback").click(function(){
			backUrl = 'activity/voteTitle.html';
			clickPoints = 'vote';
			common.Page.loadCenter('activity/publicManage.html');
		});
		
		$("#addOptions").click(function(){
			common.Page.loadMask('activity/addOptions.html');
		});
		
		
		//初始化表格
		initDataGridVoteManage();
		
		
	}
	
	function initDataGridVoteManage(){
		$('#voteOptionsTable').datagrid({
			url : $.baseUrl+'/VoteController/showVoteOptions',
			queryParams:{
				titleId : manageVoteId
				},
			idField : 'optionsId',
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
				
			}
		})
	}
	
	window.editManageOptions = function(id){
		selectVoteOptionsId = id;
		$('#maskPanel').show().panel({
			href: 'activity/editOption.html'
		});
	}
	
	window.delManageOptions = function(id){
		if(confirm("确定要删除吗？")){
			$.ajax({
				type : "post",
				url : $.baseUrl + "/VoteController/delOptions",
				dataType : "json",
				data : {
					optionsId : id
				},success : function(data){
					alert(data.message);
					$('#voteOptionsTable').datagrid('reload');
				}
			})
		}
	}
	
	window.typeTrans = function(value,row,index){
		return $.topicType.getText(value);
	}
	
	window.rowformater_voteOption = function (value,row,index){
		var s = '<button onclick="editManageOptions('+row.optionsId+')" type="button" class="solid-btn">编辑</button>';
		s = s + '<button onclick="delManageOptions('+row.optionsId+')" type="button" class="border-btn">删除</button>';
		return s;
	}
	
	return {
		init: init
	}
	
});

var selectVoteOptionsId;