define(['common', 'view/sys/sysManage'], function (common, sysManage) {
    var roleId;
    var roleName;
    var type;
    var title;

    function init() {
        common.Page.resetMain();
        initDataGrid();
        $('#addSysRole').click(function () {
            type = "add";
            isAuth('/sys/ruleAdd.html', function () {
                common.Page.loadCenter('sys/ruleAdd.html');
            });
        });

        $('#showSysRole').click(function () {
            if (checkSingle()) {
                roleId = getSingleCheckedId();
                roleName = getSingleCheckedName();
                type = "show";
                isAuth('/sys/ruleEdit.html', function () {
                    title = "查看角色";
                    common.Page.loadCenter('sys/ruleEdit.html');
                });
            } else {
                $.messager.alert("提示", "请勾选一条记录查询!");
            }
        });

        $('#editSysRole').click(function () {
            if (checkSingle()) {
                roleId = getSingleCheckedId();
                roleName = getSingleCheckedName();
                type = "edit";
                isAuth('/sys/ruleEdit.html', function () {
                    common.Page.loadCenter('sys/ruleEdit.html');
                });
            } else {
                $.messager.alert("提示", "请勾选一条记录查询!");
            }
        });

        $('#deleteSysRole').click(function () {
            if (checkSingle()) {
                type = "delete";
                $.messager.confirm('提示', '确认删除?', function(r){
                    if (r){
                        roleId = getSingleCheckedId();
                        var url = "/sysRoleController/removeSysRole.do?roleId=" + roleId;
                        isAuthForAjax(url, "POST", "application/x-www-form-urlencoded; charset=utf-8", "JSON", {}, function (data) {
                            if (data.status == 200) {
                                $.messager.alert("提示", "删除成功！");
                                $("#sysFunctionTable").datagrid('clearChecked');
                                $("#sysFunctionTable").datagrid('load');
                            } else {
                                $.messager.alert("提示", data.message);
                            }
                        }, function () {
                            $.messager.alert("提示", "request error");
                        });
                    }
                });
            } else {
                $.messager.alert("提示", "请勾选一条记录查询!");
            }
        });
    };

    function initDataGrid() {
        $('#sysFunctionTable').datagrid({
            url: baseUrl + '/sysRoleController/showSysRole.do',
            idField: 'roleId',
            pageNumber: 1,
            pageSize: 10,
            pageList: [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
        });
    }

    function checkSingle() {
        return $('#sysFunctionTable').datagrid('getChecked').length == 1;
    }

    function getSingleCheckedId() {
        return $('#sysFunctionTable').datagrid('getChecked')[0].roleId;
    }

    function getSingleCheckedName() {
        return $('#sysFunctionTable').datagrid('getChecked')[0].roleName;
    }

    function doSave() {
        var json = '';
        var airJson = '';
        var dataJson = '';
        $("[name='OIcheckbox']").each(function () {
            if ($(this).is(':checked')) {
                //alert($(this).val());
                var allvalue = $(this).val().split("|");
                var operationItemId = allvalue[0];
                var functionId = allvalue[1];
                // 如果相等，说明是二级菜单
                if (operationItemId === functionId) {
                    operationItemId = 0;
                }
                json += ',{"functionId":"' + functionId + '","operationItemId":"' + operationItemId + '"}';
            }
        });
        $("[name='AIRcheckbox']").each(function () {
            if ($(this).is(':checked')) {
                //alert($(this).val());
                var allvalue = $(this).val().split("|");
                var operationItemId = allvalue[0];
                var functionId = allvalue[1];
                // 如果相等，说明是二级菜单
                if (operationItemId === functionId) {
                    operationItemId = 0;
                }
                json += ',{"functionId":"' + functionId + '","operationItemId":"' + operationItemId + '"}';
            }
        });
        $("[name='Colcheckbox']").each(function () {
            if (!$(this).is(':checked')) {
                var allvalue = $(this).val().split("|");
                var dataItem = allvalue[0];
                var dataTypeId = allvalue[1];
                dataJson += ',{"dataTypeId":"' + dataTypeId + '","dataItems":"' + dataItem + '"}';
            }
        })

        if (dataJson != "") {
            dataJson = dataJson.substring(1);
        }
        dataJson = '[' + dataJson + ']';
        if (json != "") {
            json = json.substring(1);
        }
        json = '[' + json + ']';

        var param = {};
        param.roleId = $("#roleId").val();
        param.roleName = $("#roleName").val();
        param.type = type;
        param.roleFuncArray = json;
        param.roleDataArray = dataJson;

        var ck = formCheck($("#save_form"));
        if (ck) {
            var url = baseUrl + "/sysRoleController/saveSysRoleDetail.do";
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {
                    str: JSON.stringify(param) + ""
                },
                dataType: "json",
                success: function (result) {
                    var data = JSON.parse(result);
                    $.messager.alert("提示", data.message, "info", function () {
                        if (data.code == 1) {
                            back();
                        }
                    });
                    //if (data.code == 1) {
                    //    $("#closeButton").click();
                    //    $("#sysFunctionTable").datagrid('load');
                    //}
                },
                error: function (data) {
                    $.messager.alert("提示", "request error");
                }
            });
        }
    }

    function addEditEventBind() {
        $("#selectAll").click(function () {
            if (this.checked) {
                $("input[name='OIcheckbox']").each(function () {
                    $(this).prop("checked", true);
                });
            } else {
                $("input[name='OIcheckbox']").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });
        $("#selectAirAll").click(function () {
            if (this.checked) {
                $("input[name='AIRcheckbox']").each(function () {
                    $(this).prop("checked", true);
                });
            } else {
                $("input[name='AIRcheckbox']").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });
        $("#back_btn").click(back);
        $("#save_btn").click(doSave);
    }
    function back() {
        sysManage.setUrl('sys/ruleManage.html');
        common.Page.loadCenter('sys/sysManage.html');
    }

    return {
        init: init,
        getRoleId: function () {
            return roleId;
        },
        getRoleName: function () {
            return roleName;
        },
        getType: function () {
            return type;
        },
        getTitle: function () {
            return title;
        },
        back: back,
        save: doSave,
        addEditEventBind: addEditEventBind
    }
});

