define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initCateGrid();
		
		$("#goback").click(function(){
			prodBackUrl = 'base/productCategory.html';
			common.Page.loadCenter('base/productManage.html');
		})
		
		$("#saveCate").click(function(){
			if(cateType == 'edit'){
				editCate();
			}
			if(cateType == 'new'){
				saveCate();
			}
		})
		
	}
	
	function initCateGrid(){
		if(cateType == 'show'){
			$("#cate_title").text("类别查看");
				$.ajax({
					url:$.baseUrl+'/productCategoryController/showProductCategoryDetail.show',
					type:"POST",
					data:{
						productCategoryId : productCategoryId
					},
					dataType:"json",
					success:function(result){
						if(result.status == '200'){
							$("#productCategoryId").val(result.obj.productCategoryId);
							$("#productCategoryCode").val(result.obj.productCategoryCode);
							$("#productCategoryName").val(result.obj.productCategoryName);
							$("#productCategoryDesc").val(result.obj.productCategoryDesc);
						}else{
							$.messager.alert('提示','异常!稍后查看。');
						}
					}
				});
			}
		if(cateType == 'edit'){
			$("#cate_title").text("类别编辑");
			$("#saveCate").show();
			$.ajax({
				url:$.baseUrl+'/productCategoryController/showProductCategoryDetail.edit',
				type:"POST",
				data:{
					productCategoryId : productCategoryId
				},
				dataType:"json",
				success:function(result){
					if(result.status == '200'){
						$("#productCategoryId").val(result.obj.productCategoryId);
						$("#productCategoryCode").val(result.obj.productCategoryCode);
						$("#productCategoryName").val(result.obj.productCategoryName);
						$("#productCategoryDesc").val(result.obj.productCategoryDesc);
					}else{
						$.messager.alert('提示','异常!稍后编辑。');
					}
				}
			});
			}
		if(cateType == 'new'){
			$("#cate_title").text("类别新建");
			$("#saveCate").show();
			return;
		}
		
	}
	
	function saveCate(){
		if($("#productCategoryCode").val() == ''){
			$("#productCategoryCode").alert("不能为空");
			return;
		}
		if(isNaN($("#productCategoryCode").val())){
			$("#productCategoryCode").alert("必须为数字!");
			return;
		}
		if($("#productCategoryName").val() == ''){
			$("#productCategoryName").alert("不能为空");
			return;
		}
		$.ajax({
			url:$.baseUrl+'/productCategoryController/saveProductCategory',
			type:"POST",
			data:{
				productCategoryCode : $("#productCategoryCode").val(),
				productCategoryName : $("#productCategoryName").val(),
				productCategoryDesc : $("#productCategoryDesc").val()
			},
			dataType:"json",
			success:function(result){
				if(result.status == '200'){
					$.messager.alert('提示','新建成功!');
					prodBackUrl = 'base/productCategory.html';
					common.Page.loadCenter('base/productManage.html');
				}else{
					$.messager.alert('提示',result.message);
				}
			}
		});
	}
	
	function editCate(){
		if($("#productCategoryCode").val() == ''){
			$("#productCategoryCode").alert("不能为空");
			return;
		}
		if(isNaN($("#productCategoryCode").val())){
			$("#productCategoryCode").alert("必须为数字!");
			return;
		}
		if($("#productCategoryName").val() == ''){
			$("#productCategoryName").alert("不能为空");
			return;
		}
		$.ajax({
			url:$.baseUrl+'/productCategoryController/editProductCategory',
			type:"POST",
			data:{
				productCategoryId : $("#productCategoryId").val(),
				productCategoryCode : $("#productCategoryCode").val(),
				productCategoryName : $("#productCategoryName").val(),
				productCategoryDesc : $("#productCategoryDesc").val()
			},
			dataType:"json",
			success:function(result){
				if(result.status == '200'){
					$.messager.alert('提示','修改成功!');
					prodBackUrl = 'base/productCategory.html';
					common.Page.loadCenter('base/productManage.html');
				}else{
					$.messager.alert('提示',result.message);
				}
			}
		});
	}
	
	return {
		init: init
	}
});