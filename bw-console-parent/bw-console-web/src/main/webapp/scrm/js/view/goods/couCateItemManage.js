define(['common'], function(common) {
	
	var type;
	var couponCategoryItem;
	var couponCategoryItems;
	var statusId;
	
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		//查看
		$("#showCouCateItem").click(function(){
			var mm = check();
			
			if(mm == 1){
				type = 'show';
				isAuth("/goods/couCateItemAdd.html", function() {common.Page.loadMask('goods/couCateItemAdd.html')});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		
		//编辑
		$("#editCouCateItem").click(function(){
			var mm = check();
			if(mm == 1){
				if(statusId == '2202'){
					alert("上架状态下,兑换不可修改");
					return;
				}
				type = 'edit';
				isAuth("/couponCategoryController/saveCouponCategoryItem.do", function() {common.Page.loadMask('goods/couCateItemAdd.html')});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
			
		});

		//新建
		$("#saveCouCateItem").click(function(){
			type = 'save';
			isAuth("/couponCategoryController/saveCouponCategoryItem.do", function() {common.Page.loadMask('goods/couCateItemAdd.html')});
		});

		//删除
		$("#deletedCouCateItem").click(function(){
			var mm = check();
			
			if(mm != 2){
				if(confirm("确认删除？")){
					if(mm == 1){
						couponCategoryItems = couponCategoryItem;
					}
					deleted();
				}
			}else{
				alert("请选择条目！");
			}	
		});
		
		
	}
	
	function initDataGrid(){
		$('#couCateItemTable').datagrid( {
			url : $.baseUrl+'/couponCategoryController/showList.do',
			idField : 'couponCategoryItem',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	function check(){
		var selected = $('#couCateItemTable').datagrid('getChecked');
		couponCategoryItems = '';
		couponCategoryItem = '';
		
		if(selected.length==1){
			couponCategoryItem = selected[0].couponCategoryItem;
			statusId = selected[0].statusId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			for(var i=0; i<selected.length; i++){
				if (couponCategoryItems != ''){ 
					couponCategoryItems += ',';
			    }
				couponCategoryItems += selected[i].couponCategoryItem;
			}
			return 3;
		}

	}
	
	function deleted(){
		isAuthForAjax("/couponCategoryController/removeList.do?couponCategoryItems="+couponCategoryItems,
			"POST", "", "JSON", {}, function (result) {
				var data = eval('(' + result + ')');
				goodsShowIds="";
				goodsShowId="";
				alert(data.message);
				$("#couCateItemTable").datagrid('clearChecked');
				$("#couCateItemTable").datagrid('reload');
		}, function (){
				alert("request error");
		});
	}
	
	window.shelves_couCateItem = function shelves_couCateItem(value, row, index){
		var statusTemp = $.COUPON_CATEGORY_ITEM_STATUS.getText(row.statusId);
		
		if(statusTemp == '上架'){
			
			return "<button id='"+row.couponCategoryItem+"_start' type='button' onclick='javascript:updateCouCateStatus(&apos;"+row.couponCategoryItem+"&apos;,&apos;"+row.statusId+"&apos;)' class='border-btn'>下架</a>";
		}else{
			return "<button id='"+row.couponCategoryItem+"_start' type='button' onclick='javascript:updateCouCateStatus(&apos;"+row.couponCategoryItem+"&apos;,&apos;"+row.statusId+"&apos;)' class='solid-btn'>上架</a>";
		}
	}
	
	window.updateCouCateStatus = function(couponCategoryItem,statusId){
		isAuthForAjax("/couponCategoryController/updateStatus.do",
			"POST", "", "JSON", {couponCategoryItem : couponCategoryItem, statusId : statusId}, function (result) {
				var data = eval('(' + result + ')');
				alert(data.message);
				$("#couCateItemTable").datagrid('load');
			}, function (){
				alert("request error");
			});
	}

	return {
		init: init,
		getType : function(){
			return type;
		},
		getCouponCategoryItem : function(){
			return couponCategoryItem;
		}
	}

});

