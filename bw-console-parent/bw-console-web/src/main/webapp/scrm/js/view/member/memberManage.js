define(['common','citySelect'], function(common,citySelect) {

	function init() {
		
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		initSelect();
		initDate();
		initInput();
		addTagIdArr = [];
		addTagNameArr = [];

		//初始化表格
		//initMemberDataGrid();
		
		/*
		 * 表格查询绑定
		 */
		$('#search_btn').click(function() {

			//初始化table
			$('#huiyuandingdanTable').datagrid( {
				url : $.baseUrl+'/memberController/showMemberList.do',
				/*
				queryParams : {
				},
				*/
				idField : 'memberId',
				singleSelect : true,
				pagination : true,
				pageNumber : 1,
				pageSize : 15,
				pageList : [15, 20,30, 40, 50 ],
			    queryParams : serializeObject($('#memberSearchForm')),
				onLoadSuccess : function(result) {
					
				},
				onLoadError : function(result) {
					
				}
				});

		});
		
		/*
		 * 标签弹出层开始
		 */
		$('#labelChooseBtn').click(function() {
			$('#labelChoose').show();
			queryTag($('#likeMemberTagName').val());
		});
		$('#queryTag_bt').click(function() {
			queryTag($('#likeMemberTagName').val());
		});
		$('#tagSaveBtn').click(function() {
			$(".detailed_close").closest('.mask, .mask_in').hide();
			$("#memberTagIds").val(addTagIdArr.toString());
			$("#labelChooseBtn").val(addTagNameArr);
		});

		$('#bindBCBtn').click(function(){
			var selected  = $('#huiyuandingdanTable').datagrid('getSelected');
			if(selected){
				$('#bindCard').show();
				memberId = selected.memberId;
				$('#bind_memberId').val(memberId);
			}else{
				alert("请选择会员");
				return;
			}
		});
		$('#bindBtn').click(function(){
			$('#bindCardForm').form('submit', {
				url:$.baseUrl+'/memberController/bindBlackCard.do?cardNo='+ $('#cardNo').val(),
				method : "post",
				onSubmit: function(){
					var isValid = formCheck($(this));
					if(isValid){
						//$("#dataLoad").show();
					}
					return isValid;
				},
				success:function(data){
					//$("#dataLoad").hide();
					var result = $.parseJSON(data);
					$('#huiyuandingdanTable').datagrid('reload');
					$('#cardNo').val('');
					$('#bindCard').hide();
					alert(result.message);
				},
				error: function (e) {
					alert(e)

				}
			})
		});
		/*
		 * 详情点击
		 */
		$('#memberDetailBtn').click(function() {
			var selected  = $('#huiyuandingdanTable').datagrid('getSelected');
			if(selected){
				memberId = selected.memberId;
				isAuth('/member/memberDetail.html',function(){
					common.Page.loadMask('member/memberDetail.html');
				});
			}else{
				alert("请选择会员");
				return;
			}
		});
		
		
		/*
		 * 编辑点击
		 */
		$('#memberEditBtn').click(function() {
			var selected  = $('#huiyuandingdanTable').datagrid('getSelected');
			if(selected){
				memberId = selected.memberId;
				isAuth('/memberController/editMember.do',function(){
					common.Page.loadMask('member/memberEdit.html');
				});
			}else{
				alert("请选择会员");
				return;
			}
		});
		
		/*
		 * 新增会员
		 */
		$('#memberAddBtn').click(function() {
			isAuth('/memberController/saveMember.do',function(){
				common.Page.loadMask('member/memberAdd.html');
			});
		});
		
		//区域门店弹框
		$('#equalOrgId').click(function() {
			initTreeData("areaChooseManager","areaTreeManager","/orgController/testTree.do","1");
			var ids = $("[name='equalOrgId']").val();
			if(ids){
				var idArr = ids.split(",");
				checkNode(idArr,"areaTreeManager");
			}
		});
		//区域门店保存
		$('#areaSaveBtn').click(function() {
			var selectIds= getSelectId("areaTreeManager","1");
			var selectNames= getSelectName("areaTreeManager","1");
			$("[name='equalOrgId']").val(selectIds);
			$("[name='equalOrgName']").val(selectNames);
			$('#areaChooseManager').find(".detailed_close2").click();
		});

	
	}
	return {
		init: init
	}
});

var memberId;
//存储勾选的标签id
var addTagIdArr;
//存储勾选的标签name
var addTagNameArr;

function initMemberDataGrid(){
	//初始化table
	$('#huiyuandingdanTable').datagrid( {
		url : $.baseUrl+'/memberController/showMemberList.do',
		/*
		queryParams : {
		},
		*/
		idField : 'memberId',
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10, 20,30, 40, 50 ],
	    queryParams : serializeObject($('#memberSearchForm')),
		onLoadSuccess : function(result) {
			if(result) {
				
            }
		},
		onLoadError : function(result) {
			alert(result);
		}
		});
}

