var prodBackUrl = 'complain/complain.html';
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		loadThisMask(prodBackUrl);
		$('#complainManage_tab li').removeClass("active");
		$("#complainManage_tab li").each(function(){
			if($(this).attr("data-url") == prodBackUrl){
				$(this).addClass("active");
			}
		});
		
		$('#complainManage_tab li').click(function(){
			var url = $(this).data('url');
			$(this).addClass('active').siblings().removeClass('active');
			loadThisMask(url);
		});
		
	};

	function loadThisMask(url) {
		$('#complainliPanel').show().panel({
			href: url
		});
	};
	return {
		init: init
	}
});