define(['common'], function(common) {
	function init() {
		var editor = CKEDITOR.replace('editor03');
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		$("#add_image").uploadPreview({ Img: "add_img1_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		
		 //  url: $.baseUrl+'/memberMessageController/uploadMessageImages.do',
		
		$("#messageSaveBtn").click(function(){
			// 获取富文本编辑器的值
			console.log(editor.getData());
			$('#edit_messageContent').val(editor.getData());
			messageAdd();
		});
		
		
		$("#back").click(function(){
			common.Page.loadCenter('message/messageCenterManager.html');
		});
		
		$('#add_image').change(function() {
			$("#add_fileSpan").text($(this).val());
		});
		function messageAdd(){
			if(checkMessageForm()){
				$("#dataLoad").show();
				$.ajax({
					type: "POST",
					url : $.baseUrl+'/memberMessageController/addMessage.do',
					data:$('#add_messageForm').serialize(),
					dataType : "json",
					success : function(result) {
						$("#dataLoad").hide();
						if(result.status==200){
							alert(result.message);
							common.Page.loadCenter('message/messageCenterManager.html');
							
						}else{
							alert(result.message);
						}
					},
					error : function() {
					}
				});
				
			}
		}
	}
	return {
		init: init
	}
});

function checkMessageForm(){
	var a = $('#add_messageName').val();
	if($('#add_messageName').val()==null || $('#add_messageName').val()=='' || $('#add_messageName').val().length>200){
		alert("标题不能为空且长度不能超过200字符");
		return false;
	}else if($('#add_img1').val()==null || $('#add_img1').val()==''){
		alert("封面图像不能为空");
		return false;
	}else{
		return true;
	}
}



