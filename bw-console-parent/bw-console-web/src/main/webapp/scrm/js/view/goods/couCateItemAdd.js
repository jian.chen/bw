define(['common','view/goods/couCateItemManage'],function(common,couCateItemManage){
	
	function init(){
		
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		
		//初始化优惠券表格
		initCoupon();
		
		//初始化商品类型下拉框
		initSelectById('couponCategorySelect','/couponCategoryController/showCouponCategoryList.do','couponCategoryId','couponCategoryName');
		
		if(couCateItemManage.getType() == "show"){
			$("#goodsShow_title").html('商品详情');
			$("#areaSaveBtn").hide();
			show();
			
			//表单禁用
			disableForm('couCateItemAddForm',true);
		}else if(couCateItemManage.getType() == "edit"){
			$("#goodsShow_title").html('商品编辑');
			$("#areaSaveBtn").show();
			$("#couponDiv").attr("disabled",true);
			
			show();
		}else{
			$("#goodsShow_title").html('新建商品');
		}
		
		//优惠券弹框
		$("#couponDiv").click(function(){
			$("#coupon-div").show();
			$('#couCateItemCouponTable').datagrid('reload');
		});
		
		//优惠券筛选
		$("#seachCoupon").click(function(){
			$('#couCateItemCouponTable').datagrid('reload',{
				couponName : $("#couponNameSea").val()
			});
		});
		

	}
	
	function initCoupon(){
		$('#couCateItemCouponTable').datagrid( {
			url : $.baseUrl+'/goodsShowController/queryCouponList.do',
			idField : 'couponId',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	function show(){
		isAuthForAjax("/couponCategoryController/showDetail.do?couponCategoryItem="+couCateItemManage.getCouponCategoryItem(),
			"POST", "", "JSON", {}, function (data) {
				$("#couponCategoryItem").val(data.couponCategoryItem);
				$("#couponCategoryId").val(data.couponCategoryId);
				$("#couponCategorySelect").next("ul").find("[data-id="+data.couponCategoryId+"]").find("a").click();
				$("#statusSelect").next("ul").find("[data-id="+data.statusId+"]").find("a").click();
				$("#seq").val(data.seq);
				$("#couponId").val(data.couponId);
				$("#couponDiv").val(data.couponName);
			}, function (){
				alert("request error");
			});
	}
	
	window.goodsShowAdd = function(){
		var isValid = formCheck($("#couCateItemAddForm"));
		if(isValid){
			isAuthForAjax("/couponCategoryController/saveCouponCategoryItem.do",
				"POST", "", "JSON", serializeObject($('#couCateItemAddForm')), function (result) {
					var data = eval('(' + result + ')');
					alert(data.message);
					$('#close_btn').click();
					$("#couCateItemTable").datagrid('load');
				}, function (){
					alert("request error");
				});
		}
	}
	
	window.getCouponId = function(){
		var selected = $('#couCateItemCouponTable').datagrid('getChecked');
		//goodsShowId = selected[0].couponId;
		$("#couponId").val(selected[0].couponId);
		$("#couponDiv").val(selected[0].couponName);
		$("#closeCoupon").click();
	}
	
	
	
	return {
		init:init
	}
	
});

