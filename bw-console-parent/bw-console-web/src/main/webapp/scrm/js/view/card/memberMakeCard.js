var issueId;
define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		$("#search").click(function(){
			initDataGrid();
		})
		
		$("#daochu").click(function(){
			location.href = $.baseUrl + '/memberEntityCardController/leadToExcelMemberCard?issueId='+issueId;
		})
		
		$("#generate").click(function(){
			if($("#num").val() == ""){
				$("#num").alert("不能为空");
				return;
			}
			if($("#batch").val() == ""){
				$("#batch").alert("不能为空");
				return;
			}
			generateMemberCardNo();
		})
	}
	
	function generateMemberCardNo(){
			 $.ajax({ 
		        type: "post", 
		        url: $.baseUrl+'/memberEntityCardController/generateMemberEntityCard',  
		        dataType: "json",
		        data : {
		        	count: parseInt($("#num").val()),
		        	batch:$("#batch").val()
		        },
		        success: function (data) {
		    		alert(data.message);
		    		$('#memberIssueTable').datagrid('reload');    
		        }
		    });
	}
	
	function initDataGrid(){
		$('#memberIssueTable').datagrid({
			url : $.baseUrl+'/memberEntityCardController/showMemberEntityCard',
			queryParams : {
				batch : $("#batch").val()
			},
			idField : 'issueId',
			singleSelect : true,
			pagination : true,
			pageNumber : 1,
			pageSize : 10,
			pageList : [10, 20,30, 40, 50 ],
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
				
			}
		})
	}
	
	window.issue_formatProgress = function (value,row,index){
		
		
		var s = '<button onclick="detailMemberCardNo('+row.issueId+')" type="button" class="border-btn">查看</button>';
		//如果未制卡，制卡按钮可用
		if(row.statusId == '1308'){
			s = s + '<button onclick="make_entitycard('+row.issueId+')" type="button" class="border-btn">制卡</button>';
	    }else{
	    	s = s + '<button  type="button" style="background: #cdcdcd;" class="solid-btn">已制卡</button>';
	    }
		return s;
	};
	window.detailMemberCardNo=function(issueId){
		this.issueId=issueId;
		isAuth("/couponIssueController/showCouponIssueList.do",function(){
			$("#memberCardDetail").show();
			showMemberCardIssueDetail();
		})
	}
	//终止活动
	window.make_entitycard = function(issueId){
		$.messager.confirm('确认','您确认想要制卡吗？操作后将不可更改',function(r){    
		    if (r){    
		    	$.ajax({
					url:$.baseUrl+'/memberEntityCardController/makeMemberEntityCard',
					type:"POST",
					data : {
						issueId: parseInt(issueId)
			        },
					dataType:"json",
					success: function (data) {
			    		alert(data.message);
			    		$('#memberIssueTable').datagrid('load');
			        }
					
				});
		    }
		});
	};
	
	function showMemberCardIssueDetail(){
		$('#memberManage_detail').datagrid( {
			url : $.baseUrl+'/memberEntityCardController/showMemberEntityCardDetail',
			queryParams : {
				issueId : issueId
			},
			singleSelect : true,
			pagination : true,
			pageNumber : 1,
			pageSize : 10,
			pageList : [10, 20,30, 40, 50 ],
			onLoadSuccess : function(result) {
			},
			onLoadError : function() {
				
			}
		});
	}
	
	//检查是否单选
	function checkSingle(){
		var selected = $('#memberIssueTable').datagrid('getChecked');
		if(selected.length==1){
			return true;
		}else{
			$.messager.alert("提示","请勾选一条记录!");
			return false;
		}

	}
	//获取选择Id
	function getCheckedId(){
		var selected = $('#memberIssueTable').datagrid('getChecked');
		var issueId='';
		for(var i=0; i<selected.length; i++){
		    if (issueId != ''){ 
		    	issueId += ',';
		    }
		    issueId += selected[i].issueId;
		    //var edit4originalCouponTotal = selected[i].issueProportion.split("/")[0];
		}
		return issueId;
	}
	return {
		init: init
	}
	
});