define(['common','activityInstanceManage'], function(common,activityInstanceManage) {
	//初始化table
	function init(){
		
		initLoad();
		
		//返回
		$("#backBtn-form-activityInstanceReport").click(function () {
			common.Page.loadCenter('activity/activityInstanceManage.html');
		});
		
		$("#seachRecord").click(function(){
			$('#activity-instance-record-table').datagrid('load', serializeObject($('#recordForm')));
		});
	}
	
	function initLoad(){
		$('#activity-instance-record-table').datagrid( {
			url : $.baseUrl+'/activityController/showActivityInstanceRecord.json',
			method:'post',
			queryParams:{'activityInstanceId': activityInstanceManage.getActivityInstanceId()},
			onLoadSuccess : function(result) {
				if(result) {
					$("#recordActivityInstanceId").val(activityInstanceManage.getActivityInstanceId());
				}
			},
			onLoadError : function() {
			}
		});
	}
	
	return{
		init:init
	};
});

