define(['common'], function(common) {
	
	var type;
	var goodsShowCategoryId;
	var goodsShowCategoryIds;
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		//查看
		$("#showGoodsCategory").click(function(){
			var mm = check();
			if(mm == 1){
				type = 'show';
				isAuth("/goods/goodsShowCategoryAdd.html", function() {common.Page.loadMask('goods/goodsShowCategoryAdd.html')});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		
		//编辑
		$("#editGoodsCategory").click(function(){
			
			var mm = check();
			
			if(mm == 1){
				type = 'edit';
				isAuth("/goodsShowController/saveGoodsShowCategory.do", function() {common.Page.loadMask('goods/goodsShowCategoryAdd.html')});
			}else if(mm == 2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
			
		});

		//新建
		$("#saveGoodsCategory").click(function(){
			type = 'save';
			isAuth("/goodsShowController/saveGoodsShowCategory.do", function() {common.Page.loadMask('goods/goodsShowCategoryAdd.html')});
		});

		//删除
		$("#deletedGoodsCategory").click(function(){
			var mm = check();
			if(mm != 2){
				if(confirm("确认删除？")){
					if(mm == 1){
						goodsShowCategoryIds = goodsShowCategoryId;
					}
					deleted();
				}
			}else{
				alert("请选择条目！");
			}	
		});
		
		
	}
	
	function initDataGrid(){
		$('#goodsShowCategoryTable').datagrid( {
			url : $.baseUrl+'/goodsShowController/showCategoryList.do',
			idField : 'goodsShowCategoryId',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	function check(){
		var selected = $('#goodsShowCategoryTable').datagrid('getChecked');
		goodsShowCategoryIds = '';
		goodsShowCategoryId = '';
		
		if(selected.length==1){
			goodsShowCategoryId = selected[0].goodsShowCategoryId;
			statusId = selected[0].statusId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			for(var i=0; i<selected.length; i++){
				if (goodsShowCategoryIds != ''){ 
					goodsShowCategoryIds += ',';
			    }
				goodsShowCategoryIds += selected[i].goodsShowCategoryId;
			}
			return 3;
		}

	}
	
	function deleted(){
		isAuthForAjax("/goodsShowController/removeCategoryList.do?goodsShowCategoryIds="+goodsShowCategoryIds,
			"POST", "", "JSON", {}, function (result) {
				var data = eval('(' + result + ')');
				goodsShowCategoryIds="";
				goodsShowCategoryId="";
				alert(data.message);
				$("#goodsShowCategoryTable").datagrid('clearChecked');
				$("#goodsShowCategoryTable").datagrid('reload');
			}, function (){
				alert("request error");
			});
	}
	

	return {
		init: init,
		getType : function(){
			return type;
		},
		getGoodsShowCategoryId : function(){
			return goodsShowCategoryId;
		}
	}

});

