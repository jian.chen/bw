define(['common'], function(common) {
	
	function init(){
		$("#saveQuestion").click(function(){
			var questionName = $("#questionName").val();
			var explains = $("#explains").val();
			var endExplain = $("#endExplain").val();
			var startDate = $("#startDate").val();
			var endDate = $("#endDate").val();
			if(questionName == ""){
				$("#questionName").alert("不能为空");
				return;
			}
			if(explains == ""){
				$("#explains").alert("不能为空");
				return;
			}
			$.ajax({ 
		        type: "post", 
		        url: $.baseUrl+'/QuestionController/addQuestion', 
				dataType : "json",
		        data : {
		        	questionName: questionName, 
		        	explains: explains,
		        	endExplain : endExplain,
		        	startDate : startDate,
		        	endDate : endDate
		        },
		        success: function (data) {
		    		alert(data.message);
		    		$(".detailed_close").closest('.mask, .mask_in').hide();
		    		$('#questionTable').datagrid('reload');
		        }
		    });
			
		});
	}

	return {
		init: init
	}
	
	
});


