define(['common'], function(common) {
	
	function init() {
		common.Page.resetMain();
		initDate();
		initSelect();
		initInput();
		//初始化表格
		initDataGrid();
		// 查询
		$("#queryButton").click(function(){
			initDataGrid();
		});
		//初始化下拉选择框
		initSelectById('productCategorySelect','/productCategoryController/showProductCategoryList.do','productCategoryId','productCategoryName');
	}
	return {
		init: init
	}
	//初花加载表格
	function initDataGrid(){
		$('#productItemliTable').datagrid({
			url : $.baseUrl+'/productController/showProduct.do',
			method :'post',// 请求类型 
			singleSelect : true,
			loadMsg:"数据加载中...",
		    queryParams :serializeObject($('#productItemliForm')),
				onLoadSuccess : function(result) {
						
				},
				onLoadError : function() {
					
				}
			});
		}
});