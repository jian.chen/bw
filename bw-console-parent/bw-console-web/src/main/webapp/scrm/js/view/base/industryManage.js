define(['common','view/member/areaLinkage'], function(common) {
	var type;
	var externalMerchantId;
	function init() {
		common.Page.resetMain();
		initDataGrid();
		
		$('#show').click(function(){
			if(check()==1){
				type = 'show';
				isAuth('/base/industryEdit.html',function(){
					common.Page.loadMask('base/industryEdit.html');
				});
			}else if(check()==2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		$('#edit').click(function(){
			if(check()==1){
				type = 'edit';
				isAuth('/couponController/editExternalCouponMerchants.do',function(){
					common.Page.loadMask('base/industryEdit.html');
				});
			}else if(check()==2){
				alert("请选择条目！");
			}else{
				alert("不能同时选择多个条目！");
			}
		});
		
		$('#save').click(function(){
			type = 'save';
			isAuth('/couponController/saveExternalCouponMerchants.do',function(){
				common.Page.loadMask('base/industryEdit.html');
			});
		});
		$('#deleted').click(function(){
			if(check() != 2){
				if(confirm("确认删除？")){
					deleted();
				}
			}else{
				alert("请选择条目！");
			}
		});
			
	}
	
	
	//检查是否单选
	function checkSingle(){
		var selected = $('#couponTable').datagrid('getChecked');
		if(selected.length==1){
			return true;
		}else{
			$.messager.alert("提示","请勾选一条记录!");
			return false;
		}

	}
	
	//获取选择Id
	function getCheckedId(){
		var selected = $('#couponTable').datagrid('getChecked');
		var merchantIds='';
		for(var i=0; i<selected.length; i++){
		    if (merchantIds != ''){ 
		    	merchantIds += ',';
		    }
		    merchantIds += selected[i].merchantId;
		    edit4originalCouponTotal = selected[i].issueProportion.split("/")[0];
		}
		return merchantIds;
	}
	
	
	function initDataGrid(){
		$('#merchantTable').datagrid( {
			url : $.baseUrl+'/couponController/queryExternalCouponMerchants.do',
			idField : 'externalMerchantId',
			onLoadSuccess : function(result) {		
	
			},
			onLoadError : function() {
				
			}
		});
	}
	
	function check(){
		var selected = $('#merchantTable').datagrid('getChecked');
		if(selected.length==1){
			externalMerchantId = selected[0].externalMerchantId;
			return 1;
		}else if(selected.length==0){
			return 2;
		}else{
			return 3;
		}
		
	}
	
	function deleted(){
		var selected = $('#merchantTable').datagrid('getChecked');
		var externalMerchantIds = '';
		
		if(selected.length==0){
			alert('请选择条目！');
		}else{
			for(var i=0; i<selected.length; i++){
				if (externalMerchantIds != ''){ 
					externalMerchantIds += ',';
				}
				externalMerchantIds += selected[i].externalMerchantId;
			}
		}

	
	}
	return {
		init: init,
		getMerchantId : function(){
			return externalMerchantId;
		},
		getType : function(){
			return type;
		}
	}
});
