define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		$("#search").click(function(){
			initDataGrid();
		})
		
		$("#daochu").click(function(){
			location.href = $.baseUrl + '/ReportController/leadToExcelMemberAge?startDate='+$("#startDate").val()+'&endDate='+$("#endDate").val();
		})
		
	}
	
	function initDataGrid(){
		$('#ageRateTable').datagrid({
			url : $.baseUrl+'/ReportController/showMemberAge',
			queryParams : {
				startDate : $("#startDate").val(),
				endDate : $("#endDate").val()
			},
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
				
			}
		})
	}
	
	window.rowformater_rate = function (value,row,index){
		var a = row.dayNewCount * 100 / row.all;
		var b = a.toFixed(2);
		var rate = b + "%";
		return rate;
	}
	
	window.rowformater_all = function (value,row,index) {
		return row.maleDayCount + row.femaleDayCount + row.otherDayCount;
	}
	
	window.rowformater_type = function (value,row,index){
		if(value == 1){
			return "0-19";
		}
		if(value == 2){
			return "20-30";
		}
		if(value == 3){
			return "30-40";
		}
		if(value == 4){
			return "40+";
		}
		if(value == 5){
			return "未填写";
		}

	}
	
	return {
		init: init
	}
	
});