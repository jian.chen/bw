define(['common'], function(common) {
	
	function init() {
		common.Page.resetMain();		
		
		initOrderItemDataGrid();
		
		var productList = new Array();
		
		//产品名称
		$.Member_Product = productList;		
		getProductList();
		
		function getProductList(){
			$.ajax({
				type : "post",
				url : $.baseUrl+"/orderController/showProductForJS.do",
				dataType : "json",
				async : false,
				success : function(data){
					productList = data.productResult;
					$.Member_Product = productList;
				}
			})
		}
	}
	
	window.rowformater_Product = function(value,row,index){
		return $.Member_Product.getText(row.productCode);
	}
		
	return {
		init: init
	}
});

var order;
var orderId;

function initOrderItemDataGrid(){
	//初始化table
	$('#OrderItemTable').datagrid( {
		url : $.baseUrl+'/orderController/showOrderItemList.do?orderId='+orderId,
		idField : 'orderId',
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [ 1, 10, 20, 30, 40, 50 ],
		onLoadSuccess : function(result) {
			if(result) {
				$("#memberCode").text(order.memberCode);
	        	$("#happenTime").text(getDateString(order.happenTime));
	        	$("#createTime").text(getDateString(order.createTime));
	        	$("#payAmount").text(order.payAmount);
	        	$("#orderCode").text(order.orderCode);
	        	$("#externalId").text(order.externalId);
	        	$("#channelId").text($.Member_Channel.getText(order.channelId));
	        	$("#storeName").text(order.storeName);
	        	$("#statusId").text($.Order_Status.getText(order.statusId));
            }
		},
		onLoadError : function() {
			
		}
	});	
	
}
