define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		initDate();
		initInput();
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
		initCouponBusinessTypeData();
	}
	return {
		init: init
	}
});


function initCouponBusinessTypeData(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/couponBusinessTypeController/queryCouponBusinessTypeDetail.do',
		data : "couponBusinessTypeId="+couponIds,
		dataType : "json",
		async: false,
		success : function(result) {
			$("#couponBusinessTypeName").val(result.couponBusinessType.couponBusinessTypeName);
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});
}
	
