var activityType;
var activityId;
var activityTitle;
var saveType;
var pageTag;
var parentTag;

define(['common','activityManage'], function(common,activityManage) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		activityManage.typeId="1";
		
		//初始化表格
		initDataGrid();
		
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
	}
	
	function initDataGrid(){
		$('#activityTable').datagrid( {
			url : $.baseUrl+'/activityController/showActivityList.json',
			idField : 'activityId',
			queryParams : {
				typeId : 1
			},
			onLoadSuccess : function(result) {
				//合并列
				var rowCount = $("#activityTable").datagrid("getRows").length;
				
				var span;
				var PerValue = "";
				var CurValue = "";
				var index;
				
				span = 1;
				for (var row = 0; row <= rowCount; row++) {
					 if (row == rowCount) {
						 CurValue = "";
					 }else {
						 CurValue = $("#activityTable").datagrid("getRows")[row]['activityTypeId'];
					 }
					 if (PerValue == CurValue) {
						span += 1;
					 }else {
						index = row - span;
						if(index < 0){
							index = 0;
							span = span - 1;
						}
						
						$("#activityTable").datagrid('mergeCells', {
							index: index,
							field: 'activityTypeId',
							rowspan: span,
							colspan: null,
						});
						span = 1;
						PerValue = CurValue;
					 }
				}
				
			},
			onLoadError : function() {
			}
		});
		
		window.bangding = function(value,id,name){
			activityType = value;
			activityId = id;
			activityTitle = name;
			saveType = 'save';
			common.Page.loadCenter('activity/activitySettingDetail.html');
		};
		
		window.bangding_manage = function(value,id,name){
			activityType = value;
			activityId = id;
			activityTitle = name;
			common.Page.loadCenter('activity/activityInstanceManage.html');
		};
		
	}
	
	return {
		init: init,
		getActivityCode: function(){
			return activityType;
		}
	};
});

function rowformater_activity(value,row,index){
	return "<button id='"+value+"_start' type='button' onclick='javascript:bangding(&quot;"+value+"&quot;,"+row.activityId+",\""+row.activityName+"\")' class='solid-btn'>发起</a>" +
			"<button id='"+value+"_manage' type='button' onclick='javascript:bangding_manage(&quot;"+value+"&quot;,"+row.activityId+",\""+row.activityName+"\")' class='border-btn'>管理</a>";
}

	

