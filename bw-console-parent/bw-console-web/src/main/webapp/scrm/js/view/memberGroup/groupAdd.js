define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		document.onkeydown = function(e){
			 var ev = (typeof event!='undefined') ? window.event:e;
			 if(ev.keyCode == 13){
				e.preventDefault();
			 }
		}
		
		$('#SQLEditBtn').click(function() {
			showsql("sqlArea",2);
			$('#SQLEdit').show();
		});
		
		$("[name='executeTypeRadio']").click(function() {
			$("[name='executeType']").val($(this).val());
		});
		
		$("#memberGroupSaveBtn").click(function() {
			saveMemberGroupAdd();
		});
		
	  $("[name='memberGroupName']").blur(function(){
	  	 if($("[name='memberGroupName']").val()!=""){
	  	 	$.ajax({ 
		        type: "post", 
		        url: $.baseUrl+'/memberGroupController/showCountByName.do', 
		        dataType: "json",
		        data : {
		        	memberGroupName: $(this).val()
		        },
		        async: false,
		        success: function (data) {
		        	if(data>0){
		        		alert("分组名称已被使用");
		        	}
		        }, 
		        error: function (XMLHttpRequest, textStatus, errorThrown) { 
		            alert(errorThrown); 
		        }
		    });
	  	 }
	  	
	  });
	  
	}
	return {
		init: init
	}
});

//保存会员分组
function saveMemberGroupAdd(){
	$('#memberGroupAddForm').form('submit', {    
    url:$.baseUrl+'/memberGroupController/saveMemberGroup.do',
    onSubmit: function(){
    	var isValid = formCheck($(this));
    	if(isValid){
    		$("#dataLoad").show();
    	}
		return isValid;
    },    
    success:function(data){  
    	$("#dataLoad").hide();
    	var result = $.parseJSON(data);
    	if(result.status=='200'){
    		$(".detailed_close").closest('.mask, .mask_in').hide();
    		alert(result.message);
    		$('#memberGroupTable').datagrid('load',{});
    	}else{
    		alert(result.message);
    	}
    }    
}); 
}