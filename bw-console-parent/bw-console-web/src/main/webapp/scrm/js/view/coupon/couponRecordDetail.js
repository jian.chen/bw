define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		initcouponRecordDataGrid();
		$('#couponRecordDetail_search').click(function() {
			$('#couponRecordDetail_table').datagrid("load", {  
				memberMobile : $('#couponRecordDetail_mobile').val(),
				couponInstanceCode : $("#couponRecordDetail_couponInstanceCode").val(),
				issueStartTime : $("#couponRecordDetail_issueStartTime").val(),
				issueEndTime : $("#couponRecordDetail_issueEndTime").val(),
				couponId : couponIds,
				statusId : $("#statusId").val(),
				issueStatus : $("#issueStatus").val()
			});
		});
		
				
		$("#couponRecordExport").click(function(){
		location.href = $.baseUrl + 
		'/couponIssueController/leadToExcelCouponInstance?memberMobile='+$('#couponRecordDetail_mobile').val()
		+'&couponInstanceCode='+$("#couponRecordDetail_couponInstanceCode").val()
		+'&issueStartTime='+$("#couponRecordDetail_issueStartTime").val()
		+'&issueEndTime='+$("#couponRecordDetail_issueEndTime").val()
		+'&couponId='+couponIds
		+'&statusId='+$("#statusId").val()
		+'&issueStatus='+$("#issueStatus").val();
		})
				
	}

	return {
		init: init
	}
});

function initcouponRecordDataGrid(){
	//初始化table
	$('#couponRecordDetail_table').datagrid( {
		url : $.baseUrl+'/couponIssueController/showCouponInstance.do',
		queryParams : {
			couponId : couponIds,
			statusId : $("#statusId").val(),
			issueStatus : $("#issueStatus").val()
		},
//        queryParams : serializeObject($('#couponSearchForm')),
		onLoadSuccess : function(result) {
			$("#couponRecordDetail_couponName").text("优惠券:"+result.obj.couponName);
			if(result.obj.issueQuantity==null){
				$("#couponRecordDetail_issue_quantity").text("发放情况:0/"+result.obj.totalQuantity);
			}else{
				$("#couponRecordDetail_issue_quantity").text("发放情况:"+result.obj.issueQuantity+"/"+result.obj.totalQuantity);
			}
			if(result.obj.usedQuantity==null){
				$("#couponRecordDetail_used_quantity").text("核销情况:0/"+result.obj.totalQuantity);
			}else{
				$("#couponRecordDetail_used_quantity").text("核销情况:"+result.obj.usedQuantity+"/"+result.obj.totalQuantity);
			}
			
		},
		onLoadError : function() {
			
		}
	});
}

/*function showSearchValue(){
	var map = new Map();
	var mobile = $('#couponRecordDetail_mobile').val();
	var couponCode = $("#couponRecordDetail_couponCode").val();
	var issueStartTime = $("#couponRecordDetail_issueStartTime").val();
	var issueEndTime = $("#couponRecordDetail_issueEndTime").val();
	map.put("mobile",mobile);  
	map.put("couponCode",couponCode);
	map.put("issueStartTime",issueStartTime);  
	map.put("issueEndTime",issueEndTime);
	return map;
}*/