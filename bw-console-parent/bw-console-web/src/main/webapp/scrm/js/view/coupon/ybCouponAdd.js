define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		initYbCouponData();
		$(".add").click(function(){
			$("#addChannel").show();
			initChannelData();
		});
		$("#importYbCoupon").click(function(){
			$("#importExcel").show();
		});
		
		//下载模板
		$('#downloadExcel').click(function() {
			downTemp();
		});
		
		$('#importBtn').click(function() {
			importData();
		});
		$("#saveChannel").click(function(){
			var channelIds=""; 
			var data = new Array();
			$("input:checkbox[name='channel']:checked").each(function(){ 
				var id = $(this).val();
				var name = $(this).next().text();
				var channel = new rows(id,'','',name);
				data.push(channel);
				channelIds+=$(this).val()+",";
			}) 
			channelIds = channelIds.substring(0,channelIds.length-1);
			$("#ybChannelIds").val(channelIds);
			$('#addChannel').find(".detailed_close2").click();
			initUsedChannelHtml(data);
		});
		
		$('#fileInput').change(function() {
			$("#fileSpan").text($(this).val());
		});
	}
	return {
		init: init
	}
});
function initYbCouponData(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/ybCouponController/showYbCouponData.do',
		dataType : "json",
		async: false,
		success : function(result) {
			var data = result; 
			initUsedChannelHtml(result.channel);
			initYbCouponCountHtml(result.ybCount,result.channel);
			
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});
}
function  rows(channelId,channelTypeId,channelCode,channelName) //声明对象
{
   this.channelId = channelId;
   this.channelTypeId= channelTypeId;
   this.channelCode= channelCode;
   this.channelName = channelName;
  
}


function initChannelData(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/ybCouponController/showChannel.do',
		dataType : "json",
		async: false,
		success : function(result) {
			initChannelHtml(result.rows);
			
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});
}
function initUsedChannelHtml(data){
	var TextHtml = '';
	for(var i=0;i<data.length;i++){
		TextHtml += '<span>'+data[i].channelName+'</span>';
	}
	$("#usedChannel").html(TextHtml);
}

function initChannelHtml(data){
	var TextHtml = '';
	for(var i=0;i<data.length;i++){
		TextHtml += '<span>';
		TextHtml += '<input type="checkbox" name="channel" value="'+data[i].channelId+'"/>';
		TextHtml += '<span>'+data[i].channelName+'</span>';
		TextHtml += '</span>';
	}
	$("#ybChannel").html(TextHtml);
}

function initYbCouponCountHtml(data,channel){
	var TextHtml = '';
		TextHtml += '<td>'+data.totalCount+'</td>';
		TextHtml += '<td>'+data.usedCount+'</td>';
		TextHtml += '<td>'+data.availCount+'</td><td>';
		for(var i=0;i<channel.length;i++){
			TextHtml += '<p>'+channel[i].channelName+'</p>';
		}
		TextHtml += '</td>';
	$("#ybCouponCount").html(TextHtml);
}
//下载模板
function downTemp() {
	window.location=$.baseUrl+'/ybCouponController/downTemp.do';
}

//导入会员
function importData() {
	$("#dataLoad").show();
	$('#importForm').form('submit', {    
	    url:$.baseUrl+'/ybCouponController/importData.do',  
	    onSubmit: function(){    
	    },    
	    success:function(result){    
	    	$("#dataLoad").hide();
	    	var data = $.parseJSON(result);
	    	$('#importExcel').find(".detailed_close").click();
	    	initYbCouponData();
	   }    
	});  
}