define(['common', 'view/sys/sysUserManage', 'contants', 'view/member/tree'],
    function (common, sysUserManage, contants,  tree) {
        var userName;
        var sysUserAddArr = [];
        var roleList = new Array(); 
        

        function init() {
        	getRoleList();
        	
            sysUserAddArr = [];
            //初始化角色下拉框&输入框验证
            initSelectById('selectId', '/sysUserController/showSysRoleList.do', 'roleId', 'roleName');
            var savetype;
            $("#backBtn").click(function () {
                sysUserManage.back();
            });
            if (sysUserManage.getType() == 'show') {
                $("#su_title").html('系统用户详情');
                $("#saveSysDetail").hide();
                //延时，户名及密码用于应对360自动填充用
                setTimeout(function () {
                    show();
                }, 200);
                disableForm('sysUserForm', true);
                $("#passwordCheck_div").hide();
                $("#passwordEdit_div").hide();
            } else if (sysUserManage.getType() == 'edit') {
                $("#su_title").html('系统用户编辑');
                $("#sysUserId1").val(sysUserManage.getSysUserId());
                savetype = 'edit';
                //延时，用于应对360自动填充用户名及密码
                setTimeout(function () {
                    show();
                }, 200);
                $("#passwordEdit_div").hide();
                $("#passwordCheck_div").hide();
                $("#sys_password1").removeAttr("data-options");
                $("#passwordCheck").removeAttr("data-options");
            } else {
                $("#su_title").html('系统用户新建');
                savetype = 'save';
                initMultiple('sysUserAdd-multiple', sysUserAddArr, roleList);
            }
            
            /*$(".dropdown-menu").delegate(function() {
            	
            });*/
            $(".dropdown-menu").delegate('li','click',function(){
            	var value = $(this).attr("data-id");
            	// 店员
            	if(value ==='2'){
            		$("#org").css("display","block");
            	} else {
                    $("#org").css("display","none");
                    $("#orgId1").val("");
                    $("#orgName1").val("");
                }
            });
            
            $('#areaTree').click(function () {
                initTreeData("areaChooseAdd-store", "areaTreeAdd-store", "/orgController/testTree.do", "2");
                if ($("#orgId").val() != null) {
                    selectOrg($("#pareOrgId").val(), "areaTreeAdd-store");
                }
            });
            //区域门店保存
            $('#areaSaveBtnAdd').click(function () {
                var node = getSelectNode("areaTreeAdd-store", 2);
                var systemID = $("#systemId1").val();
                if (systemID != "2") {
                    $.messager.alert("提示", "只有门店店员才能选择门店");
                    return;
                }
                if (node.type != 4) {
                    $.messager.alert("提示", "只能选择门店");
                    return;
                }
                $("[name='orgId']").val(node.id);
                $("[name='orgName']").val(node.name);
                $('#areaChooseAdd-store').find(".detailed_close2").click();
            });

            $("#saveSysDetail").click(function () {
                if (!check()) return;
                if (savetype == 'edit') {
                    editSysUser();
                } else {
                    saveSysUser();
                }
            });
            initSelect();
        };
        
        function getRoleList() {
    		$.ajax({
    			type : "post",
                async: false,
    			url : $.baseUrl + "/scrm/view/showRoleListForJS",
    			dataType : "json",
    			success : function(data){
    				roleList = data.buttonResult;
    			}
    		})
    	}

        function initRoleIdMultiple(selectedRoleId){
            for(var i = 0;i < selectedRoleId.length;i++){
                for(var j = 0;j < roleList.length;j++){
                    if(selectedRoleId[i].roleId == roleList[j].value){
                        roleList[j].selected = true;
                        sysUserAddArr.push(selectedRoleId[i].roleId);
                    } else {
                        roleList[j].selected = false;
                    }
                }
            }
            initMultiple('sysUserAdd-multiple',sysUserAddArr,roleList);
            $('#sysUserAdd-multiple').multiselect('select', sysUserAddArr);
        }

        function check() {
            var val = $("#sys_userName1").val();
            if (val == "") {
                $.messager.alert("提示", "用户名不能为空")
                return false;
            } else if (val.length > 20) {
                $.messager.alert("提示", "用户名不能超过20个字符")
                return false;
            }
            val = $("#cn1").val();
            if (val == ""){
            	$.messager.alert("提示", "姓名不能为空")
                return false;
            }
            if (sysUserManage.getType() != "edit") {
                val = $("#sys_password1").val();
                if (val == "") {
                    $.messager.alert("提示", "密码不能为空")
                    return false;
                } else if (val.length > 20 || val.length < 6) {
                    $.messager.alert("提示", "密码长度必须大于6，小于20个字符！");
                    return false;
                }
                var reg = /^[a-zA-Z0-9]*$/;
                if (!reg.test(val)) {
                    $.messager.alert("提示", "密码可填写内容： 数字、英文、英文字符！");
                    return false;
                }
                val = $("#passwordCheck").val();
                if (val !== $("#sys_password1").val()) {
                    $.messager.alert("提示", "密码不一致")
                    return false;
                }
            }
            //val = $("#cn1").val();
            //if (val != "" && val.length > 50) {
            //    alert("姓名不能超过50个字符")
            //    return false;
            //}
            //val = $("#sexSelect").val();
            //if (val == "") {
            //    alert("性别不能为空")
            //    return false;
            //}
            //val = $("#email1").val();
            //if (val != "" && val.length > 50) {
            //    alert("email不能超过50个字符")
            //    return false;
            //}
            //val = $("#phoneNumber1").val();
            //if (val != "" && val.length > 50) {
            //    alert("email不能超过50个字符")
            //    return false;
            //}
            if (sysUserAddArr.length == 0) {
                $.messager.alert("提示", "请选择用户角色")
                return false;
            }
            val = $("#systemId1").val();
            if (val == "") {
                $.messager.alert("提示", "请选择是否是店员")
                return false;
            } else {
                if (val == "2") {
                    val = $("#orgId1").val();
                    if (val == "") {
                    	$.messager.alert("提示", "所属区域不能为空")
                    	return false;
                    }
                }
            }
            return true;
        }

        function show() {
            var param = {};
            param.sysUserId = sysUserManage.getSysUserId();
            isAuthForAjax("/sysUserController/showSysUserById.do", "POST", "", "JSON", param, function (result) {
                if (result != null && result.status == 200) {
                    var sysUser = result.obj.sysUser;
                    var roleList = result.obj.roleIds;
                    var sysOrg = result.obj.org;
                    $("#sys_userName1").val(sysUser.userName);
                    userName = sysUser.userName;
                    //$("#roleId1").val(sysRole.roleId);
                    //$("#roleId1").next().val(sysRole.roleName);
                    var selectedRoleId = [];
                    sysUserAddArr = [];
                    for (var i=0; i<roleList.length; i++) {
                        selectedRoleId.push({roleId:roleList[i] + ""});
                    }
                    initRoleIdMultiple(selectedRoleId);
                    var sex = getSex(sysUser.sex);
                    $("#sex1").val(sex);
                    $("#sex1").next().val($.gender.getText(sex));
                    $("#cn1").val(sysUser.cn);
                    $("#email1").val(sysUser.email);
                    $("#phoneNumber1").val(sysUser.phoneNumber);
                    if (sysUser.systemId != '' && sysUser.systemId != null) {
                        var systemId =
                        //var system = getSystem(sysUser.systemId);
                        //$("#systemId1").next().val(system.text);
                        $("#systemId1").val(sysUser.systemId);
                        $("#systemId1").next().val($.SYSTEM.getText(sysUser.systemId));
                        if (sysUser.orgId != '' && sysUser.orgId != null && sysOrg != null) {
                            $("#org").css("display","block");
                            $("#orgId1").val(sysOrg.orgId);
                            $("#orgName1").val(sysOrg.orgName);
                        }
                    }
                } else {
                    $.messager.alert("提示", '出错了！');
                }
            }, function (result) {
                $.messager.alert("提示", "出错");
            });
        }

        function getSex(sex) {
            if(sex != "1" && sex != "2") {
                return "3";
            }
            return sex;
        }

        function saveSysUser() {
            $("#dataLoad").show();
            var param = serializeObject($('#sysUserForm'));
            param.selectRoleIds = sysUserAddArr.join(",");
            isAuthForAjax("/sysUserController/saveSysUser.do", "POST", "", "JSON", param, function (result) {
                $("#dataLoad").hide();
                if (result != null && result.status == 200) {
                    $.messager.alert("提示", '新建成功！');
                    //$("#closeSysButton").click();
                    //$("#sysUserTable").datagrid('load');
                    sysUserAddArr = [];
                    sysUserManage.back();
                } else {
                    $.messager.alert("提示", result.message);
                }
            }, function (result) {
                $("#dataLoad").hide();
                $.messager.alert("提示", "出错");
            });
        }

        function editSysUser() {
            $("#dataLoad").show();
            var param = serializeObject($('#sysUserForm'));
            param.selectRoleIds = sysUserAddArr.join(",");
            isAuthForAjax("/sysUserController/editSysUser.do", "POST", "", "JSON", param, function (result) {
                $("#dataLoad").hide();
                if (result != null && result.status == 200) {
                    //$("#closeSysButton").click();
                    $.messager.alert("提示", '修改成功！');
                    //$('#sysUserTable').datagrid('load');
                    sysUserAddArr = [];
                    sysUserManage.back();
                } else {
                    $.messager.alert("提示", '修改失败，请重试！');
                }
            }, function (result) {
                $("#dataLoad").hide();
                $.messager.alert("提示", "出错");
            });
        }

        return {
            init: init
        }
    });

