define(['common'], function(common) {
	var sysReturnUrl = "sys/passwordManage.html";
	function init() {
		common.Page.resetMain();
		if(sysReturnUrl == null){
			loadThisMask("sys/passwordManage.html");
		}else{
			loadThisMask(sysReturnUrl);
			$("#xitongguanli_tab li").removeClass("active");
			$("#xitongguanli_tab li").each(function(){
				if($(this).attr("data-url")==sysReturnUrl){
					$(this).addClass("active");
				}
			});
		}
		$('#xitongguanli_tab li').click(function(){
			var url = $(this).data('url');
			var thisDom = $(this); 
			isAuthSys(url,thisDom);
		});
	};

	function loadThisMask(url) {
		$('#xitongguanliPanel').show().panel({
			href: url
		});
	};

	function isAuthSys(url,thisDom){
		$.ajax({
			type : "post",
			url : baseUrl + "/sysRoleController/isAuth",
			async : false,
			dataType: "json",
			data : {
				url : "/"+url
			},
			success : function(data){
				if(data.status != '200'){
					$.messager.alert("访问受限","抱歉您没有此操作权限");
					return;
				}
				thisDom.addClass('active').siblings().removeClass('active');
				$('#xitongguanliPanel').show().panel({
					href: url
				});
			}
		})
	};

	return {
		init: init,
		setUrl: function(url) {
			sysReturnUrl = url;
		}
	}
});


