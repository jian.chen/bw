define(['common','view/goods/couCateManage'],function(common,couCateManage){
	
	function init(){
		
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		
		$("#couponCate_image").uploadPreview({ Img: "couponCate_img2_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		if(couCateManage.getType() == "show"){
			$("#goodsShow_title").html('类型详情');
			$("#areaSaveBtn").hide();
			show();
			
			//表单禁用
			disableForm('couponCategoryAddForm',true);
		}else if(couCateManage.getType() == "edit"){
			$("#goodsShow_title").html('类型编辑');
			$("#couponCategoryId").val(couCateManage.getCouponCategoryId());
			$("#areaSaveBtn").show();
			
			show();
		}else{
			$("#goodsShow_title").html('新建类型');
		}
		
		$('#couponCate_image').change(function() {
			$("#couponCate_fileSpan").text($(this).val());
		});

	}
	
	function show(){
		isAuthForAjax("/couponCategoryController/showCouponCategoryDetail.do?couponCategoryId="+couCateManage.getCouponCategoryId(),
			"POST", "", "JSON", {}, function (data) {
				$("#couponCategoryId").val(data.couponCategoryId);
				$("#couponCategoryName").val(data.couponCategoryName);
				$("#seq").val(data.seq);
				$("#statusSelect").next("ul").find("[data-id="+data.statusId+"]").find("a").click();

				if(data.couponCategoryImg1 != null && data.couponCategoryImg1 != ''){
					$("#couponCate_img1").val(data.couponCategoryImg1);
					$("#couponCate_img3_path").attr("src",data.couponCategoryImg1);
				}
			}, function (){
				alert("request error");
			});
	}
	
	window.couponCategoryAdd = function(){
		
		var isValid = formCheck($("#couponCategoryAddForm"));
		if(isValid){
			isAuthForAjax("/couponCategoryController/saveCouponCategory.do",
				"POST", "", "JSON", serializeObject($('#couponCategoryAddForm')), function (result) {
					var data = eval('(' + result + ')');
					alert(data.message);
					$('#close_btn').click();
					$("#couponCategoryTable").datagrid('load');
				}, function (){
					alert("request error");
				});
		}
	}
	
	//清空图片
	window.clearImg = function clearImg(){
		$("#couponCate_image").val("");
		$("#couponCate_img1").val("");
		$("#couponCate_img3_path").attr("src","");
		$("#couponCate_fileSpan").text("未选择文件...");
	}
	
	//上传大图片
	window.uploadImage = function uploadImage(){
		$("#dataLoad").show();
		$('#couponCategoryAddForm').form('submit', {    
		    url : $.baseUrl+'/couponCategoryController/uploadImage.do',   //加loading 
		    success:function(result){
		    	$("#dataLoad").hide();
		    	result = eval('('+result+')');
		    	if(result=="不被允许上传的文件格式!"){
		    		alert("不被允许上传的文件格式!");
		    	}else{
		    		$("#couponCate_img1").val($.rootPath+result);
		    		$("#couponCate_img3_path").attr("src",$.rootPath+result);
		    	}
		    }    
		}); 
	}
	return {
		init:init
	}
	
});

