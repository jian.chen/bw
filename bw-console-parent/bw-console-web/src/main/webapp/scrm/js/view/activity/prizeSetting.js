//奖项序号
var index =0;
var initFlag = "";
define(['common'], function(common) {
	function init() {
		index =0;
		initFlag = "N";
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化下拉框&输入框验证prizeManager.js
		//initSelect();
		initInput();
		//场景游戏ID 赋值
		$("#sceneGameId").val(sceneGameId);
		$('#saveBtn_form_activitySetting').click(function() {
			$('#activitySetting-coupon-table').datagrid('appendRow',{awardsId: index,
				awardsName:index,
				awardsCode:index,
				prizeType:index,
				prizeName:index,
				equalscore:index,
				persions:index,
				probability:index,
				awardsSettingId:index
				});
			index++;
			$("#awards_count").val(index);
			
		});
		
		//优惠券筛选
		$("#seachCoupon").click(function(){
			$('#couCateItemCouponTable').datagrid('reload',{
				couponName : $("#couponNameSea").val()
			});
		});
		
		initCoupon();
		
		/**
		 * 活动提交按钮添加提交事件
		 */
		$("#saveBtn-form-activitySetting").click(function () {
			 //
			if(checkActivity()){
				 var saveUrl = "/sceneGameInstanceController/saveSceneGameInstance.json";
				 var obj = paramsToJson();
				 $.ajax({
			         type: "POST",   //访问WebService使用Post方式请求
			         url: $.baseUrl+saveUrl,
			         data:  {
							params : $.trim(JSON.stringify(obj))
			          },
			         dataType: 'json',
			         success: function (res) {//回调函数，result，返回值
			        	 $("#dataLoad").hide();
			        	 if(res.code=="SUCCESS"){
			        		 alert(res.message);
			        		 common.Page.loadCenter('activity/prizeManager.html');
			        	 }else{
			        		 alert(res.message);
			        	 }
			         }
			     });
			}
		});
		
		//分组弹出层
		$('#addFenZuBtn').click(function() {
			$('#addFenZu').show();
			queryGroup();
		});
		//分组弹出层保存
		$('#groupSaveBtn').click(function() {
			//关闭弹出层
			$('#addFenZu').find(".detailed_close2").click();
			addSelectGroupToArray();
			addSelectGroup();
		});
		
		//标签弹出层
		$('#labelChooseBtn2').click(function() {
			$('#labelChoose-registerActivity').show();
			appendTagEdit();
			queryTagEdit($('#likeMemberTagName-registerActivity').val());
		});
		//标签弹出层关闭
		$('#tagSaveBtn2').click(function() {
			//验证标签选择的个数
			if(addTagIdArrEdit.length>5){
				alert("标签添加数目，不能超过5个!");
				return;
			}
			//关闭弹出层
			$('#labelChoose-registerActivity').find(".detailed_close").click();
			addSelectTagEdit();
		});
		
		initBtn();
		
		initGradeCheck();
		
	};
	
	function initBtn(){
		
		$("#game-img").attr('src',imgUrl);
		$("#pageUrl").html(sceneGamePageUrl);
		
		//返回
		 $("#backBtn-form-activitySetting").click(function () {
			 if(pageTag=="instanceManage"){
				common.Page.loadCenter('activity/prizeInstanceManage.html');
			 }else{
				 common.Page.loadCenter('activity/prizeManager.html');
			 }
		 });
		 
		//下一步
		 $("#nextBtn-form-activitySetting").click(function () {
			 var ck = checkActivity("1");
			 if(ck){
//				 return;
				 $("#backBtn-form-activitySetting").hide();
				 $("#nextBtn-form-activitySetting").hide();
				 $("#ul-show-first").hide();
				 $("#div-title-first").hide();
				 $("#div-img").hide();
				 
				 $("#ul-show-second").show();
				 $("#aboveBtn-form-activitySetting").show();
				 $("#saveBtn-form-activitySetting").show();
				 $("#div-title-second").show();
				 
				 $("#coupon-div").show();
	        	 $("#activitySetting-coupon-table").datagrid({"height":"250"});
	        	 initNullAwards();
			 }
		 });
		 
		//上一步
		 $("#aboveBtn-form-activitySetting").click(function () {
			 $("#backBtn-form-activitySetting").show();
			 $("#nextBtn-form-activitySetting").show();
			 $("#ul-show-first").show();
			 $("#div-title-first").show();
			 $("#div-img").show();
			 
			 $("#ul-show-second").hide();
			 $("#aboveBtn-form-activitySetting").hide();
			 $("#saveBtn-form-activitySetting").hide();
			 $("#div-title-second").hide();
			 $("#coupon-div").hide();
		 });
		 
		 $("#needPointsAmount").prop("disabled", true);
		//积分消耗选择事件
		$("#isNeedPoints").change(function() {
			if(!$(this).prop('checked')){
				$("#needPointsAmount").val("");
				$("#needPointsAmount").prop("disabled", true); 
			}else{
				$("#needPointsAmount").prop("disabled", false); 
			}
		});
	};
	
	
	function initGradeCheck(){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/scrm/view/getContantsForJS",
			dataType : "json",
			success : function(data){
				var gradeList = data.gradeResult;
				var gradeHtml = "";
				for(var i=0; i < gradeList.length; i++){
					gradeHtml+="<ol>"
								+"<span style='display: inline;'>"
								+"<input type='checkbox' name='gradesCheck' value='"+gradeList[i].value+"'>"
								+gradeList[i].text
								+"</span>"
							+"</ol>";
					$("#grade-ul-prizeSetting").html(gradeHtml);
				}
			}
		});
	}
	
	return {
		init: init
	};
	
});

//显示或者隐藏标签选择按钮
function showLabelButton(value){
	if(value=='1'){
		$("#labelButton").show();
		$("#memberTagList").show();
	}else{
		$("#labelButton").hide();
		$("#memberTagList").hide();
	}
}

//显示或者隐藏分组选择按钮
function showGroupButton(value){
	if(value=='1'){
		$("#groupButton").show();
		$("#memberGroupList").show();
		$("#memberGroupListClues").show();
	}else{
		$("#groupButton").hide();
		$("#memberGroupList").hide();
		$("#memberGroupListClues").hide();
	}
}

//页面初始化加载奖项（谢谢参与奖和一二三等奖）
function initNullAwards(){
	if(initFlag=="N"){
		index = 4;
		$('#activitySetting-coupon-table').datagrid('loadData',
			{total:index,
			rows:[
			      {"awardsId":"0","awardsName":"0","awardsCode":"0","prizeType":"0","prizeName":"0","equalscore":"0","persions":"0","probability":"0","awardsSettingId":"0"},
			      {"awardsId":"1","awardsName":"1","awardsCode":"1","prizeType":"1","prizeName":"1","equalscore":"1","persions":"1","probability":"1","awardsSettingId":"1"},
			      {"awardsId":"2","awardsName":"2","awardsCode":"2","prizeType":"2","prizeName":"2","equalscore":"2","persions":"2","probability":"2","awardsSettingId":"2"},
			      {"awardsId":"3","awardsName":"3","awardsCode":"3","prizeType":"3","prizeName":"3","equalscore":"3","persions":"3","probability":"3","awardsSettingId":"3"}
			   ]
		});
		initFlag=="Y";
		$("#awards_count").val(index);
	}
	
	/*
	for(var i=0;i<4;i++){
		$('#activitySetting-coupon-table').datagrid('appendRow',{awardsId: index,
			awardsName:index,
			prizeType:index,
			prizeName:index,
			equalscore:index,
			persions:index,
			probability:index,
			awardsSettingId:index
			});
		index++;
		$("#awards_count").val(index);
	}
	*/
}

//加载优惠券
function initCoupon(){
	$('#couCateItemCouponTable').datagrid( {
		url : $.baseUrl+'/goodsShowController/queryCouponList.do',
		idField : 'couponId',
			onLoadSuccess : function(result) {
				if(result) {
					
	            }
			},
			onLoadError : function() {
				
			}
		});
}

//奖项字段展示处理
function rowformater_awards(value,row,table_index){
	var awardsHtml = "";
	if(table_index==0){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='一等奖'>一等奖";
	}else if(table_index==1){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='二等奖'>二等奖";
	}else if(table_index==2){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='三等奖'>三等奖";
	}else if(table_index==3){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='四等奖'>四等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==4){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='五等奖'>五等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==5){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='六等奖'>六等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==6){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='七等奖'>七等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==7){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='八等奖'>八等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==8){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='九等奖'>九等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==9){
		awardsHtml = "<input type='hidden' name='"+value+"_awardsName' id='"+value+"_awardsName' value='十等奖'>十等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}
	return awardsHtml;
}

//删除奖项
function delete_awards(value,table_index){
	
	if(value<index-1){
		alert("请从最后一个奖项删除！");
	}else if(value==index-1){
		
		$('#activitySetting-coupon-table').datagrid('deleteRow',table_index);
		//删除一个奖项序号减一
		index--;
		$("#awards_count").val(index);
	}else{
		alert("数据异常！");
	}
	
}

//加载积分输入框
function awards_equalscore(value,table_index){
	var equalscoreHtml = "";
//	if(value==0){
//		equalscoreHtml += "<input type='hidden' name='"+value+"_equalscore' id='"+value+"_equalscore' data-options='required:true,length:\"8\"'type='text' class='form-control input100' value='0' disabled='disabled'>";
//	}else{
		equalscoreHtml += "<input type='hidden' name='"+value+"_equalscore' id='"+value+"_equalscore' data-options='required:true,length:\"8\"'type='text' class='form-control input100' value='0'>";
//	}
	return equalscoreHtml;
}

//加载奖品编码
function rowformater_awardsCode(value,rows,table_index){
	var awardsCodeHtml = "";
	awardsCodeHtml += "<input type='text' name='"+value+"_awardsCode' id='"+value+"_awardsCode' placeholder='请勿修改此值' data-options='required:true,length:\"25\"'type='text' class='form-control input100' >";
	return awardsCodeHtml;
}
//奖项内容
function awards_content(value,table_index){
	var contentHtml = "";
//	if(value==0){
//		contentHtml += "<input type='hidden' name='"+value+"_prizeCode' id='"+value+"_prizeCode' value='0' />";
//		contentHtml += "<input type='hidden' name='"+value+"_awardsType' id='"+value+"_awardsType' value='lucky' />";
//		contentHtml += "<input name='"+value+"_prizeName' id='"+value+"_prizeName' data-options='required:true,length:\"100\"'type='text' class='form-control input180' value='谢谢参与' disabled='disabled'>";
//	}else{
		contentHtml += "<span id='"+value+"_awardsSpan'>";
		contentHtml += "<input type='hidden' name='"+value+"_prizeCode' id='"+value+"_prizeCode' value='' />";
		contentHtml += "<input type='hidden' name='"+value+"_awardsType' id='"+value+"_awardsType' value='common' />";
		contentHtml += "<input name='"+value+"_prizeName' id='"+value+"_prizeName' data-options='required:true,length:\"100\"'type='text' class='form-control input180' value=''>";
		contentHtml += "</span>";
//	}
	return contentHtml;
}



//加载奖品种类
function prizeTypeSelect(value,table_index){
	var contentHtml = "";
//	if(value==0){
//		contentHtml += "<select id='"+value+"_prizeType' name='"+value+"_prizeType' style='width:100px;' disabled='disabled'> ";
//		contentHtml += "<option value='3'>谢谢参与</option>";	
//		contentHtml += "</select>";
//	}else{
		contentHtml += "<select id='"+value+"_prizeType' name='"+value+"_prizeType' style='width:100px;' onchange=\"changeContent('"+value+"')\"> ";
		contentHtml += "<option value='1'>实物</option>";	
		contentHtml += "<option value='2'>优惠券</option>";
		contentHtml += "<option value='3'>卡牌</option>";
		contentHtml += "</select>";
//	}
	
	return contentHtml;
}

//加载产品库存
function awards_persions(value,table_index){
	var persionsHtml = "";
//	if(value==0){
//		persionsHtml += "<input name='"+value+"_persions' id='"+value+"_persions' data-options='required:true,length:\"10\"'type='hidden' class='form-control input140' value='0'>";
//		persionsHtml += "<input  type='text' class='form-control input140' value='不限' disabled='disabled'>";
//	}else{
		persionsHtml += "<input name='"+value+"_persions' id='"+value+"_persions' data-options='required:true,length:\"10\"'type='text' class='form-control input140' value='0' />";
//	}
	
	return persionsHtml;
}

//加载产品中奖率
function awards_probability(value,table_index){
	var probabilityHtml = "";
//	if(value==0){
//		probabilityHtml += "<input name='"+value+"_probability' id='"+value+"_probability' data-options='required:true,length:\"8\"'type='text' class='form-control input140' value='0' disabled='disabled'>";
//	}else{
		probabilityHtml += "<input name='"+value+"_probability' id='"+value+"_probability' data-options='required:true,length:\"8\"'type='text' class='form-control input140' value=''>";
//	}
	return probabilityHtml;
}

//奖项概率输入框失去焦点事件
function  onblurProbability(obj){
	//输入框验证
	checkDecimals(obj);
	//谢谢参与概率计算
	//calculate_calculate(obj);
}

function calculate_calculate(object){
	var total = 0;
	$("input[name$='_probability']").each(function(){
		if($(this).val()!=""){
			total = parseFloat(total) + parseFloat($(this).val());
		}
	});
	var probability = total - $("#0_probability").val();
	
	$("#0_probability").val((100-probability).toFixed(3));
}

//加载抽奖限制调条件
function awardsRestrict(value,table_index){
	var contentHtml = "";
//	if(value==0){
//		/*contentHtml += "<span class='fl' style='float:none; vertical-align:middle;'>每:</span>";*/
//		contentHtml += "<select id='"+value+"_cycle' name='"+value+"_cycle' style='width:50px;float:none; display:inline-block; vertical-align:middle; margin:0 5px;' disabled='disabled'> ";
//		contentHtml += "<option value='5'>不限</option>";	
//		contentHtml += "</select>";
//		contentHtml += "<input  type='hidden' name='"+value+"_cycleValue' id='"+value+"_cycleValue' value='0'/><input class='form-control' type='hidden' style='width: 55px; height:25px;float:none; display:inline-block; vertical-align:middle; margin:0 0 0 7px;' value='不限' disabled='disabled'/>";
//	}else{
		contentHtml += "<span class='fl' style='float:none; vertical-align:middle;'>每:</span>";
		contentHtml += "<select id='"+value+"_cycle' name='"+value+"_cycle' style='width:50px;float:none; display:inline-block; vertical-align:middle; margin:0 5px;' onchange=\"lotteryRestrict('"+value+"')\"> ";
		contentHtml += "<option value='5'>不限</option>";
		contentHtml += "<option value='1'>每</option>";	
		contentHtml += "</select>";
		contentHtml += "<span style='display:none;' id='chou_"+value+"'><input value='0' class='form-control' type='text' name='"+value+"_cycleValue' id='"+value+"_cycleValue' style='width: 55px; height:25px;float:none; display:inline-block; vertical-align:middle; margin:0 0 0 7px;' onblur='checkInteger(this)' data-options='required:true,length:\"8\"' value=''/>可抽中1次</span>";
//	}
	return contentHtml;
}

//抽奖限制JS
function lotteryRestrict(value){
	var val = $("#"+value+"_cycle").val();
	if(val==5){
		$("#chou_"+value).hide();
		$("#"+value+"_cycleValue").val("0");
	}else{
		$("#chou_"+value).show();
		$("#"+value+"_cycleValue").val("");
	}
}


//活动提交验证
function checkActivity(step){
	//活动名称验证
	//公用输入框方法验证
	var ckActivity = formCheck($("#form-activitySetting"));
	if(ckActivity){
		if($("#instanceName").val()==""){
			alert("请填写活动名称!");
			return false;
		}
		if($("#sceneGameInstanceCode").val()==""){
			alert("请填写活动编码!");
			return false;
		}
		//验证输入时间
		if($("#startTime").val()==""){
			alert("请填写活动开始时间!");
			return false;
		}
		if($("#endTime").val()==""){
			alert("请填写活动结束时间!");
			return false;
		}
		//抽奖机会次数验证
		if($("#luckyDrawValue").val()==""){
			alert("请填写抽奖次数!");
			return false;
		}
		if(step=="1"){
			return true;
		}
		//抽奖机会验证
		if($("#luckyDrawType").val()==""){
			alert("请填写抽奖机会类型!");
			return false;
		}
		//验证中奖率
//		var sum = 0;
//		$("input[name$='_probability']").each(function(){
//			sum = parseFloat(sum) + parseFloat($(this).val());
//		});
//		if(sum==100){
//			
//		}else{
//			alert("奖项总中奖率应等于100%");
//			return false;
//		}
		
		//验证分组是否选择
		if($("input[name='sptIsuseforgroupmember']:checked").val()=='1'){
			if(addGroupIdArr.length==0){
				alert("请添加会员分组!");
				return false;
			}
		}
		
		//验证标签是否选择
		if($("input[name='sptIsusememberlabel']:checked").val()=='1'){
			if(addTagIdArrEdit.length==0){
				alert("请添加会员标签!");
				return false;
			}
		}
	}else{
		return false;
	}
	
	return true;
}


//输入框整数验证
function checkInteger(obj){
	var re = /^[0-9]*$/;
	if(!re.test($(obj).val())){
		alert("请输入正整数!");
		$(obj).val("");
	}
}

//输入框小数验证
function checkDecimals(obj){
	var re = /^[0-9]\d*.\d*|0.\d*[1-9]\d*$/;
	if(!re.test($(obj).val())){
		alert("请输入数值!");
		$(obj).val("");
	}
}

//变更奖品内容输入框
function changeContent(id){
	//奖品类型
	var type = $("#"+id+"_prizeType").val();
	var html = "";
	//如果奖品类型类为优惠券，加载优惠券选择按钮
	$("#"+id+"_awardsSpan").html("");
	if(type==2){
		html += "<input type='hidden' name='"+id+"_prizeCode' id='"+id+"_prizeCode' value='' />";
		html += "<input type='hidden' name='"+id+"_awardsType' id='"+id+"_awardsType' value='common' />";
		html += "<input id='"+id+"_prizeName' name='"+id+"_prizeName' class='form-control input180' type='text' value='' readOnly placeholder='只可选择张数无限的券' onclick=\"showCouponDiv('"+id+"')\" data-options='required:true'></input>";
	}else{
		html += "<input type='hidden' name='"+id+"_prizeCode' id='"+id+"_prizeCode' value='' />";
		html += "<input type='hidden' name='"+id+"_awardsType' id='"+id+"_awardsType' value='common' />";
		html += "<input name='"+id+"_prizeName' id='"+id+"_prizeName' data-options='required:true,length:\"100\"'type='text' class='form-control input180' value=''>";
	}
	$("#"+id+"_awardsSpan").html(html);
}

//优惠券弹框
function showCouponDiv(id){
	$("#couponDiv").show();
	$('#couCateItemCouponTable').datagrid('reload');
	$("#submitButton").attr("onclick","getCouponId('"+id+"')");
}

//填充选中优惠券
function getCouponId(id){
	var selected = $('#couCateItemCouponTable').datagrid('getChecked');
	$("#"+id+"_prizeCode").val(selected[0].couponId);
	$("#"+id+"_prizeName").val(selected[0].couponName);
	$("#closeCoupon").click();
}

//请求参数封装成js对象
function paramsToJson(){
	var sptMlrids = "";
	var gradeIds = "";
	//获取会员标签的值
	$("input[name='sptMlrid']").each(function(){
		if(sptMlrids==""){
			sptMlrids = $(this).val();
		}else{
			sptMlrids += ","+$(this).val();
		}
	});
	
	$("input[name='gradesCheck']:checked").each(function(){
		if(gradeIds==""){
			gradeIds = $(this).val();
		}else{
			gradeIds += ","+$(this).val();
		}
	});
	
	//初始化实例
	var sceneGameInstance = {
		"sceneGameId":$("#sceneGameId").val(),
		"instanceName":$("#instanceName").val(),
		"sceneGameInstanceCode":$("#sceneGameInstanceCode").val(),
		"startTime":$("#startTime").val(),	
		"endTime":$("#endTime").val(),
		"luckyDrawType":$("#luckyDrawType").val(),
		"luckyDrawValue":$("#luckyDrawValue").val(),
		"instanceDetail":$("#instanceDetail").val(),
		"instanceDesc":$("#instanceDesc").val(),
		"sptIsuseforgroupmember":$("input[name='sptIsuseforgroupmember']:checked").val(),
		"sptIsusememberlabel":$("input[name='sptIsusememberlabel']:checked").val(),
		"sptIssharefamily":$("input[name='sptIssharefamily']:checked").val(),
		"sptProducer":$("#sptProducer").val(),
		"sptGrades":gradeIds,
		"isNeedPoints":$("input[name='isNeedPoints']:checked").val(),
		"needPointsAmount":$("#needPointsAmount").val(),
		"sptMgoid":$("#sptMgoid").val(),
		"sptMlrids":sptMlrids,
		"sptClues":$("#sptClues").val()
	};
	
	//初始化奖项
	var awardsArr = new Array();
	for(var i=0;i<index;i++){
		var award = {
			"awardsName":$("#"+i+"_awardsName").val(),
			"awardsCode":$("#"+i+"_awardsCode").val(),
			"prizeCode":$("#"+i+"_prizeCode").val(),
			"prizeName":$("#"+i+"_prizeName").val(),
			"prizeType":$("#"+i+"_prizeType").val(),
			"persions":$("#"+i+"_persions").val(),
			"probability":$("#"+i+"_probability").val(),
			"cycle":$("#"+i+"_cycle").val(),
			"cycleValue":$("#"+i+"_cycleValue").val(),
			"awardsType":$("#"+i+"_awardsType").val(),
			"equalscore":$("#"+i+"_equalscore").val()
		};
		awardsArr.push(award);
	}
	
	sceneGameInstance.awards = awardsArr;
	return sceneGameInstance;
	
}

//会员标签选弹出层JS==============================start
//存储勾选的标签id
var addTagIdArrEdit = new Array();
//存储勾选的标签name
var addTagNameArrEdit = new Array();

//将标签添加到已选标签
function addTagEdit(obj){
	var id = $(obj).val();
	var name = $(obj).parent().text();
	if($(obj).prop("checked")){
		if(!addTagIdArrEdit.in_array(id)){
			addTagIdArrEdit.push(id);
			addTagNameArrEdit.push(name);
		}	
	}else{
		addTagIdArrEdit.remove(id);
		addTagNameArrEdit.remove(name);
	}
	appendTagEdit();
	
}

//删除已选标签
function delAddTagEdit(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArrEdit.remove(id);
	addTagNameArrEdit.remove(name);
	appendTagEdit();
}

//展示已选择的标签
function appendTagEdit(){
	$("#labelChoose-registerActivity").find(".labelChoose_title").html("<span>已选标签:</span>");
	$.each(addTagIdArrEdit, function(index, value){
      	$("#labelChoose-registerActivity").find(".labelChoose_title").append("<span class='labelChoose_span'>"+addTagNameArrEdit[index]+
      		"<span class='icon icon_close_active' onclick='delAddTagEdit(this)' >" +
      		"<input type='hidden' value='"+addTagIdArrEdit[index]+"'/><input type='hidden' value='"+addTagNameArrEdit[index]+"' />" +
      		"</span></span>");
    });
}

//页面添加选择的标签
function addSelectTagEdit(){
	$("#memberTagList").html("");
	$.each(addTagIdArrEdit, function(index, value){
	  	$("#memberTagList").append("<span class='labelChoose_span'>"+addTagNameArrEdit[index]+
	  		"<span class='icon icon_close_active' onclick='delSelectTagEdit(this)' >" +
	  		"<input type='hidden' value='"+addTagIdArrEdit[index]+"' id='sptMlrid' name='sptMlrid' /><input type='hidden' value='"+addTagNameArrEdit[index]+"'/>" +
	  		"</span></span>");
	});
}

//删除已选标签
function delSelectTagEdit(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArrEdit.remove(id);
	addTagNameArrEdit.remove(name);
	addSelectTagEdit();
}

//查询标签
function queryTagEdit(likeMemberTagName){
	$.ajax({ 
        type: "post", 
        url: $.baseUrl+'/memberTagController/showMemberTagList.do?likeMemberTagName='+likeMemberTagName, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("#labelChoose-registerActivity").find(".labelChoose_list ul").html("");
        	
        	$.each(data.rows, function(index, value){
		      $("#labelChoose-registerActivity").find(".labelChoose_list ul").append("<li><input type='checkbox' onchange='javascript:addTagEdit(this);' name='' value='"+value.memberTagId+"' />" + value.memberTagName+ "</li>");
		    });
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
                alert(errorThrown); 
        } 
    });
}
//会员标签选弹出层JS==============================end

//会员分组JS====================start
//存储勾选的分组id
var addGroupIdArr = new Array();
//存储勾选的分组name
var addGroupNameArr = new Array();

function addSelectGroupToArray(){
	var id = $("#selectMemberGroupId").val();
	var name = $("#selectMemberGroupName").val();
	if(id!='' && !addGroupIdArr.in_array(id)){
		addGroupIdArr.push(id);
	}
	if(name!='' && !addGroupNameArr.in_array(name)){
		addGroupNameArr.push(name);
	}
}

//页面添加选择的分组
function addSelectGroup(){
	$("#memberGroupList").html("");
	$.each(addGroupIdArr, function(index, value){
	  	$("#memberGroupList").append("<span class='labelChoose_span'>"+addGroupNameArr[index]+
	  		"<span class='icon icon_close_active' onclick='delSelectGroup(this)' >" +
	  		"<input type='hidden' value='"+addGroupIdArr[index]+"' id='sptMgoid' name='sptMgoid'/><input type='hidden' value='"+addGroupNameArr[index]+"' />" +
	  				"</span></span>");
	});
}

//删除已选分组
function delSelectGroup(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addGroupIdArr.remove(id);
	addGroupNameArr.remove(name);
	addSelectGroup();
}


//查询分组
function queryGroup(){
	$.ajax({ 
            type: "get", 
            url: $.baseUrl+'/memberGroupController/showMemberGroupListSelect.do', 
            dataType: "json",
            async: false,
            success: function (data) {
            	$("#addFenZu").find(".dropdown-menu").html("");
            	$.each(data, function(index, value){
			      $("#addFenZu").find(".dropdown-menu").append("<li data-id='"+value.memberGroupId+"'><a href='javascript:;'>"+value.memberGroupName+"</a></li>");
			    });
            }, 
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    alert(errorThrown); 
            } 
        });
}
//会员分组JS=======================end

