define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格&下拉框&日期&输入框验证
		initDataGrid();
		initDate();
		initSelect();
		
		$("#queryButton").click(function(){
			initDataGrid();
		});
		
	}

	function initDataGrid(){
		$('#alipayFansTable').datagrid({
			url : $.baseUrl+'/alipayFansController/showAlipayFansList.do',
			method :'post',// 请求类型 
			singleSelect : true,
			loadMsg:"数据加载中...",
		    queryParams :serializeObject($('#fensiguanliForm')),
				onLoadSuccess : function(result) {
						
				},
				onLoadError : function() {
					
				}
			});
	}
	
	window.showFansDetail = function showFansDetail(value,row,index){
		return "<a href='javacript:void(0)' onclick='showFansDetailFrame("+row.alipayFansId+");';>查看</a>";
	}
	
	window.showFansDetailFrame = function showFansDetailFrame(value){
		$('#fensixiangqingform').form('submit',{
			url: $.baseUrl+'/alipayFansController/showAlipayFansDetail.do',
			onSubmit: function(param){
			        param.alipayFansId = value;    
			    },  
			success: function(data){
				var data = eval('(' + data + ')');  // change the JSON string to javascript object
		        if (data.status == 'success'){
		        	$("#headImgUrl").attr("src",data.alipayFans.avatar);
		    		$("#userId").html(data.alipayFans.alipayUserId);
		    		$("#nickName").html(data.alipayFans.nickName);
		    		$("#sex").html(data.alipayFans.gender);
		        }    
			}
		});
		$('#fensixiangqing').show();
		
	}

	return {
		init: init
	}
	
});