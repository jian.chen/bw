define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		window.rowformater_img = function rowformater_img(value){
			if(value == null || value == ''){
				return "<img src='' style='width: 100%;height: 133px;' />";
			}
			return "<img src='"+value+"' style='width: 100%;height: 133px;' />";
		}
		
		//查看
		$('#memberEquityDetailBtn').click(function() {
			var selected  = $('#memberEquityTable').datagrid('getSelected');
			if(selected){
				memberEquityId = selected.memberEquityId;
				common.Page.loadMask('memberEquity/equityDetail.html');
			}else{
				alert("请选择一条会员权益");
				return;
			}
		});
		
		//编辑
		$('#memberEquityEditBtn').click(function() {
			var selected  = $('#memberEquityTable').datagrid('getSelected');
			if(selected){
				memberEquityId = selected.memberEquityId;
				common.Page.loadMask('memberEquity/equityEdit.html');
			}else{
				alert("请选择一条会员权益");
				return;
			}
		});
		
		//删除
		$('#memberEquityDeleteBtn').click(function() {
			var selected  = $('#memberEquityTable').datagrid('getSelected');
			if(selected){
				memberEquityId = selected.memberEquityId;
			}else{
				alert("请选择一条会员权益");
				return;
			}
			//查询详情页面赋值
		    if (confirm('您确认想要删除选中记录吗？')){
		    	removeMemberEquityPre(memberEquityId);
			}   
		});
		
		//新增
		$('#memberEquityAddBtn').click(function() {
			common.Page.loadMask('memberEquity/equityAdd.html');	
		});
		
		$('#memberEquitySearchBtn').click(function() {
			$('#memberEquityTable').datagrid('load', serializeObject($('#memberEquityForm')));
		});
		
		initMemberEquityDataGrid();	
		
	}
	return {
		init: init
	}
});

var memberGroupId;
var isAll;

function initMemberEquityDataGrid(){
	//初始化table
	$('#memberEquityTable').datagrid( {
		url : $.baseUrl+'/memberEquityController/showMemberEquityList.do',
		idField : 'memberEquityId',
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [ 1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 ],
	    queryParams : serializeObject($('#memberEquityForm')),
		    onLoadSuccess : function(result) {
				if(result) {
	            }
			},
			onLoadError : function() {
				
			}
		});

}

//删除会员权益
function removeMemberEquityPre(memberEquityId){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/memberEquityController/removeMemberEquity.do',
		data : "memberEquityId="+memberEquityId,
		dataType : "json",
		async: false,
		success : function(result) {
			//result = eval('('+result+')');
			if(result.code==1){
				alert(result.message);
				$('#memberEquityTable').datagrid('reload'); 
				//清除所有勾选的行
				$("#memberEquityTable").datagrid('clearChecked');
				
			}else {
				alert(result.message+"!!!!!");
			}
			
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});
}


//检查表单
function checkForm(page){
	var isValid = formCheck($('#'+page+'_form'));
	return isValid;
}

//时间格式化
function dateDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}
	
//上传图片
function uploadEquityimage(page){
		$("#dataLoad").show();
		$('#'+page+'_form').form('submit', {    
		    url : $.baseUrl+'/memberEquityController/uploadEquityImage.do',   //加loading 
		    success:function(result){
		    	$("#dataLoad").hide();
		    	result = eval('('+result+')');
		    	if(result == "不被允许上传的文件格式!"){
		    		alert("不被允许上传的文件格式!");
		    	}else if(result == "图片大小不超过1MB"){
		    		alert(result);
		    	}else{
		    		$("#"+page+"_img").val($.rootPath+result);
		    		$("#"+page+"_img_path").attr("src",$.rootPath+result);
		    		$.messager.alert("提示","上传成功");
		    		//页面上传提示
		    		$("#"+page+"_hint").text("已上传");
		    	}
		    }    
		}); 
}

//清空图片
function clearEquityImg(page){
	$("#"+page+"_image").val("");
	$("#"+page+"_img").val("");
	$("#"+page+"_img_path").attr("src","");
	$("#"+page+"_fileSpan").text("未选择文件...");
	
	$("#"+page+"_filePath").val("");
	$("#"+page+"_hint").text("");
}