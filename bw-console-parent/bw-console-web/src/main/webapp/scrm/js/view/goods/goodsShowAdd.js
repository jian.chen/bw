define(['common','view/goods/goodsShowManage'],function(common,goodsShowManage){
	// 积分兑换--等级所需积分
	var goodsGradeConditions=[];
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initInput();
		initSelect();
		//初始化优惠券表格
		initCoupon();
		
		//初始化商品类型下拉框
		initSelectById('goodsShowCategorySelect','/goodsShowController/showGoodsShowCategoryList.do','goodsShowCategoryId','goodsShowCategoryName');

		if(goodsShowManage.getType() == "show"){
			goodsGradeConditions=[];
			$("#goodsShow_title").html('积分兑换详情');
			//初始化等级所需积分
			initGoodsShowCondition();
			//表单禁用
			disableForm('goodsShowAddForm',true);
		}else if(goodsShowManage.getType() == "edit"){
			goodsGradeConditions=[];
			$("#goodsShow_title").html('积分兑换编辑');
			$("#areaSaveBtn").show();
			$("#couponDiv").attr("disabled",true);
			//初始化等级所需积分
			initGoodsShowCondition();
		}else{
			goodsGradeConditions=[];
			$("#areaSaveBtn").show();
			$("#goodsShow_title").html('新建积分兑换');
			//初始化等级所需积分
			initGoodsShowCondition();
		}

		//优惠券弹框
		$("#couponDiv").click(function(){
			$("#coupon-div").show();
			$('#goodsShowCouponTable').datagrid('reload');
		});

		//优惠券筛选
		$("#seachCoupon").click(function(){
			$('#goodsShowCouponTable').datagrid('reload',{
				couponName : $("#couponNameSea").val()
			});
		});

	}
	
	// 校验积分兑换条件
	function checkGoodsShowCond(){
		var flag =true;
		/*var goodsCategoryId = $("#goodsShowCategoryId").val();*/
		if($("#goodsShowCategoryId").val() == ''){
			$("#goodsShowCategorySelect").focus();
			$("#goodsShowCategorySelect").alert('请选择商品类型！');
			flag = false;
			return;
		}
		if($("#statusId").val() == ''){
			$("#statusSelect").focus();
			$("#statusSelect").alert('请选择状态！');
			flag = false;
			return;
		}
		for(var i=0; i <goodsGradeConditions.length; i++){
			if(goodsGradeConditions[i].pointsNumber =='' || goodsGradeConditions[i].pointsNumber ==null){
				alert("所需积分必填！");
				flag = false;
				break;
			}
		}
		/*if($("#couponId").val() == ''){
			$("#couponId").focus();
			$("#couponId").alert('优惠券不为空!');
			flag = false;
			return;
		}*/
		return flag;
	}
	
	function disableTable(){
		$("input[name='No.1']").attr("readonly", "readonly");
		$("input[name='No.2']").attr("readonly", "readonly");
		$("input[name='No.3']").attr("readonly", "readonly");
	}
	
	function initCoupon(){
		$('#goodsShowCouponTable').datagrid( {
			url : $.baseUrl+'/goodsShowController/queryCouponList.do',
			idField : 'couponId',
				onLoadSuccess : function(result) {
					if(result) {
						
		            }
				},
				onLoadError : function() {
					
				}
			});
	}
	
	// 初始化积分兑换-等级所需积分
	function initGoodsShowCondition(){
		$('#goodsShow-condition-table').datagrid( {
			url : $.baseUrl+'/memberCardGradeController/showMemberCardList.do',
			method:'get',
			onLoadSuccess : function(result) {
				if(goodsShowManage.getType() == "show"){
					show();
					disableTable();
				}else if(goodsShowManage.getType() == "edit"){
					show();
				}else{
					assembleGoodsGradeCond(result.rows);
				}
			},
			onLoadError : function() {
			}
		});
	}
	
	function assembleGoodsGradeCond(data){
		for(var i=0;i < data.length;i++){
			var object ={
					goodsShowMemberCardGradeId:'',
					memberCardGradeId:data[i].memberCardGradeId,
					goodsShowId:'',
					pointsNumber:''
			};
			goodsGradeConditions.push(object);
		}
	};
	
	function show(){
		isAuthForAjax("/goodsShowController/showDetail.do?goodsShowId="+goodsShowManage.getGoodsShowId(),
			"POST", "", "JSON", serializeObject($('#goodsShowAddForm')), function (data) {
				$("#goodsShowId").val(data.goodsShowId);
				$("#goodsShowName").val(data.goodsShowName);
				$("#pointsNumber").val(data.pointsNumber);
				$("#goodsShowDesc").val(data.goodsShowDesc);

				$("#goodsShowCategoryId").val(data.goodsShowCategoryId);
				$("#goodsShowCategorySelect").next("ul").find("[data-id="+data.goodsShowCategoryId+"]").find("a").click();
				
				$("#statusSelect").next("ul").find("[data-id="+data.statusId+"]").find("a").click();
				
				$("#couponId").val(data.couponId);
				$("#couponDiv").val(data.couponName);
				
				$("#requireLevel").val(data.requireLevel);
				$("#showGradeCK input[name='level'][value='"+ data.requireLevel +"']").attr("checked",true); 
				showGoodsPointDetail(JSON.parse(data.goodsShowMemberCardGradeList));
			}, function (){
				alert("request error");
			});
	}
	
	function showGoodsPointDetail(obj){
		for(var i=0; i<obj.length;i++){
			goodsGradeConditions.push(obj[i]);
			var memberCardGradeId = obj[i].memberCardGradeId;
			var pointsNumber = obj[i].pointsNumber;
			// 绿卡
			if(memberCardGradeId == 1){
				$("input[name='No.1']").val(pointsNumber);
			}else if(memberCardGradeId == 2){
				$("input[name='No.2']").val(pointsNumber);
			}else if(memberCardGradeId == 3){
				$("input[name='No.3']").val(pointsNumber);
			}
		}
	}
	
	window.goodsShowAdd = function(){
		var levelCk = $('#showGradeCK input[name="level"]:checked').val();
		$("#requireLevel").val(levelCk);
		// 所需积分添加eval("("+c+")")
		$("#goodsShowMemberCardGradeList").val(JSON.stringify(goodsGradeConditions));
		var isValid = formCheck($("#goodsShowAddForm"));
		if(isValid && checkGoodsShowCond()){
			isAuthForAjax("/goodsShowController/saveGoodsShow.do",
				"POST", "", "JSON", serializeObject($('#goodsShowAddForm')), function (result) {
				if(result.status == '200'){
					alert(result.message);
					$('#close_btn').click();
					$("#goodsShowTable").datagrid('load');
				}
				}, function (){
					alert("request error");
				});
		}
	}
	
	window.formatProgress_goodsShowName_points = function (value,row,index){
		//name="'+row.memberCardGradeId+'"
		cond = '<input name="'+row.memberCardGradeCode+'" onblur="javascript:onBlurGoodsShowCond(\''+row.memberCardGradeId+'\',this)" size="16" type="number" class="form-control" style="width: 100%;" value=""/>';
		return cond;
	}
	
	window.onBlurGoodsShowCond = function (code,obj){
		/*var items = JSON.stringify(goodsGradeConditions[code-1]);*/
		var items = goodsGradeConditions[code-1];
		console.log(items);
		items.pointsNumber = obj.value;
		console.log(items);
	}
	
	window.getCouponId = function(){
		var selected = $('#goodsShowCouponTable').datagrid('getChecked');
		$("#couponId").val(selected[0].couponId);
		$("#couponDiv").val(selected[0].couponName);
		$("#closeCoupon").click();
	}
	
	return {
		init:init
	}
	
});

