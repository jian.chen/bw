define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelect();
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		initComplainDataGrid();
	}
	return {
		init: init
	}
});

function initComplainDataGrid(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/complainController/queryCcomplainDetail.do',
		data : "complainId="+complainIds,
		dataType : "json",
		async: false,
		success : function(result) {
			$("#complainDetail_memberCode").val(result.complain.memberCode);
			$("#complainDetail_personName").val(result.complain.personName);
			$("#complainDetail_mobile").val(result.complain.mobile);
			$("#complainDetail_createTime").val(getDateString(result.complain.createTime));
			$("#complainDetail_storeName").val(result.complain.storeName);
			$("#complainDetail_email").val(result.complain.email);
			$("#complainDetail_geoName").val(result.complain.geoName);
			$("#complainDetail_complainDesc").val(result.complain.complainDesc);
			$("#complainDetail_complainImg1").attr("src",result.complain.complainImg1);
			$("#complainDetail_complainImg2").attr("src",result.complain.complainImg2);
			if(result.complain.complainTypeId == '1'){
				$("#complainDetail_type").val("意见");
			}else if(result.complain.complainTypeId == '2'){
				$("#complainDetail_type").val("咨询");
			}else if(result.complain.complainTypeId == '3'){
				$("#complainDetail_type").val("建议");
			}else if(result.complain.complainTypeId == '4'){
				$("#complainDetail_type").val("其他");
			}
			if(result.complain.statusId!=7002 && result.complain.statusId != 7003){
				isRead();
			}
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});
}

function isRead(){
	$.ajax({
		type : "get",
		url : $.baseUrl+'/complainController/readComplain.do',
		data : "complainId="+complainIds,
		dataType : "json",
		async: false,
		success : function(result) {
			$('#complainTable').datagrid('reload'); 
		},
		error : function() {
			alert("出错了:(");
		}
	});
}

function downloadPic(val){
	if(val==1){
		if($("#complainDetail_complainImg1").attr("src")){
			location.href=$.baseUrl+'/complainController/downTemp.do?filePath='+$("#complainDetail_complainImg1").attr("src");
			
		}else{
			alert("文件不存在！");
		}
	}else if(val==2){
		if($("#complainDetail_complainImg2").attr("src")){
			location.href=$.baseUrl+'/complainController/downTemp.do?filePath='+$("#complainDetail_complainImg2").attr("src");
		}else{
			alert("文件不存在！");
		}
	}
}
function DownLoadReportIMG(imgPathURL) {  
    
    //如果隐藏IFRAME不存在，则添加  
    if (!document.getElementById("IframeReportImg"))  
        $('<iframe style="display:none;" id="IframeReportImg" name="IframeReportImg" onload="DoSaveAsIMG();" width="0" height="0" src="about:blank"></iframe>').appendTo("body");  
    if (document.all.IframeReportImg.src != imgPathURL) {  
        //加载图片  
        document.all.IframeReportImg.src = imgPathURL;  
    }  
    else {  
        //图片直接另存为  
        DoSaveAsIMG();    
    }  
}  
function DoSaveAsIMG() {  
    if (document.all.IframeReportImg.src != "about:blank")  
        document.frames("IframeReportImg").document.execCommand("SaveAs");          
} 

