define(['common'], function(common) {
	
	function init() {
		$("#createNew").click(function(){
			common.Page.loadMask('activity/addQuestion.html');
		});
		
		//初始化表格
		initDataGridQuestion();
	}
	
	window.rowformater_questionTable = function (value,row,index){
		var tempHtml = '';
		tempHtml += "<button id='manage' type='button' class='solid-btn' style='font-size: 14px;' onClick='manageQuestion("+row.questionnaireId+","+ row.templateCount + ")'>题目管理</button>";
		tempHtml += "<button id='edit' type='button' class='solid-btn' style='font-size: 14px;' onClick='editQuestion("+row.questionnaireId+")'>编辑</button>";
		tempHtml += "<button id='delete' type='button' class='solid-btn' style='font-size: 14px;' onClick='deleteQuestion("+row.questionnaireId+")'>删除</button>";
		tempHtml += "<button id='count' type='button' class='solid-btn' style='font-size: 14px;' onClick='countQuestion("+row.questionnaireId+")'>统计</button>";
		if(row.statusId == '1902'){
			tempHtml += "<button id='status' type='button' class='solid-btn' style='font-size: 14px;' onClick='stopQuestion("+row.questionnaireId+")'>停用</button>";
		}
		if(row.statusId == '1905'){
			tempHtml += "<button id='status' type='button' class='solid-btn' style='font-size: 14px;' onClick='startQuestion("+row.questionnaireId+")'>启用</button>";
		}
		return tempHtml;
	}
	
	window.rowformater_date = function (value,row,index){
		return row.startDate == null || row.startDate == "" ? "未发布" : getDateString(row.startDate);
	}
	
	return {
		init: init
	}
	
});

var editQuestionId;//存储选择的问卷id
var manageQuestionId;//存储管理的问卷id
var manageTemplateCount;//存储管理问卷的题目数量
var countQuestionId;//查看统计问卷的id

function initDataGridQuestion(){
	$('#questionTable').datagrid({
		url : $.baseUrl+'/QuestionController/showQuestion',
		idField : 'questionnaireId',
		onLoadSuccess : function(result) {
			
		},
		onLoadError : function() {
			
		}
	})
}

function editQuestion(questionnaireId){
	editQuestionId = questionnaireId;
	$('#maskPanel').show().panel({
		href: 'activity/editQuestion.html'
	});
}

function manageQuestion(questionnaireId,count){
	manageQuestionId = questionnaireId;
	manageTemplateCount = count;
	$('#mainPanel').layout('panel', 'center').panel({
		href: 'activity/questionManage.html'
	});
}

function countQuestion(questionnaireId){
	countQuestionId = questionnaireId;
	$('#mainPanel').layout('panel', 'center').panel({
		href: 'activity/questionCount.html'
	});
}

function deleteQuestion(questionnaireId){
	if(confirm("确认要删除吗？")){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/QuestionController/deleteQuestion",
			dataType : "json",
			data : {
				questionnaireId : questionnaireId
			},
			success : function(data){
				alert(data.code);
				$('#questionTable').datagrid('reload');
			}
		})
	
	}
}


function stopQuestion(questionnaireId){
	$.ajax({
		type : "post",
		url : $.baseUrl + "/QuestionController/changeQuestionStatus",
		dataType : "json",
		data : {
			questionnaireId : questionnaireId,
			status : "N"
		},
		success : function(data){
			$.messager.alert("提示",data.message);
			$('#questionTable').datagrid('reload');
		}
	})
}

function startQuestion(questionnaireId){
	$.ajax({
		type : "post",
		url : $.baseUrl + "/QuestionController/changeQuestionStatus",
		dataType : "json",
		data : {
			questionnaireId : questionnaireId,
			status : "Y"
		},
		success : function(data){
			$.messager.alert("提示",data.message);
			$('#questionTable').datagrid('reload');
		}
	})
}


