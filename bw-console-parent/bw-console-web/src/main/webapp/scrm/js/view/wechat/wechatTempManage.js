define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initSelect();
	};

	return {
		init: init
	}
});

function initSelect(){
	$('.dropdown-menu').each(function(i,v){
		var _pp = $(this);
		var s=$.trim($(this).attr("data"));
		var url_options = $.trim($(this).attr("url-options"));
		
		if(s){
			eval("var data = "+s);
			$.each(data,function(i,v){
				_pp.append("<li data-id='"+v.value+"'><a href='javascript:;'>"+v.text+"</a></li>");
			});
		}else if(url_options){
			eval("var urlData = {"+url_options+"}");
			var url = urlData.url;
			var value = urlData.value;
			var text = urlData.text;
			$.ajax({ 
		        type: "get", 
		        url: encodeURI($.baseUrl+url), 
		        dataType: "json",
		        async: false,
		        success: function (data) {
		        	$.each(data, function(i, v){
		        		eval("var aa = v."+value+"");
		        		eval("var bb = v."+text+"");
		        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
				    });
		        }, 
		        error: function (XMLHttpRequest, textStatus, errorThrown) { 
		            alert(errorThrown); 
		        }
		    });
		}
	});
}