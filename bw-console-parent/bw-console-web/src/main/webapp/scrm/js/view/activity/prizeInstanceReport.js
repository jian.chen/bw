var sceneGameInstanceId;
var optStatus;
define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化表格
		showWinningItem();
		
		//返回
		$("#backBtn-form-prizeInstanceReport").click(function () {
			common.Page.loadCenter('activity/prizeInstanceManage.html');
		});
		
		//返回
		$("#seachRecord-prizeInstanceReport").click(function () {
			showWinningItem();
		});
		
	}
	
	//查看中奖纪录
	function showWinningItem(){
		$('#scenegame-instance-record-table').datagrid( {
			url : $.baseUrl+'/sceneGameInstanceController/showGameInstanceRecord.json',
			method:'post',
			queryParams:{'sceneGameInstanceId': sceneGameInstanceId,"memberName":$("#memberName").val(),"memberCode":$("#memberCode").val()},
			onLoadSuccess : function(result) {
				if(result) {
					$("#sceneGameInstanceId").val(sceneGameInstanceId);
				}
			},
			onLoadError : function() {
			}
		});
	}
	
	
	return {
		init: init
	};
});

