define(['common'], function(common) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
		//设置下一步上一步
		$("#nextSetting").click(function(){
			$("#coupon-add-first-setting").hide();
			$("#coupon-add-second-setting").show();
			$("#div-title-first").hide();
			$("#div-title-second").show();
		});
		
		$("#beforeSetting").click(function(){
			$("#coupon-add-first-setting").show();
			$("#coupon-add-second-setting").hide();
			$("#div-title-first").show();
			$("#div-title-second").hide();
		});
		
		$("#goback").click(function(){
			common.Page.loadCenter('coupon/couponManage.html');
		})
		
		$("#goback2").click(function(){
			common.Page.loadCenter('coupon/couponManage.html');
		})
		initSelect();
		initCouponDataGrid();
		
		function showname(conid,value,targetid)
		{
			
		$("#"+targetid).val($("#"+conid).children("li[data-id='"+value+"']").find("a").html());	
		}
		
		
		
		function initCouponDataGrid(){
			$.ajax({
				type : "get",
				url : $.baseUrl+'/couponController/queryCouponDetail.do',
				data : "couponid="+couponIds,
				dataType : "json",
				async: false,
				success : function(result) {
					$("#couponDetail_couponName").val(result.coupon.couponName);
					if(result.coupon.expireType=='1'){
						$("#after_date").hide();
						$("input[name='couponDetail_coupon_Date']:eq(0)").attr("checked",'checked');
						$("#couponDetail_fromDate").val(dateDateformat(result.coupon.fromDate));
						$("#couponDetail_thruDate").val(dateDateformat(result.coupon.thruDate));
					}else if(result.coupon.expireType=='2'){
						$("#fixed_date").hide();
						$("input[name='couponDetail_coupon_Date']:eq(1)").attr("checked",'checked');
						$("#couponDetail_afterDate").val(result.coupon.afterDate);
						$("#couponDetail_expireValue").val(result.coupon.expireValue);
					}
					$("#couponDetail_orgId").val(result.couponRange.orgName);
					$("#couponDetail_extField1").val(result.coupon.extField1);
					$("#couponTypeName").text(result.couponType.couponTypeName);
					$("#couponDetail_totalQuantity").val(result.coupon.totalQuantity);
					$("#couponDetail_couponImg3_path").attr("src",result.coupon.couponImg3);
					$("#couponDetail_couponImg2_path").attr("src",result.coupon.couponImg2);
					$("#couponDetail_comments").val(result.coupon.comments);
					$("#couponDetail_remark").val(result.coupon.remark);
					
					$('#coupon_type').val(result.couponType.couponTypeName);
					$('#couponTYpe_type').val(result.couponType.couponTypeName);
					
					showname("ul",result.coupon.originId,"couponTYpe_type");
					showname("ul1",result.coupon.externalMerchantId,"couponMer_type");
					
					$("#couponDetail_couponNameEn").val(result.coupon.couponNameEn);
					$("#couponDetail_commentsEn").val(result.coupon.commentsEn);
					$("#couponDetail_remarkEn").val(result.coupon.remarkEn);
					$("#couponDetail_webchatRemark").val(result.coupon.webchatRemark);
					$("#couponDetail_extField1En").val(result.coupon.extField1En);
					
					if(result.coupon.isReceive == 'N'){
						$("#isReceiveCK").attr("checked","checked")
					}
					if(result.coupon.isRemind == 'Y'){
						$("#isRemindCK").attr("checked","checked")
					}
					if(result.coupon.isOwnerUsed == 'N'){
						$("#isOwnerUsedCK").attr("checked","checked")
					}
				},
				error : function() {
					parent.layer.alert("出错了:(");
				}
			});
			if($('#couponTYpe_type').val()=='内部券'){
				$('#merchat').hide();
				$('#externalMerchantId').val('0');
			}else if($('#couponTYpe_type').val()=='外部券'){
				$('#merchat').show();
			}
		}
		
	}
	return {
		init: init
	}
});

