define(['common'], function(common) {
	//初始化table
	function init(){
		
		initLoad();
		
		//返回
		$("#backBtn-form-activityInstanceReport").click(function () {
			common.Page.loadCenter('channel/channelInstanceManage.html');
		});
		
		$("#seachRecord").click(function(){
			$('#channel-instance-record-table').datagrid('load', serializeObject($('#recordForm')));
		});
	}
	
	function initLoad(){
		$('#channel-instance-record-table').datagrid( {
			url : $.baseUrl+'/channelController/showChannelInstanceRecord',
			method:'post',
			queryParams:{'channelCfgId': channelCfgId},
			onLoadSuccess : function(result) {
				if(result) {
					$("#channelCfgId").val(channelCfgId);
				}
			},
			onLoadError : function() {
			}
		});
	}
	return{
		init:init
	};
});

