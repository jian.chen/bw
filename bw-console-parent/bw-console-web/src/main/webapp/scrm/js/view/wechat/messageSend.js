define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		$('.wechat_msg_top_row').delegate('[data-for]', 'click', function(){
			$($(this).data('for')).show().siblings().hide();
			$(this).addClass('active').siblings().removeClass('active');
		});
		$('#showSucai').click(function(){
			$('#suCai').show();
		});
		
		initSelect("groupTypeSelect");
		
		$("#up").uploadPreview({ Img: "ImgPr", ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		
	};

	return {
		init: init
	}
});

//第一个下拉框初始化
function initSelect(id){
	$('.dropdown-menu').each(function(i,v){
	var _pp = $(this);
	var s=$.trim($(this).attr("data"));
	var url_options = $.trim($(this).attr("url-options"));
	
	var js="";
	
	if(s){
		eval("var data = "+s);
		$.each(data,function(i,v){
			if(id=="groupTypeSelect"){
				js="getGroups("+v.value+")";
			}
			_pp.append("<li data-id='"+v.value+"'><a href='javascript:"+js+";'>"+v.text+"</a></li>");
		});
	}else if(url_options){
		eval("var urlData = {"+url_options+"}");
		var url = urlData.url;
		var value = urlData.value;
		var text = urlData.text;
		$.ajax({ 
	        type: "get", 
	        url: encodeURI($.baseUrl+url), 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$.each(data, function(i, v){
	        		eval("var aa = v."+value+"");
	        		eval("var bb = v."+text+"");
	        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
			    });
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        }
	    });
	}
});
}
//设置第二个下拉框options
function getGroups(obj){
	//alert(obj);
	$("#groupSelectUl").html("");
	$("#groupSelect").val("");
	var url="/wechatGroupMessageController/showAllGroups.do?type="+obj;
	if(obj==1){
		$("#groupSelectDiv").show();
		initSelectById("groupSelect",url,"wechatFansGroupId","groupName");
	}else if(obj==2){
		$("#groupSelectDiv").show();
		initSelectById("groupSelect",url,"wechatFansLabelId","labelName");
	}else{
		$("#groupSelectDiv").hide();
	}
	
}
//初始化第二个下拉框
function initSelectById(id,selectUrl,valueFiled,textFiled){
	var _pp = $('#'+id).next("ul");
	if(!_pp){
		return;
	}
	var url = selectUrl;
	var value = valueFiled;
	var text = textFiled;
	$.ajax({ 
        type: "get", 
        url: encodeURI($.baseUrl+url), 
        dataType: "json",
        async: false,
        success: function (data) {
        	$.each(data, function(i, v){
        		eval("var aa = v."+value+"");
        		eval("var bb = v."+text+"");
        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
		    });
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        }
    });
}
