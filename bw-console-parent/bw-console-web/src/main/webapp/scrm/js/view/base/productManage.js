var prodBackUrl = 'base/productItem.html';
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		loadThisMask(prodBackUrl);
		$('#productManage_tab li').removeClass("active");
		$("#productManage_tab li").each(function(){
			if($(this).attr("data-url") == prodBackUrl){
				$(this).addClass("active");
			}
		});
		
		$('#productManage_tab li').click(function(){
			var url = $(this).data('url');
			$(this).addClass('active').siblings().removeClass('active');
			loadThisMask(url);
		});
		
	};

	function loadThisMask(url) {
		$('#productliPanel').show().panel({
			href: url
		});
	};
	return {
		init: init
	}
});