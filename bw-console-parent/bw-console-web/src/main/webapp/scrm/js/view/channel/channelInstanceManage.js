var channelCfgId;
var pageTag;
var channelId;
define(['common'], function(common) {
	//初始化table
	function init(){
		
		initLoad();
		
		this.channelId=channelId;
		
		$("#seachRecord").click(function(){
			$('#channel-instance-record-table').datagrid('load', serializeObject($('#recordForm')));
		});
		
		//返回
		$("#backBtn-form-activityInstanceManage").click(function () {
			 common.Page.loadCenter('channel/channelManager.html');
		});
		
	}
	
	function initLoad(){
		$('#channel-instance-table').datagrid( {
			url : $.baseUrl+'/channelController/showChannelInstanceList',
			method:'post',
			queryParams:{'channelId': channelId},
			onLoadSuccess : function(result) {
				if(result) {
                }
			},
			onLoadError : function() {
			}
		});
	}
	
	window.channelFormatProgressBtn = function (value,row,index){
		
		var s = '<button onclick="reportChannelBtn('+value+')" type="button" class="solid-btn">报告</button>';
		
		s = s + '<button onclick="settingChannelNoSaveBtn('+value+')" type="button" class="border-btn">查看</button>';
		
		//中止活动(只有已创建和进行中的活动可以中止&设置)
		if(row.statusId == '2102' || row.statusId == '2101'){
			s = s + '<button onclick="settingChannelBtn('+value+')" type="button" class="border-btn">设置</button>';
	    	s = s + '<button  type="button" onclick="channelStopBtn('+value+')" class="solid-btn">终止 </button>';
	    }else if(row.statusId == '2103' || row.statusId == '2104'){
	    	s = s + '<button  type="button" style="background: #cdcdcd;" class="solid-btn">设置</button>';
	    	s = s + '<button  type="button" style="background: #cdcdcd;" class="solid-btn">终止 </button>';
	    }
	    
		return s;
	};
	
	window.settingChannelBtn = function settingChannelBtn(instanceId){
		channelCfgId = instanceId;
		saveType = 'edit';
		pageTag='instanceManage';
		common.Page.loadCenter('channel/channelSetting.html');
	};
	
	window.settingChannelNoSaveBtn = function settingChannelNoSaveBtn(instanceId){
		channelCfgId = instanceId;
		saveType = 'show';
		pageTag='instanceManage';
		common.Page.loadCenter('channel/channelSetting.html');
	};
	
	window.reportChannelBtn = function reportChannelBtn(instanceId){
		channelCfgId = instanceId;
		common.Page.loadCenter('channel/channelInstanceReport.html');
	};
	
	//结束时间
	window.thruDate_setting = function(value, row, index){
		var s;
		if(row.validityType == 'forever'){
			s = '永久有效';
		}else{
			s = getDateString(value);
		}
		return s;
	}
	
	//终止活动
	window.channelStopBtn = function(instanceId){
		$.messager.confirm('确认','您确认想要终止此配置吗？终止后将不能再启用',function(r){    
		    if (r){    
		    	$.ajax({
					url:$.baseUrl+'/channelController/updateChannelCfgStatus',
					type:"POST",
					data:"channelCfgId="+instanceId,
					datatype:"json",
					success:function(result){
						if(result.status == '200'){
							alert('中止失败，请重试！');
						}else{
							alert('活动已中止！');
							$("#channel-instance-table").datagrid('load');
						}
					}
					
				});
		    }
		});
	};
	
	return{
		init:init
	};
});

