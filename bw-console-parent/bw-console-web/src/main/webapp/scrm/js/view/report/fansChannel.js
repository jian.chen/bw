define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//默认查询当前日期前一天数据
		$("#startDate").val(getDate());
		$("#endDate").val(getDate());
		
		//初始化表格&下拉框&日期&输入框验证
		initDataGrid();
		initSelect(); 
		initDate();
		initInput();
		
		$("#daochuFC").click(function(){
			location.href = $.baseUrl + '/FansChannelSummaryDayController/leadToExcelFansChannel?startDate='+$("#startDate").val()+'&endDate='+$("#endDate").val();
		});
		
		$("#searchFC").click(function(){
			var ck = formCheck($("#fansChannelForm"));
			if(ck){
				$('#fansChannelTable').datagrid('load', serializeObject($('#fansChannelForm')));
			}else{
				alert("请输入合法数据！");
			}
		});
		
	}

	function initDataGrid(){
		$('#fansChannelTable').datagrid( {
			url : $.baseUrl+'/FansChannelSummaryDayController/showListByExample.do',
				onLoadSuccess : function(result) {
				},
				onLoadError : function() {
				}
			})
	}
	//获取当前日期的前一天(返回字符串yyyy-mm-dd)
	function getDate(){
		var date = new Date();
		var yesterday_milliseconds=date.getTime()-1000*60*60*24;        
	    var yesterday = new Date(); 
	    yesterday.setTime(yesterday_milliseconds);  
       
        var year = yesterday.getFullYear();       //年
        var month = yesterday.getMonth() + 1;     //月
        var day = yesterday.getDate();            //日
       
        var clock = year + "-";
       
        if(month < 10){
        	month = "0" + month;
        }
        if(day < 10){
        	day = "0" + day;
        }
        
        clock += month + "-"+day;
       
        return(clock); 
    } 
	
	window.accountForFC = function accountForFC(value, row, index){
		if(row.dailyAddNumber == row.totalNumber){
			return '100%';
		}
		var a = row.dailyAddNumber / row.totalNumber;
		var b = a.toFixed(4);
		var rate = b.slice(2,4)+"."+b.slice(4,6)+"%";
		return rate;
	}
	
	window.accountForFC2 = function(value,row,index){
		fansNum = row.fansNum;
		if(row.totalNumber == fansNum){
			return '100%';
		}
		var a = row.totalNumber / fansNum;
		var b = a.toFixed(4);
		var rate2 = b.slice(2,4)+"."+b.slice(4,6)+"%";
		return rate2;
	}
	
	window.getChannelName = function(value){
		if(value){
			return value;
		}
		return '微信';
	}
	
	return {
		init: init
	}

});

