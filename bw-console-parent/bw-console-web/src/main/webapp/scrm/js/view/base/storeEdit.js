define(['common', 'view/base/storeManage','view/member/areaLinkage'], function(common, storeManage,areaLinkage) {
	function init() {
		common.Page.resetMain();
		var savetype;
		
		//初始化下拉框&输入框验证
		initSelect();
		initInput();
		
		initAddressData("provinceName",'20001');
		
		$('#mendian_tree_btn').click(function(){
			$('#mendian_tree').show();
		});
		
		if(storeManage.getType() == 'show'){
			$("#sto_title").html('门店详情');
			
			$("#savea").hide();
			show();
			//表单禁用
			disableForm('storeForm',true);
			$("#mendian_tree_btn").hide();
		}else if(storeManage.getType() == 'edit'){
			$("#sto_title").html('门店编辑');
			
			$("#storeId1").val(storeManage.getStoreId());
			$("#savea").show();
			savetype = 'edit';
			show();
		}else{
			$("#sto_title").html('新建门店');
			savetype = 'save';
		}
		
		$("#savea").click(function(){
			var ck = formCheck($("#storeForm"));
			if(ck){
				if($("#geoId").val() == ''){
					$("#provinceName").alert('请选择门店所在省市！');
					return;
				}
				if(savetype == 'edit'){
					editStore();
				}else{
					saveStore();
				}
			}else{
				return;
			}
		});
		
		$('#mendian_tree_btn').click(function() {
			initTreeData("areaChooseAdd-store","areaTreeAdd-store","/orgController/OrgTree.do","2");
			if($("#pareOrgId").val() != null){
				selectOrg($("#pareOrgId").val(),"areaTreeAdd-store");
			}
		});
		
		//区域门店保存
		$('#areaSaveBtnAdd').click(function() {
			var selectIds= getSelectId("areaTreeAdd-store",2);
			var selectNames= getSelectName("areaTreeAdd-store",2);
			$("[name='pareOrgId']").val(selectIds);
			$("[name='pareOrgName']").val(selectNames);
			$('#areaChooseAdd-store').find(".detailed_close2").click();
		});
		
	}
	
	function show(){
		$.ajax({
			url : $.baseUrl+'/storeController/showStoreById.do',
			type:"POST",
			data:"storeId="+storeManage.getStoreId(),
			datatype:"json",
			success:function(result){
				if(result){
					result = eval('(' + result + ')');
					
					$("#storeCode1").val(result.store.storeCode);
					$("#longitude1").val(result.store.longitude);
					$("#storeName1").val(result.store.storeName);
					$("#latitude1").val(result.store.latitude);
					$("#managerName1").val(result.store.managerName);
					if(result.store.geoId != null && result.store.geoId != ''){
						$("#geoId").val(result.store.geoId);
						$("#provinceId").val(result.provinceId);
						$("#provinceName").val(result.provinceName);
						initAddressData("cityName",result.provinceId);
						$("#cityName").val(result.cityName);
					}
					if(result.store.orgId != null && result.store.orgId != ''){
						$("#orgId1").val(result.store.orgId);
						$("#pareOrgId").val(result.store.pareOrgId);
						$("#pareOrgName").val(result.store.pareOrgName);
					}
					$("#mobile1").val(result.store.mobile);
					$("#adress1").val(result.store.adress);
					$("#storeDesc1").val(result.store.storeDesc);
					
				}else{
					alert('出错了！');
				}
			}
		});
	}

	function saveStore(){
		$("#dataLoad").show();
			$.ajax({
				url:$.baseUrl+'/storeController/saveStore.do',
				type:"POST",
				data:serializeObject($('#storeForm')),
				datatype:"json",
				success:function(result){
					$("#dataLoad").hide();
					result = $.parseJSON(result);
					if(result.status == '200'){
						$("#closeButton").click();
						$("#storeTable").datagrid('load');
					}
					alert(result.message);
				}
			
			});
	}

	function editStore(){
		$("#dataLoad").show();
			$.ajax({
				url:$.baseUrl+'/storeController/editStore.do',
				type:"POST",
				data:serializeObject($('#storeForm')),
				datatype:"json",
				success:function(result){
					$("#dataLoad").hide();
					result = $.parseJSON(result);
					if(result.status == '200'){
						$("#closeButton").click();
						$("#storeTable").datagrid('load');
					}
					alert(result.message);
				}
			});
		
	}
	
	return {
		init: init
	}
});



