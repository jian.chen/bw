define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		loadThisMask('wechat/wechatFansManagei.html');
		$('#xitongguanli_tab li').click(function(){
			var url = $(this).data('url');
			var thisDom = $(this); 
			isAuthSys(url,thisDom);
		});
	};

	function loadThisMask(url) {
		$('#gongzhonghaoguanliPanel').show().panel({
			href: url
		});
	};
	return {
		init: init
	}
});

function isAuthSys(url,thisDom){
	$.ajax({
		type : "post",
		url : $.baseUrl + "/sysRoleController/isAuth",
		async : false,
		dataType: "json",
		data : {
			url : "/"+url
		},
		success : function(data){
			if(data.flag == false){
				$.messager.alert("访问受限","抱歉您没有此操作权限");
				return;
			}
			if(data.flag == true){
				thisDom.addClass('active').siblings().removeClass('active');
				$('#gongzhonghaoguanliPanel').show().panel({
					href: url
				});
			}
		}
	})
}