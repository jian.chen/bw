define(['common'], function(common) {
	
	function init(){
		$("#saveVote").click(function(){
			var voteName = $("#voteName").val();
			var voteDetail = $("#voteDetail").val();
			var startDate = $("#startDate").val();
			var endDate = $("#endDate").val();
			if(voteName == ""){
				$("#voteName").alert("不能为空");
				return;
			}
			$.ajax({ 
		        type: "post", 
		        url: $.baseUrl+'/VoteController/addVoteTitle', 
				dataType : "json",
		        data : {
		        	voteName: voteName, 
		        	voteDetail : voteDetail,
		        	startDate : startDate,
		        	endDate : endDate
		        },
		        success: function (data) {
		    		alert(data.message);
		    		$(".detailed_close").closest('.mask, .mask_in').hide();
		    		$('#voteTable').datagrid('reload');
		        }
		    });
			
		});
	}

	return {
		init: init
	}
	
	
});


