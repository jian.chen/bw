define(['common','view/sys/smsChannelEdit'], function(common, smsChannelEdit) {
	function init() {
		var smsChannelId = smsChannelEdit.getSmsChannelId();
		$("#sendBtn").click(function() {
			testSms();
		});
		$("#backTestBtn").click(function(){
			$('button.detailed_close').click();
		});
	};

	function testSms() {
		var params = {};
		var mobile = $("#mobile").val();
		if (mobile == '' || mobile == null) {
			$.messager.alert("提示", "手机号不为空");
			return;
		}
		if (!(/^1[3|5|8][0-9]\d{4,8}$/.test(mobile))) {
			$.messager.alert("提示", "手机号非法");
			return false;
		}
		params.smsChannelId = smsChannelEdit.getSmsChannelId();
		params.mobile = mobile;
		$("#dataLoad").show();
		isAuthForAjax("/smsChannelController/testSmsChannel.do", "POST", "", "JSON", params, function(result){
			if (result != null && result.status == "200") {
				$.messager.alert("提示", "短信通道测试成功");
			} else {
				$.messager.alert("提示", "短信通道测试失败");
			}
			$("#dataLoad").hide();
		}, function(data) {
			$.messager.alert("提示", "出现错误");
			$("#dataLoad").hide();
		});
	}

	return {
		init: init
	}
});

