define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		$("#equityEdit_image").uploadPreview({ Img: "equityEdit_img_path", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		//权益基本信息
		showMemberEquityDetail();
	}
	return {
		init: init
	}
});


function showMemberEquityDetail(){
	$.ajax({ 
        type: "post", 
        url: $.baseUrl+'/memberEquityController/showMemberEquityDetail.do', 
        dataType: "json",
        async: false,
        data : {
        	memberEquityId : memberEquityId
        },
        success: function (data) {
        	var span = "";
        	
        	$("#member_EquityId").val(data.memberEquity.memberEquityId);
        	$("[name='equityName']").val(data.memberEquity.equityName);
        	$("[name='equityContent']").val(data.memberEquity.equityContent);
        	$("#equity_Starttime").val(dateDateformat(data.memberEquity.equityStarttime));
			$("#equity_Endtime").val(dateDateformat(data.memberEquity.equityEndtime));
        	
        	for(var i = 0; i < data.gradeList.length; i++){
        		if(data.egRelationList.length > 0){
	        		 for(var j = 0; j< data.egRelationList.length; j++){
	        			 if(data.egRelationList[j].gradeId == data.gradeList[i].gradeId){
	        				 span += "<span>";
	                		 span += "<input type='checkbox' name='gradeId_"+i+"' value='"+data.gradeList[i].gradeId+"' checked='checked' disabled> "+data.gradeList[i].gradeName;
	                		 span += "</span>";
	                		 break;
	        			 }
	        			 if(j==data.egRelationList.length-1){
	        				 span += "<span>";
	        				 span += "<input type='checkbox' name='gradeId_"+i+"' value='"+data.gradeList[i].gradeId+"' disabled> "+data.gradeList[i].gradeName;
	        				 span += "</span>"; 
	        			 }
	        		 }
        		}else{
        			span += "<span>";
					span += "<input type='checkbox' name='gradeId_"+i+"' value='"+data.gradeList[i].gradeId+"' > "+data.gradeList[i].gradeName;
					span += "</span>";
        		}
        	}
        	$("#grade_div").append("");
        	$("#grade_div").append(span);
        	
        	$("#equityEdit_img").val(data.memberEquity.equityImg);
        	$("#equityEdit_img_path").attr("src",data.memberEquity.equityImg);
        	
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}
