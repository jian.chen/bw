var activityType;
var activityId;
var activityTitle;
var saveType;
var pageTag;
var parentTag;
var typeId;
define(['common'], function(common) {
	
	function init(typeId) {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		this.typeId = typeId;
		//初始化表格
		initDataGrid(typeId);
		parentTag = typeId;
		
		//初始化日期插件
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd',
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
		});
		
	}
	
	function initDataGrid(typeId){
		$('#activityTable').datagrid( {
			url : $.baseUrl+'/activityController/showActivityList.json',
			idField : 'activityId',
			queryParams : {
				typeId : typeId
			},
			onLoadSuccess : function(result) {
				//合并列
				var rowCount = $("#activityTable").datagrid("getRows").length;
				
				var span;
				var PerValue = "";
				var CurValue = "";
				var index;
				
				span = 1;
				for (var row = 0; row <= rowCount; row++) {
					 if (row == rowCount) {
						 CurValue = "";
					 }else {
						 CurValue = $("#activityTable").datagrid("getRows")[row]['activityTypeId'];
					 }
						index = row - span;
						if(index < 0){
							index = 0;
							span = span - 1;
						} 
						$("#activityTable").datagrid('mergeCells', {
							index: index,
							field: 'activityTypeId',
							rowspan: rowCount,
							colspan: null,
						});
						span = 1;
						PerValue = CurValue;
				}
				
			},
			onLoadError : function() {
			}
		});
		
		window.bangding = function(value,id){
			activityType = value;
			activityId = id;
			saveType = 'save';
			pageTag = "activityManage";
			common.Page.loadCenter('activity/activitySetting.html');
		};
		
		window.bangding_manage = function(value,id){
			activityType = value;
			activityId = id;
			pageTag = "instanceManage";
			common.Page.loadCenter('activity/activityInstanceManage.html');
		};
		
	}
	
	return {
		init: init,
		getActivityType : function(){
			return activityType;
		},
		getActivityId : function(){
			return activityId;
		}
	};
});

function rowformater_activity(value,row,index){
	return "<button id='"+value+"_start' type='button' onclick='javascript:bangding(&quot;"+value+"&quot;,"+row.activityId+")' class='solid-btn'>发起</a>" +
			"<button id='"+value+"_manage' type='button' onclick='javascript:bangding_manage(&quot;"+value+"&quot;,"+row.activityId+")' class='border-btn'>管理</a>";
}


	

