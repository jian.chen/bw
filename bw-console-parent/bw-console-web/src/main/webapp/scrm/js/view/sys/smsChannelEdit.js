//define(['common', 'jquery', 'underscore', 'json2', 'jqueryeasyui', 'jqueryI18n2', 'jqueryI18n', 'view/sys/smsChannelManage'], function(common, $, _, JSON, easy, i18n, i18nconf, smsChannelManager) {
define(['common', 'view/sys/smsChannelManage'], function(common, smsChannelManager) {
    var smsChannelId;
    function init() {
        smsChannelId = smsChannelManager.getSmsChannelId();
		//初始化输入框验证
		//initInput();
		show();
        $("#backBtn").click(function(){
            smsChannelManager.back();
        });
        $("#saveSms").click(function() {
            var params = serializeObject($('#smsChannelForm'));
            if (params.appId == "") {
                $.messager.alert("提示", "通道账号不能为空");
                return false;
            }
            if (params.appSecret == "") {
                $.messager.alert("提示", "通道账号不能为空");
                return false;
            }
            params.smsChannelId = smsChannelManager.getSmsChannelId();
            saveSmsChannel(params);
        });
        $("#testSms").click(function() {
            testSms();
        });
	};

	function show(){
        var params = {};
        params.smsChannelId = smsChannelManager.getSmsChannelId();
        isAuthForAjax("/smsChannelController/showSmsChannelById.do", "POST", "", "JSON", params, function(result){
            if(result != null && result.status == "200"){
                var content = result.obj;
                $("#sms_appId").val(content.appId);
                $("#sms_appSecret").val(content.appSecret);
                $("#sms_channelId").val(content.smsChannelId);
                $("input[name='isActive']").each(function(index){
                    var _this = $(this);
                    if (_this.val() == content.isActive) {
                        _this.attr("checked", true);
                    }
                })
            }else{
                $.messager.alert("提示", "加载数据出错");
            }
        }, function(data) {
            $.messager.alert("提示", "出错");
        });
	}

	function saveSmsChannel(params){
		//$("#dataLoad").show();
        isAuthForAjax("/smsChannelController/editSmsChannel.do", "POST", "", "JSON", params, function(result){
            if (result != null && result.status == "200") {
                $.messager.alert("提示", "保存账号成功", "info", function() {smsChannelManager.back();});
            } else {
                $.messager.alert("提示", "保存账号失败");
            }
        }, function(data) {
            $.messager.alert("提示", "保存账号失败");
        });
	}

    function testSms() {
        isAuth('/smsChannelController/testSmsChannel.do',function(){
            common.Page.loadMask('sys/smsChannelTest.html');
        });
    }

	return {
		init: init,
        getSmsChannelId: function() {
            return smsChannelId;
        }
	}
});

