var couponIds;
var editStatus;
var edit4originalCouponTotal;
var ing;
var couponStatus;
var couponOriginName;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		$('#xinjianyouhuiquan').click(function() {
			isAuth("/couponController/insertCoupon.do",function(){
				common.Page.loadCenter('coupon/couponAdd.html');
			})
		});
		
		
		//导入
		$("#cateImport").click(function(){
			if(checkSingle()){
				var couponId = getCheckedId();
					$("#couponId").val(couponId);
					$('#importCateMask').show();
			}
		})
		
		
		//下载模板
		$('#downImportTemp').click(function() {
			downTemp();
		});
		
		
		//文件路径
		$('#fileInput').change(function() {
			$("#fileSpan").text($(this).val());
		});
		
		//导入提交
		$('#importBtn').click(function() {
			importData();
		});
		
		$('#couponIssue').click(function(){
			if(checkSingle()){
				if(isCouponIssue() ){
					if (getOriginName() == "外部券"){
						alert("外部券不能发放。");
						return;
					}
					couponIds = getCheckedId();
					isAuth("/couponIssueController/saveCouponIssues.do",function(){
						common.Page.loadCenter('coupon/couponIssue.html');
					})
						
				}
			}
			
		});
		
		$('#editCoupon').click(function(){
			if(checkSingle()){
				isAuth("/couponController/editCoupon.do",function(){
					if(checkStatus()){
						editStatus = "Y";
					}else{
						editStatus = "N";
					}
					couponIds = getCheckedId();
					common.Page.loadCenter('coupon/couponEdit.html');
				})
			}
		});
		
		$('#fafangjiluBtn').click(function(){
			if(checkSingle()){
				couponIds = getCheckedId();
				isAuth("/couponIssueController/showCouponIssueList.do",function(){
					$("#fafangjilu").show();
					showCouponIssueDetail();
				})
			}
		});
		$('#mingxiBtn').click(function(){
			if(checkSingle()){
				couponIds = getCheckedId();
				isAuth("/couponIssueController/showCouponInstance.do",function(){
					common.Page.loadMask('coupon/couponRecordDetail.html');
				});
			}
		});
		$('#searchCouponDetail').click(function(){
			if(checkSingle()){
				couponIds = getCheckedId();
				isAuth("/coupon/couponDetail.html",function(){
					common.Page.loadCenter('coupon/couponDetail.html');
				});
			}
		});
		$('#search_coupon_btn').click(function() {
			$('#couponTable').datagrid("load", {    
				couponName: $('#couponName').val()  
			});
		});
		initDataGrid();
	}
	//初始化数据
	function initDataGrid(){
		//初始化table
		$('#couponTable').datagrid( {
			url : $.baseUrl+'/couponController/queryCouponList.do',
			onLoadSuccess : function(result) {
				if(result) {
                }
			},
			onLoadError : function() {
				
			}
		});
	}
	return {
		init: init
	}
});

//检查表单
function checkForm(page){
	var isValid = formCheck($('#'+page+'_form'));
	return isValid;
}

//检查是否单选
function checkSingle(){
	var selected = $('#couponTable').datagrid('getChecked');
	if(selected.length==1){
		return true;
	}else{
		$.messager.alert("提示","请勾选一条记录!");
		return false;
	}

}
//获取选择Id
function getCheckedId(){
	var selected = $('#couponTable').datagrid('getChecked');
	var couponIds='';
	for(var i=0; i<selected.length; i++){
	    if (couponIds != ''){ 
	    	couponIds += ',';
	    }
	    couponIds += selected[i].couponId;
	}
	return couponIds;
}
function getOriginName() {
	var selected = $('#couponTable').datagrid('getChecked');
	couponOriginName = selected[0].originName;
	return couponOriginName;
}
//检查优惠券状态
function checkStatus(){
	var selected = $('#couponTable').datagrid('getChecked');
	couponStatus = selected[0].statusId;
	if(couponStatus=='1401'){
		return true;
	}else{
		return false;
	}
}

//检查优惠券是否可以发放
function isCouponIssue(){
	queryIsIssueIng();
	var selected = $('#couponTable').datagrid('getChecked');
	var couponStatus = '';
	couponStatus = selected[0].statusId;
	var thruDate = '';
	var curDate = getDate();
	var issueProportion = selected[0].issueProportion;
	var avaNum = selected[0].issueQuantity;
	var toaNum = selected[0].totalQuantity;
	thruDate = selected[0].thruDate;
	if(couponStatus=='1406'){
		$.messager.alert("提示","优惠券发放中!");
		return false;
	}
	if(selected[0].expireType=='1'){
		if(parseInt(curDate)>parseInt(thruDate)){
			$.messager.alert("提示","优惠券已过期!");
			return false;
		}
	}
	if(ing != '0'){
		$.messager.alert("提示","系统只能同时发放一张优惠券，请稍后再试！");
		return false;
	}else if(toaNum == avaNum){
		$.messager.alert("提示","优惠券已发完!");
		return false;
	}else if(toaNum == 0){
		return true;
	}else{
		return true;
	}
}


//下载模板
function downTemp() {
	window.location=$.baseUrl+'/couponIssueController/downloadExternalCouponTmpl.do';
}


function importData() {
	$('#importForm').form('submit', {    
	    url:$.baseUrl+'/couponIssueController/importExternalCoupon.do',
	    onSubmit: function(){    
	    },    
	    success:function(result){
	    	var result = $.parseJSON(result);
	    	$.messager.alert("提示",result.message);
	    	if(result.status == '200'){
	    		$('#importCateMask').find(".detailed_close").click();
	    		$("#productCategoryliTable").datagrid('reload');
	    	}
	   }    
	});  
}

function queryIsIssueIng(){
	$.ajax({
	type : "post",
	url : $.baseUrl+'/couponIssueController/queryIsIssueIng.do',
	dataType : "json",
	async : false,
	success : function(data) {
		if(data.status == '200'){
			ing = data.message;
		}else{
			ing = '1';
		}
	}
	});
}

function removeCouponsPre(){
	if(checkSingle()){
		if(checkStatus()){
			if(confirm("确定删除吗？")){
				removeCoupons();
			}
		}else{
			$.messager.alert("提示","不可删除已发放优惠券");
		}
	}
}
//删除优惠券
function removeCoupons(){
//	alert("remove");
//	if(checkSingle()){
		couponIds = getCheckedId();
		
		isAuthForAjax('/couponController/removeCoupons.do',"get","","json","couponids="+couponIds,function(result) {
			result = eval('('+result+')');
			if(result.code==1){
				$.messager.alert("提示",result.message);
				$('#couponTable').datagrid('reload'); 
				
			}else {
				$.messager.alert("提示",result.message);
			}
			
		},function() {
			parent.layer.alert("出错了:(");
		});
		
		/*$.ajax({
			type : "get",
			url : $.baseUrl+'/couponController/removeCoupons.do',
			data : "couponids="+couponIds,
			dataType : "json",
			async: false,
			success : function(result) {
				result = eval('('+result+')');
				if(result.code==1){
					alert(result.message);
					$('#couponTable').datagrid('reload'); 
					
				}else {
					alert(result.message+"!!!!!");
				}
				
			},
			error : function() {
				parent.layer.alert("出错了:(");
			}
		});*/
}
//查询优惠券发布信息
function showCouponIssueDetail(){
	$('#couponManage_couponIssue_detail').datagrid( {
		url : $.baseUrl+'/couponIssueController/showCouponIssueList.do',
		queryParams : {
			couponId : couponIds
		},
		onLoadSuccess : function(result) {
		},
		onLoadError : function() {
			
		}
	});
}
//核销优惠券
function verificationCoupon(){
	$("#hexiaoResult").css('display','none'); 
	var instanceCode = $("#couponUse_instanceCode").val();
	if(instanceCode == ""){
		$.messager.alert("提示","请输入券码！");
		return;
	}
	isAuthForAjax('/couponIssueController/editCouponInstance.do',"get","","json","instanceCode="+instanceCode,function(data) {
		if(data.status != null){
			$.messager.alert("提示",data.result);
			if(data.status == "1"){
				$("#couponName").text(data.coupon.couponName);
				$("#couponCode").text(data.couponInstance.couponInstanceCode);
				$("#expireDate").text(data.couponInstance.expireDate.substr(0,4)+"-"+data.couponInstance.expireDate.substr(4,2)+"-"+data.couponInstance.expireDate.substr(6,2));
				$("#couponTypeId").text($.Coupon_Type.getText(data.coupon.couponTypeId));
				$("#comments").text(data.coupon.comments);
				$("#hexiaoResult").css('display','block'); 
			}
		}else{
			$.messager.alert("提示","券码错误,请重新输入");
		}
	}, function() {
		parent.layer.alert("出错了:(");
	});
	
	
	/*$.ajax({
		type : "get",
		url : $.baseUrl+'/couponIssueController/editCouponInstance.do',
		data : "instanceCode="+instanceCode,
		dataType : "json",
		async: false,
		success : function(data) {
			if(data.status != null){
				$.messager.alert("提示",data.result);
				if(data.status == "1"){
					$("#couponName").text(data.coupon.couponName);
					$("#couponCode").text(data.couponInstance.couponInstanceCode);
					$("#expireDate").text(data.couponInstance.expireDate.substr(0,4)+"-"+data.couponInstance.expireDate.substr(4,2)+"-"+data.couponInstance.expireDate.substr(6,2));
					$("#couponTypeId").text($.Coupon_Type.getText(data.coupon.couponTypeId));
					$("#comments").text(data.coupon.comments);
					$("#hexiaoResult").css('display','block'); 
				}
			}else{
				$.messager.alert("提示","券码错误,请重新输入");
			}
		},
		error : function() {
			parent.layer.alert("出错了:(");
		}
	});*/
}
//清空图片
function clearImg(page){
	$("#"+page+"_image").val("");
	$("#"+page+"_img3").val("");
	$("#"+page+"_img3_path").attr("src","");
	$("#"+page+"_fileSpan").text("未选择文件...");
}
//上传大图片
function uploadcouponimage(page){
//	var loading = '';
//	loading = parent.layer.load('正在导入，请稍侯...');
	$("#dataLoad").show();
	$('#'+page+'_form').form('submit', {    
	    url : $.baseUrl+'/couponController/uploadCouponImage.do',   //加loading 
	    success:function(result){
	    	$("#dataLoad").hide();
//	    	parent.layer.close(loadi);
	    	result = eval('('+result+')');
	    	if(result=="不被允许上传的文件格式!"){
	    		$.messager.alert("提示","不被允许上传的文件格式!");
	    	}else{
	    		$("#"+page+"_img3").val($.rootPath+result);
	    		$("#"+page+"_img3_path").attr("src",$.rootPath+result);
	    		$.messager.alert("提示","上传成功");
	    	}
	    }    
	}); 
}
//上传小图片
function uploadcouponimage2(page){
//	var loading = '';
//	loading = parent.layer.load('正在导入，请稍侯...');
	$("#dataLoad").show();
	$('#'+page+'_form').form('submit', {    
	    url : $.baseUrl+'/couponController/uploadCouponMinImage.do',   //加loading 
	    success:function(result){
	    	$("#dataLoad").hide();
//	    	parent.layer.close(loadi);
	    	result = eval('('+result+')');
	    	if(result=="不被允许上传的文件格式!"){
	    		$.messager.alert("提示","不被允许上传的文件格式!");
	    	}else{
	    		$("#"+page+"_img2").val($.rootPath+result);
	    		$("#"+page+"_img2_path").attr("src",$.rootPath+result);
	    		$.messager.alert("提示","上传成功");
	    	}
	    }    
	}); 
}
//DateGrid Date时间格式化
function useDateformat(val) {
	if(val==undefined || val ==''){
		return "领取后生效";
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}

function useDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return "领取后生效";
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}
function dateDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}
//DateGrid Time时间格式化
function timeDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + ' ' +val.substring(8, 10)+':'+val.substring(10,12)+':'+val.substring(12,14) ;
	}
}

//Date时间格式化
function dateDateformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}

function dateTimeformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + " " + val.substring(8, 10) + ":" + val.substring(10, 12) + ":" + val.substring(12, 14);
	}
}
//获取当前时间(yyyymmdd)
function getDate(){
	var date = new Date();
	var str = '';
	if(date.getMonth()+1<10){
		str = date.getFullYear()+"0"+(date.getMonth()+1);
		
	}else{
		str = date.getFullYear()+""+(date.getMonth()+1);
		
	}
	if(date.getDate()<10){
		str += "0"+date.getDate();
	}else{
		str += date.getDate();
	}
	return str;
}