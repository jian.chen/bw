var orderData;
define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initDataGrid();
		
		$("#daochuOR").click(function(){
			location.href = $.baseUrl + '/orderReport/leadToExcelOrderRegion?startDate='+$("#startDate").val()+'&endDate='+$("#endDate").val();
		})
		
		$("#searchMC").click(function(){
			var ck = formCheck($("#memberChannelForm"));
			if(ck){
				initDataGrid();
			}else{
				alert("请输入合法数据！");
			}
		});
		
	}
	
	return {
		init: init
	}
	
});

function initDataGrid(){
	$.ajax({
        method : 'GET',
        url : $.baseUrl+'/orderReport/showOrderList',
        data: $('#memberChannelForm').serialize(),
        async : false,
        dataType : 'json',
        success : function(data) {
        	orderData = data.obj;
        	var dataArr = data.rows
        	var arr = new Array();
            for ( var machine =0; machine<data.rows.length;machine++) {
                arr[machine] = {
                    'orgName' : dataArr[machine].orgName,
                    'totalSales' : dataArr[machine].totalSales,
                    'withTotalSales' : withtotalSales(dataArr[machine].withTotalSales),
                    'totalConsumption' : dataArr[machine].totalConsumption,
                    'withTotalConsumption' : withtotalConsumption(dataArr[machine].withTotalConsumption),
                    'totalConsumer' : dataArr[machine].totalConsumer,
                    'withTotalConsumer' : withtotalConsumer(dataArr[machine].withTotalConsumer)
                };
            }
            $('#orderList').datagrid('loadData', arr);
        },
        error : function() {
            alert('error');
        }
    });
}

function withtotalSales(val){
	if(val==undefined || val ==''){
	return '';
	}else{
		return (Math.round(val / orderData.totalSales * 10000) / 100.00 + "%");
	}
}
function withtotalConsumption(val){
	if(val==undefined || val ==''){
		return '';
	}else{
		return (Math.round(val / orderData.totalConsumption * 10000) / 100.00 + "%");
	}
}
function withtotalConsumer(val){
	if(val==undefined || val ==''){
		return '';
	}else{
		return (Math.round(val / orderData.totalConsumer * 10000) / 100.00 + "%");
	}
}