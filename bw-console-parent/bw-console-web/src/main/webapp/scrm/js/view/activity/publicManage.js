var backUrl = 'activity/question.html';
var clickPoints;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		loadThisMask(backUrl);
		$('#yqhd_tab li').click(function(){
			var url = $(this).data('url');
			$(this).addClass('active').siblings().removeClass('active');
			loadThisMask(url);
		});
		if(clickPoints == 'question' || clickPoints == null){
			$("#clickQuestion").addClass('active');
			$("#clickVote").removeClass('active');
		}
		if(clickPoints == 'vote'){
			$("#clickVote").addClass('active');
			$("#clickQuestion").removeClass('active');
		}
	};

	function loadThisMask(url) {
		$('#yqhdPanel').show().panel({
			href: url
		});
	};
	return {
		init: init
	}
});
