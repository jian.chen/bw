define(['common','citySelect'], function(common,citySelect) {
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		initSelectDetailed();
		initDate();
		initInput();
		addTagIdArrEdit = [];
		//存储勾选的标签name
		addTagNameArrEdit = [];

		//使用区域选择
		new citySelect.initCity({input:'city_qr_member',inputType:true});
		
		//绑定切换
		$('#userDetailedTabBtnList').bindTab('#userDetailedTabContent');
		
		$("#languageSelect").change(function(){
			$("#language").val($(this).children('option:selected').val());
		})
		
		//标签切换查询
		 $("#userDetailedTabBtnList li").each(function() {
             $(this).click(function() {
                 var title = $(this).text();
                 if(title.trim()=='基本信息'){
                 	queryMemberBaseInfo();
                 }else if(title.trim()=='联系信息'){
                 	queryMemberContantInfo();
                 }else if(title.trim()=='标签分组'){
                 	queryMemberTagAndGroup();
                 }else if(title.trim()=='其他信息'){
                 	queryMemberExtInfo();
                 }else if(title.trim()=='积分历史'){
                 	queryMemberPoints();
                 }else if(title.trim()=='消费记录'){
                 	queryMemberOrderHeader();
                 }else if(title.trim()=='优惠券'){
                 	queryMemberCouponInstance();
                 }else if(title.trim()=='互动历史'){
                 	queryMemberWechatMessage();
                 }
             });
		});

		
		$('#interactionTabList').bindTab('#interactionTabContent');

		//标签弹出层
		$('#labelChooseBtn2').click(function() {
			$('#labelChooseEdit').show();
			appendTagEdit();
			queryTagEdit($('#likeMemberTagName2').val());
		});
		$('#queryTag_bt2').click(function() {
			queryTagEdit($('#likeMemberTagName2').val());
		});
		$('#tagSaveBtn2').click(function() {
			//关闭弹出层
			$('#labelChooseEdit').find(".detailed_close").click();
			addSelectTagEdit();
		});

		//分组弹出层
		$('#addFenZuBtn').click(function() {
			$('#addFenZu').show();
			queryGroup();
		});
		//分组弹出层保存
		$('#groupSaveBtn').click(function() {
			//关闭弹出层
			$('#addFenZu').find(".detailed_close2").click();
			addSelectGroupToArray();
			addSelectGroup();
		});
		
		
		//查询会员积分记录
		$('#pointsSearchBtn').click(function() {
			$('#memberPointsTable').datagrid('load', serializeObject($('#pointsSearchForm')));
		});
		
		//查询会员消费记录
		$('#memberOrderHeaderBtn').click(function() {
			$('#memberOrderHeaderTable').datagrid('load', serializeObject($('#memberOrderHeaderForm')));
		});
		
		//查询会员优惠劵
		$('#memberCouponInstanceBtn').click(function() {
			$('#memberCouponInstanceTable').datagrid('load', serializeObject($('#memberCouponInstanceForm')));
		});
		
		//查询会员微信互动消息
		$('#memberWechatMessageBtn').click(function() {
			queryMemberWechatMessage();
		});
		
		//查询会员短信消息
		$('#memberSmsInstanceBtn').click(function() {
			queryMemberSmsInstance();
		});
		
		//基本信息保存
		$('#memberBaseSaveBtn').click(function() {
			/*
			if(!citySelect.inputsCitys['city_qr_member']){
				alert("请选择正确的城市");
				return;
			}
			*/
			if($("#channelName").val()==""){
				$("#channelName").alert("不能为空");
				return;
			}
			$("[name='geoId']").val(citySelect.inputsCitys['city_qr_member']);
			saveMemberBaseInfo();
		});
		
		//联系信息保存
		$('#saveMemberExtAccountBtn').click(function() {
			saveMemberExtAccount();
		});
  		
		//保存会员的标签和分组
		$('#saveMemberTagAndGroupBtn').click(function() {
			saveMemberTagAndGroup();
		});
		
		//保存会员的其他信息
		$('#memberExtInfoSaveBtn').click(function() {
			saveMemberExtInfo();
		});
		
		
		//添加联系地址弹出层
		$('#memberAddressAddBtn').click(function() {
			$('#contactDetailed').show();
			initAddressData("provinceName","20001");
			//表单清空
			clearForm($('#memberAddressForm'));
		});
		//编辑地址
		$('#memberAddressEditBtn').click(function() {
			var selected  = $('#memberAddressTable').datagrid('getSelected');
			var memberAddressId;
			if(selected){
				memberAddressId = selected.memberAddressId;
			}else{
				alert("请选择地址");
				return;
			}
			$('#contactDetailed').show();
			initAddressData("provinceName","20001");
			//查询详情页面赋值
			queryMemberAddressDetail(memberAddressId);
		});
		//删除地址
		$('#memberAddressDeleteBtn').click(function() {
			var selected  = $('#memberAddressTable').datagrid('getSelected');
			var memberAddressId;
			if(selected){
				memberAddressId = selected.memberAddressId;
			}else{
				alert("请选择地址");
				return;
			}
			//查询详情页面赋值
			deleteMemberAddress(memberAddressId);
		});
		
		
		//保存会员联系地址
		$('#memberAddressSaveBtn').click(function() {
			saveMemberAddress();
		});
		
		//默认地址勾选框勾选赋值
		$('#isDefaultCheckbox').click(function() {
			if($(this).prop("checked")){
				$("[name='isDefault']").val("1");
			}else{
				$("[name='isDefault']").val("0");
			}
		});
		
		//区域门店弹框
		$('#orgName').click(function() {
			$('#areaChooseEdit').show();
			initTreeData("areaChooseEdit","areaTreeEdit","/orgController/testTree.do","2");
			selectOrg($("[name='orgId']").val(),"areaTreeEdit");
		});
		//区域门店保存
		$('#areaSaveBtnEdit').click(function() {
			var selectIds= getSelectId("areaTreeEdit","2");
			var selectNames= getSelectName("areaTreeEdit","2");
			$("[name='orgId']").val(selectIds);
			$("[name='orgName']").val(selectNames);
			$('#areaChooseEdit').find(".detailed_close2").click();
		});
		
		queryMemberBaseInfo();
	}
	return {
		init: init
	}
});

//查询会员基本信息
function queryMemberBaseInfo(){
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/showMemberDetail.do?memberId='+memberId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	data = data.obj;
        	if(data.memberPhoto){
        		$("#memberPhoto").attr('src',data.memberPhoto); 
        	}
        	
        	$("#lastLoginTime").text(getDateString(data.lastLoginTime));
        	$("#channelId").text($.Member_Channel.getText(data.channelId));
        	$("#orgId").text(data.orgName==null?"":data.orgName);
        	$("#registerTime").text(getDateString(data.registerTime));
        	$("#registerIp").text(data.registerIp==null?"":data.registerIp);
        	
        	$("[name='memberId']").val(data.memberId);
        	$("[name='memberCode']").val(data.memberCode);
        	
        	$("[name='channelId']").val(data.channelId);
        	$("[name='channelId']").next().val($.Member_Channel.getText(data.channelId));
        	
        	$("[name='orgId']").val(data.orgId);
        	$("[name='orgName']").val(data.orgName);
        	
        	$("[name='memberName']").val(data.memberName);
        	$("[name='userName']").val(data.userName);
        	
        	$("[name='gender']").val(data.gender);
        	$("[name='gender']").next().val($.gender.getText(data.gender));
        	
        	$("[name='mobile']").val(data.mobile);
        	$("[name='birthday']").val(getDateString(data.birthday));
        	$("[name='cardNo']").val(data.cardNo);
        	
        	$("[name='gradeId']").val(data.gradeId);
        	$("[name='gradeId']").next().val($.Member_Grade.getText(data.gradeId));
        	
        	$("[name='geoId']").val(data.geoId);
        	$("[name='geoName']").val(data.geoName);
        	$("[name='email']").val(data.email);
        	
        	$("[name='language']").val(data.language);
        	$("#languageSelect").find("option[value='" + data.language + "']").attr("selected",true);
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
    initGradeConvert();
}

//保存会员基本信息
function saveMemberBaseInfo(){
	//$.messager.progress();
	$('#memberBaseInfoForm').form('submit', {    
    url:$.baseUrl+'/memberController/editMember.do?memberId='+memberId,
    onSubmit: function(){
    	var isValid = formCheck($(this));
    	if(isValid){
    		$("#dataLoad").show();
    	}
		return isValid;
    },    
    success:function(data){  
    	$("#dataLoad").hide();
    	var result = $.parseJSON(data);
    	$('#huiyuandingdanTable').datagrid('reload');
    	alert(result.message);
    	if(result.status==200){
    		$(".detailed_close").closest('.mask, .mask_in').hide();
    	}
    	
    }    
}); 
}

//保存会员联系信息
function saveMemberExtAccount(){
	$('#memberExtAccountForm').form('submit', {    
    url:$.baseUrl+'/memberController/saveMemberExtAccount.do?memberId='+memberId,
    method : "post",
    onSubmit: function(){
    	var isValid = formCheck($(this));
    	if(isValid){
    		$("#dataLoad").show();
    	}
		return isValid;
    },    
    success:function(data){  
    	$("#dataLoad").hide();
    	var result = $.parseJSON(data);
    	$('#huiyuandingdanTable').datagrid('reload');
    	alert(result.message);
    	if(result.status==200){
    		$(".detailed_close").closest('.mask, .mask_in').hide();
    	}
    }    
}); 
}

//保存会员标签和分组
function saveMemberTagAndGroup(){
	$("#dataLoad").show();
	 $.ajax({ 
        type: "post", 
        url: $.baseUrl+'/memberController/saveMemberTagAndGroup.do?memberId='+memberId, 
        dataType: "json",
        data : {
        	memberTagIds: addTagIdArrEdit.toString(), 
        	memberGroupIds: addGroupIdArr.toString()
        },
        async: false,
        success: function (data) {
        	$("#dataLoad").hide();
		    //var result = $.parseJSON(data);
    		alert(data.message);
    		if(result.status==200){
    			$(".detailed_close").closest('.mask, .mask_in').hide();
    		}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        }
    });
}

//保存会员其他信息
function saveMemberExtInfo(){
	$('#saveMemberExtInfoForm').form('submit', {    
    url:$.baseUrl+'/memberController/saveMemberExtInfo.do?memberId='+memberId,
    method : "post",
    onSubmit: function(){
    	var isValid = formCheck($(this));
    	if(isValid){
    		$("#dataLoad").show();
    	}
		return isValid;
    },    
    success:function(data){  
    	$("#dataLoad").hide();
    	var result = $.parseJSON(data);
    	alert(result.message);
    	if(result.status==200){
    		$(".detailed_close").closest('.mask, .mask_in').hide();
    	}
    }    
}); 
}


//保存会员联系地址
function saveMemberAddress(){
	if($("#provinceName").val()==""){
		$("#provinceName").alert("不能为空");
		return;
	}
	if($("#cityName").val()==""){
		$("#cityName").alert("不能为空");
		return;
	}
	if($("#districtName").val()==""){
		$("#districtName").alert("不能为空");
		return;
	}
	$('#memberAddressForm').form('submit', {    
    url:$.baseUrl+'/memberController/saveMemberAddress.do?memberId='+memberId,
    method : "post",
    onSubmit: function(){
    	var isValid = formCheck($(this));
    	if(isValid){
    		$("#dataLoad").show();
    	}
		return isValid;
    },    
    success:function(data){  
    	$("#dataLoad").hide();
    	var result = $.parseJSON(data);
    	if(result.status=='200'){
    		$('#contactDetailed').find(".detailed_close2").click();
    		$('#memberAddressTable').datagrid('load',{});
    	}else{
    		alert(result.message);
    	}
    }    
}); 
}

//查询地址详情
function queryMemberAddressDetail(memberAddressId){
	
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/showMemberAddressDetail.do?memberAddressId='+memberAddressId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("[name='memberAddressId']").val(data.memberAddressId);
        	$("[name='personName']").val(data.personName);
        	$("[name='provinceId']").val(data.provinceId);
        	$("#provinceName").val(data.provinceName);
        	 $("[name='cityId']").val(data.cityId);
        	$("#cityName").val(data.cityName);
        	$("[name='districtId']").val(data.districtId);
        	$("#districtName").val(data.districtName);
        	$("[name='addressDetail']").val(data.addressDetail);
        	$("[name='personMobile']").val(data.personMobile);
        	$("[name='personPhoneZone']").val(data.personPhoneZone);
        	$("[name='personPhoneNum']").val(data.personPhoneNum);
        	$("[name='isDefault']").val(data.isDefault);
        	
        	if(data.isDefault==1){
        		$("#isDefaultCheckbox").prop("checked",true); 
        	}else{
        		$("#isDefaultCheckbox").prop("checked",false); 
        	}
        	
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

function deleteMemberAddress(memberAddressId){
	$("#dataLoad").show();
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/removeMemberAddress.do?memberAddressId='+memberAddressId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$("#dataLoad").hide();
	        if(data.status=='200'){
	    		$('#memberAddressTable').datagrid('load', {});
	    	}else{
	    		alert(data.message);
	    	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

//初始化会员等级变动记录
function initGradeConvert(){
	$('#memberGradeConvertTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberGradeConvert.do?memberId='+memberId, 
		singleSelect : true,
		onLoadSuccess : function(result) {
			if(result) {
				
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询会员联系信息
function queryMemberContantInfo(){
    $('#memberAddressTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberContactInfo.do?memberId='+memberId, 
		singleSelect : true,
		onLoadSuccess : function(result) {
			if(result) {
				$("[name='wechat']").val(result.memberContact.wechat);
        		$("[name='qq']").val(result.memberContact.qq);
        		$("[name='sina']").val(result.memberContact.sina);
        		//$("[name='msn']").val(result.memberContact.msn);
            }
		},
		onLoadError : function() {
			
		}
	});
}


//查询会员标签和分组
function queryMemberTagAndGroup(){
	addTagIdArrEdit = [];
	addTagNameArrEdit = [];
	addGroupIdArr=[];
	addGroupNameArr=[];
    $.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/showMemberTagAndGroup.do?memberId='+memberId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	$.each(data.tagList, function(index, value){
        		/*
		      	$("#memberTagList").append("<span class='labelChoose_span'>"+value.memberTagId+"<span class='icon icon_close_active' onclick='delAddTag(this)' >" 
		      	+"<input type='hidden' value='"+value.memberTagId+"' /><input type='hidden' value='"+value.memberTagName+"' />" +"</span></span>");
		      	*/
        		
    			if(!addTagIdArrEdit.in_array(value.memberTagId)){
					addTagIdArrEdit.push(value.memberTagId);
					addTagNameArrEdit.push(value.memberTagName);
				}
				/*if(!addTagNameArrEdit.in_array(value.memberTagName)){
					addTagNameArrEdit.push(value.memberTagName);
				}*/
		    });
		    
		    $.each(data.groupList, function(index, value){
        		/*
		      	$("#memberTagList").append("<span class='labelChoose_span'>"+value.memberTagId+"<span class='icon icon_close_active' onclick='delAddTag(this)' >" 
		      	+"<input type='hidden' value='"+value.memberTagId+"' /><input type='hidden' value='"+value.memberTagName+"' />" +"</span></span>");
		      	*/
    			if(!addGroupIdArr.in_array(value.memberGroupId)){
					addGroupIdArr.push(value.memberGroupId);
					addGroupNameArr.push(value.memberGroupName);
				}
				/*if(!addGroupNameArr.in_array(value.memberGroupName)){
					addGroupNameArr.push(value.memberGroupName);
				}*/
		    });
		    
		    addSelectTagEdit();
		    addSelectGroup();
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        }
    });
}

//查询会员其他信息
function queryMemberExtInfo(){
	$.ajax({ 
        type: "get", 
        url: $.baseUrl+'/memberController/showMemberExtInfo.do?memberId='+memberId, 
        dataType: "json",
        async: false,
        success: function (data) {
        	
        	$("[name='profession']").val(data.profession);
        	$("[name='profession']").next().val($.Profession.getText(data.profession));
        	$("[name='zodiac']").val(data.zodiac);
        	$("[name='zodiac']").next().val($.Member_Zodiac.getText(data.zodiac));
        	
        	$("[name='chineseZodiac']").val(data.chineseZodiac);
        	$("[name='chineseZodiac']").next().val($.Member_Chinese_Zodiac.getText(data.chineseZodiac));
        	$("[name='blood']").val(data.blood);
        	$("[name='blood']").next().val($.Member_Blood.getText(data.blood));
        	
        	$("[name='isMarried']").val(data.isMarried);
        	$("[name='isMarried']").next().val($.IsMarried.getText(data.isMarried));
        	
        	$("[name='remark']").val(data.remark);
        	
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

//查询会员积分历史
function queryMemberPoints(){
     $('#memberPointsTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberPoints.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		queryParams : serializeObject($('#pointsSearchForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询会员消费记录
function queryMemberOrderHeader(){
     $('#memberOrderHeaderTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberOrderHeader.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		method : 'post',
		queryParams : serializeObject($('#memberOrderHeaderForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询会员优惠劵
function queryMemberCouponInstance(){
     $('#memberCouponInstanceTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberCouponInstance.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		method : 'post',
		queryParams : serializeObject($('#memberCouponInstanceForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询微信互动消息
function queryMemberWechatMessage(){
     $('#memberWechatMessageTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberWechatMessage.do?memberId='+memberId, 
		singleSelect : true,
		rownumbers:true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		method : 'get',
		//queryParams : '',
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

//查询短信消息
function queryMemberSmsInstance(){
     $('#memberSmsInstanceTable').datagrid( {
		url: $.baseUrl+'/memberController/showMemberSmsInstance.do?memberId='+memberId, 
		singleSelect : true,
		pagination : true,
		pageNumber : 1,
		pageSize : 10,
		pageList : [10,20,30,40,50],
		rownumbers:true,
		method : 'get',
		//queryParams : '',
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}


/*
 * 标签js开始
 */

//存储勾选的标签id
var addTagIdArrEdit = new Array();
//存储勾选的标签name
var addTagNameArrEdit = new Array();
//将标签添加到已选标签
function addTagEdit(obj){
	var id = $(obj).val();
	var name = $(obj).parent().text();
	if($(obj).prop("checked")){
		if(!addTagIdArrEdit.in_array(id)){
			addTagIdArrEdit.push(id);
			addTagNameArrEdit.push(name);
		}
		/*if(!addTagNameArrEdit.in_array(name)){
			addTagNameArrEdit.push(name);
		}*/
		
	}else{
		addTagIdArrEdit.remove(id);
		addTagNameArrEdit.remove(name);
	}
	appendTagEdit();
	
}
//删除已选标签
function delAddTagEdit(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArrEdit.remove(id);
	addTagNameArrEdit.remove(name);
	appendTagEdit();
}

//查询标签
function queryTagEdit(likeMemberTagName){
	$.ajax({ 
            type: "post", 
            url: $.baseUrl+'/memberTagController/showMemberTagList.do?likeMemberTagName='+likeMemberTagName, 
            dataType: "json",
            async: false,
            success: function (data) {
            	$("#labelChooseEdit").find(".labelChoose_list ul").html("");
            	
            	$.each(data.rows, function(index, value){
			      $("#labelChooseEdit").find(".labelChoose_list ul").append("<li><input type='checkbox' onchange='javascript:addTagEdit(this);' name='' value='"+value.memberTagId+"' />" + value.memberTagName+ "</li>");
			    });
            }, 
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    alert(errorThrown); 
            } 
        });
}


function appendTagEdit(){
	$("#labelChooseEdit").find(".labelChoose_title").html("<span>已选标签:</span>");
	$.each(addTagIdArrEdit, function(index, value){
      	$("#labelChooseEdit").find(".labelChoose_title").append("<span class='labelChoose_span'>"+addTagNameArrEdit[index]+
      		"<span class='icon icon_close_active' onclick='delAddTagEdit(this)' >" +
      		"<input type='hidden' value='"+addTagIdArrEdit[index]+"' /><input type='hidden' value='"+addTagNameArrEdit[index]+"' />" +
      				"</span></span>");
    });
}

//页面添加选择的标签
function addSelectTagEdit(){
	$("#memberTagList").html("");
	$.each(addTagIdArrEdit, function(index, value){
	  	$("#memberTagList").append("<span class='labelChoose_span'>"+addTagNameArrEdit[index]+
	  		"<span class='icon icon_close_active' onclick='delSelectTagEdit(this)' >" +
	  		"<input type='hidden' value='"+addTagIdArrEdit[index]+"' /><input type='hidden' value='"+addTagNameArrEdit[index]+"' />" +
	  				"</span></span>");
	});
}

//删除已选标签
function delSelectTagEdit(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArrEdit.remove(id);
	addTagNameArrEdit.remove(name);
	addSelectTagEdit();
}
/*
 * 标签js结束
 */



/*
 * 分组js开始
 */

//存储勾选的分组id
var addGroupIdArr = new Array();
//存储勾选的分组name
var addGroupNameArr = new Array();

function addSelectGroupToArray(){
	var id = $("#selectMemberGroupId").val();
	var name = $("#selectMemberGroupName").val();
	if(id!='' && !addGroupIdArr.in_array(id)){
		addGroupIdArr.push(id);
	}
	if(name!='' && !addGroupNameArr.in_array(name)){
		addGroupNameArr.push(name);
	}
}

//页面添加选择的分组
function addSelectGroup(){
	$("#memberGroupList").html("");
	$.each(addGroupIdArr, function(index, value){
	  	$("#memberGroupList").append("<span class='labelChoose_span'>"+addGroupNameArr[index]+
	  		"<span class='icon icon_close_active' onclick='delSelectGroup(this)' >" +
	  		"<input type='hidden' value='"+addGroupIdArr[index]+"' /><input type='hidden' value='"+addGroupNameArr[index]+"' />" +
	  				"</span></span>");
	});
}

//删除已选分组
function delSelectGroup(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addGroupIdArr.remove(id);
	addGroupNameArr.remove(name);
	addSelectGroup();
}


//查询分组
function queryGroup(){
	$.ajax({ 
            type: "get", 
            url: $.baseUrl+'/memberGroupController/showMemberGroupListSelect.do', 
            dataType: "json",
            async: false,
            success: function (data) {
            	$("#addFenZu").find(".dropdown-menu").html("");
            	$.each(data, function(index, value){
			      $("#addFenZu").find(".dropdown-menu").append("<li data-id='"+value.memberGroupId+"'><a href='javascript:;'>"+value.memberGroupName+"</a></li>");
			    });
            }, 
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    alert(errorThrown); 
            } 
        });
}
/*
 * 分组js结束
 */

