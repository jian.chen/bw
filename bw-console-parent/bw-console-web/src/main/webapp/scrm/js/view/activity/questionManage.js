define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		$("#goback").click(function(){
			backUrl = 'activity/question.html';
			clickPoints = 'question';
			common.Page.loadCenter('activity/publicManage.html');
		});
		
		$("#addTopic").click(function(){
			if(manageTemplateCount >= 10){
				alert("最多只能创建10题！");
				return;
			}
			common.Page.loadMask('activity/addTopic.html');
		});
		
		
		//初始化表格
		initQuestionDataGrid();
		
		
	}
	
	function initQuestionDataGrid(){
		$('#questionTemplateTable').datagrid({
			url : $.baseUrl+'/QuestionController/showTemplate',
			queryParams:{
				questionnaireId:manageQuestionId
				},
			idField : 'questionnaireTemplateId',
			onLoadSuccess : function(result) {
				
			},
			onLoadError : function() {
				
			}
		})
	}
	
	window.editManage = function(id){
		selectTemplateId = id;
		isAuth('/activity/editTopic.html',function(){
			$('#maskPanel').show().panel({
				href: 'activity/editTopic.html'
			});
		})
	}
	
	window.delManage = function(id){
		if(confirm("确定要删除吗？")){
			$.ajax({
				type : "post",
				url : $.baseUrl + "/QuestionController/delTemplete",
				dataType : "json",
				data : {
					questionnaireTemplateId : id
				},success : function(data){
					alert(data.message);
					$('#questionTemplateTable').datagrid('reload');
				}
			})
		}
	}
	
	window.typeTrans = function(value,row,index){
		return $.topicType.getText(value);
	}
	
	window.rowformater_question = function (value,row,index){
		var s = '<button onclick="editManage('+row.questionnaireTemplateId+')" type="button" class="solid-btn">编辑</button>';
		s = s + '<button onclick="delManage('+row.questionnaireTemplateId+')" type="button" class="border-btn">删除</button>';
		return s;
	}
	
	return {
		init: init
	}
	
});

var selectTemplateId;