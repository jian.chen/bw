define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initDataGrid();
		
		//保存
		$("#saveSysCode").click(function(){
			if(checkForm('sysCode')){
				$.ajax({ 
			        type: "post", 
			        url: $.baseUrl+'/sysSetController/sysCodeSave', 
			        dataType: "json",
			        data : {
			        	couponCodeLength: $("#couponCodeLength").combobox('getValue'), 
			        	cardPrefix: $("#cardPrefix").val(),
			        	cardCodeLength : $("#cardCodeLength").combobox('getValue')
			        },
			        success: function (data) {
			        	if(data.status == '200'){
			        		$.messager.alert("提示","保存成功");
			        	}else{
			        		$.messager.alert("提示","保存失败");
			        	}
			        }
			    });
			}
		})
		
		
		
	}
	
	//检查表单
	function checkForm(page){
		var isValid = formCheck($('#'+page+'_form'));
		return isValid;
	}
	
	function initDataGrid(){
		$.ajax({ 
	        type: "post", 
	        url: $.baseUrl+'/sysSetController/showSysCode', 
	        dataType: "json",
	        success: function (data) {
	        	$("#couponExample").text(Math.pow(10,data.obj.couponSeqType.codeLength-1));
	        	$("#cardExample").text(data.obj.cardSeqType.codeSeqTypePrefix +""+Math.pow(10,data.obj.cardSeqType.codeLength-1));
	        	
	        	$("#couponCodeLength").combobox('setValue',data.obj.couponSeqType.codeLength);
	        	$("#cardPrefix").val(data.obj.cardSeqType.codeSeqTypePrefix);
	        	$("#cardCodeLength").combobox('setValue',data.obj.cardSeqType.codeLength);
	        }
	    });
	}
	
	return {
		init: init
	}
});
