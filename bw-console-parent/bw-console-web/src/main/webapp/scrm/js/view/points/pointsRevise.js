define(['common'], function(common) {
	
	function init() {
		common.Page.resetMain();
		initInput();
		//初始化下拉框;
		initSelect("pointsTypeIdSelect");
		
		$('#seachTask').click(function() {
			var equalMemberCode = $("#equalMemberCode").val();
			var likeMemberName = $("#likeMemberName").val();
			var equalMobile = $("#equalMobile").val();
			if(!equalMemberCode && !likeMemberName && !equalMobile){
				alert("请输入查询条件");
				//$('#memberInfoTable').datagrid('reload');
			}else{
				initMemberDataGridPointsRevise();												
			}
		});
		
		//有效期优化
		$("input:checkbox[name='never_Invalid']").change(function (){
			if(this.checked==true){
				$("#invalidDate").val("");
				$("#invalidDate").attr("disabled","true");
			}else{
				$("#invalidDate").removeAttr("disabled");
			}
		});	
		
	};
	
	
	return {
		init: init
	};
});

function initMemberDataGridPointsRevise(){
	//初始化table
	$('#memberInfoTable').datagrid( {
		url : $.baseUrl+'/pointsItemController/showMemberInfo.do',
		queryParams : serializeObject($('#memberSearchForm')),
		onLoadSuccess : function(result) {
			if(result) {
            }
		},
		onLoadError : function() {
			
		}
	});
}

function pointsReviseAdd(memberCode,memberId,pointsBalance,totalPoints){
	//调账前先清空页面信息
	$("#itemPointsNumber").val("");
	$("#invalidDate").val("");
	$("#pointsReason").val("");
	$("#pointsTypeId").val("");
	$("#pointsTypeText").val("");
	$("#never_Invalid").removeAttr("checked");
	
	$("#memberId").val(memberId);
	$("#memberCode").val(memberCode);
	$("#points_Detail").show();
}

//检查表单
function checkForm(page){
	var isValid = formCheck($('#'+page+'_form'));
	return isValid;
}

function doSave(page){
   
	if($("#itemPointsNumber").val() == ""){
		alert("请填写调账数量");
		return;
	}
	
	if($("#itemPointsNumber").val() == "0"){
		alert("积分调账数值不能为0");
		return;
	}
	
	if($("#pointsTypeId").val() == ""){
		alert("请选择积分类型");
		return;
	}
	
	var number = $("#itemPointsNumber").val();
	if(number > 0){
       if($("#invalidDate").val() == "" && $("input:checkbox[name='never_Invalid']:checked").val() != "Y" ){
    	   alert("请选择过期时间");
    	   return;
       }
	}
	
	if($("#pointsReason").val() == ""){
		 alert("请填写调账原因");
  	      return;
	}
	
   if(checkForm(page)){
	   $('.mask, .mask_in').hide();
		$.ajax({
			type: "POST",
			url : $.baseUrl+'/pointsItemController/savePointsRevise.do',
			data:$('#save_form').serialize(),
			async: false,
			dataType : "json",
			success : function(result) {
				alert(result.message);
				$("#memberInfoTable").datagrid('load');
			},
			error : function() {
				parent.layer.alert("出错了:(");
			}
		});
  }
}

function validateNumber(){
	var number = $("#itemPointsNumber").val();
	if(number > 0){
		$("#invalidDate").removeAttr("disabled");
		$("#never_Invalid").removeAttr("disabled");
	}
    if(number < 0){
    	$("#invalidDate").val("");
    	$("#invalidDate").attr("disabled","true");
    	$("#never_Invalid").attr("disabled","true");
	}
}
