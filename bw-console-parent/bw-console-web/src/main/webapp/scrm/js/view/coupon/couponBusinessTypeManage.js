var couponIds;
define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		$('#addBusinessType').click(function() {
			isAuth("/couponBusinessTypeController/insertCouponBusinessType.do",function(){
				common.Page.loadMask('coupon/couponBusinessTypeAdd.html');
			})
		});
		$('#edit').click(function(){
			if(checkSingle()){
				isAuth("/couponBusinessTypeController/editCouponBusinessType.do",function(){
					couponIds = getCheckedId();
					common.Page.loadMask('coupon/couponBusinessTypeEdit.html');
				});
			}
		});
		$('#searchDetail').click(function(){
			if(checkSingle()){
				couponIds = getCheckedId();
				isAuth("/coupon/couponBusinessTypeDetail.html",function(){
					common.Page.loadMask('coupon/couponBusinessTypeDetail.html');
				});
			}
		});
		initDataGrid();
	}
	//初始化数据
	function initDataGrid(){
		//初始化table
		$('#couponTable').datagrid( {
			url : $.baseUrl+'/couponBusinessTypeController/showBusinessTypeWithPage.do',
			onLoadSuccess : function(result) {
			},
			onLoadError : function() {
				
			}
		});
	}
	return {
		init: init
	}
});

//检查表单
function checkForm(page){
	var isValid = formCheck($('#'+page+'_form'));
	return isValid;
}
//检查是否单选
function checkSingle(){
	var selected = $('#couponTable').datagrid('getChecked');
	if(selected.length==1){
		return true;
	}else{
		alert("请勾选一条记录!");
		return false;
	}

}
//获取选择Id
function getCheckedId(){
	var selected = $('#couponTable').datagrid('getChecked');
	var couponIds='';
	for(var i=0; i<selected.length; i++){
	    if (couponIds != ''){ 
	    	couponIds += ',';
	    }
	    couponIds += selected[i].couponBusinessTypeId;
	}
//	alert(couponIds);
	return couponIds;
}

function removeCouponsPre(){
	if(checkSingle()){
//		if(checkStatus()){
			if(confirm("确定删除吗？")){
				removeCoupons();
			}
//		}else{
//			alert("不可删除已发放优惠券");
//		}
	}
}
//删除优惠券
function removeCoupons(){
		couponIds = getCheckedId();
		isAuthForAjax('/couponBusinessTypeController/removeCouponBusinessType.do',"get","","json","couponBusinessTypeId="+couponIds,function(result) {
			result = eval('('+result+')');
			if(result.code==1){
				alert(result.message);
				$('#couponTable').datagrid('reload'); 
				
			}else {
				alert(result.message+"!!!!!");
			}
			
		},function() {
			parent.layer.alert("出错了:(");
		});
		
		/*$.ajax({
			type : "get",
			url : $.baseUrl+'/couponBusinessTypeController/removeCouponBusinessType.do',
			data : "couponBusinessTypeId="+couponIds,
			dataType : "json",
			async: false,
			success : function(result) {
				result = eval('('+result+')');
				if(result.code==1){
					alert(result.message);
					$('#couponTable').datagrid('reload'); 
					
				}else {
					alert(result.message+"!!!!!");
				}
				
			},
			error : function() {
				parent.layer.alert("出错了:(");
			}
		});*/
}
//查询优惠券发布信息
function showCouponIssueDetail(){
	$('#couponManage_couponIssue_detail').datagrid( {
		url : $.baseUrl+'/couponIssueController/showCouponIssueList.do',
		queryParams : {
			couponId : couponIds
		},
		onLoadSuccess : function(result) {
		},
		onLoadError : function() {
			
		}
	});
}
//DateGrid Date时间格式化
function useDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return "领取后生效";
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}
function dateDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}
//DateGrid Time时间格式化
function timeDateformat(val,row,index) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + ' ' +val.substring(8, 10)+':'+val.substring(10,12)+':'+val.substring(12,14) ;
	}
}

//Date时间格式化
function dateDateformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8);
	}
}

function dateTimeformat(val) {
	if(val==undefined || val ==''){
		return ''
	}else{
		return val.substring(0, 4) + '-' + val.substring(4, 6) + '-' + val.substring(6, 8) + " " + val.substring(8, 10) + ":" + val.substring(10, 12) + ":" + val.substring(12, 14);
	}
}
//获取当前时间(yyyymmdd)
function getDate(){
	var date = new Date();
	var str = '';
	if(date.getMonth()+1<10){
		str = date.getFullYear()+"0"+(date.getMonth()+1);
		
	}else{
		str = date.getFullYear()+""+(date.getMonth()+1);
		
	}
	if(date.getDate()<10){
		str += "0"+date.getDate();
	}else{
		str += date.getDate();
	}
	return str;
}