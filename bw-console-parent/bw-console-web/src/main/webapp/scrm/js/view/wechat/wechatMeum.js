define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		
		$('.wechat_msg_top_row').delegate('[data-for]', 'click', function(){
			$($(this).data('for')).show().siblings().hide();
			$(this).addClass('active').siblings().removeClass('active');
		});
		
		$('#showSucai').click(function(){
			$('#suCai').show();
			initPackageIMageTextInfo();
		});
		
		$("#up").uploadPreview({ Img: "ImgPr", ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
		
		initMenu();
	};

	return {
		init: init
	}
});
var menuData;
var pareWechatMenuId = 0;
var subWechatMenuId = 0;
var menuName = "";
function initMenu(){
	 $.ajax({
        type: "get",
        url: $.baseUrl+'/wechatMenuController/showWechatMenu.do',
        dataType: "json",
        success: function(data){
        	generateMenu(data);
         }
    });
    
}

// 初始化图文列表
function initPackageIMageTextInfo(){
	 $.ajax({
        type: "post",
        url: $.baseUrl+'/wechatFansImageTextController/showImageTextList.do',
        async: true,
        dataType: "json",
        success: function(data){
        	var list =data.data;
			var trasferData = JSON.stringify(list);
			// list.length 是总的图文条数
			//$("#imageTextCount").html(list.length);
			$("#wechat_sucai_ul").html("");
			for(var i=0;i<list.length;i++){
			var imageTextHtml="<a href='javascript:void(0)'>" +
	    		"<li><div id='index"+i+"' onclick='checkImageText("+list[i][0].wechatMessageTemplateId+",&quot;" +list[i][0].mediaId+"&quot;," +i+")' class='wechat_sucai_add'><div id='fengmian"+i+"' class='fengmian'>" +
	    		"<div><img id='ImgPrFengmian"+i+"' style='width:50%;height: 50%'/></div><span>标题</span></div>" +
	    		"<div id='itemList"+i+"'></div></div></li></a>";
				
				$("#wechat_sucai_ul").append(imageTextHtml);
				for(var j=0;j<list[i].length;j++){
					// 如果是单条文
					if(j==0){
						var str="#ImgPrFengmian"+i;
						// 封面图片
						$(str).attr("src",list[i][j].pictureUrl);
						// 封面title
						var strFengmian="#fengmian"+i;
						$(strFengmian).children().eq(1).html(list[i][j].title);
					}else{
						var itemHtml="<div class='appmsgItem' ><span>"+list[i][j].title+"</span>" +
						"<div class='appmsgItem_img'><img style='width:100%;height:100%' src='"+list[i][j].pictureUrl+"'/></div></div>";
						var strItem="#itemList"+i;
						$(strItem).append(itemHtml);
					}
				}
			}
		 }
    });
	
}


//保存并发布
function saveAndPubMenu(){
	 $.ajax({
        type: "post",
        url: $.baseUrl+'/wechatMenuController/saveAndPubMenu.do',
        dataType: "json",
        success: function(data){
        	alert(data.message);
         }
    });
    
}

function generateMenu(data){
	menuData = data;
	 $("#menuUl").empty();
   	 var menulist = "";
    $.each(menuData.button, function(i, n) {
        menulist += '<li class="wechat_caidan_yiji"><div id="one_'+i+'" onclick="viewOneMenu(this,'+n.id+')">'+n.menu_name+
        	'<span class="wechat_caidan_icon2"  onclick="addTwoMenu(this,'+n.id+')"></span></div>';
		menulist += '<ul>';
		if(n.sub_button){
			$.each(n.sub_button, function(j, o) {
				menulist += '<li id="two_'+i+'_'+j+'" onclick="viewTwoMenu(this,'+o.id+')" class="active">'+o.menu_name+'</li>';
        	});
		}
        menulist += '</ul></li>';
    });   
    
    $("#menuUl").append(menulist);
}

function MenuButton(id,type,media_id,pid,menu_name,menu_key,sub_button){
	this.id = id;
	this.type = type;
	this.media_id = media_id;
	this.pid = pid;
	this.menu_name = menu_name;
	this.menu_key = menu_key;
	this.sub_button = sub_button;
}

//添加一级菜单弹层
function addOneMenu(obj){
	//判断一级菜单数量
	if(menuData.button&&menuData.button.length==3){
		return;
	}
	$("#firstAdd").find(":input").val("");
	$("#firstAdd").show();
}
//一级菜单添加
function saveAddOneMenu(obj){
	//判断一级菜单数量
	if(menuData.button.length==3){
		alert("最多支持3个菜单");
		return;
	}
	//alert(menuData.button.length);
	var menu_name =$(obj).parent().find(":input").val();
	//menuData处理
	var buttonArr = menuData.button;
	var menu_key=new Date().getTime();//当前时间戳
	var button = new MenuButton(null,"click",null,null,menu_name,menu_key,null);
	buttonArr.push(button);
	menu_name = encodeURI(encodeURI(menu_name));
	$.ajax({
        type: "post",
        url: $.baseUrl+'/wechatMenuController/saveMenu.do?menuName='+menu_name,
        dataType: "json",
        success: function(data){
        	if(data.status == 200){
        		//生成菜单树
        		generateMenu(menuData);
        		$('#firstAdd').find(".detailed_close").click();
        	}else{
        		alert(data.message);
        	}
         }
    });
}

//点击的是一级菜单的索引和二级菜单的索引
var indexOne;
var indexTwo;
//当前菜单的级别
var flag;
//添加二级菜单弹层
function addTwoMenu(obj,pId){
	pareWechatMenuId = pId;
	//点击添加按钮
	var id = $(obj).parent().attr("id");
	var arr = id.split("_");
	indexOne = arr[1];
	var subArr = menuData.button[indexOne].sub_button;
	if(subArr && subArr.length==5){
		return;
	}
	$("#secondAdd").find(":input").val("");
	$("#secondAdd").show();
}
function addTwoMenu2(){
	$("#secondAdd").find(":input").val("");
	$("#secondAdd").show();
}
//二级菜单添加
function saveAddTwoMenu(obj){
	var subArr = menuData.button[indexOne].sub_button;
	if(!subArr){
		subArr = new Array();
		menuData.button[indexOne].sub_button = subArr;
	}
	//判断二级菜单数量
	if(subArr.length==5){
		alert("最多支持5个菜单");
		return;
	}
	//alert(menuData.button.length);
	var menu_name =$(obj).parent().find(":input").val();
	menu_name= encodeURI(encodeURI(menu_name));
	//menuData处理
	var menu_key=new Date().getTime();//当前时间戳
	var button = new MenuButton(null,"click",null,null,menu_name,menu_key,null);
	$.ajax({
        type: "post",
        url: $.baseUrl+'/wechatMenuController/saveMenu.do?menuName='+menu_name+'&pareWechatMenuId='+pareWechatMenuId,
        dataType: "json",
        success: function(data){
        	if(data.status == 200){
        		//生成菜单树
        		generateMenu(menuData);
        		$('#secondAdd').find(".detailed_close").click();
        		alert(data.message);
        		subArr.push(button);
        	}else{
        		alert(data.message);
        	}
         }
    });
}


function viewOneMenu(obj,pId){
	flag = "1";
	pareWechatMenuId = pId;
	//获取一级菜单索引
	var id = $(obj).attr("id");
	var arr = id.split("_");
	indexOne = arr[1];
	
	//判断url和消息模板的值对应显示不同div
	var button = menuData.button[indexOne];
	var url = button.url;
	var tempId = button.tempId;
	var name = button.menu_name;
	
	if(!url && !tempId){//都没有
		$("#onePanel").show().siblings("div").hide();
		//菜单名字设置
		$("#onePanel").find(".wechat_caidan_right_title").find("span").html("一级菜单 ：<span>"+name+"</span>");
		//存在二级菜单，只能添加二级菜单
		var count = 0;
		var twoCount = button.sub_button;
		if(twoCount){
			count =twoCount.length;
		}
		if(count==0){
			$("#onePanelTitel").text('请设置“'+name+'”菜单的内容');
			$("#onePanel").find(".icon_addmenu").parent().show();
			$("#onePanel").find(".icon_addmsg").parent().show();
			$("#onePanel").find(".icon_addurl").parent().show();	
		}else if(count==5){
			$("#onePanelTitel").text("你已添加满5个二级菜单");
			$("#onePanel").find(".icon_addmenu").parent().hide();
			$("#onePanel").find(".icon_addmsg").parent().hide();
			$("#onePanel").find(".icon_addurl").parent().hide();	
		}else{
			$("#onePanelTitel").text('请设置“'+name+'”菜单的内容');
			$("#onePanel").find(".icon_addmenu").parent().show();
			$("#onePanel").find(".icon_addmsg").parent().hide();
			$("#onePanel").find(".icon_addurl").parent().hide();	
		}
		
	}else if(!url && tempId){//消息,查询消息模板
		//查询消息模板
		$.ajax({ 
	        type: "get", 
	        url: $.baseUrl+'/wechatFansController/showWechatMessageTemplateById.do?tempId='+tempId, 
	        dataType: "json",
	        success: function (data) {
	        	//设置是图片还是图文
	        	if(data.wechatMessageType=='news'){
	        		$("#viewDiv").find(":input").val("1");
	        		$("#viewDiv").append('<div class="wechat_sucai_add"><div id="" class="fengmian"><div><p>封面图片</p></div><span>标题</span></div></div>');
	        	}else if(data.wechatMessageType=='image'){
	        		$("#viewDiv").find(":input").val("2");
	        		$("#viewDiv").append('<img src="'+data.pictureUrl+'" />');
	        	}
	        	
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        } 
  	  });
		
		$("#viewPanel").show().siblings("div").hide();
		
	}else if(url && !tempId){//url，显示url
		$("#viewDiv").html("<span style='font-size: 14px'>"+url+"</span>");
		$("#viewText1").hide();
		$("#viewText2").show();
		$("#viewPanel").show().siblings("div").hide();
	}
	
}

function viewTwoMenu(obj,sId){
	flag = "2";
	subWechatMenuId = sId;
	$("#twoPanel").show().siblings("div").hide();
	
	//获取一级菜单索引
	var id = $(obj).attr("id");
	var arr = id.split("_");
	indexOne = arr[1];
	indexTwo = arr[2];
	//判断url和消息模板的值对应显示不同div
	var button = menuData.button[indexOne];
	var url = button.sub_button[indexTwo].url;
	var tempId = button.sub_button[indexTwo].tempId;
	var name = button.sub_button[indexTwo].menu_name;
	
	if(!url && !tempId){//都没有
		$("#twoPanelTitel").text('请设置“'+name+'”菜单的内容');
		$("#twoPanel").find(".wechat_caidan_right_title").find("span").html("二级菜单 ：<span>"+name+"</span>");
		$("#twoPanel").show().siblings("div").hide();
	}else if(!url && tempId){//消息,查询消息模板
		//查询消息模板
		
		$("#viewPanel").show().siblings("div").hide();
		
	}else if(url && !tempId){//url，显示url
		$("#viewDiv").html("<span style='font-size: 14px'>"+url+"</span>");
		$("#viewText1").hide();
		$("#viewText2").show();
		$("#viewPanel").show().siblings("div").hide();
	}
	
}

function setMessage(){
	var button = menuData.button[indexOne];
	if(flag==1){
		var name = button.menu_name;
		$("#setPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("一级菜单 ：<span>"+name+"</span>");
	}else if(flag==2){
		var name = button.sub_button[indexTwo].menu_name;
		$("#setPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("二级菜单 ：<span>"+name+"</span>");
	}
	$("#setPanel").show().siblings("div").hide();
}

function setUrl(){
	var button = menuData.button[indexOne];
	if(flag==1){
		var name = button.menu_name;
		$("#urlPanelTitle").html("一级菜单 ：<span>"+name+"</span>");
	}else if(flag==2){
		var name = button.sub_button[indexTwo].menu_name;
		$("#urlPanelTitle").html("二级菜单 ：<span>"+name+"</span>");
	}
	$("#urlPanel").find(":input").val("");
	$("#urlPanel").show().siblings("div").hide();
}

function saveUrl(obj){
	var setUrl = $("#urlPanel").find(":input").val();
	if(setUrl.indexOf('&')>=0){
		var reg=new RegExp("&","g"); //创建正则RegExp对象  
		setUrl=setUrl.replace(reg,";"); 
	}
	var button = menuData.button[indexOne];
	var id = 0;
	if(flag==1){
		button.url=setUrl;
		button.type="view";
		id = pareWechatMenuId;
	}else{
		button.sub_button[indexTwo].url=setUrl;
		button.sub_button[indexTwo].type="view";
		id = subWechatMenuId;
	}
	$.ajax({
        type: "post",
        url: $.baseUrl+'/wechatMenuController/saveUrl.do?',
        data : "menuUrl="+setUrl+"&id="+id,
        dataType: "json",
        success: function(data){
        	if(data.status == 200){
        		//刷新菜单树
        		generateMenu(menuData);
        		$("#urlPanel").show().siblings("div").hide();
        		alert(data.message);
        	}else{
        		alert(data.message);
        	}
         }
    });
}

function editMenu(obj){
	var button = menuData.button[indexOne];
	var url;
	var tempId;
	var name;
	if(flag==1){
		 url = button.url;
		 tempId = button.tempId;
		 name = button.menu_name;
		 $("#urlPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("一级菜单 ：<span>"+name+"</span>");
	}else{
		 url = button.sub_button[indexTwo].url;
		 tempId = button.sub_button[indexTwo].tempId;
		 name = button.sub_button[indexTwo].menu_name;
		 $("#urlPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("二级菜单 ：<span>"+name+"</span>");
	}
	var menu_name =$(obj).parent().find(":input").val();
	if(url && !tempId){
		$("#urlPanel").find(":input").val(url);
		$("#urlPanel").show().siblings("div").hide();
		
	}else if(!url && tempId){
		var messType = $("#viewDiv").find(":input").val();
		var content = $("#viewDiv").html();
		
		if(messType==1){//图文
			$("[data-for='#tuwenedit']").addClass("active");
			$("#tuwenedit").html("");
			$("#tuwenedit").append(content);
			$("#tuwenedit").append('<button onclick="deleteMessage()" type="button" class="btn btn-success btn-block">删除</button>');
			
		}else if(messType==2){//图片
			$("[data-for='#tupianedit']").addClass("active");
			$("#tupianedit").html("");
			$("#tupianedit").append(content);
			$("#tupianedit").append('<button onclick="deleteMessage()" type="button" class="btn btn-success btn-block">删除</button>');
		}
		
		$("#editSetPanel").show().siblings("div").hide();
	}
	
}

function resetMenu(){
	//判断一级还是二级，显示不同页面
	var button = menuData.button[indexOne];
	var name = button.menu_name;
	if(flag==1){
		 button.url=null;
		 button.tempId=null;
		 $("#onePanel").find(".wechat_caidan_right_title").find("span").eq(0).html("一级菜单 ：<span>"+name+"</span>");
		 $("#onePanelTitel").html("请设置"+"\""+name+"\""+"菜单的内容");
		 $("#onePanel").show().siblings("div").hide();
	}else{
		 button.sub_button[indexTwo].url=null;
		 button.sub_button[indexTwo].tempId=null;
		 $("#twoPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("二级菜单 ：<span>"+name+"</span>");
		 $("#twoPanelTitel").html("请设置"+"\""+name+"\""+"菜单的内容");
		  $("#twoPanel").show().siblings("div").hide();
	}
}

function deleteMessage(){
	$("#setPanel").show().siblings("div").hide();
}

//图片消息保存
function saveImage(){
	var data="";
	//图片上传获取获取返回的media_id,tempId
	$('#uploadForm').form('submit', {    
	    url:$.baseUrl+'/wechatMenuController/uploadImage.do',  
	    onSubmit: function(){    
	    },    
	    success:function(result){    
	    	data = $.parseJSON(result);
		}    
	});  
	
	//修改js数据中的media_id和type,tempId
	var button = menuData.button[indexOne];
	if(flag==1){
		 button.tempId=data.wechatMessageTemplateId;
		 button.type="media_id";
		 button.media_id=data.mediaId;
		 $("#editSetPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("二级菜单 ：<span>"+name+"</span>");
	}else{
		 button.sub_button[indexTwo].tempId=data.wechatMessageTemplateId;
		 button.sub_button[indexTwo].type="media_id";
		 button.sub_button[indexTwo].media_id=data.mediaId;
		 $("#editSetPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("二级菜单 ：<span>"+name+"</span>");
	}
	
	$("[data-for='#tupianedit']").addClass("active");
	$("#tupianedit").html("");
	$("#tupianedit").append('<img src="'+data.pictureUrl+'" />');
	$("#tupianedit").append('<button onclick="deleteMessage()" type="button" class="btn btn-success btn-block">删除</button>');
	
	//跳转编辑页面
	$("#editSetPanel").show().siblings("div").hide();
}
var selectMessageId;
var selectMediaId;
var selectMessageIndex;
function checkImageText(id,mediaId,i){
	var strDiv="<div id='check"+i+"' class='wechat_sucai_check'><i class='glyphicon glyphicon-ok'></i></div>";
	if($("#index"+i).find("#check"+i).length==0){
		$("div").remove(".wechat_sucai_check");
		$("#index"+i).append(strDiv);
	}
	selectMessageId = id;
	selectMediaId = mediaId;
	selectMessageIndex = i;
}

function selectNewsOk(){
	$('#suCai').find(".detailed_close").click();
	
	$("[data-for='#tuwenedit']").addClass("active");
	$("#tuwenedit").html("");
    $("div").find(".wechat_sucai_check").remove();
	$("#tuwenedit").append($("#index"+selectMessageIndex).html());
	$("#tuwenedit").append('<button onclick="deleteMessage()" type="button" class="btn btn-success btn-block">删除</button>');
	if(flag==1){
		 $("#editSetPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("一级菜单 ：<span>"+name+"</span>");
	}else{
		 $("#editSetPanel").find(".wechat_caidan_right_title").find("span").eq(0).html("二级菜单 ：<span>"+name+"</span>");
	}
	var button = menuData.button[indexOne];
	if(flag==1){
		 button.tempId=selectMessageId;
		 button.type="media_id";
		 button.media_id=selectMediaId;
	}else{
		 button.sub_button[indexTwo].tempId=selectMessageId;
		 button.sub_button[indexTwo].type="media_id";
		 button.sub_button[indexTwo].media_id=selectMediaId;
	}
	//跳转编辑页面
	$("#editSetPanel").show().siblings("div").hide();
}
//图文消息保存
function saveNews(){
		/*
	var button = menuData.button[indexOne];
	if(flag==1){
		 button.tempId=selectMessageId;
		 button.type="media_id";
		 button.media_id=selectMediaId;
	}else{
		 button.sub_button[indexTwo].tempId=selectMessageId;
		 button.sub_button[indexTwo].type="media_id";
		 button.sub_button[indexTwo].media_id=selectMediaId;
	}
	

	$("[data-for='#tuwenedit']").addClass("active");
	$("#tuwenedit").html("");
    $("div").find(".wechat_sucai_check").remove();
	$("#tuwenedit").append($("#index"+selectMessageIndex).html());
	
	$("#tuwenedit").append('<button onclick="deleteMessage()" type="button" class="btn btn-success btn-block">删除</button>');
	$("#editSetPanel").show().siblings("div").hide();
	*/
}

function editMenuName(){
	if(!indexOne){
		alert('请选择要修改的菜单');
		return;
	}else {
		var button = menuData.button[indexOne];
		var name;
		if(flag==1){
			 name = button.menu_name;
			 $("#firstAddEdit").find(":input").val(name);
			 $("#firstAddEdit").show();
		}else{
			 name = button.sub_button[indexTwo].menu_name;
			 $("#secondAddEdit").find(":input").val(name);
			 $("#secondAddEdit").show();
		}
		
	}
	
}

function saveEditName(){
	var button = menuData.button[indexOne];
	var menu_name;
	var id = 0;
	if(flag==1){
		menu_name = $("#firstAddEditInput").val();
		 button.menu_name=menu_name;
		 $('#firstAddEdit').find(".detailed_close").click();
		 id = pareWechatMenuId;
	}else{
		menu_name = $("#secondAddEditInput").val();
		 button.sub_button[indexTwo].menu_name=menu_name;
		 $('#secondAddEdit').find(".detailed_close").click();
		 id = subWechatMenuId;
	}
	menu_name= encodeURI(encodeURI(menu_name));
	$.ajax({
        type: "post",
        url: $.baseUrl+'/wechatMenuController/editMenu.do?menuName='+menu_name+'&id='+id,
        dataType: "json",
        success: function(data){
        	if(data.status == 200){
        		//刷新菜单树
        		generateMenu(menuData);
        		$("#viewPanelDefault").show().siblings("div").hide();
        		alert(data.message);
        	}else{
        		alert(data.message);
        	}
         }
    });
}

//删除菜单
function deleteMenu(){
	if(confirm("确定要删除菜单吗？")){
		var id = 0;
		if(flag==1){
		 	menuData.button.splice(indexOne, 1);
		 	id = pareWechatMenuId;
		}else{
			menuData.button[indexOne].sub_button.splice(indexTwo, 1);
			id = subWechatMenuId;
		}
		$.ajax({
	        type: "post",
	        url: $.baseUrl+'/wechatMenuController/deleteMenu.do?id='+id,
	        dataType: "json",
	        success: function(data){
	        	if(data.status == 200){
	        		//刷新菜单树
	        		generateMenu(menuData);
	        		$("#viewPanelDefault").show().siblings("div").hide();
	        		alert(data.message);
	        	}else{
	        		alert(data.message);
	        	}
	         }
	    });
	}
	
}
