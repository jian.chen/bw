define(['common', 'view/sys/sysManage'], function(common, sysManage) {
	function init() {
		//初始化输入框验证
		initInput();
		$("#editBtn").click(function(){
            isAuth('/wechatAccountController/saveWechatAccount.do',function(){
                common.Page.loadCenter('sys/wechatAccountConfEdit.html');
            });
		})
		queryWechatAccount(); 
	};

	function queryWechatAccount(){
		$.ajax({
			url: baseUrl+"/wechatAccountController/showWechatAccount.do",
            type:"GET",
            dataType:"json",
			success:function(result){
				if(result != null && result.status == "200"){
                    var content = result.obj;
					$("[name='connectStatusMsg']").text(content.connectStatusMsg);
					$("[name='wechatAccountName']").text(content.wechatAccountName);
					$("[name='wechatAccountNumber']").text(content.accountNumber);
					$("[name='originalId']").text(content.originalId);
					$("[name='appId']").text(content.appId);
					$("[name='appSecret']").text(content.appSecret);
				}
			}
		});
	}

    function back() {
        sysManage.setUrl('sys/wechatAccountConf.html');
        common.Page.loadCenter('sys/sysManage.html');
    }
	
	return {
		init: init,
		queryWechatAccount: queryWechatAccount,
        back: back
	}
});



