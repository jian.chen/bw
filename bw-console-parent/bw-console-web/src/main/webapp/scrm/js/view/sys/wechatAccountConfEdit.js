define(['common', 'view/sys/wechatAccountConf'], function(common, wechatAccountConf) {
	function init() {
		//初始化输入框验证
		initInput();
		$("#saveBtn").click(function(){
            var ck = formCheck($("#wechatAccountForm"));
            if(ck){
            	$("#dataLoad").show();
            	isAuth("/wechatAccountController/saveWechatAccount.do",function(){
            		$("#wechatAccountForm").form('submit',{
            			url: baseUrl+"/wechatAccountController/saveWechatAccount.do",
            			success:function(result){
            				$("#dataLoad").hide();
            				if(result == 2){
								$.messager.alert("提示", "保存成功！", "info", function(){
									//queryWechatAccount();
									wechatAccountConf.back();
								});
            				}else if(result.status == '401'){
            					$.messager.alert("");
            				}
            			}
            		});
            	});
            }else{
				$.messager.alert("提示", "输入数据不合法！");
            }
		});
        $("#backBtn").click(function(){
            wechatAccountConf.back();
        });
        $("a[name='checkConnect']").click(function(){
            isAuthForAjax("/wechatAccountController/testWechatConnect.json", "POST", "", "JSON",{}, function(data){
                if (data.status == "200") {
                    $.messager.alert("提示","连接成功");
                    $("[name='connectStatusMsg']").text("连接成功");
                } else if (data.status == "300") {
                    $.messager.alert("提示","连接失败");
                    $("[name='connectStatusMsg']").text("连接失败")
                } else {
                    $.messager.alert("提示","出错");
                };
            }, function(data) {
                $.messager.alert("提示","出错");
            });
        });
		queryWechatAccount();
	}

    function queryWechatAccount(){
		$.ajax({
			url: baseUrl+"/wechatAccountController/showWechatAccount.do",
            type:"GET",
            dataType:"json",
			success:function(result){
				if(result != null && result.status == "200"){
                    var content = result.obj;
					$("[name='connectStatusMsg']").text(content.connectStatusMsg);
					$("[name='wechatAccountName']").val(content.wechatAccountName);
					$("[name='wechatAccountNumber']").val(content.accountNumber);
					$("[name='originalId']").val(content.originalId);
					$("[name='appId']").val(content.appId);
					$("[name='appSecret']").val(content.appSecret);
				}
			}
		});
	}
	
	return {
		init: init,
		queryWechatAccount: queryWechatAccount
	}
});



