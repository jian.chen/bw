define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		//initInput();
	};

	return {
		init: init
	}
});
    	//验证是否为空   
function isNulls(val){
	if(null==val || "null"==val || "undefined"==val || ""==val){
		return true;
	}else{
		return false;
	}	
}
function doCheck(){
   	var newpassword1=$("#password2").val();
	var newpassword2=$("#password3").val();
	if (newpassword1.length < 6 || newpassword1.length > 20) {
		$.messager.alert("提示", "密码长度必须大于6，小于20个字符！");
		return false;
	}
	var reg = /^[a-zA-Z0-9]*$/;
	if (!reg.test(newpassword1)) {
		$.messager.alert("提示", "密码可填写内容： 数字、英文、英文字符！");
		return false;
	}
	if(newpassword1 != newpassword2){
		$.messager.alert("提示", "两次输入密码不同");
	    $("#password2").focus();
	    return (false);
    }
	return true;
}
function doSubmit(){
	var oldpassword=$("#password1").val();
	var newpassword=$("#password2").val();
	if(doCheck()){
 		var url=$.baseUrl+"/sysUserController/updatePassword.do?newpwd="+newpassword+"&oldpwd="+oldpassword;
    	$.ajax({
            type:"POST",
            url:url,
            dataType:"JSON",
            data:$('#save_form').serialize(),
            success:function(data){
              	if(data.status==200){
					$.messager.alert("提示", "修改密码成功");
              		$("#password1").val("");
              		$("#password2").val("");
              		$("#password3").val("");
              	} else {
					$.messager.alert("提示", data.message);
				}
            },
            error:function(){
				$.messager.alert("提示", "request error");
            }
        });
	} 
}