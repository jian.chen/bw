var pointsData;
define(['common'], function(common) {
	
	function init() {
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		
		//初始化表格
		initPointsDataGrid();
		
		$("#searchPR").click(function(){
			var ck = formCheck($("#pointsRegionForm"));
			if(ck){
				initPointsDataGrid();
			}else{
				alert("1");
			}
		});
		
		$("#daochu").click(function(){
			location.href = $.baseUrl + '/pointsReport/leadToExcelPointsRegion?startDate='+$("#startDate").val()+'&endDate='+$("#endDate").val();
		})
	
	}
	
	return {
		init: init
	}
	
});

function initPointsDataGrid(){
	$.ajax({
        method : 'GET',
        url : $.baseUrl+'/pointsReport/showPointsList',
        async : false,
        data: $('#pointsRegionForm').serialize(),
        dataType : 'json',
        success : function(data) {
        	pointsData = data.obj;
        	var dataArr = data.rows;
        	var arr = new Array();
            for ( var machine =0; machine<data.rows.length;machine++) {
                arr[machine] = {
                	'activityInstanceName' : dataArr[machine].activityInstanceName,
                    'pointsInNumber' : dataArr[machine].pointsInNumber,
                    'withPointsInNumber' : withPointsInNumber(dataArr[machine].withPointsInNumber),
                };
            }
            $('#pointsList').datagrid('loadData', arr);
        },
        error : function() {
            alert('error');
        }
    });
}

function withPointsInNumber(val){
	if(val==undefined || val ==''){
	return '';
	}else{
		return (Math.round(val / pointsData.pointsInNumber * 10000) / 100.00 + "%");
	}
}
