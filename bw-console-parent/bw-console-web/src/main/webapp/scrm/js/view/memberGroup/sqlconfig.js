/**
 * type：
 *  1、会员标签 
 *  2、会员分组 
 *  3、粉丝标签
 *  4、粉丝分组
 */
var _where = " where ";
var _or = " or ";
var _and = " and ";
//显示的sql
var fullsql='';
//显示的sql描述
var fullsqldesc='';
//条件租数量
var count=0;
function showsql(id,type) {
	
	var sql = $('#'+id).val();
	var url = "";
	if(type == '1') { // 会员标签 
		url = $.baseUrl+'/memberController/checkMemberSql.do';
	} else if (type == '2') { // 会员分组 
		url = $.baseUrl+'/memberController/checkMemberSql.do';
	} else if(type == '3') {
	} else if(type == '4') {
	}
	url = url + "?sql="+encodeURIComponent(sql),
	/*
	$.ajax({ 
        type: "get", 
        url: url, 
        dataType: "json",
        async: false,
        beforeSend: function (xhr) {
        	$.messager.progress({title : 'sql校验',msg : '正在验证当前的SQL有效性，请稍等...'});
　　　　　　},
        success: function (data) {
        	$.messager.progress('close');
	        if(data.status=='200'){
	        	initSQLView(type, sql, true);
	    	}else{
	    		initSQLView(type, sql, false);
	    	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        	$.messager.progress('close');
            alert(errorThrown); 
        } 
    });
    */
	initSQLView(type, sql, false);
}	

/**
 * 初始化SQL编辑器的视图
 * @param sql
 * @param isRight
 */
function initSQLView(type, sql, isRight) {
	if (sql == '' || !isRight) {
		if (type == 1 || type == 2) {
			fullsql = "select member_id from member_view ";
		} else if (type == 3 || type == 4) {
			fullsql = "select wechat_fans_id from fans_view ";
		}
		count = 0;
		fullsqldesc = "";
	} else {
		fullsql = sql;
	}
	
	var url = $.baseUrl+"/memberController/showSqlAttrList.do?type=" + type;
	var obj='';
	$.ajax({
		async : false,
		url : url,
		type : "get",
		dateType : "json",
		success : function(result) {
			obj = eval("(" + result + ")");	// 转换后的JSON对象
			//obj = result;
		},
		error : function() {
			alert("出错了:(");
		}
	});
	
	var htmlText = '';
	htmlText += '<div class="detailed contactDetailed sqlDetailed">';
	htmlText += '<div class="contactDetailed_title">SQL编辑器</div>';
	htmlText += '<table class="SQL_table"><tr><th width="20px"></th><th width="100px">属性</th><th width="150px">条件</th><th width="150px">值</th>' +
			'<th width="70px"></th><th width="230px">生成SQL语句</th><th width="230px">生成SQL语句描述</th></tr>';
	if(obj){
		for (var i=0;i<obj.length;i++) {
				//前
				htmlText += '<tr><td><input type="hidden" value="'+obj[i].pageType
					+'" /><input type="hidden" value="'+obj[i].columnName+'" /><input name="chk_list" type="checkbox" style="width:20px"/></td><td>'
					+obj[i].attributeName+'</td><td>';
				htmlText += '<div><input type="hidden" value=""/><input size="16" name="condition_list" type="text" class="form-control" value="等于" input-type="select" readonly>';
				htmlText += '<ul class="dropdown-menu">';
				//循环生成条件
				var arr = obj[i].attributeCondition.split('|');
				for (var j = 0;j<arr.length;j++) {
					if(obj[i].pageType == 1 || obj[i].pageType == 3){
						htmlText += '<li data-id="" ><a href="#" onclick="javascript:shownext(this);">'+arr[j]+'</a></li>';
					}else{
						htmlText += '<li data-id="" ><a href="#">'+arr[j]+'</a></li>';
					}
					
				}
				htmlText +=	'</ul><i class="input_icon input_down"></i></div></td>';
				htmlText +=	'<td name="value_list"><div>';
				//页面类型
				if (obj[i].pageType == 1) {
					htmlText +=	'<input size="16" type="text" class="form-control" value="">';
				}else if(obj[i].pageType == 2){
					htmlText += '<input type="hidden" value="" /><input size="16" type="text" class="form-control" value="" input-type="select" readonly>';
					//下拉框根据data或者url 赋值
					if(obj[i].valueType==1){
						htmlText += '<ul class="dropdown-menu" data="'+obj[i].attributeValue+'" ></ul><i class="input_icon input_down"></i>';
					}else if(obj[i].valueType==2){
						htmlText += '<ul class="dropdown-menu" url-options="'+obj[i].attributeValue+'" ></ul><i class="input_icon input_down"></i>';
					}
					
				}else if(obj[i].pageType == 3){
					htmlText += '<input size="16" type="text" class="Wdate date_input" onfocus="WdatePicker({dateFmt:&quot;yyyyMMdd&quot;,alwaysUseStartDate:true})" value="" readonly>';
				}else if(obj[i].pageType == 4){
					var divId = "areaChoose" +i;
					var id = "areaTree" +i;
					htmlText +='<input type="hidden" id="selectNodeIds'+i+'" value="" />';
					htmlText +=	'<input id="selectNodeNames'+i+'" onclick="initTreeData(&quot;'+divId+'&quot;,&quot;'+id+'&quot;,&quot;'+obj[i].attributeValue+'&quot;,&quot;1&quot;)" class="form-control" type="text"></input>';
				}
				htmlText +=	'</div></td>';					
					//前
							
			//sql 内容和描述显示区
			if(i==0){
				htmlText +=	'<td rowspan="'+(obj.length)+'">';
				htmlText +=	'<div class="sqlSelectDiv">';
				htmlText +=	'<input type="hidden" name="" id="groupref" value="1" />';
				htmlText +=	'<input size="16"  type="text" class="form-control sqlSelect" value="或者" input-type="select" readonly>';
				htmlText +=	'<ul class="dropdown-menu">';
				htmlText +=	'<li data-id="1" ><a href="#">或者</a></li>';
				htmlText +=	'<li data-id="2" ><a href="#">并且</a></li>';
				htmlText +=	'</ul>';
				htmlText +=	'<i class="input_icon input_down"></i>';
				htmlText +=	'</div>';
				htmlText +=	'<span class="icon SQL_add" onclick="javascript:getSql();">增加</span>';
				htmlText +=	'</td>';
				htmlText +=	'<td rowspan="'+(obj.length)+'">';
				htmlText +=	'<textarea id="sqldiv" class="form-control" rows="'+(obj.length*2)+'" readonly></textarea>';
				htmlText +=	'</td>';
				htmlText +=	'<td rowspan="'+(obj.length)+'">';
				htmlText +=	'<textarea id="sqldivdesc" class="form-control" rows="'+(obj.length*2)+'"  style="margin-left: 20px;" readonly></textarea>';
				htmlText +=	'</td>';
			}
			//sql 内容和描述显示区
			
			//后
			htmlText +=	'</tr><tr name="nextValue" style="display:none"><td></td><td></td><td></td><td><div>';		
			//页面类型
			if (obj[i].pageType == 1) {
				htmlText +=	'<input size="16" type="text" class="form-control" value="">';
			}else if(obj[i].pageType == 2){
				htmlText += '<input type="hidden" value="" /><input size="16" type="text" class="form-control" value="" input-type="select" readonly>';
				htmlText += '<ul class="dropdown-menu"></ul><i class="input_icon input_down"></i>';
			}else if(obj[i].pageType == 3){
				htmlText += '<input size="16" type="text" class="Wdate date_input" onfocus="WdatePicker({dateFmt:&quot;yyyyMMdd&quot;,alwaysUseStartDate:true})" value="" readonly>';
			}else if(obj[i].pageType == 4){
				
			}
			htmlText +=	'</div></td></tr>';
			//后
		}
	}
	htmlText += '</table>';
	
	//生成树控件弹出层
	for (var i=0;i<obj.length;i++) {
			htmlText += '<div id="areaChoose'+i+'" class="mask none">';
			htmlText += '	<div class="detailed labelChoose">';
			htmlText += '		<div class="contactDetailed_title">选择'+obj[i].attributeName+'</div>';
			htmlText += '			<div style="height:200px;line-height:100px;overflow:auto;overflow-x:hidden;">';
			htmlText += '				<div class="zTreeDemoBackground left">';
			htmlText += '					<ul id="areaTree'+i+'" class="ztree"></ul>';
			htmlText += '				</div>';
			htmlText += '			</div>';
			htmlText += '			<div class="contactDetailed_btn">';
			htmlText += '				<button onclick="saveTree(&quot;areaChoose'+i+'&quot;,&quot;areaTree'+i+'&quot;,'+i+')" type="button" class="btn btn-success input90">保存</button>';
			htmlText += '				<button type="button" class="btn input90 detailed_close2">关闭</button>';
			htmlText += '			</div>';
			htmlText += '		</div>';
			htmlText += '</div>';
	}
	
			
			
	htmlText += '<div class="contactDetailed_btn" style="width:290px;">';
	htmlText += '<button type="button" onclick="savesql()" style="margin-right: 10px;" class="btn btn-success input90">保存</button>';
	htmlText += '<button type="button" class="btn input90 detailed_close2" style="margin-right: 10px;">关闭</button>';
	htmlText += '<button type="button" onclick="changesql('+type+')" class="btn input90">重置</button>';
	htmlText += '</div>';
	htmlText += '</div>';
	
	$("#SQLEdit").html(htmlText);
	$('#sqldiv').text(fullsql);
	$('#sqldivdesc').html(fullsqldesc);
	initSelect();
	initDate();
	initInput();
}

function shownext(obj){
	if($(obj).text()=="介于"){
		$(obj).parent().parent().parent().parent().parent().next().show();
	}else{
		$(obj).parent().parent().parent().parent().parent().next().hide();
	}
}

//点击添加
function getSql() {
	var reg = true;
	var groupsql = '(';
	var groupsqldesc = '条件组' + (count + 1) + " : ";
	if ($("[name='chk_list']:checkbox:checked").length == 0) {
		alert("请选择属性");
		return;
	}
	$("[name='chk_list']:checkbox:checked").each(
		function(i, v) {
			var attrname = $(this).parent().next().text();
			
			var columnname = $(this).prev().val();
			var pagetype = $(this).prev().prev().val();
			var condition = $(this).parent().next().next().find('input[type="text"]').val();
			
			var value1 = '';
			var value2 = '';
			var text = '';
			
			if (pagetype == 1 || pagetype == 3) {//输入框
				 value1 = $(this).parent().next().next().next().find('input').val();
				 value2 = $(this).parent().parent().next().find('input').val();
				 
				 if (condition == '等于') {
					/*
					if (isNaN(value1) || value1=='') {
						reg = false;
						parent.layer.alert(attrname + "请输入数字！");
						return;
					}
					*/
					groupsql += columnname + "=" + value1;
					groupsqldesc += attrname + condition + value1;
				} else if (condition == '大于') {
					groupsql += columnname + ">=" + value1;
					groupsqldesc += attrname + condition + value1;
				} else if (condition == '小于') {
					groupsql += columnname + "<=" + value1;
					groupsqldesc += attrname + condition + value1;
				} else if (condition == '介于') {
					groupsql += columnname + ">=" + value1 + " and " + columnname + "<=" + value2;
					groupsqldesc += attrname + "大等于" + value1 + "小等于" + value2;
				}
				 
			} else if (pagetype == 2) {//下拉框
				value1 = $(this).parent().next().next().next().find('input[type="hidden"]').val();
				text = $(this).parent().next().next().next().find('input[type="text"]').val();
				
				groupsql += columnname + "='" + value1 + "'";
				groupsqldesc += attrname + condition + text;
				 
			} else if(pagetype == 4){//树控件
				 value1 = $(this).parent().next().next().next().find('input[type="hidden"]').val();
				 text = $(this).parent().next().next().next().find('input[type="text"]').val();
				 groupsql += columnname + " in (" + value1 +")";
				 groupsqldesc += attrname + "在(" + text +")中";
			}
			
			groupsql += " and ";
			groupsqldesc += "；";
		}
	);
	if (reg) {
		var searchWhereKey = fullsql.indexOf(_where);
		if ($("#groupref").val() == 1) {
			if(searchWhereKey == -1) {
				fullsql = fullsql + _where;
			} else {
				fullsql = fullsql + _or;
				fullsqldesc += count != 0 ? "或者" : "";
			}
		} else {
			if(searchWhereKey == -1) {
				fullsql = fullsql + _where;
			} else {
				fullsql = fullsql + _and;
				fullsqldesc += count != 0 ? "并且 " : "";
			}
		}
		count++;
		fullsql += groupsql.substring(0, groupsql.lastIndexOf("and")) + ")";
		fullsqldesc += groupsqldesc;
		clearEdit();
		$('#sqldivdesc').html(fullsqldesc);
		$('#sqldiv').html(fullsql);
	}
}
//重置sql
function changesql(type){
	if(type==1||type==2){
		fullsql = "select member_id from member_view  ";
	}else if(type==3||type==4){
		fullsql = "select wechat_fans_id from fans_view  ";
	}
	
	fullsqldesc='';
	count=0;
	clearEdit();
	$('#sqldiv').html("");
	$('#sqldivdesc').html("");
}

function savesql(){
	var sqlsetting = $('#sqldiv').val();
	$('#SQLEdit').find(".detailed_close2").click();
	$('#sqlArea').val(sqlsetting);
}
//
function clearEdit() {
  $("#SQLEdit").find("[name='chk_list']").each(function() {
  	 this.checked = false;
  });
  $("#SQLEdit").find('[name="condition_list"]').each(function() {
  	 this.value = "等于";
  });
   $("#SQLEdit").find('[name="value_list"]').find(":input").each(function() {
  	 this.value = "";
  });
  $("[name='nextValue']").hide();
  
};

function saveTree(divId,id,i){
	var selectIds= getSelectId(id,"1");
	var selectNames= getSelectName(id,"1");
	$('#'+divId).find(".detailed_close2").click();
	$('#selectNodeIds'+i).val(selectIds);
	$('#selectNodeNames'+i).val(selectNames);
}

