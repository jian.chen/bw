define(['common', 'view/sys/ruleManage'], function(common, ruleManage) {
	function init() {
		initPage();
		initInput();
		//internationPage();
		ruleManage.addEditEventBind();
		$("#back_btn").click(ruleManage.back);
	}

	//初始化table
	function initPage(){
		$("#title").text("角色添加");
		$('#huiyuandingdanTable').treegrid( {
			url : baseUrl + "/sysRoleController/showSysRoleDetail.do",
			method:'post',
			queryParams:{'roleId': ''},
			onLoadError : function() {
				$.messager.alert("提示","onLoadError!");
			}
		});
		var params = {};
		params.systemId = 2;
		$('#airTable').treegrid({
			url : baseUrl + "/sysRoleController/showSysRoleDetail.do",
			method:'post',
			queryParams: params});
		$('#shujuquanxian').datagrid({
			url : baseUrl + "/sysRoleController/showSysRoleData",
			method:'post',
			queryParams:{'roleId': ''},
			onLoadError : function() {
				$.messager.alert("提示","onLoadError!");
			}
		});
	}

	return {
		init: init
	}
});


