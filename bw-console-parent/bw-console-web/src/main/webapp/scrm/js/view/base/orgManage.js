define(['common'], function(common) {
	function init() {
		common.Page.resetMain();
		initInput();
	
		$('#save_first').click(function() {
			var ck = formCheck($("#first_org_form"));
			if(ck){
				isAuthForAjax('/orgController/saveOrg.do',"POST","","json",$('#first_org_form').serialize(),function(result){
					var data = eval('(' + result + ')');
					alert(data.message);
					if(data.code==1){
						$('#firstOrgName').val("");
						$("#tg").treegrid('load');
//						$('#tg').treegrid('collapseAll');
					}
				},function(data){
					alert("request error");
				});
				/*var url=$.baseUrl+"/orgController/saveOrg.do";
				$.ajax({
					type:"POST",
					url:url,
					dataType:"JSON",
					data:$('#first_org_form').serialize(),
					contentType: "application/x-www-form-urlencoded; charset=utf-8", 
					success:function(result){
						var data = eval('(' + result + ')');
						alert(data.message);
						if(data.code==1){
							$('#firstOrgName').val("");
							$("#tg").treegrid('load');
//							$('#tg').treegrid('collapseAll');
						}
					},
					error:function(){
						alert("request error");
					}
				});*/
			}
		});
		
	}
	return {
		init: init
	}
});

function initSelectById(id,selectUrl,valueFiled,textFiled,typeIdFiled){
	var _pp = $('#'+id).next("ul");
	if(!_pp){
		return;
	}
	var url = selectUrl;
	var value = valueFiled;
	var text = textFiled;
	var typeId=typeIdFiled;
	$.ajax({ 
        type: "get", 
        url: encodeURI($.baseUrl+url), 
        dataType: "json",
        async: false,
        success: function (data) {
        	$.each(data, function(i, v){
        		eval("var aa = v."+value+"");
        		eval("var bb = v."+text+"");
        		eval("var cc = v."+typeId+"");
        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
		    });
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        }
    });
}
