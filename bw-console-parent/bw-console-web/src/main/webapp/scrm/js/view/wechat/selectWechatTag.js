//将标签添加到已选标签
function addTag(obj){
	var id = $(obj).val();
	var name = $(obj).parent().text();
	if($(obj).prop("checked")){
		if(!addTagIdArr.in_array(id)){
			addTagIdArr.push(id);
		}
		if(!addTagNameArr.in_array(name)){
			addTagNameArr.push(name);
		}
		
	}else{
		addTagIdArr.remove(id);
		addTagNameArr.remove(name);
	}
	appendTag();
	
}
//删除已选标签
function delAddTag(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArr.remove(id);
	addTagNameArr.remove(name);
	appendTag();
}

//查询标签
function queryTag(likeMemberTagName){
	$.ajax({ 
            type: "post", 
            url: $.baseUrl+'/wechatFansLabelController/showWechatFansLabel.do?likeMemberTagName='+likeMemberTagName, 
            dataType: "json",
            async: false,
            success: function (data) {
            	$(".labelChoose_list ul").html("");
            	$.each(data.rows, function(index, value){
			      $(".labelChoose_list ul").append("<li><input type='checkbox' onchange='javascript:addTag(this);' name='' value='"+value.wechatFansLabelId+"' />" + value.labelName+ "</li>");
			    });
            }, 
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    alert(errorThrown); 
            } 
        });
}


function initFansTag(wechatFansId){
	$.ajax({ 
        type: "post",
        url: $.baseUrl+'/wechatFansLabelController/showWechatFansLabelListByFansId.do',
        data:{wechatFansId:wechatFansId},
        async: true,
        success: function (data) {
        	 var data = eval('(' + data + ')');
        	 if(data.rows != undefined){
	    		 for(var i=0;i<data.rows.length;i++){
	    			 var id = data.rows[i].wechatFansLabelList[0].wechatFansLabelId;
	    			 var name = data.rows[i].wechatFansLabelList[0].labelName;
	    			 if(!addTagIdArr.in_array(id)){
	    				 addTagIdArr.push(id);
	    			 }
	    			 if(!addTagNameArr.in_array(name)){
	    				 addTagNameArr.push(name);
	    			 }
	    		 }
        	 }
        	 $('#labelChoose-activitySetting').show();
     		 appendTag();
     		 queryTag($('#likeMemberTagName-activitySetting').val());
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
                alert(errorThrown); 
        } 
    });
}


function appendTag(){
	$(".labelChoose_title").html("<span>已选标签:</span>");
	$.each(addTagIdArr, function(index, value){
      	$(".labelChoose_title").append("<span class='labelChoose_span'>"+addTagNameArr[index]+
      		"<span class='icon icon_close_active' onclick='delAddTag(this)' >" +
      		"<input type='hidden' value='"+addTagIdArr[index]+"' /><input type='hidden' value='"+addTagNameArr[index]+"' />" +
      				"</span></span>");
    });
}