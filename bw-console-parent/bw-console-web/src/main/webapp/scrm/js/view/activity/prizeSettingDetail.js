//奖项序号
var index =0;
define(['common'], function(common) {
	function init() {
		index =0;
		//每次加载必须初始化页面大小
		common.Page.resetMain();
		//初始化下拉框&输入框验证prizeManager.js
		initSelect();
		//initInput();
		//场景游戏ID 赋值
		$("#sceneGameId").val(sceneGameId);
		
		//加载实例信息
		initSceneGameInstanceedit(sceneGameInstanceId);
		
		$('#editBtn_form_activitySetting').click(function() {
			$('#activitySetting-coupon-table').datagrid('appendRow',{awardsId: index,
				awardsName:index,
				awardsCode:index,
				prizeType:index,
				prizeName:index,
				equalscore:index,
				persions:index,
				probability:index,
				awardsSettingId:index
				});
			index++;
			$("#awards_count").val(index);
		});
		
		
		//优惠券筛选
		$("#seachCoupon").click(function(){
			$('#couCateItemCouponTable').datagrid('reload',{
				couponName : $("#couponNameSea").val()
			});
		});
		
		initCouponedit();
		
		/**
		 * 活动提交按钮添加提交事件
		 */
		$("#editBtn-form-prizeSettingDetail").click(function () {
			 //
			if(checkActivity()){
				var saveUrl = "";
				if(optStatus=='edit'){
					saveUrl = "/sceneGameInstanceController/editSceneGameInstance.json";//未激活的抽奖游戏编辑
				}else{
					saveUrl = "/sceneGameInstanceController/editSceneGameInstanceIsUse.json";//已激活的抽奖游戏编辑
				}
				var obj = paramsToJsonEdit();
				 $.ajax({
			         type: "POST",   //访问WebService使用Post方式请求
			         url: $.baseUrl+saveUrl,
			         data:  {
							params : $.trim(JSON.stringify(obj))
			          },
			         dataType: 'json',
			         success: function (res) {//回调函数，result，返回值
			        	 $("#dataLoad").hide();
			        	 if(res.code="SUCCESS"){
			        		 alert(res.message);
			        		 $("#backBtn-form-prizeSettingDetail").trigger("click");
			        	 }else{
			        		 alert(res.message);
			        	 }
			         }
			     });
			}
		});
		
		//分组弹出层
		$('#addFenZuBtn').click(function() {
			$('#addFenZu').show();
			queryGroup();
		});
		//分组弹出层保存
		$('#groupSaveBtn').click(function() {
			//关闭弹出层
			$('#addFenZu').find(".detailed_close2").click();
			addSelectGroupToArray();
			addSelectGroup();
		});
		
		//标签弹出层
		$('#labelChooseBtn2').click(function() {
			$('#labelChoose-registerActivity').show();
			appendTagEdit();
			queryTagEdit($('#likeMemberTagName-registerActivity').val());
		});
		//标签弹出层关闭
		$('#tagSaveBtn2').click(function() {
			//验证标签选择的个数
			if(addTagIdArrEdit.length>5){
				alert("标签添加数目，不能超过5个!");
				return;
			}
			//关闭弹出层
			$('#labelChoose-registerActivity').find(".detailed_close").click();
			addSelectTagEdit();
		});
		
		initBtn();
		
	};
	
	function initBtn(){
		
		$("#game-img").attr('src',imgUrl);
		
		//返回
		 $("#backBtn-form-prizeSettingDetail").click(function () {
			 if(pageTag=="prizeInstanceManager"){
				common.Page.loadCenter('activity/prizeInstanceManage.html');
			 }else{
				 common.Page.loadCenter('activity/prizeManager.html');
			 }
		 });
		 
		//下一步
		 $("#nextBtn-form-prizeSettingDetail").click(function () {
			 var ck = checkActivity("1");
			 if(ck){
				 $("#backBtn-form-prizeSettingDetail").hide();
				 $("#nextBtn-form-prizeSettingDetail").hide();
				 $("#ul-show-first-prizeSettingDetail").hide();
				 $("#div-title-first-prizeSettingDetail").hide();
				 
				 $("#div-img").hide();
				 
				 $("#ul-show-second-prizeSettingDetail").show();
				 $("#aboveBtn-form-prizeSettingDetail").show();
				 $("#div-title-second-prizeSettingDetail").show();
				 
				 $("#coupon-div").show();
	        	 $("#activitySetting-coupon-table").datagrid({"height":"250"});
	        	//加载奖项信息
	     		initAwardsedit(sceneGameInstanceId);
	     		
				//编辑页面显示保存按钮
				if(optStatus=="edit" || optStatus=="editIsUse"){
					$("#editBtn-form-prizeSettingDetail").show();
					if(optStatus=="editIsUse"){
						$("#awardsPrompt").show();
					}
				}else{
					$("#editBtn-form-prizeSettingDetail").hide();
				}
			 }
			 
		 });
		 
		//上一步
		 $("#aboveBtn-form-prizeSettingDetail").click(function () {
			 $("#backBtn-form-prizeSettingDetail").show();
			 $("#nextBtn-form-prizeSettingDetail").show();
			 $("#ul-show-first-prizeSettingDetail").show();
			 $("#div-title-first-prizeSettingDetail").show();
			 $("#div-img").show();
			 
			 $("#ul-show-second-prizeSettingDetail").hide();
			 $("#aboveBtn-form-prizeSettingDetail").hide();
			 $("#editBtn-form-prizeSettingDetail").hide();
			 $("#div-title-second-prizeSettingDetail").hide();
			 $("#coupon-div").hide();
		 });
		 
		 $("#needPointsAmount").prop("disabled", true); 
		//积分消耗选择事件
		$("#isNeedPoints").change(function() {
			if(!$(this).prop('checked')){
				$("#needPointsAmount").val("");
				$("#needPointsAmount").prop("disabled", true);
			}else{
				$("#needPointsAmount").prop("disabled", false); 
			}
		});
	}
	
	return {
		init: init
	};
	
});

function disabledForm(){
	//查看页面禁用功能
	if(optStatus!="edit" && optStatus!="editIsUse"){
		$("form[id='form-activitySetting'] :text").prop("disabled",true);  
		$("form[id='form-activitySetting'] textarea").prop("disabled",true);  
		$("form[id='form-activitySetting'] select").prop("disabled",true);  
		$("form[id='form-activitySetting'] :radio").prop("disabled",true);  
		$("form[id='form-activitySetting'] :checkbox").prop("disabled",true); 
		$("form[id='form-activitySetting'] :button").prop("disabled",true); 
	}
}

function initGradeCheck(ids){
	$.ajax({
		type : "post",
		url : $.baseUrl + "/scrm/view/getContantsForJS",
		dataType : "json",
		success : function(data){
			var gradeList = data.gradeResult;
			var gradeHtml = "";
			for(var i=0; i < gradeList.length; i++){
				gradeHtml+="<ol>"
							+"<span>";
				//如果不为空
				if(ids){
					if(ids.indexOf(gradeList[i].value)>=0){
						gradeHtml+="<input type='checkbox' name='gradesCheck' checked='checked' value='"+gradeList[i].value+"'>";
					}else{
						gradeHtml+="<input type='checkbox' name='gradesCheck' value='"+gradeList[i].value+"'>";
					}
				}else{
					gradeHtml+="<input type='checkbox' name='gradesCheck' value='"+gradeList[i].value+"'>";
				}
				gradeHtml+=gradeList[i].text
							+"</span>"
						+"</ol>";
				$("#grade-ul-prizeSetting").html(gradeHtml);
			}
			disabledForm();
		}
	});
}

//初始化活动实例信息
function initSceneGameInstanceedit(id){
	$.ajax({ 
        url: $.baseUrl+'/sceneGameInstanceController/querySceneGameInstanceDetil.do', 
        data:"sceneGameInstanceId="+id,
        dataType: "json",
        success: function (data) {
        	if(data.status=="SUCCESS"){
        		$("#sceneGameInstanceId").val(data.sceneGameInstance.sceneGameInstanceId);
        		$("#sceneGameId").val(data.sceneGameInstance.sceneGameId);
        		$("#sceneGameInstanceCode").val(data.sceneGameInstance.sceneGameInstanceCode);
        		$("#instanceName").val(data.sceneGameInstance.instanceName);
        		$("#startTime").val(data.sceneGameInstance.startTime);
        		$("#endTime").val(data.sceneGameInstance.endTime);
        		$("#luckyDrawType").val(data.sceneGameInstance.luckyDrawType);
        		$("#luckyDrawValue").val(data.sceneGameInstance.luckyDrawValue);
        		$("#instanceDetail").val(data.sceneGameInstance.instanceDetail);
        		$("#instanceDesc").val(data.sceneGameInstance.instanceDesc);
        		$("#sptProducer").val(data.sceneGameInstance.sptProducer);
        		$("#sptClues").val(data.sceneGameInstance.sptClues);
        		
        		if(data.sceneGameInstance.needPointsAmount=="0"){
        			$("#needPointsAmount").val("");
        		}else{
        			$("#needPointsAmount").val(data.sceneGameInstance.needPointsAmount);
        		}
        		
        		if(data.sceneGameInstance.isNeedPoints=="Y"){
        			$("#isNeedPoints").prop("checked",true);
        			$("#needPointsAmount").prop("disabled", false);
        		}
        		
        		//标签信息显示
        		var memberTagList = data.memberTags;
				var memberTagCount = data.memberTagCount;
				if(memberTagCount>0){
					$("#UseLabel").attr("checked","checked");
					//清空原始数组
					addTagIdArrEdit = [];
					addTagNameArrEdit = [];
					//数组赋值
					for(var i=0;i<memberTagCount;i++){
						addTagIdArrEdit.push(memberTagList[i].memberTagId);
						addTagNameArrEdit.push(memberTagList[i].memberTagName);
					}
					addSelectTagEdit();
					$("#labelButton").show();
					$("#memberTagList").show();
				}
				//分组信息显示
				var memberGroup = data.memberGroup;
				if(memberGroup!=null && memberGroup!='null' ){
					$("#someMember").attr("checked","checked");
					//清空数组
					addGroupIdArr = [];
					addGroupNameArr = [];
					//添加数据
					addGroupIdArr.push(data.memberGroup.memberGroupId);
					addGroupNameArr.push(data.memberGroup.memberGroupName);
					//调用添加页面方法
					addSelectGroup();
					$("#groupButton").show();
					$("#memberGroupList").show();
					$("#memberGroupListClues").show();
				}
				//家庭成员共享赋值
				if(data.sceneGameInstance.sptIssharefamily =='1'){
					$("#familyShare").attr("checked","checked");
					
				}else{
					$("#familyNotShare").attr("checked","checked");
				}
        		
				initGradeCheck(data.sceneGameInstance.sptGrades);
        	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}

//显示或者隐藏标签选择按钮
function showLabelButton(value){
	if(value=='1'){
		$("#labelButton").show();
		$("#memberTagList").show();
	}else{
		$("#labelButton").hide();
		$("#memberTagList").hide();
	}
}

//显示或者隐藏分组选择按钮
function showGroupButton(value){
	if(value=='1'){
		$("#groupButton").show();
		$("#memberGroupList").show();
		$("#memberGroupListClues").show();
	}else{
		$("#groupButton").hide();
		$("#memberGroupList").hide();
		$("#memberGroupListClues").hide();
	}
}

//加载奖项
function initAwardsedit(id){
	
	$.ajax({ 
        url: $.baseUrl+'/sceneGameInstanceController/queryAwardsDetail.do', 
        data:"sceneGameInstanceId="+id,
        dataType: "json",
        success: function (data) {
        	if(data.status=="SUCCESS"){
        		$('#activitySetting-coupon-table').datagrid('loadData',
        				{total:data.total,
        				rows:data.awardsExps
        			});
        		$("#awards_count").val(data.total);
        		index=0;
        		for(var i=0;i<data.total;i++){
        			/*
        			$('#activitySetting-coupon-table').datagrid('appendRow',{awardsId: index,
        				awardsName:index,
        				prizeType:index,
        				prizeName:index,
        				equalscore:index,
        				persions:index,
        				probability:index,
        				awardsSettingId:index
        				});
        			*/
        			//赋值
        			$("#"+index+"_prizeType").val(data.awardsExps[i].prizeType);
        			$("#"+index+"_persions").val(data.awardsExps[i].totalQuantity);
        			$("#"+index+"_equalscore").val(data.awardsExps[i].equalScore);
        			changeContent(index);
        			$("#"+index+"_probability").val(data.awardsExps[i].probability);
        			$("#"+index+"_cycle").val(data.awardsExps[i].cycle);
        			$("#"+index+"_prizeName").val(data.awardsExps[i].prizeName);
        			$("#"+index+"_awardsCode").val(data.awardsExps[i].awardsCode);
        			
        			lotteryRestrict(index);
        			
        			$("#"+index+"_prizeCode").val(data.awardsExps[i].prizeCode);
        			$("#"+index+"_cycleValue").val(data.awardsExps[i].cycleValue);
        			index++;
        		}
        		$("#awards_count").val(index);
        		disabledForm();
        	}
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        } 
    });
}


//加载优惠券
function initCouponedit(){
	$('#couCateItemCouponTable').datagrid( {
		url : $.baseUrl+'/goodsShowController/queryCouponList.do',
		idField : 'couponId',
			onLoadSuccess : function(result) {
				if(result) {
					
	            }
			},
			onLoadError : function() {
				
			}
		});
}

//奖项字段展示处理
function rowformater_awards(value,row,table_index){
	var awardsHtml = "";
	if(table_index==0){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='一等奖'>一等奖";
	}else if(table_index==1){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='二等奖'>二等奖";
	}else if(table_index==2){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='三等奖'>三等奖";
	}else if(table_index==3){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='四等奖'>四等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==4){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='五等奖'>五等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==5){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='六等奖'>六等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==6){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='七等奖'>七等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==7){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='八等奖'>八等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==8){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='九等奖'>九等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}else if(table_index==9){
		awardsHtml = "<input type='hidden' name='"+table_index+"_awardsName' id='"+table_index+"_awardsName' value='十等奖'>十等奖&nbsp;<a href=\"#\" onclick=\"javascript:delete_awards('"+value+"','"+table_index+"')\">删除</a>";
	}
	return awardsHtml;
}



//删除奖项
function delete_awards(value,table_index){
	
	if(table_index<index-1){
		alert("请从最后一个奖项删除！");
	}else if(table_index==index-1){
		
		$('#activitySetting-coupon-table').datagrid('deleteRow',table_index);
		//删除一个奖项序号减一
		index--;
		$("#awards_count").val(index);
	}else{
		alert("数据异常！");
	}
	
}

//加载积分输入框
function awards_equalscore(value,rows,table_index){
	var equalscoreHtml = "";
//	if(table_index==0){
//		equalscoreHtml += "<input type='hidden' name='"+table_index+"_equalscore' id='"+table_index+"_equalscore' data-options='required:true,length:\"8\"'type='text' class='form-control input100' value='0' disabled='disabled'>";
//	}else{
		equalscoreHtml += "<input type='hidden' name='"+table_index+"_equalscore' id='"+table_index+"_equalscore' data-options='required:true,length:\"8\"'type='text' class='form-control input100' value='"+value+"' onblur='checkInteger(this)'>";
//	}
	return equalscoreHtml;
}
//加载奖品编码
function rowformater_awardsCode(value,rows,table_index){
	var awardsCodeHtml = "";
	awardsCodeHtml += "<input type='text' name='"+table_index+"_awardsCode' id='"+table_index+"_awardsCode' placeholder='请勿修改此值' data-options='required:true,length:\"25\"'type='text' class='form-control input100'>";
	return awardsCodeHtml;
}


//奖项内容
function awards_content(value,row,table_index){
	var contentHtml = "";
//	if(table_index==0){
//		contentHtml += "<input type='hidden' name='"+table_index+"_prizeCode' id='"+table_index+"_prizeCode' value='0' />";
//		contentHtml += "<input type='hidden' name='"+table_index+"_awardsType' id='"+table_index+"_awardsType' value='lucky' />";
//		contentHtml += "<input name='"+table_index+"_prizeName' id='"+table_index+"_prizeName' data-options='required:true,length:\"100\"'type='text' class='form-control input180' value='谢谢参与' disabled='disabled'>";
//	}else{
		contentHtml += "<span id='"+table_index+"_awardsSpan'>";
		contentHtml += "<input type='hidden' name='"+table_index+"_prizeCode' id='"+table_index+"_prizeCode' value='"+value+"' />";
		contentHtml += "<input type='hidden' name='"+table_index+"_awardsType' id='"+table_index+"_awardsType' value='"+value+"' />";
		contentHtml += "<input name='"+table_index+"_prizeName' id='"+table_index+"_prizeName' data-options='required:true,length:\"100\"'type='text' class='form-control input180' value='"+value+"'>";
		contentHtml += "</span>";
//	}
	return contentHtml;
}



//加载奖品种类
function prizeTypeSelect(value,row,table_index){
	var contentHtml = "";
//	if(table_index==0){
//		contentHtml += "<select id='"+table_index+"_prizeType' name='"+table_index+"_prizeType' style='width:100px;' disabled='disabled'> ";
//		contentHtml += "<option value='3'>谢谢参与</option>";	
//		contentHtml += "</select>";
//	}else{
		contentHtml += "<select id='"+table_index+"_prizeType' name='"+table_index+"_prizeType' value='"+value+"' style='width:100px;' onchange=\"changeContent('"+table_index+"')\"> ";
		contentHtml += "<option value='1'>实物</option>";	
		contentHtml += "<option value='2'>优惠券</option>";
		contentHtml += "<option value='3'>卡牌</option>";
		//contentHtml += "<option value='4'>积分</option>";
		contentHtml += "</select>";
//	}
	
	return contentHtml;
}

//加载产品库存
function awards_persions(value,row,table_index){
	var persionsHtml = "";
//	if(table_index==0){
//		persionsHtml += "<input name='"+table_index+"_persions' id='"+table_index+"_persions' data-options='required:true,length:\"10\"'type='hidden' class='form-control input140' value='0'>";
//		persionsHtml += "<input  type='text' class='form-control input140' value='不限' disabled='disabled'>";
//	}else{
		persionsHtml += "<input name='"+table_index+"_persions' id='"+table_index+"_persions' data-options='required:true,length:\"10\"'type='text' class='form-control input140' value='"+value+"' onblur='checkInteger(this)'>";
//	}
	
	return persionsHtml;
}

//加载产品中奖率
function awards_probability(value,row,table_index){
	var probabilityHtml = "";
//	if(table_index==0){
//		probabilityHtml += "<input name='"+table_index+"_probability' id='"+table_index+"_probability' data-options='required:true,length:\"8\"'type='text' class='form-control input140' value='100' disabled='disabled'>";
//	}else{
		probabilityHtml += "<input name='"+table_index+"_probability' id='"+table_index+"_probability' data-options='required:true,length:\"8\"'type='text' class='form-control input140' value='"+value+"'>";
//	}
	return probabilityHtml;
}

//奖项概率输入框失去焦点事件
function  onblurProbability(obj){
	//输入框验证
	checkDecimals(obj);
	//谢谢参与概率计算
	//calculate_calculate(obj);
}

function calculate_calculate(object){
	var total = 0;
	$("input[name$='_probability']").each(function(){
		if($(this).val()!=""){
			total = parseFloat(total) + parseFloat($(this).val());
		}
	});
	var probability = total - $("#0_probability").val();
	
	$("#0_probability").val((100-probability).toFixed(3));
}

//加载抽奖限制调条件
function awardsRestrict(value,rows,table_index){
	var contentHtml = "";
//	if(table_index==0){
//		/*contentHtml += "<span class='fl' style='float:none; vertical-align:middle;'>每:</span>";*/
//		contentHtml += "<select id='"+table_index+"_cycle' name='"+table_index+"_cycle' style='width:50px;float:none; display:inline-block; vertical-align:middle; margin:0 5px;' disabled='disabled'> ";
//		contentHtml += "<option value='5'>不限</option>";	
//		contentHtml += "</select>";
//		contentHtml += "<input  type='hidden' name='"+table_index+"_cycleValue' id='"+table_index+"_cycleValue' value='0'/><input class='form-control' type='hidden' style='width: 55px; height:25px;float:none; display:inline-block; vertical-align:middle; margin:0 0 0 7px;' value='不限' disabled='disabled'/>";
//	}else{
		contentHtml += "<span class='fl' style='float:none; vertical-align:middle;'>每:</span>";
		contentHtml += "<select id='"+table_index+"_cycle' name='"+table_index+"_cycle'style='width:50px;float:none; display:inline-block; vertical-align:middle; margin:0 5px;' onchange=\"lotteryRestrict('"+table_index+"')\"> ";
		contentHtml += "<option value='5'>不限</option>";
		
		contentHtml += "<option value='1'>每</option>";	

//		contentHtml += "<option value='5'>不限</option>";
		
		contentHtml += "</select>";
		contentHtml += "<span style='display:none;' id='chou_"+table_index+"'><input value='0' class='form-control' type='text' name='"+table_index+"_cycleValue' id='"+table_index+"_cycleValue' style='width: 55px; height:25px;float:none; display:inline-block; vertical-align:middle; margin:0 0 0 7px;' data-options='required:true,length:\"8\"'/>人可抽中1次</span>";
//	}
	return contentHtml;
}


//抽奖限制JS
function lotteryRestrict(value){
	var val = $("#"+value+"_cycle").val();
	if(val==5){
		$("#chou_"+value).hide();
		$("#"+value+"_cycleValue").val("0");
	}else{
		$("#chou_"+value).show();
		$("#"+value+"_cycleValue").val("");
	}
}

//活动提交验证
function checkActivity(step){
	//活动名称验证
	//公用输入框方法验证
//	var ckActivity = formCheck($("#form-activitySetting"));
	
	if(true){
		if($("#instanceName").val()==""){
			alert("请填写活动名称!");
			return false;
		}
		//验证输入时间
		if($("#startTime").val()==""){
			alert("请填写活动开始时间!");
			return false;
		}
		if($("#endTime").val()==""){
			alert("请填写活动结束时间!");
			return false;
		}
		//抽奖机会验证
		if($("#luckyDrawType").val()==""){
			alert("请填写抽奖机会类型!");
			return false;
		}
		//抽奖机会次数验证
		if($("#luckyDrawValue").val()==""){
			alert("请填写抽奖次数!");
			return false;
		}
		
		if(step=="1"){
			return true;
		}
		
		//验证中奖率
//		var sum = 0;
//		$("input[name$='_probability']").each(function(){
//			sum = parseFloat(sum) + parseFloat($(this).val());
//		});
//		if(sum==100){
//			
//		}else{
//			alert("奖项总中奖率应等于100%");
//			return false;
//		}
		
		//验证分组是否选择
		if($("input[name='sptIsuseforgroupmember']:checked").val()=='1'){
			if(addGroupIdArr.length==0){
				alert("请添加会员分组!");
				return false;
			}
		}
		
		//验证标签是否选择
		if($("input[name='sptIsusememberlabel']:checked").val()=='1'){
			if(addTagIdArrEdit.length==0){
				alert("请添加会员标签!");
				return false;
			}
		}
		
	}else{
		return false;
	}
	
	return true;
}

//输入框整数验证
function checkInteger(obj){
	var re = /^[0-9]*$/;
	if(!re.test($(obj).val())){
		alert("请输入正整数!");
		$(obj).val("");
	}
}

//输入框小数验证
function checkDecimals(obj){
	var re = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
	if(!re.test($(obj).val())){
		alert("请输入数值!");
		$(obj).val("");
	}
}

//变更奖品内容输入框
function changeContent(id){
	//奖品类型
	var type = $("#"+id+"_prizeType").val();
	var html = "";
	//如果奖品类型类为优惠券，加载优惠券选择按钮
	$("#"+id+"_awardsSpan").html("");
	if(type==2){
		html += "<input type='hidden' name='"+id+"_prizeCode' id='"+id+"_prizeCode' value='' />";
		html += "<input type='hidden' name='"+id+"_awardsType' id='"+id+"_awardsType' value='common' />";
		html += "<input id='"+id+"_prizeName' name='"+id+"_prizeName' class='form-control input180' type='text' value='' readOnly placeholder='只可选择张数无限的券' onclick=\"showCouponDiv('"+id+"')\" data-options='required:true'></input>";
	}else{
		html += "<input type='hidden' name='"+id+"_prizeCode' id='"+id+"_prizeCode' value='' />";
		html += "<input type='hidden' name='"+id+"_awardsType' id='"+id+"_awardsType' value='common' />";
		html += "<input name='"+id+"_prizeName' id='"+id+"_prizeName' data-options='required:true,length:\"100\"'type='text' class='form-control input180' value=''>";
	}
	$("#"+id+"_awardsSpan").html(html);
}

//优惠券弹框
function showCouponDiv(id){
	$("#couponDiv").show();
	$('#couCateItemCouponTable').datagrid('reload');
	$("#submitButton").attr("onclick","getCouponId('"+id+"')");
}

//填充选中优惠券
function getCouponId(id){
	var selected = $('#couCateItemCouponTable').datagrid('getChecked');
	$("#"+id+"_prizeCode").val(selected[0].couponId);
	$("#"+id+"_prizeName").val(selected[0].couponName);
	$("#closeCoupon").click();
}

//请求参数封装成js对象
function paramsToJsonEdit(){
	var sptMlrids = "";
	var gradeIds = "";
	//获取会员标签的值
	$("input[name='sptMlrid']").each(function(){
		if(sptMlrids==""){
			sptMlrids = $(this).val();
		}else{
			sptMlrids += ","+$(this).val();
		}
	});
	$("input[name='gradesCheck']:checked").each(function(){
		if(gradeIds==""){
			gradeIds = $(this).val();
		}else{
			gradeIds += ","+$(this).val();
		}
	});
	//初始化实例
	var sceneGameInstance = {
		"sceneGameInstanceId":$("#sceneGameInstanceId").val(),	
		"sceneGameId":$("#sceneGameId").val(),
		"instanceName":$("#instanceName").val(),
		"startTime":$("#startTime").val(),	
		"endTime":$("#endTime").val(),
		"luckyDrawType":$("#luckyDrawType").val(),
		"luckyDrawValue":$("#luckyDrawValue").val(),
		"instanceDetail":$("#instanceDetail").val(),
		"instanceDesc":$("#instanceDesc").val(),
		"sptIsuseforgroupmember":$("input[name='sptIsuseforgroupmember']:checked").val(),
		"sptIsusememberlabel":$("input[name='sptIsusememberlabel']:checked").val(),
		"sptIssharefamily":$("input[name='sptIssharefamily']:checked").val(),
		"sptGrades":gradeIds,
		"isNeedPoints":$("input[name='isNeedPoints']:checked").val(),
		"needPointsAmount":$("#needPointsAmount").val(),
		"sptProducer":$("#sptProducer").val(),
		"sptMgoid":$("#sptMgoid").val(),
		"sptMlrids":sptMlrids,
		"sptClues":$("#sptClues").val()
	};
	
	//初始化奖项
	var awardsArr = new Array();
	for(var i=0;i<index;i++){
		var award = {
			"awardsName":$("#"+i+"_awardsName").val(),
			"awardsCode":$("#"+i+"_awardsCode").val(),
			"prizeCode":$("#"+i+"_prizeCode").val(),
			"prizeName":$("#"+i+"_prizeName").val(),
			"prizeType":$("#"+i+"_prizeType").val(),
			"persions":$("#"+i+"_persions").val(),
			"probability":$("#"+i+"_probability").val(),
			"cycle":$("#"+i+"_cycle").val(),
			"cycleValue":$("#"+i+"_cycleValue").val(),
			"awardsType":$("#"+i+"_awardsType").val(),
			"equalscore":$("#"+i+"_equalscore").val()
		};
		awardsArr.push(award);
	}
	
	sceneGameInstance.awards = awardsArr;
	return sceneGameInstance;
	
}

//会员标签选弹出层JS==============================start
//存储勾选的标签id
var addTagIdArrEdit = new Array();
//存储勾选的标签name
var addTagNameArrEdit = new Array();

//将标签添加到已选标签
function addTagEdit(obj){
	var id = $(obj).val();
	var name = $(obj).parent().text();
	if($(obj).prop("checked")){
		if(!addTagIdArrEdit.in_array(id)){
			addTagIdArrEdit.push(id);
			addTagNameArrEdit.push(name);
		}	
	}else{
		addTagIdArrEdit.remove(id);
		addTagNameArrEdit.remove(name);
	}
	appendTagEdit();
	
}

//删除已选标签
function delAddTagEdit(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArrEdit.remove(id);
	addTagNameArrEdit.remove(name);
	appendTagEdit();
}

//展示已选择的标签
function appendTagEdit(){
	$("#labelChoose-registerActivity").find(".labelChoose_title").html("<span>已选标签:</span>");
	$.each(addTagIdArrEdit, function(index, value){
    	$("#labelChoose-registerActivity").find(".labelChoose_title").append("<span class='labelChoose_span'>"+addTagNameArrEdit[index]+
    		"<span class='icon icon_close_active' onclick='delAddTagEdit(this)' >" +
    		"<input type='hidden' value='"+addTagIdArrEdit[index]+"'/><input type='hidden' value='"+addTagNameArrEdit[index]+"' />" +
    		"</span></span>");
  });
}

//页面添加选择的标签
function addSelectTagEdit(){
	$("#memberTagList").html("");
	$.each(addTagIdArrEdit, function(index, value){
	  	$("#memberTagList").append("<span class='labelChoose_span'>"+addTagNameArrEdit[index]+
	  		"<span class='icon icon_close_active' onclick='delSelectTagEdit(this)' >" +
	  		"<input type='hidden' value='"+addTagIdArrEdit[index]+"' id='sptMlrid' name='sptMlrid' /><input type='hidden' value='"+addTagNameArrEdit[index]+"'/>" +
	  		"</span></span>");
	});
}

//删除已选标签
function delSelectTagEdit(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addTagIdArrEdit.remove(id);
	addTagNameArrEdit.remove(name);
	addSelectTagEdit();
}

//查询标签
function queryTagEdit(likeMemberTagName){
	$.ajax({ 
      type: "post", 
      url: $.baseUrl+'/memberTagController/showMemberTagList.do?likeMemberTagName='+likeMemberTagName, 
      dataType: "json",
      success: function (data) {
      	$("#labelChoose-registerActivity").find(".labelChoose_list ul").html("");
      	
      	$.each(data.rows, function(index, value){
		      $("#labelChoose-registerActivity").find(".labelChoose_list ul").append("<li><input type='checkbox' onchange='javascript:addTagEdit(this);' name='' value='"+value.memberTagId+"' />" + value.memberTagName+ "</li>");
		    });
      }, 
      error: function (XMLHttpRequest, textStatus, errorThrown) { 
              alert(errorThrown); 
      } 
  });
}
//会员标签选弹出层JS==============================end

//会员分组JS====================start
//存储勾选的分组id
var addGroupIdArr = new Array();
//存储勾选的分组name
var addGroupNameArr = new Array();

function addSelectGroupToArray(){
	var id = $("#selectMemberGroupId").val();
	var name = $("#selectMemberGroupName").val();
	if(id!='' && !addGroupIdArr.in_array(id)){
		addGroupIdArr.push(id);
	}
	if(name!='' && !addGroupNameArr.in_array(name)){
		addGroupNameArr.push(name);
	}
}

//页面添加选择的分组
function addSelectGroup(){
	$("#memberGroupList").html("");
	$.each(addGroupIdArr, function(index, value){
	  	$("#memberGroupList").append("<span class='labelChoose_span'>"+addGroupNameArr[index]+
	  		"<span class='icon icon_close_active' onclick='delSelectGroup(this)' >" +
	  		"<input type='hidden' value='"+addGroupIdArr[index]+"' id='sptMgoid' name='sptMgoid'/><input type='hidden' value='"+addGroupNameArr[index]+"' />" +
	  				"</span></span>");
	});
}

//删除已选分组
function delSelectGroup(obj){
	var id = $(obj).find(":hidden").eq(0).val();
	var name = $(obj).find(":hidden").eq(1).val();
	addGroupIdArr.remove(id);
	addGroupNameArr.remove(name);
	addSelectGroup();
}


//查询分组
function queryGroup(){
	$.ajax({ 
          type: "get", 
          url: $.baseUrl+'/memberGroupController/showMemberGroupListSelect.do', 
          dataType: "json",
          success: function (data) {
          	$("#addFenZu").find(".dropdown-menu").html("");
          	$.each(data, function(index, value){
			      $("#addFenZu").find(".dropdown-menu").append("<li data-id='"+value.memberGroupId+"'><a href='javascript:;'>"+value.memberGroupName+"</a></li>");
			    });
          }, 
          error: function (XMLHttpRequest, textStatus, errorThrown) { 
                  alert(errorThrown); 
          } 
      });
}
//会员分组JS=======================end
