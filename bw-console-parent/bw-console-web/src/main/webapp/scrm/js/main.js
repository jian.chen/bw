requirejs.config({
	baseUrl: './../js',
	paths: {
		bootstrap: './lib/bootstrap.min',
		jquery: './lib/jquery.min',
		//页面js
		index: './index',
		//组件
		common: './mod/common',
		cityInit: './mod/cityInit',
		citySelect: './mod/citySelect',
		selectTag: './view/member/selectTag',
		tree: './view/member/tree',
		selectWechatTag: './view/wechat/selectWechatTag',
		
		//功能模块
		productManage: './view/base/productManage',
		smsManage: './view/base/smsManage',
		storeEdit: './view/base/storeEdit',
		storeManage: './view/base/storeManage',
		
		pointsRuleManage: './view/points/pointsRuleManage',
		complainManage: './view/complain/complainManage',
		//活动模块
		activityManage: './view/activity/activityManage',
		consumeActivity: './view/activity/consumeActivity',
		activitySetting: './view/activity/activitySetting',
		activitySettingDetail: './view/activity/activitySettingDetail',
		activityInstanceManage : './view/activity/activityInstanceManage',
		
		//微信模块
		wechatFansManage : './view/wechat/wechatFansManage',
		wechatFansGroup : './view/wechat/wechatFansGroup',
		wechatFansTag : './view/wechat/wechatFansTag',
		wechatManage : './view/wechat/wechatManage',
		messageAuto : './view/wechat/messageAuto',
		messageManage : './view/wechat/messageManage',
		messageSend : './view/wechat/messageSend',
		materialManage : './view/wechat/materialManage',
		materialAdd : './view/wechat/materialAdd',
		wechatMeum : './view/wechat/wechatMeum'
			
	}
});

