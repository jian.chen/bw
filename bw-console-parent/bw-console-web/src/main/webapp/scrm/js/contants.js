/**
 * 扩展数组 通过value取Text emptyValue就是在找不到的情况下 返回什么字符串
 */
Array.prototype.getText = function(value, emptyValue) {
	for ( var i = 0; i < this.length; i++) {
		if (this[i].value == value) {
			return this[i].text;
		}
	}
	return emptyValue ? emptyValue : '';
}

	//获取当前网址
    var curWwwPath = window.document.location.href;
    //获取主机地址之后的目录
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    //获取主机地址
    var localhostPath = curWwwPath.substring(0, pos);
    //获取带"/"的项目名
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    $.rootPath = localhostPath + projectName;//获取项目根目录
	var pathName=window.document.location.pathname;
	var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
	$.baseUrl = projectName;//初始化项目根路径
	
	var gradeList = new Array();
	var buttonList = new Array();
	var levelList = new Array();

	//会员等级
	$.Member_Grade = gradeList;
	getGradeList();
	
	function getGradeList(){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/scrm/view/getContantsForJS",
			dataType : "json",
			success : function(data){
				gradeList = data.gradeResult;
				$.Member_Grade = gradeList;
			}
		})
	}
	$.Member_Channel=levelList;
	getMemberLevelList();
	function getMemberLevelList(){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/scrm/view/showMemberLevelForJS",
			dataType : "json",
			success : function(data){
				levelList = data.levelResult;
				$.Member_Channel = levelList;
			}
		})
	}
	
	$.Function_Button = buttonList;
	getButtonList();
	
	function getButtonList(){
		$.ajax({
			type : "post",
			url : $.baseUrl + "/scrm/view/showFunctionButtonForJS",
			dataType : "json",
			success : function(data){
				buttonList = data.buttonResult;
				$.Function_Button = buttonList;
			}
		})
	}

/**
 * 发放比例
 */	
$.ISSUE_TYPE =
[
 {
	 value : '1',
	 text : '按活动次数,每一次触发'
 },	
 {
	 value : '2',
	 text : '按活动金额,每一元金额'
 },
 {
	 value : '3',
	 text : '累计积分每满'
 }
];	

/**
 * 意见类型
 */
$.COMPLAIN_TYPE =
[
 {
	 value : '1',
	 text : '意见建议'
 },	
 {
	 value : '2',
	 text : '咨询'
 },
 {
	 value : '3',
	 text : '其他'
 }
];
	
/**
 * 购买渠道
 */	
$.Buy_Channel =
[
 {
	 value : '1',
	 label : 'APP'
 },	
 {
	 value : '2',
	 label : 'Web'
 },
 {
	 value : '3',
	 label : 'Whcat'
 },	
 {
	 value : '4',
	 label : 'callcenter'
 },
 {
	 value : '5',
	 label : '3pp'
 }
];	

/**
 * 优惠券发放渠道
 */	
$.Coupon_Issue_Channel =
[
 {
	 value : '501',
	 text : '系统发放'
 },	
 {
	 value : '502',
	 text : '手动领取'
 },
 {
	 value : '503',
	 text : '活动发放'
 },	
 {
	 value : '504',
	 text : '积分兑换'
 },
 {
	 value : '505',
	 text : '转盘抽奖'
 },
 {
	 value : '508',
	 text : '系统手动发放'
 }
];	

/**
 * 星期
 */	
$.Grade_Level =
[
 {
	 value : '1',
	 label : '绿卡'
 },	
 {
	 value : '2',
	 label : '金卡'
 },
 {
	 value : '3',
	 label : '白金卡'
 }
];
	
/**
 * 星期
 */	
$.Time_Day =
[
 {
	 value : '1',
	 label : '星期一'
 },	
 {
	 value : '2',
	 label : '星期二'
 },
 {
	 value : '3',
	 label : '星期三'
 },
 {
	 value : '4',
	 label : '星期四'
 },
 {
	 value : '5',
	 label : '星期五'
 },
 {
	 value : '6',
	 label : '星期六'
 },
 {
	 value : '7',
	 label : '星期日'
 }
];	

/**
 * 日期类型
 */
$.Date_Type =
[
 {
	 value : 'year',
	 text : '年/月/日'
 },
 {
	 value : 'month',
	 text : '月/日'
 }
]
	
/**
 * 会员渠道
 */
$.Member_Channel = 
[{
	value : '101',
	text : '官网'
}, 
{
	value : '104',
	text : '门店'
}, 
{
	value : '202',
	text : '微信'
},
{
	value : '301',
	text : '联想'
},
{
	value : '302',
	text : '弘毅'
},
{
	value : '303',
	text : '门店VIP'
},
{
	value : '401',
	text : '黑卡渠道'
}
];

/**
 * 系统
 */
$.SYSTEM = [
	{value : '1', text: '管理员'},
	{value : '2', text: '店员'}
]

/**
 * 会员性别
 */
$.gender = 
[{
	value : '1',
	text : '男'
}, 
{
	value : '2',
	text : '女'
}, 
{
	value : '3',
	text : '未知'
}];

/**
 * 是否是会员
 */
$.isMember = 
[{
	value : '1',
	text : '是'
}, 
{
	value : '0',
	text : '否'
}];

/**
 * 婚否
 */
$.IsMarried = 
[{
	value : '1',
	text : '是'
}, 
{
	value : '0',
	text : '否'
}];

/**
 * 职业
 */
$.Profession = 
[{
	value : '1',
	text : '待业'
}, 
{
	value : '2',
	text : '白领'
}, 
{
	value : '3',
	text : '家庭主妇'
}, 
{
	value : '4',
	text : '蓝领'
}, 
{
	value : '5',
	text : '学生'
}, 
{
	value : '6',
	text : '教师'
}];

/**
 * 会员等级
 */
//$.Member_Grade = gradeList;
//需要动态赋值的请在上面先定义

/**
 * 会员星座
 */
$.Member_Zodiac = 
[{
	value : '1',
	text : '白羊座'
}, 
{
	value : '2',
	text : '金牛座'
}, 
{
	value : '3',
	text : '双子座'
}, 
{
	value : '4',
	text : '巨蟹座'
}, 
{
	value : '5',
	text : '狮子座'
}, 
{
	value : '6',
	text : '处女座'
}, 
{
	value : '7',
	text : '天枰座'
}, 
{
	value : '8',
	text : '天蝎座'
}, 
{
	value : '9',
	text : '射手座'
}, 
{
	value : '10',
	text : '摩羯座'
}, 
{
	value : '11',
	text : '水瓶座'
}, 
{
	value : '12',
	text : '双鱼座'
}];

/**
 * 会员属相
 */
$.Member_Chinese_Zodiac = 
[{
	value : '1',
	text : '鼠'
}, 
{
	value : '2',
	text : '牛'
}, 
{
	value : '3',
	text : '虎'
}, 
{
	value : '4',
	text : '兔'
}, 
{
	value : '5',
	text : '龙'
}, 
{
	value : '6',
	text : '蛇'
}, 
{
	value : '7',
	text : '马'
}, 
{
	value : '8',
	text : '羊'
}, 
{
	value : '9',
	text : '猴'
}, 
{
	value : '10',
	text : '鸡'
}, 
{
	value : '11',
	text : '狗'
}, 
{
	value : '12',
	text : '猪'
}];


/**
 * 会员血型
 */
$.Member_Blood = 
[{
	value : '1',
	text : 'A型'
}, 
{
	value : '2',
	text : 'B型'
}, 
{
	value : '3',
	text : 'AB型'
}, 
{
	value : '4',
	text : 'O型'
}];

/**
 * 订单状态
 */
$.Order_Status = 
[{
	value : '1801',
	text : '成功'
}, 
{
	value : '1802',
	text : '已取消'
}, 
{
	value : '1803',
	text : '已付款'
}];

/**
 * 优惠劵实例状态
 */
$.Coupon_Instance_Status = 
[{
	value : '1601',
	text : '已创建'
}, 
{
	value : '1602',
	text : '未使用'
}, 
{
	value : '1603',
	text : '已使用'
}, 
{
	value : '1604',
	text : '已过期'
}, 
{
	value : '1605',
	text : '已作废'
}, 
{
	value : '1606',
	text : '使用中'
}, 
{
	value : '1607',
	text : '分享中'
}];

$.Coupon_Instance_Status_All = 
[{
	value : '0',
	text : '--请选择--',
	selected : true
},
{
	value : '1600',
	text : '已创建'
}, 
{
	value : '1601',
	text : '未使用'
}, 
{
	value : '1602',
	text : '已使用'
}, 
{
	value : '1603',
	text : '已过期'
}, 
{
	value : '1604',
	text : '已作废'
}, 
{
	value : '1605',
	text : '使用中'
}];

/**
 * 黑卡制卡状态
 */
$.Coupon_Card_Status_Issue = 
[{
	value : '1306',
	text : '可使用'
}, 
{
	value : '1307',
	text : '已使用'
}, 
{
	value : '1308',
	text : '未制卡'
},
{
	value : '1309',
	text : '已制卡'
}];


/**
 * 优惠劵类型
 */
$.Coupon_Type = 
[{
	value : '1',
	text : '折扣券'
}, 
{
	value : '2',
	text : '代金券'
}, 
{
	value : '3',
	text : '优惠券'
}];

/**
 * 会员状态
 */
$.Member_Status = 
[{
	value : '1101',
	text : '未激活'
}, 
{
	value : '1102',
	text : '激活'
}, 
{
	value : '1103',
	text : '锁定'
}, 
{
	value : '1104',
	text : '停用'
}, 
{
	value : '1105',
	text : '删除'
}];

/**
 * 会员分组类别
 */
$.Member_Group_Type = 
[{
	value : '1',
	text : '动态组'
}, 
{
	value : '2',
	text : '静态组'
}];

/**
 * 会员标签类别
 */
$.Member_Tag_Type = 
[{
	value : '1',
	text : '无查询条件'
}, 
{
	value : '2',
	text : '有查询条件'
}];

/**
 * 会员标签更新类型
 */
$.Member_Update_Tag_Type = 
[{
	value : '1',
	text : '手动'
}, 
{
	value : '2',
	text : '自动'
}];

/**
 * 会员标签分组状态
 */
$.Member_GT_Status = 
[{
	value : '2001',
	text : '已创建'
}, 
{
	value : '2002',
	text : '已使用'
},
{
	value : '2003',
	text : '已删除'
},
{
	value : '2004',
	text : '执行中'
}];

/**
 * 优惠券状态
 */
$.Coupon_Status = 
 [{
 	value : '1401',
 	text : '已创建'
 },
 {
	 value : '1402',
	 text : '已下发'
 },
 {
	 value : '1403',
	 text : '已过期'
 },
 {
	 value : '1404',
	 text : '已作废'
 },
 {
	 value : '1405',
	 text : '已删除'
 },
 {
	 value : '1406',
	 text : '发放中'
 }];
/**
 * 优惠券发放状态
 */
$.Coupon_Issue_Status = 
 [{
 	value : '1501',
 	text : '待推送'
 },
 {
	 value : '1502',
	 text : '已推送'
 },
 {
	 value : '1503',
	 text : '推送中'
 },{
	 value : '1504',
	 text : '未发放'
 }];

 
 

/**
 * 活动实例状态
 */
$.activity_instance_status = 
[{
	value : '2101',
	text : '已创建'
}, 
{
	value : '2102',
	text : '已启用'
}, 
{
	value : '2103',
	text : '已过期'
}, 
{
	value : '2104',
	text : '已终止'
}
];

/**
 * 等级变动方式
 */
$.Grade_Change_Type = 
 [{
 	value : '1',
 	text : '自动等级变化'
 },
 {
	 value : '2',
	 text : '手动等级变化'
 }];

/**
 * 升级规则
 */
$.Grade_Up_Rule = 
 [{
 	value : '1',
 	text : '按账户实时数量'
 },
 {
	 value : '2',
	 text : '按周期获取量'
 }];

/**
 * 降级规则1
 */
$.Grade_Down_Rule1 = 
 [{
 	value : '1',
 	text : '按账户实时数量'
 },
 {
	 value : '3',
	 text : '不降级'
 }];
/**
 * 降级规则2
 */
$.Grade_Down_Rule2 = 
	[{
		value : '2',
		text : '按周期获取量'
	},
	{
		value : '3',
		text : '不降级'
	}];

/**
 * 短信模板类型
 */
$.Sms_Temp_Type = 
	[{
		value : '1',
		text : '系统模板'
	}, 
	{
		value : '2',
		text : '活动模板'
	}];
/**
 * 短信模板状态
 */
$.Sms_Temp_Status = 
	[{
		value : '4101',
		text : '启用'
	}, 
	{
		value : '4102',
		text : '禁用'
	}];
/**
 * 活动日期类型
 */
$.Activity_Date_Type = 
	[{
		value : '1',
		text : '固定日期'
	}, 
	{
		value : '2',
		text : '时间段'
	}, 
	{
		value : '3',
		text : '滚动日期'
	}];
/**
 * 活动日期的滚动周期类型
 */
$.Activity_Date_Value_Type = 
	[{
		value : 'year',
		text : '年'
	}, 
	{
		value : 'mouth',
		text : '月'
	}, 
	{
		value : 'week',
		text : '星期'
	}];

/**
 * 积分规则-积分类型
 */
$.POINTS_TYPE = 
	[{
		value : '1',
		text : '消费积分'
	}, 
	{
		value : '2',
		text : '活动积分'
	}];
/**
 * 积分规则-条件参数类型
 */
$.POINTS_RULE_TYPE = 
	[{
		value : '1',
		text : '按活动次数，每一次触发'
	}, 
	{
		value : '2',
		text : '按活动金额，每一元金额'
	}, 
	{
		value : '3',
		text : '每次累积30'
	}];
/**
 * 积分规则-积分有效类型
 */
$.INVALID_TIME_TYPE = 
	[{
		value : '1',
		text : '滚动日期+固定日期'
	}, 
	{
		value : '2',
		text : '固定日期'
	}, 
	{
		value : '3',
		text : '永久生效'
	}, 
	{
		value : '4',
		text : '滚动日期'
	}];
/**
 * 积分规则-积分有效期值类型
 */
$.POINT_TIME_TYPE = 
	[{
		value : 'year',
		text : '年'
	}, 
	{
		value : 'month',
		text : '月'
	}, 
	{
		value : 'day',
		text : '日'
	}];

/**
 * 积分状态
 */
$.POINT_STATUS_ID = 
	[{
		value : '1701',
		text : '可使用'
	}, 
	{
		value : '1702',
		text : '不可使用'
	}, 
	{
		value : '1703',
		text : '已使用'
	}, 
	{
		value : '1704',
		text : '已过期'
	},
	{
		value : '1705',
		text : '已冻结'
	},
	{
		value : '1705',
		text : '已取消'
	}];

/**
 * 角色权限-操作
 */
$.SYSROLR_CHECKBOX = 
	[{
		value : '1',
		text : '查看'
	}, 
	{
		value : '2',
		text : '新建'
	}, 
	{
		value : '3',
		text : '编辑'
	}, 
	{
		value : '4',
		text : '删除'
	}];

//活动发放日期
$.ACTIVITY_SEND_DATE = 
	[{
		value : '1',
		text : '当日'
	}, 
	{
		value : '2',
		text : '所选日期前'
	}, 
	{
		value : '3',
		text : '所选日期月初'
	}];

/**
 * 模板消息状态
 */
$.WECHAT_TEMPLATE_MESSAGE_ITEM_STATUS_ID = 
	[{
		value : '1',
		text : '启用'
	}, 
	{
		value : '0',
		text : '停用'
	}];

/**
 * 消息管理-微信群发-分组模式
 */
$.GROUP_TYPE = 
	[{
		value : '1',
		text : '按分组发送'
	}, 
	{
		value : '2',
		text : '按标签发送'
	}, 
	{
		value : '3',
		text : '所有粉丝发送'
	}];

/**
 * 系统用户状态
 */
$.SYS_USER_STATUS = 
	[{
		value : '4001',
		text : '启用'
	}, 
	{
		value : '4002',
		text : '停用'
	}];
/**
 * 短信通道状态
 */
$.SMS_CHANNEL_IS_ACTIVE = 
	[{
		value : 'Y',
		text : '启用'
	}, 
	{
		value : 'N',
		text : '禁用'
	}];
/**
 * 短信实例发送状态
 */
$.SMS_INSTANCE_IS_SEND = 
	[{
		value : 'Y',
		text : '已发送'
	}, 
	{
		value : 'N',
		text : '未发送'
	}];

/**
 * 定时任务状态
 */
$.TASK_LAST_STATUS = 
	[{
		value : '1001',
		text : '未执行'
	}, 
	{
		value : '1002',
		text : '执行中'
	},
	{
		value : '1003',
		text : '执行成功'
	},
	{
		value : '1004',
		text : '执行失败'
	}, 
	{
		value : '1005',
		text : '任务过期'
	}];
/**
 * 商品展示状态
 */
$.GOODS_SHOW_STATUS = 
	[{
		value : '2202',
		text : '上架'
	},
	{
		value : '2203',
		text : '下架'
	}];
/**
 * 优惠券类别明细状态
 */
$.COUPON_CATEGORY_ITEM_STATUS = 
	[{
		value : '2402',
		text : '上架'
	},
	{
		value : '2403',
		text : '下架'
	}];
/**
 * 优惠券类别状态
 */
$.COUPON_CATEGORY_STATUS = 
	[{
		value : '2502',
		text : '上架'
	},
	{
		value : '2503',
		text : '下架'
	}];
/**
 * 定时任务类型
 */
$.IS_JOB = 
	[{
		value : 'Y',
		text : '工作中'
	}, 
	{
		value : 'N',
		text : '已停止'
	}];

/**
 * 定时任务类型
 */
$.TASK_TYPE = 
	[{
		value : '1',
		text : '系统线程'
	}, 
	{
		value : '2',
		text : '活动任务'
	}];

/**
 * 月份
 */
$.MONTH_TYPE = 
	[{
		value : '1',
		text : '1月'
	}, 
	{
		value : '2',
		text : '2月'
	},
	{
		value : '3',
		text : '3月'
	},
	{
		value : '4',
		text : '4月'
	},
	{
		value : '5',
		text : '5月'
	},
	{
		value : '6',
		text : '6月'
	},
	{
		value : '7',
		text : '7月'
	},
	{
		value : '8',
		text : '8月'
	},
	{
		value : '9',
		text : '9月'
	},
	{
		value : '10',
		text : '10月'
	},
	{
		value : '11',
		text : '11月'
	},
	{
		value : '12',
		text : '12月'
	}];

/**
 * 日期
 */
$.DAY_TYPE = 
	[{
		value : '1',
		text : '1日'
	}, 
	{
		value : '2',
		text : '2日'
	},
	{
		value : '3',
		text : '3日'
	},
	{
		value : '4',
		text : '4日'
	},
	{
		value : '5',
		text : '5日'
	},
	{
		value : '6',
		text : '6日'
	},
	{
		value : '7',
		text : '7日'
	},
	{
		value : '8',
		text : '8日'
	},
	{
		value : '9',
		text : '9日'
	},
	{
		value : '10',
		text : '10日'
	},
	{
		value : '11',
		text : '11日'
	},
	{
		value : '12',
		text : '12日'
	},
	{
		value : '13',
		text : '13日'
	},
	{
		value : '14',
		text : '14日'
	},
	{
		value : '15',
		text : '15日'
	},
	{
		value : '16',
		text : '16日'
	},
	{
		value : '17',
		text : '17日'
	},
	{
		value : '18',
		text : '18日'
	},
	{
		value : '19',
		text : '19日'
	},
	{
		value : '20',
		text : '20日'
	},
	{
		value : '21',
		text : '21日'
	},
	{
		value : '22',
		text : '22日'
	},
	{
		value : '23',
		text : '23日'
	},
	{
		value : '24',
		text : '24日'
	},
	{
		value : '25',
		text : '25日'
	},
	{
		value : '26',
		text : '26日'
	},
	{
		value : '27',
		text : '27日'
	},
	{
		value : '28',
		text : '28日'
	},
	{
		value : '29',
		text : '29日'
	},{
		value : '30',
		text : '30日'
	},
	{
		value : '31',
		text : '31日'
	}];
/**
 * 会员卡样式
 */
$.MEMBER_CARD_STYLE_ID = 
	[{
		value : '1',
		text : '原始会员卡'
	}, 
	{
		value : '2',
		text : '条形码'
	}, 
	{
		value : '3',
		text : '二维码'
	}];
/**
 * 系统日志类型
 */
$.LOG_TYPE =
	[{
		value : '1',
		text : '操作日志'
	},{
		value : '2',
		text : '系统日志'
	},{
		value : '3',
		text : '接口日志'
	},{
		value : '4',
		text : '任务日志'
	}];
/**
 * 系统日志事件
 */
$.LOG_EVENT =
	[{
		value : '1',
		text : '登录'
	},{
		value : '2',
		text : '注销'
	},{
		value : '3',
		text : '新增'
	},{
		value : '4',
		text : '修改'
	},{
		value : '5',
		text : '删除'
	},{
		value : '6',
		text : '启用'
	},{
		value : '7',
		text : '停用'
	},{
		value : '8',
		text : '下发'
	},{
		value : '9',
		text : '导入'
	}];

$.Qrcode_type =
	[
		{
			value : '1',
			text : '微信关注'
		},{
			value : '2',
			text : '网页'
		}
	 	
    ];

$.Qrcode_channel =
	[
	 {
		 value : '1',
		 text : '户外广告'
	 },{
		 value : '2',
		 text : '微信'
	 },{
		 value : '3',
		 text : '门店'
	 },{
		 value : '4',
		 text : '卓贴'
	 }
	 
	 
	 ];

$.topicType = 
	[{
		value : '1',
		text : '选择题'
	}, 
	{
		value : '2',
		text : '打分题'
	}, 
	{
		value : '3',
		text : '问答题'
	}];

$.updateNode = 
	[{
		value : '1',
		text : '注册'
	}, 
	{
		value : '2',
		text : '信息修改'
	}, 
	{
		value : '3',
		text : '订单同步'
	}];
/**
 * 二维码类别
 */
$.Qrcode_type =
	[
		{
			value : '1',
			text : '微信关注'
		},{
			value : '2',
			text : '网页'
		}
	 	
    ];
/**
 * 是否会员
 */
$.IS_MEMBER = 
	[{
		value : 'Y',
		text : '是'
	}, 
	{
		value : 'N',
		text : '否'
	}];
/**
 * 服务单状态
 */
$.Complain_Status = 
	[{
		value : '7001',
		text : '未读'
	}, 
	{
		value : '7002',
		text : '已读'
	},
	{
		value : '7003',
		text : '已解决'
	}];

/**
 * 优惠券转发状态
 */
$.Relay_Status = 
	[{
		value : '1',
		text : '转发中'
	}, 
	{
		value : '2',
		text : '已转发'
	},
	{
		value : '3',
		text : '已退回'
	}];

/**
 * 活动状态
 */
$.VAQ_Status = 
	[{
		value : '1902',
		text : '已启用'
	},
	{
		value : '1905',
		text : '已停用'
	}
	];
