define(['common'], function(common) {
	common.Page.resetMain();
	
	$(function() {

	//	common.Page.loadCenter('coupon/couponManage.html');
		
		//alert($.baseUrl+"loginController/menu.do");
		/*$.ajax({ 
	        type: "get", 
	        url: encodeURI($.baseUrl+"/loginController/menu.do"), 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$("#sysSuer").html(data.sysUser.userName+",你好！");
	        	$("#menu_ul").html(data.menu);
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        }
	    });*/
		
		$(window).resize(function() {
			common.Page.resetMain();
		});
		
		$("[name='sys-li']").click(function(){
			common.Page.loadCenter('sys/sysManage.html');
			$("#menu_ul ol").each(function(){
				$(this).hide();
			});
		});
		
		/**
		 * 主菜单展开
		 */
		$('.common_menu>ul>li').click(function() {
			if ($(this).next().is('li')) {
				return false;
			}
			if ($(this).next().is(':hidden')) {
				$(this).next().show();
				common.Page.resetMain();
				$(this).next().hide();
				$(this).next().slideDown(300, function() {
					common.Page.resetMain();
				}).siblings('ol:visible').slideUp(300);
			} else {
				$(this).next().slideUp(300, function() {
					common.Page.resetMain();
				});
			}
		});
		/*
		 * 展开收起查询
		 */
		$('body').delegate('.common_content_scach_more span', 'click' ,function() {
			var isValue = $(this).is('.down');
			if (isValue) {
				$(this).removeClass('down').addClass('up');
			} else {
				$(this).removeClass('up').addClass('down');
			}
			$(this).closest('.seach_form').find('.ul_more').toggle(isValue);
			common.Page.resetMain();
		});
		
		/*
		 * 主菜单跳转
		 */
		$('.common_menu>ul>ol>li').click(function(){
			$('.common_menu>ul>ol>li').removeClass('active')
			common.Page.loadCenter($(this).addClass('active').data('url'));
		});
		
		reloadDashBoard(function(){
			$("#add_dashboard").click(function(){
				common.Page.loadMask('sys/dashboardAdd.html');
			})
		});
		
	});
});

function loginOut() {
	$.messager.confirm('系统提示', '确定要退出登录吗?', function(r){
        if (r){
        	location.href=$.baseUrl+'/loginout.do';
        }
    });
}

//下拉框初始化
function initSelect(){
	$('.dropdown-menu').each(function(i,v){
	var _pp = $(this);
	var s=$.trim($(this).attr("data"));
	var url_options = $.trim($(this).attr("url-options"));
	var $this = _pp.siblings("input[input-type='select']");
	if($this.val()==""){
		$this.val("-请选择-");
	}
	if(s){
		//_pp.html("");
		eval("var data = "+s);
		$.each(data,function(i,v){
			_pp.append("<li data-id='"+v.value+"'><a href='javascript:;'>"+v.text+"</a></li>");
		});
	}else if(url_options){
		//_pp.html("");
		eval("var urlData = {"+url_options+"}");
		var url = urlData.url;
		var value = urlData.value;
		var text = urlData.text;
		$.ajax({ 
	        type: "get", 
	        url: encodeURI($.baseUrl+url), 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$.each(data, function(i, v){
	        		eval("var aa = v."+value+"");
	        		eval("var bb = v."+text+"");
	        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
			    });
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        }
	    });
	}
});
}

function initSelectDetailed(){
	$(".detailed").find('.dropdown-menu').each(function(i,v){
	var _pp = $(this);
	var s=$.trim($(this).attr("data"));
	var url_options = $.trim($(this).attr("url-options"));
	
	if(s){
		//_pp.html("");
		eval("var data = "+s);
		$.each(data,function(i,v){
			_pp.append("<li data-id='"+v.value+"'><a href='javascript:;'>"+v.text+"</a></li>");
		});
	}else if(url_options){
		//_pp.html("");
		eval("var urlData = {"+url_options+"}");
		var url = urlData.url;
		var value = urlData.value;
		var text = urlData.text;
		$.ajax({ 
	        type: "get", 
	        url: encodeURI($.baseUrl+url), 
	        dataType: "json",
	        async: false,
	        success: function (data) {
	        	$.each(data, function(i, v){
	        		eval("var aa = v."+value+"");
	        		eval("var bb = v."+text+"");
	        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
			    });
	        }, 
	        error: function (XMLHttpRequest, textStatus, errorThrown) { 
	            alert(errorThrown); 
	        }
	    });
	}
});
}

//初始化下拉框
function initSelectById(id,selectUrl,valueFiled,textFiled){
	var _pp = $('#'+id).next("ul");
	if(!_pp){
		return;
	}
	var url = selectUrl;
	var value = valueFiled;
	var text = textFiled;
	//_pp.html("");
	$.ajax({ 
        type: "get", 
        url: encodeURI(baseUrl+url),
        dataType: "json",
        async: false,
        success: function (data) {
        	$.each(data, function(i, v){
        		eval("var aa = v."+value+"");
        		eval("var bb = v."+text+"");
        		_pp.append("<li data-id='"+aa+"'><a href='javascript:;'>"+bb+"</a></li>");
		    });
        }, 
        error: function (XMLHttpRequest, textStatus, errorThrown) { 
            alert(errorThrown); 
        }
    });
}

//日期控件初始化
function initDate(){
	$(".form_datetime").datetimepicker({
		format: 'yyyy-mm-dd',
		language: 'zh-CN',
		weekStart: 1,
		todayBtn: 1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
	});
}

//文本框验证
function initInput(){
	$('.form-control').each(function(i,v){
		  $(this).blur(function(){
				var _pp = $(this);
				 _pp.tooltip('destroy');
				var value = _pp.val();
				//$("#txtEbauthor").val().replace(/[^\u0000-\u00ff]/g,"aa").length
				var valLength = value.replace(/[^\u0000-\u00ff]/g,"aa").length;
				var data_options =$.trim($(this).attr("data-options"));
				if(data_options){
					eval("var data = {"+data_options+"}");
					var required = data.required;
					var length = data.length;
					var validType = data.validType;
					var rangeLength = data.rangeLength;
					if(required && valLength==0){
						/*
						 _pp.tooltip({
							position: 'right',    
							content: '<span name="tooltipspan" style="color:red">不能为空</span>'
						});
						_pp.tooltip('show');
						_pp.attr("check",false);
						*/
						_pp.alert("不能为空");
						_pp.attr("check",false);
						return;
					}
					
					if(length && valLength>length){
						
						/*
						_pp.tooltip({
							position: 'right',    
							content: '<span name="tooltipspan" style="color:red">最大长度为'+length+'</span>'
						});
						_pp.tooltip('show');
						*/
						_pp.alert('最大长度为'+length);
						return;
					}
					
					if(rangeLength && (valLength<rangeLength[0] || valLength>rangeLength[1])){
						
						_pp.alert('密码长度要在'+rangeLength[0]+'-'+rangeLength[1]+'之间');
						return;
						
					}
					
					if(valLength != 0){
								if(validType){
						if(validType=='mobile'){
							if(!checkMobile(value)){
								/*
								_pp.tooltip({
									position: 'right',    
									content: '<span name="tooltipspan" style="color:red">请输入合法手机号</span>'
								});
								_pp.tooltip('show');
								*/
								_pp.alert('请输入合法手机号');
								return;
							}
						}
						//验证输入内容是否是合法手机号 or 固定电话
						if(validType=='contact'){
							if(!checkMobile(value) && !istell(value)){
								_pp.alert('请输入合法的联系方式(固话请加区号)');
								return;
							}
						}
						
						if(validType=='idCardNo'){
							if(!isidcard(value)){
								/*
								_pp.tooltip({
									position: 'right',    
									content: '<span name="tooltipspan" style="color:red">请输入合法身份证</span>'
								});
								_pp.tooltip('show');
								*/
								_pp.alert('请输入合法身份证');
								return;
							}
						}
						
						if(validType=='email'){
							if(!isemail(value)){
								/*
								_pp.tooltip({
									position: 'right',    
									content: '<span name="tooltipspan" style="color:red">请输入合法邮箱</span>'
								});
								_pp.tooltip('show');
								*/
								_pp.alert('请输入合法邮箱');
								return;
							}
						}
						
						if(validType=='number'){
							if(!checkNumber(value)){
								/*
								_pp.tooltip({
									position: 'right',    
									content: '<span name="tooltipspan" style="color:red">请输入合法数字</span>'
								});
								_pp.tooltip('show');
								*/
								_pp.alert('请输入合法数字');
								return;
							}
						}
						//数字(不含正负号)
						if(validType=='positiveNumber'){
							if(!checkPositiveNumber(value)){
								_pp.alert('请输入合法数字(不可包含正负号)');
								return;
							}
						}
						//正整数(不含0)
						if(validType=='positiveInteger'){
							if(!checkPositiveInteger(value)){
								_pp.alert('请输入正整数');
								return;
							}
						}
						
						if(validType=='letAndNum'){
							if(!letAndNum(value)){
								_pp.alert('请输入合法数据(只能包含数字或字母)');
								return;
							}
						}
						
						if(validType=='ishundred'){
							if(!ishundred(value)){
								_pp.focus();
								_pp.alert('请输入1-100之间的数字');
								flag = false;
								return;
							}
						}
						if(validType=='rtZero'){
							if(!rtZero(value)){
								_pp.focus();
								_pp.alert('请输入大于等于0的整数');
								flag = false;
								return;
							}
						}
						if(validType=='rtNZero'){
							if(!rtNZero(value)){
								_pp.focus();
								_pp.alert('请输入大于0的整数');
								flag = false;
								return;
							}
						}
					}
					}
			
			}
	    })
	});
}

function formCheck(obj){
	var flag = true;
	obj.find('.form-control').each(function(i,v){
		var _pp = $(this);
		 _pp.tooltip('destroy');
		var value = _pp.val();
		var valLength = value.replace(/[^\u0000-\u00ff]/g,"aa").length;
		var data_options =$.trim($(this).attr("data-options"));
		if(data_options){
			eval("var data = {"+data_options+"}");
			var required = data.required;
			var length = data.length;
			var validType = data.validType;
			var rangeLength = data.rangeLength;
			if(required && valLength==0){
				/*
				 _pp.tooltip({
					position: 'right',    
					content: '<span name="tooltipspan" style="color:red">不能为空</span>'
				});
				_pp.tooltip('show');
				*/
				_pp.focus();
				_pp.alert('不能为空');
				flag = false;
				return;
			}
			if(length && valLength>length){
				/*
				_pp.tooltip({
					position: 'right',    
					content: '<span name="tooltipspan" style="color:red">最大长度为'+length+'</span>'
				});
				_pp.tooltip('show');
				*/
				_pp.focus();
				_pp.alert('最大长度为'+length);
				flag = false;
				return;
			}
			if(rangeLength && (valLength<rangeLength[0] || valLength>rangeLength[1])){
						
				_pp.focus();
				_pp.alert('密码长度要在'+rangeLength[0]+'-'+rangeLength[1]+'之间');
				flag = false;
				return;
						
			}
			if(valLength != 0){
				if(validType){
					if(validType=='mobile'){
						if(!checkMobile(value)){
							/*
							_pp.tooltip({
								position: 'right',    
								content: '<span name="tooltipspan" style="color:red">请输入合法手机号</span>'
							});
							_pp.tooltip('show');
							*/
							_pp.focus();
							_pp.alert('请输入合法手机号');
							flag = false;
							return;
						}
					}
					
					//验证输入内容是否是合法手机号 or 固定电话
					if(validType=='contact'){
						if(!checkMobile(value) && !istell(value)){
							_pp.focus();
							_pp.alert('请输入合法的联系方式(固话请加区号)');
							flag = false;
							return;
						}
					}
					
					if(validType=='idCardNo'){
						if(!isidcard(value)){
							/*
							_pp.tooltip({
								position: 'right',    
								content: '<span name="tooltipspan" style="color:red">请输入合法身份证</span>'
							});
							_pp.tooltip('show');
							*/
							_pp.focus();
							_pp.alert('请输入合法身份证');
							flag = false;
							return;
						}
					}
					
					if(validType=='email'){
						if(!isemail(value)){
							/*
							_pp.tooltip({
								position: 'right',    
								content: '<span name="tooltipspan" style="color:red">请输入合法邮箱</span>'
							});
							_pp.tooltip('show');
							*/
							_pp.focus();
							_pp.alert('请输入合法邮箱');
							flag = false;
							return;
						}
					}
					
					if(validType=='number'){
						if(!checkNumber(value)){
							/*
							_pp.tooltip({
								position: 'right',    
								content: '<span name="tooltipspan" style="color:red">请输入合法数字</span>'
							});
							_pp.tooltip('show');
							*/
							_pp.focus();
							_pp.alert('请输入合法数字');
							flag = false;
							return;
						}
					}
					if(validType=='positiveNumber'){
						if(!checkPositiveNumber(value)){
							_pp.focus();
							_pp.alert('请输入合法数字(不可包含正负号)');
							flag = false;
							return;
						}
					}
					if(validType=='positiveInteger'){
						if(!checkPositiveInteger(value)){
							_pp.focus();
							_pp.alert('请输入合法数字(不可包含正负号)');
							flag = false;
							return;
						}
					}
					if(validType=='ishundred'){
						if(!ishundred(value)){
							_pp.focus();
							_pp.alert('请输入1-100之间的数字');
							flag = false;
							return;
						}
					}
					if(validType=='rtZero'){
						if(!rtZero(value)){
							_pp.focus();
							_pp.alert('请输入大于等于0的整数');
							flag = false;
							return;
						}
					}
					if(validType=='rtNZero'){
						if(!rtNZero(value)){
							_pp.focus();
							_pp.alert('请输入大于0的整数');
							flag = false;
							return;
						}
					}
				}
			}
		}
	});
	return flag;
}


//表单禁用||启用  isDisabled值true or false
function disableForm(formId,isDisabled){
	
    $("form[id='"+formId+"'] :text").attr("disabled",isDisabled);  
    $("form[id='"+formId+"'] textarea").attr("disabled",isDisabled);  
    $("form[id='"+formId+"'] :radio").attr("disabled",isDisabled);  
    $("form[id='"+formId+"'] :checkbox").attr("disabled",isDisabled); 
    $("form[id='"+formId+"'] :password").attr("disabled",isDisabled); 
    
    $("form[id='"+formId+"'] :text").attr("style","background:#fff;");  
    $("form[id='"+formId+"'] textarea").attr("style","background:#fff;");  
    $("form[id='"+formId+"'] :radio").attr("style","background:#fff;");  
    $("form[id='"+formId+"'] :checkbox").attr("style","background:#fff;"); 
    $("form[id='"+formId+"'] :password").attr("style","background:#fff;"); 
    //禁用jquery easyui中的下拉框 
    $("#" + formId + " input[input-type='select']").each(function () {  
        if (this.id) {
        	if(isDisabled){
        		$("#" + this.id).next().next().hide();
        	}else{
        		$("#" + this.id).next().next().show();
        	}
        }  
    });  
    
    //禁用jquery easyui中的日期组件dataBox  
    /*$("#" + formId + " input[class='form-control form_datetime']").each(function () {  
        if (this.id) {  
            $("#" + this.id).datebox(attr);  
        }  
    }); */
}

//清空区域门店
function clearInput(obj){
	if($(obj).prev()){
		$(obj).prev().val("");
	}
	if($(obj).prev().prev()){
		$(obj).prev().prev().val("");
	}
}

function showSummary(){
	var today = new Date();
	var myDate = new Date();
	myDate.setDate(today.getDate()-1);
	var memberDay = myDate.toLocaleDateString();
	$("#memberDay").text(memberDay);
	$("#memberAll").text(memberDay);
	$("#couponDay").text(memberDay);
	myDate.setDate(today.getDate()-7);
	var memberWeek = myDate.toLocaleDateString();
	$("#memberWeek").text(memberWeek+"~"+memberDay);
	var monthStartDate = new Date(today.getFullYear(), today.getMonth()-1, 1); 
	var monthEndDate = new Date(today.getFullYear(), today.getMonth()-1, (new Date(today.getYear(), today.getMonth(), 1) - new Date(today.getYear(), today.getMonth()-1, 1))/(1000 * 60 * 60 * 24));
	$("#memberMonth").text(monthStartDate.toLocaleDateString()+"~"+monthEndDate.toLocaleDateString());
	$("#couponMonth").text(monthStartDate.toLocaleDateString()+"~"+monthEndDate.toLocaleDateString());
	$.ajax({
		type: "post", 
        url: $.baseUrl+"/memberController/queryMemberSummary", 
        dataType: "json",
        success : function(data){
        	$("#memberDayNew").text(data.memberDay ==null?"0":data.memberDay);
        	$("#memberDayChange").text(data.memberDayChange == null?"+0":data.memberDayChange);
        	$("#memberWeekNew").text(data.memberWeek ==null?"0":data.memberWeek);
        	$("#memberWeekChange").text(data.memberWeekChange == null?"+0":data.memberWeekChange);
        	$("#memberMonthNew").text(data.memberMonth ==null?"0":data.memberMonth);
        	$("#memberMonthChange").text(data.memberMonthchange == null?"+0":data.memberMonthchange);
        	$("#memberAllNew").text(data.allNewMember ==null?"0":data.allNewMember);
        	$("#memberAllChange").text(data.memberAllChange == null?"+0":data.memberAllChange);
        	
        	$("#couponDayNew").text(data.couponDay == null ? "0":data.couponDay);
        	$("#couponDayChange").text(data.couponDayChange == null?"+0":data.couponDayChange);
        	$("#couponMonthNew").text(data.couponMonth == null ? "0":data.couponMonth);
        	$("#couponMonthChange").text(data.couponMonthchange == null?"+0":data.couponMonthchange);
        },error : function(){
        	$("#memberDayNew").text("0");
        	$("#memberDayChange").text("0");
        	$("#memberWeekNew").text("0");
        	$("#memberWeekChange").text("0");
        	$("#memberMonthNew").text("0");
        	$("#memberMonthChange").text("0");
        	$("#memberAllNew").text("0");
        	$("#memberAllChange").text("0");
        	
        	$("#couponDayNew").text("0");
        	$("#couponDayChange").text("0");
        	$("#couponMonthNew").text("0");
        	$("#couponMonthChange").text("0");
        }
	})
}

var selectDash = [];

function reloadDashBoard(after){
	$("#dashShow").empty();
	selectDash = [];
	$.ajax({
		type : "post",
		url : $.baseUrl + "/sysUserController/showIndexCharts",
		dataType : "json",
		success : function(data){
			var list = data.result;
			var html = "";
			for(var i = 0;i < list.length;i ++){
				selectDash.push(list[i].chartId);
				if(list[i].chartId == 1){
					html += "<div>" +
							"<p class='title'>昨日新增会员</p>" +
							"<div class='count-panel'>" +
							"<p class='count' id='memberDayNew'></p>" +
							"<p class='change' id='memberDayChange'></p>" +
							"</div>" +
							"<p class='date' id='memberDay'></p>" +
							"</div>";
				}
				if(list[i].chartId == 2){
					html += "<div>" +
							"<p class='title'>过去7天新增会员</p>" +
							"<div class='count-panel'>" +
							"<p class='count' id='memberWeekNew'></p>" +
							"<p class='change' id='memberWeekChange'></p>" +
							"</div>" +
							"<p class='date' id='memberWeek'></p>" +
							"</div>";
				}
				if(list[i].chartId == 3){
					html += "<div>" +
							"<p class='title'>上月新增会员</p>" +
							"<div class='count-panel'>" +
							"<p class='count' id='memberMonthNew'>123</p>" +
							"<p class='change' id='memberMonthChange'></p>" +
							"</div>" +
							"<p class='date' id='memberMonth'></p>" +
							"</div>";
				}
				if(list[i].chartId == 4){
					html += "<div>" +
							"<p class='title'>至昨天为止所有会员</p>" +
							"<div class='count-panel'>" +
							"<p class='count' id='memberAllNew'></p>" +
							"<p class='change' id='memberAllChange'></p>" +
							"</div>" +
							"<p class='date' id='memberAll'></p>" +
							"</div>";
				}
				if(list[i].chartId == 5){
					html += "<div>" +
							"<p class='title'>昨日优惠券发放数</p>" +
							"<div class='count-panel'>" +
							"<p class='count' id='couponDayNew'></p>" +
							"<p class='change' id='couponDayChange'></p>" +
							"</div>" +
							"<p class='date'  id='couponDay'></p>" +
							"</div>";
				}
				if(list[i].chartId == 6){
					html += "<div>" +
							"<p class='title'>上月优惠券发放数</p>" +
							"<div class='count-panel'>" +
							"<p class='count' id='couponMonthNew'></p>" +
							"<p class='change' id='couponMonthChange'></p>" +
							"</div>" +
							"<p class='date' id='couponMonth'></p>" +
							"</div>";
				}
			}
			html += "<div>" +
					"<li id='add_dashboard' draggable='false' data-index='8' data-fid='' title='点击添加新的仪表盘'>" +
					"<a draggable='false'>" +
					"<span class='thumb' style='background-position: center center;'></span></a>" +
					"</li>" +
					"</div>";
			$("#dashShow").append(html);
			showSummary();
			after();
		}
	});
}


