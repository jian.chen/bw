define(['common'], function(common) {
	function init(){
		common.Page.resetMain();
	}

	return {
		init: init
	}
});