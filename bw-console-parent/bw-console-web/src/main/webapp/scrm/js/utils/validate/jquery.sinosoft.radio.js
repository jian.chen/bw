(function($){  
	
	$.sinosoft = $.sinosoft || {version: '@VERSION'};
	
	var tool = $.sinosoft.radio = {
			conf: {
				name:'',//单选框名称，分组使用
				enable:true, //是否启用
				enableGroup:true, //是否启用组
				check:false, //是否初始选中
				trigger: false,
				value:'', //input值
				css:{
					radio:'radio_main',
					btn:'radio_btn',
					checked:'checked',
					disable:'disabled'
				},
				skin:'normal',
				onSelect:function(){}
			},
			name:'jRadio',
			num:0,
			skinFloder : 'radio_skin',
			jsPath : getJsPath('jquery.sinosoft.radio.js'),
			skinList : [
				{name:"normal",charset:"gbk"},
				{name:"quote",charset:"gbk"},
				{name:"checkbox",charset:"gbk"}
			]
	};
	
	function getJsPath(name){
		var path = '';
		var js=document.getElementsByTagName("script");
		for(var i=js.length;i>0;i--){
			if(js[i-1].src.indexOf(name)>-1){
				path=js[i-1].src.substring(0,js[i-1].src.lastIndexOf("/")+1);
			}
		}
		return path;
	}
	
	function addstylesheet(name,charset,path){
		var head = document.getElementsByTagName('HEAD');
			links = head[0].getElementsByTagName('link');
		var link = document.createElement("link");
		var href = tool.skinFloder+'/'+name;
		
		for(var i=0 ; i < links.length ; i++){
			if(links[i].href.indexOf(href) != -1){
				return;
			}
		}
		
		link.href = path+href;
		link.rel = "stylesheet";
		link.type = "text/css";
		link.charset = charset;
		head[0].appendChild(link);
	}
	
	function getSkin(skinName){
		for(var i=0 ; i < tool.skinList.length ; i++){
			if(tool.skinList[i].name == skinName ){
				return tool.skinList[i];
			}
		}
		return undefined;
	}
	
	var padding = ['padding-left','padding-top','padding-right','padding-bottom'],
		margin = ['margin-left','margin-top','margin-right','margin-bottom'];
	
	function getCss(style,src,obj){
		for ( var i = 0; i < style.length; i++) {
			obj[getUppercase(style[i])] = src.css(style[i]);
		}
	}

	function getUppercase(str){
		var index = 0;
		while((index = str.indexOf('-')) != -1){
			var char = str.charAt(index+1);
			
			var _str1 = str.substring(0,index);
			var _str2 = str.substr(index+2);
			str = _str1 + char.toUpperCase() + _str2;
		}
		return str;
	}
	
	function cloneStyle(src,dst){
		var style = {
				float:src.css('float')
		};
		getCss(margin,src,style);
		getCss(padding,src,style);
		
		dst.css(style);
	}
	
	function JRadio(input ,conf){
		var enable = conf.enable,
			checked = conf.checked,
			self = this,
			css = conf.css,
			root = $("<div><div></div></div>").data($.sinosoft.radio.name, self);
		
		var skin = getSkin(conf.skin);
		
		if(skin){
			addstylesheet(skin.name+'/radio.css',skin.charset,tool.jsPath);
		}
		
		var select = root.addClass(css.radio).addClass(skin.name).find('div').addClass(css.btn);
		
		input.before(root);
		
		cloneStyle(input,root);
		input.hide();
		firstInit();
		
		var height = input.height();
  		
		root.click(function(){
			if(enable){
				radioCheck();
			}
		});
		
		function firstInit(){
			if(conf.check){
				select.addClass(css.checked);
				checked = true;
				input.attr('checked','checked');
			}else if(input.attr('checked')){
				select.addClass(css.checked);
				checked = true;
				input.attr('checked','checked');
			}
			if(!enable){
				root.addClass(css.disable);
			}
		}
		
		function radioCheck(group){
			
			if(!select.hasClass(css.checked)){
				for(var i = 0 ; i<$.sinosoft.radio[conf.name].length ; i++ ){
					$.sinosoft.radio[conf.name][i].uncheck();
				}
				
				checked = true;
				select.addClass(css.checked);
				
				input.attr('checked','checked');
				if(conf.onSelect){
					conf.onSelect(self,checked,group);
				}
			}else{
				if(conf.trigger){
					radioUnCheck();
					if(conf.onSelect){
						conf.onSelect(self,checked,group);
					}
				}
			}
		}
		
		function radioUnCheck(){
			select.removeClass(css.checked);
			checked = false;
			input.removeAttr('checked');
		}
		
		$.extend(self, {  
			enable: function(en){
				if((typeof en == 'undefined')){
					return enable;
				}
				
				enable = en;
				
				if(en){
					root.removeClass(css.disable);
				}else{
					root.addClass(css.disable);
				}
			},
			check: function(group){
				radioCheck(group);
			},
			uncheck: function(){
				radioUnCheck();
			},
			checked: function(){
				return checked;
			},
			getValue: function(){
				return input.val();
			},
			getInput: function(){
				return input;
			},
			setValue: function(value){
				if(self.getValue() == value){
					self.check();
				}
			},
			getRoot:function(){
				return root;
			}
		});
	}
	// jQuery plugin implementation
	$.fn.jRadio = function(conf) {
		// already installed
		if (this.data(tool.name)) { return this; } 
		// extend configuration with globals
		conf = $.extend(true, {}, tool.conf, conf);		
		
		var els = [];
		this.each(function() {		
			var name = $(this).attr('name') ? $(this).attr('name') : conf.name;
			
			if(!name){
				name = tool.name + tool.num;
				tool.num ++;
			}
			if(!$.sinosoft.radio[name]){
				$.sinosoft.radio[name] = [];
			}
			
			var el = new JRadio($(this), $.extend(true, {}, conf,{name:name}));
			el.getInput().data(tool.name, el);
			$.sinosoft.radio[name].push(el);
			
			els.push(el);
		});		
		
		return els;
	};
})(jQuery);