/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		/*{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },*/
		{ name: 'insert' },
		{ name: 'tools' },
		/*{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },*/
		/*{ name: 'links' },*/
		/*{ name: 'forms' },*/
		/*{ name: 'others' },
		'/',*/
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		/*{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },*/
		{ name: 'styles'},
		{ name: 'colors' }
	]
	config.image_previewText=' '; //预览区域显示内容
	//上传图片地址
	config.filebrowserImageUploadUrl = $.baseUrl+"/memberMessageController/uploadMessageImages.do";
	//config.pasteFromWordIgnoreFontFace = true; //默认为忽略格式
	/*config.pasteFromWordRemoveFontStyles = false;
	config.pasteFromWordRemoveStyles = false;*/
	config.allowedContent = true;
	config.format_p = { element: 'p', attributes: { 'class': 'normalPara' } };
	
};
