$(document).ready(function(){
	var pathName=window.document.location.pathname;
	var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
	$.baseUrl = projectName;//初始化项目根路径
	var userId = getURLParameter('userId');
	
	$.ajax({
		type : "get",
		url : $.baseUrl+'/shakeController/extractMemberAwards.do',
		data : "userId="+userId,
		dataType : "json",
		async: false,
		success : function(result) {
			if(result.status =='success'){
				$("#prize_level").html(getPrizeName(result.awardId));
			}
		},
		error : function() {
			
		}
	});
	
});

function getPrizeName(id){
	var prizeIds=[1,2,3,4];
	var prizeNames=['一等奖','二等奖','三等奖','参与奖'];
	var prizeId = prizeIds.indexOf(id);
	return prizeNames[prizeId];
}


function getURLParameter(name) {
	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}