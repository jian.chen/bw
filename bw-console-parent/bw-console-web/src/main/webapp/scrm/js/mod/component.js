define(['jquery'], function($) {
	$.fn.extend({
		//给一个checkbox绑定监听一组select是否选中
		listenAll: function($selecter) {
			var _this = this;
			_this.click(function() {
				$($selecter).prop("checked", $(_this).prop("checked"));
			});
			$($selecter).click(function() {
				_this.prop("checked", $($selecter + ':checked').length == $($selecter).length);
			});
		},
		bindTab : function($selecter){
			this.find('>*').each(function(i){
				$(this).click(function(){
					$($selecter).find('>*').hide().eq(i).show();
					$(this).addClass('active').siblings().removeClass('active');
				});
			});
		}
	});
	$(function() {
		/*
		 * 展开收起查询
		 */
		$('.common_content_scach_more span').click(function() {
			var isValue = $(this).is('.down');
			if (isValue) {
				$(this).removeClass('down').addClass('up');
			} else {
				$(this).removeClass('up').addClass('down');
			}
			$(this).closest('.common_content_scach').find('.ul_more').toggle(isValue);
		});
		/**
		 * 关闭标签
		 */
		$('.detailed_close, .detailed_close2').click(function() {
			$(this).closest('.mask, .mask_in').hide();
			return false;
		});
		/*
		 * select
		 */
		$('body').delegate('.dropdown-menu>li', 'click', function() {
			$(this).parent().prev().val($(this).find('a').html());
			$(this).parent().prev().prev().val($(this).data('id'));
		});
		$('body').delegate('[input-type=select]', 'click', function() {
			$(this).parent().find('.dropdown-menu').show();
		});
		$('body').delegate('[input-type=select]', 'blur', function() {
			var _this = this;
			setTimeout(function(){
				$(_this).parent().find('.dropdown-menu').hide();
			},100);
		});
		/**
		 * 主菜单展开
		 */
		$('.common_menu>ul>li').click(function(){
			if($(this).next().is('li')){
				return false;
			}
			if($(this).next().is(':hidden')){
				$(this).next().slideDown(300);
			}else{
				$(this).next().slideUp(300);
			}
		});
	});

});