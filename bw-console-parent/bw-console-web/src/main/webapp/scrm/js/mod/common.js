define([], function() {
	$.fn.extend({
		bindTab: function($selecter) {
			this.find('>*').each(function(i) {
				$(this).click(function() {
					$($selecter).find('>*').hide().eq(i).show().find('.easyui-datagrid').datagrid('resize');
					$(this).addClass('active').siblings().removeClass('active');
				});
			});
		},
		alert: function(msg, width){
			var value = $(this).offset();
			var $dom = $('<div class="prompt"><div class="alert alert-danger alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button>' +
			msg + '</div><span class="n-arrow"><b>◆</b><i>◆</i></span></div>');
			$dom.attr('top', value.top + 35).find('.close').click(function() {
				$dom.remove();
			});
			$('body').append($dom);
			$dom.css({
				position: 'fixed',
				left: value.left,
				top: value.top + 35 - $(window).scrollTop(),
				width: width || $(this).width() + 100
			});
			setTimeout(function() {
				$dom.remove();
			}, 3000);
		}
	});
	$(function() {
		/*
		 * select
		 */
		$('body').delegate('.dropdown-menu>li', 'click', function() {
			$(this).parent().prev().val($(this).find('a').html());
			$(this).parent().prev().prev().val($(this).data('id'));
		});
		$('body').delegate('[input-type=select], .input_down', 'click', function() {
			var value = $(this).offset();

			$(this).parent().find('.dropdown-menu').show().css({
				position: 'fixed',
				left: value.left,
				top: value.top + 35 - $(window).scrollTop(),
				width: $(this)[0].offsetWidth
			});
		});
		$(window).scroll(function() {
			var top = $(window).scrollTop();
			$('.dropdown-menu').each(function() {
				var $input = $(this).parent().find('[input-type=select]');
				$input.length && $(this).css({
					top: $input.offset().top + 35 - top
				});
			});
			$('.prompt').each(function() {
				var t = $(this).attr('top');
				$(this).css({
					top: t - top
				});
			});
		});
		$('body').delegate('.input_down', 'click', function() {
			$(this).parent().find('[input-type=select]').click().focus();
		});
		$('body').delegate('[input-type=select]', 'blur', function() {
			var _this = this;
			setTimeout(function() {
				$(_this).parent().find('.dropdown-menu').hide();
			}, 200);
		});

		/**
		 * 关闭标签
		 */
		$('body').delegate('.detailed_close, .detailed_close2', 'click', function() {
			$(this).closest('.mask, .mask_in').hide();
			return false;
		});
	});

	/*
	 * 页面样式的操作
	 */
	var Page = {
			mainPanel: $('#mainPanel'),
			resetMain: function() {
				var c = this.mainPanel.layout();
				var p = c.layout('panel', 'center');
				var centerOldHeight = p.panel('panel').outerHeight();
				p.panel('resize', {
					height: 'auto'
				});
				var centerNewHeight = p.panel('panel').outerHeight();
				var centerHeight = c.height() + centerNewHeight - centerOldHeight;

				var w = c.layout('panel', 'west');
				var westOldHeight = w.panel('panel').outerHeight();
				w.panel('resize', {
					height: 'auto'
				});
				var westNewHeight = w.panel('panel').outerHeight();
				var westHeight = c.height() + westNewHeight - westOldHeight;

				var height = centerHeight > westHeight ? centerHeight : westHeight;
				var winHeight = $(window).height();
				var height = height > winHeight ? height : winHeight;
				c.layout('resize', {
					height: (height),
					width: '100%'
				});
			},
			loadCenter: function(url) {
				this.mainPanel.layout('panel', 'center').panel({
					href: url
				});
			},
			loadMask: function(url) {
				$('#maskPanel').show().panel({
					href: url
				});
			},
			tableReset: function() {
				$('body').find('.easyui-datagrid').datagrid('resize');
			}
		}
		/**
		 * 失败提示框
		 * @param {Object} 提示消息
		 */
	var dangerPrompt = function(msg) {
		prompt(msg, 'danger');
	};
	//
	/**
	 * 成功提示框
	 * @param {String} 提示消息
	 */
	var successPrompt = function(msg) {
		prompt(msg, 'success');
	};
	/**
	 * 顶部提示框
	 * @param {String} 提示消息
	 * @param {String} 类型
	 */
	var prompt = function(msg, type) {
		var $dom = $('<div class="prompt"><div class="alert alert-' + type + ' alert-dismissible"><button type="button" class="close"><span aria-hidden="true">&times;</span></button>' +
			msg + '</div></div>');
		$dom.find('.close').click(function() {
			$dom.remove();
		});
		$('body').append($dom);
		$dom.css('left', ($('body').width() - $dom.width()) / 2);
		setTimeout(function() {
			$dom.remove();
		}, 3000);
	};

	return {
		Page: Page,
		alert: {
			dangerPrompt: dangerPrompt,
			successPrompt: successPrompt
		}
	}
});