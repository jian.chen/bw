<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>

	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="renderer" content="webkit">
		<link rel="shortcut icon" href="scrm/img/sage.png">
		<title>SAGE AIR</title>
		<link rel="stylesheet" type="text/css" href="scrm/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="scrm/css/bootstrap-datetimepicker.min.css" />
		<link rel="stylesheet" type="text/css" href="scrm/css/easyui.css" />
		<link rel="stylesheet" type="text/css" href="scrm/css/common.css" />
		<script src="scrm/js/lib/require.js" type="text/javascript" charset="utf-8"></script>
		<script src="scrm/js/lib/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="scrm/js/lib/jquery.easyui.min.js"></script>
		<script src="scrm/js/contants.js" type="text/javascript" charset="utf-8"></script>
		<script src="scrm/js/index.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="scrm/js/lib/jquery.easyui.min.js"></script>
		<style type="text/css">
			.loginBg{
				position: absolute;
				width: 100%;
				height: 100%;
				background: url(scrm/img/loginbg.png);
				top: 0;
			}
			.loginBg img{
				width: 100%;
				height: 100%;
			}
			.common_top{
				position: absolute;
				top: 0;
				z-index: 10;
				width: 100%;
			}
			.loginBox{
				width: 410px;
				height: 360px;
				background-image: url(scrm/img/loginBox.png);
				position: absolute;
				top: 50%;
				left: 50%;
				margin-left: -205px;
				margin-top: -180px;
			}
			.loginForm{
				position: absolute;
				top: 90px;
				left: 50%;
				width: 270px;
				margin-left: -135px;
				font-size: 16px;
			}
			.loginForm span{
				display: inline-block;
				width: 70px;
				line-height: 30px;
			}
			.loginForm input{
				width: 195px;
				height: 30px;
				line-height: 30px;
				display: inline-block;
				margin-bottom: 25px;
			}
			.loginCheck{
				padding-left: 70px;
				margin-top: -15px;
			}
			.loginCheck img{
				display: block;
				margin: auto;
			}
			.loginBtn{
				height: 55px;
				font-size: 16px;
				margin-top: 10px;
			}
			.loginBottom{
				font-size: 12px;
				color: #666666;
				position: absolute;
				top: 50%;
				left: 50%;
				margin-left: -65px;
				margin-top: 200px;
				text-align: center;
			}
		</style>
		
<script type="text/javascript">
$(function() {
	<c:if test="${!empty sessionScope.message}">
		alert('${sessionScope.message}');
	</c:if>	
	changeValidate();
	$('input:text:first').focus();
	bindLoginButtonClick();
	bindCaptchaImageClick();
	bindInputKeyDown(); 
}); 
function bindInputKeyDown() { 
	$inp = $(':input'); 
	$inp.bind('keydown', function (e) { 
		if (e.which == 13) { 
			e.preventDefault(); 
			var nxtIdx = $inp.index(this) + 1; 
			$(":input:eq(" + nxtIdx + ")").focus(); 
			if($inp.index(this) == 2) 
				$('#loginButton').click(); 
		} 
	}); 
}
function bindCaptchaImageClick() {
	$('#validateCodeImage').click(function() {
		changeValidate();
		$('#validateCode').val('');
		$('#validateCode').focus();
	});
}
function changeValidate() {
	$('#validateCodeImage').attr('src',$.baseUrl+'/validatecode.do?random=' + Math.random());
}
function bindLoginButtonClick() {
	$('#loginButton').click(function() {
		if(formCheckLogin($('#loginForm'))){
			 $('#loginForm').submit();
		}
		
	});
}

function formCheckLogin(obj){
	var flag = true;
	obj.find(':input').each(function(i,v){
		var _pp = $(this);
		 _pp.tooltip('destroy');
		var value = _pp.val();
		var valLength = value.length;
		var data_options =$.trim($(this).attr("data-options"));
		if(data_options){
			eval("var data = {"+data_options+"}");
			var required = data.required;
			if(required && valLength==0){
				 _pp.tooltip({
					position: 'right',    
					content: '<span name="tooltipspan" style="color:red">不能为空</span>'
				});
				_pp.tooltip('show');
				_pp.focus();
				flag = false;
				return;
			}
		}
	});
	return flag;
}
</script>
		
	</head>
		
	<body>
		<!--头部信息-->
		<div class="common_top clearfix">
			<div class="main-width clearfix">
				<div class="common_logo_box icon sage"></div>
			</div>
		</div>
		<div class="loginBg">
			<img src="scrm/img/loginbg.png"/>
			<div class="loginBox">
				<form id="loginForm" action="${ctx}/onlogin.do" method="post">
					<div class="loginForm">
						<div>
							<span>用户名</span>
							<input id="loginName" name="userName" data-options="required:true" class="form-control" />
						</div>
						<div>
							<span>密码</span>
							<input type="password" id="loginPass" data-options="required:true" name="password" class="form-control" />
						</div>
						<div>
							<span>验证码</span>
							<input id="validateCode" name="validateCode" data-options="required:true" class="form-control" />
						</div>
						<div class="loginCheck">
							<img style="width: 100px; height: 30px;" id="validateCodeImage" title="点击更新验证码"/>
						</div>
						<div>
							<button type="button" id="loginButton" class="btn btn-success btn-block loginBtn">登录</button>
						</div>
					</div>
				</form>
			</div>
			<p class="loginBottom">Sage赛捷软件提供授权</p>
		</div>
	</body>

</html>