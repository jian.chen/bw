package com.bw.adv.archive.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.archive.model.FileSystem;
import com.bw.adv.archive.repository.FileSystemRespository;
import com.bw.adv.archive.service.FileSystemService;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;

@Service
public class FileSystemServiceImpl extends BaseServiceImpl<FileSystem> implements FileSystemService  {

	@Autowired
	private FileSystemRespository fileSystemRespository;
	
	@Override
	public BaseRepository<FileSystem, ? extends BaseMapper<FileSystem>> getBaseRepository() {
		return fileSystemRespository;
	}

}
