package com.bw.adv.archive.controller;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.archive.model.FileSystem;
import com.bw.adv.archive.service.FileSystemService;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;

@Controller
@RequestMapping("/file/upload")
public class FileUploadController {
	
	@Autowired
	private FileSystemService fileSystemService;
	
	@RequestMapping(value="/img",method=RequestMethod.POST)
	private String imgUpload(String name,String password,@RequestParam(value="file",required=false) MultipartFile file,HttpServletRequest request)throws Exception{
		
		System.out.println(name);
		//获得物理路径webapp所在路径
		String pathRoot = request.getSession().getServletContext().getRealPath("");
		String path="";
		if(!file.isEmpty()){
			//生成uuid作为文件名称
			String uuid = UUID.randomUUID().toString().replaceAll("-","");
			//获得文件类型（可以判断如果不是图片，禁止上传）
			String contentType=file.getContentType();
			//获得文件后缀名称
			String imageName=contentType.substring(contentType.indexOf("/")+1);
			path="/static/images/"+uuid+"."+imageName;
			file.transferTo(new File(pathRoot+path));
		}
		System.out.println(path);
		request.setAttribute("imagesPath", path);
		return "success";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveFile(FileSystem f){
		f.setFileId("123321111");
		fileSystemService.save(f);
		return "success";
	}
	
	
	@RequestMapping(value = "/img", method = RequestMethod.POST)
	@ResponseBody
	public String uploadImg(String fileFlag,HttpServletRequest request,HttpServletResponse response){
		MultipartHttpServletRequest multipartRequest = null;
		String logoPathDir = null;
		String logoRealPathDir = null;
		File logoSaveFile = null;
		MultipartFile multipartFile = null;
		String suffix = null;
		String logImageName = null;
		String fileName = null;
		String filePath = null;
		File file = null;
		FileSystem fileSystem=null;
		multipartRequest = (MultipartHttpServletRequest) request;
		/** 构建文件保存的目录* */
		logoPathDir = "/business/shops/upload/coupon/"+ DateUtils.getCurrentTimeOfDb();
		/** 得到文件保存目录的真实路径* */
		logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
		/** 根据真实路径创建目录* */
		logoSaveFile = new File(logoRealPathDir);
		if (!logoSaveFile.exists()){
			logoSaveFile.mkdirs();
		}
		/** 页面控件的文件流* */
		multipartFile = multipartRequest.getFile(fileFlag);
		if (FileUtil.isAllowUpForPicture(multipartFile.getOriginalFilename())) {
			/** 获取文件的后缀* */
			suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
			/** 使用UUID生成文件名称* */
			logImageName = UUID.randomUUID().toString() + suffix;// 构建文件名称
			/** 拼成完整的文件保存路径加文件* */
			fileName = logoRealPathDir + File.separator + logImageName;
			file = new File(fileName);
			filePath = logoPathDir+ "/" + logImageName;
			try {
				multipartFile.transferTo(file);
				
				fileSystem=new FileSystem();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return filePath;
		}else{
			return "不被允许上传的文件格式!";
		}
	}
}
