package com.bw.adv.archive.mapper;

import com.bw.adv.archive.model.FileSystem;
import com.bw.adv.core.mapper.BaseMapper;

public interface FileSystemMapper extends BaseMapper<FileSystem> {
	
}