package com.bw.adv.archive.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.archive.mapper.FileSystemMapper;
import com.bw.adv.archive.model.FileSystem;
import com.bw.adv.archive.repository.FileSystemRespository;
import com.bw.adv.core.repository.impl.BaseRepositoryImpl;

@Repository
public class FileSystemRepositoryImpl extends BaseRepositoryImpl<FileSystem, FileSystemMapper> implements FileSystemRespository {

	@Override
	protected Class<FileSystemMapper> getMapperClass() {
		return FileSystemMapper.class;
	}

}
