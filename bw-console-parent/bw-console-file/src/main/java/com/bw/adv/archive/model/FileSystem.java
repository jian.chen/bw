package com.bw.adv.archive.model;

public class FileSystem {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_id
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String fileId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_type
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String fileType;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_old_name
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String fileOldName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_suffix
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String fileSuffix;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_length
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private Long fileLength;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_new_name
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String fileNewName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_path
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String filePath;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_thumb_flag
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String fileThumbFlag;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.file_thumb_path
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String fileThumbPath;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column file_system.create_time
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    private String createTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_id
     *
     * @return the value of file_system.file_id
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_id
     *
     * @param fileId the value for file_system.file_id
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFileId(String fileId) {
        this.fileId = fileId == null ? null : fileId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_type
     *
     * @return the value of file_system.file_type
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_type
     *
     * @param fileType the value for file_system.file_type
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFileType(String fileType) {
        this.fileType = fileType == null ? null : fileType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_old_name
     *
     * @return the value of file_system.file_old_name
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getFileOldName() {
        return fileOldName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_old_name
     *
     * @param fileOldName the value for file_system.file_old_name
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFileOldName(String fileOldName) {
        this.fileOldName = fileOldName == null ? null : fileOldName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_suffix
     *
     * @return the value of file_system.file_suffix
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getFileSuffix() {
        return fileSuffix;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_suffix
     *
     * @param fileSuffix the value for file_system.file_suffix
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix == null ? null : fileSuffix.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_length
     *
     * @return the value of file_system.file_length
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public Long getFileLength() {
        return fileLength;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_length
     *
     * @param fileLength the value for file_system.file_length
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFileLength(Long fileLength) {
        this.fileLength = fileLength;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_new_name
     *
     * @return the value of file_system.file_new_name
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getFileNewName() {
        return fileNewName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_new_name
     *
     * @param fileNewName the value for file_system.file_new_name
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFileNewName(String fileNewName) {
        this.fileNewName = fileNewName == null ? null : fileNewName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_path
     *
     * @return the value of file_system.file_path
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_path
     *
     * @param filePath the value for file_system.file_path
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath == null ? null : filePath.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_thumb_flag
     *
     * @return the value of file_system.file_thumb_flag
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getFileThumbFlag() {
        return fileThumbFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_thumb_flag
     *
     * @param fileThumbFlag the value for file_system.file_thumb_flag
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFileThumbFlag(String fileThumbFlag) {
        this.fileThumbFlag = fileThumbFlag == null ? null : fileThumbFlag.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.file_thumb_path
     *
     * @return the value of file_system.file_thumb_path
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getFileThumbPath() {
        return fileThumbPath;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.file_thumb_path
     *
     * @param fileThumbPath the value for file_system.file_thumb_path
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setFileThumbPath(String fileThumbPath) {
        this.fileThumbPath = fileThumbPath == null ? null : fileThumbPath.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column file_system.create_time
     *
     * @return the value of file_system.create_time
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column file_system.create_time
     *
     * @param createTime the value for file_system.create_time
     *
     * @mbg.generated Thu Jun 29 15:53:55 CST 2017
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }
}