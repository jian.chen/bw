package com.bw.adv.archive.repository;

import com.bw.adv.archive.mapper.FileSystemMapper;
import com.bw.adv.archive.model.FileSystem;
import com.bw.adv.core.repository.BaseRepository;

public interface FileSystemRespository extends BaseRepository<FileSystem, FileSystemMapper> {

}