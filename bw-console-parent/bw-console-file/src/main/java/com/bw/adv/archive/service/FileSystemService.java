package com.bw.adv.archive.service;

import com.bw.adv.archive.model.FileSystem;
import com.bw.adv.core.service.BaseService;

public interface FileSystemService extends BaseService<FileSystem> {

}