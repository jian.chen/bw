package com.bw.adv.archive.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.archive.model.FileSystem;
import com.bw.adv.archive.service.FileSystemService;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;

@Controller
@RequestMapping("/file/view")
public class ViewController {
	
	@Autowired
	private FileSystemService fileSystemService;
	
	@RequestMapping(value="/img",method=RequestMethod.POST)
	private String imageView(String fileId){
		
		return "success";
	}
	
	
	@RequestMapping(value = "/image")
	public void imageView(HttpServletRequest request,HttpServletResponse response,String fileId){
		if (StringUtils.isBlank(fileId)) 
			return;
		FileSystem fileSystem = this.fileSystemService.queryByPk(fileId);
		if(null == fileSystem){
			return;
		}
		InputStream fis = null;
		OutputStream os = null;
		try {
			String type = "image/png";
			File file = new File(fileSystem.getFilePath());
			if(!file.exists()){
				return;
			}
			
			fis = new FileInputStream(file);
			response.setContentType(type);       
			response.setHeader("Pragma","No-cache");
			response.setHeader("Cache-Control","no-cache");
			response.setDateHeader("Expire",0);
			os = response.getOutputStream();
			if(null != fis){
				byte[] buffer = new byte[1024]; 
				int count = 0;
				while((count = fis.read(buffer)) > 0){
					os.write(buffer, 0, count);
				}
			}
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
