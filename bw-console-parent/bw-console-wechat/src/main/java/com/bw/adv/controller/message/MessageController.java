package com.bw.adv.controller.message;

import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.JsApiSignUtil;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping("/messageController")
@SuppressWarnings("unused")
public class MessageController extends BaseController {
    private static final Logger LOGGER = Logger.getLogger(MessageController.class);
    
    /**
     * 消息列表
     *
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "initUserMessage")
    @ResponseBody
    public JsonModel initUserMessage(HttpServletRequest request) {
        JsonModel jsonModel = null;
//		Map<String, Object> map = null;
//		String code =null;
//		String message = null;
        String memberId = null;
        String page = null;
        String rows = null;
//		List<Map<String,Object>> listMap =null;
        try {
//			map =new HashMap<String, Object>();
            memberId = request.getParameter("memberId");
            page = request.getParameter("page");
            rows = request.getParameter("rows");
            WebClient client = createClient();
            Map<String, Object> tmp = new HashMap<String, Object>();
            tmp.put("memberId", memberId);
            tmp.put("page", page);
            tmp.put("rows", rows);
            RestParameter params = new RestParameter(tmp);
            client.accept(MediaType.APPLICATION_JSON);
            client.path("message/queryMemberMessageByMemberId" + params.getMatrixString());
            ApiResult result = client.get(ApiResult.class);
//			code =result.getCode();
//			message = result.getMessage();
//			listMap = result.getItems();
            jsonModel = new JsonModel(result.getCode(), result.getMessage(), result.getItems());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
        }
//		}finally{
//			map.put("code", code);
//			map.put("message", message);
//			map.put("list", listMap);
//		}
        return jsonModel;
    }

    /**
     * 查询会员消息详情
     *
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "initMemberMessageDetail")
    @ResponseBody
    public JsonModel initMemberMessageDetail(HttpServletRequest request) {
        JsonModel jsonModel = null;
//		Map<String, Object> map = null;
//		String code =null;
//		String message = null;
        String memberMessageId = null;
        String memberId = null;
//		Map<String,Object> dataMap =null;
        try {
//			map =new HashMap<String, Object>();
            memberMessageId = request.getParameter("memberMessageId");
            memberId = request.getParameter("memberId");
            WebClient client = createClient();
            Map<String, Object> tmp = new HashMap<String, Object>();
            tmp.put("memberMessageId", memberMessageId);
            tmp.put("memberId", memberId);
            RestParameter params = new RestParameter(tmp);
            client.accept(MediaType.APPLICATION_JSON);
            client.path("message/queryMemberMessageDetail" + params.getMatrixString());
            ApiResult result = client.get(ApiResult.class);
//			code =result.getCode();
//			message = result.getMessage();
//			dataMap = result.getData();
            jsonModel = new JsonModel(result.getCode(), result.getMessage(), result.getData());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
        }
//		}finally{
//			map.put("code", code);
//			map.put("message", message);
//			map.put("dataMap", dataMap);
//		}
        return jsonModel;
    }

    /**
     * 会员是否有未读消息
     *
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "queryMemberMessageNotRead")
    @ResponseBody
    public JsonModel queryMemberMessageNotRead(HttpServletRequest request) {
        JsonModel jsonModel = null;
//		Map<String, Object> map = null;
//		String code =null;
//		String message = null;
        String memberId = null;
//		Map<String,Object> dataMap =null;
        try {
//			map =new HashMap<String, Object>();
            memberId = request.getParameter("memberId");
            WebClient client = createClient();
            Map<String, Object> tmp = new HashMap<String, Object>();
            tmp.put("memberId", memberId);
            RestParameter params = new RestParameter(tmp);
            client.accept(MediaType.APPLICATION_JSON);
            client.path("message/queryMemberMessageNotRead" + params.getMatrixString());
            ApiResult result = client.get(ApiResult.class);
//			code =result.getCode();
//			message = result.getMessage();
//			dataMap = result.getData();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("dataMap:" + result.getData());
            }
//			System.out.println("dataMap:"+dataMap);
            jsonModel = new JsonModel(result.getCode(), result.getMessage(), result.getData());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
        }
//		}finally{
//			map.put("code", code);
//			map.put("message", message);
//			map.put("data", dataMap);
//		}
        return jsonModel;
    }

    /**
     * 获取微信参数
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "getWechatParamOfMessage")
    @ResponseBody
    public JsonModel getWechatParamOfMessage(HttpServletRequest request, HttpServletResponse response) {
        JsonModel jsonModel = null;
        Map<String, Object> map = null;
        Map<String, String> paramMap = null;
        String ticket = null;
        String token = null;
        String url = null;
        String memberMessageId = null;
//		String openId = null;
        String memberId = null;
        String seq = null;
        String link = null;
        Map<String, Object> messageMap = null;
        String messageUrl = null;
        try {
            memberMessageId = request.getParameter("memberMessageId");
            memberId = request.getParameter("memberId");
//			openId =request.getParameter("openId");
            seq = request.getParameter("seq");
            url = request.getParameter("url");
            if (url.contains(";")) {
                url = url.replaceAll(";", "&");
            }
            link = "http://" + domainName + "/scrm-bk-wechat/messageController/getMessageDetail.do?param=" + memberMessageId + ";" + memberId;
            link = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=" + link + "&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
            token = getAccessToken();
            if (StringUtils.isNotEmpty(token)) {
                WebClient client = createClient();
                Map<String, Object> tmp = new HashMap<String, Object>();
                tmp.put("token", token);
                RestParameter params = new RestParameter(tmp);
                client.accept(MediaType.APPLICATION_JSON);
                client.path("wechat/getAccessTicketCache" + params.getMatrixString());
                ApiResult result = client.get(ApiResult.class);
                map = result.getData();
                ticket = (String) map.get("ticket");
                paramMap = JsApiSignUtil.sign(ticket, url);
                paramMap.put("appId", appId);
                paramMap.put("link", link);
                paramMap.put("domainName", domainName);
                messageMap = queryMemberMessage(memberMessageId);
                if (messageMap.get("messageImg1") != null) {
                    messageUrl = messageMap.get("messageImg1").toString();
                }
                paramMap.put("imageUrl", messageUrl);
            }
            jsonModel = ResultConstantsEnum.createSuccessResult();
            jsonModel.setData(paramMap);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
        }
        return jsonModel;
    }

    @RequestMapping(method = RequestMethod.GET, value = "getMessageDetail")
    public String getMessageDetail(HttpServletRequest request, HttpServletResponse response) {
        String result = null;
        String openId = null;
        String param = request.getParameter("param");
        String[] params = param.split(";");
        String memberMessageId = params[0];
        String memberId = params[1];
        String memberMessageItemId = null;
//		String seq = params[2];
        Map<String, Object> returnMapTemp = null;
        Map<String, Object> dataMap = null;
        try {
            //returnMapTemp = getOpenId(request, response);
            openId = (String) returnMapTemp.get("openId");

            WebClient client = createClient();
            Map<String, Object> tmp = new HashMap<String, Object>();
            tmp.put("memberMessageId", memberMessageId);
            tmp.put("memberId", memberId);
            RestParameter queryparams = new RestParameter(tmp);
            client.accept(MediaType.APPLICATION_JSON);
            client.path("message/queryMemberMessageItem" + queryparams.getMatrixString());
            ApiResult resultdata = client.get(ApiResult.class);
            dataMap = resultdata.getData();
            Map<String, String> paramMap = new HashMap<String, String>();
            if (dataMap.get("memberMessageItemId") != null) {
//				memberMessageItemId = dataMap.get("memberMessageItemId") == null?"0":dataMap.get("memberMessageItemId").toString();
//				if(memberMessageItemId!="0"){
                memberMessageItemId = dataMap.get("memberMessageItemId").toString();
                paramMap.put("memberMessageItemId", memberMessageItemId);
                paramMap.put("memberMessageId", memberMessageId);
                paramMap.put("openId", openId);
                if (openId != null) {
                    saveMemberMessageItemBrowse(paramMap);
                }
//				}
            }
            result = "redirect:/view/messageDetailLink.html?memberMessageId=" + memberMessageId + "&memberId=" + memberId;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void saveMemberMessageItemBrowse(Map<String, String> messageMap) {
        WebClient client = createClient();
        RestParameter params = new RestParameter(messageMap);
        client.accept(MediaType.APPLICATION_JSON);
        client.header("key", params.getSignString());
        client.path("message/addMemberMessageItemBrowse" + params.getMatrixString());
        ApiResult result = client.get(ApiResult.class);
    }

    private Map<String, Object> queryMemberMessage(String messageItemId) {
        Map<String, Object> resultMap = null;
        Map<String, Object> map = null;

        map = new HashMap<String, Object>();
        resultMap = new HashMap<String, Object>();
        map.put("memberMessageId", messageItemId);
        WebClient client = createClient();
        RestParameter params = new RestParameter(map);
        client.accept(MediaType.APPLICATION_JSON);
        client.header("key", params.getSignString());
        client.path("message/queryMemberMessage" + params.getMatrixString());
        ApiResult result = client.get(ApiResult.class);
        resultMap = result.getData();
        return resultMap;
    }

    private Map<String, Object> getInterfaceMethod(Map<String, Object> tmp, String url) {
        WebClient client = createClient();
        RestParameter parameter = new RestParameter(tmp);
        client.accept(MediaType.APPLICATION_JSON);
        client.header("key", parameter.getSignString());
        client.path(url + parameter.getMatrixString());
        ApiResult apiResult = client.get(ApiResult.class);
        return apiResult.getData();
    }
}
