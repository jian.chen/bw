/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-air-assistant
 * File Name:LoginFilter.java
 * Package Name:com.sage.scrm.filter
 * Date:2016年4月7日下午8:54:26
 * Description: //模块目的、功能描述
 * History: //修改记录
 */

package com.bw.adv.controller.member;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.bw.adv.common.utils.HttpKit;
import com.bw.adv.module.tools.cookie.utils.CookieUtils;


/**
 * ClassName:LoginFilter <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年4月7日 下午8:54:26 <br/>
 * scrmVersion 1.0
 * @author chengdi.cui
 * @version jdk1.7
 * @see
 */
public class LoginFilter implements Filter {

	private Logger logger=Logger.getLogger(getClass());
    private static final List<String> NOT_FILTER_URL = new ArrayList<String>();
    @Value("${appId}")
	private String appId;
    @Value("${domainName}")
	private  String domainName;
    @Value("${projectName}")
	private String projectName;

    static {
        NOT_FILTER_URL.add("/static/*");
        NOT_FILTER_URL.add("/upload/*");
        NOT_FILTER_URL.add("/memberController/wechatEntrance");
        NOT_FILTER_URL.add("/member/back");
        NOT_FILTER_URL.add("/member/toAuth");
        NOT_FILTER_URL.add("/view/*");
        //NOT_FILTER_URL.add("/lottery/*");
        NOT_FILTER_URL.add("/mapController/*");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    	//ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext());
    	//applicationContext.getResource("classpath:conf/properties/dev/web_config.properties");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;
        String uri = servletRequest.getRequestURI();
        servletRequest.getRequestURL();
        String contextPath = servletRequest.getContextPath();
        
        String pathUrl = request.getScheme()+"://"+ request.getServerName()+(request.getServerPort()==80?"":":"+request.getServerPort())+servletRequest.getRequestURI()+(StringUtils.isNotBlank(servletRequest.getQueryString())?"?"+servletRequest.getQueryString():"");
        
        String url = uri.replace(contextPath, "");
        boolean filter = false;
        // 获得用户请求的URI
        //String openId =  (String) session.getAttribute("session_key_openId");
        for (String path : NOT_FILTER_URL) {
            if ((path.endsWith("*") && url.startsWith(path.replace("*", ""))) || url.contains(path)) {
                filter = true;
            }
        }
        if(filter){
        	chain.doFilter(request, response);
        }else{
        	//得到openId
        	String openId = CookieUtils.getCookieByName(servletRequest, CookieUtils.COOKIE_OPENID_KEY);
        	if(StringUtils.isNotBlank(openId)){
        		chain.doFilter(request, response);
        	}else{
        		logger.info(pathUrl);//沒有授权
        		servletResponse.sendRedirect(servletRequest.getContextPath()+"/member/toAuth?backUrl="+pathUrl);
        	}
        }
//        if(StringUtils.isBlank(openId) &&  url.startsWith(FILTER_SPECIAL_URL)){
//        	String encode = URLEncoder.encode(pathUrl,"UTF-8");
//        	servletResponse.sendRedirect("https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri="+encode+"&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect");
//        }
//        
//        if (StringUtils.isBlank(openId)) {
//            if (!filter) {
//            	PrintWriter writer = response.getWriter();
//            	writer.write("{\"code\":\"599\"}");
//            } else {
//                chain.doFilter(servletRequest, servletResponse);
//            }
//        } else {
//            chain.doFilter(servletRequest, servletResponse);
//        }
//    	chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}

