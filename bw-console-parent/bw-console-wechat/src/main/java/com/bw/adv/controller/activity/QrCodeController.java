package com.bw.adv.controller.activity;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.controller.base.BaseController;

@Controller
@RequestMapping("qrcode")
public class QrCodeController extends BaseController {
	
	@RequestMapping(value="/{qrcode}",method=RequestMethod.GET)
	public String call( @PathVariable String qrcode ,Model model){
		
		WebClient client = createClient();
		Map<String, Object> tmp = new HashMap<String, Object>();
		tmp.put("qrcode", qrcode);
		RestParameter params = new RestParameter(tmp);
		client.accept(MediaType.APPLICATION_JSON);
		
		client.path("qrcode/queryMemberByBindingAccount" + params.getMatrixString());
		
		
		return null;
	}
}
