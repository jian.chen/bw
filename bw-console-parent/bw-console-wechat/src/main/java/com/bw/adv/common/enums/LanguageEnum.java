package com.bw.adv.common.enums;

/**
 * Created by jeoy.zhou on 6/15/16.
 */
public enum LanguageEnum {
    CN("CN", "中文"),
    EN("EN", "英文");

    private String lang;
    private String msg;

    LanguageEnum(String lang, String msg) {
        this.lang = lang;
        this.msg = msg;
    }

    public String getLang() {
        return lang;
    }

    public String getMsg() {
        return msg;
    }
}
