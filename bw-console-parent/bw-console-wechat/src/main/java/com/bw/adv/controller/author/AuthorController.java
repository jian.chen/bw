package com.bw.adv.controller.author;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;


@Controller
@RequestMapping("/authorController")
public class AuthorController extends BaseController {
	
	private static String success ="200";
	@Value("${third_url}")
	private String THIRD_URL;
	
	@RequestMapping(method = RequestMethod.GET, value = "getOpenIdByCode")
	@ResponseBody
	public JsonModel getOpenIdByCode(String couponCode,HttpServletRequest request,HttpServletResponse response){
		JsonModel jsonModel = null;
		String openId = null;
//		String status =null;
		Map<String,Object> returnMapTemp = null;
		Map<String,String> resultMap =null;
		try {
			resultMap =new HashMap<String,String>();
			returnMapTemp = getOpenIdByCode(request, response);
			openId = (String)returnMapTemp.get("openId");
			jsonModel = ResultConstantsEnum.createSuccessResult();
//			status = success;
			resultMap.put("openId", openId);
			resultMap.put("url", THIRD_URL);
//			resultMap.put("status", status);
			jsonModel.setData(resultMap);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}
	
}
