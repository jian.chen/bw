package com.bw.adv.controller.gift;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean2;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;
import com.bw.adv.module.activity.enums.AccordTypeEnum;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.tools.cookie.utils.CookieUtils;

@Controller
@RequestMapping("/gift")
@SuppressWarnings("unused")
public class GiftController extends BaseController {

	// 抽奖活动Id
	protected Log logger = LogFactory.getLog(this.getClass());
	
	/**
	 * @PE
	 * 徽章礼品兑换
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "memberBadgegiftExchange")
	@ResponseBody
	public JsonModel memberBadgegiftExchange(HttpServletRequest request){
		JsonModel result = null;
		String memberId = null;
		WebClient client = createClient();
		try {
			memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("memberId", memberId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/giftExchange/memberBadgegiftExchange").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			ApiResult apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(apiResult.getData());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "myMemberGift")
	public String myMemberGift(Model model){
		return "member/badge";
	}
	@RequestMapping(method = RequestMethod.GET, value = "turntable")
	public String memberGiftGo(String page,String couponInstanceCode,Model model){
		if(StringUtils.isNotBlank(couponInstanceCode)){
			model.addAttribute("couponInstanceCode", couponInstanceCode);
		}
		return "turntable/"+page;
	}
	@RequestMapping(method = RequestMethod.GET, value = "member")
	public String member(String page,String couponInstanceCode,String createDate,Model model){
		if(StringUtils.isNotBlank(couponInstanceCode)){
			model.addAttribute("couponInstanceCode", couponInstanceCode);
			model.addAttribute("createDate", createDate);
		}
		return "member/"+page;
	}
	
	/**
	 * @PE
	 * 查询徽章
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "getMemberGiftMemberBadge")
	@ResponseBody
	public JsonModel getMemberGiftMemberBadge(HttpServletRequest request){
		JsonModel result = null;
		String memberId = null;
		WebClient client = createClient();
		try {
			memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("memberId", memberId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/giftExchange/getMemberGiftMemberBadge").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			ApiResult apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(apiResult.getItems());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	/**
	 * @PE
	 * 查询徽章
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "getMemberGift")
	@ResponseBody
	public JsonModel getMemberGift(HttpServletRequest request){
		JsonModel result = null;
		String memberId = null;
		WebClient client = createClient();
		try {
			memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("memberId", memberId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/giftExchange/getMemberGift").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			ApiResult apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(apiResult.getItems());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	/**
	 * @PE
	 * 查询徽章
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "getMemberGiftByCode")
	@ResponseBody
	public JsonModel getMemberGiftByCode(HttpServletRequest request){
		JsonModel result = null;
		String memberId = null;
		WebClient client = createClient();
		try {
			memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("memberId", memberId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/giftExchange/getMemberGift").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			ApiResult apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(apiResult.getItems());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
}
