/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-wechat
 * File Name:ComplainController.java
 * Package Name:com.sage.scrm.controller.complain
 * Date:2015年12月29日下午5:15:06
 * Description: //模块目的、功能描述
 * History: //修改记录
 */

package com.bw.adv.controller.complain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.FileUtils;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;
import com.bw.adv.module.member.complain.model.Complain;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.tools.DateUtils;

/**
 * ClassName:ComplainController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月29日 下午5:15:06 <br/>
 * scrmVersion 1.0
 * @author yu.zhang
 * @version jdk1.7
 * @see
 */
@Controller
@RequestMapping("/complainController")
public class ComplainController extends BaseController {
    private static final Logger LOGGER = Logger.getLogger(ComplainController.class);
    
    @Value("${sign}")
    private String SIGN;

    /**
     * @PE
     * 意见咨询保存
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "saveComplain")
    @ResponseBody
    public JsonModel saveComplain(HttpServletRequest request) {
        JsonModel jsonModel = null;
        List<String> filePathList = null;
        String filePath = null;
        Complain complain = new Complain();
        MultipartHttpServletRequest multipartRequest = null;
        String personName = null;
        String storeName = null;
        String mobile = null;
        String email = null;
        String complainDesc = null;
        String geoId = null;
        String complainTypeId = null;
        Long memberId = null;
        String storeId = null;
        String openId = null;
        MemberExp memberExp = null;
        try {
        	memberExp = this.getMemberExpFromSession(request);
        	if (memberExp != null) {
        		memberId = memberExp.getMemberId();
			}
            multipartRequest = (MultipartHttpServletRequest) request;
            personName = multipartRequest.getParameter("username");
            mobile = multipartRequest.getParameter("telephone");
            email = multipartRequest.getParameter("email");
            openId = this.getOpenIdFromSession(request);
            geoId = multipartRequest.getParameter("geoId");
            complainDesc = multipartRequest.getParameter("complainDesc");
            complainTypeId = multipartRequest.getParameter("complainType");
            storeName = request.getParameter("storeName");
            storeId = request.getParameter("storeId");
            complain.setCreateTime(DateUtils.getCurrentTimeOfDb());
            complain.setComplainDesc(complainDesc);
            complain.setEmail(email);
            complain.setMobile(mobile);
            complain.setPersonName(personName);
            
            if(StringUtils.isNotBlank(storeName) && !storeName.equals("undefined")){
            	complain.setStoreName(storeName);
            }
            if(StringUtils.isNotBlank(storeId) && !storeId.equals("undefined")){
            	complain.setStoreId(Long.parseLong(storeId));
            }
            if (StringUtils.isNotEmpty(geoId)) {
                complain.setGeoId(Long.valueOf(geoId));
            }
            if (memberId != null) {
                complain.setMemberId(memberId);
                complain.setIsMember("Y");
            } else {
                complain.setIsMember("N");
            }
            complain.setOpenId(openId);
            if (StringUtils.isNotEmpty(complainTypeId)) {
                complain.setComplainTypeId(Long.valueOf(complainTypeId));
            }
            filePathList = FileUtils.uploadImages(request);
            for (int i = 0; i < filePathList.size(); i++) {
                filePath = filePathList.get(i);
                filePath = "http://" + domainName + request.getContextPath() + filePath;
                if (i == 0) {
                    complain.setComplainImg1(filePath);
                } else if (i == 1) {
                    complain.setComplainImg2(filePath);
                } else if (i == 2) {
                    complain.setComplainImg3(filePath);
                } else if (i == 3) {
                    complain.setComplainImg4(filePath);
                } else if (i == 4) {
                    complain.setComplainImg5(filePath);
                }
            }
            List<Object> provides = new ArrayList<Object>();
            provides.add(new JacksonJsonProvider());
            WebClient client = createClient();
            RestParameter params = new RestParameter(complain, "shbdsyhdeuad");
            client.accept(MediaType.APPLICATION_JSON);
            client.header("systemId", "102");
            client.header("sign", SIGN);
            client.path("complain/saveComplain").header("content-type", "application/json");
            ApiResult result = client.post(params.getJsonString(), ApiResult.class);
            jsonModel = new JsonModel(result.getCode(), result.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
        }
        return jsonModel;
    }


    /**
     * @PE
     * findComplainForSatisfaction:(用户查询已解决的问题详情). <br/>
     * Date: 2016年7月8日 下午4:51:20 <br/>
     * scrmVersion standard
     * @author lutianshui
     * @version jdk1.7
     * @param requestParams
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "findComplainForSatisfaction")
    @ResponseBody
    public JsonModel findComplainForSatisfaction(@RequestBody JSONObject requestParams) {
        JsonModel jsonModel = null;
        Map<String, String> map = null;
        RestParameter params = null;
        ApiResult apiResult = null;
		WebClient client = null;
        try {
        	client = createClient();
        	map = new HashMap<String, String>();
        	if (!requestParams.containsKey("complainId")) {
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			map.put("complainId", requestParams.getString("complainId"));
			params = new RestParameter(map);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("complain/findComplainByPk" + params.getMatrixString());
			apiResult = client.post(params.getJsonString(), ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				jsonModel = ResultConstantsEnum.createFailResult();
				jsonModel.setMessage(apiResult.getMessage());
				return jsonModel;
			}
			jsonModel = ResultConstantsEnum.createSuccessResult();
			jsonModel.setData(apiResult.getData().get("complain"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
        }
        return jsonModel;
    }
    
    /**
     * @PE
     * findComplainForSatisfaction:(用户提交对问题解决的满意度). <br/>
     * Date: 2016年7月8日 下午4:52:27 <br/>
     * scrmVersion standard
     * @author lutianshui
     * @version jdk1.7
     * @param requestParams
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "saveSatisfaction")
    @ResponseBody
    public JsonModel saveSatisfaction(@RequestBody JSONObject requestParams) {
        JsonModel jsonModel = null;
        Map<String, String> map = null;
        RestParameter params = null;
        ApiResult apiResult = null;
		WebClient client = null;
		String satisfaction = null;
		String complainId = null;
		String satisfactionDesc = null;
        try {
        	if (!requestParams.containsKey("complainId")) {
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
        	complainId = requestParams.getString("complainId");
        	if(requestParams.containsKey("satisfaction")){
        		satisfaction = requestParams.getString("satisfaction");
        	}
        	if(requestParams.containsKey("satisfactionDesc")){
        		satisfactionDesc = requestParams.getString("satisfactionDesc");
        	}
        	client = createClient();
        	map = new HashMap<String, String>();
        	map.put("complainId", complainId);
			map.put("satisfaction", satisfaction);
			map.put("satisfactionDesc", satisfactionDesc);
			params = new RestParameter(map);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("complain/saveSatisfaction" + params.getMatrixString());
			apiResult = client.post(params.getJsonString(), ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				jsonModel = ResultConstantsEnum.createFailResult();
				jsonModel.setMessage(apiResult.getMessage());
				return jsonModel;
			}
			jsonModel = ResultConstantsEnum.createSuccessResult();
			jsonModel.setData(apiResult.getData().get("complain"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
        }
        return jsonModel;
    }
    
    /**
     * @PE
     * 查询门店列表
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "findStoreList")
    @ResponseBody
    public JsonModel findStoreList() {
    	JsonModel jsonModel = null;
    	WebClient client = null;
    	ApiResult apiResult = null;
    	Map<String, Object> resultMap = null;
    	try {
    		resultMap = new HashMap<>();
    		client = createClient();
    		client.accept(MediaType.APPLICATION_JSON);
    		client.path("complain/findStoreList");
    		apiResult = client.post(null, ApiResult.class);
    		if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				jsonModel = ResultConstantsEnum.createFailResult();
				jsonModel.setMessage(apiResult.getMessage());
				return jsonModel;
			}
    		jsonModel = ResultConstantsEnum.createSuccessResult();
    		resultMap.put("item", apiResult.getItems());
			jsonModel.setData(resultMap);
		} catch (Exception e) {
			e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
		}
    	return jsonModel;
    }
    
    /**
     * @PE
     * 意见咨询初始化数据
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "initComplain")
    @ResponseBody
    public JsonModel initComplain(HttpServletRequest request){
    	JsonModel jsonModel = null;
    	Map<String, Object> resultMap = null;
    	try {
    		resultMap = new HashMap<>();
    		jsonModel = new JsonModel();
    		MemberExp exp = getMemberExpFromSession(request);
    		resultMap.put("email", exp.getEmail());
    		resultMap.put("telephone", exp.getMobile());
    		resultMap.put("username", exp.getMemberName());
    		jsonModel = ResultConstantsEnum.createSuccessResult();
			jsonModel.setData(resultMap);
		} catch (Exception e) {
			e.printStackTrace();
            return ResultConstantsEnum.createFailResult();
		}
    	return jsonModel;
    
    }
}

