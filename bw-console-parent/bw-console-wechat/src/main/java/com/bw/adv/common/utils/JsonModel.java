package com.bw.adv.common.utils;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;

public class JsonModel implements Serializable {

	private static final long serialVersionUID = 1219760989503992746L;


	protected String message;
	protected String code;
	protected Object data;
	
	public JsonModel() {
	}
	
	public JsonModel(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public JsonModel(String code, String message, Object data) {
		this.message = message;
		this.code = code;
		this.data = data;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	public String getMessage() {
		return this.message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}


	public void setMessage(String message) {
		this.message = message;
	}



	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}
