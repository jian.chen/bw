package com.bw.adv.controller.member;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.FileUtils;
import com.bw.adv.common.utils.HttpKit;
import com.bw.adv.common.utils.JsApiSignUtil;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.MemberCardGrade;
import com.bw.adv.module.member.model.exp.MemberExp;

@Controller
@RequestMapping("/member")
public class MemberController extends BaseController {

	private Logger logger = Logger.getLogger(MemberController.class);
	@Value("${wechat_channel_id}")
	private String wechatChannelId;
	@Value("${special_channel_end}")
	private String specialChannelEnd;
	@Value("${wechatAuthUrl}")
	private String wechatAuthUrl;
	protected Log log = LogFactory.getLog(this.getClass());
	@Value("${menu_type}")
	private String menuType;
	@Value("${appId}")
	private String appId;
	private static Map<String, String> MENU_MAP = new HashMap<String, String>();

//	static {
//		String str[] = MENU_TYPE.split(",");
//		for (String s : str) {
//			MENU_MAP.put(s.split("-")[0], s.split("-")[1]);
//		}
//	}
	@RequestMapping(method = RequestMethod.GET, value = "/toAuth")
	public void toAuth(HttpServletRequest request ,HttpServletResponse response, String backUrl){
		
		try {
			
			//构建微信回调
			
			String ContextPath=request.getScheme()+"://"+ request.getServerName()+(request.getServerPort()==80?"":":"+request.getServerPort())+request.getContextPath()+"/member/back?url="+backUrl;
			
			String callBack = URLEncoder.encode(ContextPath, "UTF-8");
			String wechatUrl=wechatAuthUrl.replace("APPID", appId).replace("URI", callBack);
			logger.info("send wechat:"+ContextPath);
			response.sendRedirect(wechatUrl);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	}
	@RequestMapping(method = RequestMethod.GET, value = "/back")
	public void back(HttpServletRequest request ,HttpServletResponse response,String url){
		String apiResult = null;
		String openId = null;
		String code=request.getParameter("code");
		logger.info("url:"+url);
		if (StringUtils.isBlank(code) || StringUtils.isBlank(appId) || StringUtils.isBlank(appSecret)) {
			throw new MemberException(
					"入口:获取微信参数有误" + "code:" + code + " appId:" + appId + " appSecret:" + appSecret);
		}
		String accessTokenUrl = AccessTokenUrl.replace("APPID", appId).replace("SECRET", appSecret).replace("CODE",code);
		logger.info("accessTokenUrl:"+accessTokenUrl);
		try {
			apiResult = HttpKit.get(accessTokenUrl);
			JSONObject jsonObject = JSONObject.fromObject(apiResult);
			openId = (String) jsonObject.get("openid");
			if(StringUtils.isNotBlank(openId)){
				//添加openId到cookie
				addCookie(response, COOKIE_OPENID_KEY, openId);
				Member member = queryBindingAccountByOpenId(openId);
				if(member!=null){
					addCookie(response, COOKIE_MEMBER_ID, String.valueOf(member.getMemberId()));
				}
			}
			response.sendRedirect(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	
	/**
	 * @PE
	 * 注册
	 * @param request
	 * @param response
	 * @param memberDto
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "registerMember")
	@ResponseBody
	public JsonModel registerMember(HttpServletRequest request, HttpServletResponse response,
			@RequestBody MemberDto memberDto) {
		JsonModel result = null;
		Map<String, Object> returnVal = null;
		Map<String, Object> data = null;
		RestParameter params = null;
		WebClient client = null;
		ApiResult apiResult = null;
		String openId = null;
		Cookie cookie = null;
		try {
			data = new HashMap<String, Object>();
			returnVal = new HashMap<String, Object>();
			openId = getOpenIdFromSession(request);
			client = createClient();
			memberDto.setBindingAccount(openId);
			memberDto.setExtAccountTypeId("ACCOUNT_TYPE_WECHAT");
			memberDto.setChannelId(202l); // 会员来源渠道(微信)
			String specialChannel = this.getChannelIdToSession(request);
			if(StringUtils.isNotBlank(specialChannel)){
				memberDto.setChannelId(Long.parseLong(specialChannel));
			}
			Long regStoreId = this.getRegStoreToSession(request);
			if(regStoreId != null){
				memberDto.setRegStore(regStoreId);
			}
			Long memberSrcId = this.getMemberSrcIdFromSession(request);
			if(memberSrcId != null){
				memberDto.setMemberSrcId(memberSrcId);
			}
			if(StringUtils.isBlank(memberDto.getBirthday())){
				result = ResultConstantsEnum.createFailResult();
				result.setMessage("生日不能为空");
				return result;
			}
			String language = getLanguageFromSession(request);
			if(StringUtils.isBlank(memberDto.getLanguage())){
				memberDto.setLanguage("cn");
			}else{
				setLanguageToSession(memberDto.getLanguage(), request);
			}
			params = new RestParameter(memberDto);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/member/registerMember").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			data.put("subscribe", getSubscribeFromSesstion(request));
			request.getSession().removeAttribute(SESSION_KEY_CHANNELID);
			request.getSession().removeAttribute(SESSION_KEY_REGSTORE);
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(data);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}

	/**
	 * @PE 个人信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showMemberInfo")
	@ResponseBody
	public JsonModel showMemberInfo(HttpServletRequest request, HttpServletResponse response) {
		JsonModel result = null;
		String openId = null;
		Map<String, Object> returnMapTemp = null;
		String originalMobile = "";
		try {
			openId = getOpenIdFromSession(request);
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("extAccountTypeId", "ACCOUNT_TYPE_WECHAT");
			tmp.put("bindingAccount", openId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("member/queryMemberByBindingAccountAndLevel" + params.getMatrixString());
			ApiResult apiResult = client.get(ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = new JsonModel();
				result.setCode(apiResult.getCode());
				result.setMessage(apiResult.getMessage());
				return result;
			}
			
			// 会员信息放入session
			JSONObject jsonMember = JSONObject.fromObject(apiResult.getData().get("member"));
			MemberExp exp = (MemberExp) JSONObject.toBean(jsonMember, MemberExp.class);
			if(StringUtils.isBlank(exp.getMemberPhoto())){
				exp.setMemberPhoto("./view/img/icon.png");
			}
			
			JSONObject jsonMemberCard = JSONObject.fromObject(apiResult.getData().get("memberCard"));
			MemberCard card = (MemberCard) JSONObject.toBean(jsonMemberCard, MemberCard.class);

			JSONObject jsonMemberAccount = JSONObject.fromObject(apiResult.getData().get("memberAccount"));
			MemberAccount account = (MemberAccount) JSONObject.toBean(jsonMemberAccount, MemberAccount.class);

			JSONObject jsonMemberCardGrade = JSONObject.fromObject(apiResult.getData().get("memberCardGrade"));
			MemberCardGrade cardGrade = (MemberCardGrade) JSONObject.toBean(jsonMemberCardGrade, MemberCardGrade.class);
			
			String nextPoints = apiResult.getData().get("nextPoints") == null ? "-1" : apiResult.getData().get("nextPoints").toString();
			 
			if(apiResult.getData().get("couponAmount") != null){
				exp.setCouponAmount(Long.parseLong(apiResult.getData().get("couponAmount").toString()));
			}else{
				exp.setCouponAmount(0L);
			}
			originalMobile = exp.getMobile();

			if (exp.getCountryCode().equals("0086")){
				originalMobile = "0086" + exp.getMobile();
			}
			exp.setCardNo(card.getCardNo());
			exp.setOriginalMobile(originalMobile);
			exp.setPointsBalance(account.getPointsBalance() == null ? new BigDecimal(0) : account.getPointsBalance());
			exp.setTotalPoints(account.getTotalPoints() == null ? new BigDecimal(0) : account.getTotalPoints());
			exp.setCardImg(cardGrade.getImgUrl());
			if (exp.getMobile() != null && exp.getCountryCode() != null && !exp.getCountryCode().isEmpty()) {
				exp.setMobile(exp.getMobile().replaceFirst("^"+exp.getCountryCode(), ""));
			}
			if (exp.getCountryCode().indexOf("00") == 0){
				exp.setCountryCode(exp.getCountryCode().replaceFirst("^00", ""));
			}
			if(exp.getChannelId() == 401L){
				exp.setCardImg("./view/img/black.png");
			}
			exp.setNextPoints(new BigDecimal(nextPoints));
			setMemberExpToSession(exp, request);
			//setOriginalMobileToSession(exp.getOriginalMobile(), request);
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(exp);
		} catch (Exception ex) {
			ex.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}

	/**
	 * @PE
	 * 注册发送验证码
	 * @param request
	 * @param response
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "sendVerifyCode")
	@ResponseBody
	public JsonModel sendVerifyCode(HttpServletRequest request, HttpServletResponse response,
			@RequestBody JSONObject requestParams) {
		JsonModel result = null;
		Map<String, String> map = null;
		RestParameter params = null;
		ApiResult apiResult = null;
		WebClient client = null;
		Map<String, Object> resturnMap = null;
		try {
			client = createClient();
			map = new HashMap<String, String>();
			if (!requestParams.containsKey("mobile")) {
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			map.put("mobile", requestParams.getString("mobile"));
			map.put("countryCode", requestParams.getString("countryCode"));
			map.put("channelCode", ChannelConstant.WEIXIN.getInstance().getChannelCode());
			map.put("isBinding", requestParams.getString("isBinding"));
			params = new RestParameter(map);
			//如果接口要加密
			//JSONObject jsonObject = JSONObject.fromObject(map);
			//String sign = MD5SecurityUtils.genSign(jsonObject, SIGN_KEY); 
			//client.header("signKey", sign);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("member/sendVerifyCode");
			apiResult = client.post(params.getJsonString(), ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				result.setCode(apiResult.getCode());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(apiResult.getData().get("captcha"));
		} catch (Exception ex) {
			ex.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	/**
	 * @PE
	 * 更改用户语言偏好
	 * @param request
	 * @param response
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "updateLanguage")
	@ResponseBody
	public JsonModel updateLanguage(HttpServletRequest request, HttpServletResponse response,
			@RequestBody JSONObject requestParams){
		JsonModel result = null;
		Map<String, Object> map = null;
		WebClient client = null;
		RestParameter params = null;
		ApiResult apiResult = null;
		try {
			client = createClient();
			map = new HashMap<String, Object>();
			if (!requestParams.containsKey("language")) {
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			setLanguageToSession(requestParams.getString("language"), request);
			MemberExp exp = getMemberExpFromSession(request);
			map.put("language", requestParams.getString("language"));
			map.put("memberId", exp.getMemberId());
			params = new RestParameter(map);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("member/updateLanguage" + params.getMatrixString());
			apiResult = client.post(params.getJsonString(), ApiResult.class);
			result = ResultConstantsEnum.createSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}

	/**
	 * @PE 编辑个人信息
	 * @param memberExp
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "updateMember")
	@ResponseBody
	public JsonModel updateMember(HttpServletRequest request) {
		List<String> filePathList = null;
		String filePath = null;
		JsonModel result = null;
		RestParameter params = null;
		WebClient client = null;
		ApiResult apiResult = null;
		MultipartHttpServletRequest multipartRequest = null;
		MemberExp memberExp = null;
		String birthday = null;
		String comvCode = null;
		String email = null;
		String gender = null;
		String geoId = null;
		String isHasChild = null;
		String memberName = null;
		String mobile = null;
		String ext_field_1 = null;
		String ext_field_2 = null;
		String ext_field_3 = null;
		String ext_field_4 = null;
		String ext_field_5 = null;
		String countryCode = null;
		try {
			memberExp = this.getMemberExpFromSession(request);
			//String originalMobile = getOriginalMobileToSession(request);
			//log.info("originalMobile: " + originalMobile);
			multipartRequest = (MultipartHttpServletRequest) request;
			comvCode = multipartRequest.getParameter("comvCode");
			email = multipartRequest.getParameter("email");
			gender = multipartRequest.getParameter("gender");
			geoId = multipartRequest.getParameter("geoId");
			isHasChild = multipartRequest.getParameter("isHasChild");
			memberName = multipartRequest.getParameter("memberName");
			mobile = multipartRequest.getParameter("mobile");
			ext_field_1 = multipartRequest.getParameter("extField1");
			ext_field_2 = multipartRequest.getParameter("extField2");
			ext_field_3 = multipartRequest.getParameter("extField3");
			ext_field_4 = multipartRequest.getParameter("extField4");
			ext_field_5 = multipartRequest.getParameter("extField5");
			countryCode = multipartRequest.getParameter("countryCode");
			log.info("card code:" + memberExp.getCardNo());
			log.info("original Mobile:" + memberExp.getOriginalMobile());
			client = createClient();
			if(StringUtils.isNotBlank(comvCode) && !comvCode.equals("null")){
				memberExp.setComvCode(comvCode);
			}
			if(StringUtils.isNotBlank(email) && !email.equals("null")){
				memberExp.setEmail(email);
			}
			if (StringUtils.isNotBlank(geoId) && !geoId.equals("null")) {
				memberExp.setGeoId(Long.valueOf(geoId));
			}
			if (StringUtils.isNotBlank(gender)) {
				memberExp.setGender(Long.valueOf(gender));
			}
			
			if (StringUtils.isNotBlank(ext_field_1) && !ext_field_1.equals("null") && !ext_field_1.equals("undefined")) {
				memberExp.setExtField1(ext_field_1);
			}
			
			if (StringUtils.isNotBlank(ext_field_2) && !ext_field_2.equals("null") && !ext_field_2.equals("undefined")) {
				memberExp.setExtField2(ext_field_2);
			}
			
			if (StringUtils.isNotBlank(ext_field_3) && !ext_field_3.equals("null") && !ext_field_3.equals("undefined")) {
				memberExp.setExtField3(ext_field_3);
			}
			
			if (StringUtils.isNotBlank(ext_field_4) && !ext_field_4.equals("null") && !ext_field_4.equals("undefined")) {
				memberExp.setExtField4(ext_field_4);
			}
			
			if (StringUtils.isNotBlank(ext_field_5) && !ext_field_5.equals("null") && !ext_field_5.equals("undefined")) {
				memberExp.setExtField5(ext_field_5);
			}
			//memberExp.setOriginalMobile(originalMobile);
			memberExp.setIsHasChild(isHasChild);
			memberExp.setMemberName(memberName);
			memberExp.setMobile(mobile);
			memberExp.setCountryCode(countryCode);
			filePathList = FileUtils.uploadImages(request);
			if (filePathList != null && filePathList.size() > 0) {
				filePath = filePathList.get(0);
				filePath = "http://" + domainName + request.getContextPath() + filePath;
				memberExp.setMemberPhoto(filePath);
			}
			params = new RestParameter(memberExp, "");
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/member/updateMember").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			apiResult = client.post(params.getJsonString(), ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
		} catch (Exception ex) {
			ex.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	
	/**
	 * 好友邀请
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "getInviteParams")
	@ResponseBody
	public JsonModel getInviteParams(HttpServletRequest request, HttpServletResponse response){
		JsonModel result = null;
		String link = null;
		String ticket = null;
		String linkUrl = null;
		Map<String, Object> map = null;
		Map<String, String> paramMap = null;
		try {
			
			String openId = null;
			openId = getOpenIdFromSession(request);
			WebClient client1 = createClient();
			Map<String, Object> tmp1 = new HashMap<String, Object>();
			tmp1.put("extAccountTypeId", "ACCOUNT_TYPE_WECHAT");
			tmp1.put("bindingAccount", openId);
			RestParameter params1 = new RestParameter(tmp1);
			client1.accept(MediaType.APPLICATION_JSON);
			client1.path("member/queryMemberByBindingAccountAndLevel" + params1.getMatrixString());
			ApiResult apiResult1 = client1.get(ApiResult.class);
			JSONObject jsonMember = JSONObject.fromObject(apiResult1.getData().get("member"));
			MemberExp exp = (MemberExp) JSONObject.toBean(jsonMember, MemberExp.class);
			
			link = peInviteReg.replace("APPID", appId).replace("INVITATIONID", exp.getMemberId().toString());
			String token = getAccessToken();
			if (StringUtils.isNotEmpty(token)) {
				WebClient client = createClient();
				Map<String, Object> tmp = new HashMap<String, Object>();
				tmp.put("token", token);
				RestParameter params = new RestParameter(tmp);
				client.accept(MediaType.APPLICATION_JSON);
				client.path("wechat/getAccessTicketCache" + params.getMatrixString());
				ApiResult apiResult = client.get(ApiResult.class);
				map = apiResult.getData();
				ticket = (String) map.get("ticket");
				paramMap = JsApiSignUtil.sign(ticket, linkUrl);
				paramMap.put("appId", appId);
				paramMap.put("link", link);
				paramMap.put("title","你离免费的披萨仅一步之遥！");
				paramMap.put("desc",exp.getMemberName() + "向你发射了一枚最高价值128元的免费披萨！热乎乎的披萨拿好，不送！");
				String logo = "http://" + domainName + "/" + projectName + "/view/img/logo.jpg";
				paramMap.put("imgUrl",logo);
				result = ResultConstantsEnum.createSuccessResult();
				result.setData(paramMap);
				return result;
			}
			result = ResultConstantsEnum.createFailResult();
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	
	/**
	 * @PE
	 * 绑定会员
	 * @param request
	 * @param response
	 * @param memberDto
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "bindingMember")
	@ResponseBody
	public JsonModel bindingMember(HttpServletRequest request, HttpServletResponse response,
			@RequestBody MemberDto memberDto) {
		JsonModel result = null;
		Map<String, Object> returnVal = null;
		RestParameter params = null;
		WebClient client = null;
		ApiResult apiResult = null;
		String openId = null;
		Cookie cookie = null;
		try {
			returnVal = new HashMap<String, Object>();
			openId = getOpenIdFromSession(request);
			client = createClient();
			memberDto.setBindingAccount(openId);
			memberDto.setExtAccountTypeId("ACCOUNT_TYPE_WECHAT");
			String language = getLanguageFromSession(request);
			if(StringUtils.isBlank(memberDto.getLanguage())){
				memberDto.setLanguage("cn");
			}else{
				setLanguageToSession(memberDto.getLanguage(), request);
			}
			params = new RestParameter(memberDto);
			client.path("/member/bindingMember");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				result.setCode(apiResult.getCode());
				return result;
			}
			returnVal.put("message", apiResult.getMessage());
			returnVal.put("code", apiResult.getCode());
			result = ResultConstantsEnum.createSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}

}
