package com.bw.adv.controller.base;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.HttpKit;
import com.bw.adv.common.utils.JsApiSignUtil;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;

@Controller
@RequestMapping("/baseController")
public class BaseController {

	protected static final Logger LOGGER = Logger.getLogger(BaseController.class);

	@Autowired
	private final static int maxAge = 3600*24*7;//一周
	private final static int maxSessionTimes = 60 * 60 * 24 * 7;
	protected static final String API_SUCCESS_CODE = "200";
	@Value("${access_token_url}")
	protected String AccessTokenUrl;
	@Value("${appId}")
	protected String appId;
	@Value("${appSecret}")
	protected String appSecret ;
	@Value("${webClient_url}")
	protected String WEBCLIENT_URL;
	@Value("${get_userinfo_url}")
	protected String GET_USERINFO_URL;
	@Value("${domainName}")
	protected  String domainName;
	@Value("${imageUrl}")
	protected String imageUrl;
	@Value("${check_sign}")
	protected String CHECK_SIGN;
	@Value("${sign_key}")
	protected String SIGN_KEY;
	@Value("${pe_invite_reg}")
	protected String peInviteReg;
	
	@Value("${projectName}")
	protected String projectName;
	private static final Logger logger = Logger.getLogger(BaseController.class);

	public static final String COOKIE_OPENID_KEY = "cookie_openid_key_bw";//cookieOpenId
	public static final String COOKIE_HEAD_IMG_URL = "cookie_head_img_url";//cookie_head_img_url
	public static final String COOKIE_NICK_NAME = "cookie_nick_name";//cookie_nick_name
	public static final String COOKIE_MEMBER_ID = "cookie_member_bw_id";//cookie_member_id
	public static final String COOKIE_QRCODE_ID = "cookie_qrcode_id";//cookie_member_id
	public static final String COOKIE_MAPSQRCODE_ID = "cookie_mapsqrcode_id";//cookie_member_id
	
	// -----------------pe------------------//
	protected static final String SESSION_KEY_OPENID = "session_key_openId";
	protected static final String SESSION_KEY_MEMBEREXP = "session_key_memberExp";
	protected static final String SESSION_KEY_CHANNELID = "session_key_channelId";
	protected static final String SESSION_KEY_REGSTORE = "session_key_regStore";
	protected static final String SESSION_KEY_SRC = "session_key_src";
	protected static final String SESSION_KEY_LANGUAGE = "session_key_language";
	protected static final String SESSION_KEY_SUBSCRIBE = "session_key_subscribe";
	protected static final String DES_KEY = "a1b2c3d4";

	/**
	 * 创建客户端链接
	 * 
	 * @return
	 */
	public WebClient createClient() {
		return createBkClient(WEBCLIENT_URL);
	}

	public Map<String, Object> saveWeChatFans(String openId, String headImgUrl, Map<String, Object> returnMapTemp) {
		return toSaveWeChatFans(openId, headImgUrl, null, returnMapTemp);
	}

	public Map<String, Object> saveFansWithChannel(String openId, String headImgUrl, Long channelId,
			Map<String, Object> returnMapTemp) {
		return toSaveWeChatFans(openId, headImgUrl, channelId, returnMapTemp);
	}

	private Map<String, Object> toSaveWeChatFans(String openId, String headImgUrl, Long channelId,
			Map<String, Object> returnMapTemp) {
		Map<String, Object> returnVal = null;
		try {
			returnVal = new HashMap<String, Object>();
			WebClient client = createClient();
			MemberDto memberDto = new MemberDto();

			memberDto.setExtAccountTypeId("ACCOUNT_TYPE_WECHAT");// 外部账号类型(微信)
			memberDto.setBindingAccount(openId);// 微信openId
			if (null == channelId) {
				memberDto.setChannelId(202l); // 会员来源渠道(微信)
			} else {
				memberDto.setChannelId(channelId);
			}
			if (StringUtils.isNotBlank(headImgUrl)) {
				memberDto.setMemberPhoto(headImgUrl);// 会员头像
			}
			if (returnMapTemp != null) {
				memberDto.setUnionid((String) returnMapTemp.get("unionid"));
				memberDto.setNickName((String) returnMapTemp.get("nickName"));
				memberDto.setSex((String) returnMapTemp.get("sex"));
				memberDto.setCty((String) returnMapTemp.get("cty"));
				memberDto.setProvince((String) returnMapTemp.get("province"));
				memberDto.setCountry((String) returnMapTemp.get("country"));
				memberDto.setLanguage((String) returnMapTemp.get("language"));
			}

			RestParameter params = new RestParameter(memberDto);

			client.accept(MediaType.APPLICATION_JSON);
			client.path("/member/weChatFocusEvent").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");

			ApiResult result = client.post(params.getJsonString(), ApiResult.class);
			returnVal.put("wechatFans", result.getData());
			returnVal.put("memberId", result.getData().get("memberId"));
			returnVal.put("memberCode", result.getData().get("memberCode"));
			returnVal.put("message", result.getMessage());
			returnVal.put("code", result.getCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnVal;
	}

	/**
	 * 获取微信信息
	 * @param openId
	 * @return
	 */
	public Map<String, Object> getWechatInfo(String openId) {
		Map<String, Object> resultMap = null;
		try {
			resultMap = new HashMap<String, Object>();
			String getUserInfoUrl = GET_USERINFO_URL.replace("ACCESS_TOKEN", getAccessToken()).replace("OPENID", openId);
			String userInfo = HttpKit.get(getUserInfoUrl);
			JSONObject resultJson = JSONObject.fromObject(userInfo);
			String headImgUrl = (String) resultJson.get("headimgurl");
			String unionid = (String) resultJson.get("unionid");
			String nickName = (String) resultJson.get("nickname");
			String sex = resultJson.get("sex") == null ? null : resultJson.get("sex").toString();
			String cty = (String) resultJson.get("city");
			String province = (String) resultJson.get("province");
			String country = (String) resultJson.get("country");
			String language = (String) resultJson.get("language");
			String subscribe = (resultJson.get("subscribe") + "").toString();
			String getInfoType = "2";
			resultMap.put("getInfoType", getInfoType);
			resultMap.put("headImgUrl", headImgUrl);
			resultMap.put("unionid", unionid);
			resultMap.put("nickName", nickName);
			resultMap.put("sex", sex);
			resultMap.put("cty", cty);
			resultMap.put("province", province);
			resultMap.put("country", country);
			resultMap.put("language", language);
			resultMap.put("openId", openId);
			resultMap.put("subscribe", subscribe);
			System.out.println("resultJson"+resultJson+"||"+subscribe);
		} catch (Exception e) {
			LOGGER.error("AccessTokenUrl:" + AccessTokenUrl + ",appId:" + appId, e);
		}
		return resultMap;
	}

	/**
	 * getOpenIdByCode
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public Map<String, Object> getOpenIdByCode(HttpServletRequest request, HttpServletResponse response) {
		String code = null;
		String accessTokenUrl = null;
		String result = null;
		JSONObject jsonObject = null;
		String openId = null;

		Map<String, Object> resultMap = null;
		try {
			resultMap = new HashMap<String, Object>();

			if (StringUtils.isBlank(openId)) {
				code = request.getParameter("code");
				accessTokenUrl = AccessTokenUrl.replace("APPID", appId).replace("SECRET", appSecret).replace("CODE",
						code);

				result = HttpKit.get(accessTokenUrl);
				jsonObject = JSONObject.fromObject(result);
				openId = (String) jsonObject.get("openid");
			}
			resultMap.put("openId", openId);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultMap;
	}
	
	protected Member queryBindingAccountByOpenId(String openId){
		try {
		// 查询是否新粉丝
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("extAccountTypeId", "ACCOUNT_TYPE_WECHAT");
			tmp.put("bindingAccount", openId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("member/queryMemberByBindingAccount" + params.getMatrixString());
			ApiResult resultTemp = client.get(ApiResult.class);
			if(resultTemp.getCode().equals(API_SUCCESS_CODE)){
				Map<String, Object> data = resultTemp.getData();
				Member member=new Member();
				member.setMemberId(Long.parseLong(String.valueOf(data.get("memberId"))));
				return member;
			}else{
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 调取accessToken接口
	 */
	public String getAccessToken() {
		Map<String, Object> map = null;
		String accessToken = null;
		try {
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();

			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("wechat/getAccessToken" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			map = result.getData();
			accessToken = (String) map.get("accessToken");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return accessToken;
	}

	/**
	 * @PE
	 * 获取微信菜单参数@PE
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "getWechatParams")
	@ResponseBody
	public JsonModel getWechatParams(HttpServletRequest request, HttpServletResponse response){
		JsonModel result = null;
		String link = null;
		String ticket = null;
		String linkUrl = null;
		Map<String, Object> map = null;
		Map<String, String> paramMap = null;
		try {
			linkUrl = "http://" + domainName + "/" + projectName + "/index.html";
			link = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=" + link
					+ "&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
			String token = getAccessToken();
			if (StringUtils.isNotEmpty(token)) {
				WebClient client = createClient();
				Map<String, Object> tmp = new HashMap<String, Object>();
				tmp.put("token", token);
				RestParameter params = new RestParameter(tmp);
				client.accept(MediaType.APPLICATION_JSON);
				client.path("wechat/getAccessTicketCache" + params.getMatrixString());
				ApiResult apiResult = client.get(ApiResult.class);
				map = apiResult.getData();
				ticket = (String) map.get("ticket");
				paramMap = JsApiSignUtil.sign(ticket, linkUrl);
				paramMap.put("appId", appId);
				result = ResultConstantsEnum.createSuccessResult();
				result.setData(paramMap);
				return result;
			}
			result = ResultConstantsEnum.createFailResult();
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	/*
	 * 获取微信自定义右上角菜单的签各
	 */
	public Map<String, String> getSign(String url) {
		Map<String, String> paramMap = null;
		Map<String, Object> map = null;
		String token = null;
		String ticket = null;
		try {
			token = getAccessToken();
			if (StringUtils.isNotEmpty(token)) {
				WebClient client = createClient();
				Map<String, Object> tmp = new HashMap<String, Object>();
				tmp.put("token", token);
				RestParameter params = new RestParameter(tmp);
				client.accept(MediaType.APPLICATION_JSON);
				client.path("wechat/getAccessTicketCache" + params.getMatrixString());
				ApiResult result = client.get(ApiResult.class);
				map = result.getData();
				ticket = (String) map.get("ticket");
			}
			paramMap = JsApiSignUtil.sign(ticket, url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paramMap;
	}

	/*
	 * 优惠券领取记录
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveCouponReceiveRecord")
	@ResponseBody
	public Map<String, String> saveCouponReceiveRecord(HttpServletRequest request, HttpServletResponse response) {
		String couponInstanceCode = null;
		Map<String, String> resultMap = null;
		String openId = null;
		String code = "";
		try {
			couponInstanceCode = request.getParameter("couponInstanceCode");
			openId = request.getParameter("openId");
			resultMap = new HashMap<String, String>();
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("couponInstanceCode", couponInstanceCode);
			tmp.put("openId", openId);
			tmp.put("type", "receive");
			tmp.put("preOpenId", "");
			tmp.put("seq", "");
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("couponBk/handleCouponInstance" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			tmp = result.getData();
			code = (String) tmp.get("code");
			if (code.equals("100")) {
				resultMap.put("appId", appId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		resultMap.put("code", code);
		return resultMap;
	}

	/*
	 * 优惠券转发记录
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveCouponRelayRecord")
	@ResponseBody
	public Map<String, String> saveCouponRelayRecord(HttpServletRequest request, HttpServletResponse response) {
		String couponInstanceCode = null;
		String preOpenId = null;
		String openId = null;
		String seq = null;
		String code = "";
		try {
			couponInstanceCode = request.getParameter("couponInstanceCode");
			preOpenId = request.getParameter("preOpenId");
			openId = request.getParameter("openId");
			seq = request.getParameter("seq");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("couponInstanceCode", couponInstanceCode);
			tmp.put("openId", openId);
			tmp.put("type", "relay");
			tmp.put("preOpenId", preOpenId);
			tmp.put("seq", seq);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("couponBk/handleCouponInstance" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			tmp = result.getData();
			code = (String) tmp.get("code");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Map<String, String> resultMap = new HashMap<String, String>();
		resultMap.put("code", code);
		return resultMap;
	}
	/**
	 * addMemberByOpenId:根据openid查会员加到cookie <br/>
	 * Date: 2016幄1�7�1�7旄1�7 下午10:20:02 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param response
	 * @param openId
	 */
	public void addMemberByOpenId(HttpServletResponse response,String openId){
		ApiResult resultdata = null;
		Map<String, Object> mapdata = null;
		JSONObject jsonObject = null;
		WebClient client = null;
		try {
			mapdata = new HashMap<String, Object>();
			client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("extAccountTypeId", "ACCOUNT_TYPE_WECHAT");
			tmp.put("bindingAccount",openId);

			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("member/queryMemberByBindingAccount" + params.getMatrixString());
			resultdata = client.get(ApiResult.class);
			mapdata = resultdata.getData();
			if(mapdata != null){
				jsonObject = JSONObject.fromObject(mapdata);
				addCookie(response,"cookieMember",jsonObject.toString());
			}
			addCookie(response,"cookieOpenId",openId);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			closeClient(client);
		}
	}
	/**
	 * 获取openId
	 * @return
	 */
	public Map<String,Object> getOpenId(HttpServletRequest request,HttpServletResponse response){
		String code  =null;
		String accessTokenUrl =null;
		String result =null;
		JSONObject jsonObject =null;
		String openId =null;
		String getUserInfoUrl =null;

		String headImgUrl =null;
		String unionid = null;
		String nickName = null;
		String sex = null;
		String cty = null;
		String province = null;
		String country = null;
		String language = null;

		String userInfo = null;

		String getInfoType = null;

		JSONObject resultJson = null;
		Map<String, Object> resultMap = null;
		try {
			resultMap = new HashMap<String,Object>();
			openId = (String)this.getSession(SESSION_KEY_OPENID , request);
			if (openId != null){
				resultMap.put("openId", openId);
			}
			/*if(StringUtils.isNotBlank(request.getParameter("openId"))){
				openId =  request.getParameter("openId");
				headImgUrl =  request.getParameter("headImgUrl");
			}
			if(getCookieByName(request, COOKIE_OPENID_KEY) != null){
				openId = getCookieByName(request, COOKIE_OPENID_KEY).getValue();
				resultMap.put("openId", openId);
				getInfoType = "1";
			}

			if(getCookieByName(request, COOKIE_HEAD_IMG_URL) != null){
				headImgUrl = URLDecoder.decode(getCookieByName(request, COOKIE_HEAD_IMG_URL).getValue(),"utf-8");
				if(getCookieByName(request, COOKIE_NICK_NAME)!= null){
					nickName = URLDecoder.decode(getCookieByName(request, COOKIE_NICK_NAME).getValue(),"utf-8");
				}
				resultMap.put("headImgUrl", headImgUrl);
			}

			if(StringUtils.isBlank(openId)){
				code = request.getParameter("code");
				if(StringUtils.isBlank(code)|| StringUtils.isBlank(appId)|| StringUtils.isBlank(appSecret )){
					throw new MemberException("获取微信参数有误"+"code:"+code+" appId:"+ appId +" appSecret:"+ appSecret);
				}
				accessTokenUrl = AccessTokenUrl.replace("APPID", appId).replace("SECRET", appSecret).replace("CODE", code);

				result = HttpKit.get(accessTokenUrl);
				jsonObject = JSONObject.fromObject(result);
				openId = (String)jsonObject.get("openid");

				if(StringUtils.isBlank(openId)){
					throw new MemberException("获取openid为空:"+result);
				}
				getUserInfoUrl = GET_USERINFO_URL.replace("ACCESS_TOKEN", getAccessToken()).replace("OPENID", openId);
				userInfo = HttpKit.get(getUserInfoUrl);
				resultJson = JSONObject.fromObject(userInfo);

				headImgUrl = (String)resultJson.get("headimgurl");
				unionid = (String)resultJson.get("unionid");
				nickName = (String)resultJson.get("nickname");
				sex = resultJson.get("sex")==null?null:resultJson.get("sex").toString();
				cty = (String)resultJson.get("city");
				province = (String)resultJson.get("province");
				country = (String)resultJson.get("country");
				language = (String)resultJson.get("language");
				getInfoType = "2";
			}

			openId = "ooUWCuCUDtWBK-WVDMmTfDRb_DsE";
			headImgUrl = "http://wx.qlogo.cn/mmopen/eguu63QsFlHheXXXZSp5DPyQBQe63eFic862bxXd4yfCkOWYL3yD7lyXnYlQ2eIaHmjbe9aEJ7hNF1HM0CnMSePDqT0Xq3x11/0";

			if(getCookieByName(request, "cookieMember") == null){
				addMemberByOpenId(response, openId);
			}
			addCookie(response,COOKIE_NICK_NAME,nickName);
			addCookie(response,COOKIE_OPENID_KEY,openId);
			addCookie(response,COOKIE_HEAD_IMG_URL,headImgUrl);

			resultMap.put("getInfoType", getInfoType);
			resultMap.put("openId", openId);
			resultMap.put("headImgUrl", headImgUrl);
			resultMap.put("unionid", unionid);
			resultMap.put("nickName", nickName);
			resultMap.put("sex", sex);
			resultMap.put("cty", cty);
			resultMap.put("province", province);
			resultMap.put("country", country);
			resultMap.put("language", language);
			*/
		} catch (Exception e) {
			System.out.println("AccessTokenUrl:"+AccessTokenUrl+",appId:"+appId+","+result);
			e.printStackTrace();
		}
		return resultMap;
	}
	/**
	 * getMemberByOpenId:根据OPENID查会员并加载到Cookie <br/>
	 * Date: 2016幄1�7�1�7旄1�7 下午4:07:48 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "getMemberByOpenId")
	public Map<String, Object> getMemberByOpenId(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> result = null;
		String openId = null;
		Map<String, Object> mapdata = null;
		JSONObject jsonObject = null;
		ApiResult resultdata = null;
		WebClient client = null;
		try {
			result = new HashMap<String, Object>();
			mapdata = new HashMap<String, Object>();
			openId= getOpenIdFromSession(request);
			LOGGER.info("exchange_code openid: " + openId);
			client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("extAccountTypeId", "ACCOUNT_TYPE_WECHAT");
			tmp.put("bindingAccount",openId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("member/queryMemberByBindingAccount" + params.getMatrixString());
			resultdata = client.get(ApiResult.class);
			mapdata = resultdata.getData();
			if(mapdata != null){
				jsonObject = JSONObject.fromObject(mapdata);
				//addCookie(response,"cookieMember",jsonObject.toString());
			}
			//addCookie(response,"cookieOpenId",openId);
			result.put("apiResult", resultdata);
			result.put("openId", openId);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			closeClient(client);
		}
		return result;
	}
	/**
	 * 设置cookie
	 * @param response
	 * @param name  cookie名字
	 * @param value cookie值
	 */
	public static void addCookie(HttpServletResponse response,String name,String value){
		try {
			if(StringUtils.isBlank(value) || StringUtils.isBlank(name)){
				return;
			}
			Cookie cookie = new Cookie(name, URLEncoder.encode(value,"utf-8"));

			cookie.setPath("/");
			if(maxAge>0){
				cookie.setMaxAge(maxAge);
			}
			response.addCookie(cookie);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	/**
	 * 根据名字获取cookie
	 * @param request
	 * @param name cookie名字
	 * @return
	 */
	public static Cookie getCookieByName(HttpServletRequest request, String name){
	    Map<String,Cookie> cookieMap = ReadCookieMap(request);
	    if(cookieMap.containsKey(name)){
	        Cookie cookie = (Cookie)cookieMap.get(name);
	        return cookie;
	    }else{
	        return null;
	    }
	}

	/**
	 * 将cookie封装到Map里面
	 * @param request
	 * @return
	 */
	private static Map<String,Cookie> ReadCookieMap(HttpServletRequest request){
		Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
		Cookie[] cookies = request.getCookies();
		if(null!=cookies){
			for(Cookie cookie : cookies){
				cookieMap.put(cookie.getName(), cookie);
			}
		}
		return cookieMap;
	}
	/**
	 * 创建客户端链接
	 * 
	 * @param url
	 * @return
	 */
	private WebClient createBkClient(String url) {
		List<Object> provides = null;
		WebClient client = null;
		provides = new ArrayList<Object>();
		provides.add(new JacksonJsonProvider());
		client = WebClient.create(url, provides);
		client.header("Content-Type", MediaType.APPLICATION_JSON);
		return client;
	}
	public void closeClient(WebClient webClient){
		try {
			if(webClient!=null){
				webClient.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * @PE
	 * 从session中获取当前用户语言偏好
	 * @param request
	 * @return
	 */
	public String getLanguageFromSession(HttpServletRequest request){
		String language = null;
		try {
			language = (String) this.getSession(SESSION_KEY_LANGUAGE, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return language;
	}
	
	/**
	 * 更新当前用户语言偏好
	 * @param language
	 * @param request
	 */
	public void setLanguageToSession(String language, HttpServletRequest request) {
		try {
			this.setSession(SESSION_KEY_LANGUAGE, language, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @PE
	 * 从session中获取当前用户openId
	 * @param request
	 * @return
	 */
	public String getOpenIdFromSession(HttpServletRequest request) {
		String openId = null;
		try {
			openId = (String) this.getSession(SESSION_KEY_OPENID, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openId;
	}
	
	/**
	 * @PE
	 * 更新当前用户session中openId
	 * @param openId
	 * @param request
	 */
	public void setOpenIdToSession(String openId, HttpServletRequest request) {
		try {
			this.setSession(SESSION_KEY_OPENID, openId, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * 保存用户邀请人
	 * @param memberSrcId
	 * @param request
	 */
	public void setMemberSrcIdToSession(String memberSrcId, HttpServletRequest request){
		try {
			this.setSession(SESSION_KEY_SRC, memberSrcId, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取用户邀请人
	 * @param request
	 * @return
	 */
	public Long getMemberSrcIdFromSession(HttpServletRequest request){
		Long memberSrcId = null;
		try {
			if(this.getSession(SESSION_KEY_SRC, request) != null){
				memberSrcId = Long.parseLong(this.getSession(SESSION_KEY_SRC, request).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberSrcId;
	}
	
	/**
	 * 保存用户注册特殊渠道
	 * @param channelId
	 * @param request
	 */
	public void setChannelIdToSession(String channelId, HttpServletRequest request){
		try {
			this.setSession(SESSION_KEY_CHANNELID, channelId, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 读取用户注册特殊渠道
	 * @param channelId
	 * @param request
	 */
	public String getChannelIdToSession(HttpServletRequest request){
		String channelId = null;
		try {
			channelId = (String) this.getSession(SESSION_KEY_CHANNELID, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return channelId;
	}
	
	/**
	 * 保存用户关注状态
	 * @param subscribe
	 * @param request
	 */
	public void setSubscribe(String subscribe, HttpServletRequest request){
		try {
			this.setSession(SESSION_KEY_SUBSCRIBE, subscribe, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 读取用户关注状态
	 * @param request
	 * @return
	 */
	public String getSubscribeFromSesstion(HttpServletRequest request){
		String subscribe = null;
		try {
			subscribe = (String) this.getSession(SESSION_KEY_SUBSCRIBE, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subscribe;
	}
	
	/**
	 * 保存用户注册门店id
	 * @param storeId
	 * @param request
	 */
	public void setRegStoreToSession(String storeId, HttpServletRequest request){
		try {
			this.setSession(SESSION_KEY_REGSTORE, storeId, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 读取用户注册门店id
	 * @param storeId
	 * @param request
	 */
	public Long getRegStoreToSession(HttpServletRequest request){
		Long storeId = null;
		try {
			if(this.getSession(SESSION_KEY_REGSTORE, request) != null){
				storeId = Long.parseLong(this.getSession(SESSION_KEY_REGSTORE, request).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return storeId;
	}
	
	
	/**
	 * @PE
	 * 从当前用户中获取用户信息
	 * @param request
	 * @return
	 */
	public MemberExp getMemberExpFromSession(HttpServletRequest request) {
		MemberExp exp = null;
		try {
			exp = (MemberExp) this.getSession(SESSION_KEY_MEMBEREXP, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return exp;
	}

	/**
	 * @PE
	 * 更新当前用户session中的memberExp
	 * @param exp
	 * @param request
	 */
	public void setMemberExpToSession(MemberExp exp, HttpServletRequest request) {
		try {
			this.setSession(SESSION_KEY_MEMBEREXP, exp, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void setOriginalMobileToSession(String originalMobile, HttpServletRequest request) {
		try{
			this.setSession("originalMobile", originalMobile, request);
		} catch (Exception e){

		}
	}public String getOriginalMobileToSession(HttpServletRequest request) {
		String originalMobile = "";
		try{
			originalMobile = (String)this.getSession("originalMobile",  request);
		} catch (Exception e){

		}
		return originalMobile;
	}
	/**
	 * @PE
	 * 在session中添加对应的k-v
	 * @param request
	 * @param response
	 */
	private void setSession(String name, Object value, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			session.setAttribute(name, value);
			session.setMaxInactiveInterval(maxSessionTimes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @PE
	 * 根据name获取session中的值
	 * @param name
	 * @param request
	 * @return
	 */
	private Object getSession(String name, HttpServletRequest request) {
		Object object = null;
		try {
			object = new Object();
			object = request.getSession().getAttribute(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

}
