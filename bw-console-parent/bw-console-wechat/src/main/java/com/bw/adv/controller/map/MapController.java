package com.bw.adv.controller.map;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.HttpKit;
import com.bw.adv.common.utils.JsApiSignUtil;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;


@Controller
@RequestMapping("/mapController")
public class MapController  extends BaseController{
	
	// 周边检索
	@Value("${nearby_url}")
	private String NEARBYURL;
	// GeocodingAPI 提供从地址到经纬度坐标的转换服务
	@Value("${geoCodeing_url}")
	private String GEOCODEINGURL;
	// AK
	@Value("${AK}")
	private String AK;
	// 本地检索
	@Value("${local_url}")
	private String LOCALURL;
	// tableId 
	@Value("${TABLEID}")
	private String TABLEID;
	
	@RequestMapping(method = RequestMethod.POST, value = "/nearbyShopsByLocation")
	@ResponseBody
	public JsonModel nearbyShopsByLocation(HttpServletRequest request) throws KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException, UnsupportedEncodingException, IOException, ExecutionException, InterruptedException{
		JsonModel jsonModel = null;
		String latitude =null;
		String longitude =null;
		String location =null;
		String location1 =null;
		String result =null;
		String localResult =null;
		String region =null;
		String geoCodeResult =null;
		JSONObject localResultJson =null;
		JSONObject resultJson =null;
		JSONObject geoCodeResultJson =null;
		Map<String,Object> returnMap =null;
		JSONArray jsonArray =null;
		JSONArray localJsonArray =null;
		JSONArray transferLocalJsonArray =null;
		String nearByCity =null;// 附近城市
		String currentPosition =null;// 当前定位
		JSONObject geoJsonObject =null;
		JSONObject addressJson =null;
		Map<Integer,Object> map =null;
		List<Integer> list =null;
		
		String localUrl =null;// 本地检索
		String nearbyUrl =null;// 周边检索
		String geoCodingUrl =null;// 提供从地址到经纬度坐标的转换服务
		
		try {
			list =new ArrayList<Integer>();
			map =new HashMap<Integer,Object>();
			returnMap =new HashMap<String,Object>();
			transferLocalJsonArray =new JSONArray();
			latitude = request.getParameter("latitude");
			longitude = request.getParameter("longitude");
			region = request.getParameter("city");
			location=longitude+","+latitude;
			location1 =latitude+","+longitude;
			
			localUrl =LOCALURL.replace("REGION", region).replace("AK", AK).replace("TABLEID", TABLEID);
			nearbyUrl =NEARBYURL.replace("AK", AK).replace("Location", location).replace("TABLEID", TABLEID);
			geoCodingUrl =GEOCODEINGURL.replace("AK", AK).replace("Location", location1);
			
			result = HttpKit.get(nearbyUrl);
			geoCodeResult = HttpKit.get(geoCodingUrl);
			
			resultJson = JSONObject.fromObject(result);
			geoCodeResultJson = JSONObject.fromObject(geoCodeResult);
			
			if(resultJson.getString("status").equals("0")){
				jsonArray = resultJson.getJSONArray("contents");
			}
			
			if(geoCodeResultJson.getString("status").equals("0")){
				geoJsonObject = geoCodeResultJson.getJSONObject("result");
				// 当前位置
				currentPosition =(String)geoJsonObject.get("formatted_address");
				// 当前城市json数组
				addressJson =(JSONObject)geoJsonObject.get("addressComponent");
				// 当前城市
				nearByCity =addressJson.getString("city");
			}
			
			if(region !=null && region!="" && !region.equals("null")){
				jsonArray.clear();
				localResult = HttpKit.get(localUrl);
				localResultJson = JSONObject.fromObject(localResult);
				if(localResultJson.getString("status").equals("0")){
					localJsonArray = localResultJson.getJSONArray("contents");
					for(int i =0;i<localJsonArray.size();i++){
						JSONObject obj = localJsonArray.getJSONObject(i);
						JSONArray strs = obj.getJSONArray("location");
						String log2 = (Double)strs.get(0)+"";
						String lat2 = (Double)strs.get(1)+"";
						double distance = Distance(new Double(longitude),new Double(latitude),new Double(log2),new Double(lat2));
						obj.put("distance", (int)distance);
						transferLocalJsonArray.add(obj);
					}
					// 根据distance从近到远
					for(int i =0;i<transferLocalJsonArray.size();i++){
						JSONObject obj = transferLocalJsonArray.getJSONObject(i);
						map.put(obj.getInt("distance"), obj);
					}
					for(Integer strInteger:map.keySet()){
						list.add(strInteger);
					}
					Collections.sort(list);
					for(Integer i:list){
						JSONObject jsonObject = (JSONObject)map.get(i);
						jsonArray.add(jsonObject);
					}
				}
				nearByCity =region;
			}
			jsonModel = ResultConstantsEnum.createSuccessResult();
			returnMap.put("contents", jsonArray);
			returnMap.put("currentPosition", currentPosition);
			returnMap.put("nearByCity", nearByCity);
			jsonModel.setData(returnMap);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
//		}finally{
//			returnMap.put("contents", jsonArray);
//			returnMap.put("currentPosition", currentPosition);
//			returnMap.put("nearByCity", nearByCity);
//			jsonModel = ResultConstantsEnum.createSuccessResult();
//			jsonModel.setData(returnMap);
//		}
		return jsonModel;
	}
	
	/**
	 * @PE
	 * 获取微信菜单参数@PE
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "getWechatParams")
	@ResponseBody
	public JsonModel getWechatParams(HttpServletRequest request, HttpServletResponse response){
		JsonModel result = null;
		String ticket = null;
		String linkUrl = null;
		Map<String, Object> map = null;
		Map<String, String> paramMap = null;
		try {
			linkUrl = "http://" + domainName + "/" + projectName + "/view/nearby.html";
			String token = getAccessToken();
			if (StringUtils.isNotEmpty(token)) {
				WebClient client = createClient();
				Map<String, Object> tmp = new HashMap<String, Object>();
				tmp.put("token", token);
				RestParameter params = new RestParameter(tmp);
				client.accept(MediaType.APPLICATION_JSON);
				client.path("wechat/getAccessTicketCache" + params.getMatrixString());
				ApiResult apiResult = client.get(ApiResult.class);
				map = apiResult.getData();
				ticket = (String) map.get("ticket");
				paramMap = JsApiSignUtil.sign(ticket, linkUrl);
				paramMap.put("appId", appId);
				result = ResultConstantsEnum.createSuccessResult();
				result.setData(paramMap);
				return result;
			}
			result = ResultConstantsEnum.createFailResult();
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	
	public static String convert(String utfString){  
	    StringBuilder sb = new StringBuilder();  
	    int i = -1;  
	    int pos = 0;  
	      
	    while((i=utfString.indexOf("\\u", pos)) != -1){  
	        sb.append(utfString.substring(pos, i));  
	        if(i+5 < utfString.length()){  
	            pos = i+6;  
	            sb.append((char)Integer.parseInt(utfString.substring(i+2, i+6), 16));  
	        }  
	    }  
	      
	    return sb.toString();  
	} 
	
	/**
	 * 计算两点的经纬度
	 * @param long1
	 * @param lat1
	 * @param long2
	 * @param lat2
	 * @return
	 */
	public static double Distance(double long1, double lat1, double long2,  double lat2) {  
	    double a, b, R;  
	    R = 6378137; // 地球半径  
	    lat1 = lat1 * Math.PI / 180.0;  
	    lat2 = lat2 * Math.PI / 180.0;  
	    a = lat1 - lat2;  
	    b = (long1 - long2) * Math.PI / 180.0;  
	    double d;  
	    double sa2, sb2;  
	    sa2 = Math.sin(a / 2.0);  
	    sb2 = Math.sin(b / 2.0);  
	    d = 2  
	            * R  
	            * Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(lat1)  
	                    * Math.cos(lat2) * sb2 * sb2));  
	    return d; 
	
	}
	
}	
