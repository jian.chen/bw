/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:PointsItemController.java
 * Package Name:com.sage.scrm.controller.points
 * Date:2015年9月1日下午2:27:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.points;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.tools.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/pointsController")
public class PointsController extends BaseController {
	private static final Logger LOGGER = Logger.getLogger(PointsController.class);

	/**
	 * 查询会员积分
	 * @param request
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findMemberPointsItem")
	@ResponseBody
	public JsonModel findMemberPointsItem(HttpServletRequest request, @RequestBody JSONObject requestParams) {
		JsonModel result = null;
		Map<String, Object> tmp = null;
		String page = null;
		String rows = null;
		WebClient client = null;
		RestParameter params = null;
		MemberExp member = null;
		ApiResult apiResult = null;
		Map<String, Object> resultMap = null;
		try {
			member = getMemberExpFromSession(request);
			String memberCode = member.getMemberCode();
			if(!(requestParams.containsKey("pageNo") && requestParams.containsKey("pageSize"))){
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			client = createClient();
			resultMap = new HashMap<>();
			tmp = new HashMap<String, Object>();
			page = requestParams.getString("pageNo");
			rows = requestParams.getString("pageSize");
			tmp.put("memberCode", memberCode);
			tmp.put("pages", page);
			tmp.put("rows", rows);
			params = new RestParameter(tmp, "b146389c1ac562997c1463a8bff28c83");
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/points/getMemberPointsInfo");
			apiResult = client.post(params.getJsonString(), ApiResult.class);
			Map<String, Object> data =  apiResult.getData();
			String language = getLanguageFromSession(request);
			if(StringUtils.isNotBlank(language) && language.equals("en")){
				data.put("gradeName", data.get("gradeNameEn"));
			}
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
			resultMap.put("items", apiResult.getItems());
			resultMap.put("data", data);
			result.setData(resultMap);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}


	/**
	 * @PE
	 * 积分商城兑换商品列表
	 * @param request
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findPointsProduct")
	@ResponseBody
	public JsonModel findPointsProduct(HttpServletRequest request, @RequestBody com.alibaba.fastjson.JSONObject requestParams) {
		JsonModel jsonModel = null;
		Map<String, Object> tmp = null;
		String page = null;
		String rows = null;
		WebClient client = null;
		RestParameter params = null;
		ApiResult result = null;
		String isExt = null;
		List<Map<String,Object>> items = null;
		try {
			MemberExp member = getMemberExpFromSession(request);
			client = createClient();
			tmp = new HashMap<String, Object>();
			isExt = requestParams.getString("external");
			page = requestParams.getString("pageNo");
			rows = requestParams.getString("pageSize");
			tmp.put("external", StringUtils.isBlank(isExt) || isExt.equals("N")? "N": "Y");
			tmp.put("pages", StringUtils.isBlank(page) || Integer.valueOf(page) < 1 ? 1 : page);
			tmp.put("rows", StringUtils.isBlank(rows) || Integer.valueOf(rows) > 20 || Integer.valueOf(rows) < 10 ? 10 : Integer.valueOf(rows));
			tmp.put("memberId", member.getMemberId());
			params = new RestParameter(tmp, "b146389c1ac562997c1463a8bff28c83");
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/points/getPointsProductList").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(params.getJsonString());
			}
			result = client.post(params.getJsonString(), ApiResult.class);
			items = result.getItems();
			String language = getLanguageFromSession(request);
			if(StringUtils.isNotBlank(language) && language.equals("en")){
				for (Map<String, Object> map : items) {
					map.put("couponName", map.get("couponNameEn"));
					map.put("couponRemark", map.get("couponRemarkEn"));
					map.put("extField1", map.get("extField1En"));
					map.put("showArchives", map.get("showArchivesEn"));
				}
			}
			jsonModel = new JsonModel(result.getCode(), result.getMessage(), items);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}

	/**
	 * @PE
	 * 兑换品详情
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findPointsProductDetail")
	@ResponseBody
	public JsonModel findPointsProductDetail(HttpServletRequest request, @RequestBody com.alibaba.fastjson.JSONObject requestParams) {
		if (requestParams == null || !requestParams.containsKey("goodsShowId")) {
			return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_ILLEGAL);
		}
		JsonModel jsonModel = null;
		Map<String, Object> couponMap = null;
		try {
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			MemberExp exp = getMemberExpFromSession(request);
			tmp.put("goodsShowId", requestParams.getString("goodsShowId"));
			tmp.put("gradeId",exp.getGradeId());
			RestParameter params = new RestParameter(tmp, "b146389c1ac562997c1463a8bff28c83");
			client.accept(MediaType.APPLICATION_JSON);
			client.path("points/getPointsProductDetail" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			String language = getLanguageFromSession(request);
			couponMap = result.getData();
			if(StringUtils.isNotBlank(language) && language.equals("en")){
				couponMap.put("couponName", couponMap.get("couponNameEn"));
				couponMap.put("comments", couponMap.get("commentsEn"));
				couponMap.put("couponRemark", couponMap.get("couponRemarkEn"));
				couponMap.put("extField1", couponMap.get("extField1En"));
			}
			jsonModel = new JsonModel(result.getCode(), result.getMessage(), result.getData());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}

	/**
	 * @PE
	 * 积分兑换
	 * @param request
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "exchangePointsProductDetail")
	@ResponseBody
	public JsonModel exchangePointsProductDetail(HttpServletRequest request, @RequestBody com.alibaba.fastjson.JSONObject requestParams) {
		if (requestParams == null || !requestParams.containsKey("goodsShowId")) {
			return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_ILLEGAL);
		}
		MemberExp member = getMemberExpFromSession(request);
		if (member == null || member.getMemberId() == null || member.getMemberId() < 1) {
			return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_MEMBER_INFO);
		}
		JsonModel jsonModel = null;
		try {
			String goodsShowId = requestParams.getString("goodsShowId");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("goodsShowId", goodsShowId);
			tmp.put("memberId", member.getMemberId());
			RestParameter params = new RestParameter(tmp, "b146389c1ac562997c1463a8bff28c83");
			client.accept(MediaType.APPLICATION_JSON);
			client.path("points/exchangePointsProductDetail");
			ApiResult result = client.post(params.getJsonString(), ApiResult.class);
			jsonModel = new JsonModel(result.getCode(), result.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}

}
