/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-wechat
 * File Name:FileUtils.java
 * Package Name:com.bw.adv.common.utils
 * Date:2016年1月6日下午10:51:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.RandomUtils;

/**
 * ClassName:FileUtils <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月6日 下午10:51:46 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class FileUtils {
	public static final String MAPPING_FOLDER = "/upload/";
	public static void download(String filename, String path, HttpServletResponse response) throws Exception{
		response.reset();
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-disposition","attachment; filename="+new String(filename.getBytes("utf-8"), "iso-8859-1"));
		try {
			InputStream fis = new BufferedInputStream(new FileInputStream(path));
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			fis.close();

			OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
			toClient.write(buffer);
			toClient.flush();
			toClient.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
    public static String upload(MultipartHttpServletRequest multipartRequest) throws IOException{
		String result = "";
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		MultipartFile multipartFile =null;
		String fileName = null;
		//String ctxPath = MyConstants.FILE_ROOT + DateUtils.getCurrentTimeOfDb()+"/";
		String ctxPath = multipartRequest.getSession().getServletContext().getRealPath("/") + "/upload/"; //测试
		 
		File file = new File(ctxPath);
		if(!file.exists()) file.mkdirs();
		for(Map.Entry<String,MultipartFile > set:fileMap.entrySet()){
			multipartFile = set.getValue();//文件名
			fileName = multipartFile.getOriginalFilename();
            File uploadFile = new File(file.getPath() + "/"+fileName);
            try {  
                FileCopyUtils.copy(multipartFile.getBytes(), uploadFile); 
                //result =ctxPath+fileName;
               // result = MyConstants.MAPPING_FOLDER + result.replace(MyConstants.FILE_ROOT, "");
            }catch (Exception e) {
            	e.printStackTrace();
			}
		}
        //return result;
		return "/upload/" + fileName;
    }

    
    public static List<String> uploadImages(HttpServletRequest request) throws IOException{
    	// 返回的多图片路径 
    	List<String> resultLoadList =null;
    	resultLoadList =new ArrayList<String>();
    	
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		MultipartFile multipartFile =null;
		String fileName = null;
		String path = request.getSession().getServletContext().getRealPath("/");
		String ctxPath =path+MAPPING_FOLDER;
		File file = new File(ctxPath);
		if(!file.exists()) file.mkdirs();
		for(Map.Entry<String,MultipartFile > set:fileMap.entrySet()){
			String result ="";
			multipartFile = set.getValue();//文件名
			fileName = DateUtils.getCurrentTimeOfDb()+"-"+RandomUtils.generateNumberString(2)+multipartFile.getOriginalFilename();
			if(StringUtils.isNotEmpty(fileName)){
				File uploadFile = new File(ctxPath + "/"+fileName);
				try {   
					FileCopyUtils.copy(multipartFile.getBytes(), uploadFile); 
					result =ctxPath+fileName;
					result = MAPPING_FOLDER+result.replace(ctxPath, "");
					resultLoadList.add(result);
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
        return resultLoadList;
    }

}


