package com.bw.adv.common.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import net.sf.json.JSONObject;


public class MD5SecurityUtils {

    
	private static final Logger logger = Logger.getLogger(MD5SecurityUtils.class);
    /**
     * MD5加签名
     * @param reqObj
     * @param md5_key
     * @return
     */
    public static String genSign(JSONObject reqObj, String sign) {
    	String sign_src = null;
        try {
        	if (reqObj == null) {
        		return "";
        	}
        	// 生成待签名串
        	sign_src = genSignData(reqObj);
        	sign_src += "&key=" + sign;
        	
            return MD5Utils.getMD5String(sign_src);
        } catch (Exception e) {
        	logger.error("MD5签名异常" + e.getMessage());
            return "";
        }
    }
    
    
    
    /**
     * 生成待签名串
     * @param jsonObject the json object
     * @return string
     */
    private static String genSignData(JSONObject jsonObject) {
    	List<String> keys = null;
    	String signSrc = null;
        StringBuilder content = null;
        String value = null;
        
        // 按照key做首字母升序排列
        keys = new ArrayList<String>(jsonObject.keySet());
        Collections.sort(keys, String.CASE_INSENSITIVE_ORDER);
        
        content = new StringBuilder();
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            if ("sign".equals(key)) {
                continue;
            }
            value = jsonObject.get(key).toString();
            // 空串不参与签名
            if (StringUtils.isBlank(value)) {
                continue;
            }
            content.append(i == 0 ? "" : "&").append(key).append("=").append(value);
        }
        signSrc = content.toString();
        if (signSrc.startsWith("&")) {
            signSrc = signSrc.replaceFirst("&", "");
        }
        return signSrc;
    }
}
