package com.bw.adv.common.enums;


import com.bw.adv.common.utils.JsonModel;

/**
 * Created by jeoy.zhou on 6/12/16.
 */
public enum ResultConstantsEnum {
    SUCCESS("200", "成功", "success"),
    ERROR("500", "微信系统异常", "error"),
    PARAMETER_ILLEGAL("501", "参数不合法", "parameter illegal"),
    PARAMETER_EMPTY("502", "参数为空", "parameter empty"),
    API_ERROR("503", "调用接口异常", "api error"),
    FORWARD("504","跳转","wechat forward"),
    ILLEGAL_CHANNELS("505","非法渠道","Illegal channels"),
    PARAMETER_MEMBER_INFO("5012", "非法会员信息", "member info illegal"),
    TO_INDEX("599", "会员信息获取失效", "get member info fail"),
    ;

    private String code;
    private String messageCn;
    private String messageEn;

    ResultConstantsEnum(String code, String messageCn, String messageEn) {
        this.code = code;
        this.messageCn = messageCn;
        this.messageEn = messageEn;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessageCn(String messageCn) {
        this.messageCn = messageCn;
    }

    public void setMessageEn(String messageEn) {
        this.messageEn = messageEn;
    }

    public static JsonModel createResult(ResultConstantsEnum resultConstantsEnum) {
        // TODO 国际化返回message
        return new JsonModel(resultConstantsEnum.code, resultConstantsEnum.messageCn);
    }

    public static JsonModel createSuccessResult() {
        return createResult(ResultConstantsEnum.SUCCESS);
    }

    public static JsonModel createFailResult() {
        return createResult(ResultConstantsEnum.ERROR);
    }
}
