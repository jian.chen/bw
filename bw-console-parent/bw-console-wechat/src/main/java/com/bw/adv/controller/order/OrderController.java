package com.bw.adv.controller.order;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;
import com.bw.adv.module.member.model.exp.MemberExp;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/orderController")
public class OrderController extends BaseController {
	private static final Logger LOGGER = Logger.getLogger(OrderController.class);

	/**
	 * @PE
	 * @param request
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/findMemberOrderItem")
	@ResponseBody
	public JsonModel findMemberOrderItem(HttpServletRequest request, @RequestBody JSONObject requestParams) {
		JsonModel result = null;
		Map<String, Object> tmp = null;
		String page = null;
		String rows = null;
		WebClient client = null;
		RestParameter params = null;
		ApiResult apiResult = null;
		try {
			if(!(requestParams.containsKey("pageNo") && requestParams.containsKey("pageSize"))){
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			MemberExp exp = getMemberExpFromSession(request);
			String memberCode = exp.getMemberCode();
			client = createClient();
			tmp = new HashMap<String, Object>();
			page = requestParams.getString("pageNo");
			rows = requestParams.getString("pageSize");
			tmp.put("memberCode", memberCode);
			tmp.put("pages", page);
			tmp.put("rows", rows);
			params = new RestParameter(tmp, "b146389c1ac562997c1463a8bff28c83");
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/order/getMemberOrderItemInfo").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			apiResult = client.post(params.getJsonString(), ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(apiResult.getItems());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}

}
