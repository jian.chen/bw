package com.bw.adv.controller.lottery;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean2;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.HttpKit;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.common.utils.MD5Utils;
import com.bw.adv.controller.base.BaseController;
import com.bw.adv.module.activity.enums.AccordTypeEnum;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.tools.cookie.utils.CookieUtils;

@Controller
@RequestMapping("/lottery")
@SuppressWarnings("unused")
public class LotteryController extends BaseController {

	// 抽奖活动Id
	protected Log logger = LogFactory.getLog(this.getClass());
	@Value("${mapsUrl}")
	private String mapsUrl;
	@Value("${mapsKey}")
	private String mapsKey;
	@Value("${get_userinfo_url}")
	private String getUserinfoUrl;
	/**
	 * @PE
	 * 抽奖初始化查询抽奖机会
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "initMemberAwards")
	@ResponseBody
	public JsonModel initMemberAwards(HttpServletRequest request,String activityCode){
		JsonModel result = null;
		Long memberId = null;
		WebClient client = createClient();
		try {
			MemberExp exp = getMemberExpFromSession(request);
			memberId = exp.getMemberId();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("memberId", memberId);
			tmp.put("activityCode", activityCode);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/activity/initMemberAwards").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			ApiResult apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
				return result;
			}
			result = ResultConstantsEnum.createSuccessResult();
			result.setData(apiResult.getData());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	/**
	 * @PE
	 * 进行抽奖
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "extractMemberAwards")
	@ResponseBody
	public JsonModel extractMemberAwards(HttpServletRequest request) {
		JsonModel jsonModel = null;
		Map<String, Object> returnMapTemp = null;
		String result = null;
		List<Map<String, Object>> mapList = null;
		String status = null;
		try {
			String openId = CookieUtils.getCookieByName(request, COOKIE_OPENID_KEY);
			String memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
			String activityCode=CookieUtils.getCookieByName(request, COOKIE_QRCODE_ID);
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("memberId", memberId);
			tmp.put("qrCode", activityCode);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/lottery/lotteryActivityMemberAwards").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			ApiResult apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				jsonModel = ResultConstantsEnum.createFailResult();
				jsonModel.setMessage(apiResult.getMessage());
				return jsonModel;
			}
			jsonModel = ResultConstantsEnum.createSuccessResult();
			jsonModel.setData(apiResult.getData());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}
	@RequestMapping(method = RequestMethod.POST, value = "extractMemberAwardsMaps")
	@ResponseBody
	public JsonModel extractMemberAwardsMaps(HttpServletRequest request) {
		JsonModel jsonModel = null;
		Map<String, Object> returnMapTemp = null;
		String result = null;
		List<Map<String, Object>> mapList = null;
		String status = null;
		try {
			String openId = CookieUtils.getCookieByName(request, COOKIE_OPENID_KEY);
			String memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
			String qr=CookieUtils.getCookieByName(request, COOKIE_MAPSQRCODE_ID);
			if(StringUtils.isNotBlank(qr)){
				qr=URLDecoder.decode(qr, "utf-8");
			}
			String activityCode="SPR_LOTTERY";
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("memberId", memberId);
			tmp.put("activityCode", activityCode);
			tmp.put("qrCode", qr);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("/lottery/extractMemberAwardsMaps").header("content-type", "application/json");
			client.acceptEncoding("UTF-8").encoding("UTF-8");
			String jsonParam = params.getJsonString();
			ApiResult apiResult = client.post(jsonParam, ApiResult.class);
			if (!apiResult.getCode().equals(API_SUCCESS_CODE)) {
				jsonModel = ResultConstantsEnum.createFailResult();
				jsonModel.setMessage(apiResult.getMessage());
				return jsonModel;
			}
			jsonModel = ResultConstantsEnum.createSuccessResult();
			jsonModel.setData(apiResult.getData());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}


	/**
	 * 扫描二维码进入
	 * @param qrcode
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/bc/{qrcode}",method=RequestMethod.GET)
	public String call( @PathVariable String qrcode ,HttpServletRequest request,HttpServletResponse response,Model model){
		
		String openId = CookieUtils.getCookieByName(request, COOKIE_OPENID_KEY);
		String memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
		if(StringUtils.isBlank(memberId)){
			Member member = queryBindingAccountByOpenId(openId);
			CookieUtils.addCookie(response, COOKIE_MEMBER_ID, String.valueOf(member.getMemberId()));
			memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
		}
		String token = this.getAccessToken();
		
		try {
			String paramUrl = HttpKit.get(getUserinfoUrl.replace("ACCESS_TOKEN", token).replace("OPENID", openId));
			JSONObject json = JSON.parseObject(paramUrl);
			if(json.containsKey("subscribe")){
				int value = json.getIntValue("subscribe");
				if(value==0){
					return "member/follow";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
//		String openId="_1U2Rb7fQBvdf7VJDtFdhV9I";
//		String memberId="1";
		CookieUtils.addCookie(response, COOKIE_QRCODE_ID, qrcode);
		WebClient client = createClient();
		Map<String, Object> tmp = new HashMap<String, Object>();
		tmp.put("memberId", memberId);
		tmp.put("qrCodeCode", qrcode);
		RestParameter params = new RestParameter(tmp);
		client.accept(MediaType.APPLICATION_JSON);
		//初始化抽奖机会
		client.path("lottery/initLotteryMemberAwards");
		ApiResult resultTemp = client.post(params.getJsonString(),ApiResult.class);
		//return resultTemp;
		if(!StringUtils.equals(resultTemp.getCode(),API_SUCCESS_CODE)){
			model.addAttribute("error_txt", resultTemp.getMessage());
			return "turntable/error";
		}
		Map<String, Object> data = resultTemp.getData();
		Integer chance = (Integer) data.get("chance");
		if(chance<=0){
			model.addAttribute("error_txt", "您的抽奖机会已用完");
			return "turntable/error";
		}
		return "turntable/turn";
	}
	
	/**
	 * 扫描二维码进入
	 * @param qrcode
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/sprback",method=RequestMethod.GET)
	public String sprCallBack(String qr ,String sign,HttpServletRequest request,HttpServletResponse response,Model model){
		
		String openId = CookieUtils.getCookieByName(request, COOKIE_OPENID_KEY);
		String memberId=CookieUtils.getCookieByName(request, COOKIE_MEMBER_ID);
		if(StringUtils.isBlank(memberId)){
			this.queryBindingAccountByOpenId(openId);
		}
		if(StringUtils.isBlank(qr)){
			model.addAttribute("error_txt", "MAPS访问出错了<");
			return "turntable/error";
		}
		logger.info("maps_qr="+qr+",sign="+sign);
		String mapsAddr = mapsUrl.replace("QR_PARAM", qr).replace("SIGN_PARAM", sign);
		
		try {
			
			String token = this.getAccessToken();
			try {
				String paramUrl = HttpKit.get(getUserinfoUrl.replace("ACCESS_TOKEN", token).replace("OPENID", openId));
				JSONObject json = JSON.parseObject(paramUrl);
				if(json.containsKey("subscribe")){
					int value = json.getIntValue("subscribe");
					if(value==0){
						return "member/follow";
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
			String mapsParam = HttpKit.get(mapsAddr);
			logger.info("maps_back"+mapsParam);
			if(StringUtils.isBlank(mapsParam)){
				model.addAttribute("error_txt", "MAPS访问出错了<");
				return "turntable/error";
			}
			JSONObject jsonObject = JSON.parseObject(mapsParam);
			
			if(jsonObject.containsKey("code")){
				String errmsg = jsonObject.getString("errmsg");
				model.addAttribute("error_txt", errmsg);
				return "turntable/error";
			}else{
				logger.info("maps访问成功");
				CookieUtils.addCookie(response, COOKIE_MAPSQRCODE_ID, qr);
			}
		} catch (Exception e) {
			logger.error("maps访问出错", e);
			model.addAttribute("error_txt", "访问出错了<");
			return "turntable/error";
		} 
		
		
//		String openId="_1U2Rb7fQBvdf7VJDtFdhV9I";
//		String memberId="1";
		WebClient client = createClient();
		Map<String, Object> tmp = new HashMap<String, Object>();
		tmp.put("memberId", memberId);
		tmp.put("qrCode", qr);
		tmp.put("activityCode", "SPR_LOTTERY");
		RestParameter params = new RestParameter(tmp);
		client.accept(MediaType.APPLICATION_JSON);
		client.path("lottery/initMapsMemberAwards");
		ApiResult resultTemp = client.post(params.getJsonString(),ApiResult.class);
		if(!StringUtils.equals(resultTemp.getCode(),API_SUCCESS_CODE)){
			model.addAttribute("error_txt", resultTemp.getMessage());
			return "turntable/error";
		}
		return "turntable/spa-turn";
	}
}
