/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-web-wechat
 * File Name:CouponController.java
 * Package Name:com.sage.scrm.controller.coupon
 * Date:2015年10月22日下午1:58:25
 * Description: //模块目的、功能描述
 * History: //修改记录
 */

package com.bw.adv.controller.coupon;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.common.enums.ResultConstantsEnum;
import com.bw.adv.common.utils.JsonModel;
import com.bw.adv.controller.base.BaseController;
import com.bw.adv.module.member.model.exp.MemberExp;

import net.sf.json.JSONObject;

/**
 * ClassName:CouponController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年10月22日 下午1:58:25 <br/>
 * scrmVersion 1.0
 * 
 * @author june
 * @version jdk1.7
 * @see
 */
@Controller
@RequestMapping("/couponController")
public class CouponController extends BaseController {

	protected final Logger logger = Logger.getLogger(CouponController.class);

	/**
	 * @PE
	 * @param request
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findCouponList")
	@ResponseBody
	public JsonModel findCouponList(HttpServletRequest request, @RequestBody JSONObject requestParams) {
		JsonModel result = null;
		String page = null;
		String rows = null;
		String statusId = null;
		List<Map<String, Object>> couponList = null;
		String code = null;
		String message = null;
		String memberCode = null;
		MemberExp memberExp = null;
		try {
			memberExp = getMemberExpFromSession(request);
			memberCode = memberExp.getMemberCode();
			if(!(requestParams.containsKey("status") && requestParams.containsKey("pageNo") && requestParams.containsKey("pageSize"))){
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			page = requestParams.getString("pageNo");
			rows = requestParams.getString("pageSize");
			statusId = requestParams.getString("status");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("memberCode", memberCode);
			tmp.put("type", "");
			tmp.put("statusId", statusId);
			tmp.put("page", page);
			tmp.put("rows", rows);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("coupon/queryCouponInstanceByMemberCode" + params.getMatrixString());
			ApiResult apiResult = client.get(ApiResult.class);
			code = apiResult.getCode();
			message = apiResult.getMessage();
			if (!code.equals(API_SUCCESS_CODE)) {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(message);
				return result;
			}
			couponList = apiResult.getItems();
			result = ResultConstantsEnum.createSuccessResult();
			String language = getLanguageFromSession(request);
			if(StringUtils.isNotBlank(language) && language.equals("en")){
				for (Map<String, Object> map : couponList) {
					map.put("couponName", map.get("couponNameEn"));
					map.put("comments", map.get("commentsEn"));
					map.put("remark", map.get("remarkEn"));
					map.put("extField1", map.get("extField1En"));
				}
			}
			result.setData(couponList);
		} catch (Exception e) {
			result = ResultConstantsEnum.createFailResult();
			logger.error(e.getMessage());
		}
		return result;
	}

	/**
	 * @PE
	 * @param request
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findCouponDetail")
	@ResponseBody
	public JsonModel findCouponDetail(HttpServletRequest request, @RequestBody JSONObject requestParams) {
		JsonModel result = new JsonModel();
		Map<String, Object> couponMap = null;
		String couponInstanceCode = null;
		String code = null;
		MemberExp memberExp = null;
		try {
			if(!requestParams.containsKey("couponInstanceCode")){
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			couponInstanceCode = requestParams.getString("couponInstanceCode");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("couponInstanceCode", couponInstanceCode);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("coupon/queryCouponInstanceDetail" + params.getMatrixString());
			ApiResult apiResult = client.get(ApiResult.class);
			code = apiResult.getCode();
			couponMap = apiResult.getData();
			memberExp = getMemberExpFromSession(request);
			String memberId = memberExp.getMemberId() != null ? memberExp.getMemberId().toString() : "";
			String couponInstanceUser = couponMap.get("couponInstanceUser") != null
					? couponMap.get("couponInstanceUser").toString() : null;
			String couponInstanceOwner = couponMap.get("couponInstanceOwner") != null
					? couponMap.get("couponInstanceOwner").toString() : null;
			String statudId = couponMap.get("statusId").toString();
			String language = getLanguageFromSession(request);
			if(StringUtils.isNotBlank(language) && language.equals("en")){
				couponMap.put("couponName", couponMap.get("couponNameEn"));
				couponMap.put("comments", couponMap.get("commentsEn"));
				couponMap.put("remark", couponMap.get("remarkEn"));
				couponMap.put("extField1", couponMap.get("extField1En"));
			}
			if(!couponInstanceUser.equals(couponInstanceOwner)){
				couponMap.put("isOwnerUsed", "Y");
			}
			if (code.equals(API_SUCCESS_CODE) && memberId != null && couponInstanceUser != null && couponInstanceUser.equals(memberId) && !statudId.equals("1607")) {
				result = ResultConstantsEnum.createSuccessResult();
				couponMap.put("instanceCode", (String)couponMap.get("couponInstanceCode"));
				result.setData(couponMap);
			}else if(statudId.equals("1607")){
				result = ResultConstantsEnum.createFailResult();
				result.setMessage("优惠券在分享中");
			}else{
				result = ResultConstantsEnum.createFailResult();
				result.setMessage("用户错误");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	/**
	 * @PE
	 * @param request
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findCouponDetailByDes")
	@ResponseBody
	public JsonModel findCouponDetailByDes(HttpServletRequest request, @RequestBody JSONObject requestParams) {
		JsonModel result = new JsonModel();
		Map<String, Object> couponMap = null;
		String couponInstanceCode = null;
		String code = null;
		try {
			if(!requestParams.containsKey("instanceCode")){
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			couponInstanceCode = requestParams.getString("instanceCode");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("couponInstanceCode", couponInstanceCode);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("coupon/queryCouponInstanceDetail" + params.getMatrixString());
			ApiResult apiResult = client.get(ApiResult.class);
			code = apiResult.getCode();
			couponMap = apiResult.getData();
			String statusId = couponMap.get("statusId").toString();
			if (code.equals(API_SUCCESS_CODE) && statusId.equals("1607") ) {
				result = ResultConstantsEnum.createSuccessResult();
	            result.setData(couponMap);
			} else {
				result = ResultConstantsEnum.createFailResult();
				result.setMessage("该优惠券链接已经失效");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = ResultConstantsEnum.createFailResult();
		}
		return result;
	}
	
	/**
	 * @PE
	 * 立即领取优惠券
	 * @param request
	 * @param requestParams
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "immediatelyReceive")
	@ResponseBody
	public JsonModel immediatelyReceive(HttpServletRequest request, @RequestBody JSONObject requestParams) {
		JsonModel result = new JsonModel();
		String couponInstanceCode = null;
		String openId = null;
		String type = null;
		try {
			if(!(requestParams.containsKey("instanceCode") && requestParams.containsKey("type"))){
				return ResultConstantsEnum.createResult(ResultConstantsEnum.PARAMETER_EMPTY);
			}
			openId = getOpenIdFromSession(request);
			couponInstanceCode = requestParams.getString("instanceCode");
			type = requestParams.getString("type");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("couponInstanceCode", couponInstanceCode);
			tmp.put("openId", openId);
			tmp.put("type", type);
			tmp.put("preOpenId", openId);
			tmp.put("seq", 1);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("couponPe/handleCouponInstance" + params.getMatrixString());
			ApiResult apiResult = client.get(ApiResult.class);
			if (apiResult.getCode().equals(API_SUCCESS_CODE) && apiResult.getMessage().equals("200")){
				result = ResultConstantsEnum.createSuccessResult();
			}else{
				result = ResultConstantsEnum.createFailResult();
				result.setMessage(apiResult.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = ResultConstantsEnum.createFailResult();
		}
		return result;
	}

	/**
	 *
	 * findCouponCategoryList:(查询优惠券类别属于我要领劵模块). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findCouponCategoryList")
	@ResponseBody
	public JsonModel findCouponCategoryList(HttpServletRequest request) {
		JsonModel jsonModel = null;
		String page = null;
		String rows = null;
		try {
			page = request.getParameter("page");
			rows = request.getParameter("rows");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("page", page);
			tmp.put("rows", rows);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("coupon/queryCouponCategorys" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			jsonModel = new JsonModel(result.getCode(), result.getMessage(), result.getItems());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}

	/**
	 *
	 * findCouponListByCategory:(查询优惠券根据优惠券类别属于我要领劵模块). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findCouponListByCategory")
	@ResponseBody
	public JsonModel findCouponListByCategory(HttpServletRequest request) {
		JsonModel jsonModel = null;
		String couponCategoryId = null;
		try {
			couponCategoryId = request.getParameter("couponCategoryId");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("couponCategoryId", couponCategoryId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("coupon/queryCouponByCategoryId" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			jsonModel = new JsonModel(result.getCode(), result.getMessage(), result.getItems());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}

	/**
	 *
	 * findCouponCategoryDetail:(我要领劵模块-跳转到劵详情 当季优惠的劵). <br/>
	 * Date: 2016年1月7日 下午5:28:55 <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "findCouponCategoryDetail")
	@ResponseBody
	public JsonModel findCouponCategoryDetail(HttpServletRequest request) {
		JsonModel jsonModel = null;
		String couponId = null;
		try {
			couponId = request.getParameter("couponId");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("couponId", couponId);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("coupon/queryCouponCategoryDetail" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			jsonModel = new JsonModel(result.getCode(), result.getMessage(), result.getData());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}

	/**
	 *
	 * getCouponCategory:(我要领劵模块-立即领取). <br/>
	 * Date: 2016年1月7日 下午5:28:55 <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "getCouponCategory")
	@ResponseBody
	public JsonModel getCouponCategory(HttpServletRequest request) {
		JsonModel jsonModel = null;
		String couponId = null;
		String memberCode = null;
		try {
			couponId = request.getParameter("couponId");
			memberCode = request.getParameter("memberCode");
			WebClient client = createClient();
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("couponId", couponId);
			tmp.put("memberCode", memberCode);
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			client.path("coupon/getCouponCategory" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			jsonModel = new JsonModel(result.getCode(), result.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultConstantsEnum.createFailResult();
		}
		return jsonModel;
	}
	@ResponseBody
	@RequestMapping(value="exchange" , method= RequestMethod.POST)
	public JsonModel getCouponByExchangeCode(HttpServletRequest request,HttpServletResponse response, @RequestBody JSONObject requestParams){
		JsonModel json = new JsonModel();
		Long memberId = null;
		String memberCode = null;
		Map<String, Object> resultMap = null;
		Map<String, Object> map = null;
		ApiResult resultTemp = null;
		String openId = null;
		String headImgUrl = null;
		String exchangeCode = requestParams.getString("exchangeCode");
		LOGGER.info("exchange_code: " + exchangeCode);

		WebClient client = null;
		try {
			resultMap = new HashMap<>();
			map = new HashMap<String, Object>();
			map = getMemberByOpenId(request, response);
			resultTemp = (ApiResult) map.get("apiResult");
			openId = (String) map.get("openId");
			memberCode = (String) resultTemp.getData().get("memberCode");
			//openId没有对应会员
			if(ResultStatus.MEMBER_NOT_EXIST_ERROR.getCode().equals(resultTemp.getCode()) || ResultStatus.MEMBER_ACCOUNT_NOTEXIST_ERROR.getCode().equals(resultTemp.getCode())){
				//调用会员注册方法
				Map<String, Object> returnMap = saveWeChatFans(openId,headImgUrl,map);
				if(ResultStatus.SUCCESS.getCode().equals(returnMap.get("code"))){
					resultMap.put("code", ResultStatus.MEMBER_NOT_EXIST_ERROR.getCode());
					resultMap.put("message", ResultStatus.MEMBER_NOT_EXIST_ERROR.getMsg());
					json.setCode((String)resultMap.get("code"));
					json.setMessage((String)resultMap.get("message"));
					return json;
				}
			}else{
				memberId =Long.valueOf(String.valueOf(resultTemp.getData().get("memberId")));
				client = createClient();
				Map<String, Object> tmp = new HashMap<String, Object>();
				tmp.put("exchangeCode", exchangeCode);
				tmp.put("memberId", memberId);
				RestParameter params = new RestParameter(tmp);
				client.accept(MediaType.APPLICATION_JSON);
				client.path("coupon/exchange"+params.getMatrixString()).header("content-type", "application/json");
				ApiResult apiResult = client.post(params.getJsonString() ,ApiResult.class);
					//跳转列表
				resultMap.put("code", apiResult.getCode());
				resultMap.put("message", apiResult.getMessage());
				resultMap.put("data", "{\"memberCode\":"+memberCode+"}");
				json.setCode((String)resultMap.get("code"));
				json.setMessage((String)resultMap.get("message"));
				json.setData("{\"memberCode\":"+memberCode+"}");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("code", ResultStatus.MEMBER_NOT_EXIST_ERROR.getCode());
			resultMap.put("message", ResultStatus.MEMBER_NOT_EXIST_ERROR.getMsg());
		}finally{
			closeClient(client);
		}
		return json;
	}
	
}
