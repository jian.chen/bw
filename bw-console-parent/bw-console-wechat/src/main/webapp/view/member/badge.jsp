<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>酿造</title>
	<link rel="stylesheet" type="text/css" href="${contextPath}/view/app_css/page.css">
	<script type="text/javascript">
		$(function(){
			$.ajax({
				type : "post",
				url : '/bw-console-wechat/gift/getMemberGiftMemberBadge',
				dataType : "json",
				async: false,
				success : function(data) {
					if (data.code == 200) {//200 代表成功
						var jsonObj=data.data;
						for(var item in jsonObj){
							var obj=jsonObj[item];
							switch (obj.awardsCode) {//请求返回的抽中奖品字段
			                 case 'LOTTERY_TYPE_WHEAT_HEAD': //麦芽
			                 	$('#wheatHead').text(obj.badgeAvailableNum);
			                     break;
			                 case 'LOTTERY_TYPE_YAKIMA': //亚济马酒花
			                	 $('#yakima').text(obj.badgeAvailableNum);
			                     break;
			                 case 'LOTTERY_TYPE_BEECH': //榉木
			                	 $('#beech').text(obj.badgeAvailableNum);
			                     break;
			                 case 'LOTTERY_TYPE_WATER': //纯净水
			                	 $('#water').text(obj.badgeAvailableNum);
			                     break;
						}
		             }
					}else{
						alert(data.message)
					}
				},
				error : function(data) {
				}
			});
		});
		
		function receive(){
			$.ajax({
				type : "post",
				url : '/bw-console-wechat/gift/memberBadgegiftExchange',
				dataType : "json",
				async: false,
				success : function(data) {
					if (data.code == 200) {//200 代表成功
						var obj= data.data;
						if(obj){
							 window.location.href="/bw-console-wechat/gift/member?page=gift-details-youku&couponInstanceCode="+obj.couponInstanceCode+"&createDate="+obj.getTime;
						}else{
							alert('券已领完，请等待库存补充');
						}
					}else{
						alert(data.message)
					}
				},
				error : function(data) {
				}
			
			})
		}
		
		
			
	
	</script>
</head>
<body>
	<div class="page page-brew">
		<header class="header">
			<h2><img src="${contextPath}/view/app_img/brew_tit.png"></h2>
			<span class="sign"></span>
		</header>
		<section class="page-content">
			<div class="wrapper brew-wrapper">
				<h2 class="title">集齐四款徽章<br>即可获得优酷月会员1张噢</h2>
				<ul class="badge-list">
					<li class="active">
						<div class="badge-icon"></div>
						<p>x<span class="num" id="wheatHead">0</span></p>
					</li>
					<li class="active">
						<div class="badge-icon"></div>
						<p>x<span class="num" id="yakima">0</span></p>
					</li>
					<li class="active">
						<div class="badge-icon"></div>
						<p>x<span class="num" id="beech">0</span></p>
					</li>
					<li class="active">
						<div class="badge-icon"></div>
						<p>x<span class="num" id="water">0</span></p>
					</li>
				</ul>
				<a href="javascript:;" onclick="receive()" id="receive" class="btn get-btn"></a>
			</div>
		</section>
	</div>
</body>
</html>