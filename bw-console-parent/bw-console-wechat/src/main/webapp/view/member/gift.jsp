<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>查看礼品</title>
	<link rel="stylesheet" type="text/css" href="${contextPath }/view/app_css/page.css">
	<script type="text/javascript">
		$(function(){
			$.ajax({
				type : "post",
				url : '/bw-console-wechat/gift/getMemberGift',
				dataType : "json",
				async: false,
				success : function(data) {
					if (data.code == 200) {//200 代表成功
						var jsonObj=data.data;
						for(var item in jsonObj){
							var obj=jsonObj[item];
							switch (obj.giftCode) {//请求返回的抽中奖品字段
			                 case 'LOTTERY_TYPE_AIR_CLEANER': //
			                 	$('#clean_air').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_AIQIYI_MEMBER': //
			                	 $('#aiqiyi').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_YOUKU_MONTH_MEMBER': //榉木
			                	 $('#youku').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_INTELLIGENT_CLEANER_ROBOT': //纯净水
			                	 $('#robot').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_MEITUAN_PRESS': //纯净水
			                	 $('#meituan').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_XIECHENG_COUPON': //纯净水
			                	 $('#xiecheng').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_JINGDONG_COUPON': //纯净水
			                	 $('#jingdong').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_STARBUCK_COUPON': //纯净水
			                	 $('#starbuck').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_FILM_COUPON': //纯净水
			                	 $('#film').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_GIFT_FIVE_HUNDR': //纯净水
			                	 $('#lp500').text(obj.memberGiftNum);
			                     break;
			                 case 'LOTTERY_TYPE_GIFT_ONE_THOUSAND': //纯净水
			                	 $('#lp1000').text(obj.memberGiftNum);
			                     break;
						}
		             }
					}else{
						alert(data.message)
					}
				},
				error : function(data) {
				}
			});
		});
	</script>
</head>
<body>
	<div class="page page-gift">
		<header class="header">
			<h2><img src="${contextPath }/view/app_img/view_tit.png"></h2>
			<span class="sign"></span>
		</header>
		<section class="page-content">
			<div class="wrapper gift-wrapper">
				<ul class="gift-list">
					<li>
						<span class="icon"><img class="icon-dyson" src="${contextPath }/view/app_img/icon_dyson.png"></span>
						<span class="name">戴森空气净化器</span>
						<span class="num" id="clean_air"><b>0</b></span>部
						<!--<a href="javascript:;" class="view-btn"></a>-->
					</li>
					<li>
						<span class="icon"><img class="icon-iqy" src="${contextPath }/view/app_img/icon_iqy.png"></span>
						<span class="name">爱奇艺月会员</span>
						<span class="num" id="aiqiyi"><b>0</b></span>张
						<!--<a href="javascript:;" class="view-btn"></a>-->
					</li>
					<li>
						<span class="icon"><img class="icon-iqy" src="${contextPath }/view/app_img/icon_youku.png"></span>
						<span class="name">优酷月会员</span>
						<span class="num" id="youku"><b>0</b></span>张
						<!--<a href="javascript:;" class="view-btn"></a>-->
					</li>
					<li>
						<span class="icon"><img class="icon-robot" src="${contextPath }/view/app_img/icon_robot.png"></span>
						<span class="name">智能扫地机器人</span>
						<span class="num" id="robot"><b>0</b></span>部
						<!--<a href="javascript:;" class="view-btn"></a>-->
					</li>
					<li>
						<span class="icon"><img class="icon-meituan" src="${contextPath }/view/app_img/icon_meituan.png"></span>
						<span class="name">美团抵用券</span>
						<span class="num" id="meituan"><b>0</b></span>张
						<!--<a href="javascript:;" class="view-btn"></a>-->
					</li>
					<li>
						<span class="icon"><img class="icon-ticket" src="${contextPath }/view/app_img/icon_ticket.png"></span>
						<span class="name">电影票</span>
						<span class="num" id="film"><b>0</b></span>张
						<!--<a href="javascript:;" class="view-btn"></a>-->
					</li>
					<li>
						<span class="icon"><img class="icon-cards500" src="${contextPath }/view/app_img/icon_cards500.png"></span>
						<span class="name">500元礼品卡</span>
						<span class="num" id="lp500"><b>0</b></span>张
						<!--<a href="javascript:;" class="view-btn"></a>-->
					</li>
					<li>
						<span class="icon"><img class="icon-cards1000" src="${contextPath }/view/app_img/icon_cards1000.png"></span>
						<span class="name">1000元礼品卡</span>
						<span class="num" id="lp1000"><b>0</b></span>张
						<!--<a href="javascript:;" class="view-btn"></a>-->
					</li>
				</ul>
			</div>
		</section>
	</div>
</body>
</html>