<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>我的金尊之旅-领取奖品</title>
	<link rel="stylesheet" type="text/css" href="${contextPath }/view/app_css/page.css">
</head>
<body>
<div class="page page-prize">
	<section class="header">
		<h2><img src="${contextPath }/view/app_img/lottery_tit.png"></h2>
		<span class="sign"></span>
	</section>
	<section class="page-content">
		<div class="prize-wrap spa-prize-wrap">
			<div class="prize-letter">
				<h2 class="flex">
					<p>恭喜您获得</p>
					<p>携程<i>50元抵用券</i></p>
					<span>${couponInstanceCode }</span>
				</h2>
			</div>
			<a href="javascript:;" class="change-btn"></a>
			<div class="use-flow">
				<dl>
					<dt>使用流程</dt>
					<dd>1.打开携程网，登录账户</dd>
					<dd>2.进入我的携程，点击携程钱包下的礼品卡</dd>
					<dd>3.点击领用礼品卡</dd>
					<dd>4.输入礼品卡券号和密码，点击立即领用</dd>
				</dl>
			</div>
		</div>
	</section>
</div>
</body>
</html>