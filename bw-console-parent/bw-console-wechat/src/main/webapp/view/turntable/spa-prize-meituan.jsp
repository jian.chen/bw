<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>我的金尊之旅-领取奖品</title>
	<link rel="stylesheet" type="text/css" href="${contextPath }/view/app_css/page.css">
	<script type="text/javascript">
		function exchange(){
			window.location.href="http://www.meituan.com";
		}
	</script>
</head>
<body>
<div class="page page-prize">
	<section class="header">
		<h2><img src="${contextPath}/view/app_img/lottery_tit.png"></h2>
		<span class="sign"></span>
	</section>
	<section class="page-content">
		<div class="prize-wrap spa-prize-wrap">
			<div class="prize-letter">
				<h2 class="flex">
					<p>恭喜您获得<i>美团抵用券</i></p>
					<span>${couponInstanceCode}</span>
				</h2>
			</div>
			<a href="http://www.meituan.com" class="change-btn"></a>
			<div class="use-flow">
				<dl>
					<dt>使用流程</dt>
					<dd>1. 登录美团手机客户端</dd>
					<dd>2. 选择任意团购产品,点击“立即购买”</dd>
					<dd>3. 进入“提交订单”页面,点击“使用抵用券”,点击“添加”,输入串码点击“确定”</dd>
					<dd>4. 返回“提交订单”页面,点击“提交订单”完成支付即完成使用</dd>
					<dd>5. 5元美团抵用券,订单满5.01元即可用此券抵用5元,每笔订单限用一张抵用券,不与其他优惠同享</dd>
					<dd>6. 10元美团抵用券,订单满10.01元即可用此券抵用10元,每笔订单限用一张抵用券,不与其他优惠同享</dd>
					<dd>7. 客服电话400-037-9061</dd>
					<dd>8. 2017/12/31前可兑换,兑换日起1个月内有效</dd>
				</dl>
			</div>
		</div>
	</section>
</div>
</body>
</html>