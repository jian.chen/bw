<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>顾客信息</title>
	<link rel="stylesheet" type="text/css" href="../app_css/page.css">
</head>
<body>
	<div class="page page-success">
		<header class="header">
			<h2><img src="../app_img/customer_info_tit.png"></h2>
			<span class="sign"></span>
		</header>
		<section class="page-content">
			<div class="wrapper success-wrapper">
				<h2 class="title">资料提交成功!</h2>
				<div class="wrapper-footer">如有疑问请拨打4006-111-299转2</div>
			</div>
		</section>
	</div>
</body>
</html>