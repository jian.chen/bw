<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>我的金尊之旅-领取奖品</title>
	<link rel="stylesheet" type="text/css" href="${contextPath }/view/app_css/page.css">
</head>
<body>
<div class="page page-prize">
	<section class="header">
		<h2><img src="${contextPath }/view/app_img/lottery_tit.png"></h2>
		<span class="sign"></span>
	</section>
	<section class="page-content">
		<div class="prize-wrap spa-prize-wrap">
			<div class="prize-letter">
				<h2 class="flex">
					<p>恭喜您获得</p>
					<p>爱奇艺月会员</p>
					<span>${couponInstanceCode }</span>
				</h2>
			</div>
			<a href="http://www.iqiyi.com" class="change-btn"></a>
			<div class="use-flow">
				<dl>
					<dt>使用流程</dt>
					<dd>1.使用流程：在爱奇艺注册登录后选择“爱奇艺VIP”,选择相应产品后选择“激活码支付”,输入会员串码即可激活为VIP会员。</dd>
					<dd>2.VIP会员可享有免广告、专属VIP片库免费看、专属身份标识、免费获赠点播券等特权。</dd>
					<dd>3.会员卡不可兑换现金。请注意会员卡保密性，若发生盗用、泄露、遗失等问题不予调换与退款。</dd>
					<dd>4.此奖品所包含服务内容及具体使用方式以爱奇艺规定的使用规则为准，使用疑问请咨询爱奇艺热线： 400-800-7171。</dd>
					<dd>5.2017/12/31前可兑换,兑换日起1个月内有效</dd>
				</dl>
			</div>
		</div>
	</section>
</div>
</body>
</html>