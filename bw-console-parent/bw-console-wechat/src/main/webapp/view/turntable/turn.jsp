<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="wap-font-scale" content="no">
	<meta name="aplus-touch" content="1">
	<meta content="yes" name="apple-touch-fullscreen">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title>我的金尊之旅</title>
	<link rel="stylesheet" type="text/css" href="${contextPath}/view/app_css/page.css">
	<script type="text/javascript" src="${contextPath}/view/app_js/turn.js"></script>
</head>
<body>

<div class="page page-lottery">
	<header class="header">
		<h2><img src="${contextPath}/view/app_img/lottery_tit.png"></h2>
		<span class="sign"></span>
	</header>
	<section class="page-content">
		<div class="lottery-wrap flex">
			<div id="lottery">
				<table border="0" cellpadding="0" cellspacing="0">
					<tbody><tr>
						<td class="lottery-unit lottery-unit-0 active"><img src="${contextPath}/view/app_img/lottery_unit_0.png"><div class="item-mask"></div></td>
						<td class="lottery-unit lottery-unit-1 active"><img src="${contextPath}/view/app_img/lottery_unit_1.png"><div class="item-mask"></div></td>
						<td class="lottery-unit lottery-unit-2 active"><img src="${contextPath}/view/app_img/lottery_unit_2.png"><div class="item-mask"></div></td>
					</tr>
					<tr>
						<td class="lottery-unit lottery-unit-7 active"><img src="${contextPath}/view/app_img/lottery_unit_3.png"><div class="item-mask"></div></td>
						<td class="roll-btn"><img src="${contextPath}/view/app_img/lottery1.png"></td>
						<td class="lottery-unit lottery-unit-3 active"><img src="${contextPath}/view/app_img/lottery_unit_4.png"><div class="item-mask"></div></td>
					</tr>
					<tr>
						<td class="lottery-unit lottery-unit-6 active"><img src="${contextPath}/view/app_img/lottery_unit_5.png"><div class="item-mask"></div></td>
						<td class="lottery-unit lottery-unit-5 active"><img src="${contextPath}/view/app_img/lottery_unit_6.png"><div class="item-mask"></div></td>
						<td class="lottery-unit lottery-unit-4 active"><img src="${contextPath}/view/app_img/lottery_unit_7.png"><div class="item-mask"></div></td>
					</tr>
					</tbody></table>
			</div>
			<a href="javascript:;" id="reveiveBtn" class="btn get-btn"></a>
		</div>
	</section>
</div>

<div class="fifter"></div>
<!--活动未始开弹窗-->
<div class="model model-notBegin">
	<div class="model-wrap"><img src="${contextPath}/view/app_img/tips_not_begin.png"></div>
</div>
<!--活动已结束-->
<div class="model model-over">
	<div class="model-wrap"><img src="${contextPath}/view/app_img/tips_over.png"></div>
</div>
<!--重复领取-->
<div class="model model-repeat">
	<div class="model-wrap"><img src="${contextPath}/view/app_img/tips_repeat.png"></div>
</div>
</body>
</html>