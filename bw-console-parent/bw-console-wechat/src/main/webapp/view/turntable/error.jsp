<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
	<title></title>
	<meta name="format-detection" content="telephone=no,address=no,email=no" />
	<meta name="mobileOptimized" content="width" />
	<meta name="handheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<style type="text/css">
		body{
			/*
		  background: url("${contextPath}/images/body_bg.jpg") repeat-y;
		  -moz-background-size: 100% auto;
		  background-size: 100% auto;
		  */
		  background: #E2D5C4;
		}
		.kuang{
			width: 95%;
			background: url("${contextPath}/view/app_img/activityTip_picZ.png") no-repeat;
		   -moz-background-size: 100% 100%;
		  	background-size: 100% 100%;
		  	margin: 0px auto;
		}
	</style>
</head>
<body>
	<div class="publicTop" style="position: relative;">
		<img src="${contextPath}/view/app_img/top_bg_J.png" class="pct100 vm"/>
		<div style="position: absolute;z-index: 2;top: 0;left: 0;width: 100%;">
			<div class="pct20 auto" style="padding: 0.5rem 0px 0.5rem 0px;">
				<img src="${contextPath}/view/app_img/logo.png" class="pct100 vm"/>
			</div>
			<div style="height: 0.75rem;">&nbsp;</div>
			<div style="height: 0.25rem;">&nbsp;</div>
			<div style="height: 2.75rem;">&nbsp;<!--占位--></div>
		</div>
	</div>
	<div style="height: 0.25rem;">&nbsp;</div>
	<div class="kuang">
		<table class="pct100">
			<tr>
				<td colspan="3">
					<img src="${contextPath}/view/app_img/activityTip_pic001_J.png" class="pct100 vm"/>
				</td>
			</tr>
			<tr>
				<td style="width: 12.5%;">
					<img src="${contextPath}/view/app_img/activityTip_pic002_J.png" class="pct100 vm" style="height: 100%;"/>
				</td>
				<td style="width:75%;text-align: center;">
					<table style="width: 100%;text-align: center;color: #fff;table-layout: fixed;">
						<tr>
							<td style="font-size: 0.75rem;font-family: sans-serif;font-weight: bold;">
								${error_txt }
							</td>
						</tr>
						<tr>
							<td style="font-size: 0.35rem;font-family: sans-serif;font-weight: bold;">
								${error_englist }
							</td>
						</tr>
					</table>
				</td>
				<td style="width: 12.5%;">
					<img src="${contextPath}/view/app_img/activityTip_pic003_J.png" class="pct100 vm" style="height: 100%;"/>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<img src="${contextPath}/view/app_img/activityTip_pic004_J.png" class="pct100 vm"/>
				</td>
			</tr>
		</table>
	</div>
	<div style="height: 0.75rem;">&nbsp;</div>
	<!-- <div class="db pct50 auto bs20 tc" style="background: #b2a592;color: #e6e0d9;font-size: 0.45rem;padding: 0.25rem 0px 0.25rem 0px;">
		领取奖品
	</div> -->
	<div style="height: 0.75rem;">&nbsp;</div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
</script>
</html>