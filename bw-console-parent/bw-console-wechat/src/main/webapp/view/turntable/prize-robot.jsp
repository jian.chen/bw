<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>我的金尊之旅-领取奖品</title>
	<link rel="stylesheet" type="text/css" href="../app_css/page.css">
</head>
<body>
<div class="page page-prize">
	<section class="header">
		<h2><img src="${contextPath}/view/app_img/lottery_tit.png"></h2>
		<span class="sign"></span>
	</section>
	<section class="page-content">
		<div class="prize-wrap flex">
			<div class="prize-robot">
				<img src="${contextPath}/view/app_img/prize_robot.png">
				<h2 class="flex">
					<p>恭喜您获得</p>
					<span>戴森空气净化器</span>
				</h2>
			</div>
			<a href="javascript:;" class="btn fill-btn"></a>
		</div>
	</section>
</div>
</body>
</html>