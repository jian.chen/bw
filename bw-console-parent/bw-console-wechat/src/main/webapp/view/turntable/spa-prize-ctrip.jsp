<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>我的金尊之旅-领取奖品</title>
	<link rel="stylesheet" type="text/css" href="${contextPath }/view/app_css/page.css">
</head>
<body>
<div class="page page-prize">
	<section class="header">
		<h2><img src="${contextPath }/view/app_img/lottery_tit.png"></h2>
		<span class="sign"></span>
	</section>
	<section class="page-content">
		<div class="prize-wrap spa-prize-wrap">
			<div class="prize-letter">
				<h2 class="flex">
					<p>恭喜您获得<i>电影票1张</i></p>
					<span>${couponInstanceCode }</span>
				</h2>
			</div>
			<a href="http://m.maizuo.com/v4/?co=maizuo#!/card/query" class="change-btn"></a>
			<div class="use-flow">
				<dl>
					<dt>使用流程</dt>
					<dd>1. 微信上搜索输入“卖座网”公众号,选择“购票”,选择影片,点击立即购买</dd>
					<dd>2. 选择时间及场次</dd>
					<dd>3. 选择座位</dd>
					<dd>4. 输入手机号码(如果是新用户,需要先注册)</dd>
					<dd>5. 选择卖座卡兑换</dd>
					<dd>6. 选择电子卖座卡查询</dd>
					<dd>7. 输入“电子卖座卡号”查询无误即可支付</dd>
					<dd>8. 线上支付完成,凭短信到选定影院出票</dd>
					<dd>9. 可兑换单张电影票活动可直接兑换3D影片(65元以下),部分影城需2张换1张</dd>
					<dd>10.有效期：2017年12月31号</dd>
				</dl>
			</div>
		</div>
	</section>
</div>
</body>
</html>