<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<meta name="wap-font-scale" content="no">
	<meta name="aplus-touch" content="1">
	<meta content="yes" name="apple-touch-fullscreen">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<title>我的金尊之旅</title>
	<link rel="stylesheet" type="text/css" href="${contextPath}/view/app_css/page.css">
</head>
<body>

<div class="page page-lottery">
	<header class="header">
		<h2><img src="${contextPath}/view/app_img/lottery_tit.png"></h2>
		<span class="sign"></span>
	</header>
	<section class="page-content">
		<div class="lottery-wrap flex">
			<div id="lottery">
				<table border="0" cellpadding="0" cellspacing="0">
					<tbody><tr>
						<td class="lottery-unit lottery-unit-0 active"><img src="${contextPath}/view/app_img/lottery_0.png"><div class="item-mask"></div></td>
						<td class="lottery-unit lottery-unit-1 active"><img src="${contextPath}/view/app_img/lottery_1.png"><div class="item-mask"></div></td>
						<td class="lottery-unit lottery-unit-2 active"><img src="${contextPath}/view/app_img/lottery_2.png"><div class="item-mask"></div></td>
					</tr>
					<tr>
						<td class="lottery-unit lottery-unit-7 active"><img src="${contextPath}/view/app_img/lottery_7.png"><div class="item-mask"></div></td>
						<td class="roll-btn"><img src="${contextPath}/view/app_img/lottery1.png"></td>
						<td class="lottery-unit lottery-unit-3 active"><img src="${contextPath}/view/app_img/lottery_3.png"><div class="item-mask"></div></td>
					</tr>
					<tr>
						<td class="lottery-unit lottery-unit-6 active"><img src="${contextPath}/view/app_img/lottery_6.png"><div class="item-mask"></div></td>
						<td class="lottery-unit lottery-unit-5 active"><img src="${contextPath}/view/app_img/lottery_3.png"><div class="item-mask"></div></td>
						<td class="lottery-unit lottery-unit-4 active"><img src="${contextPath}/view/app_img/lottery_4.png"><div class="item-mask"></div></td>
					</tr>
					</tbody></table>
			</div>
			<a href="javascript:;" class="btn get-btn"></a>
		</div>
	</section>
</div>

<div class="fifter"></div>
<!--活动未始开弹窗-->
<div class="model model-notBegin">
	<div class="model-wrap"><img src="${contextPath}/view/app_img/tips_not_begin.png"></div>
</div>
<!--活动已结束-->
<div class="model model-over">
	<div class="model-wrap"><img src="${contextPath}/view/app_img/tips_over.png"></div>
</div>
<!--重复领取-->
<div class="model model-repeat">
	<div class="model-wrap"><img src="${contextPath}/view/app_img/tips_repeat.png"></div>
</div>
<script src="${contextPath}/view/app_js/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(function() {
	var couponInstanceCode;//券号
    var lottery = {
        place: 0, //请求后指定停留在某个位置
        click: false, //默认值为false可抽奖，防止重复点击
        index: -1, //当前转动到哪个位置，起点位置
        count: 0, //总共有多少个位置
        timer: 0, //setTimeout的ID，用clearTimeout清除
        speed: 20, //初始转动速度
        times: 0, //转动次数
        cycle: 50, //转动基本次数：即至少需要转动多少次再进入抽奖环节
        prize: -1, //中奖位置
        
        init: function(id) {
            if ($("#" + id).find(".lottery-unit").length > 0) {
                $lottery = $("#" + id);
                $units = $lottery.find(".lottery-unit");
                this.obj = $lottery;
                this.count = $units.length;
                $lottery.find(".lottery-unit-" + this.index).addClass("active");
            };
        },
        roll: function() {
            var index = this.index,
                count = this.count,
                lottery = this.obj;
            $(lottery).find(".lottery-unit-" + index).removeClass("active");
            index += 1;
            if (index > count - 1) {
                index = 0;
            };
            $(lottery).find(".lottery-unit-" + index).addClass("active");
            this.index = index;
            return false;
        },
        stop: function() {
            lottery.times += 1;
            lottery.roll(); //转动过程调用的是lottery的roll方法，这里是第一次调用初始化
            if (lottery.times > lottery.cycle + 10 && lottery.prize == lottery.index) {
                clearTimeout(lottery.timer);
                lottery.prize = -1;
                lottery.times = 0;
                lottery.click = false;
                //可以在这个位置写上中奖弹框，这个是转盘停止时触发事件
                lotteryGo(couponInstanceCode,lottery.place);
                console.log('您抽中了第' + lottery.place + '个奖品');
            } else {
                if (lottery.times < lottery.cycle) {
                    lottery.speed -= 10;
                } else if (lottery.times == lottery.cycle) {
                    //lottery.place = Math.random() * (lottery.count) | 0; //案例中奖物品通过一个随机数生成
                    lottery.prize = lottery.place;
                    // lottery.prize = lottery.place;  //这个可以通过ajax请求回来的数据赋值给lottery.place
                } else {
                    if (lottery.times > lottery.cycle + 10 && ((lottery.prize == 0 && lottery.index == 7) || lottery.prize == lottery.index + 1)) {
                        lottery.speed += 110;
                    } else {
                        lottery.speed += 20;
                    }
                }
                if (lottery.speed < 40) {
                    lottery.speed = 40;
                };
                lottery.timer = setTimeout(lottery.stop, lottery.speed); //循环调用
            }
            return false;
        },
        getLottery: function() {//ajax请求中奖接口，本案例注释便于案例正常展示效果，实际中可参考注释的代码
        	
        	$.ajax({
        		type : "post",
        		url : '/bw-console-wechat/lottery/extractMemberAwardsMaps',
        		dataType : "json",
        		async: false,
        		success : function(data) {
        			if (data.code == 200) {//200 代表成功
        				lottery.speed = 100;
                        lottery.stop(); //转圈过程不响应click事件，会将click置为false
                        lottery.click = true; //一次抽奖完成后，设置click为true，可继续抽奖
                        switch (data.data.awardsCode) {//请求返回的抽中奖品字段
                         case 'LOTTERY_TYPE_JINGDONG_COUPON': //京东
                        	 couponInstanceCode=data.data.couponInstanceCode;
                             lottery.place = 0;//当前奖品所在九宫格位置
                             break;
                         case 'LOTTERY_TYPE_FILM_COUPON': //电影券
                        	 couponInstanceCode=data.data.couponInstanceCode;
                             lottery.place = 1;//当前奖品所在九宫格位置
                             break;
                         case 'LOTTERY_TYPE_INTELLIGENT_CLEANER_ROBOT': //智能扫地机器人
                             lottery.place = 2;//当前奖品所在九宫格位置
                             break;
                         case 'LOTTERY_TYPE_MEITUAN_PRESS': //美团
                        	 couponInstanceCode=data.data.couponInstanceCode;
                             lottery.place = 3;//美团
                             break;
                         case 'LOTTERY_TYPE_STARBUCK_COUPON': //星巴克
                        	 couponInstanceCode=data.data.couponInstanceCode;
                             lottery.place = 4;//当前奖品所在九宫格位置
                             break;
                         case 'LOTTERY_TYPE_MEITUAN_PRESS': //星巴克
                        	 couponInstanceCode=data.data.couponInstanceCode;
                             lottery.place = 5;//当前奖品所在九宫格位置
                             break;
                         case 'LOTTERY_TYPE_XIECHENG_COUPON': //携程
                        	 couponInstanceCode=data.data.couponInstanceCode;
                             lottery.place = 6;//当前奖品所在九宫格位置
                             break;
                         case 'LOTTERY_TYPE_AIQIYI_MEMBER': //星巴克
                        	 couponInstanceCode=data.data.couponInstanceCode;
                             lottery.place = 7;//爱奇艺
                             break;
                     }
        			}else{
        				alert(data.message)
        			}
        		},
        		error : function(data) {
        			alert('出错了..')
        		}
        	});

        }
    };
	function lotteryGo(couponInstanceCode,place){
		
		if(couponInstanceCode==''){
   		 	alert('券已领完，请等候库存的补充');
   		 	return;
   	 	}
		 switch (place) {//请求返回的抽中奖品字段
         case 0: //京东
        	 window.location.href="/bw-console-wechat/gift/turntable?page=spa-prize-jd&couponInstanceCode="+couponInstanceCode;
             break;
         case 1: //电影券
        	 window.location.href="/bw-console-wechat/gift/turntable?page=spa-prize-ctrip&couponInstanceCode="+couponInstanceCode;
             break;
         case 2: //智能扫地机器人
        	 window.location.href="/bw-console-wechat/gift/turntable?page=prize-robot";
             break;
         case 3: //美团
        	 window.location.href="/bw-console-wechat/gift/turntable?page=spa-prize-meituan&couponInstanceCode="+couponInstanceCode;
             break;
         case 4: //星巴克
        	 window.location.href="/bw-console-wechat/gift/turntable?page=spa-prize-starbucks&couponInstanceCode="+couponInstanceCode;
             break;
         case 5: //美团
        	 window.location.href="/bw-console-wechat/gift/turntable?page=spa-prize-meituan&couponInstanceCode="+couponInstanceCode;
             break;
         case 6: //携程
        	 window.location.href="/bw-console-wechat/gift/turntable?page=spa-prize-ctrip&couponInstanceCode="+couponInstanceCode;
             break;
         case 7: //爱奇艺
        	 window.location.href="/bw-console-wechat/gift/turntable?page=spa-prize-iqy&couponInstanceCode="+couponInstanceCode;
             break;
     }
		
		
	}
    $("#lottery .roll-btn").on('click', function(event) {
        event.preventDefault();
        lottery.init('lottery');
        if (lottery.click) { //click控制一次抽奖过程中不能重复点击抽奖按钮，后面的点击不响应
            return false;
        } else {
            lottery.getLottery();
            return false;
        }
    });
    $("#reveiveBtn").on('click', function(event) {
    	window.location.href="/bw-console-wechat/gift/myMemberGift";
    });
});

</script>
</body>
</html>