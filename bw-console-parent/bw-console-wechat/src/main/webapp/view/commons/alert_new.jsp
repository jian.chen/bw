<%@page language="java" contentType="text/html; charset=UTF-8"%>
<!-- 默认弹出模态框  begin -->
<div class="modal fade" id="ycf-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog dialog_sm">
		<div class="modal-content">
			<div class="modal-header header_sm bg-primary">
				<h4 class="modal-title" id="myModalLabel">
					提示
				</h4>
			</div>
			<div class="modal-body body_sm">
					<dl class="dl-horizontal dd_sm">
						<dt class="dt_w">
							<img id="alert_img_index" src="<%=request.getContextPath()%>/images/rights_03.png"/>
						</dt>
						<dd id="data_msg_index" class="text-muted" style="margin-left: 105px;padding-top: 8px;">
							操作成功!
						</dd>
					</dl>
			</div>
			<div class="modal-footer" style="padding-top:0px">
				<button class="modal_btn1 btn btn-primary"  type="button" data-dismiss="modal" >
					确定
				</button>
			</div>
		</div>
	</div>
</div>
	<!-- 默认弹出模态框  end-->