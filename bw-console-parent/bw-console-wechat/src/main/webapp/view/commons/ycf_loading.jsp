<%@page language="java" contentType="text/html; charset=UTF-8"%>
<div class="modal fade" id="ycf-loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width: 220px;height: 120px;margin: 25% auto;">
					<div class="modal-content">
						<div class="modal-body form-inline">
							
								<div class="form-group" style="margin-bottom: 0;">
									<label class="control-label">
										<img id="alert_img_index" src="<%=request.getContextPath()%>/images/progressBar_m.gif"/>
									</label>
									<label id="data_msg_index" class="control-label" >
										数据正在加载中,请稍后...
									</label>
								</div>
							
						</div>
						
					</div>
				</div>
			</div>
