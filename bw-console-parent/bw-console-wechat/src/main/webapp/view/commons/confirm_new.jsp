<%@page language="java" contentType="text/html; charset=UTF-8"%>
<!-- 默认弹出模态框  begin -->
	<div class="modal fade" id="ycf-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width: 580px;">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" style="color: #fff;" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">
						提示
					</h4>
				</div>
				<div class="modal-body">
					<div class="my_mg">
						<dl class="dl-horizontal">
							<dt style="padding-top: 12px;">
								<img src="<%=request.getContextPath()%>/images//biaoti.png"/>
							</dt>
							<dd>
								<p class="h4">[Title]</p>
								<p class="text-muted">[Message]</p>
							</dd>
						</dl>
					</div>
				</div>
				<div class="modal-footer">
					<!-- 94 32 -->
					<button class="modal_btn1 btn btn-primary ok"  type="button" data-dismiss="modal">
						[BtnOk]
					</button>
					<button class="modal_btn2 btn btn-default cancel" type="button" data-dismiss="modal">
						[BtnCancel]
					</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal -->
	</div>
	<!-- 默认弹出模态框  end-->