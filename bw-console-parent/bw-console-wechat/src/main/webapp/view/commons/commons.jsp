<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
	String baseUrl = request.getContextPath();
	request.setAttribute("contextPath", baseUrl);
%>

<script>
	var baseUrl = "${contextPath}";
	var baseUrl_html = "scrm/view/";
</script>
<script src="${contextPath}/view/app_js/jquery-2.1.0.min.js"></script>
