<%@page language="java" contentType="text/html; charset=UTF-8"%>
<style>
    .dd_sm dd{margin-left:0;}
</style>
<!-- 默认弹出模态框  begin -->
<div class="modal fade" id="globle-alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog dialog_sm" style="margin:5% auto">
		<div class="modal-content">
			<div class="modal-header header_sm bg-primary">
				<h4 class="modal-title" id="myModalLabel">
					提示
				</h4>
			</div>
			<div class="modal-body body_sm">
					<dl class="dl-horizontal dd_sm padding0 margin0 text-center">
						<dd id="data_msg" class="text-muted margin0" style="font-size:16px;">
							操作成功!
						</dd>
					</dl>
			</div>
			<div class="modal-footer">
				<button class="modal_btn1 btn btn-primary" style="border-radius: 4px;" type="button" data-dismiss="modal" >
					确定
				</button>
			</div>
		</div>
	</div>
</div>
	<!-- 默认弹出模态框  end-->