<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../commons/commons.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="format-detection" content="telephone=no"/>
	<title>产品介绍</title>
	<link rel="stylesheet" type="text/css" href="${contextPath }/view/app_css/page.css">
</head>
<body>
	<div class="page page-product-pre">
		<header class="header">
			<h2><img src="${contextPath }/view/app_img/product_pre_tit.png"></h2>
			<span class="sign"></span>
		</header>
		<section class="page-content">
			<div class="wrapper theme-wrapper">
				<div class="product-details">
					<div class="details-item"><img src="${contextPath }/view/app_img/item_details_01.png"></div>
					<div class="details-item"><img src="${contextPath }/view/app_img/item_details_02.png"></div>
					<div class="details-item"><img src="${contextPath }/view/app_img/item_details_03.png"></div>
				</div>
			</div>
		</section>
	</div>
</body>
</html>