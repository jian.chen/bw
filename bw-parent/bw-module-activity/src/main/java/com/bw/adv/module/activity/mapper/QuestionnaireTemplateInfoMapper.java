package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.QuestionCount;
import com.bw.adv.module.activity.model.QuestionnaireTemplateInfo;

public interface QuestionnaireTemplateInfoMapper extends BaseMapper<QuestionnaireTemplateInfo>{
    int deleteByPrimaryKey(Long infoId);

    int insert(QuestionnaireTemplateInfo record);

    int insertSelective(QuestionnaireTemplateInfo record);

    QuestionnaireTemplateInfo selectByPrimaryKey(Long infoId);

    int updateByPrimaryKeySelective(QuestionnaireTemplateInfo record);

    int updateByPrimaryKey(QuestionnaireTemplateInfo record);
    
    /**
     * 查询题目选项
     * @param templateId
     * @return
     */
    public List<QuestionnaireTemplateInfo> queryTemplateInfoByTemplateId(Long templateId);
    
    /**
     * 删除题目选项
     * @param templateId
     */
    public void deleteTemplateInfoByTemplateId(Long templateId);
    
    /**
     * 查询选择题统计结果
     * @param templateId
     * @return
     */
    @Select("select qti.INFO_ID as infoId,qt.TYPE as type,COUNT(qi.INSTANCE_ID) as count FROM "
    		+ "questionnaire q "
    		+ "join questionnaire_template qt on q.QUESTIONNAIRE_ID = qt.QUESTIONNAIRE_ID "
    		+ "right join questionnaire_template_info qti on qt.QUESTIONNAIRE_TEMPLATE_ID = qti.QUESTIONNAIRE_TEMPLATE_ID "
    		+ "left join questionnaire_instance qi on qi.INFO_ID = qti.INFO_ID "
    		+ "where qti.QUESTIONNAIRE_TEMPLATE_ID = #{templateId} "
    		+ "GROUP BY qti.INFO_ID "
    		+ "ORDER BY qti.INFO_ID ASC;")
    List<QuestionCount> queryResultCountForSelect(Long templateId);
    
    /**
     * 查询打分题统计结果
     * @param templateId
     * @return
     */
    @Select("select qti.INFO_ID as infoId,qt.TYPE as type,qi.ANSWER as answer,COUNT(qi.INSTANCE_ID) as count FROM "
    		+ "questionnaire q "
    		+ "join questionnaire_template qt on q.QUESTIONNAIRE_ID = qt.QUESTIONNAIRE_ID "
    		+ "right join questionnaire_template_info qti on qt.QUESTIONNAIRE_TEMPLATE_ID = qti.QUESTIONNAIRE_TEMPLATE_ID "
    		+ "left join questionnaire_instance qi on qi.INFO_ID = qti.INFO_ID "
    		+ "where qti.QUESTIONNAIRE_TEMPLATE_ID = #{templateId} "
    		+ "GROUP BY qi.ANSWER "
    		+ "ORDER BY qi.ANSWER ASC;")
    List<QuestionCount> queryResultCountForMark(Long templateId);
    
    /**
     * 查询问答题统计结果
     * @param templateId
     * @return
     */
    @Select("select qti.INFO_ID as infoId,qt.TYPE as type,qi.ANSWER as answer FROM "
    		+ "questionnaire q "
    		+ "join questionnaire_template qt on q.QUESTIONNAIRE_ID = qt.QUESTIONNAIRE_ID "
    		+ "right join questionnaire_template_info qti on qt.QUESTIONNAIRE_TEMPLATE_ID = qti.QUESTIONNAIRE_TEMPLATE_ID "
    		+ "left join questionnaire_instance qi on qi.INFO_ID = qti.INFO_ID "
    		+ "where qti.QUESTIONNAIRE_TEMPLATE_ID = #{templateId}")
    List<QuestionCount> queryResultCountForQa(Long templateId);
    
    /**
     * 查询文件一共回答的人数
     * @param templateId
     * @return
     */
    @Select("SELECT count(*) from( "
    		+ "select qi.MEMBER_ID FROM "
    		+ "questionnaire q "
    		+ "join questionnaire_template qt on q.QUESTIONNAIRE_ID = qt.QUESTIONNAIRE_ID "
    		+ "join questionnaire_template_info qti on qt.QUESTIONNAIRE_TEMPLATE_ID = qti.QUESTIONNAIRE_TEMPLATE_ID "
    		+ "join questionnaire_instance qi on qi.INFO_ID = qti.INFO_ID "
    		+ "where qt.QUESTIONNAIRE_ID = #{templateId} "
    		+ "group by qi.MEMBER_ID)tmp")
	Long queryCountInfoForMember(Long templateId);
    
}