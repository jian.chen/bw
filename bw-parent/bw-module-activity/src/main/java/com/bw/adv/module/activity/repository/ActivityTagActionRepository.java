package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.ActivityTagActionMapper;
import com.bw.adv.module.activity.model.ActivityTagAction;

public interface ActivityTagActionRepository extends BaseRepository<ActivityTagAction, ActivityTagActionMapper>{
	
	/**
	 * findByIsNotCountWithOrderByHappenTime:(查询所有未计算的标签动作，并根据发生时间正序排序). <br/>
	 * @return
	 */
	List<ActivityTagAction> findByIsNotCountWithOrderByHappenTime();

}
