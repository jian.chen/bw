/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:SceneGameRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2016年1月15日下午1:40:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.SceneGameMapper;
import com.bw.adv.module.activity.model.SceneGame;

/**
 * ClassName:SceneGameRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午1:40:24 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
public interface SceneGameRepository extends BaseRepository<SceneGame, SceneGameMapper> {
	
	/**
	 * findAll:根据业务归属查询活动. <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * Date: 2016年3月22日 上午9:39:07 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang(*^_^*)
	 * @version jdk1.7
	 * @param map
	 * @return
	 */
	public List<SceneGame> findAll();

}

