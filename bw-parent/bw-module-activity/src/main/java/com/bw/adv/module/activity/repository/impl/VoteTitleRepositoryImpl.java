package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.VoteTitleMapper;
import com.bw.adv.module.activity.model.VoteTitle;
import com.bw.adv.module.activity.repository.VoteTitleRepository;

@Repository
public class VoteTitleRepositoryImpl extends BaseRepositoryImpl<VoteTitle, VoteTitleMapper> implements VoteTitleRepository{

	@Override
	protected Class<VoteTitleMapper> getMapperClass() {
		return VoteTitleMapper.class;
	}

	@Override
	public List<VoteTitle> findAllVoteAndOptionCount(Page<VoteTitle> templatePage) {
		return this.getMapper().findAllVoteAndOptionCount(templatePage);
	}

	@Override
	public Long queryActiveVoteAmount() {
		return this.getMapper().queryActiveVoteAmount();
	}

	@Override
	public VoteTitle findActiveVote() {
		return this.getMapper().queryActiveVote();
	}

}
