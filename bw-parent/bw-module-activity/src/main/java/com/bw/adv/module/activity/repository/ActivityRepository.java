/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.ActivityMapper;
import com.bw.adv.module.activity.model.Activity;

/**
 * ClassName:活动 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface ActivityRepository extends BaseRepository<Activity, ActivityMapper>{
	
	/**
	 * findAllActive:查询所有活动<br/>
	 * Date: 2015-8-11 下午2:42:37 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<Activity> findAllActive();
	
	/**
	 * 根据类型查找活动
	 * @param typeId
	 * @return
	 */
	List<Activity> findActiveByTypeId(Long typeId);
	
}

