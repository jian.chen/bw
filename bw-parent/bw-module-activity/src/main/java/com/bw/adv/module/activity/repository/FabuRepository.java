package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.FabuChannelMapper;
import com.bw.adv.module.activity.model.FabuChannel;

public interface FabuRepository extends BaseRepository<FabuChannel, FabuChannelMapper>{
	
	public List<FabuChannel> queryAllGroupByChannel();

}
