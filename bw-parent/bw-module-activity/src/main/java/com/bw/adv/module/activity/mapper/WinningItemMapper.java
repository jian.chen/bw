package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.WinningItemExp;
import com.bw.adv.module.common.model.Example;

public interface WinningItemMapper extends BaseMapper<WinningItem>{
    /**
     * selectWinningItemPersonsByActivityIdAndAwardId:(查询某个活动对应的奖项获奖人数). <br/>
     * Date: 2015年11月25日 上午10:40:34 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param example
     * @param page
     * @return
     */
	List<WinningItem> selectWinningItemPersonsByActivityIdAndAwardId(@Param("example")Example example, Page<WinningItem> page);
	/**
	 * selectWinningItemPersonsByActivityIdAndAwardId:(查询某个活动对应的奖项获奖人数). <br/>
	 * Date: 2015年11月25日 上午10:40:34 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	Long selectWinningItemPersonsByExample(@Param("example")Example example);
	
	/**
	 * 
	 * selectWinningItemListByMemberIdAndActivityId:(查询会员获奖记录). <br/>
	 * Date: 2015年12月15日 上午11:17:42 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<WinningItemExp> selectWinningItemListByMemberIdAndActivityId(@Param("example") Example example, Page<WinningItemExp> page);
	
	/**
	 * 
	 * selectItemLastNum:(查询最近几条中奖纪录). <br/>
	 * Date: 2016年3月22日 下午6:10:33 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param num
	 * @return
	 */
	List<WinningItemExp> selectItemLastNum(@Param("num")Long num);
}