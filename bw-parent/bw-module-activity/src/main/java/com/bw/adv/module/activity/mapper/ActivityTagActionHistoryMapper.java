package com.bw.adv.module.activity.mapper;

import com.bw.adv.module.activity.model.ActivityTagActionHistory;

public interface ActivityTagActionHistoryMapper {
    int deleteByPrimaryKey(Long tagActionId);

    int insert(ActivityTagActionHistory record);

    int insertSelective(ActivityTagActionHistory record);

    ActivityTagActionHistory selectByPrimaryKey(Long tagActionId);

    int updateByPrimaryKeySelective(ActivityTagActionHistory record);

    int updateByPrimaryKeyWithBLOBs(ActivityTagActionHistory record);

    int updateByPrimaryKey(ActivityTagActionHistory record);
}