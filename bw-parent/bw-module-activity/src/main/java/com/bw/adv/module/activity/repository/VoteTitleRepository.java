package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.VoteTitleMapper;
import com.bw.adv.module.activity.model.VoteTitle;

public interface VoteTitleRepository extends BaseRepository<VoteTitle, VoteTitleMapper>{
	
	/**
	 * 查询所有的投票并且带选项数量
	 * @return
	 */
	List<VoteTitle> findAllVoteAndOptionCount(Page<VoteTitle> templatePage);

	/**
	 *	查询激活的投票数量
	 * queryActiveVoteAmount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public Long queryActiveVoteAmount();
	
	/**
	 * 查询激活的投票
	 * findActiveVote:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public VoteTitle findActiveVote();
}
