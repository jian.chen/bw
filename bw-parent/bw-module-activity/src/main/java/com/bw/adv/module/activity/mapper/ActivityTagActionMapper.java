package com.bw.adv.module.activity.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.ActivityTagAction;

public interface ActivityTagActionMapper extends BaseMapper<ActivityTagAction>{
    int deleteByPrimaryKey(Long tagActionId);

    int insert(ActivityTagAction record);

    int insertSelective(ActivityTagAction record);

    ActivityTagAction selectByPrimaryKey(Long tagActionId);

    int updateByPrimaryKeySelective(ActivityTagAction record);

    int updateByPrimaryKeyWithBLOBs(ActivityTagAction record);

    int updateByPrimaryKey(ActivityTagAction record);
    
    /**
	 * findByIsNotCountWithOrderByHappenTime:(查询所有未计算的标签动作，并根据发生时间正序排序). <br/>
	 * @return
	 */
	List<ActivityTagAction> selectByIsNotCountWithOrderByHappenTime();
}