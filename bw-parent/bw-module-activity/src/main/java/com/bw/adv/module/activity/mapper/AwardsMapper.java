package com.bw.adv.module.activity.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.Awards;
import com.bw.adv.module.activity.model.exp.AwardsExp;

public interface AwardsMapper extends BaseMapper<Awards>{
    int deleteByPrimaryKey(Long awardsId);

    int insert(Awards record);

    int insertSelective(Awards record);

    Awards selectByPrimaryKey(Long awardsId);

    int updateByPrimaryKeySelective(Awards record);

    int updateByPrimaryKey(Awards record);
    
    /**
     * selectAwardsBySceneGameInstanceId:查询活动实例的奖项明细. <br/>
     * Date: 2016年1月19日 上午11:29:49 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    List<AwardsExp> selectAwardsBySceneGameInstanceId(Long sceneGameInstanceId);
    
    /**
     * deleteAwards:删除活动实例对应的奖项. <br/>
     * Date: 2016年1月19日 下午4:39:19 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    int deleteAwards(Long sceneGameInstanceId);
    
    /**
     * selectLuckyAwards:查询活动的谢谢参与奖项. <br/>
     * Date: 2016年1月22日 下午1:25:58 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    AwardsExp selectLuckyAwards(Long sceneGameInstanceId);
    
    /**
     * selectAwardsByAwardsId:根据奖项ID,查询奖项信息. <br/>
     * TODO(这里描述这个方法适用条件 – 可选).<br/>
     * Date: 2016年3月1日 下午5:06:30 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang(*^_^*)
     * @version jdk1.7
     * @param AwardsId
     * @return
     */
    AwardsExp selectAwardsByAwardsId(Long awardsId);

	/**
	 * selectByGameCode:(根据游戏编号查询奖项列表，只查询有效的游戏:已保存，已激活). <br/>
	 * Date: 2016-3-31 下午6:28:02 <br/>
	 * scrmVersion standard
	 * @author mennan
	 * @version jdk1.7
	 * @param gameCode
	 * @return
	 */
	List<Awards> selectByGameCode(String gameCode);
    
}