package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.VoteOptions;

public interface VoteOptionsMapper extends BaseMapper<VoteOptions>{
    int deleteByPrimaryKey(Long optionsId);

    int insert(VoteOptions record);

    int insertSelective(VoteOptions record);

    VoteOptions selectByPrimaryKey(Long optionsId);

    int updateByPrimaryKeySelective(VoteOptions record);

    int updateByPrimaryKey(VoteOptions record);
    
    /**
	 * 根据投票id查询选项
	 * @param voteId
	 * @param templatePage
	 * @return
	 */
    @Select("select options_id as optionsId,TITLE_ID as titleId,OPTION_NAME optionName,OPTION_IMG as optionImg,"
    		+ "OPTION_DETAIL as optionDetail "
    		+ "from vote_options "
    		+ "where TITLE_ID = ${titleId}")
	List<VoteOptions> findVoteOptionsByTitleId(@Param("titleId")Long titleId,Page<VoteOptions> templatePage);
}