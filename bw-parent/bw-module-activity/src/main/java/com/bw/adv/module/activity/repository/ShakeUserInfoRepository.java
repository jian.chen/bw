package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.ShakeUserInfoMapper;
import com.bw.adv.module.activity.model.ShakeUserInfo;
import com.bw.adv.module.activity.model.UserAndWinning;

public interface ShakeUserInfoRepository extends BaseRepository<ShakeUserInfo, ShakeUserInfoMapper> {
	
	/**
	 * 根据手机号查询参与者信息
	 * @param phoneNum
	 * @return
	 */
	ShakeUserInfo findShakeUserInfoByPhoneNum(String phoneNum);
	
	/**
	 * 产品发布会用抽奖查询结果
	 * @return
	 */
	List<UserAndWinning> selectAllForUserAndWinning();
}
