/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.ActivityActionMapper;
import com.bw.adv.module.activity.model.ActivityAction;

/**
 * ClassName:活动动作 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface ActivityActionRepository extends BaseRepository<ActivityAction, ActivityActionMapper>{
	
	/**
	 * findByIsNotCountWithOrderByHappenTime:(查询所有未计算的活动动作，并根据发生时间正序排序). <br/>
	 * Date: 2015-8-29 上午10:56:58 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<ActivityAction> findByIsNotCountWithOrderByHappenTime(int size);
	
	
	/**
	 * findIsNotCountByActivityCode:(根据活动编码查询活动动作). <br/>
	 * Date: 2015-9-13 下午4:04:56 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityCode
	 * @return
	 */
	List<ActivityAction> findIsNotCountByActivityCode(String activityCode);
	
	/**
	 * findIsNotCountByBusinessCode:(根据业务编码查询活动动作). <br/>
	 * Date: 2015-9-13 下午4:04:33 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param businessCode
	 * @return
	 */
	List<ActivityAction> findIsNotCountByBusinessCode(String businessCode);
}

