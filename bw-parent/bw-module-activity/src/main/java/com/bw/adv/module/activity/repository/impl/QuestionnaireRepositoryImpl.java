package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.QuestionnaireMapper;
import com.bw.adv.module.activity.model.Questionnaire;
import com.bw.adv.module.activity.repository.QuestionnaireRepository;

@Repository
public class QuestionnaireRepositoryImpl extends BaseRepositoryImpl<Questionnaire, QuestionnaireMapper> implements QuestionnaireRepository{

	@Override
	protected Class<QuestionnaireMapper> getMapperClass() {
		return QuestionnaireMapper.class;
	}

	@Override
	public List<Questionnaire> selectAllQuestionWithTemplateCount(Page<Questionnaire> templatePage) {
		return this.getMapper().selectAllQuestionWithTemplateCount(templatePage);
	}

	@Override
	public Questionnaire findActiveQuestion() {
		return this.getMapper().findActiveQuestion();
	}

	@Override
	public Long queryActiveQuestionAmount() {
		return this.getMapper().queryActiveQuestionAmount();
	}

}
