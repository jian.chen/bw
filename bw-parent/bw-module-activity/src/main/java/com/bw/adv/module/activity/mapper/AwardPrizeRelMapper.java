package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.AwardPrizeRel;

public interface AwardPrizeRelMapper extends BaseMapper<AwardPrizeRel>{
    int deleteByPrimaryKey(Long awardsPrizeRelId);

    int insert(AwardPrizeRel record);

    int insertSelective(AwardPrizeRel record);

    AwardPrizeRel selectByPrimaryKey(Long awardsPrizeRelId);

    int updateByPrimaryKeySelective(AwardPrizeRel record);

    int updateByPrimaryKey(AwardPrizeRel record);

    /**
     * deleteRelBySceneGameInstanceId:根据活动实例ID删除所有的奖项奖品关系. <br/>
     * Date: 2016年1月19日 下午3:54:31 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    int deleteRelBySceneGameInstanceId(long sceneGameInstanceId);
    
}