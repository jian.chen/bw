/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:AwardsSettingRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2015年11月20日下午3:05:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.AwardsSettingMapper;
import com.bw.adv.module.activity.model.AwardsSetting;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;

/**
 * ClassName:AwardsSettingRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:05:18 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface AwardsSettingRepository extends BaseRepository<AwardsSetting, AwardsSettingMapper> {
	
	/**
	 * 
	 * findAwardsSettingsByActivityId:(根据活动id查询出对应的奖项设置). <br/>
	 * Date: 2015年11月24日 下午4:45:50 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 */
	List<AwardsSetting> findAwardsSettingsByActivityId(Long activityId);
	
	/**
	 * 
	 * findAwardsSettingExpByActivityId:(根据活动id查询出扩展类). <br/>
	 * Date: 2015年11月26日 下午9:12:46 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 */
	List<AwardsSettingExp> findAwardsSettingExpByActivityId(Long activityId);
	
	/**
	 * removeAwardsSettings:根据活动实例ID删除奖项设置. <br/>
	 * Date: 2016年1月19日 下午4:35:30 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	int removeAwardsSettings(Long sceneGameInstanceId);

	/**
	 * removeAwardsSettings:根据奖品ID查询奖项设置. <br/>
	 * Date: 2016年1月19日 下午4:35:30 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	AwardsSettingExp findAwardsSettingExpByAwardsId(Long awardsId);
	
}

