package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.model.exp.ActivityRuleConditionExp;
import com.bw.adv.module.member.model.Member;

public interface ActivityInstanceMapper extends BaseMapper<ActivityInstance>{

	/**
	 * selectByActivityId:(根据活动ID查询活动实例). <br/>
	 * Date: 2015-8-26 下午6:42:28 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 */
	List<ActivityInstance> selectByActivityId(Long activityId);

	/**
	 * selectByActivityId:(根据活动ID查询活动实例，分页). <br/>
	 * Date: 2015-8-26 下午6:42:48 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityId
	 * @param page
	 * @return
	 */
	List<ActivityInstance> selectByActivityId(@Param("activityId")Long activityId,Page<ActivityInstance> page);
	
	/**
	 * selectExpByPrimaryKey:(根据实例ID查询活动规则条件和条件值). <br/>
	 * Date: 2015-9-1 下午5:51:36 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param instanceId
	 * @return
	 */
	List<ActivityRuleConditionExp> selectExpByPrimaryKey(@Param("activityInstanceId")Long instanceId);
	
	/**
	 * selectActivityConditionValue:(根据活动实例状态，查询所有活动实例及对应的条件值). <br/>
	 * Date: 2015-9-5 下午4:25:24 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param statusIds
	 * @return
	 */
	List<ActivityRuleConditionExp> selectActivityConditionValue(@Param("statusIds")String statusIds);
	
	
	/**
	 * selectExpByStatusIds:(根据状态查询所有活动实例). <br/>
	 * Date: 2015-9-6 下午7:44:41 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param statusIds
	 * @return
	 */
	List<ActivityInstance> selectByStatusIds(@Param("statusIds")String statusIds);
	
	
	/**
	 * selectActivityBirthday:(查询生日活动的会员). <br/>
	 * Date: 2015-9-13 上午11:37:41 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param sendType
	 * @param sendDay 
	 * @return
	 */
	List<Member> selectActivityBirthday(@Param("sendType")String sendType,@Param("sendDay")String sendDay);
	
	/**
	 * selectActivityWake:(查询唤醒活动会员). <br/>
	 * Date: 2015年9月16日 下午7:49:22 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wakeDate
	 * @return
	 */
	List<Member> selectActivityWake(@Param("wakeDate")String wakeDate);
	
	/**
	 * selectActivitySpecial:(查询指定日期会员)
	 * @return
	 */
	List<Member> selectActivitySpecial();
	
}






