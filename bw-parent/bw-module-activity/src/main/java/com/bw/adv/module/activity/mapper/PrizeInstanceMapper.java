package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.PrizeInstance;
import com.bw.adv.module.activity.model.exp.PrizeInstanceExp;

public interface PrizeInstanceMapper extends BaseMapper<PrizeInstance>{
    int deleteByPrimaryKey(Long prizeInstanceId);

    int insert(PrizeInstance record);

    int insertSelective(PrizeInstance record);

    PrizeInstance selectByPrimaryKey(Long prizeInstanceId);

    int updateByPrimaryKeySelective(PrizeInstance record);

    int updateByPrimaryKey(PrizeInstance record);
    
    /**
     * prizeInstanceExpByAwardsId:(根据获得的奖项抽取奖品实例). <br/>
     * Date: 2015年11月26日 上午11:10:18 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param awardsId
     * @return
     */
    PrizeInstanceExp prizeInstanceExpByAwardsId(@Param("awardsId")Long awardsId);
    
    /**
     * 
     * batchInsert:批量掺入奖品实例. <br/>
     * Date: 2016年1月19日 下午3:19:09 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param prizeInstanceList
     * @return
     */
    int batchInsertPrizeInstance(@Param("prizeInstanceList") List<PrizeInstance> prizeInstanceList);
    
    /**
     * deletePrizeInstanceById:删除活动实例对应的奖品实例. <br/>
     * Date: 2016年1月19日 下午4:15:59 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    int deletePrizeInstanceById(Long sceneGameInstanceId);
    
    /**
     * selectPrizeInstanceCountByPrizeId:查询已抽奖优惠券的数目. <br/>
     * Date: 2016年1月25日 下午8:28:51 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param prizeId
     * @return
     */
    int selectPrizeInstanceCountByPrizeId(Long prizeId);
    
}