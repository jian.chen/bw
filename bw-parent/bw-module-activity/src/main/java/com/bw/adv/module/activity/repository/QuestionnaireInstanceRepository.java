package com.bw.adv.module.activity.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.QuestionnaireInstanceMapper;
import com.bw.adv.module.activity.model.QuestionnaireInstance;

public interface QuestionnaireInstanceRepository extends BaseRepository<QuestionnaireInstance, QuestionnaireInstanceMapper>{

}
