package com.bw.adv.module.activity.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.PhotoScoreResultMapper;
import com.bw.adv.module.activity.model.PhotoScoreResult;
import com.bw.adv.module.activity.repository.PhotoScoreResultRepository;

@Repository
public class PhotoScoreResultRepositoryImpl extends BaseRepositoryImpl<PhotoScoreResult, PhotoScoreResultMapper> implements PhotoScoreResultRepository{

	@Override
	protected Class<PhotoScoreResultMapper> getMapperClass() {
		return PhotoScoreResultMapper.class;
	}

}
