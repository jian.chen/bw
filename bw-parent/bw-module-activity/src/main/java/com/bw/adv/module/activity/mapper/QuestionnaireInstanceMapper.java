package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.QuestionnaireInstance;

public interface QuestionnaireInstanceMapper extends BaseMapper<QuestionnaireInstance>{
    int deleteByPrimaryKey(Long instanceId);

    int insert(QuestionnaireInstance record);

    int insertSelective(QuestionnaireInstance record);

    QuestionnaireInstance selectByPrimaryKey(Long instanceId);

    int updateByPrimaryKeySelective(QuestionnaireInstance record);

    int updateByPrimaryKey(QuestionnaireInstance record);
}