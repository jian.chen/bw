package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.QuestionnaireMapper;
import com.bw.adv.module.activity.model.Questionnaire;

public interface QuestionnaireRepository extends BaseRepository<Questionnaire, QuestionnaireMapper>{
	
	/**
     * 查询所有问卷包含题目数量
     * @return
     */
    public List<Questionnaire> selectAllQuestionWithTemplateCount(Page<Questionnaire> templatePage);
    
    /**
	 * 查询启用问卷
	 * findActiveQuestion:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public Questionnaire findActiveQuestion();
	
	/**
	 * 查询启用问卷数量
	 * queryActiveQuestionAmount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public Long queryActiveQuestionAmount();
	
}
