/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.ActivityInstanceRecordMapper;
import com.bw.adv.module.activity.model.ActivityInstanceRecord;
import com.bw.adv.module.activity.model.exp.ActivityInstanceRecordExp;
import com.bw.adv.module.activity.repository.ActivityInstanceRecordRepository;
import com.bw.adv.module.common.model.Example;

@Repository
public class ActivityInstanceRecordRepositoryImpl extends BaseRepositoryImpl<ActivityInstanceRecord, ActivityInstanceRecordMapper> implements ActivityInstanceRecordRepository{

	@Override
	protected Class<ActivityInstanceRecordMapper> getMapperClass() {
		return ActivityInstanceRecordMapper.class;
	}
	
	@Override
	public List<ActivityInstanceRecordExp> findByActivityInstanceId(Long activityInstanceId, Page<ActivityInstanceRecordExp> page) {
		return this.getMapper().selectByActivityInstanceId(activityInstanceId,page);
	}

	@Override
	public List<ActivityInstanceRecordExp> findByExample(Example example,
			Page<ActivityInstanceRecordExp> page) {
		
		return this.getMapper().selectByExample(example, page);
	}
	
}

