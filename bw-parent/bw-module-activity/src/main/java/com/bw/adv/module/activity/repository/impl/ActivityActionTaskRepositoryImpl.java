/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.ActivityActionTaskMapper;
import com.bw.adv.module.activity.model.ActivityActionTask;
import com.bw.adv.module.activity.repository.ActivityActionTaskRepository;

/**
 * ClassName:活动动作 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ActivityActionTaskRepositoryImpl extends BaseRepositoryImpl<ActivityActionTask, ActivityActionTaskMapper> implements ActivityActionTaskRepository{

	@Override
	protected Class<ActivityActionTaskMapper> getMapperClass() {
		return ActivityActionTaskMapper.class;
	}
	
	/**
	 * TODO 查询所有未计算的活动动作，并根据发生时间正序排序（可选）.
	 * @see com.bw.adv.module.activity.repository.ActivityActionRepository#findByIsNotCountWithOrderByHappenTime()
	 */
	@Override
	public List<ActivityActionTask> findByIsNotCountWithOrderByHappenTime(int size) {
		return this.getMapper().selectByIsNotCountWithOrderByHappenTime(size);
	}


}

