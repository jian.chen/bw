package com.bw.adv.module.activity.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.QuestionnaireInstanceMapper;
import com.bw.adv.module.activity.model.QuestionnaireInstance;
import com.bw.adv.module.activity.repository.QuestionnaireInstanceRepository;

@Repository
public class QuestionnaireInstanceRepositoryImpl extends BaseRepositoryImpl<QuestionnaireInstance, QuestionnaireInstanceMapper> implements QuestionnaireInstanceRepository{

	@Override
	protected Class<QuestionnaireInstanceMapper> getMapperClass() {
		return QuestionnaireInstanceMapper.class;
	}

}
