package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.QuestionnaireTemplateMapper;
import com.bw.adv.module.activity.model.QuestionnaireTemplate;
import com.bw.adv.module.activity.repository.QuestionnaireTemplateRepository;

@Repository
public class QuestionnaireTemplateRepositoryImpl extends BaseRepositoryImpl<QuestionnaireTemplate, QuestionnaireTemplateMapper> implements QuestionnaireTemplateRepository{

	@Override
	public List<QuestionnaireTemplate> selectByQuestionnaireId(Long questionnaireId,Page<QuestionnaireTemplate> templatePage) {
		return this.getMapper().selectByQuestionnaireId(questionnaireId,templatePage);
	}

	@Override
	protected Class<QuestionnaireTemplateMapper> getMapperClass() {
		return QuestionnaireTemplateMapper.class;
	}

	@Override
	public List<QuestionnaireTemplate> queryQuestionTemplateNoPage(Long questionnaireId) {
		return this.getMapper().selectByQuestionnaireIdNoPage(questionnaireId);
	}

}
