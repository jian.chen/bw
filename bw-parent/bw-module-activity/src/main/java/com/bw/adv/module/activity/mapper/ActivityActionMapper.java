package com.bw.adv.module.activity.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.ActivityAction;

public interface ActivityActionMapper extends BaseMapper<ActivityAction> {
	
	
	/**
	 * selectByIsNotCountWithOrderByHappenTime:(查询所有未计算的活动动作，并根据发生时间正序排序). <br/>
	 * Date: 2015-8-29 上午10:54:56 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<ActivityAction> selectByIsNotCountWithOrderByHappenTime(int size);
	
	/**
	 * selectIsNotCountByActivityCode:(根据活动编码查询活动动作). <br/>
	 * Date: 2015-9-13 下午4:04:56 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityCode
	 * @return
	 */
	List<ActivityAction> selectIsNotCountByActivityCode(String activityCode);
	
	/**
	 * selectIsNotCountByBusinessCode:(根据业务编码查询活动动作). <br/>
	 * Date: 2015-9-13 下午4:04:33 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param businessCode
	 * @return
	 */
	List<ActivityAction> selectIsNotCountByBusinessCode(String businessCode);
	
}