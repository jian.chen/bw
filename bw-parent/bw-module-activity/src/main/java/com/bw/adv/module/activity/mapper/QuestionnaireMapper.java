package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.Questionnaire;

public interface QuestionnaireMapper extends BaseMapper<Questionnaire> {
    int deleteByPrimaryKey(Long questionnaireId);

    int insert(Questionnaire record);

    int insertSelective(Questionnaire record);

    Questionnaire selectByPrimaryKey(Long questionnaireId);

    int updateByPrimaryKeySelective(Questionnaire record);

    int updateByPrimaryKey(Questionnaire record);
    
    /**
     * 查询所有问卷包含题目数量
     * @return
     */
    @Select("SELECT q.QUESTIONNAIRE_ID as questionnaireId,q.QUESTION_NAME as questionName,q.EXPLAINS as explains,q.END_EXPLAIN as endExplain,"
    		+ "q.CREATE_USER as createUser,q.STATUS_ID as statusId,q.CREATE_TIME as createTime,q.START_DATE as startDate,q.END_DATE as endDate,"
    		+ "q.UPDATE_TIME as updateTime,count(qt.QUESTIONNAIRE_TEMPLATE_ID) as templateCount from questionnaire q "
    		+ "left join questionnaire_template qt on q.QUESTIONNAIRE_ID = qt.QUESTIONNAIRE_ID "
    		+ "GROUP BY q.QUESTIONNAIRE_ID "
    		+ "ORDER BY q.CREATE_TIME desc")
    List<Questionnaire> selectAllQuestionWithTemplateCount(Page<Questionnaire> templatePage);
    
    /**
	 * 查询启用问卷
	 * findActiveQuestion:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	Questionnaire findActiveQuestion();
	
	/**
	 * 查询启用问卷数量
	 * queryActiveQuestionAmount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	Long queryActiveQuestionAmount();
    
}