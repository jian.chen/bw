package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.InviteMemberRecord;

import java.util.List;

public interface InviteMemberRecordMapper extends BaseMapper<InviteMemberRecord>{
    int deleteByPrimaryKey(Long inviteMemberRecordId);

    int insert(InviteMemberRecord record);

    int insertSelective(InviteMemberRecord record);

    InviteMemberRecord selectByPrimaryKey(Long inviteMemberRecordId);

    int updateByPrimaryKeySelective(InviteMemberRecord record);

    int updateByPrimaryKey(InviteMemberRecord record);

    List<InviteMemberRecord> getAllValidInviteRecordOfToday(String today);
}