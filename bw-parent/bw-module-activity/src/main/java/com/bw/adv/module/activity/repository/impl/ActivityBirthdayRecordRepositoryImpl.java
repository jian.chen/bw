package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.ActivityBirthdayRecordMapper;
import com.bw.adv.module.activity.model.ActivityBirthdayRecord;
import com.bw.adv.module.activity.repository.ActivityBirthdayRecordRepository;

@Repository
public class ActivityBirthdayRecordRepositoryImpl extends BaseRepositoryImpl<ActivityBirthdayRecord,ActivityBirthdayRecordMapper> implements ActivityBirthdayRecordRepository{

	@Override
	protected Class<ActivityBirthdayRecordMapper> getMapperClass() {
		return ActivityBirthdayRecordMapper.class;
	}

	@Override
	public List<ActivityBirthdayRecord> findRecordByIdAndMember(Long memberId, Long activityInstanceId,
			String createYear) {
		return this.getMapper().findRecordByIdAndMember(memberId, activityInstanceId, createYear);
	}

}
