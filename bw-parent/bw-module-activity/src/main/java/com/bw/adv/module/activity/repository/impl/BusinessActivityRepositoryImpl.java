/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.BusinessActivityMapper;
import com.bw.adv.module.activity.model.BusinessActivity;
import com.bw.adv.module.activity.model.exp.BusinessActivityExp;
import com.bw.adv.module.activity.repository.BusinessActivityRepository;

/**
 * ClassName:活动 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class BusinessActivityRepositoryImpl extends BaseRepositoryImpl<BusinessActivity, BusinessActivityMapper> implements BusinessActivityRepository{

	@Override
	protected Class<BusinessActivityMapper> getMapperClass() {
		return BusinessActivityMapper.class;
	}


	/**
	 * TODO 查询所有业务活动（可选）.
	 * @see com.bw.adv.module.activity.repository.BusinessActivityRepository#findAllExp()
	 */
	@Override
	public List<BusinessActivityExp> findAllExp() {
		return this.getMapper().selectAllExp();
	}
	
	

}

