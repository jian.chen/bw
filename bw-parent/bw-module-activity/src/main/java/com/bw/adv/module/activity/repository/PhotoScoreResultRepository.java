package com.bw.adv.module.activity.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.PhotoScoreResultMapper;
import com.bw.adv.module.activity.model.PhotoScoreResult;

public interface PhotoScoreResultRepository extends BaseRepository<PhotoScoreResult, PhotoScoreResultMapper>{

}
