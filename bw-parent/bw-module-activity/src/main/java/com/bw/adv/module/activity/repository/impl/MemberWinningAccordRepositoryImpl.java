/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:MemberWinningAccordRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015年11月23日下午2:14:32
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.MemberWinningAccordMapper;
import com.bw.adv.module.activity.model.MemberWinningAccord;
import com.bw.adv.module.activity.repository.MemberWinningAccordRepository;

/**
 * ClassName:MemberWinningAccordRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:14:32 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberWinningAccordRepositoryImpl extends BaseRepositoryImpl<MemberWinningAccord, MemberWinningAccordMapper> implements MemberWinningAccordRepository {
	

	@Override
	protected Class<MemberWinningAccordMapper> getMapperClass() {
		return MemberWinningAccordMapper.class;
	}

	@Override
	public int findMemberSurplusNumber(Long memberId, Long activityId,Long qrCodeId) {
		return this.getMapper().queryMemberSurplusNumber(memberId, activityId,qrCodeId);
	}
	
	
	@Override
	public void updateOneByMember(Long memberId, Long activityId,Long qrCodeId) {
		
		this.getMapper().updateOneByMember(memberId, activityId,qrCodeId);
	}

	@Override
	public MemberWinningAccord queryResultByMemberIdAndActivityId(Long memberId, Long activityId,Long qrCodeId) {
		return this.getMapper().queryResultByMemberIdAndActivityId(memberId, activityId,qrCodeId);
	}

	@Override
	public int updateMemberAccordWithVersion(Long memberWinningAccordId,Long qrCodeId, Long version, String useTime) {
		return this.getMapper().updateMemberAccordWithVersion(memberWinningAccordId,qrCodeId, version, useTime);
	}

	
	/**
     * selectAccordSomeDay:(查询会员某一天的抽奖资格:包含使用和未使用). <br/>
     * Date: 2016-6-15 下午4:38:20 <br/>
     * scrmVersion standard
     * @author mennan
     * @version jdk1.7
     * @param memberId
     * @param activityId
     * @return
     */
	public List<MemberWinningAccord> findAccordListCurrentDate(Long memberId, Long activityId,Long qrCodeId,String someDay,int accordType){
		return this.getMapper().selectAccordListSomeDay(memberId, activityId,qrCodeId, someDay,accordType);
    }

	@Override
	public int findAccordSizeCurrentDate(Long memberId, Long activityId,Long qrCodeId,String someDay,int accordType) {
		return this.getMapper().selectAccordSizeSomeDay(memberId, activityId,qrCodeId, someDay ,accordType);
	}

	@Override
	public Long findMemberSurplusNumberToday(Long memberId, Long activityId,Long qrCodeId, String day) {
		return this.getMapper().queryMemberSurplusNumberToday(memberId, activityId,qrCodeId, day);
	}

	@Override
	public Long queryMemberUseChance(Long memberId, Long activityId,Long qrCodeId, String someDay) {
		return this.getMapper().queryMemberUseChance(memberId, activityId,qrCodeId, someDay);
	}

	@Override
	public MemberWinningAccord queryResultToday(Long memberId, Long activityId,Long qrCodeId,String someDay) {
		return this.getMapper().queryResultToday(memberId, activityId,qrCodeId, someDay);
	}

	@Override
	public MemberWinningAccord queryMemberMapsQrCode(Long memberId,Long activityId,String qrCode,String isUse) {
		return this.getMapper().selectMemberMapsQrCode(memberId,activityId,qrCode,isUse);
	}

	@Override
	public int updateMemberAccordMapsVersion(Long memberWinningAccordId,String qrCode, String useTime) {
		return this.getMapper().updateMemberAccordMapsVersion(memberWinningAccordId,qrCode,useTime);
	}

}

