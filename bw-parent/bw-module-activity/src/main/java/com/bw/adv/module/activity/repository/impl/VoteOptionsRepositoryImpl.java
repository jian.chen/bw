package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.VoteOptionsMapper;
import com.bw.adv.module.activity.model.VoteOptions;
import com.bw.adv.module.activity.repository.VoteOptionsRepository;

@Repository
public class VoteOptionsRepositoryImpl extends BaseRepositoryImpl<VoteOptions, VoteOptionsMapper> implements VoteOptionsRepository{

	@Override
	protected Class<VoteOptionsMapper> getMapperClass() {
		return VoteOptionsMapper.class;
	}

	@Override
	public List<VoteOptions> findVoteOptionsByTitleId(Long voteId, Page<VoteOptions> templatePage) {
		return this.getMapper().findVoteOptionsByTitleId(voteId, templatePage);
	}

}
