package com.bw.adv.module.activity.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.BusinessShowPageMapper;
import com.bw.adv.module.activity.model.BusinessShowPage;
import com.bw.adv.module.activity.repository.BusinessShowPageRepository;

@Repository
public class BusinessShowPageRepositoryImpl extends BaseRepositoryImpl<BusinessShowPage, BusinessShowPageMapper> implements BusinessShowPageRepository{

	@Override
	protected Class<BusinessShowPageMapper> getMapperClass() {
		return BusinessShowPageMapper.class;
	}

	
}