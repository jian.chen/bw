package com.bw.adv.module.activity.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.BusinessShowPageMapper;
import com.bw.adv.module.activity.model.BusinessShowPage;

public interface BusinessShowPageRepository extends BaseRepository<BusinessShowPage, BusinessShowPageMapper>{
	
}