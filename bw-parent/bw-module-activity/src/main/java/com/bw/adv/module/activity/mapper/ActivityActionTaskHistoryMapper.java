package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.ActivityActionTaskHistory;

public interface ActivityActionTaskHistoryMapper extends BaseMapper<ActivityActionTaskHistory>{
	
}