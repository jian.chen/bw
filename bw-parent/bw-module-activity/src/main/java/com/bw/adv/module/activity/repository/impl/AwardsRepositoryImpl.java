/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:AwardsRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015年11月20日下午3:21:02
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.AwardsMapper;
import com.bw.adv.module.activity.model.Awards;
import com.bw.adv.module.activity.model.exp.AwardsExp;
import com.bw.adv.module.activity.repository.AwardsRepository;

/**
 * ClassName:AwardsRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:21:02 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class AwardsRepositoryImpl extends BaseRepositoryImpl<Awards, AwardsMapper> implements AwardsRepository {
	
	@Override
	protected Class<AwardsMapper> getMapperClass() {
		return AwardsMapper.class;
	}

	@Override
	public List<AwardsExp> findAwardsBySceneGameInstanceId(Long sceneGameInstanceId) {
		return this.getMapper().selectAwardsBySceneGameInstanceId(sceneGameInstanceId);
	}

	@Override
	public int removeAwards(Long sceneGameInstanceId) {
		return this.getMapper().deleteAwards(sceneGameInstanceId);
	}

	@Override
	public AwardsExp findLuckyAwards(Long sceneGameInstanceId) {
		return this.getMapper().selectLuckyAwards(sceneGameInstanceId);
	}

	@Override
	public AwardsExp findAwardsByAwardsId(Long awardsId) {
		return this.getMapper().selectAwardsByAwardsId(awardsId);
	}

	/**
	 * TODO 根据游戏编号查询奖项列表
	 * @see com.bw.adv.module.activity.repository.AwardsRepository#findByGameCode(java.lang.String)
	 */
	@Override
	public List<Awards> findByGameCode(String gameCode) {
		return this.getMapper().selectByGameCode(gameCode);
	}
	
}

