package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.VoteOptionsMapper;
import com.bw.adv.module.activity.model.VoteOptions;

public interface VoteOptionsRepository extends BaseRepository<VoteOptions, VoteOptionsMapper>{
	
	/**
	 * 根据投票id查询选项
	 * @param voteId
	 * @param templatePage
	 * @return
	 */
	public List<VoteOptions> findVoteOptionsByTitleId(Long voteId,Page<VoteOptions> templatePage);

}
