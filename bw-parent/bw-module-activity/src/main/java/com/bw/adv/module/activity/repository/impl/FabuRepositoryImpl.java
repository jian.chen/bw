package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.FabuChannelMapper;
import com.bw.adv.module.activity.model.FabuChannel;
import com.bw.adv.module.activity.repository.FabuRepository;

@Repository
public class FabuRepositoryImpl extends BaseRepositoryImpl<FabuChannel, FabuChannelMapper> implements FabuRepository{

	@Override
	protected Class<FabuChannelMapper> getMapperClass() {
		return FabuChannelMapper.class;
	}

	@Override
	public List<FabuChannel> queryAllGroupByChannel() {
		return this.getMapper().queryAllGroupByChannel();
	}

}
