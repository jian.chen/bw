package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.VoteTitle;

public interface VoteTitleMapper extends BaseMapper<VoteTitle>{
    int deleteByPrimaryKey(Long titleId);

    int insert(VoteTitle record);

    int insertSelective(VoteTitle record);

    VoteTitle selectByPrimaryKey(Long titleId);

    int updateByPrimaryKeySelective(VoteTitle record);

    int updateByPrimaryKey(VoteTitle record);
    
    /**
	 * 查询所有的投票并且带选项数量
	 * @return
	 */
    @Select("SELECT vt.TITLE_ID as titleId,vt.VOTE_NAME as voteName,vt.VOTE_DETAIL as voteDetail,"
    		+ "vt.START_DATE as startDate,vt.status_id as statusId, "
    		+ "vt.END_DATE as endDate,COUNT(vo.OPTIONS_ID) as optionCount "
    		+ "from vote_title vt "
    		+ "LEFT JOIN vote_options vo on vt.TITLE_ID = vo.TITLE_ID "
    		+ "GROUP BY vt.TITLE_ID "
    		+ "ORDER BY vt.CREATE_DATE")
	List<VoteTitle> findAllVoteAndOptionCount(Page<VoteTitle> templatePage);
    
    /**
	 *	查询激活的投票数量
	 * queryActiveVoteAmount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public Long queryActiveVoteAmount();
	
	/**
	 * 查询激活的投票
	 * queryActiveVote:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public VoteTitle queryActiveVote();
	
}