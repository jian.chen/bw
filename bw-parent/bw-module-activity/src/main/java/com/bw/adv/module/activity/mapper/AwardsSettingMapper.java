package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.AwardsSetting;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;

public interface AwardsSettingMapper extends BaseMapper<AwardsSetting>{
    /**
     * 根据活动id查询出对应的奖项设置 
     * selectAwardsSettingsByActivityId:(). <br/>
     * Date: 2015年11月24日 下午4:40:30 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param activityId
     * @return
     */
    List<AwardsSetting> selectAwardsSettingsByActivityId(@Param("activityId")Long activityId);
    
    /**
     * selectAwardsSettingsExpByActivityId:(这). <br/>
     * Date: 2015年11月26日 下午9:14:15 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param activityId
     * @return
     */
	List<AwardsSettingExp> selectAwardsSettingsExpByActivityId(@Param("activityId")Long activityId);
	/**
	 * selectAwardsSettingsExpByActivityId:(这). <br/>
	 * Date: 2015年11月26日 下午9:14:15 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 */
	AwardsSettingExp selectAwardsSettingsExpByAwardsId(@Param("awardsId")Long awardsId);
	
	/**
	 * deleteAwardsSettings:根据活动实例ID删除奖项设置 <br/>
	 * Date: 2016年1月19日 下午4:31:35 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	int deleteAwardsSettings(Long sceneGameInstanceId);
}