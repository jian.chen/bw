package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.PhotoScoreResult;

public interface PhotoScoreResultMapper extends BaseMapper<PhotoScoreResult>{
    int deleteByPrimaryKey(Long id);

    int insert(PhotoScoreResult record);

    int insertSelective(PhotoScoreResult record);

    PhotoScoreResult selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PhotoScoreResult record);

    int updateByPrimaryKey(PhotoScoreResult record);
}