package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.QuestionnaireTemplateInfoMapper;
import com.bw.adv.module.activity.model.QuestionCount;
import com.bw.adv.module.activity.model.QuestionnaireTemplateInfo;
import com.bw.adv.module.activity.repository.QuestionnaireTemplateInfoRepository;

@Repository
public class QuestionnaireTemplateInfoRepositoryImpl extends BaseRepositoryImpl<QuestionnaireTemplateInfo, QuestionnaireTemplateInfoMapper> implements QuestionnaireTemplateInfoRepository{

	@Override
	protected Class<QuestionnaireTemplateInfoMapper> getMapperClass() {
		return QuestionnaireTemplateInfoMapper.class;
	}

	@Override
	public List<QuestionnaireTemplateInfo> queryTemplateInfoByTemplateId(Long templateId) {
		return this.getMapper().queryTemplateInfoByTemplateId(templateId);
	}

	@Override
	public void deleteTemplateInfoByTemplateId(Long templateId) {
		this.getMapper().deleteTemplateInfoByTemplateId(templateId);
	}

	@Override
	public List<QuestionCount> selectResultCountForSelect(Long questionnaireId) {
		return this.getMapper().queryResultCountForSelect(questionnaireId);
	}

	@Override
	public List<QuestionCount> selectResultCountForMark(Long questionnaireId) {
		return this.getMapper().queryResultCountForMark(questionnaireId);
	}

	@Override
	public List<QuestionCount> selectResultCountForQa(Long questionnaireId) {
		return this.getMapper().queryResultCountForQa(questionnaireId);
	}

	@Override
	public Long selectCountInfoForMember(Long questionnaireId) {
		return this.getMapper().queryCountInfoForMember(questionnaireId);
	}

}
