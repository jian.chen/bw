/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.ActivityActionTaskMapper;
import com.bw.adv.module.activity.model.ActivityActionTask;

/**
 * ClassName:活动动作动作 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface ActivityActionTaskRepository extends BaseRepository<ActivityActionTask, ActivityActionTaskMapper>{
	
	/**
	 * findByIsNotCountWithOrderByHappenTime:(查询所有未计算的活动任务动作，并根据发生时间正序排序). <br/>
	 * Date: 2015-8-29 上午10:56:58 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<ActivityActionTask> findByIsNotCountWithOrderByHappenTime(int size);
	
}

