package com.bw.adv.module.activity.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.BusinessActivity;
import com.bw.adv.module.activity.model.exp.BusinessActivityExp;

public interface BusinessActivityMapper extends BaseMapper<BusinessActivity>{
	
	/**
	 * selectAllExp:(查询所有业务活动). <br/>
	 * Date: 2015-9-5 下午9:38:03 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<BusinessActivityExp> selectAllExp();
	
}