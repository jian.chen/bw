package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.ActivityBirthdayRecord;

public interface ActivityBirthdayRecordMapper extends BaseMapper<ActivityBirthdayRecord>{
	
	/**
	 * 根据会员id，活动实例id，创建年份查找记录
	 * @param memberId
	 * @param activityInstanceId
	 * @param createYear
	 * @return
	 */
	List<ActivityBirthdayRecord> findRecordByIdAndMember(@Param("memberId")Long memberId,@Param("activityInstanceId")Long activityInstanceId,@Param("createYear")String createYear);
	
}