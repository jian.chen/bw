package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.VoteResultMapper;
import com.bw.adv.module.activity.model.VoteResult;

public interface VoteResultRepository extends BaseRepository<VoteResult, VoteResultMapper>{
	
	/**
	 * 查询投票回答人数
	 * @param titleId
	 * @return
	 */
	public Long selectCountForVoteResult(Long titleId);
	
	/**
	 * 查询投票的调查结果
	 * @param titleId
	 * @return
	 */
	public List<VoteResult> findVoteResultByTitleId(Long titleId);

}
