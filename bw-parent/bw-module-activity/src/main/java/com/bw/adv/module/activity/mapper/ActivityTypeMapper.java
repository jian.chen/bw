package com.bw.adv.module.activity.mapper;

import com.bw.adv.module.activity.model.ActivityType;

public interface ActivityTypeMapper {
    int deleteByPrimaryKey(Long activityTypeId);

    int insert(ActivityType record);

    int insertSelective(ActivityType record);

    ActivityType selectByPrimaryKey(Long activityTypeId);

    int updateByPrimaryKeySelective(ActivityType record);

    int updateByPrimaryKey(ActivityType record);
}