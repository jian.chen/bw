/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:PrizeRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2015年11月20日下午3:10:27
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.PrizeMapper;
import com.bw.adv.module.activity.model.Prize;

/**
 * ClassName:PrizeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:10:27 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface PrizeRepository extends BaseRepository<Prize, PrizeMapper> {
	
	/**
	 * findPrizeByAwardId:(根据awarId查询Prize). <br/>
	 * Date: 2015年12月15日 下午5:45:26 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param awardsId
	 * @return
	 */
	Prize findPrizeByAwardId(Long awardsId);
	
	/**
	 * removePrize:删除活动实例对应的奖品. <br/>
	 * Date: 2016年1月19日 下午4:26:11 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	int removePrize(Long sceneGameInstanceId);

}

