/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:AwardsRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2015年11月20日下午3:03:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.AwardsMapper;
import com.bw.adv.module.activity.model.Awards;
import com.bw.adv.module.activity.model.exp.AwardsExp;

/**
 * ClassName:AwardsRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:03:19 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface AwardsRepository extends BaseRepository<Awards, AwardsMapper> {
	
	/**
	 * findAwardsBySceneGameInstanceId:查询活动的奖项明细. <br/>
	 * Date: 2016年1月19日 上午11:31:03 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	 List<AwardsExp> findAwardsBySceneGameInstanceId(Long sceneGameInstanceId);
	 
	 /**
	  * removeAwards:删除活动实例对应的奖项. <br/>
	  * Date: 2016年1月19日 下午4:41:40 <br/>
	  * scrmVersion 1.0
	  * @author Tab.Wang
	  * @version jdk1.7
	  * @param sceneGameInstanceId
	  * @return
	  */
	 int removeAwards(Long sceneGameInstanceId);
	 
	 /**
	  * findLuckyAwards:查询活动的谢谢参与奖项. <br/>
	  * Date: 2016年1月22日 下午1:34:56 <br/>
	  * scrmVersion 1.0
	  * @author Tab.Wang
	  * @version jdk1.7
	  * @param sceneGameInstanceId
	  * @return
	  */
	 AwardsExp findLuckyAwards(Long sceneGameInstanceId);
	 
	 /**
	  * findAwardsByAwardsId:根据奖项ID,查询奖项信息. <br/>
	  * TODO(这里描述这个方法适用条件 – 可选).<br/>
	  * Date: 2016年3月1日 下午5:13:36 <br/>
	  * scrmVersion 1.0
	  * @author Tab.Wang(*^_^*)
	  * @version jdk1.7
	  * @param awardsId
	  * @return
	  */
	 AwardsExp findAwardsByAwardsId(Long awardsId);

	/**
	 * findByGameCode:(根据游戏编号查询奖项列表，只查询有效的游戏:已保存，已激活). <br/>
	 * Date: 2016-3-31 下午6:27:12 <br/>
	 * scrmVersion standard
	 * @author mennan
	 * @version jdk1.7
	 * @param gameCode
	 * @return
	 */
	List<Awards> findByGameCode(String gameCode);
	 
}

