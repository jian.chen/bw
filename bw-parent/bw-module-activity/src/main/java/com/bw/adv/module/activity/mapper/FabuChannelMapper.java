package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.bw.adv.module.activity.model.FabuChannel;


public interface FabuChannelMapper {
    int deleteByPrimaryKey(Long id);

    int insert(FabuChannel record);

    int insertSelective(FabuChannel record);

    FabuChannel selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(FabuChannel record);

    int updateByPrimaryKey(FabuChannel record);
    
    @Select("select nick_name as nickName,channel from fabu_channel")
    public List<FabuChannel> queryAllGroupByChannel();
}