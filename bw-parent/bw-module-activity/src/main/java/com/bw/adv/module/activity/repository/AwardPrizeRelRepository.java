/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:AwardPrizeRelRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2015年11月20日下午3:01:08
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.AwardPrizeRelMapper;
import com.bw.adv.module.activity.model.AwardPrizeRel;

/**
 * ClassName:AwardPrizeRelRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:01:08 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface AwardPrizeRelRepository extends BaseRepository<AwardPrizeRel, AwardPrizeRelMapper> {
	
	/**
	 * removeRelBySceneGameInstanceId:根据活动实例ID删除所有的奖项奖品关系. <br/>
	 * Date: 2016年1月19日 下午3:59:48 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	int removeRelBySceneGameInstanceId(long sceneGameInstanceId);

}

