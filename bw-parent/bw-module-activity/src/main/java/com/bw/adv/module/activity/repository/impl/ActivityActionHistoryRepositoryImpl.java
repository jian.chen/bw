/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.ActivityActionHistoryMapper;
import com.bw.adv.module.activity.model.ActivityActionHistory;
import com.bw.adv.module.activity.repository.ActivityActionHistoryRepository;

/**
 * ClassName:活动动作历史 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ActivityActionHistoryRepositoryImpl extends BaseRepositoryImpl<ActivityActionHistory, ActivityActionHistoryMapper> implements ActivityActionHistoryRepository{

	@Override
	protected Class<ActivityActionHistoryMapper> getMapperClass() {
		return ActivityActionHistoryMapper.class;
	}

}

