package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.ShakeUserInfoMapper;
import com.bw.adv.module.activity.model.ShakeUserInfo;
import com.bw.adv.module.activity.model.UserAndWinning;
import com.bw.adv.module.activity.repository.ShakeUserInfoRepository;

@Repository
public class ShakeUserInfoRepositoryImpl extends BaseRepositoryImpl<ShakeUserInfo, ShakeUserInfoMapper> implements ShakeUserInfoRepository {

	@Override
	protected Class<ShakeUserInfoMapper> getMapperClass() {
		return ShakeUserInfoMapper.class;
	}

	@Override
	public ShakeUserInfo findShakeUserInfoByPhoneNum(String phoneNum) {
		return this.getMapper().selectShakeUserInfoByPhoneNum(phoneNum);
	}

	@Override
	public List<UserAndWinning> selectAllForUserAndWinning() {
		return this.getMapper().selectAllForUserAndWinning();
	}

}
