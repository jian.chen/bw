/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:SceneGameInstanceRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2016年1月15日下午2:25:13
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.SceneGameInstanceMapper;
import com.bw.adv.module.activity.model.SceneGameInstance;
import com.bw.adv.module.activity.model.exp.SceneGameInstanceExp;
import com.bw.adv.module.activity.repository.SceneGameInstanceRepository;

/**
 * ClassName:SceneGameInstanceRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午2:25:13 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SceneGameInstanceRepositoryImpl extends BaseRepositoryImpl<SceneGameInstance, SceneGameInstanceMapper> implements SceneGameInstanceRepository {

	@Override
	protected Class<SceneGameInstanceMapper> getMapperClass() {
		return SceneGameInstanceMapper.class;
	}

	@Override
	public List<SceneGameInstance> findSceneGameInstanceByGameId(Long sceneGameId,Page<SceneGameInstance> page) {
		return this.getMapper().selectSceneGameInstanceByGameId(sceneGameId,page);
	}

	@Override
	public int updateSceneGameInstanceStatus(SceneGameInstance sceneGameInstance) {
		return this.getMapper().updateSceneGameInstanceStatus(sceneGameInstance);
	}

	@Override
	public int countOpenSceneGameInstance(Long sceneGameId) {
		return this.getMapper().countOpenSceneGameInstance(sceneGameId);
	}

	@Override
	public int updateStatusOutDate() {
		return this.getMapper().updateStatusOutDate();
	}

	@Override
	public SceneGameInstance findValidInstanceById(Long sceneGameInstanceId) {
		return this.getMapper().selectValidInstanceById(sceneGameInstanceId);
	}

	@Override
	public int findValidInstanceByStartTime(Long sceneGameInstanceId) {
		return this.getMapper().selectValidInstanceByStartTime(sceneGameInstanceId);
	}

	@Override
	public int findValidInstanceByEndTime(Long sceneGameInstanceId) {
		return this.getMapper().selectValidInstanceByEndTime(sceneGameInstanceId);
	}

	@Override
	public List<SceneGameInstanceExp> findSceneGameWinRate(
			String likeInstanceName, String greaterEqualCreateTime,
			String lessEqualCreateTime) {
		
		return this.getMapper().selectSceneGameWinRate(likeInstanceName,greaterEqualCreateTime,lessEqualCreateTime);
	}

	@Override
	public List<SceneGameInstanceExp> findSceneGameAwardsWinRate(
			Long sceneGameInstanceId) {
		
		return this.getMapper().selectSceneGameAwardsWinRate(sceneGameInstanceId);
	}
	@Override
	public List<SceneGameInstance> findValidInstanceByCode(String sceneGameInstanceCode) {
		return this.getMapper().selectValidInstanceByCode(sceneGameInstanceCode);
	}

	@Override
	public SceneGameInstance findValidInstance(String sceneGameCode) {
		return this.getMapper().selectValidInstance(sceneGameCode);
	}

	/**
	 * TODO 查询场景游戏的实例列表
	 * @see com.bw.adv.module.activity.repository.SceneGameInstanceRepository#findBySceneGameId(java.lang.Long)
	 */
	@Override
	public List<SceneGameInstance> findBySceneGameId(Long sceneGameId) {
		return this.getMapper().selectBySceneGameId(sceneGameId);
	}


}

