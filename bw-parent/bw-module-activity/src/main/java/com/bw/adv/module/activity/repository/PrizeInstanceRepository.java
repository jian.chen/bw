/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:PrizeInstanceRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2015年11月20日下午3:09:44
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.PrizeInstanceMapper;
import com.bw.adv.module.activity.model.PrizeInstance;
import com.bw.adv.module.activity.model.exp.PrizeInstanceExp;

/**
 * ClassName:PrizeInstanceRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:09:44 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface PrizeInstanceRepository extends BaseRepository<PrizeInstance, PrizeInstanceMapper> {
	
	/**
	 * 
	 * queryPrizeInstanceExpByAwardsId:(根据所中奖项抽取奖品实例). <br/>
	 * Date: 2015年11月26日 上午11:02:52 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param awardsId
	 * @return
	 */
	PrizeInstanceExp queryPrizeInstanceExpByAwardsId(Long awardsId);

	/**
	 * batchaddPrizeInstance:批量插入实物实例. <br/>
	 * Date: 2016年1月19日 下午3:32:25 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param prizeInstanceList
	 * @return
	 */
	int batchaddPrizeInstance(List<PrizeInstance> prizeInstanceList);
	
	/**
	 * removePrizeInstanceById:删除活动实例对应的奖品实例. <br/>
	 * Date: 2016年1月19日 下午4:16:57 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	int removePrizeInstanceById(Long sceneGameInstanceId);
	
	/**
	 * findPrizeInstanceCountByPrizeId:查询已抽奖优惠券的数目. <br/>
	 * Date: 2016年1月25日 下午8:32:01 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param prizeId
	 * @return
	 */
	int findPrizeInstanceCountByPrizeId(Long prizeId);
	
}

