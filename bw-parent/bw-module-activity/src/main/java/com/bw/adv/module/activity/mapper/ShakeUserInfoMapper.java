package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.ShakeUserInfo;
import com.bw.adv.module.activity.model.UserAndWinning;

public interface ShakeUserInfoMapper extends BaseMapper<ShakeUserInfo>{
    int deleteByPrimaryKey(Long userId);

    int insert(ShakeUserInfo record);

    int insertSelective(ShakeUserInfo record);

    ShakeUserInfo selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(ShakeUserInfo record);

    int updateByPrimaryKey(ShakeUserInfo record);
    
    /**
     * 根据手机号查询参与者信息
     * @param phoneNum
     * @return
     */
	ShakeUserInfo selectShakeUserInfoByPhoneNum(@Param("phoneNum")String phoneNum);
	
	@Select("SELECT AWARDS_ID as awardsId,USER_NAME as userName,MOBILE as mobile,EXT1 as ext1,EXT2 as ext2 "
			+ "from winning_item wi "
			+ "join shake_user_info sui on wi.MEMBER_ID = sui.USER_ID;")
	List<UserAndWinning> selectAllForUserAndWinning();
}