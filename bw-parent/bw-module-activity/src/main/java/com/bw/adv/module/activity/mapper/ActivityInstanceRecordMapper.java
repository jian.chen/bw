package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.ActivityInstanceRecord;
import com.bw.adv.module.activity.model.exp.ActivityInstanceRecordExp;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName: ActivityInstanceRecordMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-27 下午3:05:44 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface ActivityInstanceRecordMapper  extends BaseMapper<ActivityInstanceRecord>{
	
	/**
	 * selectByActivityInstanceId:(根据实例ID，活动实例记录). <br/>
	 * Date: 2015-8-27 下午3:06:26 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityInstanceId
	 * @param page 
	 * @return
	 */
	List<ActivityInstanceRecordExp> selectByActivityInstanceId(@Param("activityInstanceId")Long activityInstanceId, Page<ActivityInstanceRecordExp> page);
	
	/**
	 * 
	 * selectByExample:(根据example的值，查询活动实例记录). <br/>
	 * Date: 2015年9月30日 下午2:58:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<ActivityInstanceRecordExp> selectByExample(@Param("example")Example example, Page<ActivityInstanceRecordExp> page);
	
}