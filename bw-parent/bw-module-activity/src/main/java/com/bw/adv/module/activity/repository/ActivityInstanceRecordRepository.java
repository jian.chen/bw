/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.ActivityInstanceRecordMapper;
import com.bw.adv.module.activity.model.ActivityInstanceRecord;
import com.bw.adv.module.activity.model.exp.ActivityInstanceRecordExp;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName: ActivityInstanceRecordRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-27 下午3:07:31 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface ActivityInstanceRecordRepository extends BaseRepository<ActivityInstanceRecord, ActivityInstanceRecordMapper>{


	/**
	 * findByActivityInstanceId:(根据活动实例ID查询活动实例记录，分页). <br/>
	 * Date: 2015-8-27 下午3:12:25 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityInstanceId
	 * @param page
	 * @return
	 */
	List<ActivityInstanceRecordExp> findByActivityInstanceId(Long activityInstanceId,Page<ActivityInstanceRecordExp> page);
	
	/**
	 * 
	 * findByExample:(根据example查询活动实例记录，分页). <br/>
	 * Date: 2015年9月30日 下午3:00:47 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<ActivityInstanceRecordExp> findByExample(Example example,Page<ActivityInstanceRecordExp> page);
	
	
}

