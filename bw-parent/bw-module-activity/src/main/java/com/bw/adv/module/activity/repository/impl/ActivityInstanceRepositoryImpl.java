/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.ActivityInstanceMapper;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.model.exp.ActivityRuleConditionExp;
import com.bw.adv.module.activity.repository.ActivityInstanceRepository;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.tools.DateUtils;

/**
 * ClassName:活动 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ActivityInstanceRepositoryImpl extends BaseRepositoryImpl<ActivityInstance, ActivityInstanceMapper> implements ActivityInstanceRepository{
	
	private final static String SEND_TYPE_CURRENT_DATE = "1";
	private final static String SEND_TYPE_BEFORE_DATE = "2";
	private final static String SEND_TYPE_MONTH_BEGIN = "3";
	

	@Override
	protected Class<ActivityInstanceMapper> getMapperClass() {
		return ActivityInstanceMapper.class;
	}

	@Override
	public List<ActivityInstance> findByActivityId(Long activityId) {
		return this.getMapper().selectByActivityId(activityId);
	}

	@Override
	public List<ActivityInstance> findByActivityId(Long activityId,Page<ActivityInstance> page) {
		return this.getMapper().selectByActivityId(activityId,page);
	}

	@Override
	public List<ActivityRuleConditionExp> findActivityInstanceExpByInstanceId(Long instanceId) {
		return this.getMapper().selectExpByPrimaryKey(instanceId);
	}

	@Override
	public List<ActivityRuleConditionExp> findActivityConditionValue(String statusIds) {
		return this.getMapper().selectActivityConditionValue(statusIds);
	}
	
	@Override
	public List<ActivityInstance> findByStatusIds(String statusIds) {
		return this.getMapper().selectByStatusIds(statusIds);
	}
	
	@Override
	public List<Member> findActivityWake(String wakeDate) {
		// 时间格式:20150901201223
		if(!StringUtils.isBlank(wakeDate)){
			String diffDate = DateUtils.addDays(-Integer.parseInt(wakeDate));
//			wakeDate =diffDate+DateUtils.getCurrentTimeStr();
			wakeDate =diffDate;
		}
		return this.getMapper().selectActivityWake(wakeDate);
	}
	
	@Override
	public List<Member> findActivitySpecial(){
		return this.getMapper().selectActivitySpecial();
	}
	
	@Override
	public List<Member> findActivityBirthday(String sendDay) {
		String sendType = null;
		
		sendType = sendDay.split("/")[0];
		if(sendType.equals(SEND_TYPE_CURRENT_DATE)){
			sendDay = DateUtils.getCurrentDateOfDb().substring(4);
		}
		if(sendType.equals(SEND_TYPE_BEFORE_DATE)){
			sendDay = DateUtils.addDays(Integer.parseInt(sendDay.split("/")[1])).substring(4);
		}
		if(sendType.equals(SEND_TYPE_MONTH_BEGIN)){
			sendDay = DateUtils.formatDate(new Date(),"MM");
		}
		System.out.println(sendType+" ---- "+sendDay);
		return this.getMapper().selectActivityBirthday(sendType,sendDay);
	}
	
}

