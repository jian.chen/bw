package com.bw.adv.module.activity.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.InviteMemberRecordMapper;
import com.bw.adv.module.activity.model.InviteMemberRecord;
import com.bw.adv.module.activity.repository.InviteMemberRecordRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Ronald on 2016/12/12.
 */
@Repository
public class InviteMemberRecordRepositoryImpl extends BaseRepositoryImpl<InviteMemberRecord, InviteMemberRecordMapper> implements InviteMemberRecordRepository{
    @Override
    protected Class<InviteMemberRecordMapper> getMapperClass() {
        return InviteMemberRecordMapper.class;
    }

    @Override
    public List<InviteMemberRecord> getAllValidInviteMemberRecordOfToday(String today) {
        return this.getMapper().getAllValidInviteRecordOfToday(today);
    }
}
