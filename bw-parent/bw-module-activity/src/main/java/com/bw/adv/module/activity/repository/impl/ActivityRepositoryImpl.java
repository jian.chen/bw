/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.ActivityMapper;
import com.bw.adv.module.activity.model.Activity;
import com.bw.adv.module.activity.repository.ActivityRepository;

/**
 * ClassName:活动 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ActivityRepositoryImpl extends BaseRepositoryImpl<Activity, ActivityMapper> implements ActivityRepository{

	@Override
	protected Class<ActivityMapper> getMapperClass() {
		return ActivityMapper.class;
	}

	/**
	 * findAllActive:查询所有活动<br/>
	 * Date: 2015-8-11 下午2:42:37 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	@Override
	public List<Activity> findAllActive() {
		return this.getMapper().selectAllActive();
	}
	
	@Override
	public List<Activity> findActiveByTypeId(Long typeId) {
		return this.getMapper().selectAllActiveByTypeId(typeId);
	}
	
}

