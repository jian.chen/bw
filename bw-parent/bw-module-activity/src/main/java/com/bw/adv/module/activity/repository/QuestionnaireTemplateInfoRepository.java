package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.QuestionnaireTemplateInfoMapper;
import com.bw.adv.module.activity.model.QuestionCount;
import com.bw.adv.module.activity.model.QuestionnaireTemplateInfo;

public interface QuestionnaireTemplateInfoRepository extends BaseRepository<QuestionnaireTemplateInfo, QuestionnaireTemplateInfoMapper>{

	/**
     * 查询题目选项
     * @param templateId
     * @return
     */
    public List<QuestionnaireTemplateInfo> queryTemplateInfoByTemplateId(Long templateId);
    
    /**
     * 删除题目选项
     * @param templateId
     */
    public void deleteTemplateInfoByTemplateId(Long templateId);
    
    /**
     * 查询选择题统计结果
     * @param templateId
     * @return
     */
    List<QuestionCount> selectResultCountForSelect(Long questionnaireId);
    
    /**
     * 查询打分题统计结果
     * @param templateId
     * @return
     */
    List<QuestionCount> selectResultCountForMark(Long questionnaireId);
    
    /**
     * 查询问答题统计结果
     * @param templateId
     * @return
     */
    List<QuestionCount> selectResultCountForQa(Long questionnaireId);
    
    /**
     * 查询文件一共回答的人数
     * @param templateId
     * @return
     */
	Long selectCountInfoForMember(Long questionnaireId);
}
