package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.SceneGameInstance;
import com.bw.adv.module.activity.model.exp.SceneGameInstanceExp;

public interface SceneGameInstanceMapper extends BaseMapper<SceneGameInstance>{
    
    /**
     * 
     * selectSceneGameInstanceByGameId:查询某个场景游戏下所有的实例. <br/>
     * Date: 2016年1月18日 上午11:37:20 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameId
     * @return
     */
    List<SceneGameInstance> selectSceneGameInstanceByGameId(@Param("sceneGameId")Long sceneGameId,@Param("page")Page<SceneGameInstance> page);
    
    /**
     * updateSceneGameInstanceStatus:更新活动实例的状态. <br/>
     * Date: 2016年1月19日 下午6:28:00 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstance
     * @return
     */
    int updateSceneGameInstanceStatus(SceneGameInstance sceneGameInstance);
    
    /**
     * countOpenSceneGameInstance:统计某个场景下激活实例的数量. <br/>
     * Date: 2016年1月19日 下午7:08:22 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    int countOpenSceneGameInstance(Long sceneGameId);
    
    /**
     * updateStatusOutDate:更新已过期的活动. <br/>
     * Date: 2016年1月19日 下午8:38:26 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @return
     */
    int updateStatusOutDate();
    
    /**
     * selectValidInstanceById:查询有效的活动. <br/>
     * Date: 2016年1月21日 下午4:50:13 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    SceneGameInstance selectValidInstanceById(Long sceneGameInstanceId);
    
    /**
     * selectValidInstanceByStartTime:查询活动是否开始. <br/>
     * Date: 2016年1月25日 下午3:24:19 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    int selectValidInstanceByStartTime(Long sceneGameInstanceId);
    
    /**
     * selectValidInstanceByEndTime:查询活动是否过期. <br/>
     * Date: 2016年1月25日 下午3:26:47 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    int selectValidInstanceByEndTime(Long sceneGameInstanceId);

	List<SceneGameInstanceExp> selectSceneGameWinRate(@Param("likeInstanceName")String likeInstanceName,
			@Param("greaterEqualCreateTime")String greaterEqualCreateTime, @Param("lessEqualCreateTime")String lessEqualCreateTime);

	List<SceneGameInstanceExp> selectSceneGameAwardsWinRate(@Param("sceneGameInstanceId")Long sceneGameInstanceId);
	
	/**
	 * selectValidInstance:查询有效的活动. <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * Date: 2016年3月1日 下午8:57:09 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang(*^_^*)
	 * @version jdk1.7
	 * @return
	 */
	SceneGameInstance selectValidInstance(String sceneGameCode);
	SceneGameInstance selectValidInstance();

	/**
	 * selectBySceneGameId:(查询场景游戏的实例列表). <br/>
	 * Date: 2016-3-30 下午2:50:11 <br/>
	 * scrmVersion standard
	 * @author mennan
	 * @version jdk1.7
	 * @param sceneGameId
	 * @return
	 */
	List<SceneGameInstance> selectBySceneGameId(Long sceneGameId);

	/**
	 * selectValidInstanceByCode:(查询场景游戏的实例列表). <br/>
	 * Date: 2016-3-30 下午2:50:11 <br/>
	 * scrmVersion standard
	 * @author chenjian
	 * @version jdk1.7
	 * @param sceneGameInstanceCode 编码
	 * @return
	 */
	List<SceneGameInstance> selectValidInstanceByCode(String sceneGameInstanceCode);
}