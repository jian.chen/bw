/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:WinningItemRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2015年11月20日下午3:11:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.WinningItemMapper;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.WinningItemExp;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName:WinningItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:11:06 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WinningItemRepository extends BaseRepository<WinningItem, WinningItemMapper> {
	
	/**
	 * 
	 * findWinningItemPersonsByActivityIdAndAwardId:(查询某活动对应的奖项获奖的人数). <br/>
	 * Date: 2015年11月25日 上午10:37:58 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<WinningItem> findWinningItemPersonsByActivityIdAndAwardId(Example example, Page<WinningItem> page);
	/**
	 * 
	 * findWinningItemPersonsByExample:(查询某活动对应的奖项获奖的人数). <br/>
	 * Date: 2015年11月25日 上午10:37:58 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	Long findWinningItemPersonsByExample(Example example);
	
	/**
	 * 
	 * queryWinningItemListByMemberIdAndActivityId:(查询会员信息). <br/>
	 * Date: 2015年12月15日 上午11:16:07 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<WinningItemExp> queryWinningItemListByMemberIdAndActivityId(Example example, Page<WinningItemExp> page);
	
	/**
	 * 
	 * queryItemLastNum:(查询最近几条中奖纪录). <br/>
	 * Date: 2016年3月22日 下午6:11:24 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param num
	 * @return
	 */
	List<WinningItemExp> queryItemLastNum(Long num);

}

