package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.VoteResultMapper;
import com.bw.adv.module.activity.model.VoteResult;
import com.bw.adv.module.activity.repository.VoteResultRepository;

@Repository
public class VoteResultRepositpryImpl extends BaseRepositoryImpl<VoteResult, VoteResultMapper> implements VoteResultRepository{

	@Override
	protected Class<VoteResultMapper> getMapperClass() {
		return VoteResultMapper.class;
	}

	@Override
	public Long selectCountForVoteResult(Long titleId) {
		return this.getMapper().selectCountForVoteResult(titleId);
	}

	@Override
	public List<VoteResult> findVoteResultByTitleId(Long titleId) {
		return this.getMapper().findVoteResultByTitleId(titleId);
	}

}
