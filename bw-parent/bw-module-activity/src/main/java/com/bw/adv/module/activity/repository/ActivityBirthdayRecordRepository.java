package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.ActivityBirthdayRecordMapper;
import com.bw.adv.module.activity.model.ActivityBirthdayRecord;

public interface ActivityBirthdayRecordRepository extends BaseRepository<ActivityBirthdayRecord,ActivityBirthdayRecordMapper>{

	/**
	 * 根据会员id，活动实例id，创建年份查找记录
	 * @param memberId
	 * @param activityInstanceId
	 * @param createYear
	 * @return
	 */
	List<ActivityBirthdayRecord> findRecordByIdAndMember(Long memberId,Long activityInstanceId,String createYear);
}
