package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.ActivityTagActionMapper;
import com.bw.adv.module.activity.model.ActivityTagAction;
import com.bw.adv.module.activity.repository.ActivityTagActionRepository;

@Repository
public class ActivityTagActionRepositoryImpl extends BaseRepositoryImpl<ActivityTagAction, ActivityTagActionMapper> implements ActivityTagActionRepository{

	@Override
	protected Class<ActivityTagActionMapper> getMapperClass() {
		return ActivityTagActionMapper.class;
	}

	@Override
	public List<ActivityTagAction> findByIsNotCountWithOrderByHappenTime() {
		return this.getMapper().selectByIsNotCountWithOrderByHappenTime();
	}

}
