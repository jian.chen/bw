package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.QuestionnaireTemplate;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QuestionnaireTemplateMapper extends BaseMapper<QuestionnaireTemplate>{
    int deleteByPrimaryKey(Long questionnaireTemplateId);

    int insert(QuestionnaireTemplate record);

    int insertSelective(QuestionnaireTemplate record);

    QuestionnaireTemplate selectByPrimaryKey(Long questionnaireTemplateId);

    int updateByPrimaryKeySelective(QuestionnaireTemplate record);

    int updateByPrimaryKey(QuestionnaireTemplate record);
    
    /**
     * 根据问卷id查询问卷题目
     * @param questionnaireId
     * @return
     */
    List<QuestionnaireTemplate> selectByQuestionnaireId(@Param("questionnaireId")Long questionnaireId,@Param("templatePage")Page<QuestionnaireTemplate> templatePage);

    /**
     * 根据问卷id查询问卷题目(不带分页)
     * @param questionnaireId
     * @return
     */
    List<QuestionnaireTemplate> selectByQuestionnaireIdNoPage(@Param("questionnaireId")Long questionnaireId);
}