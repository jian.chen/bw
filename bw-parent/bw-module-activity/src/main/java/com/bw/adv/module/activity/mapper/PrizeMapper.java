package com.bw.adv.module.activity.mapper;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.Prize;

public interface PrizeMapper extends BaseMapper<Prize>{
    int deleteByPrimaryKey(Long prizeId);

    int insert(Prize record);

    int insertSelective(Prize record);

    Prize selectByPrimaryKey(Long prizeId);

    int updateByPrimaryKeySelective(Prize record);

    int updateByPrimaryKey(Prize record);

	Prize selectPrizeByAwardId(@Param("awardsId")Long awardsId);
	
	/**
	 * deletePrize:删除奖品. <br/>
	 * Date: 2016年1月19日 下午4:21:21 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	int deletePrize(Long sceneGameInstanceId);
	
}