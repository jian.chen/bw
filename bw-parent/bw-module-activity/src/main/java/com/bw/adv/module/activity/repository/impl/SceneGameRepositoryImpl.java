/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:SceneGameRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2016年1月15日下午1:41:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.SceneGameMapper;
import com.bw.adv.module.activity.model.SceneGame;
import com.bw.adv.module.activity.repository.SceneGameRepository;

/**
 * ClassName:SceneGameRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午1:41:16 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SceneGameRepositoryImpl extends BaseRepositoryImpl<SceneGame, SceneGameMapper> implements SceneGameRepository {

	@Override
	protected Class<SceneGameMapper> getMapperClass() {
		return SceneGameMapper.class;
	}

	@Override
	public List<SceneGame> findAll() {
		return this.getMapper().selectAll();
	}


}

