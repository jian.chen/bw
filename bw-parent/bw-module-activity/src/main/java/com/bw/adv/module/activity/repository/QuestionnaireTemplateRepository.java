package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.QuestionnaireTemplateMapper;
import com.bw.adv.module.activity.model.QuestionnaireTemplate;

public interface QuestionnaireTemplateRepository extends BaseRepository<QuestionnaireTemplate, QuestionnaireTemplateMapper>{

	/**
     * 根据问卷id查询问卷题目
     * @param questionnaireId
     * @return
     */
    List<QuestionnaireTemplate> selectByQuestionnaireId(Long questionnaireId,Page<QuestionnaireTemplate> templatePage);
    
    /**
     * 根据问卷id查询问卷题目（不带分页）
     * @param questionnaireId
     * @return
     */
    List<QuestionnaireTemplate> queryQuestionTemplateNoPage(Long questionnaireId);
    
}
