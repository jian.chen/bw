/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:MemberWinningAccordRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2015年11月20日下午3:06:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.MemberWinningAccordMapper;
import com.bw.adv.module.activity.model.MemberWinningAccord;

/**
 * ClassName:MemberWinningAccordRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>	
 * Date: 2015年11月20日 下午3:06:21 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberWinningAccordRepository extends BaseRepository<MemberWinningAccord, MemberWinningAccordMapper> {
	
	/**
	 * findMemberSurplusNumber:(统计符合抽奖的会员资格的次数). <br/>
	 * Date: 2015年11月24日 下午3:16:50 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberId
	 * @param activityId
	 * @return
	 */
	int findMemberSurplusNumber(Long memberId, Long activityId,Long qrCodeId);
	
	/**
	 * 查询当天抽奖次数
	 * @param memberId
	 * @param activityId
	 * @param day
	 * @return
	 */
	Long findMemberSurplusNumberToday(Long memberId, Long activityId,Long qrCodeId,String day);
	
	/**
	 * 当天使用了的次数
	 * @param memberId
	 * @param activityId
	 * @param someDay
	 * @return
	 */
	Long queryMemberUseChance(Long memberId, Long activityId,Long qrCodeId,String someDay);
	
	/**
	 * 
	 * updateOneByMember:(每抽奖一次，根据活动id,修改抽奖资格(创建时间早的优先修改)). <br/>
	 * Date: 2016年3月17日 下午4:34:26 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param memberId
	 * @param activityId
	 */
	void updateOneByMember(Long memberId, Long activityId,Long qrCodeId);
	
	/**
     * 根据会员id和活动id查询抽奖资格
     * @param memberId
     * @param activityId
     * @return
     */
    MemberWinningAccord queryResultByMemberIdAndActivityId(Long memberId,Long activityId,Long qrCodeId);
    
    MemberWinningAccord queryResultToday(Long memberId,Long activityId,Long qrCodeId,String someDay);
    
    /**
     * 更新抽奖资格表
     * @param memberWinningAccordId
     * @param version
     * @param useTime
     * @return
     */
    int updateMemberAccordWithVersion(Long memberWinningAccordId,Long qrCodeId,Long version,String useTime);
    
	

	/**
     * selectAccordSomeDay:(查询会员某一天的抽奖资格:包含使用和未使用). <br/>
     * Date: 2016-6-15 下午4:38:20 <br/>
     * scrmVersion standard
     * @author mennan
     * @version jdk1.7
     * @param memberId
     * @param activityId
     * @return
     */
    List<MemberWinningAccord> findAccordListCurrentDate(Long memberId,Long activityId,Long qrCodeId,String someDay,int accordType);
    
    
    /**
     * selectAccordSomeDay:(查询会员某一天的抽奖资格:包含使用和未使用). <br/>
     * Date: 2016-6-15 下午4:38:20 <br/>
     * scrmVersion standard
     * @author mennan
     * @version jdk1.7
     * @param memberId
     * @param activityId
     * @return
     */
    int findAccordSizeCurrentDate(Long memberId,Long activityId,Long qrCodeId,String someDay,int accordType);

	MemberWinningAccord queryMemberMapsQrCode(Long memberId, Long activityId,String qrCode,String isUse);

	int updateMemberAccordMapsVersion(Long memberWinningAccordId,String qrCode, String useTime);
    
}

