package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.MemberWinningAccord;

public interface MemberWinningAccordMapper extends BaseMapper<MemberWinningAccord>{
    
    /**
     * queryMemberSurplusNumber:(查询会员抽奖资格次数). <br/>
     * Date: 2015年11月24日 下午3:05:11 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param memberId
     * @param activityId
     * @return
     */
    int queryMemberSurplusNumber(@Param("memberId")Long memberId, @Param("activityId")Long activityId,@Param("qrCodeId")Long qrCodeId);
    
    /**
     * 查询会员当天抽奖次数
     * @param memberId
     * @param activityId
     * @param someDay
     * @return
     */
    Long queryMemberSurplusNumberToday(@Param("memberId")Long memberId, @Param("activityId")Long activityId,@Param("qrCodeId") Long qrCodeId,@Param("someDay")String someDay);
    
    /**
     * 
     * updateOneByMember:(每抽奖一次，根据活动id,修改抽奖资格(创建时间早的优先修改)). <br/>
     * Date: 2016年3月17日 下午4:31:54 <br/>
     * scrmVersion 1.0
     * @author sherry.wei
     * @version jdk1.7
     * @param memberId
     * @param activityId
     */
    void updateOneByMember(@Param("memberId")Long memberId, @Param("activityId")Long activityId,@Param("qrCodeId")Long qrCodeId);
    
    /**
     * 根据会员id和活动id查询抽奖资格
     * @param memberId
     * @param activityId
     * @return
     */
    MemberWinningAccord queryResultByMemberIdAndActivityId(@Param("memberId")Long memberId, @Param("activityId")Long activityId,@Param("qrCodeId")Long qrCodeId);
    
    MemberWinningAccord queryResultToday(@Param("memberId")Long memberId, @Param("activityId")Long activityId,@Param("qrCodeId")Long qrCodeId,@Param("someDay")String someDay);
    
    /**
     * 更新抽奖资格表
     * @param memberWinningAccordId
     * @param version
     * @param useTime
     * @return
     */
    int updateMemberAccordWithVersion(@Param("memberWinningAccordId")Long memberWinningAccordId,@Param("qrCodeId")Long qrCodeId,@Param("version")Long version,@Param("useTime")String useTime);
    
    /**
     * selectAccordListSomeDay:(查询会员某一天的抽奖资格:包含使用和未使用). <br/>
     * Date: 2016-6-15 下午4:38:20 <br/>
     * scrmVersion standard
     * @author mennan
     * @version jdk1.7
     * @param memberId
     * @param activityId
     * @param someDay
     * @return
     */
    List<MemberWinningAccord> selectAccordListSomeDay(@Param("memberId")Long memberId, @Param("activityId")Long activityId,@Param("qrCodeId")Long qrCodeId,@Param("someDay")String someDay,@Param("accordType")int accordType);
    
    /**
     * 查询用户今天已经用了抽奖机会次数
     * @param memberId
     * @param activityId
     * @param someDay
     * @return
     */
    Long queryMemberUseChance(@Param("memberId")Long memberId, @Param("activityId")Long activityId,@Param("qrCodeId")Long qrCodeId,@Param("someDay")String someDay);
    
    /**
     * selectAccordSizeSomeDay:(查询会员某一天的抽奖资格:包含使用和未使用). <br/>
     * Date: 2016-6-15 下午4:38:20 <br/>
     * scrmVersion standard
     * @author mennan
     * @version jdk1.7
     * @param memberId
     * @param activityId
     * @param someDay
     * @return
     */
    int selectAccordSizeSomeDay(@Param("memberId")Long memberId, @Param("activityId")Long activityId,@Param("qrCodeId")Long qrCodeId,@Param("someDay")String someDay,@Param("accordType")int accordType);

	MemberWinningAccord selectMemberMapsQrCode(@Param("memberId")Long memberId,@Param("activityId") Long activityId,@Param("qrCode") String qrCode,@Param("isUse")String isUse);

	int updateMemberAccordMapsVersion(@Param("memberWinningAccordId")Long memberWinningAccordId,@Param("qrCode")String qrCode, @Param("useTime")String useTime);

	
    
}