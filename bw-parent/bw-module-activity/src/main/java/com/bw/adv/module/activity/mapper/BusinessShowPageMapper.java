package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.BusinessShowPage;

public interface BusinessShowPageMapper extends BaseMapper<BusinessShowPage>{
	
}