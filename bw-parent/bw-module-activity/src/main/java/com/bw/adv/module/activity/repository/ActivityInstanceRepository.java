/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:ss.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015-8-11下午1:42:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.ActivityInstanceMapper;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.model.exp.ActivityRuleConditionExp;
import com.bw.adv.module.member.model.Member;

/**
 * ClassName:活动 <br/>
 * Date: 2015-8-11 下午1:42:05 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface ActivityInstanceRepository extends BaseRepository<ActivityInstance, ActivityInstanceMapper>{

	/**
	 * findByActivityId:(根据活动ID查询活动实例). <br/>
	 * Date: 2015-8-26 下午6:33:45 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 */
	List<ActivityInstance> findByActivityId(Long activityId);

	/**
	 * findByActivityId:(根据活动ID查询活动实例,分页). <br/>
	 * Date: 2015-8-26 下午6:39:55 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityId
	 * @param page
	 * @return
	 */
	List<ActivityInstance> findByActivityId(Long activityId,Page<ActivityInstance> page);

	/**
	 * findActivityInstanceExpByInstanceId:(查询活动实例信息). <br/>
	 * Date: 2015-9-1 下午4:08:45 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param instanceId
	 * @return
	 */
	List<ActivityRuleConditionExp> findActivityInstanceExpByInstanceId(Long instanceId);
	
	/**
	 * findActivityConditionValue:(根据活动实例状态，查询所有活动实例及对应的条件值). <br/>
	 * Date: 2015-9-5 下午4:26:58 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param statusIds
	 * @return
	 */
	List<ActivityRuleConditionExp> findActivityConditionValue(String statusIds);
	
	/**
	 * findByStatusIds:(根据活动实例状态，查询所有活动实例). <br/>
	 * Date: 2015-9-5 下午4:26:58 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param statusIds
	 * @return
	 */
	List<ActivityInstance> findByStatusIds(String statusIds);
	
	
	/**
	 * findActivityBirthday:(查询生日活动的会员). <br/>
	 * Date: 2015-9-13 上午11:40:35 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param sendDay
	 * @return
	 */
	List<Member> findActivityBirthday(String sendDay);
	
	/**
	 * findActivityWake:(查询唤醒活动的会员). <br/>
	 * Date: 2015年9月16日 下午7:47:25 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wakeDate
	 * @return
	 */
	List<Member> findActivityWake(String wakeDate);
	
	/**
	 * findActivitySpecial:(查询指定时间的会员)
	 * @return
	 */
	List<Member> findActivitySpecial();
	
}

