package com.bw.adv.module.activity.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.Activity;

public interface ActivityMapper extends BaseMapper<Activity>{
	
    int deleteByPrimaryKey(Long activityId);

    int insert(Activity record);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(Long activityId);

    int updateByPrimaryKeySelective(Activity record);

    int updateByPrimaryKey(Activity record);
    
    /**
     * selectAllActive:查询所有的启用的活动<br/>
     * Date: 2015-8-11 下午2:33:20 <br/>
     * scrmVersion 1.0
     * @author mennan
     * @version jdk1.7
     * @return
     */
    List<Activity> selectAllActive();
    
    /**
     * 根据活动类型查询启用活动
     * @param activityTypeId
     * @return
     */
    List<Activity> selectAllActiveByTypeId(Long activityTypeId);
    
}