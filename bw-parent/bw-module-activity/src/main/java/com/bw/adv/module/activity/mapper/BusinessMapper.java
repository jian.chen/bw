package com.bw.adv.module.activity.mapper;

import com.bw.adv.module.activity.model.Business;

public interface BusinessMapper {
    int deleteByPrimaryKey(Long businessId);

    int insert(Business record);

    int insertSelective(Business record);

    Business selectByPrimaryKey(Long businessId);

    int updateByPrimaryKeySelective(Business record);

    int updateByPrimaryKey(Business record);
}