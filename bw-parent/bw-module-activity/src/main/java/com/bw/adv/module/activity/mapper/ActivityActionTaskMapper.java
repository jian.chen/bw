package com.bw.adv.module.activity.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.ActivityActionTask;

public interface ActivityActionTaskMapper extends BaseMapper<ActivityActionTask>{

	/**
	 * selectByIsNotCountWithOrderByHappenTime:(查询所有未计算的活动动作，并根据发生时间正序排序). <br/>
	 * Date: 2015-8-29 上午10:54:56 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<ActivityActionTask> selectByIsNotCountWithOrderByHappenTime(int size);
	
}