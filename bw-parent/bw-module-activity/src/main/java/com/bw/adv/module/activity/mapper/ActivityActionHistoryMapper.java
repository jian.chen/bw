package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.ActivityActionHistory;

public interface ActivityActionHistoryMapper extends BaseMapper<ActivityActionHistory> {
	
}