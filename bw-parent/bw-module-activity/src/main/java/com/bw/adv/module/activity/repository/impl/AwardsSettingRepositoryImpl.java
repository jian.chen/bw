/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:AwardsSettingRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015年11月20日下午3:22:57
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.AwardsSettingMapper;
import com.bw.adv.module.activity.model.AwardsSetting;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;
import com.bw.adv.module.activity.repository.AwardsSettingRepository;

/**
 * ClassName:AwardsSettingRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:22:57 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class AwardsSettingRepositoryImpl extends BaseRepositoryImpl<AwardsSetting, AwardsSettingMapper> implements AwardsSettingRepository {
	
	@Override
	protected Class<AwardsSettingMapper> getMapperClass() {
		return AwardsSettingMapper.class;
	}

	@Override
	public List<AwardsSetting> findAwardsSettingsByActivityId(Long activityId) {
		
		return this.getMapper().selectAwardsSettingsByActivityId(activityId);
	}

	@Override
	public List<AwardsSettingExp> findAwardsSettingExpByActivityId(Long activityId) {
		return this.getMapper().selectAwardsSettingsExpByActivityId(activityId);
	}
	@Override
	public AwardsSettingExp findAwardsSettingExpByAwardsId(Long awardsId) {
		return this.getMapper().selectAwardsSettingsExpByAwardsId(awardsId);
	}
	
	@Override
	public int removeAwardsSettings(Long sceneGameInstanceId) {
		return this.getMapper().deleteAwardsSettings(sceneGameInstanceId);
	}
	
}

