/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:PrizeInstanceRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015年11月20日下午3:31:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.PrizeInstanceMapper;
import com.bw.adv.module.activity.model.PrizeInstance;
import com.bw.adv.module.activity.model.exp.PrizeInstanceExp;
import com.bw.adv.module.activity.repository.PrizeInstanceRepository;

/**
 * ClassName:PrizeInstanceRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:31:21 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class PrizeInstanceRepositoryImpl extends BaseRepositoryImpl<PrizeInstance, PrizeInstanceMapper> implements PrizeInstanceRepository {
	
	@Override
	protected Class<PrizeInstanceMapper> getMapperClass() {
		return PrizeInstanceMapper.class;
	}

	@Override
	public PrizeInstanceExp queryPrizeInstanceExpByAwardsId(Long awardsId) {
		return this.getMapper().prizeInstanceExpByAwardsId(awardsId);
	}

	@Override
	public int batchaddPrizeInstance(List<PrizeInstance> prizeInstanceList) {
		return this.getMapper().batchInsertPrizeInstance(prizeInstanceList);
	}

	@Override
	public int removePrizeInstanceById(Long sceneGameInstanceId) {
		return this.getMapper().deletePrizeInstanceById(sceneGameInstanceId);
	}

	@Override
	public int findPrizeInstanceCountByPrizeId(Long prizeId) {
		return this.getMapper().selectPrizeInstanceCountByPrizeId(prizeId);
	}

}

