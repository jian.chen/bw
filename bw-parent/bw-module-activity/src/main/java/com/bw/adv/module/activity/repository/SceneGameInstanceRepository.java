/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:SceneGameInstanceRepository.java
 * Package Name:com.sage.scrm.module.activity.repository
 * Date:2016年1月15日下午2:24:27
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.SceneGameInstanceMapper;
import com.bw.adv.module.activity.model.SceneGameInstance;
import com.bw.adv.module.activity.model.exp.SceneGameInstanceExp;

/**
 * ClassName:SceneGameInstanceRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午2:24:27 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
public interface SceneGameInstanceRepository extends BaseRepository<SceneGameInstance, SceneGameInstanceMapper> {
	
	/**
	 * findSceneGameInstanceByGameId:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2016年1月18日 下午5:14:44 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameId
	 * @return
	 */
	List<SceneGameInstance> findSceneGameInstanceByGameId(Long sceneGameId,Page<SceneGameInstance> page);
	
	
	/**
	 * findBySceneGameId:(查询场景游戏的实例列表). <br/>
	 * Date: 2016-3-30 下午2:48:47 <br/>
	 * scrmVersion standard
	 * @author mennan
	 * @version jdk1.7
	 * @param sceneGameId
	 * @return
	 */
	List<SceneGameInstance> findBySceneGameId(Long sceneGameId);
	
	/**
	 * updateSceneGameInstanceStatus:更新活动实例的状态. <br/>
	 * Date: 2016年1月19日 下午6:36:38 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstance
	 * @return
	 */
	int updateSceneGameInstanceStatus(SceneGameInstance sceneGameInstance);
	
	/**
	 * countOpenSceneGameInstance:查询当前场景下已激活的活动实例. <br/>
	 * Date: 2016年1月19日 下午7:15:22 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameId
	 * @return
	 */
	int countOpenSceneGameInstance(Long sceneGameId);
	
	/**
	 * updateStatusOutDate:更新过期的活动状态. <br/>
	 * Date: 2016年1月19日 下午8:43:31 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @return
	 */
	int updateStatusOutDate();
	
	/**
	 * findValidInstanceById:查询有效的活动. <br/>
	 * Date: 2016年1月21日 下午4:54:49 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	SceneGameInstance findValidInstanceById(Long sceneGameInstanceId);
	
	/**
	 * findValidInstanceByStartTime:查询活动是否开始. <br/>
	 * Date: 2016年1月25日 下午3:33:32 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
    int findValidInstanceByStartTime(Long sceneGameInstanceId);
    
    /**
     * findValidInstanceByEndTime:查询活动是否过期. <br/>
     * Date: 2016年1月25日 下午3:33:46 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    int findValidInstanceByEndTime(Long sceneGameInstanceId);

    /**
     * 
     * findSceneGameWinRate:查询抽奖活动的中奖率 <br/>
     * Date: 2016年2月18日 上午10:06:08 <br/>
     * scrmVersion 1.0
     * @author simon
     * @version jdk1.7
     * @param likeInstanceName
     * @param greaterEqualCreateTime
     * @param lessEqualCreateTime
     * @return
     */
	List<SceneGameInstanceExp> findSceneGameWinRate(String likeInstanceName,
			String greaterEqualCreateTime, String lessEqualCreateTime);

	List<SceneGameInstanceExp> findSceneGameAwardsWinRate(
			Long sceneGameInstanceId);
	
	/**
	 * findValidInstance:查询有效的活动 <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * Date: 2016年3月1日 下午9:00:56 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang(*^_^*)
	 * @version jdk1.7
	 * @return
	 */
	SceneGameInstance findValidInstance(String sceneGameCode);

	/**
	 * findValidInstanceByCode:查询有效的活动 <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * Date: 2016年3月1日 下午9:00:56 <br/>
	 * scrmVersion 1.0
	 * @author chenjian(*^_^*)
	 * @version jdk1.7
	 * @return
	 */
	List<SceneGameInstance> findValidInstanceByCode(String sceneGameInstanceCode);
}

