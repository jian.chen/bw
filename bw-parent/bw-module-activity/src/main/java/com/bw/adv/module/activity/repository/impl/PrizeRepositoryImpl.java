/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:PrizeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015年11月20日下午3:32:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.PrizeMapper;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.activity.repository.PrizeRepository;

/**
 * ClassName:PrizeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:32:56 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class PrizeRepositoryImpl extends BaseRepositoryImpl<Prize, PrizeMapper> implements PrizeRepository {
	
	@Override
	protected Class<PrizeMapper> getMapperClass() {
		return PrizeMapper.class;
	}

	@Override
	public Prize findPrizeByAwardId(Long awardsId) {
		return this.getMapper().selectPrizeByAwardId(awardsId);
	}
	
	@Override
	public int removePrize(Long sceneGameInstanceId) {
		return this.getMapper().deletePrize(sceneGameInstanceId);
	}
	
}

