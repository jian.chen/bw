/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:WinningItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015年11月20日下午3:34:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.mapper.WinningItemMapper;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.WinningItemExp;
import com.bw.adv.module.activity.repository.WinningItemRepository;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName:WinningItemRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:34:22 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WinningItemRepositoryImpl extends BaseRepositoryImpl<WinningItem, WinningItemMapper> implements WinningItemRepository {
	
	@Override
	protected Class<WinningItemMapper> getMapperClass() {
		return WinningItemMapper.class;
	}

	@Override
	public List<WinningItem> findWinningItemPersonsByActivityIdAndAwardId(Example example, Page<WinningItem> page) {
		return this.getMapper().selectWinningItemPersonsByActivityIdAndAwardId(example,page);
	}

	@Override
	public List<WinningItemExp> queryWinningItemListByMemberIdAndActivityId(Example example, Page<WinningItemExp> page) {
		return this.getMapper().selectWinningItemListByMemberIdAndActivityId(example,page);
	}
	
	@Override
	public List<WinningItemExp> queryItemLastNum(Long num) {
		return this.getMapper().selectItemLastNum(num);
	}

	@Override
	public Long findWinningItemPersonsByExample(Example example) {
		return this.getMapper().selectWinningItemPersonsByExample(example);
	}
	
}

