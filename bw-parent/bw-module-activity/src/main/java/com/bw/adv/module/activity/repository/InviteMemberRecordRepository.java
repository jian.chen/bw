package com.bw.adv.module.activity.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.activity.mapper.InviteMemberRecordMapper;
import com.bw.adv.module.activity.model.InviteMemberRecord;

import java.util.List;

/**
 * Created by Ronald on 2016/12/12.
 */
public interface InviteMemberRecordRepository extends BaseRepository<InviteMemberRecord, InviteMemberRecordMapper>{
    public List<InviteMemberRecord> getAllValidInviteMemberRecordOfToday(String today);
}
