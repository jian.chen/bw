/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-activity
 * File Name:AwardPrizeRelRepositoryImpl.java
 * Package Name:com.sage.scrm.module.activity.repository.impl
 * Date:2015年11月20日下午3:13:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.activity.mapper.AwardPrizeRelMapper;
import com.bw.adv.module.activity.model.AwardPrizeRel;
import com.bw.adv.module.activity.repository.AwardPrizeRelRepository;

/**
 * ClassName:AwardPrizeRelRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月20日 下午3:13:18 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class AwardPrizeRelRepositoryImpl extends BaseRepositoryImpl<AwardPrizeRel, AwardPrizeRelMapper> implements AwardPrizeRelRepository {
	
	@Override
	protected Class<AwardPrizeRelMapper> getMapperClass() {
		return AwardPrizeRelMapper.class;
	}
	
	@Override
	public int removeRelBySceneGameInstanceId(long sceneGameInstanceId) {
		return this.getMapper().deleteRelBySceneGameInstanceId(sceneGameInstanceId);
	}
	
}

