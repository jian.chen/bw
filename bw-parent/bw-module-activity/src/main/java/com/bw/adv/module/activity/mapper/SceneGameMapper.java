package com.bw.adv.module.activity.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.SceneGame;

public interface SceneGameMapper extends BaseMapper<SceneGame>{
    int deleteByPrimaryKey(Long sceneGameId);

    int insert(SceneGame record);

    int insertSelective(SceneGame record);

    SceneGame selectByPrimaryKey(Long sceneGameId);

    int updateByPrimaryKeySelective(SceneGame record);

    int updateByPrimaryKey(SceneGame record);
    
}