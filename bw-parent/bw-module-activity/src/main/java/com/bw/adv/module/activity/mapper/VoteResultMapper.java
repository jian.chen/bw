package com.bw.adv.module.activity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.activity.model.VoteResult;

public interface VoteResultMapper extends BaseMapper<VoteResult>{
    int deleteByPrimaryKey(Long resultId);

    int insert(VoteResult record);

    int insertSelective(VoteResult record);

    VoteResult selectByPrimaryKey(Long resultId);

    int updateByPrimaryKeySelective(VoteResult record);

    int updateByPrimaryKey(VoteResult record);
    
    /**
	 * 查询问卷回答人数
	 * @param titleId
	 * @return
	 */
    @Select("select count(*) from vote_result "
    		+ "where title_id = #{titleId}")
	public Long selectCountForVoteResult(@Param("titleId")Long titleId);
    
    /**
	 * 查询投票的调查结果
	 * @param titleId
	 * @return
	 */
    @Select("select vo.OPTIONS_ID as optionId,vo.OPTION_NAME as optionName,count(*) as memberSum "
    		+ "FROM vote_result vr "
    		+ "right join vote_options vo on vr.OPTIONS_ID = vo.OPTIONS_ID "
    		+ "where vo.TITLE_ID = #{titleId} "
    		+ "GROUP BY vo.OPTIONS_ID "
    		+ "ORDER BY vo.OPTIONS_ID ASC;")
	public List<VoteResult> findVoteResultByTitleId(@Param("titleId")Long titleId);
}