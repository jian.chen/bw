/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageGroupItemRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月18日下午4:35:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatMessageGroupItemMapper;
import com.bw.adv.module.wechat.model.WechatMessageGroupItem;

/**
 * ClassName:WechatMessageGroupItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:35:09 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMessageGroupItemRepository extends BaseRepository<WechatMessageGroupItem, WechatMessageGroupItemMapper> {

}

