/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansGroupRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月11日下午5:48:23
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatFansGroupMapper;
import com.bw.adv.module.wechat.model.WechatFansGroup;

/**
 * ClassName:WechatFansGroupRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午5:48:23 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansGroupRepository extends BaseRepository<WechatFansGroup, WechatFansGroupMapper> {
	
	/**
	 * findAllGroups:(获取所有分组信息). <br/>
	 * Date: 2015年8月20日 下午4:40:47 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	public List<WechatFansGroup> findAllGroups();
	
	/**
	 * findWechatFansGroup:(根据分组id查询分组信息). <br/>
	 * Date: 2015年8月20日 下午4:41:10 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param groupId
	 * @return
	 */
	public WechatFansGroup findWechatFansGroup(String groupId);
	
}

