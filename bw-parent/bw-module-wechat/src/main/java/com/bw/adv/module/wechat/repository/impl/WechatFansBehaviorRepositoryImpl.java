/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansBehaviorRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月19日下午3:43:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatFansBehaviorMapper;
import com.bw.adv.module.wechat.model.WechatFansBehavior;
import com.bw.adv.module.wechat.repository.WechatFansBehaviorRepository;

/**
 * ClassName:WechatFansBehaviorRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午3:43:51 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatFansBehaviorRepositoryImpl extends BaseRepositoryImpl<WechatFansBehavior, WechatFansBehaviorMapper> implements WechatFansBehaviorRepository {

	@Override
	protected Class<WechatFansBehaviorMapper> getMapperClass() {
		return WechatFansBehaviorMapper.class;
	}
	
	
}

