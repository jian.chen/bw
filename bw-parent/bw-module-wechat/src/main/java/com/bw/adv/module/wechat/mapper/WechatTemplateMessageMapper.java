package com.bw.adv.module.wechat.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatTemplateMessage;

public interface WechatTemplateMessageMapper  extends BaseMapper<WechatTemplateMessage>  {
    int deleteByPrimaryKey(Long templateMessId);

    int insert(WechatTemplateMessage record);

    int insertSelective(WechatTemplateMessage record);

    WechatTemplateMessage selectByPrimaryKey(Long templateMessId);

    int updateByPrimaryKeySelective(WechatTemplateMessage record);

    int updateByPrimaryKey(WechatTemplateMessage record);
    
    List<WechatTemplateMessage> selectWechatTemplateMessageList();
    
}