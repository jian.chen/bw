/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatGroupMessage.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月18日下午4:52:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatGroupMessageMapper;
import com.bw.adv.module.wechat.model.WechatGroupMessage;

/**
 * ClassName:WechatGroupMessage <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:52:34 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatGroupMessageRepository extends BaseRepository<WechatGroupMessage, WechatGroupMessageMapper> {
	
}

