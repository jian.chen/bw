/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMsgQueueRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年12月9日下午9:41:27
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatMsgQueueMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueue;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueExp;

/**
 * ClassName:WechatMsgQueueRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午9:41:27 <br/>
 * scrmVersion 1.0
 * 
 * @author yu.zhang
 * @version jdk1.7
 * @see
 */
public interface WechatMsgQueueRepository extends BaseRepository<WechatMsgQueue, WechatMsgQueueMapper> {
	
	public List<WechatMsgQueueExp> findAllWechatMsgQueues(Long limit);

	public int deleteByPrimaryKey(Long wechatMsgQueueId);

	public void deleteAllWechatMsgQueue(List<Long> list);

	public void updateBatchWechatMsgQueue(List<WechatMsgQueueExp> list);
	
	/**
	 * findExpByLimit:(查询为发送的消息条数，根据限制数量). <br/>
	 * Date: 2016-5-6 下午5:37:37 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param start
	 * @param size
	 * @return
	 */
	public List<WechatMsgQueueExp> findExpByLimit(int start,int size);
	
	/**
	 * findAllCount:(查询所有未发送的消息数量). <br/>
	 * Date: 2016-5-6 下午5:39:10 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	public int findAllCount();
	
	
	void updateQueueToY();

	void deleteQueueWithR();
	
}











