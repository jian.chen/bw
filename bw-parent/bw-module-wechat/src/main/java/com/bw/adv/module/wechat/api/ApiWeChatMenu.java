/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WeChatFanAPI.java
 * Package Name:com.sage.scrm.module.wechat.api
 * Date:2015年8月13日下午3:46:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.json.JSONObject;

import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.module.wechat.util.HttpKit;

/**
 * ClassName:WeChatFanAPI <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月13日 下午3:46:30 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Component
public class ApiWeChatMenu {
	
	private AccessTokenCacheService accessTokenCacheService;
	

	public JSONObject menuCreate(String newStr){
		String menuCreateUrl =null;
		// 微信返回的结果
		String result =null;
		JSONObject jsonResult =null;
		try {
			jsonResult =new JSONObject();
			menuCreateUrl =ApiWeChat.MENU_CREATE.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			result = HttpKit.post(menuCreateUrl, newStr);
			jsonResult =JSONObject.fromObject(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonResult;
	}
	
	@Autowired
	public void setAccessTokenCacheService(
			AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}
	
}

