/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFanLabelRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年9月3日下午3:34:53
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.mapper.WechatFanLabelMapper;
import com.bw.adv.module.wechat.model.WechatFanLabel;
import com.bw.adv.module.wechat.model.exp.WechatFanLabelExp;

/**
 * ClassName:WechatFanLabelRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月3日 下午3:34:53 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFanLabelRepository extends BaseRepository<WechatFanLabel, WechatFanLabelMapper> {
	
	/**
	 * 查询粉丝标签
	 * findWechatFanLabelExp:(). <br/>
	 * Date: 2015年9月3日 下午4:13:54 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFansLabelId
	 * @param wechatFansId
	 * @param page 
	 * @return
	 */
	List<WechatFanLabelExp> findWechatFanLabelExp(Long wechatFansLabelId,Long wechatFansId, Page<WechatFanLabelExp> page);
	
	/**
	 * 
//	 * findWechatFanLabelList:(根据粉丝id查询对应的标签). <br/>
	 * Date: 2015年9月3日 下午6:31:38 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFansId
	 * @return
	 */
	List<WechatFanLabelExp> findWechatFanLabelList(Long wechatFansId);

}

