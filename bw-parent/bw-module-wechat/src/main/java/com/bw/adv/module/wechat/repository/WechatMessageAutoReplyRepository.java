/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageAutoReplyRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月19日下午2:12:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;


import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatMessageAutoReplyMapper;
import com.bw.adv.module.wechat.model.WechatMessageAutoReply;
import com.bw.adv.module.wechat.model.exp.WechatMessageAutoReplyExp;

/**
 * ClassName:WechatMessageAutoReplyRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午2:12:21 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMessageAutoReplyRepository extends BaseRepository<WechatMessageAutoReply, WechatMessageAutoReplyMapper> {
	
	WechatMessageAutoReplyExp findWechatMessageAutoReply(String type);
	
	void removeAllWechatMessageAutoReply(String replytype);

}

