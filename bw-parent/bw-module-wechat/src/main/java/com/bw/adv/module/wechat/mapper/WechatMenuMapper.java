package com.bw.adv.module.wechat.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatButton;
import com.bw.adv.module.wechat.model.WechatMenu;

public interface WechatMenuMapper extends BaseMapper<WechatMenu>{

	List<WechatButton> selectAllMenu();

	void deleteAllMenu();
	
	List<WechatMenu> selectAll();
	
	int deleteByParentId(Long pareWechatMenuId);
}