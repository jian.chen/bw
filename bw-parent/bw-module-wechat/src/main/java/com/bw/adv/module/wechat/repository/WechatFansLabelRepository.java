/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansLabelRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月17日上午10:45:12
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatFansLabelMapper;
import com.bw.adv.module.wechat.model.WechatFansLabel;

/**
 * ClassName:WechatFansLabelRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 上午10:45:12 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansLabelRepository extends BaseRepository<WechatFansLabel, WechatFansLabelMapper> {
	
	/**
	 * selectWechatFansLabels:(查询所有有效的标签). <br/>
	 * Date: 2015年8月17日 上午11:50:18 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	List<WechatFansLabel> findWechatFansLabels();
	
	/**
	 * queryWechatFansLabel:(根据标签名称查询标签列表). <br/>
	 * Date: 2015年9月4日 下午5:31:27 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param labelName
	 * @return
	 */
	List<WechatFansLabel> queryWechatFansLabelByLabelName(String labelName);
}

