/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:KeysGroupRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月24日下午5:28:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.KeysGroupMapper;
import com.bw.adv.module.wechat.model.KeysGroup;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.KeysGroupExp;

/**
 * ClassName:KeysGroupRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午5:28:40 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface KeysGroupRepository extends BaseRepository<KeysGroup,KeysGroupMapper> {
	
	/**
	 * findKeysGroupExpBykeysGroup:(查询扩展类). <br/>
	 * Date: 2015年8月26日 下午6:19:58 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param keysGroup
	 * @return
	 */
	KeysGroupExp findKeysGroupExpBykeysGroup(KeysGroup keysGroup);
	
}

