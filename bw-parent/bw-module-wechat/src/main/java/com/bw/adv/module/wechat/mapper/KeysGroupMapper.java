package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.KeysGroup;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.KeysGroupExp;

public interface KeysGroupMapper extends BaseMapper<KeysGroup>{
    int deleteByPrimaryKey(Long keysGroupId);

    int insert(KeysGroup record);

    int insertSelective(KeysGroup record);

    KeysGroup selectByPrimaryKey(Long keysGroupId);

    int updateByPrimaryKeySelective(KeysGroup record);

    int updateByPrimaryKey(KeysGroup record);

	KeysGroup selectKeysGroupByKeysRule(WechatKeysRule keysRule);
	
	/**
	 * findKeysGroupExpBykeysGroup:(查询扩展类). <br/>
	 * Date: 2015年8月26日 下午6:18:19 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param keysGroup
	 * @return
	 */
	KeysGroupExp selectKeysGroupExpBykeysGroup(KeysGroup keysGroup);
}