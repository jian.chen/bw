/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:AccessTokenCacheRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月11日上午11:22:53
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.AccessTokenCacheMapper;
import com.bw.adv.module.wechat.model.AccessTokenCache;
import com.bw.adv.module.wechat.model.exp.AccessTokenCacheExp;
import com.bw.adv.module.wechat.repository.AccessTokenCacheRepository;

/**
 * ClassName:AccessTokenCacheRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 上午11:22:53 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class AccessTokenCacheRepositoryImpl extends 
			BaseRepositoryImpl<AccessTokenCache, AccessTokenCacheMapper> implements AccessTokenCacheRepository {

	@Override
	protected Class<AccessTokenCacheMapper> getMapperClass() {
		return AccessTokenCacheMapper.class;
	}
	
	@Override
	public AccessTokenCache getAccessTokenCache() {
		return this.getMapper().selectAccessTokenCache();
	}

	@Override
	public int insert(AccessTokenCache accessTokenCache) {
		return this.getMapper().insert(accessTokenCache);
	}

	@Override
	public int updateAccessTokenCache(AccessTokenCacheExp accessTokenCacheExp) {
		return this.getMapper().updateAccessTokenCache(accessTokenCacheExp);
	}

}

