/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansGroupItemRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月20日下午4:15:48
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.mapper.WechatFansGroupItemMapper;
import com.bw.adv.module.wechat.model.WechatFansGroupItem;
import com.bw.adv.module.wechat.model.exp.WechatFansGroupItemExp;

/**
 * ClassName:WechatFansGroupItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午4:15:48 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansGroupItemRepository extends BaseRepository<WechatFansGroupItem, WechatFansGroupItemMapper> {
	
	/**
	 * WechatFansGroupItemByGroupId:(根据分组查询粉丝信息). <br/>
	 * Date: 2015年9月1日 下午4:40:11 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param groupId
	 * @return
	 */
	List<WechatFansGroupItemExp> WechatFansGroupItemByGroupId(Long groupId);
	
	/**
	 * 
	 * findWechatFansGroupItemByFansId:(根据粉丝id查询粉丝信息). <br/>
	 * Date: 2015年9月2日 下午4:26:45 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatId
	 * @return
	 */
	WechatFansGroupItem findWechatFansGroupItemByFansId(Long wechatId);
	
	/**
	 * findWechatFansGroupItemsByGroupId:(根据分组id查询分组关系). <br/>
	 * Date: 2015年12月27日 下午5:04:31 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatGroupId
	 * @return
	 */
	List<WechatFansGroupItem> findWechatFansGroupItemsByGroupId(Long wechatGroupId);
	
	
}

