package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatGroupMessage;

public interface WechatGroupMessageMapper extends BaseMapper<WechatGroupMessage>{
    int deleteByPrimaryKey(Long wechatGroupMessageId);

    int insert(WechatGroupMessage record);

    int insertSelective(WechatGroupMessage record);

    WechatGroupMessage selectByPrimaryKey(Long wechatGroupMessageId);

    int updateByPrimaryKeySelective(WechatGroupMessage record);

    int updateByPrimaryKey(WechatGroupMessage record);
}