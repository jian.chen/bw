package com.bw.adv.module.wechat.mapper;

import com.bw.adv.module.wechat.model.WechatFansFriend;

public interface WechatFansFriendMapper {
    int deleteByPrimaryKey(Long wechatFansFriendId);

    int insert(WechatFansFriend record);

    int insertSelective(WechatFansFriend record);

    WechatFansFriend selectByPrimaryKey(Long wechatFansFriendId);

    int updateByPrimaryKeySelective(WechatFansFriend record);

    int updateByPrimaryKey(WechatFansFriend record);
}