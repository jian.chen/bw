/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:AccessTokenCache.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月11日上午11:16:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.AccessTokenCacheMapper;
import com.bw.adv.module.wechat.model.AccessTokenCache;
import com.bw.adv.module.wechat.model.exp.AccessTokenCacheExp;
/**
 * ClassName:AccessTokenCache <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 上午11:16:46 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface AccessTokenCacheRepository extends BaseRepository<AccessTokenCache, AccessTokenCacheMapper>{
	
	/**
	 * 获取有效的accessToken
	 * getAccessTokenCache:(获取有效的accessToken). <br/>
	 * Date: 2015年8月11日 上午11:18:46 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	public AccessTokenCache getAccessTokenCache();
	
	/**
	 * 新增
	 * insert:(新增). <br/>
	 * Date: 2015年8月11日 上午11:20:46 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param accessTokenCache
	 * @return
	 */
	int insert(AccessTokenCache accessTokenCache);
	
	/**
	 * 更新
	 * Date: 2015年8月11日 下午2:30:12 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param accessTokenCache
	 * @return
	 */
	int updateAccessTokenCache(AccessTokenCacheExp accessTokenCacheExp);
	
}

