/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFanLabelRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年9月3日下午3:35:38
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.mapper.WechatFanLabelMapper;
import com.bw.adv.module.wechat.model.WechatFanLabel;
import com.bw.adv.module.wechat.model.exp.WechatFanLabelExp;
import com.bw.adv.module.wechat.repository.WechatFanLabelRepository;

/**
 * ClassName:WechatFanLabelRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月3日 下午3:35:38 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatFanLabelRepositoryImpl extends BaseRepositoryImpl<WechatFanLabel, WechatFanLabelMapper> implements WechatFanLabelRepository {
	@Override
	protected Class<WechatFanLabelMapper> getMapperClass() {
		return WechatFanLabelMapper.class;
	}

	@Override
	public List<WechatFanLabelExp> findWechatFanLabelExp(Long wechatFansLabelId, Long wechatFansId,Page<WechatFanLabelExp> page ) {
		return this.getMapper().selectWechatFanLabelExpList(wechatFansLabelId,wechatFansId,page);
	}

	@Override
	public List<WechatFanLabelExp> findWechatFanLabelList(Long wechatFansId) {
		return this.getMapper().selectWechatFanLabelList(wechatFansId);
	}
	
	
}

