/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:ChannelQrCodeRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年11月26日下午6:40:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.ChannelQrCodeMapper;
import com.bw.adv.module.wechat.model.ChannelQrCode;

/**
 * ClassName:ChannelQrCodeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 下午6:40:34 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface ChannelQrCodeRepository extends BaseRepository<ChannelQrCode, ChannelQrCodeMapper> {

	/**
	 * 
	 * findEffectiveList:(查询所有有效的二维码渠道). <br/>
	 * Date: 2015年11月26日 下午6:41:45 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<ChannelQrCode> findEffectiveList();
	
}

