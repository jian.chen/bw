/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WeChatAPI.java
 * Package Name:com.sage.scrm.module.wechat.common
 * Date:2015年8月11日上午10:41:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.api;

import org.springframework.beans.factory.annotation.Value;

/**
 * 微信API
 * ClassName:WeChatAPI <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 上午10:41:09 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class ApiWeChat {
	
	/*public static final String appId = "wxdec754363a0f1e5c";
	public static final String appSecret = "a44e1db30eb89e3f718f9abd788104ea";*/
	
	public static WeChatConfig WECHAT_CONFIG;
	@Value("${appId}")
	private String appId;
	@Value("${appSecret}")
	private String appSecret ;
	
	/**
	 * 通过appid和secret获取accessToken 每日限额2000次    get方式
	 */
	public static String ACCESS_TOKEN =
			"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	/**
	 * 指定用户设置备注名    post方式
	 */
	public static String UPDATE_FANREMARK =
			"https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=ACCESS_TOKEN";
	/**
	 * 本接口来获取帐号的关注者列表,关注者列表由一串OpenID组成,一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。get方式
	 */
	public static String GET_WECHATFANS =
			"https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID";
	/**
	 * OpenID来获取用户基本信息  get方式
	 */
	public static String GET_WECHATFANINFO =
			"https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
	
	/**
	 * 创建分组  每个公众帐号最多支持100个分组  post方式
	 */
	public static String CREATE_GROUP= 
			"https://api.weixin.qq.com/cgi-bin/groups/create?access_token=ACCESS_TOKEN";
	
	/**
	 * 批量获取用户基本信息。最多支持一次拉取100条  post方式
	 */
	public static String BATCHGET_FANSINFO= 
			"https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN";
	/**
	 * 查询所有分组 get方式
	 */
	public static String GET_GROUP=
			"https://api.weixin.qq.com/cgi-bin/groups/get?access_token=ACCESS_TOKEN";
	
	/**
	 * 修改分组名  post方式
	 */
	public static String UPDATE_GROUP=
			"https://api.weixin.qq.com/cgi-bin/groups/update?access_token=ACCESS_TOKEN";
	
	/**
	 * 删除分组,删除分组后，所有该分组内的用户自动进入默认分组  post方式
	 */
	public static String DELETE_GROUP=
			"https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=ACCESS_TOKEN";
	
	/**
	 * 移动用户分组  post方式
	 */
	public static String UPDATE_FAN_GROUP=
			"https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=ACCESS_TOKEN";
	/**
	 * 批量移动用户分组  post方式  size不能超过50
	 */
	public static String BATCH_UPDATE_FANS_GROUP=
			"https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=ACCESS_TOKEN";
	/**
	 * 上传临时素材接口   post方式
	 */
	public static String UPLOAD_TEMP_MEDIA_URL=
			"https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";
	/**
	 * 新增永久图文素材
	 */
	public static String UPLOAD_FORVER_MEDIA_URL=
			"https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN";
	/**
	 * 上传图文消息素材
	 */
	public static String UPLOAD_IMAGE_TEXT_MEDIA_URL=
			"https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=ACCESS_TOKEN";
	/**
	 * 根据opendId列表群发   post方式
	 */
	public static String SEND_MESSAGE_BY_OPENIDS=
			"https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=ACCESS_TOKEN";
	/**
	 * 根据分组列表群发   post方式
	 */
	public static String SEND_MESSAGE_BY_GROUP=
			"https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN";	
	/**
	 * 预览接口  post方式
	 */
	public static String SEND_PREVIEW_MESSAGE=
			"https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=ACCESS_TOKEN";
	
	/**
	 * 自动回复规则    get方式
	 */
	public static String GET_AUTOREPLY_INFO=
			"https://api.weixin.qq.com/cgi-bin/get_current_autoreply_info?access_token=ACCESS_TOKEN";
	
	/**
	 * 客服接口-发消息   post方式
	 */
	public static String SEND_CUSTOM_MESSAGE=
			"https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
	
	/**
	 * 发送模板消息 	post方式
	 */
	public static String SEND_TEMPLATE=
			"https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
	
	/**
	 * 获取临时素材  get方式
	 */
	public static String DOWNLOAD_TEMP_MATERIAL=
			"https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
	
	/**
	 * 微信菜单发布接口
	 */
	public static String MENU_CREATE=
			"https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	
	/**
	 * 微信长链接转短链接
	 */
	public static String LONG_TO_SHORT = 
			"https://api.weixin.qq.com/cgi-bin/shorturl?access_token=ACCESS_TOKEN";
	
	public static class WeChatConfig {
		private String appId;
		private String appSecret;

		public WeChatConfig(String appId, String appSecret) {
			this.appId = appId;
			this.appSecret = appSecret;
		}

		public String getAppId() {
			return appId;
		}

		public void setAppId(String appId) {
			this.appId = appId;
		}

		public String getAppSecret() {
			return appSecret;
		}

		public void setAppSecret(String appSecret) {
			this.appSecret = appSecret;
		}
	}
	
}

