package com.bw.adv.module.wechat.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.model.WechatFansGroupItem;
import com.bw.adv.module.wechat.model.exp.WechatFansGroupItemExp;

public interface WechatFansGroupItemMapper extends BaseMapper<WechatFansGroupItem>{
    int deleteByPrimaryKey(Long wechatFansGroupItemId);

    int insert(WechatFansGroupItem record);

    int insertSelective(WechatFansGroupItem record);

    WechatFansGroupItem selectByPrimaryKey(Long wechatFansGroupItemId);

    int updateByPrimaryKeySelective(WechatFansGroupItem record);

    int updateByPrimaryKey(WechatFansGroupItem record);
    
    /**
     * selectWechatFansByGroupId:(根据微信分组id查询微信粉丝列表). <br/>
     * Date: 2015年9月1日 下午4:30:39 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param wechatFansGroupId
     * @return
     */
    List<WechatFansGroupItemExp> selectWechatFansByGroupId(Long wechatFansGroupId);
    
    /**
     * selectWechatFansGroupItemByFansId:(根据粉丝id查询信息). <br/>
     * Date: 2015年9月2日 下午4:29:20 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param wechatId
     * @return
     */
	WechatFansGroupItem selectWechatFansGroupItemByFansId(Long wechatId);
	
	/**
	 * selectWechatFansGroupItemsByGroupId:(根据分组id查询分组关系). <br/>
	 * Date: 2015年12月27日 下午5:05:41 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatGroupId
	 * @return
	 */
	List<WechatFansGroupItem> selectWechatFansGroupItemsByGroupId(@Param("wechatGroupId")Long wechatGroupId);
}