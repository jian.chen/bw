/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansMessageRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月25日下午5:00:14
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatFansMessageMapper;
import com.bw.adv.module.wechat.model.WechatFansMessage;

/**
 * ClassName:WechatFansMessageRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午5:00:14 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansMessageRepository extends BaseRepository<WechatFansMessage, WechatFansMessageMapper> {

}

