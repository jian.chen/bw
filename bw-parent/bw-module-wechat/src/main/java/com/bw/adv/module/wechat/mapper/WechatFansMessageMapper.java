package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatFansMessage;

public interface WechatFansMessageMapper extends BaseMapper<WechatFansMessage>{
    int deleteByPrimaryKey(Long wechatFansMessageId);

    int insert(WechatFansMessage record);

    int insertSelective(WechatFansMessage record);

    WechatFansMessage selectByPrimaryKey(Long wechatFansMessageId);

    int updateByPrimaryKeySelective(WechatFansMessage record);

    int updateByPrimaryKey(WechatFansMessage record);
}