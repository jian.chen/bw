package com.bw.adv.module.wechat.mapper;


import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;

public interface WechatMessageTemplateMapper extends BaseMapper<WechatMessageTemplate>{
    int deleteByPrimaryKey(Long wechatMessageTemplateId);

    int insert(WechatMessageTemplate record);

    int insertSelective(WechatMessageTemplate record);

    WechatMessageTemplate selectByPrimaryKey(Long wechatMessageTemplateId);

    int updateByPrimaryKeySelective(WechatMessageTemplate record);

    int updateByPrimaryKey(WechatMessageTemplate record);
    
    /**
     * 
     * selectParentImageText:(查询父级的图文消息以及单图文消息). <br/>
     * Date: 2015年9月10日 下午3:12:26 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @return
     */
	List<WechatMessageTemplate> selectParentImageText();
	
	/**
	 * selectSubImageTextList:(). <br/>
	 * Date: 2015年9月10日 下午3:44:53 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatMessageTemplateId
	 * @return
	 */
	List<WechatMessageTemplate> selectSubImageTextList(String wechatMessageTemplateId);
    
}