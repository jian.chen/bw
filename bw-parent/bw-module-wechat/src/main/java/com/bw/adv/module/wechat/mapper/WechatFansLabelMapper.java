package com.bw.adv.module.wechat.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatFansLabel;

public interface WechatFansLabelMapper extends BaseMapper<WechatFansLabel>{
    int deleteByPrimaryKey(Long wechatFansLabelId);

    int insert(WechatFansLabel record);

    int insertSelective(WechatFansLabel record);

    WechatFansLabel selectByPrimaryKey(Long wechatFansLabelId);

    int updateByPrimaryKeySelective(WechatFansLabel record);

    int updateByPrimaryKey(WechatFansLabel record);
    
    /**
     * selectWechatFansLabels:(查询所有有效的标签). <br/>
     * Date: 2015年8月17日 上午11:49:12 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @return
     */
    List<WechatFansLabel> selectWechatFansLabels();
    
    /**
     * selectWechatFansLabelByLableName:(根据粉丝标签名称查询标签列表). <br/>
     * Date: 2015年9月4日 下午5:37:04 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param labelName
     * @return
     */
	List<WechatFansLabel> selectWechatFansLabelByLableName(String labelName);
}