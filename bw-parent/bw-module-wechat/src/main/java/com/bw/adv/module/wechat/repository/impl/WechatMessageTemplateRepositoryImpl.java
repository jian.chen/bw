/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageTemplateRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月18日下午4:44:43
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMessageTemplateMapper;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.repository.WechatMessageTemplateRepository;

/**
 * ClassName:WechatMessageTemplateRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:44:43 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMessageTemplateRepositoryImpl extends BaseRepositoryImpl<WechatMessageTemplate, WechatMessageTemplateMapper> implements WechatMessageTemplateRepository {
	@Override
	protected Class<WechatMessageTemplateMapper> getMapperClass() {
		return WechatMessageTemplateMapper.class;
	}
	
	@Override
	public List<WechatMessageTemplate> findParentImageText() {
		return this.getMapper().selectParentImageText();
	}

	@Override
	public List<WechatMessageTemplate> findSubImageTextList(String wechatMessageTemplateId) {
		return this.getMapper().selectSubImageTextList(wechatMessageTemplateId);
	}
	
	
}

