package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatMessageGroupItem;

public interface WechatMessageGroupItemMapper extends BaseMapper<WechatMessageGroupItem>{
    int deleteByPrimaryKey(Long wechatMessageGroupItemId);

    int insert(WechatMessageGroupItem record);

    int insertSelective(WechatMessageGroupItem record);

    WechatMessageGroupItem selectByPrimaryKey(Long wechatMessageGroupItemId);

    int updateByPrimaryKeySelective(WechatMessageGroupItem record);

    int updateByPrimaryKey(WechatMessageGroupItem record);
}