/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:ChannelQrCodeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年11月26日下午6:42:20
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.ChannelQrCodeMapper;
import com.bw.adv.module.wechat.model.ChannelQrCode;
import com.bw.adv.module.wechat.repository.ChannelQrCodeRepository;

/**
 * ClassName:ChannelQrCodeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 下午6:42:20 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ChannelQrCodeRepositoryImpl extends BaseRepositoryImpl<ChannelQrCode, ChannelQrCodeMapper>
	implements ChannelQrCodeRepository{
	
	@Override
	protected Class<ChannelQrCodeMapper> getMapperClass() {
		
		return ChannelQrCodeMapper.class;
	}

	@Override
	public List<ChannelQrCode> findEffectiveList() {
		
		return this.getMapper().selectEffectiveList();
	}


}

