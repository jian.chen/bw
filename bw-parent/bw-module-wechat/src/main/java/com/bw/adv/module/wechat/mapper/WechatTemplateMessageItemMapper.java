package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatTemplateMessageItem;

public interface WechatTemplateMessageItemMapper  extends BaseMapper<WechatTemplateMessageItem>   {
    int deleteByPrimaryKey(Long templateMessItemId);

    int insert(WechatTemplateMessageItem record);

    int insertSelective(WechatTemplateMessageItem record);

    WechatTemplateMessageItem selectByPrimaryKey(Long templateMessItemId);

    int updateByPrimaryKeySelective(WechatTemplateMessageItem record);

    int updateByPrimaryKey(WechatTemplateMessageItem record);
    
    WechatTemplateMessageItem selectWechatTemplateMessageItemByTemplateMessId(Long templateMessId);
    
}