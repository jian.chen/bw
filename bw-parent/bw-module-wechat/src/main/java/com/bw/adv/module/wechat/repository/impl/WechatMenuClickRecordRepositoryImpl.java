/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMenuClickRecordRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年12月9日下午3:56:20
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMenuClickRecordMapper;
import com.bw.adv.module.wechat.model.WechatMenuClickRecord;
import com.bw.adv.module.wechat.repository.WechatMenuClickRecordRepository;

/**
 * ClassName:WechatMenuClickRecordRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午3:56:20 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMenuClickRecordRepositoryImpl extends BaseRepositoryImpl<WechatMenuClickRecord, WechatMenuClickRecordMapper> implements WechatMenuClickRecordRepository{

	@Override
	protected Class<WechatMenuClickRecordMapper> getMapperClass() {
		return WechatMenuClickRecordMapper.class;
	}

}

