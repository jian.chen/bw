/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageGroupItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月18日下午4:35:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMessageGroupItemMapper;
import com.bw.adv.module.wechat.model.WechatMessageGroupItem;
import com.bw.adv.module.wechat.repository.WechatMessageGroupItemRepository;

/**
 * ClassName:WechatMessageGroupItemRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:35:51 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMessageGroupItemRepositoryImpl extends BaseRepositoryImpl<WechatMessageGroupItem, WechatMessageGroupItemMapper> implements WechatMessageGroupItemRepository {

	@Override
	protected Class<WechatMessageGroupItemMapper> getMapperClass() {
		return WechatMessageGroupItemMapper.class;
	}


}

