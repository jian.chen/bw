/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageTemplateRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月18日下午4:44:43
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;


import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatTemplateMessageItemMapper;
import com.bw.adv.module.wechat.model.WechatTemplateMessageItem;
import com.bw.adv.module.wechat.repository.WechatTemplateMessageItemRepository;

@Repository
public class WechatTemplateMessageItemRepositoryImpl extends BaseRepositoryImpl<WechatTemplateMessageItem, WechatTemplateMessageItemMapper> implements WechatTemplateMessageItemRepository {
	@Override
	protected Class<WechatTemplateMessageItemMapper> getMapperClass() {
		return WechatTemplateMessageItemMapper.class;
	}

	@Override
	public WechatTemplateMessageItem findWechatTemplateMessageItemByTemplateMessId(
			Long templateMessId) {
		return this.getMapper().selectWechatTemplateMessageItemByTemplateMessId(templateMessId);
	}

	

}

