/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatConstant.java
 * Package Name:com.sage.scrm.module.wechat.common
 * Date:2015年8月7日上午10:10:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.common;
/**
 * ClassName:WechatConstant <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月7日 上午10:10:39 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WechatConstant {
	// get请求
	public static String GET_TYPE="get";
	// post请求
	public static String POST_TYPE="post";
	// 日期转换常量
	public static Long RADIO=1000L;
	
}

