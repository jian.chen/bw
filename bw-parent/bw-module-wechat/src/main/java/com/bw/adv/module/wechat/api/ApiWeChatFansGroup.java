/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:WeChatFansGroupAPI.java
 * Package Name:com.sage.scrm.module.wechat.api
 * Date:2015年8月12日下午4:10:53
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.sys.model.SysOrg;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.module.wechat.util.HttpKit;

/**
 * ClassName:WeChatFansGroupAPI <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月12日 下午4:10:53 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Component
public class ApiWeChatFansGroup {
	
	protected final Logger logger = Logger.getLogger(ApiWeChatFansGroup.class);
	private AccessTokenCacheService accessTokenCacheService;
	/**
	 * createWeChatGroup:(微信新建分组). <br/>
	 * Date: 2015年8月12日 下午4:11:59 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 * @throws IOException 
	 */
	public Map<String,String> createWeChatGroup(String name){
		
		//创建分组接口
		String createWeChatGroupUrl =null;
		//微信返回的响应数据
		String result =null;
		//转换后的结果
		JSONObject resultJsonObject =null;
		//分组信息
		String group =null;
		//分组ID
		String groupId =null;
		//分组名称
		String groupName =null;
		// 返回结果
		Map<String,String> resultMap =null;
		//拼装数据
		JSONObject jsonObject =null;
		JSONObject subJonsObject =null;
		String code =null;
		try {
			resultMap = new HashMap<String,String>();
			jsonObject = new JSONObject();
			subJonsObject = new JSONObject();
			createWeChatGroupUrl = ApiWeChat.CREATE_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			subJonsObject.put("name", name);
			jsonObject.put("group", subJonsObject);
			
			result = HttpKit.post(createWeChatGroupUrl, jsonObject.toString());
			resultJsonObject = JSONObject.fromObject(result);
				
			group = resultJsonObject.getString("group");
			JSONObject groupJson = JSONObject.fromObject(group);
			code =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
			groupId =groupJson.getString("id");
			groupName =groupJson.getString("name");
			
		} catch (Exception e) {
			code =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			logger.error(e.getMessage());
		} finally{
			resultMap.put("id", groupId);
			resultMap.put("name", groupName);
			resultMap.put("code", code);
		}
		return resultMap;
	}
	
	/**
	 * queryWeChetGroups:(查询所有分组). <br/>
	 * Date: 2015年8月13日 上午10:53:49 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	public List<Map<String,String>> queryWeChetGroups(){
		
		String queryGroupsUrl =null;
		String result =null;
		//转换后的结果
		JSONObject resultJsonObject =null;//分组信息
		//公众平台分组信息列表
		JSONArray groups =null;
		// 所有分组结果集 
		List<Map<String,String>> resultMap =null;
		try {
			resultMap = new ArrayList<Map<String,String>>();
			queryGroupsUrl = ApiWeChat.GET_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			result = HttpKit.get(queryGroupsUrl);
			resultJsonObject = JSONObject.fromObject(result);
			groups = resultJsonObject.getJSONArray("groups");
			for(int i=0;i<groups.size();i++){
				Map<String,String> map =new HashMap<String,String>();
				JSONObject jsonObject = (JSONObject)groups.get(i);
				// 分组id，由微信分配
				map.put("id", jsonObject.getString("id"));
				// 分组名字，UTF8编码
				map.put("name", jsonObject.getString("name"));
				// 分组内用户数量
				map.put("count", jsonObject.getString("count"));
				resultMap.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return resultMap;
	}
	
	/**
	 * updateWeChatGroup:(修改分组名). <br/>
	 * Date: 2015年8月13日 上午11:52:11 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param jsonObject
	 * @return
	 */
	public Map<String,String> updateWeChatGroup(String id,String name){
		String updateGroupUrl =null;
		// 微信返回的结果
		String result =null;
		// json类型的结果
		JSONObject resultObject =null;
		// 状态码
		String errcode =null;
		// 状态描述
		String errmsg =null;
		String code =null;
		Map<String,String> resultMap =null;
		//拼装数据
		JSONObject jsonObject =null;
		JSONObject subJsonObject =null ;
		try {
			resultMap =new HashMap<String,String>();
			jsonObject = new JSONObject();
			subJsonObject =new JSONObject();
			updateGroupUrl = ApiWeChat.UPDATE_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			subJsonObject.put("id", id);
			subJsonObject.put("name", name);
			jsonObject.put("group", subJsonObject);
			result =HttpKit.post(updateGroupUrl, jsonObject.toString());
			resultObject =JSONObject.fromObject(result);
			code =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
			errcode =resultObject.getString("errcode");
			errmsg =resultObject.getString("errmsg");
		} catch (Exception e) {
			code =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			logger.error(e.getMessage());
		}finally{
			resultMap.put("code", code);
			resultMap.put("errcode", errcode);
			resultMap.put("errmsg", errmsg);
		}
		return resultMap;
	}
	
	/**
	 * deleteWeChatGroup:(删除分组). <br/>
	 * Date: 2015年8月13日 下午1:57:10 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param id
	 * @return
	 */
	public Map<String,String> deleteWeChatGroup(String id){
		String deleteGroupUrl =null;
		// 微信返回的结果
		String result =null;
		// json类型的结果
		JSONObject resultObject =null;
		// 状态码
		String errcode =null;
		// 状态描述
		String errmsg =null; 
		Map<String,String> resultMap =null;
		//拼装数据
		JSONObject jsonObject =null;
		JSONObject subJsonObject =null;
		
		String code =null;
		try {
			resultMap = new HashMap<String,String>();
			jsonObject = new JSONObject();
			subJsonObject =new JSONObject();
			deleteGroupUrl =ApiWeChat.DELETE_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			subJsonObject.put("id", Long.valueOf(id));
			jsonObject.put("group", subJsonObject);
			result = HttpKit.post(deleteGroupUrl, jsonObject.toString());
			code =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
			/*errcode = resultObject.getString("errcode")+"";
			errmsg = resultObject.getString("errmsg");*/
			
		} catch (Exception e) {
			code =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			logger.error(e.getMessage());
		}finally{
			resultMap.put("code", code);
			/*resultMap.put("errcode", errcode);
			resultMap.put("errmsg", errmsg);*/
		}
		return resultMap;
	}
	
	/**
	 * updateFanGroup:(移动用户分组). <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param openId
	 * @param toGroupId
	 * @return
	 */
	public Map<String,String> updateFanGroup(String openId,String toGroupId){
		String updateFanGroupUrl =null;
		// 微信返回的结果
		String result =null;
		// json类型的结果
		JSONObject resultObject =null;
		// 状态码
		String errcode =null;
		// 状态描述
		String errmsg =null; 
		Map<String,String> resultMap =null;
		//数据拼装
		JSONObject jsonObject =null;
		try {
			jsonObject =new JSONObject();
			resultMap =new HashMap<String,String>();
			jsonObject.put("openid", openId);
			jsonObject.put("to_groupid", toGroupId);
			updateFanGroupUrl = ApiWeChat.UPDATE_FAN_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			result = HttpKit.post(updateFanGroupUrl, jsonObject.toString());
			resultObject = JSONObject.fromObject(result);
			errcode = resultObject.getString("errcode");
			errmsg = resultObject.getString("errmsg");
			
			resultMap.put("errcode", errcode);
			resultMap.put("errmsg", errmsg);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	
	/**
	 * batchUpdateFansGroup:("批量移动用户分组"). <br/>
	 * Date: 2015年8月14日 下午12:18:04 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param openId
	 * @param toGroupId
	 * @return
	 */
	public Map<String,String> batchUpdateFansGroup(JSONArray openIds,String toGroupId){
		String updateFanGroupUrl =null;
		// 微信返回的结果
		String result =null;
		// json类型的结果
		JSONObject resultObject =null;
		// 状态码
		String errcode =null;
		// 状态描述
		String errmsg =null; 
		Map<String,String> resultMap =null;
		//数据拼装
		JSONObject jsonObject =null;
		try {
			jsonObject =new JSONObject();
			resultMap =new HashMap<String,String>();
			jsonObject.put("openid_list", openIds);
			jsonObject.put("to_groupid", Long.valueOf(toGroupId));
			updateFanGroupUrl = ApiWeChat.BATCH_UPDATE_FANS_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			System.out.println(jsonObject.toString());
			result = HttpKit.post(updateFanGroupUrl, jsonObject.toString());
			resultObject = JSONObject.fromObject(result);
			errcode = resultObject.getString("errcode");
			errmsg = resultObject.getString("errmsg");
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			resultMap.put("errcode", errcode);
			resultMap.put("errmsg", errmsg);
		}
		return resultMap;
	}
	
	@Autowired
	public void setAccessTokenCacheService(
			AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}
}

