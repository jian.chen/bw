/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:AccessTicketCacheRepositoryImpl.java
 * Package Name:com.sage.scrm.bk.wechat.repository.impl
 * Date:2015年11月17日下午3:36:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.AccessTicketCacheMapper;
import com.bw.adv.module.wechat.model.AccessTicketCache;
import com.bw.adv.module.wechat.model.exp.AccessTicketCacheExp;
import com.bw.adv.module.wechat.repository.AccessTicketCacheRepository;

/**
 * ClassName:AccessTicketCacheRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月17日 下午3:36:21 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class AccessTicketCacheRepositoryImpl extends BaseRepositoryImpl<AccessTicketCache, AccessTicketCacheMapper> implements AccessTicketCacheRepository{

	@Override
	public AccessTicketCache getAccessTicketCache() {
		return this.getMapper().getAccessTicketCache();
	}

	@Override
	public int updateByPrimaryKey(AccessTicketCacheExp record) {
		return this.getMapper().updateByPrimaryKey(record);
	}

	@Override
	protected Class<AccessTicketCacheMapper> getMapperClass() {
		return AccessTicketCacheMapper.class;
	}

}

