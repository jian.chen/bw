package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.AccessTokenCache;
import com.bw.adv.module.wechat.model.exp.AccessTokenCacheExp;

public interface AccessTokenCacheMapper extends BaseMapper<AccessTokenCache>{
	
    int deleteByPrimaryKey(Long accessTokenCacheId);

    int insert(AccessTokenCache record);

    int insertSelective(AccessTokenCache record);

    AccessTokenCache selectByPrimaryKey(Long accessTokenCacheId);
    
    AccessTokenCache selectAccessTokenCache();

    int updateAccessTokenCache(AccessTokenCacheExp accessTokenCacheExp);

    int updateByPrimaryKey(AccessTokenCache record);
}