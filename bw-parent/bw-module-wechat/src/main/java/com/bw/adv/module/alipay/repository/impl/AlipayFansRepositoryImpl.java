/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-alipay-service-window
 * File Name:AlipayFansRepositoryImpl.java
 * Package Name:com.sage.scrm.alipay.repository.impl
 * Date:2016年4月5日下午4:32:12
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.alipay.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.alipay.mapper.AlipayFansMapper;
import com.bw.adv.module.alipay.model.AlipayFans;
import com.bw.adv.module.alipay.repository.AlipayFansRepository;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName:AlipayFansRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年4月5日 下午4:32:12 <br/>
 * scrmVersion 1.0
 * @author   sherry.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class AlipayFansRepositoryImpl extends BaseRepositoryImpl<AlipayFans, AlipayFansMapper> 
	implements AlipayFansRepository{

	@Override
	protected Class<AlipayFansMapper> getMapperClass() {
		
		return AlipayFansMapper.class;
	}

	@Override
	public AlipayFans findByUserId(String userId) {
		
		return this.getMapper().selectByUserId(userId);
	}

	@Override
	public List<AlipayFans> findByExample(Example example, Page<AlipayFans> page) {
		
		return this.getMapper().selectByExample(example,page);
	}


}

