/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMsgQueueHistoryRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年12月9日下午9:39:17
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMsgQueueHistoryMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueueHistory;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHistoryRepository;

/**
 * ClassName:WechatMsgQueueHistoryRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午9:39:17 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMsgQueueHistoryRepositoryImpl extends BaseRepositoryImpl<WechatMsgQueueHistory, WechatMsgQueueHistoryMapper> implements WechatMsgQueueHistoryRepository{

	@Override
	protected Class<WechatMsgQueueHistoryMapper> getMapperClass() {
		return WechatMsgQueueHistoryMapper.class;
	}
	
	public void saveAllWechatMsgQueueHistory(List<WechatMsgQueueHistory> historys){
		this.getMapper().insertBatch(historys);
	}
}

