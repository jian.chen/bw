package com.bw.adv.module.wechat.common.enums;

import org.apache.commons.lang.StringUtils;

/**
 * Created by jeoy.zhou on 3/11/16.
 */
public enum  WechatAccountConfigStatus {

    CONNECTED("1", "已经连接"),
    NOT_CONNECTED("2", "未连接");

    private String key;
    private String value;

    private WechatAccountConfigStatus(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getMsg(String key) {
        for (WechatAccountConfigStatus status : WechatAccountConfigStatus.values()) {
            if (status.getKey().equals(key)) {
                return status.getValue();
            }
        }
        return StringUtils.EMPTY;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
