package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatMenuClickRecord;

public interface WechatMenuClickRecordMapper extends BaseMapper<WechatMenuClickRecord>{
    int deleteByPrimaryKey(Long wechatMenuClickRecordId);

    int insert(WechatMenuClickRecord record);

    int insertSelective(WechatMenuClickRecord record);

    WechatMenuClickRecord selectByPrimaryKey(Long wechatMenuClickRecordId);

    int updateByPrimaryKeySelective(WechatMenuClickRecord record);

    int updateByPrimaryKey(WechatMenuClickRecord record);
}