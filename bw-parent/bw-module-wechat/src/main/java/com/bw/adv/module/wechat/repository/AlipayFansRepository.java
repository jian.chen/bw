/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-alipay-service-window
 * File Name:AlipayFansRepository.java
 * Package Name:com.sage.scrm.alipay.repository
 * Date:2016年4月5日下午4:30:14
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.alipay.mapper.AlipayFansMapper;
import com.bw.adv.module.alipay.model.AlipayFans;

/**
 * ClassName:AlipayFansRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年4月5日 下午4:30:14 <br/>
 * scrmVersion 1.0
 * @author   sherry.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface AlipayFansRepository extends BaseRepository<AlipayFans, AlipayFansMapper> {
	
	/**
	 * 
	 * findByUserId:(根据支付宝userId查询支付宝粉丝). <br/>
	 * Date: 2016年4月6日 上午11:21:13 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param userId
	 * @return
	 */
	AlipayFans findByUserId(String userId);
	
}

