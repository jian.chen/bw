/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:AccessTicketCacheService.java
 * Package Name:com.sage.scrm.bk.wechat.service
 * Date:2015年11月17日下午3:38:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.AccessTicketCache;
import com.bw.adv.module.wechat.model.exp.AccessTicketCacheExp;

/**
 * ClassName:AccessTicketCacheService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月17日 下午3:38:50 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface AccessTicketCacheService extends BaseService<AccessTicketCache>{
	public AccessTicketCache getAccessTicketCache();
	public int updateByPrimaryKey(AccessTicketCacheExp record); 
}

