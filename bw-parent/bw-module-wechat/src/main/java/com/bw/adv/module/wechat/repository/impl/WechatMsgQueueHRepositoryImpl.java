/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMsgQueueHRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年12月16日上午11:07:12
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMsgQueueHMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueHExp;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;

/**
 * ClassName:WechatMsgQueueHRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月16日 上午11:07:12 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMsgQueueHRepositoryImpl extends BaseRepositoryImpl<WechatMsgQueueH, WechatMsgQueueHMapper> implements WechatMsgQueueHRepository{
	@Override
	public List<WechatMsgQueueHExp> findAllWechatMsgQueueHs(Long limit) {
		return this.getMapper().findAllWechatMsgQueueHs(limit);
	}

	@Override
	public void deleteAllWechatMsgQueueH(List<Long> ids) {
		this.getMapper().deleteBatch(ids);
	}

	@Override
	protected Class<WechatMsgQueueHMapper> getMapperClass() {
		return WechatMsgQueueHMapper.class;
	}
	
	@Override
	public void updateBatchWechatMsgQueueH(List<WechatMsgQueueHExp> list){
		this.getMapper().updateBatchWechatMsgQueueH(list);
	}

	@Override
	public void updateQueueToY() {
		this.getMapper().updateQueueToY();
	}

	@Override
	public void deleteQueueWithR() {
		this.getMapper().deleteQueueWithR();
	}
}

