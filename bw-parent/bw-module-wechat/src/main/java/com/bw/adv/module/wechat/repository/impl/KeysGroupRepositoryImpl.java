/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:KeysGroupRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月24日下午5:29:23
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.KeysGroupMapper;
import com.bw.adv.module.wechat.model.KeysGroup;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.KeysGroupExp;
import com.bw.adv.module.wechat.repository.KeysGroupRepository;

/**
 * ClassName:KeysGroupRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午5:29:23 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class KeysGroupRepositoryImpl extends BaseRepositoryImpl<KeysGroup,KeysGroupMapper> implements KeysGroupRepository {

	@Override
	protected Class<KeysGroupMapper> getMapperClass() {
		return KeysGroupMapper.class;
	}

	@Override
	public KeysGroupExp findKeysGroupExpBykeysGroup(KeysGroup keysGroup) {
		return this.getMapper().selectKeysGroupExpBykeysGroup(keysGroup);
	}
	
	
	
	
}

