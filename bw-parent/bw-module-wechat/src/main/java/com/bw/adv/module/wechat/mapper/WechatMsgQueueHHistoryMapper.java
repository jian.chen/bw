package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueueHHistory;

public interface WechatMsgQueueHHistoryMapper extends BaseMapper<WechatMsgQueueHHistory>{
    int deleteByPrimaryKey(Long wechatMsgQueueId);

    int insert(WechatMsgQueueHHistory record);

    int insertSelective(WechatMsgQueueHHistory record);

    WechatMsgQueueHHistory selectByPrimaryKey(Long wechatMsgQueueId);

    int updateByPrimaryKeySelective(WechatMsgQueueHHistory record);

    int updateByPrimaryKeyWithBLOBs(WechatMsgQueueHHistory record);

    int updateByPrimaryKey(WechatMsgQueueHHistory record);
}