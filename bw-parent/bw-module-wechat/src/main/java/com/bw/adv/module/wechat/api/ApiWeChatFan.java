/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WeChatFanAPI.java
 * Package Name:com.sage.scrm.module.wechat.api
 * Date:2015年8月13日下午3:46:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.json.JSONObject;

import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.module.wechat.service.impl.AccessTokenCacheServiceImpl;
import com.bw.adv.module.wechat.util.HttpKit;

/**
 * ClassName:WeChatFanAPI <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月13日 下午3:46:30 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Component
public class ApiWeChatFan {
	
	private AccessTokenCacheService accessTokenCacheService;
	
	/**
	 * 本接口来获取帐号的关注者列表
	 * getWeChatFans:(). <br/>
	 * Date: 2015年8月13日 下午4:40:56 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	public Map<String,Object> getWeChatFans(){
		String getWeChatFansUrl =null;
		String result =null;
		//关注该公众账号的总用户数
		String total =null;
		//拉取的OPENID个数，最大值为10000
		String count =null;
		//列表数据，OPENID的列表
		JSONObject data =null;
		//拉取列表的最后一个用户的OPENID
		String nextOpenid =null;
		Map<String,Object> resultMap =null;
		System.out.println(accessTokenCacheService.getAccessTokenCache());
		try {
			resultMap =new HashMap<String,Object>();
			getWeChatFansUrl = ApiWeChat.GET_WECHATFANS.replace("ACCESS_TOKEN",accessTokenCacheService.getAccessTokenCache()).replace("NEXT_OPENID", "");
			result = HttpKit.get(getWeChatFansUrl);
			JSONObject jsonResult = JSONObject.fromObject(result);
			total =jsonResult.getInt("total")+"";
			count =jsonResult.getInt("count")+"";
			data =(JSONObject)jsonResult.get("data");
			nextOpenid =(String)jsonResult.get("next_openid");
			
			resultMap.put("total", total);
			resultMap.put("count", count);
			resultMap.put("data", data);
			resultMap.put("nextOpenid", nextOpenid);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return resultMap;
	}
	
	/**
	 * UpdateFansRemark:(设置备注名). <br/>
	 * Date: 2015年8月13日 下午4:59:44 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param openId
	 * @param remark
	 * @return
	 */
	public Map<String,String> updateFansRemark(String openId,String remark){
		String updateFanRemarkUrl=null;
		//微信返回的结果
		String result =null;
		//拼装数据
		JSONObject jsonObject =null;
		//code 
		String errcode =null;
		// errmsg
		String errmsg=null;
		// 返回的结果
		Map<String,String> map =null;
		try {
			jsonObject = new JSONObject();
			map = new HashMap<String,String>();
			updateFanRemarkUrl =ApiWeChat.UPDATE_FANREMARK.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			jsonObject.put("openid", openId);
			jsonObject.put("remark", remark);
			result = HttpKit.post(updateFanRemarkUrl, jsonObject.toString());
			JSONObject jsonResult = JSONObject.fromObject(result);
			errcode = jsonResult.getString("errcode");
			errmsg = jsonResult.getString("errmsg");
			map.put("errcode", errcode);
			map.put("errmsg", errmsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * getFanInfo:(获取用户基本信息). <br/>
	 * Date: 2015年8月13日 下午5:58:59 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param openId
	 * @return
	 */
	public JSONObject getFanInfo(String openId){
		String getFanInfoUrl =null;
		// 微信返回的结果
		String result =null;
		JSONObject jsonResult =null;
		try {
			jsonResult =new JSONObject();
			getFanInfoUrl =ApiWeChat.GET_WECHATFANINFO.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache()).replace("OPENID", openId);
			result = HttpKit.get(getFanInfoUrl);
			jsonResult =JSONObject.fromObject(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonResult;
	}
	
	@Autowired
	public void setAccessTokenCacheService(
			AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}
	
}

