package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatFansBehavior;

public interface WechatFansBehaviorMapper extends BaseMapper<WechatFansBehavior>{
    int deleteByPrimaryKey(Long wechatFansBehaviorId);

    int insert(WechatFansBehavior record);

    int insertSelective(WechatFansBehavior record);

    WechatFansBehavior selectByPrimaryKey(Long wechatFansBehaviorId);

    int updateByPrimaryKeySelective(WechatFansBehavior record);

    int updateByPrimaryKey(WechatFansBehavior record);
}