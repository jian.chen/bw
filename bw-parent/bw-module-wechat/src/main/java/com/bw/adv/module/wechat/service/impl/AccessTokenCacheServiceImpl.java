/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:AccessTokenCacheServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月11日上午11:51:53
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.service.impl;

import java.security.KeyManagementException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.api.ApiAccessToken;
import com.bw.adv.module.wechat.model.AccessTokenCache;
import com.bw.adv.module.wechat.model.exp.AccessTokenCacheExp;
import com.bw.adv.module.wechat.repository.AccessTokenCacheRepository;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;

/**
 * ClassName:AccessTokenCacheServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 上午11:51:53 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class AccessTokenCacheServiceImpl extends BaseServiceImpl<AccessTokenCache> implements AccessTokenCacheService {
	
	private static Long MULTIPLE = Long.valueOf(1000);
	private static Long DIFFMINUS =Long.valueOf(3600);
	private AccessTokenCacheRepository accessTokenCacheRepository;
	@Autowired
	private ApiAccessToken apiAccessToken;
	@Override
	public AccessTokenCache forceRefreshAccessToken(boolean throwException) throws KeyManagementException {
		String expiresIn =null;
		// 有效时间
		Long currentTime = System.currentTimeMillis();
		AccessTokenCache accessTokenCache = null;
		Map<String,String> map = null;
		try {
			accessTokenCache = accessTokenCacheRepository.getAccessTokenCache();
			map = apiAccessToken.getAccessToken();
			expiresIn = map.get("expires_in");
			AccessTokenCacheExp accessTokenCacheExp = new AccessTokenCacheExp();
			accessTokenCacheExp.setAccessToken(map.get("access_token"));
			accessTokenCacheExp.setTimeOut(currentTime + (Long.valueOf(expiresIn) - DIFFMINUS) * MULTIPLE + "");
			accessTokenCacheExp.setTimeCondition(accessTokenCache.getTimeOut());
			accessTokenCacheExp.setAccessTokenCacheId(accessTokenCache.getAccessTokenCacheId());
			if(accessTokenCacheRepository.updateAccessTokenCache(accessTokenCacheExp) > 0){
				return accessTokenCacheExp;
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (throwException) {
				throw new KeyManagementException(e.getMessage());
			}
		}
		return accessTokenCacheRepository.getAccessTokenCache();
	}
	
	@Override
	public AccessTokenCache queryAccessTokenCache() {
		
		String accessToken =null;
		String expiresIn =null;
		// 有效时间
		String validateTime =null;
		AccessTokenCache accessTokenCache = null;
		String timeOut =null;
		Long currentTime =null;
		Map<String,String> map = null;
		int result = 0;
		try {
			accessTokenCache = accessTokenCacheRepository.getAccessTokenCache();
			if(null != accessTokenCache){
				// 获取当前有效时间
				timeOut = accessTokenCache.getTimeOut();
				// 获取当前时间
				currentTime = DateUtils.getCurrentDate().getTime();
				// 凭证已经过期
				if(Long.valueOf(timeOut) <= currentTime){
					map = apiAccessToken.getAccessToken(); 
					accessToken = (String) map.get("access_token");
					expiresIn = (String)map.get("expires_in");
					validateTime = currentTime+(Long.valueOf(expiresIn)-DIFFMINUS)*MULTIPLE+"";
					AccessTokenCacheExp accessTokenCacheExp = new AccessTokenCacheExp();
					accessTokenCacheExp.setAccessToken(accessToken);
					accessTokenCacheExp.setTimeOut(validateTime);
					accessTokenCacheExp.setTimeCondition(timeOut);
					accessTokenCacheExp.setAccessTokenCacheId(accessTokenCache.getAccessTokenCacheId());
					result = accessTokenCacheRepository.updateAccessTokenCache(accessTokenCacheExp);
					if(result == 1){
						accessTokenCache.setAccessToken(accessToken);
						return accessTokenCache;
					}
					return accessTokenCacheRepository.getAccessTokenCache();
				}
			}else{
				accessTokenCache =new AccessTokenCache();
				// 获取当前时间
				currentTime = DateUtils.getCurrentDate().getTime();
				map = apiAccessToken.getAccessToken(); 
				accessToken = (String) map.get("access_token");
				expiresIn = (String)map.get("expires_in");
				validateTime = currentTime+(Long.valueOf(expiresIn)-DIFFMINUS)*MULTIPLE+"";
				accessTokenCache.setAccessToken(accessToken);
				accessTokenCache.setTimeOut(validateTime);
				accessTokenCacheRepository.save(accessTokenCache);
				
				return accessTokenCache;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return accessTokenCache;
	}

	@Override
	public int save(AccessTokenCache accessTokenCache) {
		return this.accessTokenCacheRepository.insert(accessTokenCache);
	}

	
	@Override
	public int updateAccessTokenCache(AccessTokenCacheExp accessTokenCacheExp) {
		return this.accessTokenCacheRepository.updateAccessTokenCache(accessTokenCacheExp);
	}
	
	@Override
	public String getAccessTokenCache() {
		return this.queryAccessTokenCache().getAccessToken();
	}
	
	@Override
	public BaseRepository<AccessTokenCache, ? extends BaseMapper<AccessTokenCache>> getBaseRepository() {
		return null;
	}
	
	@Autowired
	public void setAccessTokenCacheRepository(
			AccessTokenCacheRepository accessTokenCacheRepository) {
		this.accessTokenCacheRepository = accessTokenCacheRepository;
	}

	
	
}

