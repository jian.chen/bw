/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansGroupRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月11日下午5:48:23
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatMenuMapper;
import com.bw.adv.module.wechat.model.WechatButton;
import com.bw.adv.module.wechat.model.WechatMenu;

/**
 * ClassName:WechatFansGroupRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午5:48:23 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMenuRepository extends BaseRepository<WechatMenu, WechatMenuMapper> {

	/**
	 * 
	 * findAllMenu:查询微信菜单 <br/>
	 * Date: 2015年9月5日 下午2:03:06 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @return
	 */
	List<WechatButton> findAllMenu();

	/**
	 * 
	 * removeAllMenu:删除所有菜单 <br/>
	 * Date: 2015年9月14日 下午2:31:16 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 */
	void removeAllMenu();
	
	List<WechatMenu> findAllMenus();
	
	int removeByParentId(Long id);
}

