/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:AccessTokenCacheService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月11日上午11:48:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.service;

import java.security.KeyManagementException;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.AccessTokenCache;
import com.bw.adv.module.wechat.model.exp.AccessTokenCacheExp;
/**
 * ClassName:AccessTokenCacheService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 上午11:48:33 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface AccessTokenCacheService extends BaseService<AccessTokenCache> {
		
	/**
	 * 获取accessToken
	 * getAccessTokenCache:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年8月11日 上午11:49:32 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	public AccessTokenCache queryAccessTokenCache();
	
	/**
	 * update或者add accessToken
	 * insert:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年8月11日 上午11:49:55 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param accessTokenCache
	 * @return
	 */
	int save(AccessTokenCache accessTokenCache);
	
	/**
	 * update accessToken
	 * updateAccessTokenCache:(这里用一句话描述这个方法的作用). <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param accessTokenCache
	 * @return
	 */
	int updateAccessTokenCache(AccessTokenCacheExp accessTokenCacheExp);
	
	/**
	 * getAccessTokenCache:(获取accessToken). <br/>
	 * Date: 2015年8月20日 上午11:20:20 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	public String getAccessTokenCache();
	
	/**
	 * 强制更新token
	 * @return
	 */
	public AccessTokenCache forceRefreshAccessToken(boolean throwException) throws KeyManagementException;
	
}

