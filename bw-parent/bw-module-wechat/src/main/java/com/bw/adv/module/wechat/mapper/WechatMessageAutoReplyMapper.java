package com.bw.adv.module.wechat.mapper;


import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatMessageAutoReply;
import com.bw.adv.module.wechat.model.exp.WechatMessageAutoReplyExp;

public interface WechatMessageAutoReplyMapper extends BaseMapper<WechatMessageAutoReply>{
    int deleteByPrimaryKey(Long wechatMessageAutoReplyId);

    int insert(WechatMessageAutoReply record);

    int insertSelective(WechatMessageAutoReply record);

    WechatMessageAutoReply selectByPrimaryKey(Long wechatMessageAutoReplyId);

    int updateByPrimaryKeySelective(WechatMessageAutoReply record);

    int updateByPrimaryKey(WechatMessageAutoReply record);
    
    WechatMessageAutoReplyExp selectWechatMessageAutoReply(String type);
    
    int deleteAllWechatMessageAutoReply(String replytype);
}