/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WeChatRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月10日下午1:34:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.wechat.mapper.WechatFansMapper;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.exp.WechatFansExp;
import com.bw.adv.module.wechat.repository.WeChatFansRepository;

/**
 * ClassName:WeChatRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月10日 下午1:34:09 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WeChatFansRepositoryImpl extends BaseRepositoryImpl<WechatFans,WechatFansMapper> implements WeChatFansRepository {
	
	@Override
	protected Class<WechatFansMapper> getMapperClass() {
		return WechatFansMapper.class;
	}
	
	@Override
	public WechatFans findWeChatFunsById(Long id) {
		return this.getMapper().selectByPrimaryKey(id);
	}
	

	@Override
	public WechatFans findWeChatFunsByOpenId(String openId) {
		
		return this.getMapper().selectByOpenId(openId);
	}

	
	@Override
	public List<WechatFans> findWeChatFansByParams(String opendId,String nickName, String sex, String attentionTime,
			String cancelAttentionTime,String orgName,Page<WechatFans> page) {
		return this.getMapper().selectWeChatFansByParams(opendId,nickName,sex,attentionTime,cancelAttentionTime,orgName,page);
	}

	@Override
	public List<WechatFans> findWeChatFansList(Example example,Page<WechatFans> page) {
		return this.getMapper().selectWechatFansListByParams(example,page);
	}

	@Override
	public List<WechatFans> findWechatFansListByWechatFansLabelId(
			Long wechatFansLabelId) {
		return this.getMapper().selectWechatFansListByWechatFansLabelId(wechatFansLabelId);
	}


	@Override
	public List<WechatFansExp> findWeChatFansExpList(Example example,Page<WechatFansExp> page) {
		return this.getMapper().selectWeChatFansExpList(example,page);
	}
	
	
	@Override
	public List<WechatFans> findWechatFans() {
		return this.getMapper().selectWechatFans();
	}

	@Override
	public List<WechatFans> findWechatFansListByWechatFansGroupId(
			Long wechatFansGroupId) {
		return this.getMapper().selectWechatFansListByWechatFansGroupId(wechatFansGroupId);
	}

	@Override
	public void updateByOpenIdSelective(WechatFans wechatFans) {
		
		this.getMapper().updateByOpenIdSelective(wechatFans);
	}

}

