/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WeChatRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月10日下午1:26:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.wechat.mapper.WechatFansMapper;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.exp.WechatFansExp;

/**
 * ClassName:WeChatRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月10日 下午1:26:11 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WeChatFansRepository extends BaseRepository<WechatFans, WechatFansMapper>{
	
	/**
	 * 主键查询
	 * getWeChatFunsById:(主键查询). <br/>
	 * Date: 2015年8月10日 下午6:14:42 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param id
	 * @return
	 */
	
	public WechatFans findWeChatFunsById(Long id);
	
	/**
	 * 
	 * findWeChatFunsByOpenId:(根据openId查询粉丝表). <br/>
	 * Date: 2015年12月10日 下午10:13:51 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param openId
	 * @return
	 */
	public WechatFans findWeChatFunsByOpenId(String openId);
	
	/**
	 * 查询粉丝列表
	 * queryWeChatFansByParams:(查询粉丝列表). <br/>
	 * Date: 2015年8月10日 下午6:14:59 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param opendId
	 * @param nickName
	 * @param sex
	 * @param attentionTime
	 * @param cancelAttentionTime
	 * @param page
	 * @return
	 */
	public List<WechatFans> findWeChatFansByParams(String opendId,String nickName, String sex, String attentionTime,
			String cancelAttentionTime,String orgName,Page<WechatFans> page);
	
	/**
	 * 
	 * findWeChatFansList:(根据参数查询粉丝列表). <br/>
	 * Date: 2015年8月29日 下午2:13:56 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<WechatFans> findWeChatFansList(Example example,Page<WechatFans> page);
	
	/**
	 * findWechatFansListByWechatFansLabelId:(根据标签ID查询会员). <br/>
	 * Date: 2015-9-6 下午3:08:06 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param wechatFansLabelId
	 * @return
	 */
	public List<WechatFans> findWechatFansListByWechatFansLabelId(Long wechatFansLabelId);
	
	/**
	 * findWechatFans:(查询所有会员). <br/>
	 * Date: 2015-9-6 下午3:08:34 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 */
	public List<WechatFans> findWechatFans();
	
	/**
	 * findWechatFansListByWechatFansGroupId:(根据组id查询会员). <br/>
	 * Date: 2015-9-6 下午8:05:29 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param wechatFansGroupId
	 * @return
	 */
	public List<WechatFans> findWechatFansListByWechatFansGroupId(Long wechatFansGroupId);
	
	/**
	 * findWeChatFansExpList:(查询粉丝分组信息). <br/>
	 * Date: 2015年12月22日 下午3:00:05 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<WechatFansExp> findWeChatFansExpList(Example example,Page<WechatFansExp> page);
	
	/**
	 * 
	 * updateByOpenIdSelective:(根据openid修改粉丝记录). <br/>
	 * Date: 2016年3月3日 上午11:50:37 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param wechatFans
	 */
	public void updateByOpenIdSelective(WechatFans wechatFans);
}

