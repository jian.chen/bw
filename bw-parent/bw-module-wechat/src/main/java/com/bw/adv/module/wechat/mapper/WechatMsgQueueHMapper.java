package com.bw.adv.module.wechat.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueHExp;

public interface WechatMsgQueueHMapper extends BaseMapper<WechatMsgQueueH> {
	int deleteByPrimaryKey(Long wechatMsgQueueId);

	int insert(WechatMsgQueueH record);

	int insertSelective(WechatMsgQueueH record);

	WechatMsgQueueH selectByPrimaryKey(Long wechatMsgQueueId);

	int updateByPrimaryKeySelective(WechatMsgQueueH record);

	int updateByPrimaryKeyWithBLOBs(WechatMsgQueueH record);

	int updateByPrimaryKey(WechatMsgQueueH record);

	List<WechatMsgQueueHExp> findAllWechatMsgQueueHs(Long limit);

	void deleteBatch(List<Long> ids);

	void updateBatchWechatMsgQueueH(List<WechatMsgQueueHExp> list);

	void updateQueueToY();

	void deleteQueueWithR();
}