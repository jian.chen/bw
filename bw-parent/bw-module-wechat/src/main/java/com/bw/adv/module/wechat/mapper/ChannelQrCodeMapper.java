package com.bw.adv.module.wechat.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.ChannelQrCode;

public interface ChannelQrCodeMapper extends BaseMapper<ChannelQrCode> {

	/**
	 * 
	 * selectEffectiveList:(查询所有有效的二维码渠道). <br/>
	 * Date: 2015年11月26日 下午6:39:21 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	List<ChannelQrCode> selectEffectiveList();

}