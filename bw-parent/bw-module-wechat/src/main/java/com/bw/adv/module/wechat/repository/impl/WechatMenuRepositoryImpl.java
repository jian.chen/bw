

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:WechatMenuRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMenuMapper;
import com.bw.adv.module.wechat.model.WechatButton;
import com.bw.adv.module.wechat.model.WechatMenu;
import com.bw.adv.module.wechat.repository.WechatMenuRepository;

/**
 * ClassName:WechatMenuRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMenuRepositoryImpl extends BaseRepositoryImpl<WechatMenu, WechatMenuMapper> implements WechatMenuRepository{
	
	public List<WechatMenu> findAllMenus(){
		return this.getMapper().selectAll();
	}
	
	@Override
	protected Class<WechatMenuMapper> getMapperClass() {
		return WechatMenuMapper.class;
	}

	@Override
	public List<WechatButton> findAllMenu() {
		
		return this.getMapper().selectAllMenu();
	}

	@Override
	public void removeAllMenu() {
		this.getMapper().deleteAllMenu();
	}
	
	@Override
	public int removeByParentId(Long id){
		return this.getMapper().deleteByParentId(id);
	}
}