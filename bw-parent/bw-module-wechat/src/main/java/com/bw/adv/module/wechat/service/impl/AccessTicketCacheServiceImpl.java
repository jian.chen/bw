/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:AccessTicketCacheServiceImpl.java
 * Package Name:com.sage.scrm.bk.wechat.service.impl
 * Date:2015年11月17日下午3:40:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.wechat.model.AccessTicketCache;
import com.bw.adv.module.wechat.model.exp.AccessTicketCacheExp;
import com.bw.adv.module.wechat.repository.AccessTicketCacheRepository;
import com.bw.adv.module.wechat.service.AccessTicketCacheService;

/**
 * ClassName:AccessTicketCacheServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月17日 下午3:40:34 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class AccessTicketCacheServiceImpl extends BaseServiceImpl<AccessTicketCache> implements AccessTicketCacheService{
	private AccessTicketCacheRepository accessTicketCacheRepository;
	@Override
	public AccessTicketCache getAccessTicketCache() {
		return accessTicketCacheRepository.getAccessTicketCache();
	}

	@Override
	public int updateByPrimaryKey(AccessTicketCacheExp record) {
		return accessTicketCacheRepository.updateByPrimaryKey(record);
	}

	@Override
	public BaseRepository<AccessTicketCache, ? extends BaseMapper<AccessTicketCache>> getBaseRepository() {
		return accessTicketCacheRepository;
	}

	@Autowired
	public void setAccessTicketCacheRepository(AccessTicketCacheRepository accessTicketCacheRepository) {
		this.accessTicketCacheRepository = accessTicketCacheRepository;
	}
	
}

