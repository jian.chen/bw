/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMenuClickRecordRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年12月9日下午3:55:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatMenuClickRecordMapper;
import com.bw.adv.module.wechat.model.WechatMenuClickRecord;

/**
 * ClassName:WechatMenuClickRecordRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午3:55:24 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMenuClickRecordRepository extends BaseRepository<WechatMenuClickRecord, WechatMenuClickRecordMapper>{

}

