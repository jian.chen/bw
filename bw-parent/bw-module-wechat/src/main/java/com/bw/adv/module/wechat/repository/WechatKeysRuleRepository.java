/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatKeysRuleRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月24日下午5:34:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatKeysRuleMapper;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.WechatKeysRuleExp;

/**
 * ClassName:WechatKeysRuleRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午5:34:56 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatKeysRuleRepository extends BaseRepository<WechatKeysRule, WechatKeysRuleMapper> {
	
	/**
	 * findWechatKeyRules:(查询有效的关键字规则组). <br/>
	 * Date: 2015年8月26日 下午5:36:10 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	List<WechatKeysRule> findWechatKeyRules();
	
	/**
	 * findWechatKeyRuleExpByKeyRule:(查询扩展类). <br/>
	 * Date: 2015年8月26日 下午5:36:54 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatKeysRule
	 * @return
	 */
	WechatKeysRuleExp findWechatKeyRuleExpByKeyRule(WechatKeysRule wechatKeysRule);

}

