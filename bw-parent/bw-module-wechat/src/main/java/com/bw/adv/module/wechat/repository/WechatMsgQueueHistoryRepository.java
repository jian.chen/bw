/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMsgQueueHistoryRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年12月9日下午9:37:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatMsgQueueHistoryMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueueHistory;

/**
 * ClassName:WechatMsgQueueHistoryRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午9:37:55 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMsgQueueHistoryRepository extends BaseRepository<WechatMsgQueueHistory, WechatMsgQueueHistoryMapper>{
	public void saveAllWechatMsgQueueHistory(List<WechatMsgQueueHistory> historys);
}

