/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageTemplateRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月18日下午4:44:43
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatTemplateMessageMapper;
import com.bw.adv.module.wechat.model.WechatTemplateMessage;
import com.bw.adv.module.wechat.repository.WechatTemplateMessageRepository;



@Repository
public class WechatTemplateMessageRepositoryImpl extends BaseRepositoryImpl<WechatTemplateMessage, WechatTemplateMessageMapper> implements WechatTemplateMessageRepository {
	@Override
	protected Class<WechatTemplateMessageMapper> getMapperClass() {
		return WechatTemplateMessageMapper.class;
	}

	@Override
	public List<WechatTemplateMessage> findWechatTemplateMessageList() {
		return this.getMapper().selectWechatTemplateMessageList();
	}

	

}

