/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:AccessTicketCacheRepository.java
 * Package Name:com.sage.scrm.bk.wechat.repository
 * Date:2015年11月17日下午3:34:23
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.AccessTicketCacheMapper;
import com.bw.adv.module.wechat.model.AccessTicketCache;
import com.bw.adv.module.wechat.model.exp.AccessTicketCacheExp;

/**
 * ClassName:AccessTicketCacheRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月17日 下午3:34:23 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface AccessTicketCacheRepository extends BaseRepository<AccessTicketCache, AccessTicketCacheMapper>{
	public AccessTicketCache getAccessTicketCache();
	public int updateByPrimaryKey(AccessTicketCacheExp record); 
}

