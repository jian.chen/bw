package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.AccessTicketCache;
import com.bw.adv.module.wechat.model.exp.AccessTicketCacheExp;

public interface AccessTicketCacheMapper extends BaseMapper<AccessTicketCache>{
    int deleteByPrimaryKey(Long accessTicketCacheId);

    int insert(AccessTicketCache record);

    int insertSelective(AccessTicketCache record);

    AccessTicketCache selectByPrimaryKey(Long accessTicketCacheId);

    int updateByPrimaryKeySelective(AccessTicketCache record);

    int updateByPrimaryKey(AccessTicketCacheExp record);
    
    AccessTicketCache getAccessTicketCache();
}