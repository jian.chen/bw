/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatUtils.java
 * Package Name:com.sage.scrm.module.wechat.util
 * Date:2015年8月7日上午10:23:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Value;

import net.sf.json.JSONObject;

import com.bw.adv.module.wechat.api.ApiWeChat;



/**
 * ClassName:WechatUtils <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月7日 上午10:23:26 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WechatUtils {
	
	@Value("${appId}")
	private static String appId;
	@Value("${appSecret}")
	private static String appSecret ;
	
	/**
	 * 获取accessToken和凭证有效时间
	 * getAccessToken:(这里用一句话描述这个方法的作用). <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	public static final Map getAccessToken() throws KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException, UnsupportedEncodingException, IOException, ExecutionException, InterruptedException{
		
		String accessToken =null;
		String expiresIn =null;
		Map map =new HashMap<String,String>();
 		
		String accessTokenUrl = 
				ApiWeChat.ACCESS_TOKEN.replace("APPID", appId).replace("APPSECRET", appSecret);
		String result = HttpKit.get(accessTokenUrl);
		JSONObject jsonObject = JSONObject.fromObject(result);
		
		accessToken = jsonObject.getString("access_token");
		expiresIn = jsonObject.getInt("expires_in")+"";
		
		map.put("access_token", accessToken);
		map.put("expires_in", expiresIn);
		
		return map;
	}
	
	public static final String inputStream2String(InputStream in)
	        throws UnsupportedEncodingException, IOException {
	        if (in == null)
	            return "";
	        StringBuffer out = new StringBuffer();
	        byte[] b = new byte[4096];
	        for (int n; (n = in.read(b)) != -1;) {
	            out.append(new String(b, 0, n, "UTF-8"));
	        }
	        return out.toString();
	    }
	
	
	/*public static final boolean checkSignature(String token, String signature, String timestamp, String nonce) {
        if (StringUtils.isEmpty(signature)) {
            return false;
        }
        List<String> params = new ArrayList<String>();
        params.add(token);
        params.add(timestamp);
        params.add(nonce);
        
        Collections.sort(params, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });	
        
        String temp = params.get(0) + params.get(1) + params.get(2);
        return SHA1.encode(temp).equals(signature);
    }*/
	
}

