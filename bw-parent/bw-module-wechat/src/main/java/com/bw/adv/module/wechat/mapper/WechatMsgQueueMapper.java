package com.bw.adv.module.wechat.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueue;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueExp;

public interface WechatMsgQueueMapper extends BaseMapper<WechatMsgQueue>{
	
    List<WechatMsgQueueExp> findAllWechatMsgQueues(Long limit);
    
    void deleteBatch(List<Long> ids);
    
    void updateBatchWechatMsgQueue(List<WechatMsgQueueExp> list);
    
    /**
     * selectExpByLimit:(查询消息列表). <br/>
     * Date: 2016-5-6 下午2:49:55 <br/>
     * scrmVersion 1.0
     * @author mennan
     * @version jdk1.7
     * @param start
     * @param size
     * @return
     */
    List<WechatMsgQueueExp> selectExpByLimit(@Param("start")int start,@Param("size")int size);

	/**
	 * selectAllCount:(查询所有未发送的消息数量). <br/>
	 * Date: 2016-5-6 下午5:41:50 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	int selectAllCount();
	
	void updateQueueToY();

	void deleteQueueWithR();
	
}