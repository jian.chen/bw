/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatGroupMessageRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月18日下午4:54:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatGroupMessageMapper;
import com.bw.adv.module.wechat.model.WechatGroupMessage;
import com.bw.adv.module.wechat.repository.WechatGroupMessageRepository;

/**
 * ClassName:WechatGroupMessageRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:54:11 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatGroupMessageRepositoryImpl extends BaseRepositoryImpl<WechatGroupMessage, WechatGroupMessageMapper> implements WechatGroupMessageRepository{

	@Override
	protected Class<WechatGroupMessageMapper> getMapperClass() {
		return WechatGroupMessageMapper.class;
	}
	
	
	
}

