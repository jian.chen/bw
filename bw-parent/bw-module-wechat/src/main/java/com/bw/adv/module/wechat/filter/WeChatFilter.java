/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WeChatFilter.java
 * Package Name:com.sage.scrm.module.wechat.filter
 * Date:2015年8月6日下午2:21:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.wechat.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bw.adv.module.wechat.common.WechatConstant;
import com.bw.adv.module.wechat.util.WechatUtils;

/**
 * ClassName:WeChatFilter <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月6日 下午2:21:29 <br/>
 * scrmVersion 1.0
 * @author june
 * @version jdk1.7
 * @see
 */
public class WeChatFilter implements Filter {
	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		System.out.println("进入doFilter过滤器..");
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		// 微信服务器请求方式只有get和post两种方式
		Boolean isGet = request.getMethod().equals(WechatConstant.GET_TYPE);
		System.out.println("requesttype:"+isGet);
		if (isGet) {
			doGet(request, response);
		} else {
			doPost(request, response);
		}
	}

	/**
	 * doPost:(处理doPost方式). <br/>
	 * Date: 2015年8月7日 上午10:17:18 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @param response
	 */
	private void doPost(HttpServletRequest request, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
        response.setContentType("text/xml");
        ServletInputStream in =null;
		try {
			in = request.getInputStream();
			String xmlMsg = WechatUtils.inputStream2String(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 处理微信发过来的get请求 doGet:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年8月7日 上午10:18:34 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @param response
	 */
	private void doGet(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("进入doGet方法。。");
		String path = null;
		String pathInfo = null;
		String token = null;
		String outRes = null;
		try {
			path = request.getServletPath();
			pathInfo = path.substring(path.lastIndexOf("/"));
			outRes = "error";
			if (pathInfo != null) {
				token = pathInfo.substring(1);
				String signature = request.getParameter("signature");// 微信加密签名
				String timestamp = request.getParameter("timestamp");// 时间戳
				String nonce = request.getParameter("nonce");// 随机数
				String echostr = request.getParameter("echostr");// 随机字符串
				// 对请求进行校验
				/*if (WechatUtils.checkSignature(token, signature, timestamp,nonce)) {
					outRes = echostr;
				}*/
			}
			response.getWriter().write(outRes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println("-----CRM微信WeChatFilter容器已经启动-----");
	}

	@Override
	public void destroy() {
		System.out.println("-----CRM微信WeChatFilter容器已经销毁-----");
	}

}
