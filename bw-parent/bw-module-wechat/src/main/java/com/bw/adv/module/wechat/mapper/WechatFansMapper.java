package com.bw.adv.module.wechat.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.exp.WechatFansExp;

public interface WechatFansMapper extends BaseMapper<WechatFans>{
	
	/**
	 * 主键查询
	 * selectByPrimaryKey:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年8月10日 下午6:15:43 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFansIds
	 * @return
	 */
    WechatFans selectByPrimaryKey(@Param("wechatFansId")Long wechatFansId);
    
    /**
     * 
     * selectByOpenId:(根据openId查询粉丝表). <br/>
     * Date: 2015年12月10日 下午10:12:36 <br/>
     * scrmVersion 1.0
     * @author chuanxue.wei
     * @version jdk1.7
     * @param openId
     * @return
     */
    WechatFans selectByOpenId(String openId);
    
    /**
     * 查询列表
     * queryWeChatFansByParams:(这里用一句话描述这个方法的作用). <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param opendId
     * @param nickName
     * @param sex
     * @param attentionTime
     * @param cancelAttentionTime
     * @param page 
     * @return
     */
	List<WechatFans> selectWeChatFansByParams(@Param("openid")String opendId, @Param("nickName")String nickName,@Param("sex")String sex, @Param("attentionTime")String attentionTime, @Param("cancelAttentionTime")String cancelAttentionTime,@Param("orgName")String orgName, Page<WechatFans> page);
	
	/**
	 * selectWechatFansListByParams:(根据参数查询粉丝信息). <br/>
	 * Date: 2015年8月29日 下午2:16:46 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<WechatFans> selectWechatFansListByParams(@Param("example")Example example,Page<WechatFans> page);
	
	/**
	 * selectWechatFansListByWechatFansLabelId:(根据标签ID查询会员list). <br/>
	 * Date: 2015-9-6 下午3:02:08 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param wechatFansLabelId
	 * @return
	 */
	List<WechatFans> selectWechatFansListByWechatFansLabelId(Long wechatFansLabelId);
	    
	/**
	 * selectWechatFans:(查询所有会员). <br/>
	 * Date: 2015-9-6 下午8:01:28 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 */
	List<WechatFans> selectWechatFans();
	
	/**
	 * selectWechatFansListByWechatFansGroupId:(根据组ID查询会员list). <br/>
	 * Date: 2015-9-6 下午8:02:07 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param wechatFansGroupId
	 * @return
	 */
	List<WechatFans> selectWechatFansListByWechatFansGroupId(Long wechatFansGroupId);
	
	/**
	 * selectWeChatFansExpList:(查询粉丝分组信息). <br/>
	 * Date: 2015年12月22日 下午3:01:27 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<WechatFansExp> selectWeChatFansExpList(@Param("example")Example example,Page<WechatFansExp> page);
	
	/**
	 * 
	 * updateByOpenIdSelective:(根据openid修改粉丝记录). <br/>
	 * Date: 2016年3月3日 上午11:46:31 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param wechatFans
	 */
	void updateByOpenIdSelective(WechatFans wechatFans);
}