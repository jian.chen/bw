package com.bw.adv.module.wechat.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatFansGroup;

public interface WechatFansGroupMapper extends BaseMapper<WechatFansGroup>{
    int deleteByPrimaryKey(Long wechatFansGroupId);

    int insert(WechatFansGroup record);

    int insertSelective(WechatFansGroup record);

    WechatFansGroup selectByPrimaryKey(Long wechatFansGroupId);

    int updateByPrimaryKeySelective(WechatFansGroup record);

    int updateByPrimaryKey(WechatFansGroup record);
    
    List<WechatFansGroup> selectAllGroups();
    
    /**
     * selectGroup:(根据分组id查询分组详情). <br/>
     * Date: 2015年8月20日 下午4:42:15 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param groupId 
     * @return
     */
	WechatFansGroup selectGroup(String groupId);
}