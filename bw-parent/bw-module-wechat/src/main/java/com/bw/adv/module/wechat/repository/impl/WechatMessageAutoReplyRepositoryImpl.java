/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageAutoReplyRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月19日下午2:13:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;


import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMessageAutoReplyMapper;
import com.bw.adv.module.wechat.model.WechatMessageAutoReply;
import com.bw.adv.module.wechat.model.exp.WechatMessageAutoReplyExp;
import com.bw.adv.module.wechat.repository.WechatMessageAutoReplyRepository;

/**
 * ClassName:WechatMessageAutoReplyRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午2:13:19 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMessageAutoReplyRepositoryImpl extends BaseRepositoryImpl<WechatMessageAutoReply, WechatMessageAutoReplyMapper> implements WechatMessageAutoReplyRepository {

	@Override
	protected Class<WechatMessageAutoReplyMapper> getMapperClass() {
		return WechatMessageAutoReplyMapper.class;
	}
	
	@Override
	public WechatMessageAutoReplyExp findWechatMessageAutoReply(String type) {
		return this.getMapper().selectWechatMessageAutoReply(type);
	}

	@Override
	public void removeAllWechatMessageAutoReply(String replytype) {
		this.getMapper().deleteAllWechatMessageAutoReply(replytype);
	}
	
	
	
}

