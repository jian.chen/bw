/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansMessageRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月25日下午5:00:53
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatFansMessageMapper;
import com.bw.adv.module.wechat.model.WechatFansMessage;
import com.bw.adv.module.wechat.repository.WechatFansMessageRepository;

/**
 * ClassName:WechatFansMessageRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午5:00:53 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatFansMessageRepositoryImpl extends BaseRepositoryImpl<WechatFansMessage, WechatFansMessageMapper> implements WechatFansMessageRepository {

	@Override
	protected Class<WechatFansMessageMapper> getMapperClass() {
		return WechatFansMessageMapper.class;
	}
	
	
	
}

