/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageTemplateRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月18日下午4:43:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatMessageTemplateMapper;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;

/**
 * ClassName:WechatMessageTemplateRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:43:42 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMessageTemplateRepository extends BaseRepository<WechatMessageTemplate, WechatMessageTemplateMapper> {
	
	/**
	 * 
	 * findParentImageText:(查询父级的图文消息以及单图文消息). <br/>
	 * Date: 2015年9月10日 下午3:09:14 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	List<WechatMessageTemplate> findParentImageText();
	
	/**
	 * findSubImageTextList:(查询父级下的). <br/>
	 * Date: 2015年9月10日 下午3:34:15 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatMessageTemplateId 
	 * @return
	 */
	List<WechatMessageTemplate> findSubImageTextList(String wechatMessageTemplateId);
	
}

