/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMsgQueueHHistoryRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年12月16日上午11:02:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatMsgQueueHHistoryMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueueHHistory;

/**
 * ClassName:WechatMsgQueueHHistoryRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月16日 上午11:02:28 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMsgQueueHHistoryRepository extends BaseRepository<WechatMsgQueueHHistory, WechatMsgQueueHHistoryMapper>{
	public void saveAllWechatMsgQueueHHistory(List<WechatMsgQueueHHistory> historys);
}

