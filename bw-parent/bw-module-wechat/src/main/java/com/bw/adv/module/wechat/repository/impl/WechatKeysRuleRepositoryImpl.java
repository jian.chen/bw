/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatKeysRuleRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月24日下午5:35:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatKeysRuleMapper;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.WechatKeysRuleExp;
import com.bw.adv.module.wechat.repository.WechatKeysRuleRepository;

/**
 * ClassName:WechatKeysRuleRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午5:35:29 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatKeysRuleRepositoryImpl extends BaseRepositoryImpl<WechatKeysRule, WechatKeysRuleMapper> 
				implements WechatKeysRuleRepository {

	@Override
	protected Class<WechatKeysRuleMapper> getMapperClass() {
		return WechatKeysRuleMapper.class;
	}

	@Override
	public List<WechatKeysRule> findWechatKeyRules() {
		return this.getMapper().selectWechatKeyRules();
	}

	@Override
	public WechatKeysRuleExp findWechatKeyRuleExpByKeyRule(
			WechatKeysRule wechatKeysRule) {
		
		return this.getMapper().selectWechatKeyRuleExpByKeyRule(wechatKeysRule);
	}
	
	
	
}

