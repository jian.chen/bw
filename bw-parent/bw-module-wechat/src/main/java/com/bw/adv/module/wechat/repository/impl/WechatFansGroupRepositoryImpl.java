/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansGroupRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月11日下午5:57:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatFansGroupMapper;
import com.bw.adv.module.wechat.model.WechatFansGroup;
import com.bw.adv.module.wechat.repository.WechatFansGroupRepository;

/**
 * ClassName:WechatFansGroupRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午5:57:18 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatFansGroupRepositoryImpl extends BaseRepositoryImpl<WechatFansGroup, WechatFansGroupMapper> implements WechatFansGroupRepository {
	
	@Override
	protected Class<WechatFansGroupMapper> getMapperClass() {
		return WechatFansGroupMapper.class;
	}

	@Override
	public List<WechatFansGroup> findAllGroups() {
		return this.getMapper().selectAllGroups();
	}

	@Override
	public WechatFansGroup findWechatFansGroup(String groupId) {
		return this.getMapper().selectGroup(groupId);
	}
	
	
}

