/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMaterialUploadAPI.java
 * Package Name:com.sage.scrm.module.wechat.api
 * Date:2015年8月17日下午3:09:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.wechat.api;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bw.adv.module.wechat.model.WeixinMedia;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.module.wechat.util.FileUtil;
import com.bw.adv.module.wechat.util.HttpKit;

/**
 * ClassName:WechatMaterialUploadAPI <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午3:09:22 <br/>
 * scrmVersion 1.0
 * 
 * @author june
 * @version jdk1.7
 * @see
 */
@Component
public class ApiWechatMaterialUpload {
	
	private AccessTokenCacheService accessTokenCacheService;
	
	@Autowired
	private ApiAccessToken apiAccessToken;

	/**
	 * uploadMedia:(上传临时素材接口). <br/>
	 * Date: 2015年8月17日 下午3:16:38 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param accessToken
	 *            调用接口凭证
	 * @param type
	 *            媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
	 * @param mediaFileUrl
	 *            素材的url
	 * @return 注意事项: 图片（image）: 1M，支持JPG格式 语音（voice）：2M，播放长度不超过60s，支持AMR\MP3格式
	 *         视频（video）：10MB，支持MP4格式 缩略图（thumb）：64KB，支持JPG格式
	 */
	public WeixinMedia uploadMedia(String type, String mediaFileUrl) {
		WeixinMedia weixinMedia = null;
		// 上传临时素材接口
		String uploadMediaUrl = null;
		// 定义数据分隔符
		String boundary = "------------7da2e536604c8";
		try {
			// 拼装请求地址
			uploadMediaUrl = ApiWeChat.UPLOAD_TEMP_MEDIA_URL.replace(
					"ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache()).replace("TYPE", type);
			URL uploadUrl = new URL(uploadMediaUrl);
			HttpURLConnection uploadConn = (HttpURLConnection) uploadUrl
					.openConnection();
			uploadConn.setDoOutput(true);
			uploadConn.setDoInput(true);
			uploadConn.setRequestMethod("POST");
			// 设置请求头Content-Type
			uploadConn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			// 获取媒体文件上传的输出流（往微信服务器写数据）
			OutputStream outputStream = uploadConn.getOutputStream();

			URL mediaUrl = new URL(mediaFileUrl);
			HttpURLConnection meidaConn = (HttpURLConnection) mediaUrl
					.openConnection();
			meidaConn.setDoOutput(true);
			meidaConn.setRequestMethod("GET");

			// 从请求头中获取内容类型
			String contentType = meidaConn.getHeaderField("Content-Type");
			// 根据内容类型判断文件扩展名
			// String fileExt = WeixinUtil.getFileEndWitsh(contentType);
			String fileName = mediaUrl.getFile();
			fileName = fileName.substring(fileName.lastIndexOf("/") + 1,
					fileName.length());
			String fileExt = FileUtil.getFileExt(fileName);

			// 请求体开始
			outputStream.write(("--" + boundary + "\r\n").getBytes());
			outputStream.write(String.format(
					"Content-Disposition: form-data; name=\"media\"; filename=\""
							+ fileName + "\"\r\n", fileExt).getBytes());
			outputStream.write(String.format("Content-Type: %s\r\n\r\n",
					contentType).getBytes());

			// 获取媒体文件的输入流（读取文件）
			BufferedInputStream bis = new BufferedInputStream(
					meidaConn.getInputStream());
			byte[] buf = new byte[8096];
			int size = 0;
			while ((size = bis.read(buf)) != -1) {
				// 将媒体文件写到输出流（往微信服务器写数据）
				outputStream.write(buf, 0, size);
			}
			// 请求体结束
			outputStream.write(("\r\n--" + boundary + "--\r\n").getBytes());
			outputStream.close();
			bis.close();
			meidaConn.disconnect();

			// 获取媒体文件上传的输入流（从微信服务器读数据）
			InputStream inputStream = uploadConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(
					inputStreamReader);
			StringBuffer buffer = new StringBuffer();
			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			// 释放资源
			inputStream.close();
			inputStream = null;
			uploadConn.disconnect();

			// 使用JSON-lib解析返回结果
			net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject
					.fromObject(buffer.toString());
			// 测试打印结果
			System.out.println("打印测试结果" + jsonObject);
			weixinMedia = new WeixinMedia();
			// weixinMedia.setType(jsonObject.getString("type"));
			// type等于 缩略图（thumb） 时的返回结果和其它类型不一样
			if ("thumb".equals(type))
				weixinMedia.setMediaId(jsonObject.getString("thumb_media_id"));
			else
				weixinMedia.setMediaId(jsonObject.getString("media_id"));
			// weixinMedia.setCreatedAt(jsonObject.getInt("created_at"));
		} catch (Exception e) {
			weixinMedia = null;
			String error = String.format("上传媒体文件失败：%s", e);
			System.out.println(error);
		}
		return weixinMedia;
	}
	
	/**
	 * uploadMediaForever:(上传永久素材). <br/>
	 * Date: 2015年8月17日 下午8:33:19 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param accessToken
	 * @param type
	 * @param mediaFileUrl
	 * @return
	 */
	 public WeixinMedia uploadMediaForever(String type, String mediaFileUrl) {
	        WeixinMedia weixinMedia = null;
	        // 定义数据分隔符
	        String boundary = "------------7da2e536604c8";
	        try {
	        	// 拼装请求地址
	        	String uploadMediaUrl = ApiWeChat.UPLOAD_FORVER_MEDIA_URL.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
	            URL uploadUrl = new URL(uploadMediaUrl);
	            HttpURLConnection uploadConn = (HttpURLConnection)uploadUrl.openConnection();
	            uploadConn.setDoOutput(true);
	            uploadConn.setDoInput(true);
	            uploadConn.setRequestMethod("POST");
	            // 设置请求头Content-Type
	            uploadConn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
	            // 获取媒体文件上传的输出流（往微信服务器写数据）
	            OutputStream outputStream = uploadConn.getOutputStream();
	            URL mediaUrl = new URL(mediaFileUrl);
	            HttpURLConnection meidaConn = (HttpURLConnection)mediaUrl.openConnection();
	            meidaConn.setDoOutput(true);
	            meidaConn.setRequestMethod("GET");
	            
	            // 从请求头中获取内容类型
	            String contentType = meidaConn.getHeaderField("Content-Type");
	            // 根据内容类型判断文件扩展名
	            // String fileExt = WeixinUtil.getFileEndWitsh(contentType);
	            String fileName = mediaUrl.getFile();
	            fileName = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.length());
	            String fileExt = FileUtil.getFileExt(fileName);
	            
	            // 请求体开始
	            outputStream.write(("--" + boundary + "\r\n").getBytes());
	            outputStream.write(String.format("Content-Disposition: form-data; name=\"media\"; filename=\"" + fileName + "\"\r\n", fileExt).getBytes());
	            outputStream.write(String.format("Content-Type: %s\r\n\r\n", contentType).getBytes());
	            
	            // 获取媒体文件的输入流（读取文件）
	            BufferedInputStream bis = new BufferedInputStream(meidaConn.getInputStream());
	            byte[] buf = new byte[8096];
	            int size = 0;
	            while ((size = bis.read(buf)) != -1) {
	                // 将媒体文件写到输出流（往微信服务器写数据）
	                outputStream.write(buf, 0, size);
	            }
	            // 请求体结束
	            outputStream.write(("\r\n--" + boundary + "--\r\n").getBytes());
	            outputStream.close();
	            bis.close();
	            meidaConn.disconnect();
	            
	            // 获取媒体文件上传的输入流（从微信服务器读数据）
	            InputStream inputStream = uploadConn.getInputStream();
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            StringBuffer buffer = new StringBuffer();
	            String str = null;
	            while ((str = bufferedReader.readLine()) != null) {
	                buffer.append(str);
	            }
	            bufferedReader.close();
	            inputStreamReader.close();
	            // 释放资源
	            inputStream.close();
	            inputStream = null;
	            uploadConn.disconnect();
	            
	            // 使用JSON-lib解析返回结果
	            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(buffer.toString());
	            // 测试打印结果
	            System.out.println("打印测试结果" + jsonObject);
	            weixinMedia = new WeixinMedia();
	            // weixinMedia.setType(jsonObject.getString("type"));
	            // type等于 缩略图（thumb） 时的返回结果和其它类型不一样
	            if ("thumb".equals(type))
	                weixinMedia.setMediaId(jsonObject.getString("thumb_media_id"));
	            else
	                weixinMedia.setMediaId(jsonObject.getString("media_id"));
	            // weixinMedia.setCreatedAt(jsonObject.getInt("created_at"));
	        }
	        catch (Exception e) {
	            weixinMedia = null;
	            String error = String.format("上传媒体文件失败：%s", e);
	            System.out.println(error);
	        }
	        return weixinMedia;
	    }
	
	/**
	 * uploadFodder:(). <br/>
	 * Date: 2015年8月17日 下午4:59:15 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param uploadurl
	 * @param access_token
	 * @param data
	 * @return
	 */
	/*public static String uploadFodder(String uploadurl, String access_token, String data) {
		org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
		String posturl = String.format("%s?access_token=%s", uploadurl,
				access_token);
		PostMethod post = new PostMethod(posturl);
		post.setRequestHeader(
				"User-Agent",
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0");
		post.setRequestHeader("Host", "file.api.weixin.qq.com");
		post.setRequestHeader("Connection", "Keep-Alive");
		post.setRequestHeader("Cache-Control", "no-cache");
		String result = null;
		try {
			post.setRequestBody(data);
			int status = client.executeMethod(post);
			if (status == HttpStatus.SC_OK) {
				String responseContent = post.getResponseBodyAsString();
				System.out.println(responseContent);
				JsonParser jsonparer = new JsonParser();// 初始化解析json格式的对象
				JsonObject json = jsonparer.parse(responseContent)
						.getAsJsonObject();
				if (json.get("errcode") == null) {// 正确 { "type":"news",
				// "media_id":"CsEf3ldqkAYJAU6EJeIkStVDSvffUJ54vqbThMgplD-VJXXof6ctX5fI6-aYyUiQ","created_at":1391857799}
					result = json.get("media_id").getAsString();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return result;
		}
	}*/
	 
	/**
	 * upLoadMultiImageText:(微信上传多图文). <br/>
	 * Date: 2015年9月9日 下午4:48:56 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param mediaList
	 * @param dataJson
	 * @return
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
	public JSONObject upLoadMultiImageText(List<WeixinMedia> mediaList,JSONArray dataJson) 
			throws KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException, UnsupportedEncodingException, IOException, ExecutionException, InterruptedException {
		String uploadImageTextUrl =null;
		JSONArray ImageTextArray =null;
		ImageTextArray =new JSONArray();
		String ImageTextTitle =null;
		String ImageTextAuthor =null;
		String ImageTextDigest =null;
		String ImageTextCover =null;
		String ImageTextBody =null;
		String ImageTextLink =null;
		String media =null;
		JSONObject jsonObject =null;
		JSONObject resultJson =null;
		jsonObject =new JSONObject();
		ImageTextArray =new JSONArray();
		uploadImageTextUrl = ApiWeChat.UPLOAD_IMAGE_TEXT_MEDIA_URL.replace("ACCESS_TOKEN",apiAccessToken.getAccessTokenString());
		for(int i=0;i<mediaList.size();i++){
			JSONObject json =(JSONObject)dataJson.get(i);
			// 标题
			ImageTextTitle = (String)json.get("ImageTextTitle");
			// 图文消息的封面图片素材id（必须是永久mediaID）
			media =mediaList.get(i).getMediaId();
			// 作者
			ImageTextAuthor = (String)json.get("ImageTextAuthor");
			// 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
			ImageTextDigest = (String)json.get("ImageTextDigest");
			// 是否显示封面，0为false，即不显示，1为true，即显示
			ImageTextCover =json.getInt("ImageTextCover")+"";
			// 图文消息的具体内容，支持HTML标签
			ImageTextBody =(String)json.get("ImageTextBody");
			// 图文消息的原文地址
			ImageTextLink =(String)json.get("ImageTextLink");
			
			JSONObject subJsonObject =new JSONObject();
			subJsonObject.put("title",ImageTextTitle);
			subJsonObject.put("thumb_media_id",media);
			subJsonObject.put("author",ImageTextAuthor);
			subJsonObject.put("show_cover_pic",ImageTextCover);
			subJsonObject.put("content",ImageTextBody);
			subJsonObject.put("content_source_url",ImageTextLink);
			if(i==0){
				subJsonObject.put("digest",ImageTextDigest);
			}
			ImageTextArray.add(subJsonObject);
		}
		jsonObject.put("articles", ImageTextArray);
		resultJson = JSONObject.fromObject(HttpKit.post(uploadImageTextUrl, jsonObject.toString()));
		return resultJson;
	}
	
	 @Autowired
	 public void setAccessTokenCacheService(
				AccessTokenCacheService accessTokenCacheService) {
			this.accessTokenCacheService = accessTokenCacheService;
		}
}
