/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMsgQueueHHistoryRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年12月16日上午11:05:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMsgQueueHHistoryMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueueHHistory;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHHistoryRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:WechatMsgQueueHHistoryRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月16日 上午11:05:16 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMsgQueueHHistoryRepositoryImpl extends BaseRepositoryImpl<WechatMsgQueueHHistory, WechatMsgQueueHHistoryMapper> implements WechatMsgQueueHHistoryRepository{

	@Override
	public void saveAllWechatMsgQueueHHistory(List<WechatMsgQueueHHistory> historys) {
		if(historys == null || historys.size() <= 0){
			return;
		}
		this.getMapper().insertBatch(historys);
	}

	@Override
	protected Class<WechatMsgQueueHHistoryMapper> getMapperClass() {
		return WechatMsgQueueHHistoryMapper.class;
	}

}

