/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansGroupItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月20日下午4:20:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.mapper.WechatFansGroupItemMapper;
import com.bw.adv.module.wechat.model.WechatFansGroupItem;
import com.bw.adv.module.wechat.model.exp.WechatFansGroupItemExp;
import com.bw.adv.module.wechat.repository.WechatFansGroupItemRepository;

/**
 * ClassName:WechatFansGroupItemRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午4:20:56 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatFansGroupItemRepositoryImpl extends BaseRepositoryImpl<WechatFansGroupItem, WechatFansGroupItemMapper> implements WechatFansGroupItemRepository {

	@Override
	protected Class<WechatFansGroupItemMapper> getMapperClass() {
		return WechatFansGroupItemMapper.class;
	}
	
	@Override
	public List<WechatFansGroupItemExp> WechatFansGroupItemByGroupId(Long groupId) {
		return this.getMapper().selectWechatFansByGroupId(groupId);
	}

	@Override
	public WechatFansGroupItem findWechatFansGroupItemByFansId(Long wechatId) {
		return this.getMapper().selectWechatFansGroupItemByFansId(wechatId);
	}

	@Override
	public List<WechatFansGroupItem> findWechatFansGroupItemsByGroupId(Long wechatGroupId) {
		return this.getMapper().selectWechatFansGroupItemsByGroupId(wechatGroupId);
	}
	
	
	
	
}

