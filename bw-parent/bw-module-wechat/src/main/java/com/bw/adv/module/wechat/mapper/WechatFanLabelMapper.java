package com.bw.adv.module.wechat.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.model.WechatFanLabel;
import com.bw.adv.module.wechat.model.exp.WechatFanLabelExp;

public interface WechatFanLabelMapper extends BaseMapper<WechatFanLabel>{
    int deleteByPrimaryKey(Long wechatFanLabelId);

    int insert(WechatFanLabel record);

    int insertSelective(WechatFanLabel record);

    WechatFanLabel selectByPrimaryKey(Long wechatFanLabelId);

    int updateByPrimaryKeySelective(WechatFanLabel record);

    int updateByPrimaryKey(WechatFanLabel record);

    /**
     * 
     * selectWechatFanLabelExpList:(根据标签查询对应的粉丝列表). <br/>
     * Date: 2015年9月3日 下午5:25:24 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @param wechatFansLabelId
     * @param wechatFansId
     * @param page
     * @return
     */
	List<WechatFanLabelExp> selectWechatFanLabelExpList(Long wechatFansLabelId,Long wechatFansId, Page<WechatFanLabelExp> page);
	
	/**
	 * 
	 * selectWechatFanLabelList:(根据粉丝id查询对应的标签). <br/>
	 * Date: 2015年9月3日 下午6:32:54 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFansId
	 * @return
	 */
	List<WechatFanLabelExp> selectWechatFanLabelList(Long wechatFansId);
}