/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMsgQueueRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年12月9日下午9:42:23
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatMsgQueueMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueue;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueExp;
import com.bw.adv.module.wechat.repository.WechatMsgQueueRepository;

/**
 * ClassName:WechatMsgQueueRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午9:42:23 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatMsgQueueRepositoryImpl extends BaseRepositoryImpl<WechatMsgQueue, WechatMsgQueueMapper> implements WechatMsgQueueRepository{

	@Override
	protected Class<WechatMsgQueueMapper> getMapperClass() {
		
		return WechatMsgQueueMapper.class;
	}

	@Override
	public List<WechatMsgQueueExp> findAllWechatMsgQueues(Long limit) {
		return this.getMapper().findAllWechatMsgQueues(limit);
	}
	
	@Override
	public int deleteByPrimaryKey(Long wechatMsgQueueId){
		return this.getMapper().deleteByPrimaryKey(wechatMsgQueueId);
	}
	
	@Override
	public void deleteAllWechatMsgQueue(List<Long> ids){
		this.getMapper().deleteBatch(ids);
	}
	
	@Override
	public void updateBatchWechatMsgQueue(List<WechatMsgQueueExp> list){
		this.getMapper().updateBatchWechatMsgQueue(list);
	}
	
	@Override
	public List<WechatMsgQueueExp> findExpByLimit(int start,int size){
		return this.getMapper().selectExpByLimit(start, size);
	}
	
	/**
	 * TODO 查询所有未发送的消息数量（可选）.
	 * @see com.bw.adv.module.wechat.repository.WechatMsgQueueRepository#findAllCount()
	 */
	public int findAllCount(){
		return this.getMapper().selectAllCount();
	}
	
	@Override
	public void updateQueueToY() {
		this.getMapper().updateQueueToY();
	}

	@Override
	public void deleteQueueWithR() {
		this.getMapper().deleteQueueWithR();
	}
	
}

