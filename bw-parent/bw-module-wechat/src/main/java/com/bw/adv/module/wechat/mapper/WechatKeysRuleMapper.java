package com.bw.adv.module.wechat.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.WechatKeysRuleExp;

public interface WechatKeysRuleMapper extends BaseMapper<WechatKeysRule>{
    int deleteByPrimaryKey(Long wechatKeysRuleId);

    int insert(WechatKeysRule record);

    int insertSelective(WechatKeysRule record);

    WechatKeysRule selectByPrimaryKey(Long wechatKeysRuleId);

    int updateByPrimaryKeySelective(WechatKeysRule record);

    int updateByPrimaryKey(WechatKeysRule record);
    
    /**
     * selectWechatKeyRules:(查询有效的关键字规则组). <br/>
     * Date: 2015年8月26日 下午5:38:52 <br/>
     * scrmVersion 1.0
     * @author june
     * @version jdk1.7
     * @return
     */
	List<WechatKeysRule> selectWechatKeyRules();
	
	/**
	 * selectWechatKeyRuleExpByKeyRule:(查询扩展类). <br/>
	 * Date: 2015年8月26日 下午5:38:24 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatKeysRule
	 * @return
	 */
	WechatKeysRuleExp selectWechatKeyRuleExpByKeyRule(WechatKeysRule wechatKeysRule);
    
}