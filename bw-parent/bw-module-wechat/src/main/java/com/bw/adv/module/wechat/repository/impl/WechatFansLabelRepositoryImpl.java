/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatFansLabelRepositoryImpl.java
 * Package Name:com.sage.scrm.module.wechat.repository.impl
 * Date:2015年8月17日上午10:46:47
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.wechat.mapper.WechatFansLabelMapper;
import com.bw.adv.module.wechat.model.WechatFansLabel;
import com.bw.adv.module.wechat.repository.WechatFansLabelRepository;

/**
 * ClassName:WechatFansLabelRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 上午10:46:47 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class WechatFansLabelRepositoryImpl extends BaseRepositoryImpl<WechatFansLabel, WechatFansLabelMapper> implements WechatFansLabelRepository {

	@Override
	protected Class<WechatFansLabelMapper> getMapperClass() {
		return WechatFansLabelMapper.class;
	}
	

	@Override
	public List<WechatFansLabel> queryWechatFansLabelByLabelName(String labelName) {
		return this.getMapper().selectWechatFansLabelByLableName(labelName);
	}
	

	@Override
	public List<WechatFansLabel> findWechatFansLabels() {
		return this.getMapper().selectWechatFansLabels();
	}



	
}

