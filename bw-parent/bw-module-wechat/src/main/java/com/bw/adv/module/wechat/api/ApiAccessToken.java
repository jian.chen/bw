/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:AccessTokenAPI.java
 * Package Name:com.sage.scrm.module.wechat.service.api
 * Date:2015年8月12日下午2:59:47
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.wechat.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.sf.json.JSONObject;

import com.bw.adv.module.wechat.util.HttpKit;


/**
 * ClassName:AccessTokenAPI <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月12日 下午2:59:47 <br/>
 * scrmVersion 1.0
 * 
 * @author june
 * @version jdk1.7
 * @see
 */
@Component
public class ApiAccessToken {

	
	@Value("${appId}")
	private String appId;
	@Value("${appSecret}")
	private String appSecret ;
	
	/**
	 * 获取accessToken和凭证有效时间 getAccessToken:(这里用一句话描述这个方法的作用). <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	public Map<String,String> getAccessToken() throws KeyManagementException,NoSuchAlgorithmException, NoSuchProviderException,UnsupportedEncodingException, IOException, ExecutionException,InterruptedException {

		String accessToken = null;
		String expiresIn = null;
		Map<String,String> map = null;
		String accessTokenUrl =null;
		String result =null;
		JSONObject jsonObject =null;
		
		try {
			map = new HashMap<String, String>();
			accessTokenUrl = ApiWeChat.ACCESS_TOKEN.replace("APPID",appId).replace("APPSECRET", appSecret);
			result = HttpKit.get(accessTokenUrl);
			jsonObject = JSONObject.fromObject(result);

			accessToken = jsonObject.getString("access_token");
			expiresIn = jsonObject.getInt("expires_in") + "";

			map.put("access_token", accessToken);
			map.put("expires_in", expiresIn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 
	 * getAccessTokenString:(获取accessToken). <br/>
	 * Date: 2015年8月12日 下午4:28:11 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
	public String getAccessTokenString() throws KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException, UnsupportedEncodingException, IOException, ExecutionException, InterruptedException{
		
		return (String)getAccessToken().get("access_token");
	}
}
