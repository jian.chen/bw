package com.bw.adv.module.alipay.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.alipay.model.AlipayFans;
import com.bw.adv.module.common.model.Example;

public interface AlipayFansMapper extends BaseMapper<AlipayFans>{
    int deleteByPrimaryKey(Long alipayFansId);

    int insert(AlipayFans record);

    int insertSelective(AlipayFans record);

    AlipayFans selectByPrimaryKey(Long alipayFansId);

    int updateByPrimaryKeySelective(AlipayFans record);

    int updateByPrimaryKey(AlipayFans record);
    
    AlipayFans selectByUserId(String userId);
    
    List<AlipayFans> selectByExample(@Param("example")Example example,Page<AlipayFans> page);
}