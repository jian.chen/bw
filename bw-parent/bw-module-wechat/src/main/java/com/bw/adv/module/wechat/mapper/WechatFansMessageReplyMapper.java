package com.bw.adv.module.wechat.mapper;

import com.bw.adv.module.wechat.model.WechatFansMessageReply;

public interface WechatFansMessageReplyMapper {
    int deleteByPrimaryKey(Long wechatFansMessageReplyId);

    int insert(WechatFansMessageReply record);

    int insertSelective(WechatFansMessageReply record);

    WechatFansMessageReply selectByPrimaryKey(Long wechatFansMessageReplyId);

    int updateByPrimaryKeySelective(WechatFansMessageReply record);

    int updateByPrimaryKey(WechatFansMessageReply record);
}