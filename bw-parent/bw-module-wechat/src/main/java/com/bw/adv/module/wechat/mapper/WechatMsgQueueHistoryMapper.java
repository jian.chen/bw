package com.bw.adv.module.wechat.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatMsgQueueHistory;

public interface WechatMsgQueueHistoryMapper extends BaseMapper<WechatMsgQueueHistory>{
    int deleteByPrimaryKey(Long wechatMsgQueueId);

    int insert(WechatMsgQueueHistory record);

    int insertSelective(WechatMsgQueueHistory record);

    WechatMsgQueueHistory selectByPrimaryKey(Long wechatMsgQueueId);

    int updateByPrimaryKeySelective(WechatMsgQueueHistory record);

    int updateByPrimaryKey(WechatMsgQueueHistory record);
}