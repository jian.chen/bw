/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMessageTemplateRepository.java
 * Package Name:com.sage.scrm.module.wechat.repository
 * Date:2015年8月18日下午4:43:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.repository;


import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.wechat.mapper.WechatTemplateMessageItemMapper;
import com.bw.adv.module.wechat.model.WechatTemplateMessageItem;

public interface WechatTemplateMessageItemRepository extends BaseRepository<WechatTemplateMessageItem, WechatTemplateMessageItemMapper> {
	
	 public WechatTemplateMessageItem findWechatTemplateMessageItemByTemplateMessId(Long templateMessId);
	 
}

