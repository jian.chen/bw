/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsShowRepositoryImpl.java
 * Package Name:com.sage.scrm.module.goods.repository.impl
 * Date:2015年12月9日上午10:50:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.goods.mapper.GoodsExchangeItemMapper;
import com.bw.adv.module.goods.model.GoodsExchangeItem;
import com.bw.adv.module.goods.repository.GoodsExchangeRepository;

@Repository
public class GoodsExchangeRepositoryImpl extends BaseRepositoryImpl<GoodsExchangeItem, GoodsExchangeItemMapper> 
	implements GoodsExchangeRepository{

	@Override
	protected Class<GoodsExchangeItemMapper> getMapperClass() {
		
		return GoodsExchangeItemMapper.class;
	}

	@Override
	public List<GoodsExchangeItem> findByMemberId(Long memberId,Page<GoodsExchangeItem> page) {
		
		return this.getMapper().selectByMemberId(memberId,page);
	}


}

