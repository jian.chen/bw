/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsShowRepository.java
 * Package Name:com.sage.scrm.module.goods.repository.impl
 * Date:2015年12月9日上午10:48:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.goods.mapper.GoodsShowMapper;
import com.bw.adv.module.goods.model.GoodsShow;
import com.bw.adv.module.goods.model.exp.GoodsShowExp;

/**
 * ClassName:GoodsShowRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 上午10:48:29 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface GoodsShowRepository extends BaseRepository<GoodsShow, GoodsShowMapper> {
	
	/**
	 * 
	 * findListByStatus:(根据状态查询商品展示列表(不等于传入状态的所有数据)). <br/>
	 * Date: 2015年12月9日 上午11:09:42 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param pageObj
	 * @param statusId
	 * @return
	 */
	public List<GoodsShowExp> findListByStatus(Page<GoodsShowExp> pageObj,Long statusId);
	
	/**
	 * findPointsProductByStatus:根据状态查商品兑换信息<br/>
	 * Date: 2015年12月14日 下午9:28:53 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param statusId
	 * @param pageObj
	 * @return
	 */
	public List<GoodsShowExp> findPointsProductByStatus(Long statusId, Page<GoodsShowExp> pageObj);
	/**
	 * findPointsProductByStatus:根据状态内外部查商品兑换信息<br/>
	 * Date: 2015年12月14日 下午9:28:53 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param statusId
	 * @param pageObj
	 * @return
	 */
	public List<GoodsShowExp> findExtPointsProductByStatus(Long statusId, Page<GoodsShowExp> pageObj);
	/**
	 * 
	 * removeList:(批量删除商品展示(逻辑删除)). <br/>
	 * Date: 2015年12月9日 下午4:15:08 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowIds
	 * @param statusId
	 * @return
	 */
	public int removeList(String goodsShowIds,Long statusId);
	
	/**
	 * 
	 * findExpByPk:(根据Id查询商品展示详细信息). <br/>
	 * Date: 2015年12月10日 下午2:57:38 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowId
	 * @return
	 */
	public GoodsShowExp findExpByPk(String goodsShowId);
	
	/**
	 * findExpByExample:根据条件查询积分商城详情<br/>
	 * Date: 2015年12月15日 下午8:42:47 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	public GoodsShowExp findExpByExample(Example example);

}

