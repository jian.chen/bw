/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:MemberPointsItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.points.repository.impl
 * Date:2015年8月20日下午6:13:59
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.points.mapper.MemberPointsItemRelMapper;
import com.bw.adv.module.points.model.MemberPointsItemRel;
import com.bw.adv.module.points.repository.MemberPointsItemRelRepository;

/**
 * ClassName:MemberPointsItemRelRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:13:59 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberPointsItemRelRepositoryImpl extends BaseRepositoryImpl<MemberPointsItemRel, MemberPointsItemRelMapper> implements MemberPointsItemRelRepository {

	@Override
	protected Class<MemberPointsItemRelMapper> getMapperClass() {
		return MemberPointsItemRelMapper.class;
	}

}

