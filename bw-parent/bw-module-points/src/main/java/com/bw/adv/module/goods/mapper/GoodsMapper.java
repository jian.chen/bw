package com.bw.adv.module.goods.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.goods.model.Goods;

public interface GoodsMapper extends BaseMapper<Goods> {
   
}