/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:PointsRuleRepositoryImpl.java
 * Package Name:com.sage.scrm.module.points.repository.impl
 * Date:2015年8月20日下午4:59:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.points.mapper.PointsRuleMapper;
import com.bw.adv.module.points.model.PointsRule;
import com.bw.adv.module.points.repository.PointsRuleRepository;

/**
 * ClassName:PointsRuleRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午4:59:50 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class PointsRuleRepositoryImpl extends BaseRepositoryImpl<PointsRule, PointsRuleMapper> implements PointsRuleRepository {

	@Override
	protected Class<PointsRuleMapper> getMapperClass() {
		
		return PointsRuleMapper.class;
	}

	@Override
	public List<PointsRule> findByIsActive(String isActive,Page<PointsRule> page) {
		return this.getMapper().selectByIsActive(isActive,page);
	}
	
	@Override
	public List<PointsRule> findByPointsTypeId(Long pointsTypeId) {
		return this.getMapper().selectByPointsTypeId(pointsTypeId);
	}
	@Override
	public void removePointsRuleByPointsRuleId(Long pointsRuleId) {
		this.getMapper().deletePointsRuleByPointsRuleId(pointsRuleId);
	}

	@Override
	public List<PointsRule> findByPointsRuleName(String isActive,
			String pointsRuleName) {
		return this.getMapper().selectByPointsRuleName(isActive, pointsRuleName);
	}

	@Override
	public List<PointsRule> findByIsActive(String isActive) {
		return this.getMapper().selectByIsActive(isActive);
	}

}

