package com.bw.adv.module.goods.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.goods.model.GoodsType;

public interface GoodsTypeMapper extends BaseMapper<GoodsType>{
   
}