package com.bw.adv.module.badge.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.badge.model.MemberBadgeItem;


public interface MemberBadgeItemMapper extends BaseMapper<MemberBadgeItem> {
}