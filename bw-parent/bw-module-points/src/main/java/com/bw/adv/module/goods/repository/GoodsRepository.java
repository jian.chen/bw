/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsRepository.java
 * Package Name:com.sage.scrm.module.goods.repository
 * Date:2015年12月23日下午2:50:48
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.goods.mapper.GoodsMapper;
import com.bw.adv.module.goods.model.Goods;

/**
 * ClassName:GoodsRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月23日 下午2:50:48 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface GoodsRepository extends BaseRepository<Goods, GoodsMapper>{

}

