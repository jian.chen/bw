package com.bw.adv.module.points.mapper;

import java.math.BigDecimal;
import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.points.model.MemberPointsItem;
import com.bw.adv.module.points.model.MemberPointsItemRel;
import com.bw.adv.module.points.model.PointsType;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.points.model.exp.MemberPointsItemStoreExp;

import org.apache.ibatis.annotations.Param;

public interface MemberPointsItemMapper extends BaseMapper<MemberPointsItem> {
	
	public List<MemberPointsItem> selectAll(Page<MemberPointsItem> page);
	
	List<MemberPointsItem> selectByExample(@Param("example")Example example,Page<MemberPointsItem> page);
	
	List<MemberPointsItemExp> selectExpByExample(@Param("example")Example example,Page<MemberPointsItemExp> page);
	
	List<PointsType> selectPointsTypeList();
	

	/**
	 * selectInvalidDatePointsItems:(查询过期的积分记录). <br/>
	 * Date: 2015-10-9 下午5:35:23 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param invalidDate
	 * @return
	 */
	public List<MemberPointsItemExp> selectInvalidDatePointsItems(@Param("invalidDate")String invalidDate);
	
	/**
	 * updateBatchByCleanPointsItems:(积分清理). <br/>
	 * Date: 2015-10-9 下午5:35:23 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param list
	 * @return
	 */
	int updateBatchByCleanPointsItems(List<MemberPointsItem> list);

	/**
	 * selectByOrderId:(根据订单查询积分记录). <br/>
	 * Date: 2016-1-8 下午1:59:47 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param orderId
	 * @return
	 */
	public List<MemberPointsItem> selectByOrderId(Long orderId);

	
	/**
	 * selectAvailablePointsItems:(查询会员的可用积分). <br/>
	 * Date: 2016-1-12 下午3:15:14 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param currentDate
	 * @return
	 */
	public List<MemberPointsItem> selectAvailablePointsItems(Long memberId,String currentDate);

	/**
	 * updateMemberAccount:更新会员账号积分信息. <br/>
	 * Date: 2016年1月20日 下午5:03:14 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberAccount
	 * @return
	 */
	 int updateMemberAccountPoints(@Param("memberAccount")MemberAccount memberAccount,@Param("pointReviseNumber")BigDecimal pointReviseNumber);
	 
	 /**
	 * selectPointsReviseList:查询积分调账记录. <br/>
	 * Date: 2016年1月21日 上午10:41:51 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<MemberPointsItemExp> selectPointsReviseList(@Param("example")Example example,Page<MemberPointsItemExp> page);
	
	 /**
	 * selectListSearchAuto:查询会员积分信息. <br/>
	 * Date: 2016年1月21日 上午10:42:13 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberCode
	 * @return
	 */
	MemberPointsItemExp selectListSearchAuto(String memberCode);
	
	/**
	 * selectAddPointsInfo:查询会员即将过期的新增积分记录. <br/>
	 * Date: 2016年1月25日 下午9:56:52 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<MemberPointsItemExp> selectAddPointsInfo(MemberPointsItem memberPointsItem);
	
	
	/**
	 * updateAvailablePointsInfo:更新积分记录表可用积分额度. <br/>
	 * Date: 2016年1月26日 上午10:28:23 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItem
	 * @return
	 */
	int updateAvailablePointsInfo(MemberPointsItem memberPointsItem);
	
	
	/**
	 * insertPointsItemRel:新增积分扣除关系记录. <br/>
	 * Date: 2016年1月26日 上午11:12:29 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItemRel
	 * @return
	 */
	int insertPointsItemRel(MemberPointsItemRel memberPointsItemRel);

	List<MemberPointsItemStoreExp> selectStoreExpByMemberCode(@Param("memberCode") String memberCode,
															  @Param("pageSize") int pageSize,
															  @Param("start") int start);

}