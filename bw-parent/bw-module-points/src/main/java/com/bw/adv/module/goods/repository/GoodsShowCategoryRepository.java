/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsShowCategoryRepository.java
 * Package Name:com.sage.scrm.module.goods.repository
 * Date:2015年12月23日下午2:52:52
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.goods.mapper.GoodsShowCategoryMapper;
import com.bw.adv.module.goods.model.GoodsShowCategory;

/**
 * ClassName:GoodsShowCategoryRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月23日 下午2:52:52 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface GoodsShowCategoryRepository extends BaseRepository<GoodsShowCategory, GoodsShowCategoryMapper> {

	/**
	 * 
	 * findListByStatus:(根据状态查询商品展示类别List). <br/>
	 * Date: 2015年12月23日 下午4:35:55 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param pageObj
	 * @param statusId
	 * @return
	 */
	public List<GoodsShowCategory> findListByStatus(Page<GoodsShowCategory> pageObj,Long statusId);
	
	/**
	 * 
	 * removeList:(批量删除商品展示类别). <br/>
	 * Date: 2015年12月23日 下午4:36:43 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowCategoryIds
	 * @param statusId
	 * @return
	 */
	public int removeList(String goodsShowCategoryIds,Long statusId);
	
}

