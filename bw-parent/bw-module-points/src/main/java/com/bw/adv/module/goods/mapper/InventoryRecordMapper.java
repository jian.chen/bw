package com.bw.adv.module.goods.mapper;

import com.bw.adv.module.goods.model.InventoryRecord;

public interface InventoryRecordMapper {
    int deleteByPrimaryKey(Long inventoryRecordId);

    int insert(InventoryRecord record);

    int insertSelective(InventoryRecord record);

    InventoryRecord selectByPrimaryKey(Long inventoryRecordId);

    int updateByPrimaryKeySelective(InventoryRecord record);

    int updateByPrimaryKey(InventoryRecord record);
}