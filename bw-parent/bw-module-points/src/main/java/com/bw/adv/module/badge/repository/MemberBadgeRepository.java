/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:MemberPointsItemRepository.java
 * Package Name:com.sage.scrm.module.points.repository
 * Date:2015年8月20日下午6:13:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.badge.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.badge.mapper.MemberBadgeMapper;
import com.bw.adv.module.badge.model.MemberBadge;
import com.bw.adv.module.badge.model.exp.MemberBadgeExt;

/**
 * ClassName:MemberPointsItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:13:42 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberBadgeRepository extends BaseRepository<MemberBadge, MemberBadgeMapper> {

	/**
	 * 查询会员是否存在此徽章
	 * @param memberId
	 * @param awardsId 徽章类型
	 * @return
	 */
	MemberBadge findMemberBadgeByMemberId(Long memberId, Long awardsId);

	/**
	 * 查询会员徽章
	 * @param memberId
	 * @return
	 */
	List<MemberBadgeExt> findMemberBadgeExtByMemberId(Long memberId);

	/**
	 * 较少会员徽章
	 * @param memberId
	 * @return
	 */
	int updateMemberBadgeGiftExchange(Long memberId);
	

}

