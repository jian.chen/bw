/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsShowRepositoryImpl.java
 * Package Name:com.sage.scrm.module.goods.repository.impl
 * Date:2015年12月9日上午10:50:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.goods.mapper.GoodsShowMapper;
import com.bw.adv.module.goods.model.GoodsShow;
import com.bw.adv.module.goods.model.exp.GoodsShowExp;
import com.bw.adv.module.goods.repository.GoodsShowRepository;
/**
 * ClassName:GoodsShowRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 上午10:50:25 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class GoodsShowRepositoryImpl extends BaseRepositoryImpl<GoodsShow, GoodsShowMapper> 
	implements GoodsShowRepository{

	@Override
	protected Class<GoodsShowMapper> getMapperClass() {
		
		return GoodsShowMapper.class;
	}

	@Override
	public List<GoodsShowExp> findListByStatus(Page<GoodsShowExp> pageObj,
			Long statusId) {
		
		return this.getMapper().selectListByStatus(statusId,pageObj);
	}

	@Override
	public int removeList(String goodsShowIds, Long statusId) {
		
		return this.getMapper().deleteList(goodsShowIds, statusId);
	}

	@Override
	public GoodsShowExp findExpByPk(String goodsShowId) {
		
		return this.getMapper().selectExpByPK(goodsShowId);
	}

	@Override
	public List<GoodsShowExp> findPointsProductByStatus(Long statusId,Page<GoodsShowExp> page) {
		
		return this.getMapper().selectPointsProductByStatus(statusId, page);
	}

	@Override
	public GoodsShowExp findExpByExample(Example example) {
		
		return this.getMapper().selectExpByExample(example);
	}

	@Override
	public List<GoodsShowExp> findExtPointsProductByStatus(Long statusId,  Page<GoodsShowExp> pageObj) {
		return this.getMapper().selectExtPointsProductByStatus(statusId, pageObj);
	}
}

