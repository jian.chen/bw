/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:MemberBadgeItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.points.repository.impl
 * Date:2015年8月20日下午6:13:59
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.badge.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.badge.mapper.MemberBadgeItemMapper;
import com.bw.adv.module.badge.model.MemberBadgeItem;
import com.bw.adv.module.badge.repository.MemberBadgeItemRepository;

/**
 * ClassName:MemberBadgeItemRelRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:13:59 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberBadgeItemRepositoryImpl extends BaseRepositoryImpl<MemberBadgeItem, MemberBadgeItemMapper> implements MemberBadgeItemRepository {

	@Override
	protected Class<MemberBadgeItemMapper> getMapperClass() {
		return MemberBadgeItemMapper.class;
	}

}

