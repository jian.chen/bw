package com.bw.adv.module.goods.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.goods.model.GoodsExchangeItem;

public interface GoodsExchangeItemMapper extends BaseMapper<GoodsExchangeItem>{
    
    List<GoodsExchangeItem> selectByMemberId(@Param("memberId")Long memberId,@Param("page")Page<GoodsExchangeItem> page);
}