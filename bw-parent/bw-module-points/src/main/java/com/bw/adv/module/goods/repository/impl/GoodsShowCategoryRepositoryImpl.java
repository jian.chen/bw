/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsShowCategoryRepositoryImpl.java
 * Package Name:com.sage.scrm.module.goods.repository.impl
 * Date:2015年12月23日下午2:53:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.goods.mapper.GoodsShowCategoryMapper;
import com.bw.adv.module.goods.model.GoodsShowCategory;
import com.bw.adv.module.goods.repository.GoodsShowCategoryRepository;

/**
 * ClassName:GoodsShowCategoryRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月23日 下午2:53:39 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class GoodsShowCategoryRepositoryImpl extends BaseRepositoryImpl<GoodsShowCategory, GoodsShowCategoryMapper> 
	implements GoodsShowCategoryRepository{

	@Override
	protected Class<GoodsShowCategoryMapper> getMapperClass() {
		
		return GoodsShowCategoryMapper.class;
	}

	@Override
	public List<GoodsShowCategory> findListByStatus(
			Page<GoodsShowCategory> pageObj, Long statusId) {
		
		return this.getMapper().selectListByStatus(statusId, pageObj);
	}

	@Override
	public int removeList(String goodsShowCategoryIds, Long statusId) {
		
		return this.getMapper().deleteList(goodsShowCategoryIds, statusId);
	}

}

