/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:MemberPointsItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.points.repository.impl
 * Date:2015年8月20日下午6:13:59
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.points.mapper.MemberPointsItemMapper;
import com.bw.adv.module.points.model.MemberPointsItem;
import com.bw.adv.module.points.model.MemberPointsItemRel;
import com.bw.adv.module.points.model.PointsType;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.points.model.exp.MemberPointsItemStoreExp;
import com.bw.adv.module.points.repository.MemberPointsItemRepository;
import com.bw.adv.module.tools.DateUtils;

import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * ClassName:MemberPointsItemRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:13:59 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberPointsItemRepositoryImpl extends BaseRepositoryImpl<MemberPointsItem, MemberPointsItemMapper> implements MemberPointsItemRepository {

	@Override
	protected Class<MemberPointsItemMapper> getMapperClass() {
		return MemberPointsItemMapper.class;
	}

	@Override
	public List<MemberPointsItemExp> findExpByExample(Example example,Page<MemberPointsItemExp> page) {
		return this.getMapper().selectExpByExample(example,page);
	}

	@Override
	public List<PointsType> findPointsTypeList() {
		return this.getMapper().selectPointsTypeList();
	}

	@Override
	public List<MemberPointsItemExp> findInvalidDatePointsItems(String invalidDate) {
		return this.getMapper().selectInvalidDatePointsItems(invalidDate);
	}

	/**
	 * TODO 根据订单查询积分记录（）.
	 */
	@Override
	public List<MemberPointsItem> findByOrderId(Long orderId) {
		return this.getMapper().selectByOrderId(orderId);
	}

	@Override
	public List<MemberPointsItem> findAvailablePointsItems(Long memberId) {
		String currentDate = DateUtils.getCurrentDateOfDb();
		return this.getMapper().selectAvailablePointsItems(memberId,currentDate);
	}

	@Override
	public int insertPointsRevise(MemberPointsItem memberPointsItem) {
		return this.getMapper().insertSelective(memberPointsItem);
	}

	@Override
	public int updateMemberAccount(MemberAccount memberAccount, BigDecimal pointReviseNumber) {
		return this.getMapper().updateMemberAccountPoints(memberAccount,pointReviseNumber);
	}

	@Override
	public List<MemberPointsItemExp> findPointsReviseList(Example example, Page<MemberPointsItemExp> page) {
		return this.getMapper().selectPointsReviseList(example,page);
	}

	@Override
	public MemberPointsItemExp findListSearchAuto(String memberCode) {
		return this.getMapper().selectListSearchAuto(memberCode);
	}

	@Override
	public List<MemberPointsItemExp> findAddPointsInfo(MemberPointsItem memberPointsItem) {
		return this.getMapper().selectAddPointsInfo(memberPointsItem);
	}

	@Override
	public int updateAvailablePointsInfo(MemberPointsItem memberPointsItem) {
		return this.getMapper().updateAvailablePointsInfo(memberPointsItem);
	}

	@Override
	public int insertPointsItemRel(MemberPointsItemRel memberPointsItemRel) {
		return this.getMapper().insertPointsItemRel(memberPointsItemRel);
	}

	@Override
	public List<MemberPointsItemStoreExp> findStoreExpByMemberCode(String memberCode, int pageSize, int pageNo) {
		return this.getMapper().selectStoreExpByMemberCode(memberCode, pageSize,
				(pageNo -1) * pageSize < 0 ? 0 : (pageNo - 1) * pageSize);
	}


}

