package com.bw.adv.module.points.mapper;

import com.bw.adv.module.points.model.PointsType;

public interface PointsTypeMapper {
    int deleteByPrimaryKey(Long pointsTypeId);

    int insert(PointsType record);

    int insertSelective(PointsType record);

    PointsType selectByPrimaryKey(Long pointsTypeId);

    int updateByPrimaryKeySelective(PointsType record);

    int updateByPrimaryKey(PointsType record);
}