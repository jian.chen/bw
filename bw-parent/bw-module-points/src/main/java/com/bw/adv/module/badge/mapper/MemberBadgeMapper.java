package com.bw.adv.module.badge.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.badge.model.MemberBadge;
import com.bw.adv.module.badge.model.exp.MemberBadgeExt;

public interface MemberBadgeMapper extends BaseMapper<MemberBadge> {

	/**
	 * 查询会员的卡牌
	 * @param memberId
	 * @param awardsId
	 * @return
	 */
	MemberBadge selectMemberBadgeByMemberId(@Param("memberId")Long memberId,@Param("awardsId") Long awardsId);
	
	/**
	 * 根据会员id查询会员的卡牌
	 * @param memberId
	 * @param awardsId
	 * @return
	 */
	List<MemberBadgeExt> selectMemberBadgeExtByMemberId(@Param("memberId")Long memberId);

	/**
	 * 每个徽章的数量减一
	 * @param memberId
	 * @return
	 */
	int updateMemberBadgeGiftExchange(Long memberId);
}