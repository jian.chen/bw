/**
 * Copyright (C), 2010-2015,百威.
 * Project Name:scrm-module-points
 * File Name:MemberBadgeItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.points.repository.impl
 * Date:2015年8月20日下午6:13:59
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.badge.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.badge.mapper.MemberBadgeMapper;
import com.bw.adv.module.badge.model.MemberBadge;
import com.bw.adv.module.badge.model.exp.MemberBadgeExt;
import com.bw.adv.module.badge.repository.MemberBadgeRepository;

/**
 * ClassName:MemberBadgeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:13:59 <br/>
 * scrmVersion 1.0
 * @author  chenjian
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberBadgeRepositoryImpl extends BaseRepositoryImpl<MemberBadge, MemberBadgeMapper> implements MemberBadgeRepository {

	@Override
	protected Class<MemberBadgeMapper> getMapperClass() {
		return MemberBadgeMapper.class;
	}

	@Override
	public MemberBadge findMemberBadgeByMemberId(Long memberId, Long awardsId) {
		return this.getMapper().selectMemberBadgeByMemberId(memberId,awardsId);
	}

	@Override
	public List<MemberBadgeExt> findMemberBadgeExtByMemberId(Long memberId) {
		return this.getMapper().selectMemberBadgeExtByMemberId(memberId);
	}

	@Override
	public int updateMemberBadgeGiftExchange(Long memberId) {
		return this.getMapper().updateMemberBadgeGiftExchange(memberId);
	}
}

