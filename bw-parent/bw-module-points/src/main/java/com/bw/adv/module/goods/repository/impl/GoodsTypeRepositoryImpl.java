/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsTypeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.goods.repository.impl
 * Date:2015年12月23日下午2:55:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.goods.mapper.GoodsTypeMapper;
import com.bw.adv.module.goods.model.GoodsType;
import com.bw.adv.module.goods.repository.GoodsTypeRepository;

/**
 * ClassName:GoodsTypeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月23日 下午2:55:46 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class GoodsTypeRepositoryImpl extends BaseRepositoryImpl<GoodsType, GoodsTypeMapper>
	implements GoodsTypeRepository{

	@Override
	protected Class<GoodsTypeMapper> getMapperClass() {
		
		return GoodsTypeMapper.class;
	}

}

