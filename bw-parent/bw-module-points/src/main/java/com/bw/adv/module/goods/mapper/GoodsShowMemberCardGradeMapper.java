/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsShowMemberCardGradeMapper.java
 * Package Name:com.sage.scrm.module.goods.mapper
 * Date:2016年7月18日下午7:12:20
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.mapper;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.goods.model.GoodsShowMemberCardGrade;

/**
 * ClassName:GoodsShowMemberCardGradeMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年7月18日 下午7:12:20 <br/>
 * scrmVersion standard
 * @author   dong.d
 * @version  jdk1.7
 * @see 	 
 */
public interface GoodsShowMemberCardGradeMapper extends BaseMapper<GoodsShowMemberCardGrade>{

	
	
	
	/**
	 * 批量插入积分兑换要求信息
	 * Date: 2016年7月18日 下午7:13:20 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @return
	 */
	int insertGoodsShowMemverCardGrade(@Param("goodsShowMemberCardGradeList")List<GoodsShowMemberCardGrade> goodsShowMemberCardGradeList);
	
	
	
	/**
	 * updateGoodsShowMemberCardGrade:(积分兑换要求信息). <br/>
	 * Date: 2016年7月18日 下午8:30:14 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param goodsShowMemberCardGradeList
	 * @return
	 */
	int updateGoodsShowMemberCardGrade(GoodsShowMemberCardGrade goodsShowMemberCardGrade);
	
	
	
	/**
	 * selectGoodsShowMemberCardGradeListByGoodsShowId:(根据兑换单ID获取会员等级ID与积分的关系列表). <br/>
	 * Date: 2016年7月19日 上午11:40:10 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param goodsShowId
	 * @return
	 */
	List<GoodsShowMemberCardGrade> selectGoodsShowMemberCardGradeListByGoodsShowId(@Param("goodsShowId")Long goodsShowId);
	
	
	
	/**
	 * selectGoodsShowMemberCardGradeListByIds:(根据兑换单ID列表获取会员等级ID与积分的关系列表). <br/>
	 * Date: 2016年7月25日 上午11:28:25 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param goodsIds
	 * @return
	 */
	List<GoodsShowMemberCardGrade> selectGoodsShowMemberCardGradeListByIds(@Param("goodsIds") Set<Long> goodsIds);
	
	
	
	
}

