package com.bw.adv.module.points.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.points.model.PointsRule;

public interface PointsRuleMapper extends BaseMapper<PointsRule> {
	
	public List<PointsRule> selectByIsActive(@Param("isActive")String isActive,Page<PointsRule> page);
	
	/**
	 * selectByPointsTypeId:(根据积分类型查询积分规则). <br/>
	 * Date: 2015-8-26 上午11:35:37 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param pointsTypeId
	 * @return
	 */
	public List<PointsRule> selectByPointsTypeId(Long pointsTypeId);
	
	/**
	 * selectByPointsRuleName:(通过规则名称查询). <br/>
	 * Date: 2015-9-22 下午4:09:23 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param isActive
	 * @param pointsRuleName
	 * @return
	 */
	public List<PointsRule> selectByPointsRuleName(@Param("isActive") String isActive,@Param("pointsRuleName") String pointsRuleName);
	
	public void deletePointsRuleByPointsRuleId(Long pointsRuleId);
	
	/**
	 * selectByIsActive:(查询启用的积分规则). <br/>
	 * Date: 2015-9-8 下午6:25:04 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param isActive
	 * @return
	 */
	public List<PointsRule> selectByIsActive(@Param("isActive")String isActive);
	
}

