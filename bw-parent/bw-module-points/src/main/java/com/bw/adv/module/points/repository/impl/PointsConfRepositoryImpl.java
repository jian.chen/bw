/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:PointsRuleRepository.java
 * Package Name:com.sage.scrm.module.points.repository
 * Date:2015年8月20日下午4:58:47
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.points.mapper.PointsConfMapper;
import com.bw.adv.module.points.model.PointsConf;
import com.bw.adv.module.points.repository.PointsConfRepository;

/**
 * ClassName: PointsConfRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016-3-18 下午3:56:28 <br/>
 * scrmVersion standard
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class PointsConfRepositoryImpl extends BaseRepositoryImpl<PointsConf, PointsConfMapper> implements PointsConfRepository {

	@Override
	protected Class<PointsConfMapper> getMapperClass() {
		return PointsConfMapper.class;
	}


}

