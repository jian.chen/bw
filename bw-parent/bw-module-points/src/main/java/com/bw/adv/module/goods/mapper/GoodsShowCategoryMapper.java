package com.bw.adv.module.goods.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.goods.model.GoodsShowCategory;

public interface GoodsShowCategoryMapper extends BaseMapper<GoodsShowCategory> {
	
	List<GoodsShowCategory> selectListByStatus(@Param("statusId")Long statusId,Page<GoodsShowCategory> page);
	
	int deleteList(@Param("goodsShowCategoryIds")String goodsShowCategoryIds,@Param("statusId")Long statusId);
   
}