/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:MemberPointsItemRepository.java
 * Package Name:com.sage.scrm.module.points.repository
 * Date:2015年8月20日下午6:13:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.points.mapper.MemberPointsItemRelMapper;
import com.bw.adv.module.points.model.MemberPointsItemRel;

/**
 * MemberPointsItemRelRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:13:42 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberPointsItemRelRepository extends BaseRepository<MemberPointsItemRel, MemberPointsItemRelMapper> {

}

