/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsTypeRepository.java
 * Package Name:com.sage.scrm.module.goods.repository
 * Date:2015年12月23日下午2:55:03
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.goods.mapper.GoodsTypeMapper;
import com.bw.adv.module.goods.model.GoodsType;

/**
 * ClassName:GoodsTypeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月23日 下午2:55:03 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface GoodsTypeRepository extends BaseRepository<GoodsType, GoodsTypeMapper> {

}

