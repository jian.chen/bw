/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:MemberPointsItemRepository.java
 * Package Name:com.sage.scrm.module.points.repository
 * Date:2015年8月20日下午6:13:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.repository;

import java.math.BigDecimal;
import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.points.mapper.MemberPointsItemMapper;
import com.bw.adv.module.points.model.MemberPointsItem;
import com.bw.adv.module.points.model.MemberPointsItemRel;
import com.bw.adv.module.points.model.PointsType;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.points.model.exp.MemberPointsItemStoreExp;

import org.apache.ibatis.annotations.Param;

/**
 * ClassName:MemberPointsItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:13:42 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberPointsItemRepository extends BaseRepository<MemberPointsItem, MemberPointsItemMapper> {
	
	/**
	 * 
	 * findExpByExample:根据example查询会员积分list
	 * Date: 2015年9月2日 下午3:49:40 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<MemberPointsItemExp> findExpByExample(Example example,Page<MemberPointsItemExp> page);
	
	/**
	 * 
	 * findPointsTypeList:查询所有积分类型
	 * Date: 2015年9月2日 下午3:50:45 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<PointsType> findPointsTypeList();

	/**
	 * findInvalidDatePointsItems:(查询过期的积分记录). <br/>
	 * Date: 2015-10-9 下午5:34:24 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param invalidDate
	 * @return
	 */
	public List<MemberPointsItemExp> findInvalidDatePointsItems(String invalidDate);

	/**
	 * findByOrderId:(根据订单查询积分记录). <br/>
	 * Date: 2016-1-8 下午1:58:38 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param orderId
	 */
	public List<MemberPointsItem> findByOrderId(Long orderId);
	
	/**
	 * findAvailablePointsItems:(查询会员可用积分). <br/>
	 * Date: 2016-1-12 下午3:11:13 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param size
	 * @return
	 */
	public List<MemberPointsItem> findAvailablePointsItems(Long memberId);

	/**
	 * insertPointsRevise:新增积分调账信息. <br/>
	 * Date: 2016年1月20日 下午3:00:56 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItem
	 * @return
	 */
	public int insertPointsRevise(MemberPointsItem memberPointsItem);
	
	/**
	 * updateMemberAccount:更新会员账号积分信息. <br/>
	 * Date: 2016年1月20日 下午5:00:39 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberAccount
	 * @return
	 */
	public int updateMemberAccount(MemberAccount memberAccount,BigDecimal pointReviseNumber);
	
	
	/**
	 * findPointsReviseList:查询积分调账记录. <br/>
	 * Date: 2016年1月20日 下午7:49:07 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItemExp
	 * @param page
	 * @return
	 */
	public List<MemberPointsItemExp> findPointsReviseList(Example example,Page<MemberPointsItemExp> page);
	
	/**
	 * findListSearchAuto:根据会员编码查询会员积分余额信息. <br/>
	 * Date: 2016年1月21日 上午12:16:57 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberCode
	 * @return
	 */
	MemberPointsItemExp findListSearchAuto(String memberCode); 
	
	/**
	 * selectAddPointsInfo:查询会员即将过期的新增积分记录. <br/>
	 * Date: 2016年1月25日 下午9:54:54 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<MemberPointsItemExp> findAddPointsInfo(MemberPointsItem memberPointsItem);
	
	/**
	 * updateAvailablePointsInfo:更新积分记录表可用积分额度. <br/>
	 * Date: 2016年1月26日 上午10:25:56 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItem
	 * @return
	 */
	int updateAvailablePointsInfo(MemberPointsItem memberPointsItem);
	
	/**
	 * insertPointsItemRel:新增积分扣除关系记录. <br/>
	 * Date: 2016年1月26日 上午11:11:15 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItemRel
	 * @return
	 */
	int insertPointsItemRel(MemberPointsItemRel memberPointsItemRel);

	/**
	 * 根据会员编码查询积分明细
	 * @param memberCode
	 * @return
	 */
	List<MemberPointsItemStoreExp> findStoreExpByMemberCode(String memberCode, int pageSize, int pageNo);

}

