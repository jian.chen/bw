package com.bw.adv.module.points.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.points.model.MemberPointsItemRel;

public interface MemberPointsItemRelMapper extends BaseMapper<MemberPointsItemRel>{
	
}