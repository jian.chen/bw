/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:GoodsShowRepository.java
 * Package Name:com.sage.scrm.module.goods.repository.impl
 * Date:2015年12月9日上午10:48:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.goods.mapper.GoodsExchangeItemMapper;
import com.bw.adv.module.goods.model.GoodsExchangeItem;

public interface GoodsExchangeRepository extends BaseRepository<GoodsExchangeItem, GoodsExchangeItemMapper> {
	
	public List<GoodsExchangeItem> findByMemberId(Long memberId,Page<GoodsExchangeItem> page);

}

