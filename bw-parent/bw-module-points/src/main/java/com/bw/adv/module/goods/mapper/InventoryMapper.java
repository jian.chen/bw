package com.bw.adv.module.goods.mapper;

import com.bw.adv.module.goods.model.Inventory;

public interface InventoryMapper {
    int deleteByPrimaryKey(Long inventoryId);

    int insert(Inventory record);

    int insertSelective(Inventory record);

    Inventory selectByPrimaryKey(Long inventoryId);

    int updateByPrimaryKeySelective(Inventory record);

    int updateByPrimaryKey(Inventory record);
}