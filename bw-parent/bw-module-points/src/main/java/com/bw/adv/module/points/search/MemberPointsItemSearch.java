/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:MemberPointsItemSearch.java
 * Package Name:com.sage.scrm.module.points.search
 * Date:2015年8月20日下午6:50:58
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.search;
/**
 * ClassName:MemberPointsItemSearch <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:50:58 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class MemberPointsItemSearch {
	
	/**
	 * 会员编号
	 */
	private String memberCode;
	
	/**
	 * 会员id
	 */
	private String memberId;
	
	/**
	 * 发生时间
	 */
	private String happenTime;
	
	/**
	 * 过期时间
	 */
	private String expirationTime;
	
	/**
	 * 积分名称
	 */
	private String pointsRuleName;
	
	/**
	 * 积分类型
	 */
	private Long pointsTypeId;
	

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getHappenTime() {
		return happenTime;
	}

	public void setHappenTime(String happenTime) {
		this.happenTime = happenTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getPointsRuleName() {
		return pointsRuleName;
	}

	public void setPointsRuleName(String pointsRuleName) {
		this.pointsRuleName = pointsRuleName;
	}

	public Long getPointsTypeId() {
		return pointsTypeId;
	}

	public void setPointsTypeId(Long pointsTypeId) {
		this.pointsTypeId = pointsTypeId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	

}

