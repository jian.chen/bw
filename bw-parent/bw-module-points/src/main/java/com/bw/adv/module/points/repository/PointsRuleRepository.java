/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-points
 * File Name:PointsRuleRepository.java
 * Package Name:com.sage.scrm.module.points.repository
 * Date:2015年8月20日下午4:58:47
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.points.mapper.PointsRuleMapper;
import com.bw.adv.module.points.model.PointsRule;

/**
 * ClassName:PointsRuleRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午4:58:47 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface PointsRuleRepository extends BaseRepository<PointsRule, PointsRuleMapper> {
	
	public List<PointsRule> findByIsActive(String isActive,Page<PointsRule> page);

	/**
	 * findByPointsTypeId: 根据类型查询积分规则 <br/>
	 * Date: 2015-8-25 下午5:45:22 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param pointsTypeId
	 * @return
	 */
	List<PointsRule> findByPointsTypeId(Long pointsTypeId);
	
	List<PointsRule> findByPointsRuleName(String isActive,String pointsRuleName);
	
	public void removePointsRuleByPointsRuleId(Long pointsRuleId);
	
	/**
	 * findByIsActive:(查询所有启用积分规则). <br/>
	 * Date: 2015-9-8 下午6:26:54 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param isActive
	 * @return
	 */
	public List<PointsRule> findByIsActive(String isActive);
	
}

