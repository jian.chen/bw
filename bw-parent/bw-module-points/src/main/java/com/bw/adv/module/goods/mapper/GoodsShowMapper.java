package com.bw.adv.module.goods.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.goods.model.GoodsShow;
import com.bw.adv.module.goods.model.exp.GoodsShowExp;

public interface GoodsShowMapper extends BaseMapper<GoodsShow> {

	/**
	 * 
	 * selectListByStatus:(根据状态查询商品展示列表(不等于传入状态的所有数据)). <br/>
	 * Date: 2015年12月9日 上午11:08:03 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param pageObj
	 * @param statusId
	 * @return
	 */
	List<GoodsShowExp> selectListByStatus(@Param("statusId")Long statusId,@Param("page")Page<GoodsShowExp> page);
	
	/**
	 * selectPointsProductByStatus:根据状态查询商品兑换信息<br/>
	 * Date: 2015年12月14日 下午9:25:51 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @return
	 */
	List<GoodsShowExp> selectPointsProductByStatus(@Param("statusId")Long statusId,@Param("page")Page<GoodsShowExp> page);
	List<GoodsShowExp> selectExtPointsProductByStatus(@Param("statusId")Long statusId,@Param("page")Page<GoodsShowExp> page);
	/**
	 * 
	 * deleteList:(批量删除商品展示(逻辑删除)). <br/>
	 * Date: 2015年12月9日 下午4:13:11 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowIds
	 * @param statusId
	 * @return
	 */
	int deleteList(@Param("goodsShowIds")String goodsShowIds,@Param("statusId")Long statusId);
	
	/**
	 * 
	 * selectExpByPK:(根据Id查询商品展示详细信息). <br/>
	 * Date: 2015年12月10日 下午2:56:05 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowId
	 * @return
	 */
	GoodsShowExp selectExpByPK(String goodsShowId);
	
	/**
	 * selectExpByExample:根据条件查询积分商城详情<br/>
	 * Date: 2015年12月15日 下午8:40:59 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	GoodsShowExp selectExpByExample(@Param("example")Example example);
	
}