package com.bw.adv.wechat.parse.exception;


public class WeChatSendServiceException extends RuntimeException {

	private static final long serialVersionUID = 8894403183047713964L;

	public WeChatSendServiceException() {
        super();
    }

    public WeChatSendServiceException(String msg) {
        super(msg);
    }

    public WeChatSendServiceException(Throwable cause) {
        super(cause);
    }

    public WeChatSendServiceException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
