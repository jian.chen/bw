package com.bw.adv.wechat.parse.service.event.impl;

import java.util.Map;

import javax.annotation.PostConstruct;

import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.service.event.ManagerService;


public class OperatorServiceImpl implements ManagerService{

    private Map<String, ManagerService> serviceMap;
    
    @PostConstruct
    public void test(){
    	System.out.println("OperatorServiceImpl:"+serviceMap);
    }
    
    public WeChatSend operatorReceive(WeChatReceive weChatReceive) {
        return serviceMap.get(weChatReceive.getMsgType()).operatorReceive(weChatReceive);
    }
    
	public void setServiceMap(Map<String, ManagerService> serviceMap) {
		this.serviceMap = serviceMap;
	}
	
}
