package com.bw.adv.wechat.parse.domain.receive;

import org.apache.commons.lang.StringUtils;

import com.bw.adv.wechat.parse.domain.WeChatBaseInfo;
import com.bw.adv.wechat.parse.domain.WeChatParam;
import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.exception.WeChatParamException;

public class WeChatReceive extends WeChatBaseInfo {

	private Long msgId;

	public WeChatReceive() {
		
	}
	
	public WeChatReceive(WeChatParam weChatParam) {
		super(weChatParam);
		this.msgId = weChatParam.getMsgId();
	}

	public final static WeChatReceive getReceiveFromParam(WeChatParam weChatParam) throws WeChatParamException {
		if (StringUtils.isEmpty(weChatParam.getMsgType())) {
			throw new WeChatParamException("微信对象类型不能为空");
		}
		if (weChatParam.getMsgType().equals(WeChatBaseInfo.MSG_TYPE_TEXT)) {
			return new WeChatTextReceive(weChatParam);
		} else if (weChatParam.getMsgType().equals(WeChatBaseInfo.MSG_TYPE_LOCATION)) {
			return new WeChatLocationReceive(weChatParam);
		} else if (weChatParam.getMsgType().equals(WeChatBaseInfo.MSG_TYPE_EVENT)) {
			return new WeChatEventReceive(weChatParam);
		} else if(weChatParam.getMsgType().equals(WeChatBaseInfo.MSG_TYPE_IMAGE)){
			return new WeChatImageReceive(weChatParam);
		}
		throw new WeChatParamException("没有匹配的微信类型");
	}

	public final Long getMsgId() {
		return this.msgId;
	}

}
