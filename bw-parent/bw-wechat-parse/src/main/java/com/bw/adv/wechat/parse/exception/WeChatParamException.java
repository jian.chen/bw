package com.bw.adv.wechat.parse.exception;

public class WeChatParamException extends RuntimeException{

	private static final long serialVersionUID = -1107236259282237342L;

	public WeChatParamException() {
        super();
    }

    public WeChatParamException(String msg) {
        super(msg);
    }

    public WeChatParamException(Throwable cause) {
        super(cause);
    }

    public WeChatParamException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
