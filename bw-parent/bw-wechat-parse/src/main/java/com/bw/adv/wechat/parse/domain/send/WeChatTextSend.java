/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WeChatTextSend.java
 * Package Name:com.sage.scrm.module.wechat.api.domain.send
 * Date:2015年8月9日上午10:06:17
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.wechat.parse.domain.send;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;

/**
 * ClassName:WeChatTextSend <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月9日 上午10:06:17 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@XmlRootElement(name="xml")
public class WeChatTextSend extends WeChatSend {
	
	private String content;
	
	private WeChatTextSend() {
        super(WeChatSend.MSG_TYPE_TEXT);
    }
	
	public static WeChatTextSend createWeChatTextSend(WeChatReceive weChatReceive) {
    	WeChatTextSend text = new WeChatTextSend();
    	text.changeFromToUsersByReceiveWithCreateTime(weChatReceive);
    	return text;
    }
	
	@XmlElement(name = "Content")
    public final String getContent() {
        return this.content;
    }

    public final void setContent(final String argContent) {
        this.content = argContent;
    }
}

