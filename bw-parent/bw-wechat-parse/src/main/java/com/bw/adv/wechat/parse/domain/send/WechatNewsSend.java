package com.bw.adv.wechat.parse.domain.send;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;

@XmlRootElement(name="xml")
public class WechatNewsSend extends WeChatSend{
	private int articleCount;
	private WechatNewsArticles articles;

	private WechatNewsSend(){
		super(WeChatSend.MSG_TYPE_NEWS);
	}
	
	public static WechatNewsSend createWeChatTextSend(WeChatReceive weChatReceive) {
		WechatNewsSend news = new WechatNewsSend();
		news.changeFromToUsersByReceiveWithCreateTime(weChatReceive);
    	return news;
    }
	
	@XmlElement(name = "ArticleCount")
	public final int getArticleCount() {
		return articleCount;
	}


	public final void setArticleCount(int articleCount) {
		this.articleCount = articleCount;
	}

	@XmlElement(name = "Articles")
	public WechatNewsArticles getArticles() {
		return articles;
	}
	
	public void setArticles(WechatNewsArticles articles) {
		this.articles = articles;
	}

}
