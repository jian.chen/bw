package com.bw.adv.wechat.parse.service.event.impl;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;
import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.service.event.EventManagerService;
import com.bw.adv.wechat.parse.service.event.ManagerService;

@Service
public class EventManagerServiceImpl implements ManagerService {

	private Map<String, EventManagerService> eventMaps;
	
	@PostConstruct
	public void test(){
		System.out.println("EventManagerServiceImpl:"+eventMaps);
	}
	
	@Override
	public WeChatSend operatorReceive(WeChatReceive weChatReceive) {
		// 根据event判断事件类型,并作后续处理
		WeChatEventReceive weChatEventReceive = (WeChatEventReceive) weChatReceive;
		try {
			EventManagerService manageService  = eventMaps.get(weChatEventReceive.getEvent());
			if(null == manageService){
				return null;
			}
			return manageService.operatorReceive(weChatEventReceive);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public void setEventMaps(Map<String, EventManagerService> eventMaps) {
		this.eventMaps = eventMaps;
	}
}
