/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-wechat-parse
 * File Name:WechatOperateColligate.java
 * Package Name:com.sage.scrm.wechat.parse.service.event.impl
 * Date:2016年5月19日下午6:06:02
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.wechat.parse.service.event.impl;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.module.member.constant.ExtAccountTypeConstant;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.wechat.api.ApiWeChat;
import com.bw.adv.module.wechat.api.ApiWechatMaterialUpload;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.wechat.msg.model.WechatImageTemplate;
import com.bw.adv.service.wechat.msg.model.WechatTextTemplate;
import com.bw.adv.wechat.parse.domain.LogoConfig;
import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatImageSend;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.domain.send.WeChatSubImageSend;
import com.bw.adv.wechat.parse.domain.send.WeChatTextSend;
import com.bw.adv.wechat.parse.service.event.WechatOperateColligateService;
import com.bw.adv.wechat.parse.util.HttpKit;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * ClassName:WechatOperateColligate <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月19日 下午6:06:02 <br/>
 * scrmVersion 1.0
 * 
 * @author chengdi.cui
 * @version jdk1.7
 * @see
 */
@Service
@SuppressWarnings("restriction")
public class WechatOperateColligateServiceImpl implements WechatOperateColligateService {

	private static  Logger LOGGER = Logger.getLogger(WechatOperateColligateServiceImpl.class);



	// 获取海报成功提示
	@Value("${pe_invite_cpover}")
	private static  String peInviteCpover;

	@Value("${webRoot}")
	private static  String webRoot;

	@Value("${imgUrl}")
	private static  String imgUrl;

	@Value("${appId}")
	public static  String appId;

	private MemberService memberService;

	private AccessTokenCacheService accessTokenService;

	private ApiWechatMaterialUpload apiWechatMaterialUpload;

	private static ExecutorService EXECUTOR_SERVICE;

	static {
		int threadSize = Runtime.getRuntime().availableProcessors() * 2 + 1;
		EXECUTOR_SERVICE = Executors.newFixedThreadPool(threadSize);
	}

	@Autowired
	public void setApiWechatMaterialUpload(ApiWechatMaterialUpload apiWechatMaterialUpload) {
		this.apiWechatMaterialUpload = apiWechatMaterialUpload;
	}

	@Autowired
	public void setAccessTokenService(AccessTokenCacheService accessTokenService) {
		this.accessTokenService = accessTokenService;
	}

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	/**
	 * 会员点击英雄令菜单事件 resultSendForKing: <br/>
	 * Date: 2016年5月20日 下午2:44:23 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param token
	 * @param weChatEventReceive
	 * @return
	 */
	public WeChatSend resultSendForKing(String token, WeChatEventReceive weChatEventReceive) throws Exception {
		String openId = weChatEventReceive.getFromUserName();
		Member member = null;
		member = this.memberService.queryMemberByBindingAccount(ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getCode(),
				openId);
//		// 1.判断是否会员,不是会员或者未激活的会员返回文字,是会员的返回图片
//		String content = peInviteContent.replace("APPID", appId);
//		if (member == null) {
//			return textSendForKing(token, weChatEventReceive, content);
//		} else {
//			EXECUTOR_SERVICE.submit(new OperationKing(member, openId, this));
//			return textSendForKing(token, weChatEventReceive, "邀请二维码已经生成，赶快邀请好友吧！");
//		}
		return null;
	}

	private class OperationKing implements Runnable {

		private Member member;
		private String openId;
		private WechatOperateColligateServiceImpl wechatOperateColligateService;

		private OperationKing(Member member, String openId, WechatOperateColligateServiceImpl wechatOperateColligateService) {
			this.member = member;
			this.openId = openId;
			this.wechatOperateColligateService = wechatOperateColligateService;
		}

		@Override
		public void run() {
			try {
//				String url = LongUrlToShort(peInviteReg, member.getMemberId());
//				String imgPath = structureImage(url, member.getMemberCode());
//				WeixinMedia media = apiWechatMaterialUpload.uploadMedia("image", imgPath);
//				String mediaId = media.getMediaId();
//				removeUploadMedia(imgPath);
//				wechatOperateColligateService.imageSend(accessTokenService.getAccessTokenCache(), openId, mediaId);
				//wechatOperateColligateService.textSend(accessTokenService.getAccessTokenCache(), openId);
			} catch (Exception e) {
				LOGGER.error("pe invite error: memberId[" + member.getMemberId() + "]", e);
			}
		}
	}

	/**
	 * 回复文本消息 textSendForKing: <br/>
	 * Date: 2016年5月20日 下午2:45:57 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param token
	 * @param weChatEventReceive
	 * @return
	 */
	private WeChatSend textSendForKing(String token, WeChatEventReceive weChatEventReceive, String content) {
		WeChatTextSend textSend = WeChatTextSend.createWeChatTextSend(weChatEventReceive);
		textSend.setContent(content);
		return textSend;
	}

	/**
	 * 回复图片消息 imageSendForKing: <br/>
	 * Date: 2016年5月20日 下午3:11:12 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param token
	 * @param weChatEventReceive
	 * @param mediaId
	 * @return
	 */
	@SuppressWarnings("unused")
	private WeChatSend imageSendForKing(String token, WeChatEventReceive weChatEventReceive, String mediaId) {
		WeChatImageSend imageSend = WeChatImageSend.createWeChatImageSend(weChatEventReceive);
		WeChatSubImageSend image = null;
		image = new WeChatSubImageSend();
		image.setMediaId(mediaId);
		imageSend.setWeChatSubImageSend(image);
		return imageSend;
	}

	/**
	 * 客服接口发送图片 imageSend: <br/>
	 * Date: 2016年5月24日 下午4:07:44 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param accessToken
	 * @param openId
	 * @param mediaId
	 * @return
	 * @throws Exception
	 */
	private JSONObject imageSend(String accessToken, String openId, String mediaId) throws Exception {
		WechatImageTemplate imageTemplate = new WechatImageTemplate();
		imageTemplate.setTouser(openId);
		imageTemplate.setMsgtype("image");
		Map<String, String> map = new HashMap<String, String>();
		map.put("media_id", mediaId);
		imageTemplate.setImage(map);
		String url = ApiWeChat.SEND_CUSTOM_MESSAGE.replace("ACCESS_TOKEN", accessToken);
		String result = HttpKit.post(url, JSONObject.toJSONString(imageTemplate));
		if (StringUtils.isNotEmpty(result)) {
			return JSONObject.parseObject(result);
		}
		return null;
	}

	/**
	 * 客服接口发送文本 textSend: <br/>
	 * Date: 2016年5月24日 下午4:08:04 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param accessToken
	 * @param openId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private JSONObject textSend(String accessToken, String openId) throws Exception {
		WechatTextTemplate textTemplate = new WechatTextTemplate();
		textTemplate.setTouser(openId);
		textTemplate.setMsgtype("text");
		String content = peInviteCpover;
		Map<String, String> map = new HashMap<String, String>();
		map.put("content", content);
		textTemplate.setText(map);
		String url = ApiWeChat.SEND_CUSTOM_MESSAGE.replace("ACCESS_TOKEN", accessToken);
		String result = HttpKit.post(url, JSONObject.toJSONString(textTemplate));
		if (StringUtils.isNotEmpty(result)) {
			return JSONObject.parseObject(result);
		}
		return null;
	}

	/**
	 * 长链接转短链接 LongUrlToShort: <br/>
	 * Date: 2016年5月20日 下午5:28:51 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param longUrl
	 * @return
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	private String LongUrlToShort(String longUrl, Long memberId)
			throws IOException, ExecutionException, InterruptedException {
		JSONObject object = null;
		String accessToken = accessTokenService.getAccessTokenCache();
		longUrl = longUrl.replace("APPID", appId);
		longUrl = longUrl.replace("INVITATIONID", memberId.toString());
		String data = "{\"action\":\"long2short\",\"long_url\":\"" + longUrl + "\"}";
		String result = HttpKit.post(ApiWeChat.LONG_TO_SHORT.replace("ACCESS_TOKEN", accessToken), data);
		object = JSONObject.parseObject(result);
		return object.getString("short_url");
	}

	/**
	 * 构造带用户信息的二维码海报 structureImage: <br/>
	 * Date: 2016年5月22日 下午8:37:42 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param url
	 * @param memberCode
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String structureImage(String url, String memberCode) {
		String path = null;
		String fileName = "";
		try {
			int width = 200;
			int height = 200;
			String format = "jpg";
			Hashtable hints = new Hashtable();
			hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
			BitMatrix bitMatrix = new MultiFormatWriter().encode(url, BarcodeFormat.QR_CODE, width, height, hints);
			bitMatrix = updateBit(bitMatrix, 3);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, format, outputStream);
			byte[] icon = outputStream.toByteArray();

			ByteArrayInputStream bais = new ByteArrayInputStream(icon);
			BufferedImage bi1 = ImageIO.read(bais);

			path = this.getClass().getClassLoader().getResource("/").getPath() + "./../../" + imgUrl;
			String realPath = path;
			File dir = new File(realPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			fileName = path + "/" +memberCode + ".jpg";
			OutputStream os = new FileOutputStream(fileName);
			// 创键编码器，用于编码内存中的图象数据。
			JPEGImageEncoder en = JPEGCodec.createJPEGEncoder(os);
			en.encode(bi1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return webRoot + imgUrl + "/" + memberCode + ".jpg";
	}

	/**
	 * 删除上传的会员海报文件 removeUploadMedia: <br/>
	 * Date: 2016年5月25日 下午12:39:37 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param imgPath
	 * @throws IOException
	 */
	private void removeUploadMedia(String imgPath) throws IOException {
		imgPath = this.getClass().getClassLoader().getResource("/").getPath() + "./../../" + imgUrl + imgPath.substring(imgPath.lastIndexOf("/"));
		File file = new File(imgPath);
		if (file.isFile() && file.exists()) {
			file.delete();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		String path = null;
		String fileName = "";
		try {
			int width = 200;
			int height = 200;
			String format = "jpg";
			Hashtable hints = new Hashtable();
			hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
			String url = "https://www.baidu.com";
			BitMatrix bitMatrix = new MultiFormatWriter().encode(url, BarcodeFormat.QR_CODE, width, height, hints);
			bitMatrix = updateBit(bitMatrix, 3);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, format, outputStream);
			byte[] icon = outputStream.toByteArray();

			ByteArrayInputStream bais = new ByteArrayInputStream(icon);
			BufferedImage bi1 = ImageIO.read(bais);

			path = "e:/";
			String realPath = path;
			File dir = new File(realPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			fileName = path + "qwerty" + ".jpg";
			OutputStream os = new FileOutputStream(fileName);
			JPEGImageEncoder en = JPEGCodec.createJPEGEncoder(os);
			en.encode(bi1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 拼logo
	 * addLogo_QRCode: <br/>
	 * Date: 2016年5月25日 下午3:06:32 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param qrPic
	 * @param logoPic
	 * @param logoConfig
	 * @return
	 */
	public static BufferedImage addLogo_QRCode(BufferedImage qrPic, File logoPic, LogoConfig logoConfig) {
		try {

			/**
			 * 读取二维码图片，并构建绘图对象
			 */
			BufferedImage image = qrPic;
			Graphics2D g = image.createGraphics();

			/**
			 * 读取Logo图片
			 */
			BufferedImage logo = ImageIO.read(logoPic);
			/**
			 * 设置logo的大小,本人设置为二维码图片的20%,因为过大会盖掉二维码
			 */
			int widthLogo = logo.getWidth(null) > image.getWidth() * 2 / 10 ? (image.getWidth() * 2 / 10)
					: logo.getWidth(null),
					heightLogo = logo.getHeight(null) > image.getHeight() * 2 / 10 ? (image.getHeight() * 2 / 10)
							: logo.getWidth(null);

			// 计算图片放置位置
			/**
			 * logo放在中心
			 */
			int x = (image.getWidth() - widthLogo) / 2;
			int y = (image.getHeight() - heightLogo) / 2;
			/**
			 * logo放在右下角
			 */
			// 开始绘制图片
			g.drawImage(logo, x, y, widthLogo, heightLogo, null);
			g.drawRoundRect(x, y, widthLogo, heightLogo, 15, 15);
			g.setStroke(new BasicStroke(logoConfig.getBorder()));
			g.setColor(logoConfig.getBorderColor());
			g.drawRect(x, y, widthLogo, heightLogo);

			g.dispose();
			logo.flush();
			image.flush();

			ImageIO.write(image, "png", new File("e:/test.png"));
			return image;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 去白边 updateBit: <br/>
	 * Date: 2016年5月25日 下午12:38:16 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param matrix
	 * @param margin
	 * @return
	 */
	private static BitMatrix updateBit(BitMatrix matrix, int margin) {
		int tempM = margin * 2;
		int[] rec = matrix.getEnclosingRectangle(); // 获取二维码图案的属性
		int resWidth = rec[2] + tempM;
		int resHeight = rec[3] + tempM;
		BitMatrix resMatrix = new BitMatrix(resWidth, resHeight); // 按照自定义边框生成新的BitMatrix
		resMatrix.clear();
		for (int i = margin; i < resWidth - margin; i++) { // 循环，将二维码图案绘制到新的bitMatrix中
			for (int j = margin; j < resHeight - margin; j++) {
				if (matrix.get(i - margin + rec[0], j - margin + rec[1])) {
					resMatrix.set(i, j);
				}
			}
		}
		return resMatrix;
	}
	
}
