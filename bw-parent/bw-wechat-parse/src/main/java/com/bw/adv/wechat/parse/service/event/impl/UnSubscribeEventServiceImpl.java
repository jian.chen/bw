/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:UnSubscribeEventServiceImpl.java
 * Package Name:com.sage.scrm.module.wechat.api.service.event.impl
 * Date:2015年8月20日下午6:19:00
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.wechat.parse.service.event.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.common.WechatConstant;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.service.wechat.service.WeChatFansService;
import com.bw.adv.service.wechat.service.WechatFansBehaviorService;
import com.bw.adv.wechat.parse.domain.WeChatBaseInfo;
import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.service.event.EventManagerService;

/**
 * ClassName:UnSubscribeEventServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:19:00 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class UnSubscribeEventServiceImpl implements EventManagerService {
	
	protected final Logger logger = Logger.getLogger(UnSubscribeEventServiceImpl.class);
	
	private WeChatFansService weChatFansService;
	private WechatFansBehaviorService wechatFansBehaviorService;
	
	@Override
	public WeChatSend operatorReceive(WeChatEventReceive weChatEventReceive)
			throws Exception {
		return getWeChatSend(weChatEventReceive);
	}
	
	/**
	 * getWeChatSend:(取消关注). <br/>
	 * Date: 2015年8月20日 下午6:19:56 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param weChatEventReceive
	 * @return
	 */
	private WeChatSend getWeChatSend(WeChatEventReceive weChatEventReceive) {
		List<WechatFans> wechatFansList =null;
		WechatFans wechatFans =null;
		String fromUserName =null;
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_PATTERN_YYYYMMDDHHmmss);
		Long cancelAttentionLong =null;
		try {
			fromUserName =weChatEventReceive.getFromUserName();
			// 取消关注
			if(weChatEventReceive.getEvent().equals(WeChatBaseInfo.MSG_TYPE_UNSUBSCRIBE)){
				wechatFansList = weChatFansService.queryWechatFansByParams(fromUserName, null, null, null, null, null, null);
				if(wechatFansList.size()>0){
					wechatFans =wechatFansList.get(0);
					wechatFans.setIsFans(WechatTypeConstant.IS_NOT_WECHATFAN.getCode());
					// 如果用户曾多次关注，则取最后关注时间
					cancelAttentionLong =weChatEventReceive.getCreateTime() * WechatConstant.RADIO;
					wechatFans.setCancelAttentionTime(sdf.format(new Date(cancelAttentionLong)));
					weChatFansService.updateByPkSelective(wechatFans);
					//同步粉丝行为
					wechatFansBehaviorService.saveUnSubscribeFansBehavior(wechatFans);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	@Autowired
	public void setWeChatFansService(WeChatFansService weChatFansService) {
		this.weChatFansService = weChatFansService;
	}

	@Autowired
	public void setWechatFansBehaviorService(
			WechatFansBehaviorService wechatFansBehaviorService) {
		this.wechatFansBehaviorService = wechatFansBehaviorService;
	}

}

