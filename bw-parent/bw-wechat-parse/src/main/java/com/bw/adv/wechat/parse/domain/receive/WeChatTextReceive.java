package com.bw.adv.wechat.parse.domain.receive;

import com.bw.adv.wechat.parse.domain.WeChatParam;

public class WeChatTextReceive extends WeChatReceive {

    private String content;

    public WeChatTextReceive() {
    	
    }

    public WeChatTextReceive(WeChatParam weChatParam) {
        super(weChatParam);
        this.content = weChatParam.getContent();
    }

    public final String getContent() {
        return this.content;
    }

}
