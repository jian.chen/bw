package com.bw.adv.wechat.parse.service.event;

public interface ScanEventHandleAfter {
	public String operatorReceive(String token,String openId);
}
