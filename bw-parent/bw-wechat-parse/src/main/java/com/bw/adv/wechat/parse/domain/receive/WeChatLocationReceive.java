
package com.bw.adv.wechat.parse.domain.receive;

import com.bw.adv.wechat.parse.domain.WeChatParam;

public class WeChatLocationReceive extends WeChatReceive {

    private String location_X;
    private String location_Y;
    private String scale;
    private String label;

    public WeChatLocationReceive() {
    	
    }
    
    public WeChatLocationReceive(WeChatParam weChatParam) {
        super(weChatParam);
        this.location_X = weChatParam.getLocation_X();
        this.location_Y = weChatParam.getLocation_Y();
        this.scale = weChatParam.getScale();
        this.label = weChatParam.getLabel();
    }
    
    public final String getLocation_X() {
        return this.location_X;
    }

    public final String getLocation_Y() {
        return this.location_Y;
    }

    public final String getScale() {
        return this.scale;
    }

    public final String getLabel() {
        return this.label;
    }

}
