package com.bw.adv.wechat.parse.domain;

import javax.xml.bind.annotation.XmlElement;

public abstract class WeChatBaseInfo {

    //微信信息类型
    public final static String MSG_TYPE_TEXT = "text";
    public final static String MSG_TYPE_IMAGE = "image";
    public final static String MSG_TYPE_VOICE = "voice";
    public final static String MSG_TYPE_VOIDE = "voide";
    public final static String MSG_TYPE_LOCATION = "location";
    public final static String MSG_TYPE_LINK = "link";
    public final static String MSG_TYPE_EVENT = "event";
    //关注
    public final static String MSG_TYPE_SUBSCRIBE = "subscribe";
    //取消关注
    public final static String MSG_TYPE_UNSUBSCRIBE = "unsubscribe";
    
    //微信基本信息
    private String toUserName;
    private String fromUserName;
    private Long createTime;
    private String msgType;
    

    public WeChatBaseInfo() {
    	
    }
    
    public WeChatBaseInfo(String msgType) {
    	this.msgType = msgType;
    }
    
    public WeChatBaseInfo(WeChatParam weChatParam) {
    	this(weChatParam.getMsgType());
        this.toUserName = weChatParam.getToUserName();
        this.fromUserName = weChatParam.getFromUserName();
        this.createTime= weChatParam.getCreateTime();
     }
    
    @XmlElement(name = "ToUserName")
    public final String getToUserName() {
        return this.toUserName;
    }

    public final void setToUserName(final String argToUserName) {
        this.toUserName = argToUserName;
    }
    
    @XmlElement(name = "FromUserName")
    public final String getFromUserName() {
        return this.fromUserName;
    }

    public final void setFromUserName(final String argFromUserName) {
        this.fromUserName = argFromUserName;
    }
    
    @XmlElement(name = "CreateTime")
    public final Long getCreateTime() {
        return this.createTime;
    }

    public final void setCreateTime(final Long argCreateTime) {
        this.createTime = argCreateTime;
    }
    
    @XmlElement(name = "MsgType")
    public final String getMsgType() {
        return this.msgType;
    }
}
