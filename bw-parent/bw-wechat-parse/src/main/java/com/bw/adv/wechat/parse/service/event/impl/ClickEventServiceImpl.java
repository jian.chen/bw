package com.bw.adv.wechat.parse.service.event.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.domain.send.WeChatTextSend;
import com.bw.adv.wechat.parse.domain.send.WechatNewsArticles;
import com.bw.adv.wechat.parse.domain.send.WechatNewsItem;
import com.bw.adv.wechat.parse.domain.send.WechatNewsSend;
import com.bw.adv.wechat.parse.service.event.EventManagerService;
import com.bw.adv.wechat.parse.service.event.WechatOperateColligateService;

public class ClickEventServiceImpl implements EventManagerService {

	@Value("${myCoupon_title}")
	private static  String myCouponTitle;
	@Value("${myCoupon_description}")
	private static  String myCouponDescription;
	@Value("${myCoupon_url}")
	private static  String myCouponUrl;
	@Value("${myCoupon_picurl}")
	private static  String myCouponPicurl;
	private AccessTokenCacheService accessTokenCacheService;
	private WechatOperateColligateService wechatOperateColligate;
	@Override
	public WeChatSend operatorReceive(WeChatEventReceive weChatEventReceive)
			throws Exception {
		if(weChatEventReceive.getEventKey().equals("LINKUS")){
			return textTextSend(accessTokenCacheService.getAccessTokenCache(),weChatEventReceive.getFromUserName(),weChatEventReceive);
		}
		
		if(weChatEventReceive.getEventKey().equals("MYCOUPON")){
			return imageTextSendForMyCoupon(accessTokenCacheService.getAccessTokenCache(),weChatEventReceive.getFromUserName(),weChatEventReceive);
		}else if(weChatEventReceive.getEventKey().equals("INVITE")){
			return wechatOperateColligate.resultSendForKing(accessTokenCacheService.getAccessTokenCache(), weChatEventReceive);
		}else{
			return null;
		}
	}
	
	private WeChatSend textTextSend(String token, String openId,WeChatEventReceive weChatEventReceive) {
		WeChatTextSend textSend = WeChatTextSend
				.createWeChatTextSend(weChatEventReceive);
		textSend.setContent("如有问题，请留言或拨打客服热线13816192456咨询");
		return textSend;
	}
	
	/**
	 * 我的优惠券菜单点击推送图文消息
	 * @param token
	 * @param openId
	 * @param weChatEventReceive
	 * @return
	 * @author yu.zhang
	 */
	private WeChatSend imageTextSendForMyCoupon(String token,String openId,WeChatEventReceive weChatEventReceive){
		WechatNewsSend newsSend = WechatNewsSend.createWeChatTextSend(weChatEventReceive);
		WechatNewsArticles articles = new WechatNewsArticles();
		WechatNewsItem item = new WechatNewsItem();
		item.setTitle(myCouponTitle);
		item.setDescription(myCouponDescription);
		item.setUrl(myCouponUrl);
		item.setPicUrl(myCouponPicurl);
		List<WechatNewsItem> items = new ArrayList<WechatNewsItem>();
		items.add(item);
		articles.setNewsItems(items);
		newsSend.setArticleCount(items.size());
		newsSend.setArticles(articles);
		
		return newsSend;
	}

	@Autowired
	public void setAccessTokenCacheService(AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}

	@Autowired
	public void setWechatOperateColligate(WechatOperateColligateService wechatOperateColligate) {
		this.wechatOperateColligate = wechatOperateColligate;
	}
}
