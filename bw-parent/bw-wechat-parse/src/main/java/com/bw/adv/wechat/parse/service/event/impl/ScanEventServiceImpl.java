package com.bw.adv.wechat.parse.service.event.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.service.event.EventManagerService;
import com.bw.adv.wechat.parse.service.event.ScanEventHandleAfter;

@Service
public class ScanEventServiceImpl implements EventManagerService{
	private AccessTokenCacheService accessTokenCacheService;
	private ScanEventHandleAfter eventHandleAfter;
	@Value("${eventKey_test}")
	private static String eventKeyTest;
	@Override
	public WeChatSend operatorReceive(WeChatEventReceive weChatEventReceive) throws Exception {
		return handleScanEvent(weChatEventReceive);
	}

	private WeChatSend handleScanEvent(WeChatEventReceive weChatEventReceive){
		try {
			if(weChatEventReceive.getEventKey().equals(eventKeyTest)){
				eventHandleAfter.operatorReceive(accessTokenCacheService.getAccessTokenCache(),weChatEventReceive.getFromUserName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Autowired
	public void setAccessTokenCacheService(AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}

	@Autowired
	public void setEventHandleAfter(ScanEventHandleAfter eventHandleAfter) {
		this.eventHandleAfter = eventHandleAfter;
	}

}
