/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:WeChatImageReceive.java
 * Package Name:com.sage.scrm.module.wechat.api.domain.receive
 * Date:2015年8月25日下午7:33:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.wechat.parse.domain.receive;

import com.bw.adv.wechat.parse.domain.WeChatParam;

/**
 * ClassName:WeChatImageReceive <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午7:33:40 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WeChatImageReceive extends WeChatReceive {
	
	private String picUrl;
	private String mediaId;

	public WeChatImageReceive() {
		
	}
	
	public WeChatImageReceive(WeChatParam weChatParam) {
        super(weChatParam);
        this.picUrl =weChatParam.getPicUrl();
        this.mediaId =weChatParam.getMediaId();
    }
	
	public String getPicUrl() {
		return picUrl;
	}
	
	public String getMediaId() {
		return mediaId;
	}
	
	
}

