package com.bw.adv.wechat.parse.service.event;

import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;

public interface EventManagerService {
	
	public WeChatSend operatorReceive(WeChatEventReceive weChatEventReceive) throws Exception;
}
