package com.bw.adv.wechat.parse.service.event.impl;

import com.bw.adv.wechat.parse.domain.receive.WeChatLocationReceive;
import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.domain.send.WeChatTextSend;
import com.bw.adv.wechat.parse.service.event.ManagerService;

//@Service("location")
public class LocationManagerServiceImpl implements ManagerService {

    @Override
    public WeChatSend operatorReceive(WeChatReceive weChatReceive) {
        return this.getClosestProductStore(weChatReceive);
    }

    private WeChatSend getClosestProductStore(WeChatReceive weChatReceive) {
        WeChatLocationReceive location = (WeChatLocationReceive) weChatReceive;
        //TODO 得到最近的门店信息
        String reponseStore = "";
        WeChatTextSend text = WeChatTextSend.createWeChatTextSend(weChatReceive);
        text.setContent(reponseStore);
        return text;
    }
}
