/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:ImageManagerServiceImpl.java
 * Package Name:com.sage.scrm.module.wechat.api.service.event.impl
 * Date:2015年8月25日下午7:31:38
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.wechat.parse.service.event.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansMessage;
import com.bw.adv.service.wechat.service.WeChatFansService;
import com.bw.adv.service.wechat.service.WechatFansMessageService;
import com.bw.adv.wechat.parse.domain.WeChatBaseInfo;
import com.bw.adv.wechat.parse.domain.receive.WeChatImageReceive;
import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;
import com.bw.adv.wechat.parse.domain.receive.WeChatTextReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.domain.send.WeChatTextSend;
import com.bw.adv.wechat.parse.service.event.ManagerService;

/**
 * ClassName:ImageManagerServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午7:31:38 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class ImageManagerServiceImpl implements ManagerService {

	private WechatFansMessageService wechatFansMessageService;
	private WeChatFansService weChatFansService;
	

	@Override
	public WeChatSend operatorReceive(WeChatReceive weChatReceive) {
		// 粉丝id
		/*Long wechatFanId = null;
		// opendId
		String fromUsername = null;
		// 消息类型
		String msgType = null;
		// 是否回复
		String isReply = null;
		WechatFans wechatFans = null;
		WechatFansMessage fansMessage = null;
		List<WechatFans> wechatFansList = null;
		
		try {
			fansMessage = new WechatFansMessage();
			WeChatImageReceive weChatImageReceive =(WeChatImageReceive)weChatReceive;
			fromUsername =weChatImageReceive.getFromUserName();
			isReply = WechatTypeConstant.IS_NOT_REPLY.getCode();
			msgType = WeChatBaseInfo.MSG_TYPE_IMAGE;// 消息类型
			String isKeyWorld = "0";
			wechatFansList =
						weChatFansService.queryWechatFansByParams(fromUsername, null, null, null, null, null);
			if(wechatFansList.size()>0){
				wechatFans = wechatFansList.get(0);
				wechatFanId =wechatFans.getWechatFansId();
			}
			
			fansMessage.setFssFansid(wechatFanId);
			fansMessage.setFssOpenid(fromUsername);
			fansMessage.setMsgType(msgType);
			fansMessage.setIsReply(isReply);
			fansMessage.setIsKeyword(isKeyWorld);
			fansMessage.setFssCreateddate(DateUtils.getCurrentDateOfDb());
			fansMessage.setMediaId(weChatImageReceive.getMediaId()+"");
			fansMessage.setPictureUrl(weChatImageReceive.getPicUrl());
			
			wechatFansMessageService.save(fansMessage);
			
			WeChatTextSend text =WeChatTextSend.createWeChatTextSend(weChatReceive);
			text.setContent("推送的消息类型是:Image");
			return text;
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		return null;
	}
	
	
	
	@Autowired
	public void setWechatFansMessageService(
			WechatFansMessageService wechatFansMessageService) {
		this.wechatFansMessageService = wechatFansMessageService;
	}
	
	@Autowired
	public void setWeChatFansService(WeChatFansService weChatFansService) {
		this.weChatFansService = weChatFansService;
	}
	
}

