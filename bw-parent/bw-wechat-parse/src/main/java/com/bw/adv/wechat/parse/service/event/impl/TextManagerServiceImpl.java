package com.bw.adv.wechat.parse.service.event.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.service.wechat.service.KeysGroupService;
import com.bw.adv.service.wechat.service.WeChatFansService;
import com.bw.adv.service.wechat.service.WechatFansMessageService;
import com.bw.adv.service.wechat.service.WechatKeysRuleService;
import com.bw.adv.service.wechat.service.WechatMessageAutoReplyService;
import com.bw.adv.wechat.parse.domain.WeChatBaseInfo;
import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;
import com.bw.adv.wechat.parse.domain.receive.WeChatTextReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatImageSend;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.domain.send.WeChatSubImageSend;
import com.bw.adv.wechat.parse.domain.send.WeChatTextSend;
import com.bw.adv.wechat.parse.service.event.ManagerService;
import com.bw.adv.wechat.parse.util.HttpKit;

@Service
public class TextManagerServiceImpl implements ManagerService {

	protected final Logger logger = Logger.getLogger(TextManagerServiceImpl.class);
	
	private WechatFansMessageService wechatFansMessageService;
	private WeChatFansService weChatFansService;
	private WechatKeysRuleService wechatKeysRuleService;
	private KeysGroupService keysGroupService;
	private WechatMessageAutoReplyService wechatMessageAutoReplyService;
	

	private static final Long COUNT =Long.valueOf(1);
//	private static final String xingJunKey = PropertiesConfInit.getString("webBkConfig", "xingJun_key");//关键字 卯日星君回复
//	private static final String bullDemonKingKey = PropertiesConfInit.getString("webBkConfig", "bullDemonKing_key");//关键字 牛魔王回复
//	private static final String lieutenantsKey = PropertiesConfInit.getString("webBkConfig", "lieutenants_key");//关键字 虾兵蟹将回复
//	
//	private static final String xingJunMediaId = PropertiesConfInit.getString("webBkConfig", "xingJun_mediaId");//关键字 卯日星君回复
//	private static final String bullDemonKingMediaId = PropertiesConfInit.getString("webBkConfig", "bullDemonKing_mediaId");//关键字 牛魔王回复
//	private static final String lieutenantsMediaId = PropertiesConfInit.getString("webBkConfig", "lieutenants_mediaId");//关键字 虾兵蟹将回复
	@Override
	public WeChatSend operatorReceive(WeChatReceive weChatReceive) {
//		WeChatTextReceive weChatTextReceive = null;
//		WechatKeysRule wechatKeysRule =null;
//		WechatKeysRuleExp wechatKeysRuleExp =null;
//		KeysGroupExp keysGroupExp=null;
//		WechatMessageAutoReplyExp wechatMessageAutoReplyExp =null;
//		WechatMessageTemplate wechatMessageTemplate =null;
//		String sendCustomMessageUrl =null;
//		// 消息内容
//		String content = null;
//		boolean isFlag =false;
//		WeChatTextSend text =null;
//		WeChatImageSend image =null;
//		WeChatSubImageSend weChatSubImageSend =null;
//		
//		Map<String,String> map =null;
//		Map<String,String> sendMap =null;
//		
//		try {
//			map =new HashMap<String,String>(); 
//			// text类型
//			weChatTextReceive = (WeChatTextReceive) weChatReceive;
//			content = weChatTextReceive.getContent();
//			// 是否是关键字
//			isFlag = keysGroupService.keywordReplay(content);
//			map = createWechatFansMessageReceiveTextData(weChatTextReceive,isFlag);
//			wechatFansMessageService.saveWechatFansMessage(map);
//			/**
//			 * ----  关键字回复规则-------
//			 * 1.如果包含关键字，那么发送关键字的信息
//			 * 		a.如果查询出的规则组是回复全部的规则，那么调用客服接口发送多条消息
//			 * 		b.如果不是，那么如果是多条的话，就随机回复
//			 * 2.如果不包含关键字，那么查看自动回复消息是否有信息
//			 * 		a.如果没有，那么就不会消息
//			 * 		b.如果有，那么就直接回复
//			 * 
//			 * 以上的回复信息全部保存在fansMessage表中,作为历史记录
//			 */		
//			if(isFlag){
//				wechatKeysRule = wechatKeysRuleService.queryWechatKeyRule(content);
//				wechatKeysRuleExp =wechatKeysRuleService.queryWechatKeyRuleExpByKeyRule(wechatKeysRule);
//				// 如果匹配到规则组
//				if(wechatKeysRuleExp.getKeysGroupList().size()>0){
//					keysGroupExp= keysGroupService.queryKeysGroupExpBykeysGroup(wechatKeysRuleExp.getKeysGroupList().get(0));
//					// 判断回复规则 默认是随机回复  是全部回复
//					if(keysGroupExp.getIsAllReply().equals(WechatTypeConstant.IS_ALL_REPLY.getCode())){
//						// 如果有对应的消息模板
//						if(keysGroupExp.getWechatMessageTemplateList().size()>0){
//							// 对应的消息模版只有一条 
//							if(keysGroupExp.getWechatMessageTemplateList().size() == COUNT){
//								// 判断类型，然后发送不同的模版消息
//								wechatMessageTemplate =keysGroupExp.getWechatMessageTemplateList().get(0);
//								// 如果是text文本类型
//								if(wechatMessageTemplate.getWechatMessageType().equals(WeChatBaseInfo.MSG_TYPE_TEXT)){
//									text = WeChatTextSend.createWeChatTextSend(weChatReceive);
//									text.setContent(wechatMessageTemplate.getContent());
//									sendMap = createWechatFansMessageSendTextData(text,isFlag);
//									wechatFansMessageService.saveWechatFansMessage(sendMap);
//									return text;
//									// 如果是image类型
//								}else if(wechatMessageTemplate.getWechatMessageType().equals(WeChatBaseInfo.MSG_TYPE_IMAGE)){
//									image =WeChatImageSend.createWeChatImageSend(weChatReceive);
//									weChatSubImageSend =new WeChatSubImageSend();
//									weChatSubImageSend.setMediaId(wechatMessageTemplate.getMediaId()); 
//									image.setWeChatSubImageSend(weChatSubImageSend);
//									sendMap = createWechatFansMessageSendImageData(image,isFlag);
//									wechatFansMessageService.saveWechatFansMessage(sendMap);
//									return image;
//								}
//								// 回复多条，调用客服发送接口 
//							}else{
//								sendCustomMessageUrl = 
//										ApiWeChat.SEND_CUSTOM_MESSAGE.replace("ACCESS_TOKEN", ApiAccessToken.getAccessTokenString());
//								sendFansMessageByKeyWords(sendCustomMessageUrl,keysGroupExp.getWechatMessageTemplateList(),weChatTextReceive.getFromUserName());
//							}
//						}
//						// 不是全部回复，那么随机回复 
//					}else{
//						if(keysGroupExp.getWechatMessageTemplateList().size()>0){
//							// 随机重排
//							Collections.shuffle(keysGroupExp.getWechatMessageTemplateList());
//							wechatMessageTemplate =keysGroupExp.getWechatMessageTemplateList().get(0);
//							if(wechatMessageTemplate.getWechatMessageType().equals(WeChatBaseInfo.MSG_TYPE_TEXT)){
//								text = WeChatTextSend.createWeChatTextSend(weChatReceive);
//								text.setContent(wechatMessageTemplate.getContent());
//								sendMap = createWechatFansMessageSendTextData(text,isFlag);
//								wechatFansMessageService.saveWechatFansMessage(sendMap);
//								return text;
//								// 如果是image类型
//							}else if(wechatMessageTemplate.getWechatMessageType().equals(WeChatBaseInfo.MSG_TYPE_IMAGE)){
//								image =WeChatImageSend.createWeChatImageSend(weChatReceive);
//								weChatSubImageSend =new WeChatSubImageSend();
//								weChatSubImageSend.setMediaId(wechatMessageTemplate.getMediaId()); 
//								image.setWeChatSubImageSend(weChatSubImageSend);
//								sendMap = createWechatFansMessageSendImageData(image,isFlag);
//								wechatFansMessageService.saveWechatFansMessage(sendMap);
//								return image;
//							}
//						}
//					}
//				}
//				// 如果没有匹配到关键字,消息自动回复
//			}else{
//				wechatMessageAutoReplyExp = 
//						wechatMessageAutoReplyService.queryWechatMessageAutoReply(WechatTypeConstant.AUTOREPLAY_TYPE_AUTOREPLAY.getCode());
//				if(null !=wechatMessageAutoReplyExp){
//					if(wechatMessageAutoReplyExp.getReplytype().equals(WechatTypeConstant.AUTOREPLAY_TYPE_AUTOREPLAY.getCode())){
//						// 回复文本 
//						if(wechatMessageAutoReplyExp.getWechatMessageTemplateList().get(0).getWechatMessageType().equals(WeChatBaseInfo.MSG_TYPE_TEXT)){
//							text = WeChatTextSend.createWeChatTextSend(weChatReceive);
//							text.setContent(wechatMessageAutoReplyExp.getWechatMessageTemplateList().get(0).getContent());
//							sendMap = createWechatFansMessageSendTextData(text,isFlag);
//							wechatFansMessageService.saveWechatFansMessage(sendMap);
//							return text;
//							// 回复图片
//						}else if(wechatMessageAutoReplyExp.getWechatMessageTemplateList().get(0).getWechatMessageType().equals(WeChatBaseInfo.MSG_TYPE_IMAGE)){
//							image =WeChatImageSend.createWeChatImageSend(weChatReceive);
//							weChatSubImageSend =new WeChatSubImageSend();
//							weChatSubImageSend.setMediaId(wechatMessageAutoReplyExp.getWechatMessageTemplateList().get(0).getMediaId()); 
//							image.setWeChatSubImageSend(weChatSubImageSend);
//							sendMap = createWechatFansMessageSendImageData(image,isFlag);
//							wechatFansMessageService.saveWechatFansMessage(sendMap);
//							return image;
//						}
//					}
//				}
//			}
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			e.printStackTrace();
//		}
		return null;
		//return handleTextOperator(weChatReceive);
	}
	
	private WeChatSend handleTextOperator(WeChatReceive weChatReceive){
		WeChatImageSend imageSend = null;
		WeChatSubImageSend subImageSend = null;
		WeChatTextReceive textReceive = null;
		String content = null;
		String mediaId = null;
		imageSend = WeChatImageSend.createWeChatImageSend(weChatReceive);
		subImageSend =new WeChatSubImageSend();
		textReceive = (WeChatTextReceive) weChatReceive;
		content = textReceive.getContent();
		mediaId = getMediaId(content);
		if(StringUtils.isEmpty(mediaId)){
			return null;
		}else{
			subImageSend.setMediaId(mediaId);
			imageSend.setWeChatSubImageSend(subImageSend);
		}
		return imageSend;
	}
	
	private String getMediaId(String content){
//		if(content.equals(xingJunKey)){
//			return xingJunMediaId;
//		}else if(content.equals(bullDemonKingKey)){
//			return bullDemonKingMediaId;
//		}else if(content.equals(lieutenantsKey)){
//			return lieutenantsMediaId;
//		}
		return null;
	}
	/**
	 * 
	 * createWechatFansMessageSendImageData:(记录图片类型发送信息). <br/>
	 * Date: 2015年8月27日 下午3:34:25 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param image
	 * @param isFlag
	 * @return
	 */
	private Map<String, String> createWechatFansMessageSendImageData(WeChatImageSend image, boolean isFlag) {
		
		Long  wechatFanId =null;
		String fromUsername =null;
		String isReply =null;
		String msgType =null;
		String mediaId =null;
		String isKeyWord =null;
		List<WechatFans> wechatFansList = null;
		WechatFans wechatFans = null; 
		Map<String,String> map =null;
		
		fromUsername = image.getToUserName();// openId
		mediaId =image.getWeChatSubImageSend().getMediaId();// mediaId
		msgType = WeChatBaseInfo.MSG_TYPE_IMAGE;// 消息类型
		isReply = WechatTypeConstant.IS_REPLY.getCode();
		isKeyWord = WechatTypeConstant.IS_NOT_KEY_WORD.getCode();// 默认不是关键字
		if(Boolean.TRUE.equals(isFlag)){
			isKeyWord =WechatTypeConstant.IS_KEY_WORD.getCode();
		}else{
			isKeyWord =WechatTypeConstant.IS_NOT_KEY_WORD.getCode();
		}
				
		wechatFansList = weChatFansService.queryWechatFansByParams(fromUsername, null, null, null, null, null, null);
		if (wechatFansList.size() > 0) {
			wechatFans = wechatFansList.get(0);
			wechatFanId = wechatFans.getWechatFansId();
		}
		map =new HashMap<String,String>();
		map.put("mediaId",mediaId);
		map.put("wechatFanId",wechatFanId+"");
		map.put("fromUsername",fromUsername);
		map.put("msgType",msgType);
		map.put("isReply",isReply);
		map.put("isKeyWorld",isKeyWord);
		map.put("isRead", WechatTypeConstant.IS_NOT_READ.getCode());
		return map;
	}

	/**
	 * 
	 * createWechatFansMessageSendTextData:(记录文本发送信息). <br/>
	 * Date: 2015年8月27日 下午3:23:16 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param text
	 * @param isFlag
	 * @return 
	 */
	private Map<String, String> createWechatFansMessageSendTextData(WeChatTextSend text,boolean isFlag) {
		
		Long  wechatFanId =null;
		String fromUsername =null;
		String content =null;
		String isReply =null;
		String msgType =null;
		String isKeyWord =null;
		List<WechatFans> wechatFansList = null;
		WechatFans wechatFans = null; 
		Map<String,String> map =null;
		
		fromUsername = text.getToUserName();// openId
		content = text.getContent();// 回复内容
		msgType = WeChatBaseInfo.MSG_TYPE_TEXT;// 消息类型
		isReply = WechatTypeConstant.IS_REPLY.getCode();
		isKeyWord = WechatTypeConstant.IS_NOT_KEY_WORD.getCode();// 默认不是关键字
		if(Boolean.TRUE.equals(isFlag)){
			isKeyWord =WechatTypeConstant.IS_KEY_WORD.getCode();
		}else{
			isKeyWord =WechatTypeConstant.IS_NOT_KEY_WORD.getCode();
		}
				
		wechatFansList = weChatFansService.queryWechatFansByParams(fromUsername, null, null, null, null, null, null);
		if (wechatFansList.size() > 0) {
			wechatFans = wechatFansList.get(0);
			wechatFanId = wechatFans.getWechatFansId();
		}
		map =new HashMap<String,String>();
		map.put("content",content);
		map.put("wechatFanId",wechatFanId+"");
		map.put("fromUsername",fromUsername);
		map.put("msgType",msgType);
		map.put("isReply",isReply);
		map.put("isKeyWorld",isKeyWord);
		map.put("isRead", WechatTypeConstant.IS_NOT_READ.getCode());
		return map;
	}

	/**
	 * sendFansMessageByKeyWords:(通过客服接口发送信息). <br/>
	 * Date: 2015年8月27日 下午3:22:43 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param sendCustomMessageUrl
	 * @param wechatMessageTemplateList
	 * @param fromUserName
	 * @return
	 */
	private JSONObject sendFansMessageByKeyWords(String sendCustomMessageUrl,List<WechatMessageTemplate> wechatMessageTemplateList,String fromUserName) {
		JSONArray jsonArray =null;
		JSONObject jsonParam =null;
		String result =null;
		JSONObject resultJson =null;
		String errCode =null;
		Map<String,String> map =null;
		try {
			jsonArray = new JSONArray();
			for(WechatMessageTemplate wechatMessageTemplate : wechatMessageTemplateList){
				if(wechatMessageTemplate.getWechatMessageType().equals(WeChatBaseInfo.MSG_TYPE_TEXT)){
					JSONObject json =new JSONObject();
					JSONObject subJson =new JSONObject();
					subJson.put("content", wechatMessageTemplate.getContent());
					json.put("touser", fromUserName);
					json.put("msgtype", wechatMessageTemplate.getWechatMessageType());
					json.put("text", subJson);
					jsonArray.add(json);
				}else if(wechatMessageTemplate.getWechatMessageType().equals(WeChatBaseInfo.MSG_TYPE_IMAGE)){
					JSONObject json =new JSONObject();
					JSONObject subJson =new JSONObject();
					subJson.put("media_id", wechatMessageTemplate.getMediaId());
					json.put("touser", fromUserName);
					json.put("msgtype", wechatMessageTemplate.getWechatMessageType());
					json.put("image", subJson);
					jsonArray.add(json);
				}
			}
			// 循环发送信息
			for(int i=0;i<jsonArray.size();i++){
				jsonParam = (JSONObject)jsonArray.get(i);
				result = HttpKit.post(sendCustomMessageUrl, jsonParam.toString());
				// 记录发送记录 {"errcode":0,"errmsg":"ok"}
				resultJson = JSONObject.fromObject(result);
				errCode =resultJson.getString("errcode");
				// 返回成功,记录发送消息
				if(errCode.equals(WechatTypeConstant.WECHAT_SUCESS_CODE.getId()+"")){
					map = createWechatFansMessageSendAllData(jsonParam,true);
					wechatFansMessageService.saveWechatFansMessage(map);
				}
				logger.info("客服接口发送消息状态:"+resultJson);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return null;
	}

	/**
	 * 默认是关键字
	 * createWechatFansMessageSendAllData:(记录发送成功的消息). <br/>
	 * Date: 2015年8月27日 下午4:47:37 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param jsonParam
	 * @param b
	 * @return
	 */
	private Map<String,String> createWechatFansMessageSendAllData(JSONObject jsonParam, boolean flag) {
		
		Long  wechatFanId =null;
		String fromUsername =null;
		String content =null;
		String mediaId =null;
		String isReply =null;
		String msgType =null;
		String isKeyWord =null;
		List<WechatFans> wechatFansList = null;
		WechatFans wechatFans = null; 
		Map<String,String> map =null;
		JSONObject subJson =null;
		
		map =new HashMap<String,String>();
		// openId
		fromUsername = jsonParam.getString("touser");
		// 已经回复
		isReply = WechatTypeConstant.IS_REPLY.getCode();
		// 默认不是关键字
		isKeyWord = WechatTypeConstant.IS_NOT_KEY_WORD.getCode();
		msgType = jsonParam.getString("msgtype");
		// 如果是text类型
		if(msgType.equals(WeChatBaseInfo.MSG_TYPE_TEXT)){
			subJson = jsonParam.getJSONObject("text");
			content =subJson.getString("content");
		}else if(msgType.equals(WeChatBaseInfo.MSG_TYPE_IMAGE)){
			subJson = jsonParam.getJSONObject("image");
			mediaId =subJson.getString("media_id");
		}
		if(Boolean.TRUE.equals(flag)){
			isKeyWord =WechatTypeConstant.IS_KEY_WORD.getCode();
		}else{
			isKeyWord =WechatTypeConstant.IS_NOT_KEY_WORD.getCode();
		}
		wechatFansList = weChatFansService.queryWechatFansByParams(fromUsername, null, null, null, null, null, null);
		if (wechatFansList.size() > 0) {
			wechatFans = wechatFansList.get(0);
			wechatFanId = wechatFans.getWechatFansId();
		}
		map.put("content",content);
		map.put("mediaId", mediaId);
		map.put("wechatFanId",wechatFanId+"");
		map.put("fromUsername",fromUsername);
		map.put("msgType",msgType);
		map.put("isReply",isReply);
		map.put("isKeyWorld",isKeyWord);
		
		return map;
	}

	/**
	 * 
	 * createWechatFansMessageData:(拼装参数). <br/>
	 * Date: 2015年8月27日 上午10:21:21 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param weChatTextReceive
	 * @return
	 */
	private Map<String,String> createWechatFansMessageReceiveTextData(WeChatTextReceive weChatTextReceive,boolean isFlag) {
		
		Long  wechatFanId =null;
		String fromUsername =null;
		String content =null;
		String isReply =null;
		String msgType =null;
		String isKeyWord =null;
		List<WechatFans> wechatFansList = null;
		WechatFans wechatFans = null; 
		Map<String,String> map =null;
		
		fromUsername = weChatTextReceive.getFromUserName();// openId
		content = weChatTextReceive.getContent();// 回复内容
		isReply = WechatTypeConstant.IS_NOT_REPLY.getCode();
		msgType = WeChatBaseInfo.MSG_TYPE_TEXT;// 消息类型
		isKeyWord = WechatTypeConstant.IS_NOT_KEY_WORD.getCode();// 默认不是关键字
		if(Boolean.TRUE.equals(isFlag)){
			isKeyWord =WechatTypeConstant.IS_KEY_WORD.getCode();
		}else{
			isKeyWord =WechatTypeConstant.IS_NOT_KEY_WORD.getCode();
		}
				
		wechatFansList = weChatFansService.queryWechatFansByParams(fromUsername, null, null, null, null, null, null);
		if (wechatFansList.size() > 0) {
			wechatFans = wechatFansList.get(0);
			wechatFanId = wechatFans.getWechatFansId();
		}
		map =new HashMap<String,String>();
		map.put("content",content);
		map.put("wechatFanId",wechatFanId==null?null:wechatFanId+"");
		map.put("fromUsername",fromUsername);
		map.put("msgType",msgType);
		map.put("isReply",isReply);
		map.put("isKeyWorld",isKeyWord);
		
		return map;
	}

	@Autowired
	public void setWechatFansMessageService(
			WechatFansMessageService wechatFansMessageService) {
		this.wechatFansMessageService = wechatFansMessageService;
	}

	@Autowired
	public void setWeChatFansService(WeChatFansService weChatFansService) {
		this.weChatFansService = weChatFansService;
	}
	
	@Autowired
	public void setWechatKeysRuleService(WechatKeysRuleService wechatKeysRuleService) {
		this.wechatKeysRuleService = wechatKeysRuleService;
	}
	
	@Autowired
	public void setKeysGroupService(KeysGroupService keysGroupService) {
		this.keysGroupService = keysGroupService;
	}
	
	@Autowired
	public void setWechatMessageAutoReplyService(
			WechatMessageAutoReplyService wechatMessageAutoReplyService) {
		this.wechatMessageAutoReplyService = wechatMessageAutoReplyService;
	}
}
