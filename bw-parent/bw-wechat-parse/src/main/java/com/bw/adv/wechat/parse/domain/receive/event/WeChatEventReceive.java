package com.bw.adv.wechat.parse.domain.receive.event;

import com.bw.adv.wechat.parse.domain.WeChatParam;
import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;

public class WeChatEventReceive extends WeChatReceive {
	
	private String event;
	private String eventKey;
    private String ticket;
    private String latitude;	//维度
    private String longitude;	//经度
    private String precision;	//精度
    
	public WeChatEventReceive(WeChatParam weChatParam) {
		super(weChatParam);
		this.event = weChatParam.getEvent();
		this.eventKey = weChatParam.getEventKey();
		this.ticket = weChatParam.getEvent();
		this.latitude = weChatParam.getLabel();
		this.longitude = weChatParam.getLongitude();
		this.precision = weChatParam.getPrecision();
	}

	public String getEvent() {
		return event;
	}

	public String getEventKey() {
		return eventKey;
	}

	public String getTicket() {
		return ticket;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getPrecision() {
		return precision;
	}
}
