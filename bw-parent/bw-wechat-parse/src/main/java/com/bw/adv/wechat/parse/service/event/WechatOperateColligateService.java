/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-wechat-parse
 * File Name:WechatOperateColligateServiceI.java
 * Package Name:com.sage.scrm.wechat.parse.service.event
 * Date:2016年5月23日下午12:06:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.wechat.parse.service.event;

import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;

/**
 * ClassName:WechatOperateColligateService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月23日 下午12:06:09 <br/>
 * scrmVersion 1.0
 * @author   chengdi.cui
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatOperateColligateService {
	
	public WeChatSend resultSendForKing(String token, WeChatEventReceive weChatEventReceive) throws Exception;

}

