package com.bw.adv.wechat.parse.domain;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "xml")
public class WeChatParam {

    //基础属性
	@XmlElement(name = "ToUserName")
    private String toUserName;
	@XmlElement(name = "FromUserName")
    private String fromUserName;
	@XmlElement(name = "CreateTime")
    private Long createTime;
	@XmlElement(name = "MsgType")
    private String msgType;
	@XmlElement(name = "MsgId")
    private Long msgId;
	
    //文本信息
	@XmlElement(name = "Content")
    private String content;

    //图片信息
	@XmlElement(name = "PicUrl")
    private String picUrl;
	@XmlElement(name = "MediaId")
    private String mediaId;

    //语音信息
	@XmlElement(name = "Format")
    private String format;

    //视频
	@XmlElement(name = "ThumbMediaId")
    private Long thumbMediaId;
    
    //地理位置
	@XmlElement(name = "Location_X")
    private String location_X;
	@XmlElement(name = "Location_Y")
    private String location_Y;
	@XmlElement(name = "Scale")
    private String scale;
	@XmlElement(name = "Label")
    private String label;

    //链接
	@XmlElement(name = "Title")
    private String title;
	@XmlElement(name = "Description")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
    private String description;
	@XmlElement(name = "Url")
    private String url;
    
    //事件
	@XmlElement(name = "Event")
    private String event;
	@XmlElement(name = "EventKey")
    private String eventKey;
	@XmlElement(name = "Ticket")
    private String ticket;
	@XmlElement(name = "Latitude")
    private String latitude;	//维度
	@XmlElement(name = "Longitude")
    private String longitude;	//经度
	@XmlElement(name = "Precision")
    private String precision;	//精度
	
    public final String getToUserName() {
        return this.toUserName;
    }

    public final String getFromUserName() {
        return this.fromUserName;
    }

    public final Long getCreateTime() {
        return this.createTime;
    }
    
    public final String getMsgType() {
        return this.msgType;
    }

    public final Long getMsgId() {
        return this.msgId;
    }

    public final String getContent() {
        return this.content;
    }
    
    public final String getPicUrl() {
        return this.picUrl;
    }

    public final String getMediaId() {
        return this.mediaId;
    }
    
    public final String getFormat() {
        return this.format;
    }

    public final Long getThumbMediaId() {
        return this.thumbMediaId;
    }

    public final String getLocation_X() {
        return this.location_X;
    }

    public final String getLocation_Y() {
        return this.location_Y;
    }

    public final String getScale() {
        return this.scale;
    }

    public final String getLabel() {
        return this.label;
    }
    
    public final String getTitle() {
        return this.title;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getUrl() {
        return this.url;
    }
    
	public String getEvent() {
		return event;
	}

	public String getEventKey() {
		return eventKey;
	}

	public String getTicket() {
		return ticket;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getPrecision() {
		return precision;
	}
}
