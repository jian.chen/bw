/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:SubscribeEventServiceImpl.java
 * Package Name:com.sage.scrm.module.wechat.api.service.event.impl
 * Date:2015年8月19日上午11:27:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.wechat.parse.service.event.impl;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.member.constant.ExtAccountTypeConstant;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.wechat.api.ApiWeChatFan;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.wechat.service.WeChatFansService;
import com.bw.adv.wechat.parse.domain.receive.event.WeChatEventReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.domain.send.WeChatTextSend;
import com.bw.adv.wechat.parse.domain.send.WechatNewsArticles;
import com.bw.adv.wechat.parse.domain.send.WechatNewsItem;
import com.bw.adv.wechat.parse.domain.send.WechatNewsSend;
import com.bw.adv.wechat.parse.service.event.EventManagerService;

/**
 * subscribe事件类型处理 ClassName:SubscribeEventServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 上午11:27:54 <br/>
 * scrmVersion 1.0
 * 
 * @author june
 * @version jdk1.7
 * @see
 */
@Service
public class SubscribeEventServiceImpl implements EventManagerService {

	protected  Logger logger = Logger
			.getLogger(SubscribeEventServiceImpl.class);

	private WeChatFansService weChatFansService;
	private ApiWeChatFan weChatFanAPI;
	private AccessTokenCacheService accessTokenCacheService;

	private MemberService memberService;
	@Value("${subscribe_title}")
	private  String subscribeTitle;
	@Value("${subscribe_description}")
	private  String subscribeDescription;
	@Value("${subscribe_url}")
	private  String subscribeUrl;
	@Value("${subscribe_picurl}")
	private  String subscribePicurl;

	@Value("${subscribe_title2}")
	private  String subscribeTitle2;
	@Value("${subscribe_title3}")
	private  String subscribeTitle3;
	@Value("${subscribe_url2}")
	private  String subscribeUrl2;
	@Value("${subscribe_picurl2}")
	private  String subscribePicurl2;
	@Value("${subscribe_welcome}")
	private  String subscribeWelcome;

	/**
	 * 带参数二维码关注事件解析事件
	 */
	@Override
	@Transactional
	public WeChatSend operatorReceive(WeChatEventReceive weChatEventReceive)
			throws Exception {
		return getWeChatSend(weChatEventReceive);
	}

	/**
	 * getWeChatSend:(关注后事件处理). <br/>
	 * Date: 2015年8月19日 下午2:44:54 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author june
	 * @version jdk1.7
	 * @param weChatEventReceive
	 * @return
	 */
	@Transactional
	private WeChatSend getWeChatSend(WeChatEventReceive weChatEventReceive) {
		String fromUserName = null;
		JSONObject resultResult = null;

		String headImgUrl = null;
		String unionid = null;
		String nickName = null;
		String sex = null;
		String cty = null;
		String province = null;
		String country = null;
		String language = null;

		Long channelId = null;// 渠道Id
		String token = null;
		WechatFans wechatFans = null;
		String eventKey = null;
		String[] eventKeys = null;
		try {
			wechatFans = new WechatFans();
			eventKey = weChatEventReceive.getEventKey();
			if (StringUtils.isNotEmpty(eventKey)) {
				eventKeys = eventKey.split("_");
			}
			if (null != eventKeys && eventKeys.length == 2) {
				channelId = Long.valueOf(eventKeys[1]);
				// 通过微信消息解析来的
				wechatFans.setQrCodeId(channelId);
				wechatFans.setChannelId(ChannelConstant.WEIXIN.getId());
			}
			fromUserName = weChatEventReceive.getFromUserName();// 微信openId
			resultResult = weChatFanAPI.getFanInfo(fromUserName);

			headImgUrl = (String) resultResult.get("headimgurl");
			unionid = (String) resultResult.get("unionid");
			nickName = (String) resultResult.get("nickname");
			sex = resultResult.get("sex") == null ? null : resultResult.get(
					"sex").toString();
			cty = (String) resultResult.get("city");
			province = (String) resultResult.get("province");
			country = (String) resultResult.get("country");
			language = (String) resultResult.get("language");

			wechatFans.setUnionid(unionid);
			wechatFans.setNickName(nickName);
			wechatFans.setSex(sex);
			wechatFans.setCty(cty);
			wechatFans.setProvince(province);
			wechatFans.setCountry(country);
			wechatFans.setLanguage(language);
			wechatFans.setHeadImgUrl(headImgUrl);
			wechatFans.setOpenid(fromUserName);

			// 保存粉丝信息
			weChatFansService.saveOrUpdate(wechatFans);

			Member member = memberService.queryMemberByBindingAccount(
					ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getCode(),
					fromUserName);
			if (member != null) {
				member.setMemberPhoto(headImgUrl);
				memberService.updateByPkSelective(member);
			} else {
				memberService.saveMemberInfo(wechatFans);
			}

			token = accessTokenCacheService.getAccessTokenCache();
			return imageTextSend(token, fromUserName, weChatEventReceive, "N");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 关注推送图文消息
	 * 
	 * @param token
	 * @param openId
	 * @param weChatEventReceive
	 * @return
	 * @author yu.zhang
	 */
	private WeChatSend textTextSend(String token, String openId,WeChatEventReceive weChatEventReceive) {
		WeChatTextSend textSend = WeChatTextSend
				.createWeChatTextSend(weChatEventReceive);
		textSend.setContent(subscribeWelcome);
		return textSend;
	}

	/**
	 * 关注推送图文消息
	 * 
	 * @param token
	 * @param openId
	 * @param weChatEventReceive
	 * @return
	 * @author yu.zhang
	 */
	private WeChatSend imageTextSend(String token, String openId,WeChatEventReceive weChatEventReceive, String Flag) {
		WechatNewsSend newsSend = WechatNewsSend.createWeChatTextSend(weChatEventReceive);
		WechatNewsArticles articles = new WechatNewsArticles();
		WechatNewsItem item = new WechatNewsItem();
		item.setTitle("BUD金尊促销平台");
		item.setDescription("BUD金尊促销平台");
		item.setUrl("http://supreme.amj-sh.com/bw-console-wechat/view/turntable/present.jsp");
		item.setPicUrl("http://supreme.amj-sh.com/bw-console-file/file/view/image?fileId=550E8400E29B11D4A716446655440000");

		List<WechatNewsItem> items = new ArrayList<WechatNewsItem>();
		items.add(item);
		articles.setNewsItems(items);
		newsSend.setArticleCount(items.size());
		newsSend.setArticles(articles);

		return newsSend;
	}

	@Autowired
	public void setWeChatFansService(WeChatFansService weChatFansService) {
		this.weChatFansService = weChatFansService;
	}

	@Autowired
	public void setWeChatFanAPI(ApiWeChatFan weChatFanAPI) {
		this.weChatFanAPI = weChatFanAPI;
	}

	@Autowired
	public void setAccessTokenCacheService(
			AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

}
