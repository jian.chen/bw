package com.bw.adv.wechat.parse.service.event;

import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;


public interface ManagerService {

    public WeChatSend operatorReceive(WeChatReceive weChatReceive);
}
