package com.bw.adv.wechat.parse.domain.send;


import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.wechat.parse.domain.WeChatBaseInfo;
import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;

public abstract class WeChatSend extends WeChatBaseInfo {
	
	public final static String MSG_TYPE_TEXT = "text";
	public final static String MSG_TYPE_IMAGE = "image";
	public final static String MSG_TYPE_NEWS = "news";
	
    public WeChatSend() {
		
    }
	
    public WeChatSend(String msgType) {
    	
        super(msgType);
    }
	
    public void changeFromToUsersByReceiveWithCreateTime(WeChatReceive weChatReceive) {
        super.setToUserName(weChatReceive.getFromUserName());
        super.setFromUserName(weChatReceive.getToUserName());
        super.setCreateTime(DateUtils.getCurrentDate().getTime());
    }
    
}
