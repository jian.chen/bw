/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:WeChatImage.java
 * Package Name:com.sage.scrm.module.wechat.api.domain.send
 * Date:2015年8月24日下午2:34:44
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.wechat.parse.domain.send;

import javax.xml.bind.annotation.XmlElement;

/**
 * ClassName:WeChatImage <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午2:34:44 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WeChatSubImageSend{
	
	private String mediaId;
	
	@XmlElement(name="MediaId")
	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	
}

