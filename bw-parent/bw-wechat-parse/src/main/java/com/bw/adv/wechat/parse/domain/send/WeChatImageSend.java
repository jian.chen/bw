/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:WeChatImageSend.java
 * Package Name:com.sage.scrm.module.wechat.api.domain.send
 * Date:2015年8月21日下午3:43:57
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.wechat.parse.domain.send;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;

/**
 * ClassName:WeChatImageSend <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月21日 下午3:43:57 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@XmlRootElement(name="xml")
public class WeChatImageSend extends WeChatSend {
	
	public static WeChatImageSend createWeChatImageSend(WeChatReceive weChatReceive) {
		WeChatImageSend image = new WeChatImageSend();
		image.changeFromToUsersByReceiveWithCreateTime(weChatReceive);
    	return image;
    }
	
	private WeChatSubImageSend weChatSubImageSend;

	public WeChatImageSend() {
		super(WeChatSend.MSG_TYPE_IMAGE);
	}
	
	@XmlElement(name="Image")
	public WeChatSubImageSend getWeChatSubImageSend() {
		return weChatSubImageSend;
	}
	
	public void setWeChatSubImageSend(WeChatSubImageSend weChatSubImageSend) {
		this.weChatSubImageSend = weChatSubImageSend;
	}

	
}

