package com.bw.adv.wechat.parse.domain.send;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class WechatNewsArticles {
	private List<WechatNewsItem> newsItems = new ArrayList<WechatNewsItem>();

	@XmlElement(name="item")
	public List<WechatNewsItem> getNewsItems() {
		return newsItems;
	}

	public void setNewsItems(List<WechatNewsItem> newsItems) {
		this.newsItems = newsItems;
	}
}
