package com.bw.adv.module.order.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.model.MemberGift;
import com.bw.adv.module.member.model.exp.MemberGiftExt;
import com.bw.adv.module.order.mapper.MemberGiftMapper;
import com.bw.adv.module.order.repository.MemberGiftRepository;


/**
 * ClassName: 我的礼品<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:42:18 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
@Repository
public class MemberGiftRepositoryImpl extends BaseRepositoryImpl<MemberGift, MemberGiftMapper> implements MemberGiftRepository{

	@Override
	protected Class<MemberGiftMapper> getMapperClass() {
		return MemberGiftMapper.class;
	}

	@Override
	public MemberGift getCountNotReceiveMemberGiftRecord(Long memberId, Long couponId) {
		return this.getMapper().selectCountNotReceiveMemberGiftRecord(memberId,couponId);
	}

	@Override
	public List<MemberGiftExt> queryReceiveMemberGiftRecord(Long memberId) {
		return this.getMapper().selectReceiveMemberGiftRecord(memberId);
	}

	@Override
	public List<MemberGift> queryReceiveMemberGiftByGiftCode(String giftCode,Long memberId) {
		return this.getMapper().selectReceiveMemberGiftByGiftCode(giftCode, memberId);
	}
	
}