package com.bw.adv.module.order.repository;


import java.math.BigDecimal;
import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.order.mapper.OrderHeaderMapper;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.exp.OrderHeaderExp;


public interface OrderHeaderRepository extends BaseRepository<OrderHeader, OrderHeaderMapper> {
	/**
	 * 
	 * findExpByExample:根据example查询订单list
	 * Date: 2015年9月2日 下午3:54:12 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<OrderHeaderExp> findExpByExample(Example example,Page<OrderHeaderExp> page);
	
	public List<OrderHeaderExp> findorderExpByExample(Example example,Page<OrderHeaderExp> page);
	
	/**
	 * findByExternalId:(根据外部订单号查询订单信息). <br/>
	 * Date: 2015-12-14 下午3:19:05 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param externalId
	 * @return
	 */
	public OrderHeader findByExternalId(String externalId);
	
	/**
	 * findByExternalIdForDayNumber:(查询多少天内的订单). <br/>
	 * Date: 2016-1-6 下午6:19:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param externalId
	 * @return
	 */
	public OrderHeader findByExternalIdForDayNumber(String externalId,int dayNumber);
	
	
	public List<OrderHeaderExp> findExpByExa(String memberCode,Page<OrderHeaderExp> page);
	
	public List<OrderHeaderExp> findExpListByExa(Example example,Page<OrderHeaderExp> page);
	
	public List<OrderHeaderExp> findExpListByExample(Example example,Page<OrderHeaderExp> page);
	
	public OrderHeaderExp findOrderPointsById(Long orderId);
	
	public OrderHeaderExp findOrderCouponById(Long orderId);
	
	/**
	 * 查询用户的消费门店
	 * @param memberId
	 * @return
	 */
	public List<OrderHeader> selectOrderHeaderByMemberIdForStoreId(Long memberId,String storeId);
	
	/**
	 * OrderCount
	 * @param memberId
	 * @param startTime
	 * @param endTime
	 * @param countStart
	 * @param countEnd
	 * @return
	 */
	public List<OrderHeader> selectOrderCount(Long memberId,String startTime,String endTime,Long countStart,Long countEnd);
	
	/**
	 * OrderGrand
	 * @param memberId
	 * @param startTime
	 * @param endTime
	 * @param sumStart
	 * @param sumEnd
	 * @return
	 */
	public List<OrderHeader> selectOrderGrand(Long memberId,String startTime,String endTime,Long sumStart,Long sumEnd);
	
	/**
	 * OrderOne
	 * @param memberId
	 * @param startTime
	 * @param endTime
	 * @param oneStart
	 * @param oneEnd
	 * @return
	 */
	public List<OrderHeader> selectOrderOne(Long memberId,String startTime,String endTime,Long oneStart,Long oneEnd);

	/**
	 * air助手
	 * showShopOrderCount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param storeCode
	 * @param date
	 * @return
	 */
	public List<OrderHeaderExp> showShopOrderCount(String storeCode,String date);
	
	/**
	 * 根据会员和金额要求查询本月消费次数
	 * @param amount
	 * @param memberId
	 * @return
	 */
	public Long queryCountByAmountAndMemberId(Long amount,Long memberId,String orderDate);
	
	
	/**
	 * 查询会员一天消费总额
	 * @param orderDate
	 * @return
	 */
	BigDecimal queryEveryDayOrderAmount(Long memberId,String orderDate);
	
}




