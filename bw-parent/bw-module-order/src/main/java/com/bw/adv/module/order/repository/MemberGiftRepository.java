package com.bw.adv.module.order.repository;


import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.model.MemberGift;
import com.bw.adv.module.member.model.exp.MemberGiftExt;
import com.bw.adv.module.order.mapper.MemberGiftMapper;


/**
 * ClassName: 我的礼品<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:42:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface MemberGiftRepository extends BaseRepository<MemberGift, MemberGiftMapper> {

	MemberGift getCountNotReceiveMemberGiftRecord(Long memberId, Long couponId);
	
	/**
	 * 统计我的奖品
	 * @param memberId
	 * @return
	 */
	List<MemberGiftExt> queryReceiveMemberGiftRecord(Long memberId);
	
	/**
	 * 统计我的奖品根据奖品
	 * @param memberId
	 * @return
	 */
	List<MemberGift> queryReceiveMemberGiftByGiftCode(String giftCode,Long memberId);
	
}