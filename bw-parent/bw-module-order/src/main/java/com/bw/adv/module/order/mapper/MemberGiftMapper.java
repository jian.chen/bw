package com.bw.adv.module.order.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.MemberGift;
import com.bw.adv.module.member.model.exp.MemberGiftExt;


public interface MemberGiftMapper extends BaseMapper<MemberGift> {

	MemberGift selectCountNotReceiveMemberGiftRecord(Long memberId, Long couponId);
	
	/**
	 * 统计我的奖品
	 * @param memberId
	 * @return
	 */
	List<MemberGiftExt> selectReceiveMemberGiftRecord(Long memberId);
	
	/**
	 * 统计我的奖品根据奖品
	 * @param memberId
	 * @return
	 */
	List<MemberGift> selectReceiveMemberGiftByGiftCode(@Param("giftCode")String giftCode,@Param("memberId")Long memberId);
	
}