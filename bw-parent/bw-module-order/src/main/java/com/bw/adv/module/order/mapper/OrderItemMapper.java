package com.bw.adv.module.order.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.order.model.OrderItem;

public interface OrderItemMapper extends BaseMapper<OrderItem>{
	/**
	 * 
	 * queryOrderItemByOrderId:按订单id查询订单明细. <br/>
	 * Date: 2016年5月27日 下午4:07:41 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param orderId
	 * @param page
	 * @return
	 */
	public List<OrderItem> selectOrderItemByOrderId(@Param("orderId")Long orderId,Page<OrderItem> page);
	
	/**
	 * 
	 * queryGradeListForJS:查询产品id和产品名称. <br/>
	 * Date: 2016年5月29日 下午3:28:40 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @return
	 */
	@Select("select PRODUCT_CODE as productCode,PRODUCT_NAME as productName from product order by PRODUCT_ID")
   	public List<Product> queryProductListForJS();
}