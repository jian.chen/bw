package com.bw.adv.module.order.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.order.model.OrderPaymentMethod;

public interface OrderPaymentMethodMapper extends BaseMapper<OrderPaymentMethod>{
	
}