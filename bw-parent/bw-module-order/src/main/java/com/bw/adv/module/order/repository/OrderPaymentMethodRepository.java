package com.bw.adv.module.order.repository;


import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.order.mapper.OrderPaymentMethodMapper;
import com.bw.adv.module.order.model.OrderPaymentMethod;


/**
 * ClassName: 订单支付方式<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:42:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface OrderPaymentMethodRepository extends BaseRepository<OrderPaymentMethod, OrderPaymentMethodMapper> {
	
}