package com.bw.adv.module.order.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.order.model.PaymentMethod;

public interface PaymentMethodMapper extends BaseMapper<PaymentMethod>{
	
}