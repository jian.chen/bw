package com.bw.adv.module.order.repository.impl;


import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.order.mapper.OrderPaymentMethodMapper;
import com.bw.adv.module.order.model.OrderPaymentMethod;
import com.bw.adv.module.order.repository.OrderPaymentMethodRepository;


/**
 * ClassName: 订单支付方式<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:42:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class OrderPaymentMethodRepositoryImpl extends BaseRepositoryImpl<OrderPaymentMethod, OrderPaymentMethodMapper> implements OrderPaymentMethodRepository{

	@Override
	protected Class<OrderPaymentMethodMapper> getMapperClass() {
		return OrderPaymentMethodMapper.class;
	}
	
}