package com.bw.adv.module.order.repository.impl;


import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.order.mapper.OrderCouponMapper;
import com.bw.adv.module.order.model.OrderCoupon;
import com.bw.adv.module.order.repository.OrderCouponRepository;


/**
 * ClassName: 订单优惠<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:42:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class OrderCouponRepositoryImpl extends BaseRepositoryImpl<OrderCoupon, OrderCouponMapper> implements OrderCouponRepository{

	@Override
	protected Class<OrderCouponMapper> getMapperClass() {
		return OrderCouponMapper.class;
	}
	
}