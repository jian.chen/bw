package com.bw.adv.module.order.repository;


import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.order.mapper.OrderItemMapper;
import com.bw.adv.module.order.model.OrderItem;


/**
 * ClassName: 订单明细 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:42:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface OrderItemRepository extends BaseRepository<OrderItem, OrderItemMapper> {
	
	/**
	 * 
	 * queryOrderItemByOrderId:按订单id查询订单明细. <br/>
	 * Date: 2016年5月27日 下午4:01:44 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param orderId
	 * @param page
	 * @return
	 */
	public List<OrderItem> queryOrderItemByOrderId(Long orderId,Page<OrderItem> page);
	
	/**
	 * 
	 * queryProductListForJS:查询产品id和产品名称. <br/>
	 * Date: 2016年5月29日 下午3:43:26 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @return
	 */
	public List<Product> queryProductListForJS();
}