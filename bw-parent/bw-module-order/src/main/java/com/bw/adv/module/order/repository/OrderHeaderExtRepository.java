package com.bw.adv.module.order.repository;


import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.order.mapper.OrderHeaderExtMapper;
import com.bw.adv.module.order.model.OrderHeaderExt;


/**
 * ClassName: OrderHeaderExtRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午8:27:23 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface OrderHeaderExtRepository extends BaseRepository<OrderHeaderExt, OrderHeaderExtMapper> {
	
}