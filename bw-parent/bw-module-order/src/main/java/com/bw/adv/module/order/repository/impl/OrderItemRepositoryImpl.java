package com.bw.adv.module.order.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.order.mapper.OrderItemMapper;
import com.bw.adv.module.order.model.OrderItem;
import com.bw.adv.module.order.repository.OrderItemRepository;


/**
 * ClassName: 订单明细 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:42:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class OrderItemRepositoryImpl extends BaseRepositoryImpl<OrderItem, OrderItemMapper> implements OrderItemRepository{

	@Override
	protected Class<OrderItemMapper> getMapperClass() {
		return OrderItemMapper.class;
	}

	@Override
	public List<OrderItem> queryOrderItemByOrderId(Long orderId, Page<OrderItem> page) {
		return this.getMapper().selectOrderItemByOrderId(orderId, page);
	}

	@Override
	public List<Product> queryProductListForJS() {
		return this.getMapper().queryProductListForJS();
	}
	
}