package com.bw.adv.module.order.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.order.mapper.OrderHeaderExtMapper;
import com.bw.adv.module.order.model.OrderHeaderExt;
import com.bw.adv.module.order.repository.OrderHeaderExtRepository;


/**
 * ClassName: OrderHeaderExtRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午8:27:19 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class OrderHeaderExtRepositoryImpl extends BaseRepositoryImpl<OrderHeaderExt, OrderHeaderExtMapper> implements OrderHeaderExtRepository{

	@Override
	protected Class<OrderHeaderExtMapper> getMapperClass() {
		return OrderHeaderExtMapper.class;
	}

}