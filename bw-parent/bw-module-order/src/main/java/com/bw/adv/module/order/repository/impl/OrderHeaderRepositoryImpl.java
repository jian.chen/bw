package com.bw.adv.module.order.repository.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.order.mapper.OrderHeaderMapper;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.exp.OrderHeaderExp;
import com.bw.adv.module.order.repository.OrderHeaderRepository;
import com.bw.adv.module.tools.DateUtils;


/**
 * 
 * ClassName: DistinctTypeRepository <br/>
 * date: 2015-8-12 下午3:51:27 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class OrderHeaderRepositoryImpl extends BaseRepositoryImpl<OrderHeader, OrderHeaderMapper> implements OrderHeaderRepository{

	@Override
	protected Class<OrderHeaderMapper> getMapperClass() {
		return OrderHeaderMapper.class;
	}

	@Override
	public List<OrderHeaderExp> findExpByExample(Example example,
			Page<OrderHeaderExp> page) {
		
		return this.getMapper().selectExpByExample(example, page);
	}
	
	@Override
	public List<OrderHeaderExp> findorderExpByExample(Example example,
			Page<OrderHeaderExp> page) {
		
		return this.getMapper().selectOrderExpByExample(example, page);
	}

	@Override
	public OrderHeader findByExternalId(String externalId) {
		return this.getMapper().selectByExternalId(externalId);
	}
	
	@Override
	public OrderHeader findByExternalIdForDayNumber(String externalId, int dayNumber) {
		String beforeDay = DateUtils.addDays(0-dayNumber)+"000000";
		return this.getMapper().selectByExternalIdForDayNumber(externalId,beforeDay);
	}

	@Override
	public List<OrderHeaderExp> findExpByExa(String memberCode,Page<OrderHeaderExp> page) {
		return this.getMapper().selectOrderExpByExa(memberCode,page);
	}

	@Override
	public OrderHeaderExp findOrderPointsById(Long orderId) {
		return this.getMapper().selectOrderPoints(orderId);
	}

	@Override
	public OrderHeaderExp findOrderCouponById(Long orderId) {
		return this.getMapper().selectOrderCoupon(orderId);
	}
	
	@Override
	public List<OrderHeaderExp> findExpListByExa(Example example,Page<OrderHeaderExp> page) {
		return this.getMapper().selectOrderExpListByExa(example, page);
	}
	@Override
	public List<OrderHeaderExp> findExpListByExample(Example example,Page<OrderHeaderExp> page) {
		return this.getMapper().selectOrderExpListByExample(example, page);
	}

	@Override
	public List<OrderHeader> selectOrderHeaderByMemberIdForStoreId(Long memberId,String storeId) {
		return this.getMapper().selectOrderHeaderByMemberIdForStoreId(memberId,storeId);
	}

	@Override
	public List<OrderHeader> selectOrderCount(Long memberId, String startTime, String endTime, Long countStart,
			Long countEnd) {
		return this.getMapper().selectOrderCount(memberId, startTime, endTime, countStart, countEnd);
	}

	@Override
	public List<OrderHeader> selectOrderGrand(Long memberId, String startTime, String endTime, Long sumStart,
			Long sumEnd) {
		return this.getMapper().selectOrderGrand(memberId, startTime, endTime, sumStart, sumEnd);
	}

	@Override
	public List<OrderHeader> selectOrderOne(Long memberId, String startTime, String endTime, Long oneStart,
			Long oneEnd) {
		return this.getMapper().selectOrderOne(memberId, startTime, endTime, oneStart, oneEnd);
	}

	@Override
	public List<OrderHeaderExp> showShopOrderCount(String storeCode, String date) {
		return this.getMapper().showShopOrderCount(storeCode, date);
	}

	@Override
	public Long queryCountByAmountAndMemberId(Long amount, Long memberId,String orderDate) {
		return this.getMapper().queryCountByAmountAndMemberId(amount, memberId, orderDate);
	}

	@Override
	public BigDecimal queryEveryDayOrderAmount(Long memberId, String orderDate) {
		return this.getMapper().queryEveryDayOrderAmount(memberId, orderDate);
	}

}