package com.bw.adv.module.order.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.exp.OrderHeaderExp;

public interface OrderHeaderMapper extends BaseMapper<OrderHeader>{
    
	List<OrderHeaderExp> selectExpByExample(@Param("example")Example example,Page<OrderHeaderExp> page);
	
	List<OrderHeaderExp> selectOrderExpByExample(@Param("example")Example example,Page<OrderHeaderExp> page);
	
	List<OrderHeaderExp> selectOrderExpByExa(@Param("memberCode")String memberCode,Page<OrderHeaderExp> page);
	
	OrderHeaderExp selectOrderPoints(@Param("orderId")Long orderId);
	
	OrderHeaderExp selectOrderCoupon(@Param("orderId")Long orderId);
	
	List<OrderHeaderExp> selectOrderExpListByExa(@Param("example")Example example,Page<OrderHeaderExp> page);
	
	List<OrderHeaderExp> selectOrderExpListByExample(@Param("example")Example example,Page<OrderHeaderExp> page);
	
	/**
	 * selectByExternalId:(根据外部订单号，查询订单). <br/>
	 * Date: 2015-12-14 下午3:22:35 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param externalId
	 * @return
	 */
	OrderHeader selectByExternalId(@Param("externalId")String externalId);
	
	/**
	 * 查询用户的消费门店
	 * @param memberId
	 * @return
	 */
	@Select("SELECT MEMBER_ID "
			+ "from order_header "
			+ "where MEMBER_ID = #{memberId} and STORE_ID in ( #{storeId} )")
	List<OrderHeader> selectOrderHeaderByMemberIdForStoreId(@Param("memberId")Long memberId,@Param("storeId")String storeId);
	
	/**
	 * OrderCount
	 * @param memberId
	 * @param startTime
	 * @param endTime
	 * @param countStart
	 * @param countEnd
	 * @return
	 */
	@Select("SELECT MEMBER_ID "
			+ "from order_header "
			+ "where MEMBER_ID = #{memberId} "
			+ "and ORDER_DATE >= #{startTime} "
			+ "and ORDER_DATE <= #{endTime} "
			+ "HAVING COUNT(MEMBER_ID) >= #{countStart} "
			+ "AND COUNT(MEMBER_ID) <= #{countEnd} ")
	List<OrderHeader> selectOrderCount(@Param("memberId")Long memberId,@Param("startTime")String startTime,@Param("endTime")String endTime,@Param("countStart")Long countStart,@Param("countEnd")Long countEnd);
	
	/**
	 * OrderGrand
	 * @param memberId
	 * @param startTime
	 * @param endTime
	 * @param sumStart
	 * @param sumEnd
	 * @return
	 */
	@Select("SELECT MEMBER_ID "
			+ "from order_header "
			+ "where MEMBER_ID = #{memberId} "
			+ "and ORDER_DATE >= #{startTime} "
			+ "and ORDER_DATE <= #{endTime} "
			+ "HAVING SUM(ORDER_AMOUNT) >= #{sumStart} "
			+ "AND SUM(ORDER_AMOUNT) <= #{sumEnd} ")
	List<OrderHeader> selectOrderGrand(@Param("memberId")Long memberId,@Param("startTime")String startTime,@Param("endTime")String endTime,@Param("sumStart")Long sumStart,@Param("sumEnd")Long sumEnd);
	
	/**
	 * OrderOne
	 * @param memberId
	 * @param startTime
	 * @param endTime
	 * @param oneStart
	 * @param oneEnd
	 * @return
	 */
	@Select("SELECT MEMBER_ID "
			+ "from order_header "
			+ "where MEMBER_ID = #{memberId} "
			+ "and ORDER_DATE >= #{startTime} "
			+ "and ORDER_DATE <= #{endTime} "
			+ "AND ORDER_AMOUNT >= #{oneStart} "
			+ "AND ORDER_AMOUNT <= #{oneEnd} ")
	List<OrderHeader> selectOrderOne(@Param("memberId")Long memberId,@Param("startTime")String startTime,@Param("endTime")String endTime,@Param("oneStart")Long oneStart,@Param("oneEnd")Long oneEnd);

	
	/**
	 * selectByExternalIdForDayNumber:(查询多少天内的订单). <br/>
	 * Date: 2016-1-6 下午6:28:13 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param externalId
	 * @param beforeDay
	 * @return
	 */
	OrderHeader selectByExternalIdForDayNumber(String externalId,String beforeDay);

	/**
	 * air助手
	 * showShopOrderCount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param storeCode
	 * @param date
	 * @return
	 */
	public List<OrderHeaderExp> showShopOrderCount(@Param("storeCode")String storeCode,@Param("date")String date);
	
	/**
	 * 根据会员和金额要求查询本月消费次数
	 * @param amount
	 * @param memberId
	 * @return
	 */
	Long queryCountByAmountAndMemberId(@Param("amount")Long amount,@Param("memberId")Long memberId,@Param("orderDate")String orderDate);
	
	/**
	 * 查询会员一天消费总额
	 * @param orderDate
	 * @return
	 */
	BigDecimal queryEveryDayOrderAmount(@Param("memberId")Long memberId,@Param("orderDate")String orderDate);
	
}