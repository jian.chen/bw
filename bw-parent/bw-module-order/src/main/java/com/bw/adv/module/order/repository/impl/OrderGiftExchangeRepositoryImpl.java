package com.bw.adv.module.order.repository.impl;


import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.order.mapper.OrderGiftExchangeMapper;
import com.bw.adv.module.order.model.OrderGiftExchange;
import com.bw.adv.module.order.repository.OrderGiftExchangeRepository;


/**
 * ClassName: 订单优惠<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:42:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class OrderGiftExchangeRepositoryImpl extends BaseRepositoryImpl<OrderGiftExchange, OrderGiftExchangeMapper> implements OrderGiftExchangeRepository{

	@Override
	protected Class<OrderGiftExchangeMapper> getMapperClass() {
		return OrderGiftExchangeMapper.class;
	}
	
}