
package com.sage.scrm.api.member.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Test;

import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;

public class MemberAccountApiTest {
	
	private static String API_URL = "http://139.196.48.250:8084/scrm-web-main/rs/external/api/";
	
	/**
	 * test:(测试get方法). <br/>
	 * Date: 2015-10-22 下午5:11:05 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	@Test
	public void test() {
		try {
			List<Object> provides = new ArrayList<Object>();
			provides.add(new JacksonJsonProvider());
			WebClient client = WebClient.create(API_URL, provides);
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("extAccountTypeId", "1");
			tmp.put("bindingAccount", "wwwwwwwwwww");
			RestParameter params = new RestParameter(tmp, "b146389c1ac562997c1463a8bff28c83");
			client.accept(MediaType.APPLICATION_JSON);
			client.header("key", params.getSignString());
			client.header("systemId", "102");
			System.out.println(params.getMatrixString());//字符串参数getMatrixString()
			client.path("member/queryMemberAccountByBindingAccount" + params.getMatrixString());
			ApiResult result = client.get(ApiResult.class);
			System.out.println(result.getData());
			System.out.println("end...");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * updateMember:修改会员信息；post方法. <br/>
	 * @author WangJian
	 * @since JDK 1.6
	 */
	@Test
	public void testRegisterMember() {
		List<Object> provides = new ArrayList<Object>();
		provides.add(new JacksonJsonProvider());
		WebClient client = WebClient.create(API_URL, provides);
		MemberDto memberDto = new MemberDto();
		memberDto.setMemberCode("1883418373");
		memberDto.setExtAccountTypeId("1");
		memberDto.setBindingAccount("1asdfsdfsdf");
		memberDto.setMobile("18317117311");
		
		RestParameter params = new RestParameter(memberDto, "b146389c1ac562997c1463a8bff28c83");
		client.accept(MediaType.APPLICATION_JSON);
		client.header("sign", params.getSignString());
		client.header("systemId", "302");
		client.path("member/registerMemberNoWechat").header("content-type", "application/json");
		client.acceptEncoding("UTF-8").encoding("UTF-8");
		System.out.println(params.getJsonString());//获取json参数
		ApiResult result = client.post(params.getJsonString(),ApiResult.class);
		System.out.println(result.getMessage());
	}
}

