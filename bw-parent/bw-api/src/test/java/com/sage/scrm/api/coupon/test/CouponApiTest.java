
package com.sage.scrm.api.coupon.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Test;

import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.api.parameter.RestParameter;
import com.bw.adv.api.result.ApiResult;

public class CouponApiTest {
	
	private static String API_URL = "http://localhost:8081/scrm-bk-web/rs/external/api/";
	
	@Test
	public void test() {
		try {
			List<Object> provides = new ArrayList<Object>();
			provides.add(new JacksonJsonProvider());
			WebClient client = WebClient.create(API_URL, provides);
			Map<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("queryCode", "1805607220");
			RestParameter params = new RestParameter(tmp);
			client.accept(MediaType.APPLICATION_JSON);
			
			client.header("sign", "sss");
			
			client.path("memberCard/queryMemberInfoByQueryCode" + params.getMatrixString());
			
			ApiResult result = client.get(ApiResult.class);
			System.out.println(result.getMessage());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

