package com.bw.adv.api.order.validtion;

import com.bw.adv.module.order.model.OrderCoupon;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.OrderHeaderExt;
import com.bw.adv.module.order.model.OrderItem;
import com.bw.adv.module.order.model.OrderPaymentMethod;
import com.bw.adv.module.order.model.dto.OrderCouponDto;
import com.bw.adv.module.order.model.dto.OrderHeaderDto;
import com.bw.adv.module.order.model.dto.OrderItemDto;
import com.bw.adv.module.order.model.dto.OrderPaymentMethodDto;


public interface OrderValidation {
	
	
	/**
	 * 检验orderHeaderDtos参数
	 * @param orderHeaderDtos
	 * @return
	 */
	OrderHeaderDto validationOrderHeaderDto(OrderHeaderDto orderHeaderDto, Long channelId);
	
	/**
	 * 通过orderHeaderDto转换成系统所需要的orderHeaderExt
	 * @param orderHeaderDto
	 * @return
	 */
	OrderHeaderExt convertOrderHeaderExt(OrderHeaderDto orderHeaderDto);
	
	/**
	 * 通过orderHeaderDto转换成系统所需要的orderHeader
	 * @param orderHeaderDto
	 * @return
	 */
	OrderHeader convertOrderHeaderByOrderHeaderDto(OrderHeaderDto orderHeaderDto);
	
	/**
	 * 根据OrderItemDto转换成对应的orderItem
	 * @param orderItemDto
	 * @param seq
	 * @return
	 */
	OrderItem convertOrderItemByOrderItemDto(OrderItemDto orderItemDto, Long seq);
	
	/**
	 * 根据orderCouponDto转换成对应的orderCoupon
	 * @param orderCouponDto
	 * @return
	 */
	OrderCoupon convertOrderCouponByOrderCouponDto(OrderCouponDto orderCouponDto);
	
	/**
	 * 根据orderPaymentMethodDto转换成对应的orderPaymentMethod
	 * @param orderCouponDto
	 * @return
	 */
	OrderPaymentMethod convertOrderPaymentMethodByOrderPaymentMentodDto(OrderPaymentMethodDto orderPaymentMethodDto);
	
}
