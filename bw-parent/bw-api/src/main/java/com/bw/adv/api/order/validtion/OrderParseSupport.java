package com.bw.adv.api.order.validtion;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.OrderHeaderExt;
import com.bw.adv.module.order.model.OrderItem;
import com.bw.adv.module.order.model.OrderPaymentMethod;
import com.bw.adv.module.order.model.dto.OrderHeaderDto;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.order.model.dto.OrderItemDto;
import com.bw.adv.module.order.model.dto.OrderPaymentMethodDto;
import com.bw.adv.module.tools.BeanUtils;


/**
 * ClassName: 订单解析支持类 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-16 下午6:33:28 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public abstract class OrderParseSupport{
	
	@Value("${employee_card_length}")
	private static Integer EMPLOYEE_CARD_LENGTH ;
	
	/**
	 * parseOrderBefore:(解析之前执行). <br/>
	 * Date: 2016-2-29 下午3:34:43 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param orderHeaderDto
	 * @return
	 */
	public abstract OrderHeaderDto parseOrderBefore(OrderHeaderDto orderHeaderDto);
	
	/**
	 * parseOrderAfter:(解析之后执行). <br/>
	 * Date: 2016-2-29 下午3:34:44 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param orderInfo
	 * @return
	 */
	public abstract OrderInfo parseOrderAfter(OrderInfo orderInfo);
	
	
	/**
	 * parseOrderInfo:(解析订单数据). <br/>
	 * Date: 2015-9-16 下午2:46:10 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	public OrderInfo parseOrderInfo(OrderHeaderDto orderHeaderDto){
		OrderInfo orderInfo = null;
		//订单
		OrderHeader orderHeader = null;
		OrderHeaderExt orderHeaderExt = null;
		//订单明细
		OrderItem orderItem = null;
		List<OrderItem> orderItemList = null;
		List<OrderItemDto> orderItemDtoList = null;
		
		//订单付款方式
		OrderPaymentMethod orderPaymentMethod = null;
		List<OrderPaymentMethodDto> orderPaymentMethodDtoList = null;
		List<OrderPaymentMethod> orderPaymentMethodList = null;
		
		orderInfo = new OrderInfo();
		orderHeader = new OrderHeader();
		orderHeaderExt = new OrderHeaderExt();
		
		if(parseOrderBefore(orderHeaderDto) != null){
			orderHeaderDto = parseOrderBefore(orderHeaderDto);
		}
		
		orderInfo.setOrderHeaderDto(orderHeaderDto);
		
		//解析订单主信息
		BeanUtils.copyNotNull(orderHeaderDto, orderHeader);
		orderHeader.setField1(orderHeaderDto.getCardNo());
		//判断是不是员工卡
		if(StringUtils.isNotBlank(orderHeaderDto.getCardNo()) && EMPLOYEE_CARD_LENGTH != null 
				&& orderHeaderDto.getCardNo().length() == EMPLOYEE_CARD_LENGTH){
			orderHeader.setField2("Y");
		}else{
			orderHeader.setField2("N");
		}
		orderInfo.setOrderHeader(orderHeader);
		
		//保存订单扩展信息
		BeanUtils.copyNotNull(orderHeaderDto, orderHeaderExt);
		orderHeaderExt.setOrderId(orderHeader.getOrderId());
		orderInfo.setOrderHeaderExt(orderHeaderExt);
		
		//保存订单明细
		orderItemDtoList = orderHeaderDto.getOrderItemDtoList();
		if(orderItemDtoList != null){
			orderItemList = new ArrayList<OrderItem>();
			for (OrderItemDto orderItemDto : orderItemDtoList) {
				orderItem = new OrderItem();
				BeanUtils.copyNotNull(orderItemDto, orderItem);
				orderItem.setOrderId(orderHeader.getOrderId());
				orderItemList.add(orderItem);
			}
			orderInfo.setOrderItems(orderItemList);
		}
		
		//保存订单优惠
		orderInfo.setOrderCoupons(orderHeaderDto.getOrderCouponDtoList());
		
		//保存订单付款方式
		orderPaymentMethodDtoList = orderHeaderDto.getOrderPaymentMethodDtoList();
		if(orderPaymentMethodDtoList != null){
			orderPaymentMethodList = new ArrayList<OrderPaymentMethod>();
			for (OrderPaymentMethodDto orderPaymentMethodDto : orderPaymentMethodDtoList) {
				orderPaymentMethod = new OrderPaymentMethod();
				BeanUtils.copyNotNull(orderPaymentMethodDto, orderPaymentMethod);
				orderPaymentMethod.setOrderId(orderHeader.getOrderId());
				orderPaymentMethodList.add(orderPaymentMethod);
			}
			orderInfo.setOrderPaymentMethods(orderPaymentMethodList);
		}
		
		if(parseOrderAfter(orderInfo)!= null){
			orderInfo = parseOrderAfter(orderInfo);
		};
		return orderInfo;
	}
		
}
