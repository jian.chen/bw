package com.bw.adv.aop.service;


import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.module.activity.constant.ActivityConstant;
import com.bw.adv.module.activity.constant.BusinessConstant;
import com.bw.adv.module.activity.model.ActivityAction;
import com.bw.adv.module.activity.model.ActivityActionTask;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.repository.ActivityActionRepository;
import com.bw.adv.module.activity.repository.ActivityActionTaskRepository;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.repository.StoreRepository;
import com.bw.adv.module.component.rule.constant.ConditionsConstant;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.repository.MemberCardRepository;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.order.enums.OrderOptEnum;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;
import com.bw.adv.service.member.service.MemberTagService;

/**
 * ClassName: 活动动作功能拦截器 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-31 上午10:06:10 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class  ActivityActionAdvice implements AfterReturningAdvice {

	@Value("${points_count_flag}")
	private static String POINTS_COUNT_FLAG ;
	private static String NO_COUNT = "N";
	//	private static String[] CHECK_FILEDS = PropertiesConfInit.getString("webConfig", "member_completed_filed").split(";");
	private static Logger logger = Logger.getLogger(ActivityActionAdvice.class);
	@Value("${birthday_length}")
	private static Integer BIRTHDAY_LENGTH ;
	
	@Value("${new_member_tag}")
	private static String NEW_MEMBER_TAG ;
	@Value("${update_member_tag}")
	private static String UPDATE_MEMBER_TAG ;
	@Value("${handle_order_tag}")
	private static String HANDLE_ORDER_TAG;
	
	private static String SEND_TYPE_CURRENT_DATE = "1";
	private static String SEND_TYPE_BEFORE_DATE = "2";
	private static String SEND_TYPE_MONTH_BEGIN = "3";

	private ActivityActionRepository activityActionRepository;
	private OrderActivityActionParams orderActivityActionParams;
	private RegisterActivityActionParams registerActivityActionParams;
	private PerfectActivityActionParams perfectActivityActionParams;

	private MemberRepository memberRepository;
	private MemberCardRepository memberCardRepository;
	private ActivityActionTaskRepository activityActionTaskRepository;
	private StoreRepository storeRepository;
	private WechatMsgQueueHRepository wechatMsgQueueHRepository;

	private MemberTagService memberTagService;

	private AfterRegisterAdvice afterRegisterAdvice;
	

	@Override
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		// 如果是1，那么就是需要送积分的
		if ("N".equals(POINTS_COUNT_FLAG)) {
			return;
		}
		// 如果是注册
		if (method.getName().equals("registMemberNoWechat")) {
			registerActivityAction(args, returnValue);
		}
		// 如果是注册激活
		if (method.getName().equals("registMemberInfo")) {
			registerActivityAction(args, returnValue);
		}
		// 完善个人信息 
		if(method.getName().equals("updateMember") || method.getName().equals("wechatUserEdit")){
			updateActivityAction(args, returnValue);
		}
		// 如果是页面创建用户
		if (method.getName().equals("insertMember")) {
			registerActivityAction(args, returnValue);
		}
		// 同步订单
		else if (method.getName().equals("handleSyncOrder")) {
			handleSyncOrderActivityAction(args, returnValue);
		}

	}
	
	private boolean employeeDecide(String isEmployee){
		if(StringUtils.isNotBlank(isEmployee) && isEmployee.equals("Y")){
			return false;
		}else{
			return true;
		}
	}


	/**
	 * updateActivityAction:(完善会员信息). <br/>
	 * Date: 2015年11月11日 下午6:16:08 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param args
	 * @param returnValue
	 */
	private void updateActivityAction(Object[] args, Object returnValue){
		MemberDto memberDto = null;
		Member member = null;
		JSONObject json = null;
		ActivityAction activityAction = null;
		String isPerfectCheck =null;

		if(args[0] instanceof Member){
			member = (Member) args[0];
			memberDto = new MemberDto();
			BeanUtils.copy(member, memberDto);
		}else if(args[0] instanceof MemberDto){
			memberDto = (MemberDto) args[0];
		}else{
			return;
		}
		
		boolean flag = this.employeeDecide(memberDto.getIsEmployee());
		if(!flag){
			return;
		}

		String currentDate = DateUtils.getCurrentTimeOfDb();
		isPerfectCheck =  memberRepository.findByPk(memberDto.getMemberId()).getPerfectCheck();
		if(!StringUtils.isEmpty(isPerfectCheck)){
			// 会员信息已经完善
			if(isPerfectCheck.equals("Y")){
				return;
			}
		}
		// 如果符合完善会员信息
		activityAction = new ActivityAction();
		json = perfectActivityActionParams.getPerfectParams(memberDto);
		activityAction.setMemberId(memberDto.getMemberId());
		activityAction.setActivityCode(ActivityConstant.ACTIVIY_PERFECT_MEMBER.getCode());
		activityAction.setRuleOptParams(json.toString());
		activityAction.setHappenTime(memberDto.getRegisterTime());
		activityAction.setCreateTime(currentDate);
		activityAction.setIsCount(NO_COUNT);
		activityActionRepository.save(activityAction);

		if(StringUtils.isNotBlank(UPDATE_MEMBER_TAG) && UPDATE_MEMBER_TAG.equals("Y")){
			this.memberTagService.structureTagAopUpdateMember(memberDto);
		}
		//保存会员生日活动
		saveBirthdayActivity(memberDto);
	}

	/**
	 * registerPointsAction:(注册动作). <br/>
	 * Date: 2015-9-7 下午8:51:08 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param args
	 * @param returnValue
	 */
	private void registerActivityAction(Object[] args, Object returnValue) {
		MemberDto memberDto = null;
		Member member = null;
		JSONObject json = null;
		ActivityAction activityAction = null;
		String happenTime = null;
		String createTime = null;

		if(args[0] instanceof Member){
			member = (Member) args[0];
			memberDto = new MemberDto();
			BeanUtils.copy(member, memberDto);
		}else if(args[0] instanceof MemberDto){
			memberDto = (MemberDto) args[0];
		}else{
			return;
		}
		
		boolean flag = this.employeeDecide(memberDto.getIsEmployee());
		if(!flag){
			return;
		}
		
		happenTime = memberDto.getRegisterTime();
		createTime = DateUtils.getCurrentTimeOfDb();
		if(StringUtils.isBlank(happenTime)){
			happenTime = createTime; 
		}
		
		activityAction = new ActivityAction();
		json = registerActivityActionParams.getRegisterParams(memberDto);
		activityAction.setMemberId(memberDto.getMemberId());
		activityAction.setActivityCode(ActivityConstant.ACTIVIY_NEW_MEMBER.getCode());
		activityAction.setRuleOptParams(json.toString());
		activityAction.setHappenTime(happenTime);
		activityAction.setCreateTime(createTime);
		activityAction.setIsCount(NO_COUNT);
		activityActionRepository.save(activityAction);
		//保存会员生日活动
		saveBirthdayActivity(memberDto);
		// 邀请新会员活动
		activiyInviteNewMember(memberDto);
		
		if(StringUtils.isNotBlank(NEW_MEMBER_TAG) && NEW_MEMBER_TAG.equals("Y")){
			this.memberTagService.structureTagAopNewMember(memberDto);
		}
		
		if(afterRegisterAdvice != null){
			afterRegisterAdvice.afterReturning(memberDto);
		}

	}
	
	/**
	 * activiyInviteNewMember:(邀请新会员活动). <br/>
	 * Date: 2016-5-23 上午9:37:37 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param memberDto
	 */
	private void activiyInviteNewMember(MemberDto memberDto) {
		ActivityAction activityAction = null;
		JSONObject json = null;
		String happenTime = null;
		String createTime = null;

		if (memberDto.getMemberSrcId() != null) {
			activityAction = new ActivityAction();
			createTime = DateUtils.getCurrentTimeOfDb();
			happenTime = memberDto.getRegisterTime();
			json = registerActivityActionParams.getRegisterParams(memberDto);
			activityAction.setMemberId(memberDto.getMemberSrcId());
			activityAction.setActivityCode(ActivityConstant.ACTIVIY_INVITE_NEW_MEMBER.getCode());
			activityAction.setRuleOptParams(json.toString());
			activityAction.setHappenTime(happenTime);
			activityAction.setCreateTime(createTime);
			activityAction.setIsCount(NO_COUNT);
			activityActionRepository.save(activityAction);
			
			activityAction = new ActivityAction();
			createTime = DateUtils.getCurrentTimeOfDb();
			happenTime = memberDto.getRegisterTime();
			json = registerActivityActionParams.getRegisterParams(memberDto);
			activityAction.setMemberId(memberDto.getMemberId());
			activityAction.setActivityCode(ActivityConstant.ACTIVIY_INVITE_NEW_MEMBER.getCode());
			activityAction.setRuleOptParams(json.toString());
			activityAction.setHappenTime(happenTime);
			activityAction.setCreateTime(createTime);
			activityAction.setIsCount(NO_COUNT);
			activityActionRepository.save(activityAction);
		}

	}


	/**
	 * saveBirthdayActivity:(保存会员生日活动). <br/>
	 * Date: 2016-1-6 下午6:09:03 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberDto
	 */
	private void saveBirthdayActivity(MemberDto memberDto) {
		String currentDate = DateUtils.getCurrentTimeOfDb();
		List<ActivityInstance> list = RuleInit.getActivityInstanceList(ActivityConstant.ACTIVIY_MEMBER_BIRTHDAY.getCode());
			for (ActivityInstance instance : list) {
				if(StringUtils.isNotBlank(instance.getThruDate())){
					if(instance.getThruDate().compareTo(currentDate) <= 0){
						continue;
					}
				}
				JSONObject json = RuleInit.getInstanceConditionJson(instance.getActivityInstanceId());
				String sendDate = json.get(ConditionsConstant.SEND_DATE.getCode()).toString();
				String sendType = null;
				
				sendType = sendDate.split("/")[0];
				if(sendType.equals(SEND_TYPE_CURRENT_DATE)){
					sendDate = DateUtils.getCurrentDateOfDb().substring(4);
				}
				if(sendType.equals(SEND_TYPE_BEFORE_DATE)){
					sendDate = DateUtils.addDays(Integer.parseInt(sendDate.split("/")[1])).substring(4);
				}
				if(sendType.equals(SEND_TYPE_MONTH_BEGIN)){
					sendDate = DateUtils.formatDate(new Date(),"MM");
				}
				if( StringUtils.isNotBlank(memberDto.getBirthday()) && memberDto.getBirthday().substring(4).compareTo(sendDate) <= 0 
						&& memberDto.getBirthday().substring(4).compareTo(DateUtils.getCurrentDateOfDb().substring(4, 6)+"01") >= 0){
					ActivityActionTask activityActionBirthday = new ActivityActionTask();
					JSONObject jsonParams = new JSONObject();
					activityActionBirthday.setActivityCode(ActivityConstant.ACTIVIY_MEMBER_BIRTHDAY.getCode());
					activityActionBirthday.setMemberId(memberDto.getMemberId());
					activityActionBirthday.setCreateTime(currentDate);
					activityActionBirthday.setHappenTime(currentDate);
					activityActionBirthday.setIsCount("N");
					jsonParams.put("memberCode", memberDto.getMemberCode());
					jsonParams.put("grade", memberDto.getGradeId());
					jsonParams.put("birthday", memberDto.getBirthday());
					jsonParams.put("MEMBER_GROUP", memberDto.getMemberGroupIds());
					jsonParams.put("MEMBER_TAG", memberDto.getMemberTagIds());
					jsonParams.put("ACTIVITY_INSTANCE_ID", instance.getActivityInstanceId());
					activityActionBirthday.setRuleOptParams(jsonParams.toString());
					activityActionTaskRepository.save(activityActionBirthday);
				}
			}
	}

	/**
	 * handleSyncOrderActivityAction:(同步订单动作). <br/>
	 * Date: 2015-9-7 下午8:51:21 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param args
	 * @param returnValue
	 */
	private void handleSyncOrderActivityAction(Object[] args, Object returnValue) {
		ActivityAction activityAction = null;
		ActivityAction activityActionOftenOrder = null;
		JSONObject json = null;
		OrderHeader orderHeader = null;
		OrderInfo orderInfo = null;
		MemberExp memberExp = null;
		String currentTime = null;
		try {
			orderInfo = (OrderInfo) args[0];
			activityAction = new ActivityAction();
			activityActionOftenOrder = new ActivityAction();
			orderHeader = orderInfo.getOrderHeader();
			currentTime = DateUtils.getCurrentTimeOfDb();
			memberExp = memberRepository.findMemberExpByMemberId(orderHeader.getMemberId());
			
			boolean flag = this.employeeDecide(memberExp.getIsEmployee());
			if(!flag){
				return;
			}
			
			json = orderActivityActionParams.getOrderParams(orderInfo,memberExp);
			List<JSONObject> oftenJson = orderActivityActionParams.getOftenParams(orderInfo,memberExp); //常客优惠参数
			
			for (JSONObject jsonObject : oftenJson) {
				activityActionOftenOrder.setMemberId(orderHeader.getMemberId());
				activityActionOftenOrder.setRuleOptParams(jsonObject.toString());
				activityActionOftenOrder.setActivityCode(ActivityConstant.ACTIVIY_ORDER_OFTEN.getCode());
				activityActionOftenOrder.setHappenTime(orderHeader.getCreateTime());//订单的原是创建时间
				activityActionOftenOrder.setCreateTime(currentTime);
				activityActionOftenOrder.setIsCount(NO_COUNT);
				activityActionOftenOrder.setExternalId(orderHeader.getExternalId());
				activityActionOftenOrder.setCountAmount(orderHeader.getPayAmount());
				activityActionRepository.save(activityActionOftenOrder);
			}
			

			//取消单不执行活动
			if(orderInfo.getOrderHeaderDto().getOptType().equals(OrderOptEnum.ORDER_OPERATE_WAY_DELETE.getKey())){
				activityAction.setMemberId(orderHeader.getMemberId());
				activityAction.setBusinessCode(BusinessConstant.BUSINESS_ORDER.getCode());
				activityAction.setHappenTime(currentTime);//订单的取消时间
				activityAction.setCreateTime(currentTime);//订单的取消时间
				activityAction.setIsCount(NO_COUNT);
				activityAction.setExternalId(orderHeader.getExternalId());
				activityAction.setCountAmount(orderHeader.getPayAmount());
				activityAction.setRuleOptParams(json.toString());
				activityActionRepository.save(activityAction);
				return;
			}

			activityAction.setMemberId(orderHeader.getMemberId());
			activityAction.setRuleOptParams(json.toString());
			activityAction.setBusinessCode(BusinessConstant.BUSINESS_ORDER.getCode());
			activityAction.setHappenTime(orderHeader.getCreateTime());//订单的原是创建时间
			activityAction.setCreateTime(currentTime);
			activityAction.setIsCount(NO_COUNT);
			activityAction.setExternalId(orderHeader.getExternalId());
			activityAction.setCountAmount(orderHeader.getPayAmount());
			activityActionRepository.save(activityAction);
			
			//首笔消费记录
			ActivityActionTask activityActionTask = new ActivityActionTask();
			activityActionTask.setMemberId(orderHeader.getMemberId());
			activityActionTask.setRuleOptParams(json.toString());
			activityActionTask.setBusinessCode(BusinessConstant.BUSINESS_ORDER.getCode());
			activityActionTask.setActivityCode(ActivityConstant.ACTIVIY_FIRST_SALE.getCode());
			activityActionTask.setHappenTime(orderHeader.getCreateTime());//订单的原是创建时间
			activityActionTask.setCreateTime(currentTime);
			activityActionTask.setIsCount(NO_COUNT);
			activityActionTask.setExternalId(orderHeader.getExternalId());
			activityActionTask.setCountAmount(orderHeader.getPayAmount());
			activityActionTaskRepository.save(activityActionTask);
			
			//发送订单消费微信消息
			sendWecatMsg(args, returnValue);

			if(StringUtils.isNotBlank(HANDLE_ORDER_TAG) && HANDLE_ORDER_TAG.equals("Y")){
				this.memberTagService.structureTagAopHandleOrder(memberExp,orderInfo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * sendWecatMsg:(发送微信消息). <br/>
	 * Date: 2015-12-16 下午7:46:47 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param args
	 * @param returnValue
	 */
	private void sendWecatMsg(Object[] args, Object returnValue) {
		OrderInfo orderInfo = null;
		OrderHeader orderHeader = null;
		JSONObject json = null;
		Store store = null;
		WechatMsgQueueH wechatMsgQueueH = null;
		try {
			orderInfo = (OrderInfo) args[0];
			orderHeader = orderInfo.getOrderHeader();
			json = new JSONObject();
			wechatMsgQueueH = new WechatMsgQueueH();

			store = storeRepository.findByStoreCode(orderHeader.getStoreCode());

			json.put("orderId", orderHeader.getOrderId());
			json.put("orderDate", DateUtils.dateStrToNewFormat(orderHeader.getCreateTime(), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss, DateUtils.DATE_PATTERN_YYYYMMDDHHmm_3));
			if(store != null){
				json.put("storeName", store.getStoreName());
			}else{
				json.put("storeName", "");
			}
			json.put("orderAmount", orderHeader.getOrderAmount());
			json.put("payAmount", orderHeader.getPayAmount());
			json.put("storeUrl", store.getStoreDesc());

			wechatMsgQueueH.setBusinessType(WechatMsgBusinessTypeEnum.ORDER_SALE.getId());
			wechatMsgQueueH.setCreateTime(DateUtils.getCurrentTimeOfDb());
			wechatMsgQueueH.setIsSend("N");
			wechatMsgQueueH.setMsgKey(orderHeader.getOrderCode());
			wechatMsgQueueH.setMemberId(orderHeader.getMemberId());
			wechatMsgQueueH.setMsgContent(json.toString());
			wechatMsgQueueHRepository.save(wechatMsgQueueH);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Autowired
	public void setActivityActionRepository(ActivityActionRepository activityActionRepository) {
		this.activityActionRepository = activityActionRepository;
	}

	@Autowired
	public void setOrderActivityActionParams(OrderActivityActionParams orderActivityActionParams) {
		this.orderActivityActionParams = orderActivityActionParams;
	}

	@Autowired
	public void setRegisterActivityActionParams(RegisterActivityActionParams registerActivityActionParams) {
		this.registerActivityActionParams = registerActivityActionParams;
	}

	@Autowired
	public void setActivityActionTaskRepository(ActivityActionTaskRepository activityActionTaskRepository) {
		this.activityActionTaskRepository = activityActionTaskRepository;
	}

	@Autowired
	public void setPerfectActivityActionParams(PerfectActivityActionParams perfectActivityActionParams) {
		this.perfectActivityActionParams = perfectActivityActionParams;
	}

	@Autowired
	public void setStoreRepository(StoreRepository storeRepository) {
		this.storeRepository = storeRepository;
	}

	@Autowired
	public void setWechatMsgQueueHRepository(WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}

	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}

	@Autowired
	public void setMemberCardRepository(MemberCardRepository memberCardRepository) {
		this.memberCardRepository = memberCardRepository;
	}

	@Autowired
	public void setMemberTagService(MemberTagService memberTagService) {
		this.memberTagService = memberTagService;
	}

	@Autowired
	public void setAfterRegisterAdvice(AfterRegisterAdvice afterRegisterAdvice) {
		this.afterRegisterAdvice = afterRegisterAdvice;
	}
}
