package com.bw.adv.aop.service.impl;


import com.bw.adv.aop.service.AfterRegisterAdvice;
import com.bw.adv.module.member.model.Member;


/**
 * ClassName: 注册之后执行 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016-3-16 下午5:51:04 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class AfterRegisterAdviceDefault implements AfterRegisterAdvice{
	
	public void afterReturning(Member member){
		
	}
	
}
