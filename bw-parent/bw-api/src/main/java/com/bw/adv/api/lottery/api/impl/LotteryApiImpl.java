package com.bw.adv.api.lottery.api.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.lottery.api.LotteryApi;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.MemberWinningAccord;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.activity.model.SceneGameInstance;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;
import com.bw.adv.module.activity.model.exp.WinningItemExp;
import com.bw.adv.module.activity.repository.WinningItemRepository;
import com.bw.adv.module.base.exception.ActivityException;
import com.bw.adv.module.base.model.QrCode;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.AwardsSettingService;
import com.bw.adv.service.activity.service.LotteryService;
import com.bw.adv.service.activity.service.MemberWinningAccordService;
import com.bw.adv.service.activity.service.PrizeService;
import com.bw.adv.service.activity.service.SceneGameInstanceService;
import com.bw.adv.service.activity.service.WinningItemService;
import com.bw.adv.service.base.service.QrCodeService;


/**
 * ClassName: LotteryApiImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2017年6月25日 下午8:24:01 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public class LotteryApiImpl implements LotteryApi {
	
private LotteryService lotteryService;
	
	private AwardsSettingService awardsSettingService;
	
	private SceneGameInstanceService sceneGameInstanceService;
	
	private MemberWinningAccordService memberWinningAccordService;
	
	private QrCodeService qrCodeService;
	
	private WinningItemService winningItemService;
	
	private WinningItemRepository winningItemRepository;
	
	private PrizeService prizeService;

	private RuleInit ruleInit;
	
	@Override
	public ApiResult lotteryActivityMemberAwards(String jsonParam){
		List<AwardsSettingExp> awardsSettingList =null;
		AwardsSettingExp awardsSettingExp =null;
		ApiResult result = null;
		Map<String,Object> map =null;
		Prize prize =null;
		try {
			result = new ApiResult();
			map =new HashMap<String,Object>();
			JSONObject jo = JSONObject.parseObject(jsonParam);
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
			String qrCodeCode = jo.containsKey("qrCode") ? jo.getString("qrCode") : null;
			
			//判断二维码是否存在
			QrCode qrCode=qrCodeService.queryQrCodeByQrCode(qrCodeCode);
			if(qrCode==null){
				return ApiUtil.getResult(result, ResultStatus.QRCODE_NOT_EXISTS);
			}
			//判断是否有效期
			SceneGameInstance sceneGameInstance = sceneGameInstanceService.queryValidInstanceByCode(qrCode.getActivityCode());//根据活动编码判断是否存在活动
			if(sceneGameInstance==null){
				return ApiUtil.getResult(result, ResultStatus.NO_ACTIVITY_END);
			}
			awardsSettingList = awardsSettingService.queryAwardsSettingExpByActivityId(sceneGameInstance.getSceneGameInstanceId());
			awardsSettingExp = lotteryService.extractMemberAwardsEveryMonth(memberId, sceneGameInstance.getSceneGameInstanceId(),qrCode.getQrCodeId());
			prize = prizeService.queryPrizeByAwardId(awardsSettingExp.getAwardsId());
			map.put("awardsCode", awardsSettingExp.getAwardsCode());
			map.put("prizeTypeId", prize.getPrizeType());
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(awardsSettingList, ApiUtil.getExtSystemId()));
			result.setData(map);
		} catch(ActivityException e){
			if(ActivityException.NO_QUALIFICATION.getErrorId().equals(e.getErrorData().getErrorId())){
				result = ApiUtil.getResult(result,ResultStatus.NO_QUALIFICATION);
			}else if(ActivityException.ACTIVITY_END.getErrorId().equals(e.getErrorData().getErrorId())){
				result = ApiUtil.getResult(result,ResultStatus.ACTIVITY_END);
			}else{
				result= ApiUtil.getResult(result,e.getResultStatus());
			}
		}catch (NumberFormatException e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	public ApiResult initMapsMemberAwards(String jsonParam){
		ApiResult result = null;
		Map<String,Object> map =null;
		try {
			result = new ApiResult();
			map =new HashMap<String,Object>();
			JSONObject jo = JSONObject.parseObject(jsonParam);
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
			String qrCode = jo.containsKey("qrCode") ? jo.getString("qrCode") : null;
			String activityCode = jo.containsKey("activityCode") ? jo.getString("activityCode") : null;
			
			//判断是否有效期
			SceneGameInstance sceneGameInstance = sceneGameInstanceService.queryValidInstanceByCode(activityCode);//根据活动编码判断是否存在活动
			if(sceneGameInstance==null){
				return ApiUtil.getResult(result, ResultStatus.NO_ACTIVITY_END);
			}
			//如果已经存在qrCode
			MemberWinningAccord memberWinningAccord= memberWinningAccordService.queryMemberMapsQrCode(null,sceneGameInstance.getSceneGameInstanceId(), qrCode,null);
			//增加一次抽奖机会
			if(memberWinningAccord==null){
				lotteryService.addMemberWinningAccord(memberId,sceneGameInstance.getSceneGameInstanceId(),qrCode);
			}
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setData(map);
		}catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public ApiResult extractMemberAwards(String jsonParam) {
		List<AwardsSettingExp> awardsSettingList =null;
		AwardsSettingExp awardsSettingExp =null;
		ApiResult result = null;
		Map<String,Object> map =null;
		Prize prize =null;
		try {
			result = new ApiResult();
			map =new HashMap<String,Object>();
			JSONObject jo = JSONObject.parseObject(jsonParam);
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
			String qrCodeCode = jo.containsKey("qrCode") ? jo.getString("qrCode") : null;
			
			//判断二维码是否存在
			QrCode qrCode=qrCodeService.queryQrCodeByQrCode(qrCodeCode);
			if(qrCode==null){
				return ApiUtil.getResult(result, ResultStatus.QRCODE_NOT_EXISTS);
			}
			//判断是否有效期
			SceneGameInstance sceneGameInstance = sceneGameInstanceService.queryValidInstanceByCode(qrCode.getActivityCode());//根据活动编码判断是否存在活动
			if(sceneGameInstance==null){
				return ApiUtil.getResult(result, ResultStatus.NO_ACTIVITY_END);
			}
			awardsSettingList = awardsSettingService.queryAwardsSettingExpByActivityId(sceneGameInstance.getSceneGameInstanceId());
			awardsSettingExp = lotteryService.extractMemberAwardsEveryMonth(memberId, sceneGameInstance.getSceneGameInstanceId(),qrCode.getQrCodeId());
			prize = prizeService.queryPrizeByAwardId(awardsSettingExp.getAwardsId());
			map.put("awardsCode", awardsSettingExp.getAwardsCode());
			map.put("prizeTypeId", prize.getPrizeType());
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(awardsSettingList, ApiUtil.getExtSystemId()));
			result.setData(map);
		} catch(ActivityException e){
			if(ActivityException.NO_QUALIFICATION.getErrorId().equals(e.getErrorData().getErrorId())){
				result = ApiUtil.getResult(result,ResultStatus.NO_QUALIFICATION);
			}else if(ActivityException.ACTIVITY_END.getErrorId().equals(e.getErrorData().getErrorId())){
				result = ApiUtil.getResult(result,ResultStatus.ACTIVITY_END);
			}else{
				result= ApiUtil.getResult(result,e.getResultStatus());
			}
		}catch (NumberFormatException e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult extractMemberAwardsMaps(String jsonParam) {
		List<AwardsSettingExp> awardsSettingList =null;
		AwardsSettingExp awardsSettingExp =null;
		ApiResult result = null;
		Map<String,Object> map =null;
		Prize prize =null;
		try {
			result = new ApiResult();
			map =new HashMap<String,Object>();
			JSONObject jo = JSONObject.parseObject(jsonParam);
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
			String activityCode = jo.containsKey("activityCode") ? jo.getString("activityCode") : null;
			String qrCode = jo.containsKey("qrCode") ? jo.getString("qrCode") : null;
			
			//判断是否有效期
			SceneGameInstance sceneGameInstance = sceneGameInstanceService.queryValidInstanceByCode(activityCode);//根据活动编码判断是否存在活动
			if(sceneGameInstance==null){
				return ApiUtil.getResult(result, ResultStatus.NO_ACTIVITY_END);
			}
			awardsSettingList = awardsSettingService.queryAwardsSettingExpByActivityId(sceneGameInstance.getSceneGameInstanceId());
			awardsSettingExp = lotteryService.extractMemberAwardsMaps(memberId, sceneGameInstance.getSceneGameInstanceId(),qrCode);
			prize = prizeService.queryPrizeByAwardId(awardsSettingExp.getAwardsId());
			map.put("awardsCode", awardsSettingExp.getAwardsCode());
			map.put("prizeTypeId", prize.getPrizeType());
			map.put("couponInstanceCode", awardsSettingExp.getCouponCodeInstance());
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(awardsSettingList, ApiUtil.getExtSystemId()));
			result.setData(map);
		} catch(ActivityException e){
			if(ActivityException.NO_QUALIFICATION.getErrorId().equals(e.getErrorData().getErrorId())){
				result = ApiUtil.getResult(result,ResultStatus.NO_QUALIFICATION);
			}else if(ActivityException.ACTIVITY_END.getErrorId().equals(e.getErrorData().getErrorId())){
				result = ApiUtil.getResult(result,ResultStatus.ACTIVITY_END);
			}else{
				result= ApiUtil.getResult(result,e.getResultStatus());
			}
		}catch (NumberFormatException e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult initLotteryMemberAwards(String jsonParam) {
		ApiResult result = null;
		Map<String,Object> map = null;
		List<AwardsSettingExp> awardsSettingList =null;
		try {
			result = new ApiResult();
			map = new HashMap<String, Object>();
			JSONObject jo = JSONObject.parseObject(jsonParam);
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
			String qrCodeCode = jo.containsKey("qrCodeCode") ? jo.getString("qrCodeCode") : null;
			//判断二维码是否存在
			QrCode qrCode=qrCodeService.queryQrCodeByQrCode(qrCodeCode);
			if(qrCode==null){
				return ApiUtil.getResult(result, ResultStatus.QRCODE_NOT_EXISTS);
			}
			//判断二维码是否存在
			//判断是否有效期
			SceneGameInstance sceneGameInstance = sceneGameInstanceService.queryValidInstanceByCode(qrCode.getActivityCode());//根据活动编码判断是否存在活动
			if(sceneGameInstance==null){
				return ApiUtil.getResult(result, ResultStatus.NO_ACTIVITY_END);
			}
			awardsSettingList = awardsSettingService.queryAwardsSettingExpByActivityId(sceneGameInstance.getSceneGameInstanceId());
			Long chance = lotteryService.queryMemberLotteryChance(memberId, sceneGameInstance.getSceneGameId(),qrCode.getQrCodeId());
			Long useChance = memberWinningAccordService.queryMemberUseChance(memberId,sceneGameInstance.getSceneGameId(),qrCode.getQrCodeId(), DateUtils.getCurrentDateOfDb().substring(0, 6));
			map.put("chance", chance);
			map.put("useChance", useChance);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(awardsSettingList, ApiUtil.getExtSystemId()));
			result.setData(map);
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	
	@Override
	public ApiResult queryMemberSurplusNumber(String memberId, String activityId,Long qrCodeId) {
		ApiResult result = null;
		Integer count =null;
		Map<String,Object> map =null;
		try {
			map =new HashMap<String,Object>();
			result =new ApiResult();
			count = memberWinningAccordService.queryMemberSurplusNumber(Long.valueOf(memberId), Long.valueOf(activityId),qrCodeId);
			map.put("count", count);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setData(map);
		} catch (NumberFormatException e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	@Override
	public ApiResult queryWinningItemsByMemberIdAndActivityId(String memberId,String activityId, int page, int rows) {
		List<WinningItem> winningItems =null;
		ApiResult result = null;
		Example example = null;
		Page<WinningItem> pageobj = null;
		try {
			pageobj = new Page<WinningItem>();
			pageobj.setPageNo(page);
			pageobj.setPageSize(rows);
			result = new ApiResult();
			example = new Example();
			Criteria criteria =  example.createCriteria();
			criteria.andEqualTo("wi.MEMBER_ID", memberId);
			criteria.andEqualTo("wi.ACTIVITY_ID", activityId);
			winningItems = winningItemRepository.findWinningItemPersonsByActivityIdAndAwardId(example, pageobj);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(winningItems, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public ApiResult queryLastWinningItem(Long num) {
		ApiResult result = null;
		List<WinningItemExp> winningItems =null;
		try {
			result = new ApiResult();
			winningItems = winningItemService.findLastNum(num);
			//result.setData(ApiObjUtil.getApiResultMap(winningItems, ApiUtil.getExtSystemId()));
			result.setItems(ApiObjUtil.getApiResultItems(winningItems, ApiUtil.getExtSystemId()));
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}


	@Override
	public ApiResult reloadGlobalInfo() {
		ApiResult result = new ApiResult();
		ruleInit.reLoad();
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("data", "ok");
		result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
		result.setData(data);

		return result;
	}
	
	@Autowired
	public void setMemberWinningAccordService(MemberWinningAccordService memberWinningAccordService) {
		this.memberWinningAccordService = memberWinningAccordService;
	}
	
	@Autowired
	public void setLotteryService(LotteryService lotteryService) {
		this.lotteryService = lotteryService;
	}

	@Autowired
	public void setAwardsSettingService(AwardsSettingService awardsSettingService) {
		this.awardsSettingService = awardsSettingService;
	}

	@Autowired
	public void setWinningItemService(WinningItemService winningItemService) {
		this.winningItemService = winningItemService;
	}

	@Autowired
	public void setWinningItemRepository(WinningItemRepository winningItemRepository) {
		this.winningItemRepository = winningItemRepository;
	}

	@Autowired
	public void setQrCodeService(QrCodeService qrCodeService) {
		this.qrCodeService = qrCodeService;
	}

	@Autowired
	public void setPrizeService(PrizeService prizeService) {
		this.prizeService = prizeService;
	}

	@Autowired
	public void setRuleInit(RuleInit ruleInit) {
		this.ruleInit = ruleInit;
	}

	@Autowired
	public void setSceneGameInstanceService(SceneGameInstanceService sceneGameInstanceService) {
		this.sceneGameInstanceService = sceneGameInstanceService;
	}
	
}
