package com.bw.adv.aop.service.impl;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Component;

import com.bw.adv.aop.service.PerfectActivityActionParams;
import com.bw.adv.module.member.model.Member;

@Component
public class PerfectActivityActionParamsImpl implements PerfectActivityActionParams{

	@Override
	public JSONObject getPerfectParams(Member member) {
		return JSONObject.fromObject(member);
	}

	
}
