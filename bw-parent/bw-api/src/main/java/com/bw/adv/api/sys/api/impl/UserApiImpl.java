package com.bw.adv.api.sys.api.impl;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.sys.api.UserApi;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.core.utils.MD5Utils;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.sys.enums.SysSystemEnum;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.SysFunctionOperation;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.service.base.service.StoreService;
import com.bw.adv.service.sys.service.SysFunctionOperationService;
import com.bw.adv.service.sys.service.SysFunctionService;
import com.bw.adv.service.sys.service.SysUserService;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jeoy.zhou on 6/12/16.
 */
public class UserApiImpl implements UserApi {

    private SysFunctionService sysFunctionService;
    private SysFunctionOperationService sysFunctionOperationService;
    private SysUserService sysUserService;
    private StoreService storeService;

    @Override
    public ApiResult loginCheck(String json) {
        ApiResult result = null;
        Map<String, Object> map = new HashMap<String, Object>();
        SysUser sysUser = null;
        List<SysFunction> sysFunctionList = null;
        List<SysFunctionOperation> roleFunctionOperations = null;
        Store store = null;
        try {
            result = new ApiResult();
            JSONObject jo = JSONObject.parseObject(json);
            String username = jo.containsKey("username") ? jo.getString("username") : null;
            String password = jo.containsKey("password") ? jo.getString("password") : null;
            sysUser = sysUserService.querySysUserByUserName(username);
            if (sysUser == null) {
                result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
                result.setMessage("用户名错误!");
                return result;
            }
            if(!sysUser.getPassword().equals(MD5Utils.getMD5String(password)) ){
                result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
                result.setMessage("密码错误!");
                return result;
            }
            if (!StatusConstant.SYS_USER_ACTIVE.getId().equals(sysUser.getStatusId())
                    || !SysSystemEnum.AIR_ASSISANT.getSystemId().equals(sysUser.getSystemId())) {
                result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
                result.setMessage("用户状态错误!请联系系统管理员");
                return result;
            }
            sysFunctionList = sysFunctionService.findListForSysFunctionByUserId(sysUser.getUserId(), sysUser.getSystemId());//功能权限
            roleFunctionOperations = sysFunctionOperationService.queryButtonFunctionOperationsByUserId(sysUser.getUserId());
            store = storeService.queryByOrgId(sysUser.getOrgId());
            sysUser.setStoreCode(store.getStoreCode() == null ? "":store.getStoreCode());
            sysUser.setStoreName(store.getStoreName() == null ? "无门店":store.getStoreName());
            map.put("sysFunctionList", sysFunctionList);
            map.put("roleFunctionOperations", roleFunctionOperations);
            sysUser.setPassword(null);
            map.put("sysUser", sysUser);
            result.setData(map);
            result.setCode("200");
        } catch (Exception e) {
            result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }

    @Autowired
    public void setSysFunctionService(SysFunctionService sysFunctionService) {
        this.sysFunctionService = sysFunctionService;
    }

    @Autowired
    public void setSysUserService(SysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    @Autowired
    public void setSysFunctionOperationService(SysFunctionOperationService sysFunctionOperationService) {
        this.sysFunctionOperationService = sysFunctionOperationService;
    }

    @Autowired
    public void setStoreService(StoreService storeService) {
        this.storeService = storeService;
    }
}
