package com.bw.adv.api.gift.api.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.gift.api.GiftExchangeApi;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.module.badge.model.exp.MemberBadgeExt;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberGift;
import com.bw.adv.module.member.model.exp.MemberGiftExt;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.service.badge.service.MemberBadgeItemSevice;
import com.bw.adv.service.coupon.service.CouponIssueService;
import com.bw.adv.service.coupon.service.CouponService;
import com.bw.adv.service.order.service.MemberGiftService;


/**
 * ClassName: GradeApiImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月14日 下午8:24:01 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
public class GiftExchangeApiImpl implements GiftExchangeApi {
	
	@Value("${memberBadgeCouponCode}")
	private String memberBadgeCouponCode;
	@Autowired
	private MemberBadgeItemSevice memberBadgeItemSevice;
	@Autowired
	private MemberRepository memberRepository;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private CouponIssueService couponIssueService;
	
	@Autowired
	private MemberGiftService memberGiftService;
	
	@Override
	public ApiResult memberBadgegiftExchange(String jsonParam){
		JSONObject jo = JSONObject.parseObject(jsonParam);
		Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
		ApiResult result = null;
		try{
			result=new ApiResult();
			if(memberId==null){
				return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
			Member member = memberRepository.findByPk(memberId);
			if(member==null){
				return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
			boolean isQualifications = memberBadgeItemSevice.validMemberBadgeQualifications(memberId);
			if(isQualifications){
				Coupon coupon = couponService.findCouponByCode(memberBadgeCouponCode);
				if(coupon==null){
					return ApiUtil.getResult(result, ResultStatus.COUPON_INSTANCE_STATUS_NOT_RECEIVE_ERROR);
				}
				CouponInstance couponInstance = couponIssueService.getMemberBadgeGiftCoupon(memberId, coupon.getCouponId());
				if(couponInstance==null){
					return ApiUtil.getResult(result, ResultStatus.COUPON_INSTANCE_INVENTORY_EMPTY_ERROR);
				}else{
					memberGiftService.saveOrUpdateMemberGiftRecord(couponInstance, memberId, coupon);
					result.setData(ApiObjUtil.getApiResultMap(couponInstance, ApiUtil.getExtSystemId()));
					return ApiUtil.getResult(result, ResultStatus.SUCCESS);
				}
			}else{
				return ApiUtil.getResult(result, ResultStatus.MEMBER_BADGE_ISNOT_QUALIFY);
			}
		}catch (NumberFormatException e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
			return result;
		}
	}
	@Override
	public ApiResult getMemberGiftMemberBadge(String jsonParam){
		JSONObject jo = JSONObject.parseObject(jsonParam);
		Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
		ApiResult result = null;
		try{
			result=new ApiResult();
			if(memberId==null){
				return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
			Member member = memberRepository.findByPk(memberId);
			if(member==null){
				return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
			List<MemberBadgeExt> extMember = memberBadgeItemSevice.queryMemberBadgeExtByMemberId(memberId);
			result.setItems(ApiObjUtil.getApiResultItems(extMember, ApiUtil.getExtSystemId()));
			return ApiUtil.getResult(result, ResultStatus.SUCCESS);
			
		}catch (NumberFormatException e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			return result;
		}
	}
	
	@Override
	public ApiResult getMemberGift(String jsonParam){
		JSONObject jo = JSONObject.parseObject(jsonParam);
		Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
		ApiResult result = null;
		try{
			result=new ApiResult();
			if(memberId==null){
				return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
			Member member = memberRepository.findByPk(memberId);
			if(member==null){
				return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
			List<MemberGiftExt> memberGiftExts = memberGiftService.findReceiveMemberGiftRecord(memberId);
			result.setItems(ApiObjUtil.getApiResultItems(memberGiftExts, ApiUtil.getExtSystemId()));
			return ApiUtil.getResult(result, ResultStatus.SUCCESS);
			
		}catch (NumberFormatException e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			return result;
		}
	}
	@Override
	public ApiResult getMemberGiftByCode(String jsonParam){
		JSONObject jo = JSONObject.parseObject(jsonParam);
		Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
		String giftCode = jo.containsKey("giftCode") ? jo.getString("giftCode") : null;
		ApiResult result = null;
		try{
			result=new ApiResult();
			if(memberId==null){
				return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
			Member member = memberRepository.findByPk(memberId);
			if(member==null){
				return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
			List<MemberGift> memberGifts = memberGiftService.findReceiveMemberGiftByGiftCode(giftCode, memberId);
			result.setItems(ApiObjUtil.getApiResultItems(memberGifts, ApiUtil.getExtSystemId()));
			return ApiUtil.getResult(result, ResultStatus.SUCCESS);
			
		}catch (NumberFormatException e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			return result;
		}
	}
}
