package com.bw.adv.api.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.helpers.CastUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.io.DelegatingInputStream;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Value;

import com.bw.adv.core.utils.ThreadLocalUtil;

/**
 * ClassName: CheckValidInterceptor <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-19 上午10:52:01 <br/>
 * scrmVersion 1.0
 * 
 * @author mennan
 * @version jdk1.7
 */
public class CheckValidInterceptor extends AbstractPhaseInterceptor<Message> {
	@Value("${webservice_client_ips}")
	private static String WEBSERVICE_CLIENT_IPS ;
	@Value("${webservice_client_filter}")
	private static String WEBSERVICE_CLIENT_FILTER ;
	@Value("${is_check_sign}")
	private static String IS_CHECK_SIGN;
	@Value("${check_sign}")
	private static String CHECK_KEY_NAME ;
	@Value("${sign_key}")
	private static String SIGN_KEY;
	private Logger logger = Logger.getLogger("CheckValidInterceptor");
	private static List<String> IGNORE_CHECK_URLS = new ArrayList<String>();
	private static List<String> CHECK_URLS = new ArrayList<String>();

	static {
		CHECK_URLS.add("/scrm-pe-web/rs/external/api/member/sendVerifyCode");
	}

	public CheckValidInterceptor() {
		super(Phase.PRE_INVOKE);
	}

	@Override
	public void handleMessage(Message message) throws Fault {
		Map<String, List<String>> headerMap = null;
		List<String> sa = null;
		List<String> langx = null;
		List<String> org = null;
		List<String> store = null;
		List<String> msg = null;
		List<String> channel = null;
		String uncheckedKey = null;
		HttpServletRequest request = null;
		String clientIp = null;

		request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);

		clientIp = RequestUtil.getRemoteIpAddr(request);
		
		if("127.0.0.1".equals(clientIp)){
			return;
		}
		
		ThreadLocalUtil.set(ThreadLocalUtil.CXF_REQUEST_IP, clientIp);

		// TODO 如果key==N 并且url不在checkUrl中
		// 是否不校验key
		if (StringUtils.isNotBlank(IS_CHECK_SIGN) && IS_CHECK_SIGN.equals("N")
				&& !CHECK_URLS.contains(request.getRequestURI().toString())) {
			return;
		}
		if(IS_CHECK_SIGN==null){
			return;
		}

		if ("Y".equals(WEBSERVICE_CLIENT_FILTER) && !checkClientIps(request)) {
			throw new Fault("非法客户端调用", logger);
		}
		boolean isNeedCheck = true;
		for (String url : IGNORE_CHECK_URLS) {
			if (request.getRequestURL().toString().contains(url)) {
				isNeedCheck = false;
				break;
			}
		}
		if (isNeedCheck) {
			headerMap = CastUtils.cast((Map<?, ?>) message.get(Message.PROTOCOL_HEADERS));

			// 取得header中的校验字符串
			if (headerMap != null) {
				sa = headerMap.get(CHECK_KEY_NAME);
				langx = headerMap.get(ThreadLocalUtil.LANGX);
				org = headerMap.get(ThreadLocalUtil.SYSTEM_ID);
				store = headerMap.get(ThreadLocalUtil.PRODUCT_STORE_ID);
				msg = headerMap.get(ThreadLocalUtil.MSG_ID);
				channel = headerMap.get(ThreadLocalUtil.CHANNEL_ID);
				if (sa != null && sa.size() > 0) {
					uncheckedKey = sa.get(0);
					if (StringUtils.isBlank(uncheckedKey)) {
						throw new Fault("密钥为空", logger);
					} else {
						checkSign(message, uncheckedKey);
					}
				} else {
					throw new Fault("无密钥", logger);
				}
				if (org != null && org.size() > 0) {
					ThreadLocalUtil.set(ThreadLocalUtil.SYSTEM_ID, org.get(0));
				}
				if (store != null && store.size() > 0) {
					ThreadLocalUtil.set(ThreadLocalUtil.PRODUCT_STORE_ID, store.get(0));
				}
				if (msg != null && msg.size() > 0) {
					ThreadLocalUtil.set(ThreadLocalUtil.MSG_ID, msg.get(0));
				}
				if (channel != null && channel.size() > 0) {
					ThreadLocalUtil.set(ThreadLocalUtil.CHANNEL_ID, channel.get(0));
				}
				if (langx != null && langx.size() > 0) {
					ThreadLocalUtil.set(ThreadLocalUtil.LANGX, langx.get(0));
				} else {
					ThreadLocalUtil.set(ThreadLocalUtil.LANGX, "cn");
				}
			} else {
				throw new Fault("无header信息", logger);
			}

			List<?> tmpList = message.getContent(List.class);
			// if(!uncheckedKey.equals(SIGN_KEY)){
			// throw new Fault("验证错误", logger);
			// }
			message.setContent(List.class, tmpList);
		}
	}

	private void checkSign(Message message, String requestSign) {
		String queryStr = null;
		InputStream is = null;
		InputStream bis = null;
		JSONObject json = null;
		String queryStrs[] = null;
		String sign = null;
		String contentType = null;
		try {
			json = new JSONObject();

			queryStr = (String) message.get(Message.QUERY_STRING);
			contentType = (String) message.get(Message.CONTENT_TYPE);

			if (StringUtils.isNotBlank(queryStr)) {
				queryStrs = queryStr.split(";");
				for (String str : queryStrs) {
					json.put(str.split("=")[0], str.split("=")[1]);
				}
			} else {
				is = message.getContent(InputStream.class);
				bis = is instanceof DelegatingInputStream ? ((DelegatingInputStream) is).getInputStream() : is;
				if (StringUtils.isNotBlank(contentType) && contentType.indexOf("xml") >= 0) {
					json = xmlFirstNodeToJson(bis.toString());
				}
				if (StringUtils.isNotBlank(contentType) && contentType.indexOf("json") >= 0) {
					json = JSONObject.fromObject(bis.toString());
				}
			}

			sign = MD5SecurityUtils.genSign(json, SIGN_KEY);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!sign.equals(requestSign)) {
			throw new Fault("验证失败", logger);
		}
	}

	/**
	 * checkClientIps:(检查客户端是否为合法调用ip). <br/>
	 * Date: 2015-8-19 上午10:53:26 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	private boolean checkClientIps(HttpServletRequest request) {
		String clientIp = RequestUtil.getRemoteIpAddr(request);
		if (WEBSERVICE_CLIENT_IPS.contains(clientIp)) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
	private static String xmltoJson(String xml) {
		XMLSerializer xmlSerializer = new XMLSerializer();
		return xmlSerializer.read(xml).toString();
	}

	/**
	 * xml根节无子节点的元素转json
	 * 
	 * @param xml
	 * @return
	 */
	private static JSONObject xmlFirstNodeToJson(String xml) {
		Element elm = null;
		JSONObject json = null;
		Document document = null;
		Element nodeElement = null;
		List<?> node = null;
		try {
			json = new JSONObject();
			document = DocumentHelper.parseText(xml);
			nodeElement = document.getRootElement();
			node = nodeElement.elements();
			for (Iterator<?> it = node.iterator(); it.hasNext();) {
				elm = (Element) it.next();
				// 过滤掉有子节点的属性
				if (elm.elements().size() == 0) {
					json.put(elm.getName(), elm.getText());
				}
				elm = null;
			}
			node = null;
			nodeElement = null;
			document = null;
			return json;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println(xmlFirstNodeToJson("<?xml version=\"1.0\" encoding=\"utf-8\"?><orderHeaderDto>"
				+ "<orderCouponDto><couponInstanceCode>000166112</couponInstanceCode></orderCouponDto>"
				+ "<externalId>ESB6KE994EBFC9BE</externalId><cardNo>DQ0000119sss23</cardNo><memberCode>000011923</memberCode>"
				+ "<channelId>104</channelId></orderHeaderDto>"));
	}

}
