package com.bw.adv.api.member.api.enums;

import org.apache.commons.lang.StringUtils;

/**
 * Created by jeoy.zhou on 6/13/16.
 */
public enum MemberQueryType {
    MEMBER_CODE("memberCode"),
    MEMBER_MOBILE("mobile"),
    MEMBER_CARD_NO("cardNo");

    private String queryType;

    MemberQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public static MemberQueryType getMemberQueryType(String type) {
        if (StringUtils.isBlank(type)) return null;
        for (MemberQueryType memberQueryType : MemberQueryType.values()) {
            if (memberQueryType.queryType.equals(type)) {
                return memberQueryType;
            }
        }
        return null;
    }
}
