/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:ComplainApi.java
 * Package Name:com.sage.scrm.api.complain.api
 * Date:2015年12月30日下午3:12:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.complain.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bw.adv.api.result.ApiResult;
import com.bw.adv.module.member.complain.model.Complain;

/**
 * ClassName:ComplainApi <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月30日 下午3:12:51 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Path("/complain")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface ComplainApi {
	@POST
	@Path("/saveComplain")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult saveComplain(Complain complain);
	
	@POST
	@Path("/findComplainByPk")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult findComplainByPk(@MatrixParam("complainId")String complainId);
	
	@GET
	@Path("/findComplainType")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult findComplainType();
	
	@POST
	@Path("/saveSatisfaction")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult saveSatisfaction(@MatrixParam("complainId")String complainId,@MatrixParam("satisfaction")String satisfaction,@MatrixParam("satisfactionDesc")String satisfactionDesc);
	
	@POST
	@Path("/findStoreList")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult findStoreList();
}

