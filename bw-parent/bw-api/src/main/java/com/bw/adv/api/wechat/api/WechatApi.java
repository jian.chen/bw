/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:WechatApi.java
 * Package Name:com.sage.scrm.api.wechat.api
 * Date:2015年11月10日下午8:49:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.wechat.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.bw.adv.api.result.ApiResult;
import com.bw.adv.wechat.parse.domain.WeChatParam;

/**
 * ClassName:WechatApi <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月10日 下午8:49:04 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Path("/wechat")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface WechatApi {
	
	/**
	 * getAccessToken:(获取微信token). <br/>
	 * Date: 2015年11月10日 下午8:52:22 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	@GET
	@Path("/getAccessToken")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getAccessToken();
	
	@GET
	@Path("/getAccessTicketCache")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ApiResult getAccessTicketCache(@MatrixParam("token")String token);
	
	/**
	 * checkSign:(微信接入). <br/>
	 * Date: 2015年8月18日 下午8:32:45 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param signature
	 * @param timestamp
	 * @param nonce
	 * @param echostr
	 * @return
	 */
	@GET
    @Path("/gateWay")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public String checkSign(@QueryParam("signature") String signature, @QueryParam("timestamp")  String timestamp,  @QueryParam("nonce") String nonce,  @QueryParam("echostr") String echostr);
	
	/**
	 * gateWay:(接受消息和事件的入口). <br/>
	 * Date: 2015年8月18日 下午8:33:05 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param weChatParam
	 * @return
	 */
	@POST
    @Path("/gateWay")
	@Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML,MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML,MediaType.TEXT_XML})
    public String gateWay(WeChatParam weChatParam);
	
	/**
	 * 
	 * getTicketAndSign:(获取ticket及其它参数). <br/>
	 * Date: 2016年3月9日 下午2:10:29 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param url
	 * @return
	 */
	@POST
    @Path("/getTicketAndSign")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ApiResult getTicketAndSign(String url);
}

