package com.bw.adv.api.member.api.impl;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.common.util.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.member.api.MemberApi;
import com.bw.adv.api.member.api.enums.MemberQueryType;
import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.member.constant.ExtAccountTypeConstant;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.MemberCardGrade;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.sys.model.ComVerification;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.StringUtil;
import com.bw.adv.module.wechat.api.ApiWeChatFan;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.service.base.service.MemberCardGradeService;
import com.bw.adv.service.cfg.service.ChannelCfgValueService;
import com.bw.adv.service.coupon.service.CouponInstanceService;
import com.bw.adv.service.member.service.MemberAddressService;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.wechat.service.WeChatFansService;


public class MemberApiImpl implements MemberApi {

    //会员信息查询级别
    public static String QUERY_LEVEL_MEMBER = "member";
    public static String QUERY_LEVEL_MEMBER_ACCOUNT = "member_account";
    public static String QUERY_LEVEL_MEMBER_CARD = "member_card";
    @Value("${captchaNum}")
    private static Long captchaNum;
    private Logger log = Logger.getLogger(MemberApiImpl.class);
    private WeChatFansService weChatFansService;

    private MemberService memberService;

    private MemberCardGradeService memberCardGradeService;

    private MemberAddressService memberAddressService;

    private CouponInstanceService couponInstanceService;
    
    private ChannelCfgValueService channelCfgValueService;
    
    @Autowired
    private ApiWeChatFan weChatFanAPI;
    
    /**
     * weChatFocusEvent:(微信新增粉丝事件，不新增会员). <br/>
     * TODO(代替原WeChatFocusEvent_bak方法).<br/>
     * Date: 2016-4-7 下午4:55:48 <br/>
     * scrmVersion 1.0
     *
     * @param memberDto
     * @return
     * @author mennan
     * @version jdk1.7
     */
    @Override
    public ApiResult weChatFocusEvent(MemberDto memberDto) {
        ApiResult result = null;
        WechatFans wechatFans = null;
        try {
            result = new ApiResult();
            wechatFans = new WechatFans();

            wechatFans.setOpenid(memberDto.getBindingAccount());
            BeanUtils.copyNotNull(memberDto, wechatFans);

            if (memberDto.getMemberPhoto() != null) {
                wechatFans.setHeadImgUrl(memberDto.getMemberPhoto());
            }

            wechatFans = weChatFansService.saveOrUpdate(wechatFans);
            result.setData(ApiObjUtil.getApiResultMap(wechatFans, ApiUtil.getExtSystemId()));
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
        } catch (MemberException e) {
            result = ApiUtil.getResult(result, e.getResultStatus());
            e.printStackTrace();
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }


    public ApiResult memberActivation(MemberDto memberDto) {

        ApiResult result = null;
        ResultStatus res = null;
        String couponCode = null; //是否转赠券

        try {
            result = new ApiResult();
            couponCode = memberDto.getCouponCode();
            res = this.memberService.activation(memberDto, couponCode);

            result = ApiUtil.getResult(result, res);
        } catch (MemberException e) {
            result = ApiUtil.getResult(result, e.getResultStatus());
            e.printStackTrace();
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }

        return result;
    }

    public ApiResult registerMember(MemberDto memberDto) {
        ApiResult result = null;
        Map<String, Object> map = null;
        try {
            result = new ApiResult();
            log.info("mobile: " + memberDto.getMobile());
            log.info("countryCode: " + memberDto.getCountryCode());
            if(memberDto.getCountryCode().indexOf("00") != 0){
                memberDto.setCountryCode("00" + memberDto.getCountryCode());
            }
            memberDto.setMobile(StringUtil.differentInternationalMobile(memberDto.getMobile(), memberDto.getCountryCode()));
            map = memberService.registMemberInfo(memberDto);
            Long memberId = (Long) map.get("memberId");
            Long channelId = (Long) map.get("channelId");
            if(channelId >= 300L && channelId < 400L && channelId != 303L){
            	memberService.pointsAfterRegister(memberId);
            }
            channelCfgValueService.afterRegiter(memberId,channelId);
            result.setData(ApiObjUtil.getApiResultMap(map.get("member"), ApiUtil.getExtSystemId()));
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
            result.setCode((String) map.get("code"));
        } catch (MemberException e) {
            result = ApiUtil.getResult(result, e.getResultStatus());
            e.printStackTrace();
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * TODO 绑定会员（可选）.
     *
     * @see com.bw.adv.api.member.api.MemberApi#bindingMember(com.bw.adv.api.member.dto.MemberDto)
     */
    @Override
    public ApiResult bindingMember(MemberDto memberDto) {
        ApiResult result = null;
        Member member = null;
        try {
            result = new ApiResult();
            member = memberService.bindingMember(memberDto);
            result.setData(ApiObjUtil.getApiResultMap(member, ApiUtil.getExtSystemId()));
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
        } catch (MemberException e) {
            result = ApiUtil.getResult(result, e.getResultStatus());
            e.printStackTrace();
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }

    public ApiResult registerMemberNoWechat(MemberDto memberDto) {
        ApiResult result = null;
        String extAccountTypeId = null;
        String bindingAccount = null;
        String mobile = null;
        Member member = null;
        MemberSearch search = null;

        try {
            result = new ApiResult();
            extAccountTypeId = memberDto.getExtAccountTypeId();
            bindingAccount = memberDto.getBindingAccount();
            mobile = memberDto.getMobile();
            //验证手机号是否重复（必输）
            if (StringUtils.isBlank(mobile)) {
                result = ApiUtil.getResult(result, ResultStatus.MEMBER_PHONE_NULL_ERROR);
                return result;
            } else {
                search = new MemberSearch();
                search.setEqualMobile(mobile);
                Long count = memberService.queryCountSearchAuto(search);
                if (count > 0) {
                    result = ApiUtil.getResult(result, ResultStatus.MEMBER_PHONE_EXIST_ERROR);
                    return result;
                }
            }

            member = new Member();
            member.setMemberPhoto(memberDto.getMemberPhoto());//头像
            member.setChannelId(ApiUtil.getChannelId());//渠道
            member.setRegisterTime(DateUtils.getCurrentTimeOfDb());//注册时间
            member.setMobile(mobile);//移动电话


            //5、保存会员数据，同时新增账户信息, 新增外部账号信息,更新粉丝表会员id
            memberService.registMemberNoWechat(member, extAccountTypeId, bindingAccount);
            result.setData(ApiObjUtil.getApiResultMap(member, ApiUtil.getExtSystemId()));
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ApiResult updateMember(MemberExp memberExp) {
        ApiResult result = null;

        try {
            result = new ApiResult();
            if (memberExp.getCountryCode().indexOf("00") != 0){
                memberExp.setCountryCode("00" + memberExp.getCountryCode());
            }
            memberExp.setMobile(StringUtil.differentInternationalMobile(memberExp.getMobile(), memberExp.getCountryCode()));
            log.info("mobile: " + memberExp.getMobile());
            log.info("originalMobile: " + memberExp.getOriginalMobile());
            this.memberService.wechatUserEdit(memberExp);

            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
        } catch (MemberException e) {
            result = ApiUtil.getResult(result, e.getResultStatus());
            e.printStackTrace();
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ApiResult queryMemberByBindingAccount(String extAccountTypeId, String bindingAccount, String mobile) {
        ApiResult result = null;
        try {
            result = new ApiResult();
            Member member = this.memberService.queryMemberByBindingAccount(extAccountTypeId, bindingAccount);

            if (member != null) {
                result.setData(ApiObjUtil.getApiResultMap(member, ApiUtil.getExtSystemId()));

                result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
            } else {
            	net.sf.json.JSONObject resultResult = weChatFanAPI.getFanInfo(bindingAccount);

    			String headImgUrl = (String) resultResult.get("headimgurl");
    			String unionid = (String) resultResult.get("unionid");
    			String nickName = (String) resultResult.get("nickname");
    			String sex = resultResult.get("sex") == null ? null : resultResult.get(
    					"sex").toString();
    			String cty = (String) resultResult.get("city");
    			String province = (String) resultResult.get("province");
    			String country = (String) resultResult.get("country");
    			String language = (String) resultResult.get("language");

    			WechatFans wechatFans=new WechatFans();
    			wechatFans.setUnionid(unionid);
    			wechatFans.setNickName(nickName);
    			wechatFans.setSex(sex);
    			wechatFans.setCty(cty);
    			wechatFans.setProvince(province);
    			wechatFans.setCountry(country);
    			wechatFans.setLanguage(language);
    			wechatFans.setHeadImgUrl(headImgUrl);
    			wechatFans.setOpenid(bindingAccount);

    			// 保存粉丝信息
    			weChatFansService.saveOrUpdate(wechatFans);

    			Member memberExt = memberService.queryMemberByBindingAccount(
    					ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getCode(),
    					bindingAccount);
    			if (memberExt != null) {
    				memberExt.setMemberPhoto(headImgUrl);
    				memberService.updateByPkSelective(memberExt);
    			} else {
    				memberService.saveMemberInfo(wechatFans);
    			}

            }
        } catch (MemberException e) {
            result = ApiUtil.getResult(result, ResultStatus.MEMBER_ACCOUNT_NOTEXIST_ERROR);
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ApiResult queryMemberAccountByBindingAccount(String extAccountTypeId, String bindingAccount) {
        ApiResult result = null;
        Long memberId = null;
        Member member = null;
        MemberAccount memberAccount = null;
        try {
            result = new ApiResult();
            member = this.memberService.queryMemberByBindingAccount(extAccountTypeId, bindingAccount);
            if (member != null) {

                memberId = member.getMemberId();
                memberAccount = this.memberService.queryMemberAccountByMemberId(memberId);
                result.setData(ApiObjUtil.getApiResultMap(memberAccount, ApiUtil.getExtSystemId()));
            }
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public ApiResult queryMemberByBindingAccountAndLevel(String extAccountTypeId, String bindingAccount, String queryLevel) {
        ApiResult result = null;
        Member member = null;
        Long memberId = null;
        MemberCard memberCard = null;
        MemberCardGrade memberCardGrade = null;
        MemberAccount memberAccount = null;
        MemberAddress memberAddress = null;
        Map<String, Object> m = null;
        try {
            m = new HashMap<String, Object>();

            result = new ApiResult();
            member = this.memberService.queryMemberByBindingAccount(extAccountTypeId, bindingAccount);
            
            if (member == null) {
                result = ApiUtil.getResult(result, ResultStatus.MEMBER_ACCOUNT_NOTEXIST_ERROR);
                return result;
            } else if (StatusConstant.MEMBER_NO_ACTIVATION.getId().equals(member.getStatusId())) {
                result = ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_ACTIVE_ERROR);//会员未激活
                return result;
            }
            memberId = member.getMemberId();
            List<CouponInstanceExp> ciList = this.couponInstanceService.queryActiveListByMemberId(memberId);
            if(!CollectionUtils.isEmpty(ciList)){
            	m.put("couponAmount", ciList.size());
            }
            if (StringUtils.isBlank(queryLevel)) {
                memberCard = this.memberService.queryMemberCardByMemberId(memberId);
                memberCardGrade = this.memberCardGradeService.queryByPk(member.getGradeId());
                memberAccount = this.memberService.queryMemberAccountByMemberId(memberId);
                m.put("memberCard", memberCard);
                m.put("memberCardGrade", memberCardGrade);
                m.put("memberAccount", memberAccount);
            } else if (queryLevel.equals("1")) {
                memberAddress = this.memberService.queryMemberDefault(memberId);
                m.put("memberAddress", memberAddress);
            }

            BigDecimal totalPoints = memberService.upGrade(member) != null ? memberService.upGrade(member) : new BigDecimal(0);
            BigDecimal nextPoints = new BigDecimal(0);
            if(member.getGradeId().equals(3L)){
            	nextPoints = new BigDecimal(-1);
			}else{
				Grade grade = GradeInit.getGradeByPk(member.getGradeId()+1L);
				nextPoints = new BigDecimal(grade.getGtValue()).subtract(totalPoints);
			}
            
            Grade grade = GradeInit.getGradeBySeq(member.getGradeId().intValue());
            member.setGradeName(grade.getGradeName());
            m.put("nextPoints", nextPoints);
            m.put("member", member);

            result.setData(m);
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ApiResult sendVerifyCode(String json) {
        ApiResult result = null;
        MemberSearch search = null;
        Example example = null;
        List<ComVerification> comVs = null;
        Channel channel = null;
        ComVerification comVerification = null;
        JSONObject params = JSONObject.parseObject(json);
        try {
            result = new ApiResult();
            String mobile = params.getString("mobile");
            String countryCode = params.getString("countryCode");
            if (countryCode.indexOf("00") != 0){
                countryCode = "00" + countryCode;
            }
            mobile = StringUtil.differentInternationalMobile(mobile, countryCode);
            log.info("search mobile: " + mobile);
            String isBinding = params.getString("isBinding");
            String channelCode = params.getString("channelCode");
            //验证手机号是否重复（必输）
            if (StringUtils.isBlank(mobile)) {
                result = ApiUtil.getResult(result, ResultStatus.MEMBER_PHONE_NULL_ERROR);
                return result;
            } else {
                //如果为空或者不是绑定,则校验会员是否存在
                if (StringUtils.isBlank(isBinding) || !isBinding.equals("Y")) {
                    search = new MemberSearch();
                    search.setEqualMobile(mobile);
                    Long count = memberService.queryCountSearchAuto(search);
                    if (count > 0) {
                        result = ApiUtil.getResult(result, ResultStatus.MEMBER_PHONE_EXIST_ERROR);
                        return result;
                    }
                }
            }

            channel = ChannelConstant.getConstantByCode(ChannelConstant.class, channelCode);

            //验证验证码
            example = new Example();
            Criteria exampleTemp = example.createCriteria();
            exampleTemp.andEqualTo("MOBILE", mobile);
            exampleTemp.andGreaterThanOrEqualTo("create_time", DateUtils.getCurrentDateOfDb() + "000000");
            exampleTemp.andLessThanOrEqualTo("create_time", DateUtils.getCurrentDateOfDb() + "235959");
            exampleTemp.andLessThanOrEqualTo("channel_Id", channel.getChannelId());

            comVs = this.memberService.findComvByExample(example);
            if (comVs != null && comVs.size() >= captchaNum) {
                result = ApiUtil.getResult(result, ResultStatus.TEXT_NUM_ERROR);//短信已达每日发送上限
                return result;
            } else {
                comVerification = new ComVerification();
                comVerification.setMobile(mobile);
                comVerification.setChannelId(channel.getChannelId());
                comVerification.setField1(isBinding);
                memberService.insertComVerification(comVerification);
            }
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);

        } catch (MemberException e) {
            result = ApiUtil.getResult(result, e.getResultStatus());
            e.printStackTrace();
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public ApiResult addMemberAddress(MemberAddress memberAddress) {
        ApiResult result = null;
        MemberAddress memberAddressDto = null;
        Map<String, Object> map = null;
        try {
            result = new ApiResult();
            map = new HashMap<String, Object>();
            memberAddressDto = new MemberAddress();
            BeanUtils.copy(memberAddress, memberAddressDto);
            this.memberAddressService.save(memberAddressDto);
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
            map.put("memberAddressId", memberAddressDto.getMemberAddressId());
            result.setData(map);
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }

        return result;
    }


    @Override
    public ApiResult updateMemberAddress(MemberAddress memberAddress) {
        ApiResult result = null;
        MemberAddress memberAddressDto = null;
        try {
            result = new ApiResult();
            memberAddressDto = this.memberAddressService.queryMemberAddressByMemberId(memberAddress.getMemberId());
            if (memberAddressDto != null) {
//				BeanUtils.copy(memberAddress, memberAddressDto);
                memberAddressDto.setAddressDetail(memberAddress.getAddressDetail());
                memberAddressDto.setCityId(memberAddress.getCityId());
                memberAddressDto.setPersonMobile(memberAddress.getPersonMobile());
                memberAddressDto.setPersonName(memberAddress.getPersonName());
                memberAddressDto.setIsDefault(memberAddress.getIsDefault());
                this.memberAddressService.updateByPk(memberAddressDto);
            }
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
        } catch (NumberFormatException e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public ApiResult queryMemberAddressByMemberId(String memberId) {
        ApiResult result = null;
        Map<String, Object> map = null;
        MemberAddress memberAddress = null;
        try {
            result = new ApiResult();
            map = new HashMap<String, Object>();
            memberAddress = memberAddressService.queryMemberAddressByMemberId(Long.valueOf(memberId));
            map.put("memberAddress", memberAddress);
            result.setData(map);
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }

        return result;
    }


    @Override
    public ApiResult updateWeChatFans(WechatFans wechatFans) {

        ApiResult result = null;

        try {
            result = new ApiResult();

            weChatFansService.updateFansAndMem(wechatFans);

            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);

        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }

        return result;

    }

    @Override
    public ApiResult queryMemberByCode(JSONObject requestParams) {
        if (requestParams == null || requestParams.get("queryCode") == null || requestParams.get("queryType") == null) {
            return ApiUtil.getResult(new ApiResult(), ResultStatus.SYSTEM_ERROR);
        }
        String queryCode = requestParams.getString("queryCode");
        String queryType = requestParams.getString("queryType");
        MemberQueryType memberQueryType = MemberQueryType.getMemberQueryType(queryType);
        if (memberQueryType == null) {
            return ApiUtil.getResult(new ApiResult(), ResultStatus.SYSTEM_ERROR);
        }
        ApiResult result = new ApiResult();
        MemberExp memberExp = null;
        Map<String, Object> map = null;
        try {
            if (MemberQueryType.MEMBER_MOBILE == memberQueryType
                    || memberQueryType.MEMBER_CARD_NO == memberQueryType) {
                memberExp = memberService.findMemberExpByCode(queryCode, queryType);
            }
            if (memberExp == null) {
                return ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
            }
            if(memberExp.getPointsBalance()==null){
            	memberExp.setPointsBalance(new BigDecimal("0"));
            }
            List<MemberTag> tagList = this.memberService.queryMemberTagByMemberId(memberExp.getMemberId());
            String[] tags = new String[tagList.size()];
            for(int i = 0;i < tagList.size();i++){
            	tags[i] = new String(tagList.get(i).getMemberTagName());
            }
            memberExp.setMemberTag(tags);
            map = new HashMap<String, Object>();
            map.put("member", memberExp);
            result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
            result.setData(map);
        } catch (Exception e) {
            result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
        }
        return result;
    }
    
    @Override
	public ApiResult updateLanguage(Long memberId, String language) {
    	ApiResult result = new ApiResult();
		try {
			Member member = new Member();
			member.setMemberId(memberId);
			member.setLanguage(language);
			memberService.updateByPkSelective(member);
		} catch (Exception e) {
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
            e.printStackTrace();
		}
		return result;
	}


    @Autowired
    public void setWeChatFansService(WeChatFansService weChatFansService) {
        this.weChatFansService = weChatFansService;
    }


    @Autowired
    public void setMemberService(MemberService memberService) {
        this.memberService = memberService;
    }

    @Autowired
    public void setMemberCardGradeService(
            MemberCardGradeService memberCardGradeService) {
        this.memberCardGradeService = memberCardGradeService;
    }

    @Autowired
    public void setMemberAddressService(MemberAddressService memberAddressService) {
        this.memberAddressService = memberAddressService;
    }

    @Autowired
	public void setCouponInstanceService(CouponInstanceService couponInstanceService) {
		this.couponInstanceService = couponInstanceService;
	}

    @Autowired
	public void setChannelCfgValueService(ChannelCfgValueService channelCfgValueService) {
		this.channelCfgValueService = channelCfgValueService;
	}

}
