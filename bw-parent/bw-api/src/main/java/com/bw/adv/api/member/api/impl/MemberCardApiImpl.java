/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:MemberCardApiImpl.java
 * Package Name:com.sage.scrm.api.member.api.impl
 * Date:2015-11-17下午3:21:00
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.member.api.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.member.api.MemberCardApi;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.coupon.exception.CouponException;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.member.exception.MemberCardException;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.service.coupon.service.CouponInstanceService;
import com.bw.adv.service.member.service.MemberCardService;
import com.bw.adv.service.member.service.MemberService;

/**
 * ClassName:MemberCardApiImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-11-17 下午3:21:00 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public class MemberCardApiImpl implements MemberCardApi {
	
	private static String QUERY_TYPE_MEMBER_CARD = "1";
	private static String QUERY_TYPE_COUPON = "2";
	
	@Value("${member_card_length}")
	private static Integer MEMBER_CARD_LENGTH ;
	
	private static final Logger logger = Logger.getLogger(MemberCardApiImpl.class);
	
	
	private MemberCardService memberCardService;
	private MemberService memberService;
	private CouponInstanceService couponInstanceService;
	

	public ApiResult queryMemberInfoByQueryCode(String queryCode,String storeCode) {
		String memberType = null;
		String queryType = null;
		MemberCard memberCard = null;
		MemberExp memberExp = null;
//		Member member = null;
		ApiResult apiResult = null;
		Map<String, Object> data = null;
		CouponInstanceExp couponInstanceExp = null;
//		List<CouponInstanceExp> couponList = null;
		Long memberId = null;
		String billInfo = null;
		try {
			apiResult = new ApiResult();
			if(StringUtils.isBlank(queryCode)){
				return ApiUtil.getResult(apiResult, ResultStatus.COMMON_PARAMS_ERROR);
			}
			//如果是会员卡
			if(MEMBER_CARD_LENGTH != null && queryCode.length() == MEMBER_CARD_LENGTH){
				queryType = QUERY_TYPE_MEMBER_CARD;
			}else{
				queryType = QUERY_TYPE_COUPON;
			}
			
			if(StringUtils.isBlank(queryType)){
				return ApiUtil.getResult(apiResult, ResultStatus.COMMON_PARAMS_ERROR);
			}
			//如果是普通会员
			if(StringUtils.isBlank(memberType)){
			}
			
			data = new HashMap<String, Object>();
			
			//如果是根据会员卡
			if(queryType.equals(QUERY_TYPE_MEMBER_CARD)){
				memberCard = memberCardService.queryByCardNo(queryCode);
				if(memberCard == null){
					return ApiUtil.getResult(apiResult, ResultStatus.MEMBER_CARD_NOT_EXIST);
				}
				//判断该会员卡是否可用
				if(memberCard.getStatusId().equals(StatusConstant.MEMBER_CARD_NOT_ACTIVATION.getId()) ||
						memberCard.getStatusId().equals(StatusConstant.MEMBER_CARD_DISABLED.getId()) ||
						memberCard.getStatusId().equals(StatusConstant.MEMBER_CARD_REMOVE.getId()) ){
					return ApiUtil.getResult(apiResult, ResultStatus.MEMBER_CARD_STATUS_ERROR);
				}
				memberId = memberCard.getMemberId();
//				couponList = couponInstanceService.queryActiveListByMemberId(memberCard.getMemberId());
				memberExp = memberService.queryExpByMemberId(memberId);
				if(memberExp == null){
					apiResult = ApiUtil.getResult(apiResult, ResultStatus.MEMBER_NOT_EXIST_ERROR);
					return apiResult;
				}
				//返回会员，会员卡，优惠券列表
				data.put("member", ApiObjUtil.getApiResultMap(memberExp, ApiUtil.getExtSystemId()));
				data.put("memberCard", ApiObjUtil.getApiResultMap(memberCard, ApiUtil.getExtSystemId()));
//				data.put("couponList", ApiObjUtil.getApiResultItems(couponList, ApiUtil.getExtSystemId()));
				data.put("couponInstance",null);
			}//手机号
			else if(StringUtils.isBlank(queryType)){
			}//优惠券
			else if(queryType.equals(QUERY_TYPE_COUPON)){
				couponInstanceExp = couponInstanceService.freezeCouponInstanceByCode(queryCode,storeCode);
				//查询该券的会员信息
				memberId = couponInstanceExp.getCouponInstanceUser();
				memberExp = memberService.queryExpByMemberId(memberId);
				if(memberExp == null){
					apiResult = ApiUtil.getResult(apiResult, ResultStatus.MEMBER_NOT_EXIST_ERROR);
					return apiResult;
				}
				memberCard = memberCardService.queryByMemberId(memberId);
				//返回会员，会员卡，优惠券列表
				data.put("member", ApiObjUtil.getApiResultMap(memberExp, ApiUtil.getExtSystemId()));
				data.put("couponInstance", ApiObjUtil.getApiResultMap(couponInstanceExp, ApiUtil.getExtSystemId()));
				data.put("memberCard", memberCard);
			}
			billInfo = "会员卡：" + getEncryptCardNo(memberCard.getCardNo())+"\r\n";
			billInfo += "皇冠余额：" + (memberExp.getPointsBalance() == null ? 0 :memberExp.getPointsBalance().intValue());
			data.put("queryType", queryType);
			data.put("billInfo", billInfo);
			apiResult.setData(data);
			apiResult = ApiUtil.getResult(apiResult, ResultStatus.SUCCESS);
		} catch (MemberException e) {
			e.printStackTrace();
			apiResult = ApiUtil.getResult(apiResult, e.getResultStatus());
		}catch (MemberCardException e) {
			e.printStackTrace();
			apiResult = ApiUtil.getResult(apiResult, e.getResultStatus());
		} catch (CouponException e) {
			e.printStackTrace();
			apiResult = ApiUtil.getResult(apiResult, e.getResultStatus());
		} catch (Exception e) {
			e.printStackTrace();
			apiResult = ApiUtil.getResult(apiResult, ResultStatus.SYSTEM_ERROR);
		}
		logger.debug("ip:"+ApiUtil.getIp()+","+
				"storeCode:"+storeCode+ ","+
				"queryCode:"+ queryCode+ ","+
				"resultCode:"+apiResult.getCode() + ","+
				"resultMsg:"+apiResult.getMessage());
		return apiResult;
	}
	
	
	

	/**
	 * getEncryptCardNo:(卡号加密隐藏). <br/>
	 * Date: 2016-1-12 上午11:23:33 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param cardNo
	 * @return
	 */
	private String getEncryptCardNo(String cardNo){
		String encryptCardNo = "";
		encryptCardNo = cardNo.substring(0,2);
		for (int i = 0; i < cardNo.substring(2,cardNo.length()-2).length(); i++) {
			encryptCardNo += "*"; 
		}
		encryptCardNo += cardNo.substring(cardNo.length()-2); 
		return encryptCardNo;
	}
	
	
	
	@Autowired
	public void setCouponInstanceService(CouponInstanceService couponInstanceService) {
		this.couponInstanceService = couponInstanceService;
	}
	

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	@Autowired
	public void setMemberCardService(MemberCardService memberCardService) {
		this.memberCardService = memberCardService;
	}

}

