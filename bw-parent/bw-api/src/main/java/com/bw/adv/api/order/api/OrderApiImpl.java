package com.bw.adv.api.order.api;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.order.OrderApi;
import com.bw.adv.api.order.validtion.OrderParseSupport;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.coupon.exception.CouponException;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.order.model.dto.OrderHeaderDto;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.order.model.exp.OrderHeaderExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.order.service.OrderHeaderService;

/**
 * ClassName: OrderApiImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 上午10:49:00 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class OrderApiImpl implements OrderApi {
	
	private static final Logger logger = Logger.getLogger(OrderApiImpl.class);
	private OrderHeaderService orderHeaderService;
	private OrderParseSupport orderParseSupport;

	@Override
	public ApiResult handleSyncOrder(OrderHeaderDto orderHeaderDto) {
		ApiResult result = null;
		OrderInfo orderInfo = null;
		Map<String, Object> dataMap = null;
		String billInfo = null;
		try {
			result = new ApiResult();
			dataMap = new HashMap<String, Object>();
			
			orderInfo = orderParseSupport.parseOrderInfo(orderHeaderDto);
			billInfo = orderHeaderService.handleSyncOrder(orderInfo,orderHeaderDto.getOptType());
			dataMap.put("result", billInfo);
			result.setData(dataMap);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
		} catch (MemberException e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result, e.getResultStatus());
		} catch (CouponException e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result, e.getResultStatus());
		} catch (Exception e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
		}
		logger.debug("ip:"+ApiUtil.getIp()+","+
				"storeCode:"+orderHeaderDto.getStoreCode() + ","+
				"externalId:"+orderHeaderDto.getExternalId() + ","+
				"happingTime:"+orderHeaderDto.getHappenTime() + ","+
				"createTime:"+DateUtils.getCurrentTimeOfDb() + ","+
				"resultCode:"+result.getCode() + ","+
				"resultMsg:"+result.getMessage()
				);
		return result;
	}
	
	@Override
	public ApiResult getMemberOrderItemInfo(String json) {
		ApiResult result = null;
		List<OrderHeaderExp> queryResult = null;
		Page<OrderHeaderExp> pageObj = null;
		Example example = null;
		Criteria criteria = null;
		
		try {
			JSONObject jo = JSONObject.fromObject(json);
			result = new ApiResult();
			String memberCode = jo.containsKey("memberCode") ? jo.getString("memberCode") : null ;
			int pages = jo.containsKey("pages") ? jo.getInt("pages") : null;
			int rows = jo.containsKey("rows") ? jo.getInt("rows") : null;
			example = new Example();
			criteria = example.createCriteria();
			criteria.andEqualTo("mem.MEMBER_CODE", memberCode);
			pageObj = new Page<OrderHeaderExp>();
			pageObj.setPageNo(pages);
			pageObj.setPageSize(rows);
			queryResult = orderHeaderService.queryOrderExpListByExa(example, pageObj);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(queryResult, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public ApiResult showShopOrderCount(String json) {
		ApiResult result = null;
		List<OrderHeaderExp> countList = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			result = new ApiResult();
			com.alibaba.fastjson.JSONObject jo = com.alibaba.fastjson.JSONObject.parseObject(json);
			String storeCode = jo.containsKey("storeCode") ? jo.getString("storeCode") : null;
			String date = jo.containsKey("date") ? jo.getString("date") : null;
			countList = this.orderHeaderService.showShopOrderCount(storeCode, date);
			map.put("countList", countList);
			result.setData(map);
			result.setCode("200");
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Autowired
	public void setOrderHeaderService(OrderHeaderService orderHeaderService) {
		this.orderHeaderService = orderHeaderService;
	}

	@Autowired
	public void setOrderParseSupport(OrderParseSupport orderParseSupport) {
		this.orderParseSupport = orderParseSupport;
	}
	

}
