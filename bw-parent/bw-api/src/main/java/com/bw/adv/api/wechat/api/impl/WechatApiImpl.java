/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:WechatApiImpl.java
 * Package Name:com.sage.scrm.api.wechat.api.impl
 * Date:2015年11月10日下午8:50:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.wechat.api.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.api.wechat.api.WechatApi;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.JaxbUtil;
import com.bw.adv.module.tools.JaxbUtil.CollectionWrapper;
import com.bw.adv.module.tools.JsApiSignUtil;
import com.bw.adv.module.wechat.model.AccessTicketCache;
import com.bw.adv.module.wechat.model.WechatMenuClickRecord;
import com.bw.adv.module.wechat.model.exp.AccessTicketCacheExp;
import com.bw.adv.module.wechat.service.AccessTicketCacheService;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.module.wechat.util.SHA1;
import com.bw.adv.service.wechat.msg.util.WechatApiUtil;
import com.bw.adv.service.wechat.service.WechatMenuClickRecordService;
import com.bw.adv.wechat.parse.domain.WeChatParam;
import com.bw.adv.wechat.parse.domain.receive.WeChatReceive;
import com.bw.adv.wechat.parse.domain.send.WeChatSend;
import com.bw.adv.wechat.parse.service.event.ManagerService;

/**
 * ClassName:WechatApiImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月10日 下午8:50:35 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WechatApiImpl implements WechatApi {

	private static final Logger LOGGER = LoggerFactory.getLogger(WechatApiImpl.class);
	private static final String TOKEN = "baiweiBugToken";
	@Value("${appId}")
	public String appId;
	@Value("${domainName}")
	public String domainName;
	
	private static Long MULTIPLE = Long.valueOf(1000);
	private static Long DIFFMINUS =Long.valueOf(3600);
	private AccessTokenCacheService accessTokenService;
	private AccessTicketCacheService accessTicketCacheService;
	private ManagerService operatorService;
	private WechatMenuClickRecordService wechatMenuClickRecordService;
	
	@Override
	public ApiResult getAccessToken() {
		ApiResult result = null;
		String accessToken =null;
		Map<String,Object> data =null;
		
		try {
			data =new HashMap<String,Object>();
			result =new ApiResult();
			accessToken = accessTokenService.getAccessTokenCache();
			data.put("accessToken", accessToken);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setData(data);
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult getAccessTicketCache(String token) {
		String expiresIn = "";
		String ticket = "";
		int count = 0;
		ApiResult result = null;
		Map<String,Object> map = null;
		AccessTicketCache accessTicketCache = null;
		long currentTime = 0;
		String validateTime = "";
		try {
			result = new ApiResult();
			map = new HashMap<String, Object>();
			accessTicketCache = accessTicketCacheService.getAccessTicketCache();
			if (accessTicketCache == null || StringUtils.isEmpty(accessTicketCache.getAccessTicket())) {
                JSONObject obj = WechatApiUtil.getTicket(token);
                ticket = obj.getString("ticket");
                expiresIn = obj.getString("expires_in");
                if(StringUtils.isNotEmpty(ticket)){
                	currentTime = DateUtils.getCurrentDate().getTime();
                	validateTime = currentTime+(Long.valueOf(expiresIn)-DIFFMINUS)*MULTIPLE+"";
                	AccessTicketCache newTicket = new AccessTicketCache();
                	newTicket.setTimeOut(validateTime);
                	newTicket.setAccessToken(token);
                	newTicket.setAccessTicket(ticket);
                	accessTicketCacheService.save(newTicket);
                	result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
                	map.put("ticket", ticket);
                	result.setData(map);
                }else{
                	result = ApiUtil.getResult(result,ResultStatus.WECHAT_ERROR);
                }
	        }else {
	        	String timeOut = accessTicketCache.getTimeOut();
				currentTime = DateUtils.getCurrentDate().getTime();
				// 凭证已经过期
				if(Long.valueOf(timeOut) <= currentTime){
	            	JSONObject obj = WechatApiUtil.getTicket(token);
	                ticket = obj.getString("ticket");
	                expiresIn = obj.getString("expires_in");
	                if(StringUtils.isNotEmpty(ticket)){
	                	validateTime = currentTime+(Long.valueOf(expiresIn)-DIFFMINUS)*MULTIPLE+"";
	                	AccessTicketCacheExp accessTicketCacheExp = new AccessTicketCacheExp();
	                	accessTicketCacheExp.setAccessTicket(ticket);
	                	accessTicketCacheExp.setAccessToken(token);
	                	accessTicketCacheExp.setTimeOut(validateTime);
	                	accessTicketCacheExp.setTimeCondition(timeOut);
	                	accessTicketCacheExp.setAccessTicketCacheId(accessTicketCache.getAccessTicketCacheId());
	                	count = accessTicketCacheService.updateByPrimaryKey(accessTicketCacheExp);
	                	result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
	                	if(count == 1){
	                		map.put("ticket", ticket);
	                	}else{
	                		accessTicketCache = accessTicketCacheService.getAccessTicketCache();
	                		map.put("ticket", accessTicketCache.getAccessTicket());
	                	}
	                	result.setData(map);
	                }else{
	                	result = ApiUtil.getResult(result,ResultStatus.WECHAT_ERROR);
	                }
				}else{
					ticket = accessTicketCache.getAccessTicket();
					result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
	            	map.put("ticket", ticket);
	            	result.setData(map);
				}
	        }
		}catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	public String checkSign(String signature, String timestamp, String nonce,String echostr) {
		// 将获取到的参数放入数组
		String[] ArrTmp = {TOKEN, timestamp, nonce};
		// 按微信提供的方法，对数据内容进行排序
		Arrays.sort(ArrTmp);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < ArrTmp.length; i++) {
			sb.append(ArrTmp[i]);
		}
		String pwd = new SHA1().getDigestOfString(sb.toString().getBytes()).toLowerCase();
		if (pwd.equals(signature)) {
			LOGGER.info("微信平台签名消息验证成功！");
			return echostr;
		}
		LOGGER.info("微信平台签名消息验证失败！");
		return "error";
	}
	
	public String gateWay(WeChatParam weChatParam) {
        WeChatSend send = null;
        String xmlRes = null;
        try {
            WeChatReceive weChatReceive = WeChatReceive.getReceiveFromParam(weChatParam);
            send = operatorService.operatorReceive(weChatReceive);
            WechatMenuClickRecord clickRecord = new WechatMenuClickRecord();
            clickRecord.setCreateTime(DateUtils.getCurrentTimeOfDb());
            clickRecord.setOpenId(weChatParam.getFromUserName());
            clickRecord.setRemark(weChatParam.getEventKey());
            wechatMenuClickRecordService.save(clickRecord);
            //如果不做操作转换为空字符串以免微信三次回调
            if(send != null){
     			JaxbUtil requestBinder = new JaxbUtil(send.getClass(), CollectionWrapper.class);
     			xmlRes = requestBinder.toXml(send,"utf-8");
     		}else{
     			xmlRes = "";
     		}
        } catch (Exception e) {
        	e.printStackTrace();
            LOGGER.error(e.getMessage());
            xmlRes = "";
        }
        return xmlRes;
    }
	
	@Override
	public ApiResult getTicketAndSign(String url) {
		ApiResult result = null;
		
		ApiResult tokenResult = null; //获取token
		String token = null;
		ApiResult ticketResult = null; // 获取ticket
		String ticket = null;
		
		Map<String,String> paramMap = null; 
		Map<String,Object> data = null; //返回参数
		
		try {
			
			result = new ApiResult();
			data = new HashMap<String, Object>();
			
			if(StringUtils.isBlank(url)){
				result = ApiUtil.getResult(result, ResultStatus.COMMON_PARAMS_ERROR);
				return result;
			}
			
			if(url.contains(";")){
				url = url.replaceAll(";", "&");
			}
			
			tokenResult = getAccessToken();
			token = (String)tokenResult.getData().get("accessToken");
			
			ticketResult = getAccessTicketCache(token);
			ticket = (String)ticketResult.getData().get("ticket");
			
			paramMap = JsApiSignUtil.sign(ticket, url);
			
			Iterator<String> iter = paramMap.keySet().iterator();
			while (iter.hasNext()) {
				String key = iter.next();
				data.put(key, paramMap.get(key));
			}
			data.put("appId", appId);
			data.put("token", token);
			data.put("ticket", ticket);
			
			result.setData(data);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			
		} catch (Exception e) {
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		
		return result;
	}

	@Autowired
	public void setAccessTokenService(AccessTokenCacheService accessTokenService) {
		this.accessTokenService = accessTokenService;
	}

	public void setOperatorService(ManagerService operatorService) {
		this.operatorService = operatorService;
	}

	@Autowired
	public void setWechatMenuClickRecordService(WechatMenuClickRecordService wechatMenuClickRecordService) {
		this.wechatMenuClickRecordService = wechatMenuClickRecordService;
	}

	@Autowired
	public void setAccessTicketCacheService(AccessTicketCacheService accessTicketCacheService) {
		this.accessTicketCacheService = accessTicketCacheService;
	}


	
}

