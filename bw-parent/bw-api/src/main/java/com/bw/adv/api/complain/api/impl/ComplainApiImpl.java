/**

 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:ComplainApiImpl.java
 * Package Name:com.sage.scrm.api.complain.api.impl
 * Date:2015年12月30日下午3:16:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.complain.api.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.api.complain.api.ComplainApi;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.member.complain.model.Complain;
import com.bw.adv.module.member.complain.model.ComplainType;
import com.bw.adv.module.member.complain.model.exp.ComplainExp;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.service.base.service.StoreService;
import com.bw.adv.service.complain.service.ComplainService;
import com.bw.adv.service.complain.service.ComplainTypeService;
import com.bw.adv.service.member.service.MemberService;

/**
 * ClassName:ComplainApiImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月30日 下午3:16:39 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class ComplainApiImpl implements ComplainApi{
	private ComplainService complainService;
	private ComplainTypeService complainTypeService;
	private MemberService memberService;
	private CodeService codeService;
	private StoreService storeService;
	/**
	 * 
	 * TODO 提交投诉建议.
	 * @see com.bw.adv.api.complain.api.ComplainApi#SaveComplain(com.bw.adv.module.member.complain.model.Complain)
	 */
	@Override
	public ApiResult saveComplain(Complain complain) {
		ApiResult result = new ApiResult();
		String complainCode = null;
		if(null == complain){
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
		}else{
			if(StringUtils.isNotEmpty(complain.getIsMember()) && complain.getIsMember().equals("Y") && StringUtils.isNotEmpty(complain.getOpenId())){
				Member member = memberService.queryMemberByBindingAccount("ACCOUNT_TYPE_WECHAT", complain.getOpenId());
				if(null != member){
					complain.setMemberId(member.getMemberId());
				}
			}
			try {
				complainCode = this.codeService.generateCode(Complain.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
			complain.setComplainCode(complainCode);
			complain.setStatusId(StatusConstant.COMPLAIN_NOTREAD.getId());
			complainService.save(complain);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			result.setMessage("非常感谢 我们会尽快联系您");
		}
		return result;
	}
	/**
	 * 
	 * TODO 初始化投诉意见类型.
	 * @see com.bw.adv.api.complain.api.ComplainApi#findComplainType()
	 */
	@Override
	public ApiResult findComplainType(){
		List<ComplainType> complainTypes = new ArrayList<ComplainType>();
		ApiResult result = new ApiResult();
		complainTypes = complainTypeService.queryAllComplainType();
		result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
		result.setItems(ApiObjUtil.getApiResultItems(complainTypes, ApiUtil.getExtSystemId()));
		return result;
	}
	
	/**
	 * TODO 根据ID查询建议详情（可选）.
	 * @see com.bw.adv.api.complain.api.ComplainApi#findComplainByPk(java.lang.String)
	 */
	@Override
	public ApiResult findComplainByPk(String complainId) {
		ApiResult result = new ApiResult();
		Integer satisfation = null;
		Map<String, Object> resultMap = new HashMap<String, Object>();
		ComplainExp complainExp = complainService.queryDetailByPk(Long.valueOf(complainId));
		satisfation = complainExp.getSatisfaction();
		if (satisfation != null && satisfation > 0) {
			complainExp.setIsHandled("1");
		}else {
			complainExp.setIsHandled("0");
		}
		resultMap.put("complain", complainExp);
		result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
		result.setData(resultMap);
		return result;
	}
	/**
	 * TODO 保存用户对建议解决方案的评分（可选）.
	 * @see com.bw.adv.api.complain.api.ComplainApi#saveSatisfaction(java.lang.String)
	 */
	@Override
	public ApiResult saveSatisfaction(String complainId,String satisfaction,String satisfactionDesc) {
		ApiResult result = new ApiResult();
		Complain complain = new Complain();
		if (StringUtils.isNotBlank(complainId) && !complainId.equals("null")) {
			complain.setComplainId(Long.valueOf(complainId));
		}else {
			result = ApiUtil.getResult(result, ResultStatus.COMMON_PARAMS_ERROR);
			return result;
		}
		if (StringUtils.isNotBlank(satisfaction) && !satisfaction.equals("null")) {
			complain.setSatisfaction(Integer.valueOf(satisfaction));
		}else {
			complain.setSatisfaction(0);
		}
		if (StringUtils.isNotBlank(satisfactionDesc) && !satisfactionDesc.equals("null")) {
			complain.setSatisfactionDesc(satisfactionDesc);
		}
		complainService.updateByPkSelective(complain);
		result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
		return result;
	}
	
	@Override
	public ApiResult findStoreList() {
		ApiResult result = new ApiResult();
		try {
			List<Store> store = storeService.queryStoreList();
			result.setItems(ApiObjUtil.getApiResultItems(store, ApiUtil.getExtSystemId()));
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
		}
		return result;
	}
	
	@Autowired
	public void setComplainService(ComplainService complainService) {
		this.complainService = complainService;
	}

	@Autowired
	public void setComplainTypeService(ComplainTypeService complainTypeService) {
		this.complainTypeService = complainTypeService;
	}
	
	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}
	
	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}
	
	@Autowired
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
}

