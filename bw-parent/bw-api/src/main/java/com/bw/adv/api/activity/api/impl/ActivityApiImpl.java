/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:ActivityApiImpl.java
 * Package Name:com.sage.scrm.api.activity.api.impl
 * Date:2015年11月26日下午6:04:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.activity.api.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.activity.api.ActivityApi;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.activity.model.SceneGameInstance;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;
import com.bw.adv.module.activity.model.exp.WinningItemExp;
import com.bw.adv.module.activity.repository.WinningItemRepository;
import com.bw.adv.module.base.exception.ActivityException;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.AwardsSettingService;
import com.bw.adv.service.activity.service.LotteryService;
import com.bw.adv.service.activity.service.MemberWinningAccordService;
import com.bw.adv.service.activity.service.PrizeService;
import com.bw.adv.service.activity.service.SceneGameInstanceService;
import com.bw.adv.service.activity.service.WinningItemService;

/**
 * ClassName:ActivityApiImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 下午6:04:29 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class ActivityApiImpl implements ActivityApi {
	
	private LotteryService lotteryService;
	
	private AwardsSettingService awardsSettingService;
	
	private SceneGameInstanceService sceneGameInstanceService;
	
	private MemberWinningAccordService memberWinningAccordService;
	
	private WinningItemService winningItemService;
	
	private WinningItemRepository winningItemRepository;
	
	private PrizeService prizeService;

	private RuleInit ruleInit;

	@Override
	public ApiResult extractMemberAwards(String jsonParam) {
		List<AwardsSettingExp> awardsSettingList =null;
		AwardsSettingExp awardsSettingExp =null;
		ApiResult result = null;
		Map<String,Object> map =null;
		Prize prize =null;
		try {
			result = new ApiResult();
			map =new HashMap<String,Object>();
			JSONObject jo = JSONObject.parseObject(jsonParam);
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
			//Long activityId = jo.containsKey("activityId") ? jo.getLong("activityId") : null;
			String activityCode = jo.containsKey("activityCode") ? jo.getString("activityCode") : null;
			SceneGameInstance sceneGameInstance = sceneGameInstanceService.queryValidInstanceByCode(activityCode);//根据活动编码判断是否存在活动
			if(sceneGameInstance==null){
				return ApiUtil.getResult(result, ResultStatus.NO_ACTIVITY_END);
			}
			awardsSettingList = awardsSettingService.queryAwardsSettingExpByActivityId(sceneGameInstance.getSceneGameInstanceId());
			awardsSettingExp = lotteryService.extractMemberAwardsEveryMonth(memberId, sceneGameInstance.getSceneGameInstanceId(),null);
			prize = prizeService.queryPrizeByAwardId(awardsSettingExp.getAwardsId());
			map.put("awardsCode", awardsSettingExp.getAwardsCode());
			map.put("prizeTypeId", prize.getPrizeType());
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(awardsSettingList, ApiUtil.getExtSystemId()));
			result.setData(map);
		} catch(ActivityException e){
			if(ActivityException.NO_QUALIFICATION.getErrorId().equals(e.getErrorData().getErrorId())){
				result = ApiUtil.getResult(result,ResultStatus.NO_QUALIFICATION);
			}else if(ActivityException.ACTIVITY_END.getErrorId().equals(e.getErrorData().getErrorId())){
				result = ApiUtil.getResult(result,ResultStatus.ACTIVITY_END);
			}else{
				result= ApiUtil.getResult(result,e.getResultStatus());
			}
		}catch (NumberFormatException e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult initMemberAwards(String jsonParam) {
		ApiResult result = null;
		Map<String,Object> map = null;
		try {
			result = new ApiResult();
			map = new HashMap<String, Object>();
			JSONObject jo = JSONObject.parseObject(jsonParam);
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null;
			Long activityId = jo.containsKey("activityId") ? jo.getLong("activityId") : null;
			Long qrCodeId = jo.containsKey("qrCodeId") ? jo.getLong("qrCodeId") : null;
			Long chance = lotteryService.queryMemberLotteryChance(memberId,qrCodeId, activityId);
			Long useChance = memberWinningAccordService.queryMemberUseChance(memberId, activityId,null, DateUtils.getCurrentDateOfDb());
			map.put("chance", chance);
			map.put("useChance", useChance);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setData(map);
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	
	@Override
	public ApiResult queryMemberSurplusNumber(String memberId, String activityId) {
		ApiResult result = null;
		Integer count =null;
		Map<String,Object> map =null;
		try {
			map =new HashMap<String,Object>();
			result =new ApiResult();
			count = memberWinningAccordService.queryMemberSurplusNumber(Long.valueOf(memberId), Long.valueOf(activityId),null);
			map.put("count", count);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setData(map);
		} catch (NumberFormatException e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	@Override
	public ApiResult queryWinningItemsByMemberIdAndActivityId(String memberId,String activityId, int page, int rows) {
		List<WinningItem> winningItems =null;
		ApiResult result = null;
		Example example = null;
		Page<WinningItem> pageobj = null;
		try {
			pageobj = new Page<WinningItem>();
			pageobj.setPageNo(page);
			pageobj.setPageSize(rows);
			result = new ApiResult();
			example = new Example();
			Criteria criteria =  example.createCriteria();
			criteria.andEqualTo("wi.MEMBER_ID", memberId);
			criteria.andEqualTo("wi.ACTIVITY_ID", activityId);
			winningItems = winningItemRepository.findWinningItemPersonsByActivityIdAndAwardId(example, pageobj);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(winningItems, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public ApiResult queryLastWinningItem(Long num) {
		ApiResult result = null;
		List<WinningItemExp> winningItems =null;
		try {
			result = new ApiResult();
			winningItems = winningItemService.findLastNum(num);
			//result.setData(ApiObjUtil.getApiResultMap(winningItems, ApiUtil.getExtSystemId()));
			result.setItems(ApiObjUtil.getApiResultItems(winningItems, ApiUtil.getExtSystemId()));
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}


	@Override
	public ApiResult reloadGlobalInfo() {
		ApiResult result = new ApiResult();
		ruleInit.reLoad();
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("data", "ok");
		result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
		result.setData(data);

		return result;
	}

	/**
	 * TODO 新增抽奖机会（可选）.
	 * @see com.bw.adv.api.activity.api.ActivityApi#addMemberWinningAccord(java.lang.String)
	 */
	public ApiResult addMemberWinningAccord(String openid, Long activityId,int accordType) {
		ApiResult result = null;
		try {
			result = new ApiResult();
			//lotteryService.addMemberWinningAccord(openid, activityId,null,accordType);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Autowired
	public void setMemberWinningAccordService(MemberWinningAccordService memberWinningAccordService) {
		this.memberWinningAccordService = memberWinningAccordService;
	}
	
	@Autowired
	public void setLotteryService(LotteryService lotteryService) {
		this.lotteryService = lotteryService;
	}

	@Autowired
	public void setAwardsSettingService(AwardsSettingService awardsSettingService) {
		this.awardsSettingService = awardsSettingService;
	}

	@Autowired
	public void setWinningItemService(WinningItemService winningItemService) {
		this.winningItemService = winningItemService;
	}

	@Autowired
	public void setWinningItemRepository(WinningItemRepository winningItemRepository) {
		this.winningItemRepository = winningItemRepository;
	}

	@Autowired
	public void setPrizeService(PrizeService prizeService) {
		this.prizeService = prizeService;
	}

	@Autowired
	public void setRuleInit(RuleInit ruleInit) {
		this.ruleInit = ruleInit;
	}

	@Autowired
	public void setSceneGameInstanceService(SceneGameInstanceService sceneGameInstanceService) {
		this.sceneGameInstanceService = sceneGameInstanceService;
	}
}

