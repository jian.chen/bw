package com.bw.adv.api.gift.api;


import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bw.adv.api.result.ApiResult;

@Path("/giftExchange")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface GiftExchangeApi {
	
	@POST
	@Path("/memberBadgegiftExchange")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult memberBadgegiftExchange(String jsonParam);
	
	@POST
	@Path("/getMemberGiftMemberBadge")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult getMemberGiftMemberBadge(String jsonParam);

	@POST
	@Path("/getMemberGift")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberGift(String jsonParam);

	@POST
	@Path("/getMemberGiftByCode")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberGiftByCode(String jsonParam);
}
