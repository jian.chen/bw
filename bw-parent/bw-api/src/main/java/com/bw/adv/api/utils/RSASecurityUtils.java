package com.bw.adv.api.utils;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

public class RSASecurityUtils {

	/** 指定加密算法为RSA */
	private static final String ALGORITHM = "RSA";
	/** 密钥长度，用来初始化 */
	private static final int KEYSIZE = 1024;
	/** 指定公钥存放文件 */
	private static String PUBLIC_KEY_FILE = "";
	/** 指定私钥存放文件 */
	private static String PRIVATE_KEY_FILE = "";

	/**
	 * 生成密钥对
	 * 
	 * @throws Exception
	 */
	private static void generateKeyPair() throws Exception {

		// /** RSA算法要求有一个可信任的随机数源 */
		// SecureRandom secureRandom = new SecureRandom();

		/** 为RSA算法创建一个KeyPairGenerator对象 */
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);

		/** 利用上面的随机数据源初始化这个KeyPairGenerator对象 */
		// keyPairGenerator.initialize(KEYSIZE, secureRandom);
		keyPairGenerator.initialize(KEYSIZE);

		/** 生成密匙对 */
		KeyPair keyPair = keyPairGenerator.generateKeyPair();

		/** 得到公钥 */
		Key publicKey = keyPair.getPublic();

		/** 得到私钥 */
		Key privateKey = keyPair.getPrivate();

		PUBLIC_KEY_FILE = encryptBASE64(publicKey.getEncoded());
		PRIVATE_KEY_FILE = encryptBASE64(privateKey.getEncoded());

		System.out.println(PUBLIC_KEY_FILE);

		System.out.println(PRIVATE_KEY_FILE);
	}

	public static String encryptBASE64(byte[] key) throws Exception {
		return Base64.getBASE64(key);
	}

	/**
	 * 加密方法
	 * 
	 * @param source
	 *            源数据
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String source) throws Exception {

		PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.getBytesBASE64(PRIVATE_KEY_FILE));
		KeyFactory keyf = KeyFactory.getInstance("RSA");
		PrivateKey myprikey = keyf.generatePrivate(priPKCS8);

		/** 得到Cipher对象来实现对源数据的RSA加密 */
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, myprikey);
		byte[] b = source.getBytes();
		/** 执行加密操作 */
		byte[] b1 = cipher.doFinal(b);

		return Base64.getBASE64(b1);
	}

	/**
	 * 解密算法
	 * 
	 * @param cryptograph
	 *            密文
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String cryptograph) throws Exception {
		X509EncodedKeySpec bobPubKeySpec = new X509EncodedKeySpec(Base64.getBytesBASE64(PUBLIC_KEY_FILE));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey pubKey = keyFactory.generatePublic(bobPubKeySpec);

		/** 得到Cipher对象对已用公钥加密的数据进行RSA解密 */
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, pubKey);

		byte[] b1 = Base64.getBytesBASE64(cryptograph);

		/** 执行解密操作 */
		byte[] b = cipher.doFinal(b1);
		return new String(b);
	}

	public static void main(String[] args) throws Exception {
		generateKeyPair();

		String source = "恭喜发财!";// 要加密的字符串
		System.out.println("准备用公钥加密的字符串为：" + source);

		String cryptograph = encrypt(source);// 生成的密文
		System.out.print("用公钥加密后的结果为:" + cryptograph);
		System.out.println();
		String target = decrypt(cryptograph);// 解密密文
		System.out.println("用私钥解密后的字符串为：" + target);
		System.out.println();
	}
}
