package com.bw.adv.api.badge.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.result.ApiResult;

@Path("/memberBadge")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface MemberBadgeApi {
	
	/**
	 * getMemberBadgeInfo:查询会员徽章 <br/>
	 * Date: 2015年12月31日 下午2:36:42 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param json
	 * @return
	 */
	@POST
	@Path("/getMemberBadgeInfo")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberBadgeInfo(String json);
	
	/**
	 * getMemberBadgeExchangeInfo:查询会员徽章兑换记录 <br/>
	 * Date: 2015年12月31日 下午2:35:28 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param json
	 * @return
	 */
	@POST
	@Path("/getMemberBadgeExchangeInfo")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberBadgeExchangeInfo(String json);
	
	/**
	 * exchangeBadgeProductDetail:徽章兑换<br/>
	 * Date: 2015年12月31日 下午2:35:54 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param json
	 * @return
	 */
	@POST
	@Path("/exchangeBadgeProductDetail")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult exchangeBadgeProductDetail(String json);
	
	/**
	 * getBadgeProduct:查询商品兑换信息<br/>
	 * Date: 2015年12月14日 下午9:37:11 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param json
	 * @return
	 */
	@POST
	@Path("/getBadgeProduct")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getBadgeProduct(String json);

	/**
	 * queryBadgeProductDetail:查询徽章商城详情<br/>
	 * Date: 2015年12月31日 下午2:36:13 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param goodsShowId
	 * @return
	 */
	@GET
	@Path("/getBadgeProductDetail")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryBadgeProductDetail(@MatrixParam("goodsShowId")Long goodsShowId,@MatrixParam("gradeId")Long gradeId);

	@POST
	@Path("/getMemberBadgeList")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberBadgeList(JSONObject memberBadgeParams);

	/**
	 * 获取徽章商城商品
	 * 不包含会员相关信息，只是徽章商品
	 * @param params
	 * @return
	 */
	@POST
	@Path("/getBadgeProductList")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult queryBadgeProductList(JSONObject params);
}
