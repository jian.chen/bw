package com.bw.adv.aop.service;

import net.sf.json.JSONObject;

import java.util.List;

import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.order.model.dto.OrderInfo;


/**
 * ClassName: 订单活动动作参数 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-10-11 下午7:00:25 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface OrderActivityActionParams {

	/**
	 * getOrderParams:(根据订单信息，生成不同的活动参数). <br/>
	 * TODO(不同项目活动参数不同).<br/>
	 * TODO(可根据项目需求改写项目所需参数).<br/>
	 * Date: 2015-10-11 下午7:02:12 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param orderInfo
	 * @return
	 */
	JSONObject getOrderParams(OrderInfo orderInfo,MemberExp memberExp);
	
	/**
	 * 常客订单参数
	 * @param memberExp
	 * @return
	 */
	List<JSONObject> getOftenParams(OrderInfo orderInfo,MemberExp memberExp);

}
