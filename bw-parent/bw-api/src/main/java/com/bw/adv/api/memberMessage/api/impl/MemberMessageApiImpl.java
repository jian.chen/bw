package com.bw.adv.api.memberMessage.api.impl;


import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.memberMessage.api.MemberMessageApi;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.member.message.model.MemberMessage;
import com.bw.adv.module.member.message.model.MemberMessageItem;
import com.bw.adv.module.member.message.model.MemberMessageItemBrowse;
import com.bw.adv.module.member.message.model.NotReadMessage;
import com.bw.adv.module.member.message.model.exp.MemberMessageExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.member.service.MemberMessageItemBrowseService;
import com.bw.adv.service.member.service.MemberMessageItemService;
import com.bw.adv.service.member.service.MemberMessageService;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MemberMessageApiImpl implements MemberMessageApi {
	
	private MemberMessageService memberMessageService;
	
	private MemberMessageItemService memberMessageItemService;
	
	private MemberMessageItemBrowseService memberMessageItemBrowseService;

	@Override
	public ApiResult queryMemberMessageByMemberCode(Long memberId, int page,
			int rows) {
		ApiResult result = null;
		Page<MemberMessageExp> pageObj = null;
		List<MemberMessageExp> memberList = null;
		
		try {
			pageObj = new Page<MemberMessageExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = new ApiResult();
			memberList = memberMessageService.queryListByMemberId(memberId, pageObj);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(memberList, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return result;
	}

	@Override
	public ApiResult queryMemberMessageDetail(Long memberMessageId,Long memberId) {
		ApiResult result = null;
		MemberMessage message = null;
		MemberMessageItem messageItem = null;
		Example example = null;
		Criteria criteria = null;
		
		try {
			example = new Example();
			criteria = example.createCriteria();
			criteria.andEqualTo("MEMBER_MESSAGE_ID", memberMessageId);
			criteria.andEqualTo("MEMBER_ID", memberId);
			result =new ApiResult();
			message = memberMessageService.queryByPk(memberMessageId);
			messageItem = memberMessageItemService.queryByMessageId(example);
			if(null==message){
				result = ApiUtil.getResult(result, ResultStatus.COMMON_PARAMS_ERROR);
				return result;
			}else{
				if("Y".equals(message.getIsAll())){
					if(messageItem==null){
						memberMessageItemService.insertObj(memberId, memberMessageId);
					}
				}else{
					messageItem.setIsRead("Y");
					messageItem.setReadTime(DateUtils.getCurrentTimeOfDb());
					memberMessageItemService.updateByPk(messageItem);
					
				}
				result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
				result.setData(ApiObjUtil.getApiResultMap(message, ApiUtil.getExtSystemId()));
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
			
		}
		return result;
	}

	@Override
	public ApiResult queryMemberMessage(Long memberMessageId) {
		ApiResult result = null;
		MemberMessage message = null;
		
		try {
			result =new ApiResult();
			message = memberMessageService.queryByPk(memberMessageId);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			result.setData(ApiObjUtil.getApiResultMap(message, ApiUtil.getExtSystemId()));
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
			
		}
		return result;
	}
	
	@Override
	public ApiResult queryMemberMessageItem(Long memberMessageId,Long memberId) {
		ApiResult result = null;
		MemberMessageItem messageItem = null;
		Example example = null;
		Criteria criteria = null;
		
		try {
			example = new Example();
			criteria = example.createCriteria();
			criteria.andEqualTo("MEMBER_MESSAGE_ID", memberMessageId);
			criteria.andEqualTo("MEMBER_ID", memberId);
			result =new ApiResult();
			messageItem = memberMessageItemService.queryByMessageId(example);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			result.setData(ApiObjUtil.getApiResultMap(messageItem, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return result;
	}
	@Override
	public ApiResult queryMemberMessageNotRead(Long memberId) {
		ApiResult result = null;
		NotReadMessage notReadMessage = null;
		
		try {
			result =new ApiResult();
			notReadMessage = memberMessageItemService.queryNotReadMessageByMemberId(memberId);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			result.setData(ApiObjUtil.getApiResultMap(notReadMessage, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		return result;
	}

	@Override
	public ApiResult addMemberMessageItemBrowse(String memberMessageId,
			String memberMessageItemId, String openId) {
		ApiResult result = null;
		MemberMessageItemBrowse memberMessageItemBrowse = null;

		try {
			result =new ApiResult();
			memberMessageItemBrowse = new MemberMessageItemBrowse();
			memberMessageItemBrowse.setBrowseTime(DateUtils.getCurrentTimeOfDb());
			memberMessageItemBrowse.setMemberMessageId(Long.parseLong(memberMessageId));
			memberMessageItemBrowse.setMemberMessageItemId(Long.parseLong(memberMessageItemId));
			memberMessageItemBrowse.setOpenId(openId);
			memberMessageItemBrowseService.save(memberMessageItemBrowse);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			result.setData(ApiObjUtil.getApiResultMap(memberMessageItemBrowse, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		return result;
	}
	
	@Autowired
	public void setMemberMessageService(MemberMessageService memberMessageService) {
		this.memberMessageService = memberMessageService;
	}


	@Autowired
	public void setMemberMessageItemService(
			MemberMessageItemService memberMessageItemService) {
		this.memberMessageItemService = memberMessageItemService;
	}
	
	@Autowired
	public void setMemberMessageItemBrowseService(
			MemberMessageItemBrowseService memberMessageItemBrowseService) {
		this.memberMessageItemBrowseService = memberMessageItemBrowseService;
	}

	
	
}
