package com.bw.adv.aop.service;

import net.sf.json.JSONObject;

import com.bw.adv.module.member.model.Member;


public interface PerfectActivityActionParams {

	public JSONObject getPerfectParams(Member member);
	
}
