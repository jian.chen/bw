package com.bw.adv.api.memberMessage.api;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bw.adv.api.result.ApiResult;

@Path("/message")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface MemberMessageApi {
	
	/**
	 * queryMemberMessageByMemberId:根据会员编号查询会员消息 <br/>
	 * Date: 2016年2月28日 下午10:02:37 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @param page
	 * @param rows
	 * @return
	 */
	@GET
	@Path("/queryMemberMessageByMemberId")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryMemberMessageByMemberCode(@MatrixParam("memberId")Long memberId,@MatrixParam("page")int page,@MatrixParam("rows")int rows); 
	
	/**
	 * queryMemberMessageDetail:查询消息详情(加明细)<br/>
	 * Date: 2016年2月28日 下午10:03:22 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	@GET
	@Path("/queryMemberMessageDetail")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryMemberMessageDetail(@MatrixParam("memberMessageId")Long memberMessageId,@MatrixParam("memberId")Long memberId);
	
	/**
	 * queryMemberMessage:查询消息详情<br/>
	 * Date: 2016年3月6日 下午9:00:35 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 * @param memberId
	 * @return
	 */
	@GET
	@Path("/queryMemberMessage")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryMemberMessage(@MatrixParam("memberMessageId")Long memberMessageId);
	
	/**
	 * queryMemberMessageItem:查询 会员消息<br/>
	 * Date: 2016年3月5日 下午5:10:01 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 * @param memberId
	 * @return
	 */
	@GET
	@Path("/queryMemberMessageItem")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryMemberMessageItem(@MatrixParam("memberMessageId")Long memberMessageId,@MatrixParam("memberId")Long memberId);
	/**
	 * queryMemberMessageNotRead:查询未读消息 <br/>
	 * Date: 2016年3月2日 下午5:51:15 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	@GET
	@Path("/queryMemberMessageNotRead")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryMemberMessageNotRead(@MatrixParam("memberId")Long memberId);
	
	/**
	 * addMemberMessageItemBrowse:插入消息点击记录 <br/>
	 * Date: 2016年3月4日 下午6:57:19 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 * @param memberMessageItemId
	 * @param openId
	 * @return
	 */
	@GET
	@Path("/addMemberMessageItemBrowse")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult addMemberMessageItemBrowse(@MatrixParam("memberMessageId")String memberMessageId,@MatrixParam("memberMessageItemId")String memberMessageItemId,@MatrixParam("openId")String openId);
}
