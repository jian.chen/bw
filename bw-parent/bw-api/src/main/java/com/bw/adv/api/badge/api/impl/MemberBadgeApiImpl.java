package com.bw.adv.api.badge.api.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.api.badge.api.MemberBadgeApi;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.badge.model.MemberBadge;
import com.bw.adv.module.badge.model.MemberBadgeItem;
import com.bw.adv.module.badge.model.exp.MemberBadgeExt;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.goods.model.GoodsExchangeItem;
import com.bw.adv.module.goods.model.exp.GoodsShowExp;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.service.badge.service.MemberBadgeItemSevice;


public class MemberBadgeApiImpl implements MemberBadgeApi{

	private static final Logger LOGGER = Logger.getLogger(MemberBadgeApiImpl.class);
	
	@Autowired
	private MemberBadgeItemSevice memberBadgeItemSevice;
	
	@Autowired
	private MemberRepository memberRepository;
	
	@Override
	public ApiResult getMemberBadgeInfo(String json) {
		ApiResult result = null;
		Member member=null;
		try {
			JSONObject jo = JSONObject.fromObject(json);
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null ;
			result=new ApiResult();
			member = memberRepository.findByPk(memberId);
			if(member==null){
				result = ApiUtil.getResult(result,ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}else{
				List<MemberBadgeExt> memberBadges= memberBadgeItemSevice.queryMemberBadgeExtByMemberId(memberId);
				result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
				result.setItems(ApiObjUtil.getApiResultItems(memberBadges, ApiUtil.getExtSystemId()));
			}
		} catch (Exception e) {
			ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
		}
		return result;
	
	}
	@Override
	public ApiResult getMemberBadgeExchangeInfo(String json) {
		ApiResult result = null;
		List<GoodsExchangeItem> queryResult = null;
		Page<GoodsExchangeItem> pageObj = null;
		
		try {
			JSONObject jo = JSONObject.fromObject(json);
			result = new ApiResult();
			Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null ;
			int pages = jo.containsKey("pages") ? jo.getInt("pages") : null;
			int rows = jo.containsKey("rows") ? jo.getInt("rows") : null;
			pageObj = new Page<GoodsExchangeItem>();
			pageObj.setPageNo(pages);
			pageObj.setPageSize(rows);
			//queryResult = goodsExchangeRepository.findByMemberId(memberId, pageObj);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(queryResult, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	@Override
	public ApiResult exchangeBadgeProductDetail(String json) {
		ApiResult result = null;
		GoodsShowExp goodsShowExp = null;
		GoodsExchangeItem goodsExchangeItem = null;
		Example example = null;
		String currentTime = null;
		MemberAccount memberAccount = null;
		MemberExp member = null;
//		//String lock = "lock";
//			try {
//				currentTime = DateUtils.getCurrentTimeOfDb();
//				JSONObject jo = JSONObject.fromObject(json);
//				result = new ApiResult();
//				Long memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null ;
//				Long goodsShowId = jo.containsKey("goodsShowId") ? jo.getLong("goodsShowId") : null ;
//				String source = jo.containsKey("source") ? jo.getString("source") : null ;
//				example = new Example();                                         
////				member = memberRepository.findExpByMemberId(memberId);
////				Criteria criteria =  example.createCriteria();                           
////				criteria.andEqualTo("gs.GOODS_SHOW_ID", goodsShowId);
////				criteria.andEqualTo("gcg.MEMBER_CARD_GRADE_ID", member.getGradeId());
////				synchronized (BadgeApiImpl.class) {
////				goodsShowExp = goodsShowRepository.findExpByExample(example);
////				// 验证会员等级是否满足兑换商品所需等级
////				if (member.getGradeId() < goodsShowExp.getRequireLevel()) {
////					return ApiUtil.getResult(result, ResultStatus.GRADE_NOT_ENOUGH);
////				}
////				Coupon coupon = couponRepository.findByPk(goodsShowExp.getCouponId());
////				ResultStatus resultStatus = couponIssueService.checkCoupon(coupon, 1);
////				if (resultStatus != ResultStatus.SUCCESS) {
////					return ApiUtil.getResult(result, ResultStatus.COUPON_INSTANCE_STATUS_EXPIRE_ERROR);
////				}
////				goodsExchangeItem = new GoodsExchangeItem();
////				goodsExchangeItem.setCreateTime(currentTime);
////				goodsExchangeItem.setGoodsShowId(goodsShowId);
////				goodsExchangeItem.setMemberId(memberId);
////				goodsExchangeItem.setStatusId(1l);
////				goodsExchangeItem.setBadgeNumber(goodsShowExp.getBadgeNumber());
//				goodsExchangeItem.setRemark("兑换"+goodsShowExp.getCouponName());
//	//			goodsExchangeRepository.saveSelective(goodsExchangeItem);
//	//			memberAccount = memberService.queryMemberAccountByMemberId(memberId);
//	//			memberAccount.setBadgeBalance(memberAccount.getBadgeBalance().divide(goodsShowExp.getBadgeNumber()));
//	//			memberAccountRepository.updateByPkSelective(memberAccount);
//					goodsShowService.exchangeBadgeProduct(goodsExchangeItem, goodsShowExp.getCouponId(), source);
//				}
//				result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
//			} catch (MemberBadgeItemException e) {
//				e.printStackTrace();
//				result = ApiUtil.getResult(result,e.getResultStatus());
//			} catch (CouponException e){
//				LOGGER.error("coupon num no enough", e);
//				result = ApiUtil.getResult(result,ResultStatus.COUPON_NOT_ENOUGH);
//			}
//			catch (Exception e) {
//				e.printStackTrace();
//				result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
//			}
		return result;
		
	}

	@Override
	public ApiResult queryBadgeProductList(com.alibaba.fastjson.JSONObject params) {
		String pageString = params.getString("pages");
		String rowsString = params.getString("rows");
		Long memberId = params.getLong("memberId");
		String isExt = params.getString("external");
		int pages = StringUtils.isBlank(pageString) ? 1 : Integer.valueOf(pageString);
		int rows = StringUtils.isBlank(rowsString) ? 10 :
					Integer.valueOf(rowsString) > 20 ? 20 : Integer.valueOf(rowsString);
		List<GoodsShowExp> queryResult = null;
		ApiResult result = new ApiResult();
		try {
			Page<GoodsShowExp> pageObj = new Page<GoodsShowExp>();
			pageObj.setPageNo(pages);
			pageObj.setPageSize(rows);
//			if (isExt.equals("Y")){
//				queryResult = goodsShowRepository.findExtBadgeProductByStatus(StatusConstant.GOODS_SHOW_ENABLE.getId(), pageObj);
//			} else {
//				queryResult = goodsShowRepository.findBadgeProductByStatus(StatusConstant.GOODS_SHOW_ENABLE.getId(), pageObj);
//			}
			this.groupGoodsShowMemberCardGrade(queryResult , memberId);
			Grade grade = null;
			for (GoodsShowExp goodsShowExp : queryResult) {
				if (goodsShowExp.getRequireLevel() != null && (grade = GradeInit.getGradeByPk(goodsShowExp.getRequireLevel())) != null) {
					goodsShowExp.setRequireLevelRemark(grade.getGradeName());
				}
			}
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(queryResult, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
			LOGGER.error("BadgeApi[queryBadgeProductList] error, with params [" + params + "]", e);
		}
		return result;
	}

	
	
	
	/**
	 * groupGoodsShowMemberCardGrade:(将获取到的结果分组). <br/>
	 * Date: 2016年7月25日 上午11:54:44 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param queryResult
	 */
	private void groupGoodsShowMemberCardGrade(List<GoodsShowExp> queryResult , Long memberId){
		
//		if(queryResult == null || queryResult.size() == 0 || memberId == null){
//			return;
//		}
//		MemberExp member = memberRepository.findExpByMemberId(memberId);
//		if(member == null){
//			return;
//		}
//		Map<Long, GoodsShowExp> map = new HashMap<Long, GoodsShowExp>();
//		for(GoodsShowExp goods : queryResult){
//			map.put(goods.getGoodsShowId(), goods);
//		}
//		Set<Long> goodsIds =  map.keySet();
//	    List<GoodsShowMemberCardGrade> goodsShowMemberCardGradeList = goodsShowMemberCardGradeMapper.selectGoodsShowMemberCardGradeListByIds(goodsIds);
//	    if(goodsShowMemberCardGradeList != null && goodsShowMemberCardGradeList.size() > 0){
//	    	for(Long goodsId :map.keySet()){
//	    		Map<Long, BigDecimal> pointMap = new HashMap<Long, BigDecimal>();
//		    	for(GoodsShowMemberCardGrade goodsShowMemberCardGrade : goodsShowMemberCardGradeList){
//		    		if(goodsId.equals(goodsShowMemberCardGrade.getGoodsShowId())){
//		    			pointMap.put(goodsShowMemberCardGrade.getMemberCardGradeId(), goodsShowMemberCardGrade.getBadgeNumber());
//		    		}
//		    	}
//		    	this.showRule(map.get(goodsId), member, pointMap);
//	    	}
//	    }
	}
	
	
	
	/**
	 * showRule:(显示规则). <br/>
	 * Date: 2016年7月26日 上午10:15:36 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param queryResult
	 * @param member
	 * @param pointMap
	 */
	private void showRule(GoodsShowExp queryResult , MemberExp member , Map<Long, BigDecimal> pointMap){
		
		if(queryResult == null || member == null ){
			return;
		}
		
//		//规则如果当前会员为绿卡，则文案显示金卡及以上。否则显示绿卡文案
//		if(GoodsShowGradeTypeEnums.No_1.getCode().equals(member.getGradeId())){
//			if(pointMap.get(GoodsShowGradeTypeEnums.No_1.getCode()) != null){
//				queryResult.setRequireBadge(pointMap.get(GoodsShowGradeTypeEnums.No_1.getCode()));
//			}
//			
//			if(pointMap.get(GoodsShowGradeTypeEnums.No_2.getCode()) != null){
//				queryResult.setShowArchives(GoodsShowGradeTypeEnums.No_1.getDesc().replace("XX",pointMap.get(GoodsShowGradeTypeEnums.No_2.getCode()).toString()));
//				queryResult.setShowArchivesEn(GoodsShowGradeTypeEnums.No_1_En.getDesc().replace("XX",pointMap.get(GoodsShowGradeTypeEnums.No_2_En.getCode()).toString()));
//			}
//			
//		}else{
//			if(pointMap.get(GoodsShowGradeTypeEnums.No_2.getCode()) != null){
//				queryResult.setRequireBadge(pointMap.get(GoodsShowGradeTypeEnums.No_2.getCode()));
//			}
//			
//			if(pointMap.get(GoodsShowGradeTypeEnums.No_1.getCode()) != null){
//				queryResult.setShowArchives(GoodsShowGradeTypeEnums.No_2.getDesc().replace("XX", pointMap.get(GoodsShowGradeTypeEnums.No_1.getCode()).toString()));
//				queryResult.setShowArchivesEn(GoodsShowGradeTypeEnums.No_2_En.getDesc().replace("XX", pointMap.get(GoodsShowGradeTypeEnums.No_1_En.getCode()).toString()));
//			}
//		}
		
	}
	
	
	
	
	
	
	@Override
	public ApiResult getBadgeProduct(String json) {
		ApiResult result = null;
		List<GoodsShowExp> queryResult = null;
		Page<GoodsShowExp> pageObj = null;
		Long memberId = null;
		MemberAccount memberAccount = null;
		try {
			JSONObject jo = JSONObject.fromObject(json);
			result = new ApiResult();
			memberId = jo.containsKey("memberId") ? jo.getLong("memberId") : null ;
			int pages = jo.containsKey("pages") ? jo.getInt("pages") : null;
			int rows = jo.containsKey("rows") ? jo.getInt("rows") : null;
			
			//memberAccount = memberService.queryMemberAccountByMemberId(memberId);
			pageObj = new Page<GoodsShowExp>();
			pageObj.setPageNo(pages);
			pageObj.setPageSize(rows);
			//queryResult = goodsShowRepository.findBadgeProductByStatus(StatusConstant.GOODS_SHOW_ENABLE.getId(), pageObj);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(queryResult, ApiUtil.getExtSystemId()));
			result.setData(ApiObjUtil.getApiResultMap(memberAccount, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public ApiResult queryBadgeProductDetail(Long goodsShowId,Long gradeId) {
		ApiResult result = null;
		GoodsShowExp goodsShowExp = null;
		Example example = null;
		try {
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();                           
			criteria.andEqualTo("gs.GOODS_SHOW_ID", goodsShowId);
			criteria.andEqualTo("gcg.MEMBER_CARD_GRADE_ID", gradeId);
//			goodsShowExp = goodsShowRepository.findExpByExample(example);
//			Grade grade = null;
//			if (goodsShowExp.getRequireLevel() != null && (grade = GradeInit.getGradeByPk(goodsShowExp.getRequireLevel())) != null) {
//				goodsShowExp.setRequireLevelRemark(grade.getGradeName());
//			}
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			result.setData(ApiObjUtil.getApiResultMap(goodsShowExp, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
			
		}
		return result;
	}

	@Override
	public ApiResult getMemberBadgeList(com.alibaba.fastjson.JSONObject memberBadgeParams) {
		if (memberBadgeParams == null || !memberBadgeParams.containsKey("memberCode")
				|| !memberBadgeParams.containsKey("pageNo") || !memberBadgeParams.containsKey("pageSize")) {
			return ApiUtil.getResult(new ApiResult(), ResultStatus.COMMON_PARAMS_ERROR);
		}
		ApiResult result = null;
//		List<MemberBadgeItemStoreExp> memberBadgeItemStoreExps = null;
//		try {
//			memberBadgeItemStoreExps = memberBadgeItemSevice.selectStoreExpByMemberCode(
//					memberBadgeParams.getString("memberCode"), memberBadgeParams.getIntValue("pageSize"),
//					memberBadgeParams.getIntValue("pageNo"));
//			result = new ApiResult();
//			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
//			result.setItems(ApiObjUtil.getApiResultItems(memberBadgeItemStoreExps, ApiUtil.getExtSystemId()));
//		} catch (Exception e) {
//			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
//			e.printStackTrace();
//		}
		return result;
	}
}
