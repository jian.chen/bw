package com.bw.adv.api.order.validtion;

import com.bw.adv.module.order.model.dto.OrderHeaderDto;
import com.bw.adv.module.order.model.dto.OrderInfo;


/**
 * ClassName: 订单解析 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-16 下午6:33:28 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class OrderParse extends OrderParseSupport{

	@Override
	public OrderHeaderDto parseOrderBefore(OrderHeaderDto orderHeaderDto) {
		return orderHeaderDto;
	}

	@Override
	public OrderInfo parseOrderAfter(OrderInfo orderInfo) {
		return orderInfo;
	}
	
}
