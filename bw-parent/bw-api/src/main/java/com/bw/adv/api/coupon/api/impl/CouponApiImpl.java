package com.bw.adv.api.coupon.api.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.coupon.api.CouponApi;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.api.utils.ApiObjUtil;
import com.bw.adv.api.utils.ApiUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.repository.StoreRepository;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.coupon.exception.CouponException;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.CouponCategory;
import com.bw.adv.module.coupon.model.exp.CouponCategoryItemExp;
import com.bw.adv.module.coupon.model.exp.CouponExp;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.coupon.model.exp.CouponRangeExp;
import com.bw.adv.module.coupon.model.exp.CouponResult;
import com.bw.adv.module.coupon.repository.CouponInstanceRepository;
import com.bw.adv.module.coupon.repository.CouponRangeRepository;
import com.bw.adv.module.member.constant.ExtAccountTypeConstant;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.service.base.service.StoreService;
import com.bw.adv.service.coupon.service.CouponCategoryService;
import com.bw.adv.service.coupon.service.CouponInstanceService;
import com.bw.adv.service.coupon.service.CouponIssueService;
import com.bw.adv.service.coupon.service.CouponService;
import com.bw.adv.service.member.service.MemberService;

/**
 * ClassName: CouponApiImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月19日 上午10:33:08 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public class CouponApiImpl implements CouponApi {
	
	
	private static final Logger logger = Logger.getLogger(CouponApiImpl.class);
	
	private CouponService couponService;
	
	private CouponInstanceService couponInstanceService;
	
	private CouponInstanceRepository couponInstanceRepository;
	
	private StoreRepository storeRepository;
	
	private CouponRangeRepository couponRangeRepository;
	
	private CouponCategoryService couponCategoryService;
	
	private MemberService memberService;
	
	private CouponIssueService couponIssueService;
	
	private StoreService storeService;
	
	@Override
	public ApiResult queryCouponInstanceByMemberCode(String type,String memberCode,String statusId,String source,int page,int rows) {
		List<CouponInstanceExp> memberList = null;
		ApiResult result = null;
		Example example = null;
		Page<CouponInstanceExp> pageobj = null;
		String currentDate = null;
		List<Object> getCoupon = null;
		getCoupon = new ArrayList<Object>();
		getCoupon.add(ChannelConstant.GET_COUPON_HANDLE.getId());
		getCoupon.add(ChannelConstant.GET_COUPON_ALIPAY_HANDLE.getId());
		currentDate = DateUtils.formatCurrentDate("yyyyMMdd");
		List<Object> statusIdList = null;
		try {
			pageobj = new Page<CouponInstanceExp>();
			pageobj.setPageNo(page);
			pageobj.setPageSize(rows);
			result = new ApiResult();
			example = new Example();
			statusIdList = new ArrayList<Object>();
			Criteria criteria =  example.createCriteria();
			criteria.andEqualTo("mem.MEMBER_CODE", memberCode);
			criteria.andNotEqualTo("mem.IS_EMPLOYEE", "Y");
			if(null!=source && source.equals("alipay")){
				criteria.andEqualTo("ext.EXT_ACCOUNT_TYPE_ID", ExtAccountTypeConstant.ACCOUNT_TYPE_ALIPAY.getId());
			}else {
				criteria.andEqualTo("ext.EXT_ACCOUNT_TYPE_ID", ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getId());
			}
			if (StringUtils.isNotBlank(type)) {
				if("1".endsWith(type)){
					criteria.andNotIn("couin.GET_CHANNEL_ID", getCoupon);
				}else if("2".endsWith(type)){
					criteria.andIdIn("couin.GET_CHANNEL_ID", getCoupon);
				}
			}
			criteria.andNotEqualTo("couin.STATUS_ID", StatusConstant.COUPON_INSTANCE_SHARE.getId());
			criteria.andNotEqualTo("couin.STATUS_ID", StatusConstant.COUPON_INSTANCE_FFING.getId());
			if(statusId.equals("EXPIRE")){
				criteria.andNotEqualTo("couin.STATUS_ID", StatusConstant.COUPON_INSTANCE_USED.getId());
				criteria.andLessThan("couin.EXPIRE_DATE", currentDate);
			}else if(statusId.equals("USED")){
				statusIdList.add(StatusConstant.COUPON_INSTANCE_USED.getId());
				statusIdList.add(StatusConstant.COUPON_INSTANCE_FREEZE.getId());
				criteria.andIdIn("couin.STATUS_ID", statusIdList);
			}else if(statusId.equals("UNUSED")){
				criteria.andEqualTo("couin.STATUS_ID", StatusConstant.COUPON_INSTANCE_UNUSED.getId());
				criteria.andGreaterThanOrEqualTo("couin.EXPIRE_DATE", currentDate);
			}
			if(StringUtils.isNotBlank(source) && source.equals("air")){
				criteria.andNotEqualTo("cou.is_owner_used", "N");
			}
			memberList = couponInstanceRepository.findCouponInstanceListByDyc(example,pageobj);
//			if(memberList == null || memberList.isEmpty()){
//				result = ApiUtil.getResult(result,ResultStatus.COUPONINSTANCE_NULL_ERROR);
//			}else{
				result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
				result.setItems(ApiObjUtil.getApiResultItems(memberList, ApiUtil.getExtSystemId()));
//			}
		} catch (Exception e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult queryCouponInstanceByBindingAccount(String bindingAccount,String extAccountType,int page,int rows) {
		List<CouponInstanceExp> memberList = null;
		ApiResult result = null;
		Example example = null;
		Page<CouponInstanceExp> pageobj = null;
		try {
			pageobj = new Page<CouponInstanceExp>();
			pageobj.setPageNo(page);
			pageobj.setPageSize(rows);
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();                           
			criteria.andEqualTo("ext.EXT_ACCOUNT_TYPE_ID", extAccountType);
			criteria.andEqualTo("ext.BINDING_ACCOUNT",bindingAccount);
			memberList = couponInstanceRepository.findCouponInstanceListByDyc(example,pageobj);
			if(memberList == null || memberList.isEmpty()){
				result = ApiUtil.getResult(result,ResultStatus.COUPONINSTANCE_NULL_ERROR);
			}else{
				result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
				result.setItems(ApiObjUtil.getApiResultItems(memberList, ApiUtil.getExtSystemId()));
			}
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult queryCouponInstanceDetail(String couponInstanceCode,String source) {
		ApiResult result = null;
		CouponInstanceExp couponInstanceExp = null;
		Example example = null;
		try {
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();                           
			criteria.andEqualTo("couin.COUPON_INSTANCE_CODE", couponInstanceCode);
			if(null!=source && source.equals("alipay")){
				criteria.andEqualTo("ext.EXT_ACCOUNT_TYPE_ID", ExtAccountTypeConstant.ACCOUNT_TYPE_ALIPAY.getId());
			}else {
				criteria.andEqualTo("ext.EXT_ACCOUNT_TYPE_ID", ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getId());
			}
			couponInstanceExp = couponInstanceRepository.findCouponInstanceExpByExample(example);
			if(couponInstanceExp==null){
				result = ApiUtil.getResult(result,ResultStatus.COUPONINSTANCE_NULL_ERROR);
			}else{
				result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
				result.setData(ApiObjUtil.getApiResultMap(couponInstanceExp, ApiUtil.getExtSystemId()));
			}
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
			
		}
		return result;
	}
	

	/**
	 * TODO 查询优惠券信息并验证优惠券是否可用
	 * @see com.bw.adv.api.coupon.api.CouponApi#queryCouponInstanceDetailAndValidate(java.lang.String)
	 */
	public ApiResult queryCouponInstanceDetailAndValidate(String couponInstanceCode) {
		ApiResult result = null;
		CouponInstanceExp couponInstanceExp = null;
		Example example = null;
		try {
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();                           
			criteria.andEqualTo("couin.COUPON_INSTANCE_CODE", couponInstanceCode);
			couponInstanceExp = couponInstanceService.queryCouponInstanceExpByExample(example);
			if(couponInstanceExp==null){
				result = ApiUtil.getResult(result,ResultStatus.COUPONINSTANCE_NULL_ERROR);
			}else{
				//是否过期
				if(StringUtils.isNotBlank(couponInstanceExp.getExpireDate()) && couponInstanceExp.getExpireDate().compareTo(DateUtils.getCurrentDateOfDb()) < 0){
					result = ApiUtil.getResult(result,ResultStatus.COUPON_INSTANCE_STATUS_EXPIRE_ERROR);
				}//是否使用
				else if(couponInstanceExp.getStatusId().equals(StatusConstant.COUPON_INSTANCE_USED.getId())){
					result = ApiUtil.getResult(result,ResultStatus.COUPON_INSTANCE_STATUS_USED_ERROR);
				}//是否作废
				else if(couponInstanceExp.getStatusId().equals(StatusConstant.COUPON_INSTANCE_DESTROY.getId())){
					result = ApiUtil.getResult(result,ResultStatus.COUPON_INSTANCE_STATUS_DESTROY_ERROR);
				}else{
					result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
					result.setData(ApiObjUtil.getApiResultMap(couponInstanceExp, ApiUtil.getExtSystemId()));
				}
			}
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	@Transactional
	public ApiResult useCouponInstance(String couponInstanceCode,String storeCode){
		ApiResult result = null;
		Example example = null;
		CouponInstanceExp couponInstanceExp = null;
		Coupon coupon = null;
		JsonModel jsonModel = null;
		try {
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();                           
			criteria.andEqualTo("couin.COUPON_INSTANCE_CODE", couponInstanceCode);
			couponInstanceExp = couponInstanceRepository.findCouponInstanceExpByExample(example);
			jsonModel = scrmcheckCouponInstance(couponInstanceExp,storeCode);
			if(jsonModel.getStatus()==1){
				Store store = storeRepository.findByStoreCode(storeCode);
				couponInstanceExp.setStatusId(StatusConstant.COUPON_INSTANCE_USED.getId());
				couponInstanceExp.setUpdateTime(DateUtils.getCurrentTimeOfDb());
				couponInstanceExp.setUsedTime(DateUtils.getCurrentTimeOfDb());
				couponInstanceExp.setStoreId(store.getStoreId());
				couponInstanceService.updateByPkSelective(couponInstanceExp);//couponInstance
				coupon = couponService.queryByPk(couponInstanceExp.getCouponId());
				if(coupon.getUsedQuantity() == null){
					coupon.setUsedQuantity(1L);
				}else{
					coupon.setUsedQuantity(coupon.getUsedQuantity()+1L);
				}
				couponService.updateByPkSelective(coupon);
				result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
				result.setData(ApiObjUtil.getApiResultMap(couponInstanceExp, ApiUtil.getExtSystemId()));
				return result;
			}else{
				result = new ApiResult();
				result.setCode(jsonModel.getStatus()+"");
				result.setMessage(jsonModel.getMessage());
			}
		
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
			
		}
		return result;
	}
	
	@Override
	@Transactional
	public ApiResult useCouponInstanceList(String couponInstanceCode,String storeCode){
		ApiResult result = null;
		try {
			result = new ApiResult();
			List<String> couponInstanceCodesList = JSON.parseArray(couponInstanceCode, String.class);
			couponService.verificationCoupon(couponInstanceCodesList, storeCode, "");
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult showShopCheckCount(String json) {
		ApiResult result = null;
		List<CouponInstanceExp> countList = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			result = new ApiResult();
			JSONObject jo = JSONObject.parseObject(json);
			String storeCode = jo.containsKey("storeCode") ? jo.getString("storeCode") : null;
			String date = jo.containsKey("date") ? jo.getString("date") : null;
			countList = couponInstanceService.queryShopCheckout(storeCode, date);
			map.put("countList", countList);
			result.setData(map);
			result.setCode("200");
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	@Transactional
	public ApiResult reverseUseCouponInstance(String couponInstanceCode){
		
		ApiResult result = null;
		Example example = null;
		CouponInstanceExp couponInstanceExp = null;
		Coupon coupon = null;
		
		try {
			
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();                           
			criteria.andEqualTo("COUPON_INSTANCE_CODE", couponInstanceCode);
			couponInstanceExp = couponInstanceRepository.findCouponInstanceExpByExample(example);
			
			if(couponInstanceExp == null){
				result = ApiUtil.getResult(result, ResultStatus.COUPONINSTANCE_CODE_NULL_ERROR);
				logger.debug("ip:"+ApiUtil.getIp()+","+
						"couponInstanceCode:"+couponInstanceCode+ ","+
						"resultCode:"+result.getCode() + ","+
						"resultMsg:"+result.getMessage());
				return result;
			}
			if(StatusConstant.COUPON_INSTANCE_CREATE.getId().equals(couponInstanceExp.getStatusId())){
				result = ApiUtil.getResult(result, ResultStatus.COUPON_INSTANCE_REVERSE_CREATE_ERROR);
				logger.debug("ip:"+ApiUtil.getIp()+","+
						"couponInstanceCode:"+couponInstanceCode+ ","+
						"resultCode:"+result.getCode() + ","+
						"resultMsg:"+result.getMessage());
				return result;
			}else if(StatusConstant.COUPON_INSTANCE_UNUSED.getId().equals(couponInstanceExp.getStatusId())){
				result = ApiUtil.getResult(result, ResultStatus.COUPON_INSTANCE_REVERSE_UNUSED_ERROR);
				logger.debug("ip:"+ApiUtil.getIp()+","+
						"couponInstanceCode:"+couponInstanceCode+ ","+
						"resultCode:"+result.getCode() + ","+
						"resultMsg:"+result.getMessage());
				return result;
			}
			//修改优惠券实例
			couponInstanceExp.setStatusId(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
			couponInstanceExp.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			couponInstanceExp.setUsedTime("");
			couponInstanceService.updateByPkSelective(couponInstanceExp);
			
			coupon = couponService.queryByPk(couponInstanceExp.getCouponId());
			
			if(coupon.getUsedQuantity()==null){
				coupon.setUsedQuantity(0L);
			}else{
				coupon.setUsedQuantity(coupon.getUsedQuantity() - 1L);
			}
			couponService.updateByPkSelective(coupon);
			
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		
		logger.debug("ip:"+ApiUtil.getIp()+","+
				"couponInstanceCode:"+couponInstanceCode+ ","+
				"resultCode:"+result.getCode() + ","+
				"resultMsg:"+result.getMessage());
		return result;
		
	}
	
	public JsonModel scrmcheckCouponInstance(CouponInstanceExp couponInstance ,String storeCode) {
		JsonModel jsonModel = null;
		String currentDate = null;
		List<CouponRangeExp> couponRanges = null;
		Store store = null;
		
		store = storeRepository.findByStoreCode(storeCode);
		//校验券是否存在
		if (couponInstance == null) {
			jsonModel = new JsonModel(6004001,"优惠券核销失败："+ResultStatus.COUPONINSTANCE_CODE_NULL_ERROR.getMsg());
			return jsonModel;
		}
		// 使用和作废都只能是未使用或使用中状态下的优惠券实例
		if(StatusConstant.COUPON_INSTANCE_USED.getId().equals(couponInstance.getStatusId())) {
			jsonModel = new JsonModel(6004003,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_USED_ERROR.getMsg());
			return jsonModel;
        }
		if(StatusConstant.COUPON_INSTANCE_EXPIRE.getId().equals(couponInstance.getStatusId())) {
			jsonModel = new JsonModel(6004005,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_EXPIRE_ERROR.getMsg());
			return jsonModel;
        }
		if(StatusConstant.COUPON_INSTANCE_DESTROY.getId().equals(couponInstance.getStatusId())) {
			jsonModel = new JsonModel(6004004,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_DESTROY_ERROR.getMsg());
			return jsonModel;
        }
		//校验券是否过期
		currentDate = DateUtils.formatCurrentDate("yyyyMMdd");
		if (currentDate.compareTo(couponInstance.getExpireDate()) > 0) {
			jsonModel = new JsonModel(6004007,"优惠券核销失败："+ResultStatus.COUPON_NOT_AVALIABLE.getMsg());
			return jsonModel;
		}
		if(StringUtils.isNotBlank(couponInstance.getStartDate()) && currentDate.compareTo(couponInstance.getStartDate()) < 0) {
			jsonModel = new JsonModel(6004008,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_NOT_BEGIN_DATE.getMsg());
			return jsonModel;
		}
		
		//校验券的门店
		couponRanges = couponRangeRepository.findByCouponId(couponInstance.getCouponId());
		if(!couponRanges.isEmpty()){
			if(store == null || store.getOrgId()==null){
//				logger.error("优惠券核销失败："+ResultStatus.COUPON_STORE_ERROR.getMsg());
				jsonModel = new JsonModel(6004009,"优惠券核销失败："+ResultStatus.COUPON_ORG_NULL_ERROR.getMsg());
				return jsonModel;
			}
			for(int i = 0; i < couponRanges.size(); i++){
				if(store.getOrgId().equals(couponRanges.get(i).getOrgId())){
					return new JsonModel(1,"优惠券核销成功!");
				}
			}
//			logger.error("优惠券核销失败："+ResultStatus.COUPON_ORG_NULL_ERROR.getMsg());
//			return false;
			jsonModel = new JsonModel(6004010,"优惠券核销失败："+ResultStatus.COUPON_ORG_NULL_ERROR.getMsg());
			return jsonModel;
		}
		jsonModel = new JsonModel(1,"优惠券核销成功!");
		return jsonModel;
	}

	@Override
	public ApiResult queryCouponCategorys(String statusId,int page,int rows) {
		List<CouponCategory> couponCategoryList = null;
		ApiResult result = null;
		Example example = null;
		Page<CouponCategory> pageobj = null;
		try {
			pageobj = new Page<CouponCategory>();
			pageobj.setPageNo(page);
			pageobj.setPageSize(rows);
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();    
			criteria.andEqualTo("STATUS_ID", StatusConstant.COUPON_CATEGORY_ENABLE.getId());
			couponCategoryList = couponCategoryService.queryCouponCategoryPage(example,pageobj);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(couponCategoryList, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult queryCouponByCategoryId(String couponCategoryId){
		List<CouponExp> couponInstanceList = null;
		ApiResult result = null;
		Example example = null;
		try {
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();                           
			criteria.andEqualTo("cc.coupon_category_id", couponCategoryId);
			criteria.andEqualTo("cci.status_id", StatusConstant.COUPON_CATEGORY_ITEM_ENABLE.getId());
			couponInstanceList = couponService.queryCouponByCategoryID(example);
			result = ApiUtil.getResult(result,ResultStatus.SUCCESS);
			result.setItems(ApiObjUtil.getApiResultItems(couponInstanceList, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult queryCouponCategoryDetail(String couponId){
		ApiResult result = null;
		Coupon coupon = null;
		Example example = null;
		try {
			result = new ApiResult();
			example = new Example();                                         
			Criteria criteria =  example.createCriteria();                           
			criteria.andEqualTo("couin.COUPON_ID", couponId);
			coupon = couponService.queryByPk(couponId);
			if(coupon==null){
				result = ApiUtil.getResult(result,ResultStatus.COUPONINSTANCE_NULL_ERROR);
			}else{
				result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
				result.setData(ApiObjUtil.getApiResultMap(coupon, ApiUtil.getExtSystemId()));
			}
		} catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult getCouponCategory(String couponId, String memberCode,String source) {
		ApiResult result = null;
		CouponResult couponResult = null;
		List<CouponResult> couponList = null;
		List<CouponCategoryItemExp> couponCategoryItemList = null;
		Member member = null;
		couponResult = new CouponResult();
		couponResult.setCouponId(Long.valueOf(couponId));
		couponResult.setQuantity(1);
		couponList = new ArrayList<CouponResult>();
		couponList.add(couponResult);
		Long sendCouponId = null;
		try {
			result = new ApiResult();
			if(StringUtils.isBlank(couponId) || StringUtils.isBlank(memberCode)){
				return ApiUtil.getResult(result,ResultStatus.COMMON_PARAMS_ERROR);
			}
			
			sendCouponId = Long.valueOf(couponId);
			
			couponCategoryItemList = this.couponCategoryService.queryListByCouponId(Long.valueOf(couponId));
			if(couponCategoryItemList == null || couponCategoryItemList.size() < 1){
				return ApiUtil.getResult(result, ResultStatus.COUPONINSTANCE_INVALID);
			}
			
			member = memberService.queryMemberByCode(memberCode);
			if(null != member){
				if(null!=source && source.equals("alipay")){
					couponIssueService.insertCouponIssueSingle(member.getMemberId(), sendCouponId, ChannelConstant.GET_COUPON_ALIPAY_HANDLE.getId(), null,WechatMsgBusinessTypeEnum.GET_COUPON_RECEIVE.getId());
				}else{
					couponIssueService.insertCouponIssueSingle(member.getMemberId(), sendCouponId, ChannelConstant.GET_COUPON_HANDLE.getId(), null,WechatMsgBusinessTypeEnum.GET_COUPON_RECEIVE.getId());
				}
				result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			}else{
				result = ApiUtil.getResult(result, ResultStatus.MEMBER_NOT_EXIST_ERROR);
			}
		}catch (CouponException e) {
			result = ApiUtil.getResult(result,e.getResultStatus());
			e.printStackTrace();
		}catch (Exception e) {
			result = ApiUtil.getResult(result,ResultStatus.SYSTEM_ERROR);
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public ApiResult queryMemberByExtAccount(String bindingAccount,String extAccountType) {
		ApiResult result = null;
		Member member = null;
		
		try {
			result = new ApiResult();
			member = memberService.queryMemberByBindingAccount(extAccountType, bindingAccount);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
			result.setData(ApiObjUtil.getApiResultMap(member, ApiUtil.getExtSystemId()));
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return result;
	}
	@Override
	public ApiResult exchangeCoupon(String memberId, String exchangeCode) {
		ApiResult result = null;
		try {
			result = new ApiResult();
			couponInstanceService.updateCouponInstanceByExchangeCode(Long.valueOf(memberId), exchangeCode);
			result = ApiUtil.getResult(result, ResultStatus.SUCCESS);
		} catch (CouponException e) {
			e.printStackTrace();
			ResultStatus resultStatus = new ResultStatus(ResultStatus.EXCHANGE_ERROR.getCode(), e.getMessage());
			result = ApiUtil.getResult(result, resultStatus);
		}catch (Exception e) {
			e.printStackTrace();
			result = ApiUtil.getResult(result, ResultStatus.SYSTEM_ERROR);
		}
		return result;
	}


	@Autowired
	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

	@Autowired
	public void setCouponInstanceService(CouponInstanceService couponInstanceService) {
		this.couponInstanceService = couponInstanceService;
	}

	@Autowired
	public void setCouponInstanceRepository(
			CouponInstanceRepository couponInstanceRepository) {
		this.couponInstanceRepository = couponInstanceRepository;
	}
	
	@Autowired
	public void setStoreRepository(StoreRepository storeRepository) {
		this.storeRepository = storeRepository;
	}
	
	@Autowired
	public void setCouponRangeRepository(CouponRangeRepository couponRangeRepository) {
		this.couponRangeRepository = couponRangeRepository;
	}

	@Autowired
	public void setCouponCategoryService(CouponCategoryService couponCategoryService) {
		this.couponCategoryService = couponCategoryService;
	}

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	@Autowired
	public void setCouponIssueService(CouponIssueService couponIssueService) {
		this.couponIssueService = couponIssueService;
	}

	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	
}
