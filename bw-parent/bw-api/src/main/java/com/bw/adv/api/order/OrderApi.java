package com.bw.adv.api.order;

import com.bw.adv.api.result.ApiResult;
import com.bw.adv.module.order.model.dto.OrderHeaderDto;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * ClassName: OrderApi <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 上午10:47:43 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Path("/order")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface OrderApi {
	
	/**
	 * handleSyncOrder:(同步订单). <br/>
	 * Date: 2015-9-15 上午10:47:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param orderHeaderDto
	 * @return
	 */
	@POST
	@Path("/handleSyncOrder")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult handleSyncOrder(OrderHeaderDto orderHeaderDto);
	
	/**
	 * 查询用户订单详情
	 * @param json
	 * @return
	 */
	@POST
	@Path("/getMemberOrderItemInfo")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberOrderItemInfo(String json);


	@POST
	@Path("/showShopOrderCount")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult showShopOrderCount(String json);
}
