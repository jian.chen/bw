package com.bw.adv.api.utils;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




import com.bw.adv.module.common.init.ApiObjMappingInit;
import com.bw.adv.module.tools.ReflectionUtils;

/**
 * ClassName:ApiObjUtil <br/>
 * Function: api返回至映射工具类N. <br/>
 * Date:     2013-12-12 下午11:37:18 <br/>
 * @author   Jimmy
 * @version  
 * @see 	 
 */
public class ApiObjUtil {
	/**
	 * 
	 * getApiResultMap:(将需要返回的对象映射成map). <br/>
	 * @author Jimmy
	 * @param cls 需要映射的返回值类
	 * @param orgId 需要返回的组织ID(系统id);
	 * @return
	 */
	public static Map<String, Object> getApiResultMap(Object obj, Long systemId){
		List<String> fieldNameList = ApiObjMappingInit.getApiObjFields(obj.getClass(), systemId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(fieldNameList == null){
			fieldNameList = new ArrayList<String>();
			List<Field> fieldList = ReflectionUtils.getFieldList(obj.getClass(), true);
			for(Field field : fieldList){
				fieldNameList.add(field.getName());
			}
		}
		
		for(String fieldName : fieldNameList){
			resultMap.put(fieldName, ReflectionUtils.invokeGetterMethod(obj, fieldName));
		}
		return resultMap;
	}
	
	/**
	 * 
	 * getApiResultItems:(返回对象映射map的集合). <br/>
	 *
	 * @author Jimmy
	 * @param list
	 * @param systemId
	 * @return
	 */
	public static List<Map<String, Object>> getApiResultItems(List<?> list, Long systemId){
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		for(Object obj : list){
			resultList.add(getApiResultMap(obj, systemId));
		}
		return resultList;
	}

}

