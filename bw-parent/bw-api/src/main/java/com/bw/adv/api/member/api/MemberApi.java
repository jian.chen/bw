package com.bw.adv.api.member.api;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.api.result.ApiResult;
import com.bw.adv.module.alipay.model.AlipayFans;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.wechat.model.WechatFans;

/**
 * ClassName: MemberApi <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-17 下午8:58:47 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Path("/member")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface MemberApi {
		
	/**
	 * weChatFocusEventWithoutMember:(微信新增粉丝事件，不新增会员). <br/>
	 * TODO(代替原WeChatFocusEvent_bak方法).<br/>
	 * Date: 2016-4-7 下午4:55:48 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	@POST
	@Path("/weChatFocusEvent")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult weChatFocusEvent(MemberDto memberDto);
	
	/**
	 * 
	 * memberActivation:(会员激活). <br/>
	 * Date: 2015年11月6日 下午4:33:30 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	@POST
	@Path("/memberActivation")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult memberActivation(MemberDto memberDto);
	
	/**
	 * 
	 * registerMember:会员注册 <br/>
	 * Date: 2015年8月19日 上午10:14:54 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	@POST
	@Path("/registerMember")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult registerMember(MemberDto memberDto);
	
	
	/**
	 * bindingMember:(绑定会员). <br/>
	 * Date: 2016-4-9 下午8:46:15 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	@POST
	@Path("/bindingMember")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult bindingMember(MemberDto memberDto);

	/**
	 * registerMemberNoWechat:(注册（无微信）并开卡). <br/>
	 * Date: 2015-9-15 下午7:42:31 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	@POST
	@Path("/registerMemberNoWechat")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult registerMemberNoWechat(MemberDto memberDto);
	/**
	 * 
	 * updateMember:会员信息修改 <br/>
	 * Date: 2015年8月19日 上午10:15:15 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	@POST
	@Path("/updateMember")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult updateMember(MemberExp memberExp);
	/**
	 * 
	 * updateMember:(会员受奖地址新增). <br/>
	 * Date: 2015年12月14日 下午9:01:49 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	@POST
	@Path("/addMemberAddress")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult addMemberAddress(MemberAddress memberAddress);
	
	/**
	 * addMemberAddress:(更新会员受奖地址). <br/>
	 * Date: 2015年12月17日 下午4:03:00 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberAddress
	 * @return
	 */
	@POST
	@Path("/updateMemberAddress")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult updateMemberAddress(MemberAddress memberAddress);
	
	/**
	 * 
	 * queryMemberByBindingAccount:根据外部商户查询会员信息 <br/>
	 * Date: 2015年8月19日 上午10:15:38 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param extAccountTypeId
	 * @param bindingAccount
	 * @return
	 */
	@GET
	@Path("/queryMemberByBindingAccount")
	ApiResult queryMemberByBindingAccount(@MatrixParam("extAccountTypeId")String extAccountTypeId,
			@MatrixParam("bindingAccount")String bindingAccount,@MatrixParam("mobile")String mobile);
	
	/**
	 * 
	 * queryMemberAccountByBindingAccount:根据外部商户查询会员账户信息 <br/>
	 * Date: 2015年8月19日 上午10:16:15 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param extAccountTypeId
	 * @param bindingAccount
	 * @return
	 */
	@GET
	@Path("/queryMemberAccountByBindingAccount")
	ApiResult queryMemberAccountByBindingAccount(@MatrixParam("extAccountTypeId")String extAccountTypeId,
			@MatrixParam("bindingAccount")String bindingAccount);
	
	
	/**
	 * queryMemberByBindingAccountAndLevel:(根据外部账户信息，查询会员信息). <br/>
	 * Date: 2015-10-26 上午10:27:40 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param extAccountTypeId：外部账户类型
	 * @param bindingAccount：外部账号
	 * @param queryLevel：查询级别:member,member_account,member_card
	 * @return
	 */
	@GET
	@Path("/queryMemberByBindingAccountAndLevel")
	ApiResult queryMemberByBindingAccountAndLevel(@MatrixParam("extAccountTypeId")String extAccountTypeId, @MatrixParam("bindingAccount")String bindingAccount,@MatrixParam("queryLevel") String queryLevel);
	
	/**
	 * sendVerifyCode:(发送验证码，分渠道，绑定或注册). <br/>
	 * Date: 2016-4-9 下午7:37:06 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param mobile
	 * @param channelCode
	 * @param isBinding
	 * @return
	 */
	@POST
	@Path("/sendVerifyCode")
	ApiResult sendVerifyCode(String json);
	
	
	
	/**
	 * queryCaptcha:(根据memberId查询会员受奖地址). <br/>
	 * Date: 2015年12月17日 下午2:00:28 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param mobile
	 * @return
	 */
	@GET
	@Path("/queryMemberAddressByMemberId")
	ApiResult queryMemberAddressByMemberId(@MatrixParam("memberId")String memberId);
	
	/**
	 * 
	 * updateWeChatFans:(更新粉丝数据&会员头像). <br/>
	 * Date: 2016年3月2日 下午6:27:54 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param wechatFans
	 * @return
	 */
	@POST
	@Path("/updateWeChatFans")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult updateWeChatFans(WechatFans wechatFans);

	/**
	 * 查询会员信息
	 * @param requestParams
	 * key: queryCode, queryType
	 * @return
	 */
	@POST
	@Path("/queryMemberByCode")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult queryMemberByCode(JSONObject requestParams);
	
	@POST
	@Path("/updateLanguage")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult updateLanguage(@MatrixParam("memberId")Long memberId,@MatrixParam("language")String language);

}
