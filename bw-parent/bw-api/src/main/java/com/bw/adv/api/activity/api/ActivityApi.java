/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-api
 * File Name:ActivityApi.java
 * Package Name:com.sage.scrm.api.activity.api
 * Date:2015年11月26日下午5:59:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.activity.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bw.adv.api.result.ApiResult;

/**
 * ClassName:ActivityApi <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 下午5:59:56 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Path("/activity")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface ActivityApi {
	
	/**
	 * extractMemberAwards:(统计符合抽奖的会员资格的次数). <br/>
	 * Date: 2015年12月11日 下午4:24:26 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberId
	 * @param activityId
	 * @return
	 */
	@GET
	@Path("/queryMemberSurplusNumber")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryMemberSurplusNumber(@MatrixParam("memberId")String memberId,@MatrixParam("activityId")String activityId);
	
	/**
	 * 
	 * queryCouponInstanceByMemberCode:(查询会员抽奖历史记录). <br/>
	 * Date: 2015年12月15日 上午11:09:33 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberCode
	 * @param statusId
	 * @param page
	 * @param rows
	 * @return
	 */
	@GET
	@Path("/queryWinningItemByMemberIdAndActivityId")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryWinningItemsByMemberIdAndActivityId(@MatrixParam("memberId")String memberId,@MatrixParam("activityId")String activityId,@MatrixParam("page")int page,@MatrixParam("rows")int rows);
	
	/**
	 * 
	 * queryLastWinningItem:(查询最近num条中奖信息). <br/>
	 * Date: 2016年3月22日 下午8:55:23 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param num
	 * @return
	 */
	@GET
	@Path("/queryLastWinningItem")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryLastWinningItem(@MatrixParam("num")Long num);
	/**
	 * 重载任务信息
	 */
	@GET
	@Path("/reloadGlobalInfo")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult reloadGlobalInfo();

	/**
	 * addMemberWinningAccord:(新增抽奖机会). <br/>
	 * Date: 2015年12月11日 下午4:17:10 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberId
	 * @param activityId
	 * @return
	 */
	@POST
	@Path("/addMemberWinningAccord")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult addMemberWinningAccord(@MatrixParam("openid")String openid,@MatrixParam("activityId")Long activityId,@MatrixParam("accordType")int accordType);
	
	/**
	 * 会员抽奖初始化查询机会
	 * @param memberId
	 * @param activityId
	 * @return
	 */
	@POST
	@Path("/initMemberAwards")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult initMemberAwards(String jsonParam);
	
	/**
	 * 进行抽奖
	 * @param jsonParam
	 * @return
	 */
	@POST
	@Path("/extractMemberAwards")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult extractMemberAwards(String jsonParam);
	
	
}

