package com.bw.adv.api.member.api;


import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bw.adv.api.result.ApiResult;

/**
 * ClassName: MemberCardApi <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-11-17 下午3:21:33 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Path("/memberCard")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface MemberCardApi {
	
	
	/**
	 * queryMemberInfoByQueryCode:(根据查询的值，查询会员信息). <br/>
	 * Date: 2015-11-17 下午3:21:41 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param queryCode：可以是会员编号，优惠券编号
	 * @param storeCode：门店编号
	 * @return
	 */
	@GET
	@Path("/queryMemberInfoByQueryCode")
	ApiResult queryMemberInfoByQueryCode(@MatrixParam("queryCode")String queryCode,@MatrixParam("storeCode")String storeCode);

	
}
