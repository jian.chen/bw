package com.bw.adv.api.sys.api;

import com.bw.adv.api.result.ApiResult;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by jeoy.zhou on 6/12/16.
 */
@Path("/user")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface UserApi {

    @POST
    @Path("/loginCheck")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ApiResult loginCheck(String json);
}
