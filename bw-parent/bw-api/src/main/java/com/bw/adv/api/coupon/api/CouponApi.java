package com.bw.adv.api.coupon.api;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.bw.adv.api.result.ApiResult;

@Path("/coupon")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface CouponApi {
	
	/**
	 * queryCouponInstanceByMemberCode:根据会员编号查询优惠券 <br/>
	 * Date: 2015年12月31日 下午2:31:23 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberCode
	 * @param statusId
	 * @param page
	 * @param rows
	 * @return
	 */
	@GET
	@Path("/queryCouponInstanceByMemberCode")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryCouponInstanceByMemberCode(@MatrixParam("type")String type,@MatrixParam("memberCode")String memberCode,@MatrixParam("statusId")String statusId,@MatrixParam("source")String source,@MatrixParam("page")int page,@MatrixParam("rows")int rows);
	
	/**
	 * queryMemberByExtAccount:根据外部账号查会员<br/>
	 * Date: 2016年1月8日 下午1:24:59 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param bindingAccount
	 * @param extAccountType
	 * @return
	 */
	@GET
	@Path("/queryMemberByExtAccount")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryMemberByExtAccount(@MatrixParam("bindingAccount")String bindingAccount,@MatrixParam("extAccountType")String extAccountType);
	
	/**
	 * queryCouponInstanceByBindingAccount:根据外部账号查询优惠券 <br/>
	 * Date: 2015年12月31日 下午2:31:56 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param bindingAccount
	 * @param extAccountType
	 * @param page
	 * @param rows
	 * @return
	 */
	@GET
	@Path("/queryCouponInstanceByBindingAccount")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryCouponInstanceByBindingAccount(@MatrixParam("bindingAccount")String bindingAccount,@MatrixParam("extAccountType")String extAccountType,@MatrixParam("page")int page,@MatrixParam("rows")int rows);
	
	/**
	 * queryCouponInstanceDetail:查询优惠券详情 <br/>
	 * Date: 2015年12月31日 下午2:32:17 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	@GET
	@Path("/queryCouponInstanceDetail")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryCouponInstanceDetail(@MatrixParam("couponInstanceCode")String couponInstanceCode,@MatrixParam("source")String source);
	
	/**
	 * queryCouponInstanceDetailAndValidate:(根据优惠券编号查询优惠券并验证优惠券是否有效). <br/>
	 * Date: 2015-11-10 下午4:37:36 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	@GET
	@Path("/queryCouponInstanceDetailAndValidate")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryCouponInstanceDetailAndValidate(@MatrixParam("couponInstanceCode")String couponInstanceCode);
	
	/**
	 * useCouponInstance:核销优惠券 <br/>
	 * Date: 2015年12月31日 下午2:32:37 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @param storeCode
	 * @return
	 */
	@GET
	@Path("/useCouponInstance")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult useCouponInstance(@MatrixParam("couponInstanceCode")String couponInstanceCode,@MatrixParam("storeCode")String storeCode);
	
	/**
	 * air助手批量核销优惠券
	 * @param couponInstanceCode
	 * @param storeCode
	 * @return
	 */
	@GET
	@Path("/useCouponInstanceList")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult useCouponInstanceList(@MatrixParam("couponInstanceCode")String couponInstanceCode,@MatrixParam("storeCode")String storeCode);
	
	/**
	 * 
	 * reverseUseCouponInstance:(反核销优惠券). <br/>
	 * Date: 2016年3月9日 上午10:22:30 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	@GET
	@Path("/reverseUseCouponInstance")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult reverseUseCouponInstance(@MatrixParam("couponInstanceCode")String couponInstanceCode);
	
	/**
	 * 
	 * queryCouponCategorys:(我要领劵模块---查询优惠券类别). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @param rows
	 * @return
	 */
	@GET
	@Path("/queryCouponCategorys")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryCouponCategorys(@MatrixParam("statusId")String statusId,@MatrixParam("page")int page,@MatrixParam("rows")int rows);
	
	/**
	 * 
	 * queryCouponByCategoryId:(我要领劵模块---根据优惠券类别查询优惠券). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @param rows
	 * @return
	 */
	@GET
	@Path("/queryCouponByCategoryId")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryCouponByCategoryId(@MatrixParam("couponCategoryId")String couponCategoryId);
	
	/**
	 * 
	 * queryCouponCategoryDetail:(我要领劵模块---查询优惠券详情). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param couponId
	 * @return
	 */
	@GET
	@Path("/queryCouponCategoryDetail")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryCouponCategoryDetail(@MatrixParam("couponId")String couponId);
	
	/**
	 * 
	 * getCouponCategory:(我要领劵模块---立即领取). <br/>
	 * Date: 2016年1月7日 下午11:58:20 <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param couponId
	 * @param memberId
	 * @return
	 */
	@GET
	@Path("/getCouponCategory")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult getCouponCategory(@MatrixParam("couponId")String couponId,@MatrixParam("memberCode")String memberCode,@MatrixParam("source")String source);


	@POST
	@Path("/showShopCheckCount")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult showShopCheckCount(String json);

	/**
	 *
	 * exchangeCoupon:(兑换优惠券). <br/>
	 * Date: 2016年11月7日 上午11:58:20 <br/>
	 * scrmVersion 1.0
	 * @author ronald.yang
	 * @version jdk1.7
	 * @param memberId
	 * @param exchangeCode
	 * @return
	 */
	@POST
	@Path("/exchange")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult exchangeCoupon(@MatrixParam("memberId") String memberId , @MatrixParam("exchangeCode") String exchangeCode);
}
