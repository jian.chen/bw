package com.bw.adv.api.order.validtion;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.order.enums.OrderOptEnum;
import com.bw.adv.module.order.model.dto.OrderHeaderDto;

public class OrderValidationImpl extends OrderValidationSupport{
	
	

	@Override
	public OrderHeaderDto validationOrderHeaderDto(OrderHeaderDto orderHeaderDto, Long channelId) {
		if(orderHeaderDto.getDeliveryAmount() == null){
			orderHeaderDto.setDeliveryAmount(BigDecimal.ZERO);
		}
		if(orderHeaderDto.getUsedPoints() == null || (orderHeaderDto.getUsedPoints().compareTo(BigDecimal.ZERO) == 0)) {
			orderHeaderDto.setUsedPoints(BigDecimal.ZERO);
			orderHeaderDto.setIsUsePoints("N");
		}else {
			orderHeaderDto.setIsUsePoints("Y");
		}
		if(StringUtils.isEmpty(orderHeaderDto.getLangx()) || !"en".equals(orderHeaderDto.getLangx())) {
			orderHeaderDto.setLangx("cn");
		}
		if((orderHeaderDto.getChannelId() == null || orderHeaderDto.getChannelId().equals(0l))
				&& channelId != null) {
			orderHeaderDto.setChannelId(channelId);
		}
		
		String opt = orderHeaderDto.getOptType();
		if(StringUtils.isEmpty(opt)) {
			orderHeaderDto.setStatusId(null);
		}else if(opt.equals(OrderOptEnum.ORDER_OPERATE_WAY_INSERT.getKey()) || opt.equals(OrderOptEnum.ORDER_OPERATE_WAY_UPDATE.getKey())) {
			orderHeaderDto.setStatusId(StatusConstant.ORDER_STATUS_SUCCESS.getId());
		}else if(opt.equals(OrderOptEnum.ORDER_OPERATE_WAY_DELETE.getKey())) {
			orderHeaderDto.setStatusId(StatusConstant.ORDER_STATUS_CANCEL.getId());
		}
		return orderHeaderDto;
	}

}
