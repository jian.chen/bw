package com.bw.adv.api.order.validtion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.order.enums.OrderOptEnum;
import com.bw.adv.module.order.enums.OrderSourceWayEnum;
import com.bw.adv.module.order.model.OrderCoupon;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.OrderHeaderExt;
import com.bw.adv.module.order.model.OrderItem;
import com.bw.adv.module.order.model.OrderPaymentMethod;
import com.bw.adv.module.order.model.dto.OrderCouponDto;
import com.bw.adv.module.order.model.dto.OrderHeaderDto;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.order.model.dto.OrderItemDto;
import com.bw.adv.module.order.model.dto.OrderPaymentMethodDto;
import com.bw.adv.module.tools.BeanUtils;


public abstract class OrderValidationSupport implements OrderValidation{
	
	/**
	 * 处理参数
	 * @param orderHeaderDto
	 * @return
	 */
	public final OrderInfo convertOrderInfo(OrderHeaderDto orderHeaderDto, Long channelId) {
		Map<String, OrderItem> orderItemMap = new HashMap<String, OrderItem>();
		OrderInfo orderInfo = null;
		OrderItem orderItem = null;
		OrderCoupon orderCoupon = null;
		
		orderInfo = new OrderInfo();
		//对orderHeaderDto参数检验
		orderHeaderDto = this.validationOrderHeaderDto(orderHeaderDto, channelId);
		orderInfo.setOrderHeaderDto(orderHeaderDto);
		//通过orderHeaderDto转换成orderHeader
		orderInfo.setOrderHeader(this.convertOrderHeaderByOrderHeaderDto(orderHeaderDto));
		//通过orderHeaderDto转换成系统所需要的orderHeaderExt
		orderInfo.setOrderHeaderExt(this.convertOrderHeaderExt(orderHeaderDto));
		
		//转换订单明细orderItemList
		if(orderHeaderDto.getOrderItemDtoList() != null) {
			List<OrderItem> itemList = new ArrayList<OrderItem>();
			Long index = 0l;
			for (OrderItemDto orderItemDto : orderHeaderDto.getOrderItemDtoList()) {
				orderItem = this.convertOrderItemByOrderItemDto(orderItemDto, index++);
				itemList.add(orderItem);
				orderItemMap.put(orderItemDto.getExternalItemId(), orderItem);
			}
			orderInfo.setOrderItems(itemList);
		}
		
		//转换优惠券orderCoupon
		if(orderHeaderDto.getOrderCouponDtoList() != null) {
			List<OrderCoupon> couponList = new ArrayList<OrderCoupon>();
			for (OrderCouponDto orderCouponDto : orderHeaderDto.getOrderCouponDtoList()) {
				orderCoupon = this.convertOrderCouponByOrderCouponDto(orderCouponDto);
				//用来设置订单明细优惠券
				if(orderItemMap.containsKey(orderCouponDto.getExternalItemId())) {
					orderCoupon.setExternalItemId(orderCouponDto.getExternalItemId());
				}
				couponList.add(orderCoupon);
			}
			orderInfo.setOrderCoupons(orderHeaderDto.getOrderCouponDtoList());
		}
		
		//订单付款方式
		if(orderHeaderDto.getOrderPaymentMethodDtoList() != null) {
			List<OrderPaymentMethod> methods = new ArrayList<OrderPaymentMethod>();
			for (OrderPaymentMethodDto orderPaymentMethodDto : orderHeaderDto.getOrderPaymentMethodDtoList()) {
				methods.add(this.convertOrderPaymentMethodByOrderPaymentMentodDto(orderPaymentMethodDto));
			}
			orderInfo.setOrderPaymentMethods(methods);
		}
		return orderInfo;
	}
	
	
	/**
	 * 检验orderHeaderDtos参数
	 * @param orderHeaderDtos
	 * @return
	 */
	@Override
	public OrderHeaderDto validationOrderHeaderDto(OrderHeaderDto orderHeaderDto, Long channelId) {
		if(orderHeaderDto.getDeliveryAmount() == null){
			orderHeaderDto.setDeliveryAmount(BigDecimal.ZERO);
		}
		if(orderHeaderDto.getUsedPoints() == null || (orderHeaderDto.getUsedPoints().compareTo(BigDecimal.ZERO) == 0)) {
			orderHeaderDto.setUsedPoints(BigDecimal.ZERO);
			orderHeaderDto.setIsUsePoints("N");
		}else {
			orderHeaderDto.setIsUsePoints("Y");
		}
		if(StringUtils.isEmpty(orderHeaderDto.getLangx()) || !"en".equals(orderHeaderDto.getLangx())) {
			orderHeaderDto.setLangx("cn");
		}
		if((orderHeaderDto.getChannelId() == null || orderHeaderDto.getChannelId().equals(0l))
				&& channelId != null) {
			orderHeaderDto.setChannelId(channelId);
		}
		String opt = orderHeaderDto.getOptType();
		if(StringUtils.isEmpty(opt)) {
			orderHeaderDto.setStatusId(null);
		}else if(opt.equals(OrderOptEnum.ORDER_OPERATE_WAY_INSERT.getKey()) || opt.equals(OrderOptEnum.ORDER_OPERATE_WAY_UPDATE.getKey())) {
			orderHeaderDto.setStatusId(StatusConstant.ORDER_STATUS_SUCCESS.getId());
		}else if(opt.equals(OrderOptEnum.ORDER_OPERATE_WAY_DELETE.getKey())) {
			orderHeaderDto.setStatusId(StatusConstant.ORDER_STATUS_CANCEL.getId());
		}
		return orderHeaderDto;
	}
	
	/**
	 * 通过orderHeaderDto转换成系统所需要的orderHeaderExt
	 * @param orderHeaderDto
	 * @return
	 */
	@Override
	public OrderHeaderExt convertOrderHeaderExt(OrderHeaderDto orderHeaderDto) {
		OrderHeaderExt orderHeaderExt = new OrderHeaderExt();
		if (StringUtils.isNotEmpty(orderHeaderDto.getMemberCode())) {
			//订单来源 会员编码
			orderHeaderExt.setOrderSourceWay(OrderSourceWayEnum.MEMBER_CODE.getCode());
			orderHeaderExt.setOrderSource(orderHeaderDto.getMemberCode());
		}else if (StringUtils.isNotEmpty(orderHeaderDto.getMobile())) {
			//订单来源 手机号码
			orderHeaderExt.setOrderSourceWay(OrderSourceWayEnum.MEMBER_MOBILE.getCode());
			orderHeaderExt.setOrderSource(orderHeaderDto.getMobile());
		}
		return orderHeaderExt;
	}
	
	/**
	 * 通过orderHeaderDto转换成系统所需要的orderHeader
	 * @param orderHeaderDto
	 * @return
	 */
	@Override
	public OrderHeader convertOrderHeaderByOrderHeaderDto(OrderHeaderDto orderHeaderDto) {
		OrderHeader orderHeader = new OrderHeader();
		BeanUtils.copy(orderHeaderDto,orderHeader);
		if(orderHeaderDto.getCouponAmount() != null) {
			orderHeader.setCouponAmount(orderHeaderDto.getCouponAmount());
		}else {
			orderHeader.setCouponAmount(BigDecimal.ZERO);
		}
		orderHeader.setPayAmount(orderHeaderDto.getPayAmount());
		if(orderHeaderDto.getDeliveryAmount() != null) {
			orderHeader.setDeliveryAmount(orderHeaderDto.getDeliveryAmount());
		}else {
			orderHeader.setDeliveryAmount(BigDecimal.ZERO);
		}
		if(orderHeaderDto.getUsedPoints() != null) {
			orderHeader.setUsedPoints(orderHeaderDto.getUsedPoints());
		}else {
			orderHeader.setUsedPoints(BigDecimal.ZERO);
		}
//		orderHeader.setIsUsePoints(orderHeaderDto.getIsUsePoints());
//		orderHeader.setLangx(orderHeaderDto.getLangx());
		orderHeader.setStaffId(orderHeaderDto.getCreateBy());
		orderHeader.setCreateTime(orderHeaderDto.getCreateTime());
		orderHeader.setLastModifiedStaffId(orderHeaderDto.getLastModifiedBy());
		orderHeader.setLastModifiedTime(orderHeaderDto.getLastModifiedTime());
		orderHeader.setStatusId(orderHeaderDto.getStatusId());
//		orderHeader.setDataVersion(orderHeaderDto.getDataVersion());
		return orderHeader;
	}
	
	/**
	 * 根据OrderItemDto转换成对应的orderItem
	 * @param orderItemDto
	 * @param seq
	 * @return
	 */
	@Override
	public OrderItem convertOrderItemByOrderItemDto(OrderItemDto orderItemDto, Long seq) {
		OrderItem orderItem = new OrderItem();
		orderItem.setProductId(orderItemDto.getProductId());
		orderItem.setOrderItemTypeId(orderItemDto.getOrderItemTypeId());
		orderItem.setOrderItemName(orderItemDto.getOrderItemName());
		orderItem.setQuantity(orderItemDto.getQuantity());
		orderItem.setUnitPrice(orderItemDto.getUnitPrice());
		orderItem.setAmount(orderItemDto.getAmount());
		orderItem.setCouponAmount(orderItemDto.getCouponAmount());
		orderItem.setPayAmount(orderItemDto.getPayAmount());
		orderItem.setSeq(seq);
//		orderItem.setProductCategoryId(orderItemDto.getGroupId());
//		orderItem.setIsCoupon(orderItemDto.getIsCoupon());
		return orderItem;
	}
	
	/**
	 * 根据orderCouponDto转换成对应的orderCoupon
	 * @param orderCouponDto
	 * @return
	 */
	@Override
	public OrderCoupon convertOrderCouponByOrderCouponDto(OrderCouponDto orderCouponDto) {
		OrderCoupon orderCoupon = new OrderCoupon();
		orderCoupon.setCouponAmount(orderCouponDto.getCouponAmount());
		orderCoupon.setCouponPercentage(orderCouponDto.getCouponPercentage());
//		orderCoupon.setCouponInstanceCode(orderCouponDto.getCouponInstanceCode());
		//pos提交订单情况下，当isCommon为空时系统默认为非通用优惠券
//		if(StringUtils.isEmpty(orderCoupon.getIsCommon())){
//			orderCoupon.setIsCommon("N");
//		}
		return orderCoupon;
	}
	
	/**
	 * 根据orderPaymentMethodDto转换成对应的orderPaymentMethod
	 * @param orderCouponDto
	 * @return
	 */
	@Override
	public OrderPaymentMethod convertOrderPaymentMethodByOrderPaymentMentodDto(OrderPaymentMethodDto orderPaymentMethodDto) {
		OrderPaymentMethod method = new OrderPaymentMethod();
		method.setStatusId(StatusConstant.ORDER_PAYMENT_STATUS_SUCCESS.getId());
		method.setPaymentMethodId(orderPaymentMethodDto.getPaymentMethodId());
		method.setPayAmount(orderPaymentMethodDto.getPayAmount());
		method.setCreateTime(orderPaymentMethodDto.getCreateTime());
		method.setPaymentMethodName(orderPaymentMethodDto.getPaymentMethodName());
		method.setPayTime(orderPaymentMethodDto.getPayTime());
		return method;
	}
	
}
