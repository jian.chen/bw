package com.bw.adv.aop.service;

import net.sf.json.JSONObject;

import com.bw.adv.module.member.model.Member;


/**
 * ClassName: 注册活动动作参数 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-10-11 下午7:02:44 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface RegisterActivityActionParams {

	/**
	 * getRegisterParams:(根据注册信息，生成不同的活动参数). <br/>
	 * TODO(不同项目活动参数不同).<br/>
	 * TODO(可根据项目需求改写项目所需参数).<br/>
	 * Date: 2015-10-11 下午7:03:04 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param member
	 * @return
	 */
	public JSONObject getRegisterParams(Member member);
	
}
