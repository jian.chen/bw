package com.bw.adv.aop.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bw.adv.aop.service.OrderActivityActionParams;
import com.bw.adv.module.activity.constant.ActivityConstant;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.repository.StoreRepository;
import com.bw.adv.module.component.rule.constant.ConditionsConstant;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.OrderItem;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.order.repository.OrderHeaderRepository;
import com.bw.adv.module.tools.DateUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Component
public class OrderActivityActionParamsImpl implements OrderActivityActionParams{

	
	private StoreRepository storeRepository;
	
	private OrderHeaderRepository orderHeaderRepository;
	
	@Override
	public JSONObject getOrderParams(OrderInfo orderInfo,MemberExp memberExp) {
		JSONObject jsonObject = null;
		OrderHeader orderHeader = null;
		List<OrderItem> orderItemList = null;
		String tagIds = "";
		String groupIds = "";
		String channelId = "";
		String gradeId = "";
		JSONArray orderItemArray = null;
		JSONObject orderItems = null;
		Store store = null;
		
		
		orderItemArray = new JSONArray();
		
		jsonObject = new JSONObject();
		orderHeader = orderInfo.getOrderHeader();
		orderItemList = orderInfo.getOrderItems();
		if(orderItemList != null){
			for (OrderItem orderItem : orderItemList) {
				orderItems = new JSONObject();
				orderItems.put("productCode", orderItem.getProductCode());
				orderItems.put("quantity", orderItem.getQuantity());
				orderItems.put("amount", orderItem.getAmount());
				orderItemArray.add(orderItems);
			}
		}
		groupIds = memberExp.getMemberGroupIds();
		tagIds = memberExp.getMemberTagIds();
		channelId = memberExp.getChannelId().toString();
		gradeId = memberExp.getGradeId().toString();
		if(StringUtils.isNotBlank(orderHeader.getStoreCode())){
			store = storeRepository.findByStoreCode(orderHeader.getStoreCode());
		}
		
		if(store != null){
			jsonObject.put("storeName", store.getStoreName());
		}else{
			jsonObject.put("storeName", "");
		}
		jsonObject.put("orderId", orderHeader.getOrderId());
		jsonObject.put("externalId", orderHeader.getExternalId());
		jsonObject.put("channelId", channelId);
		jsonObject.put("gradeId", gradeId);
		jsonObject.put(ConditionsConstant.MEMBER_GROUP.getCode(), groupIds);
		jsonObject.put(ConditionsConstant.MEMBER_TAG.getCode(), tagIds);
		jsonObject.put(ConditionsConstant.APPOINT_PRODUCT.getCode(), orderItemArray);
		jsonObject.put(ConditionsConstant.APPOINT_AMOUNT.getCode(), orderHeader.getPayAmount());
		jsonObject.put(ConditionsConstant.APPOINT_PRODUCT_NUM.getCode(), orderInfo.getSaleCount());
		jsonObject.put(ConditionsConstant.CONSUMPTION_COUNT.getCode(), memberExp.getTotalConsumptionCount());
		jsonObject.put(ConditionsConstant.TOTAL_CONSUMPTION.getCode(), memberExp.getTotalConsumption() == null ? 0 : memberExp.getTotalConsumption());//会员累积消费额
		jsonObject.put(ConditionsConstant.ORDER_STORE_CODE.getCode(), StringUtils.isBlank(orderHeader.getStoreCode()) ? "" : orderHeader.getStoreCode());
		jsonObject.put(ConditionsConstant.ORDER_DATE.getCode(), StringUtils.isBlank(orderHeader.getOrderDate()) ? DateUtils.getCurrentDateOfDb() : orderHeader.getOrderDate());
		jsonObject.put(ConditionsConstant.MEMBER_BRITHDAY.getCode(), memberExp.getBirthday());
		return jsonObject;
	}

	
	
	public static void main(String[] args) {
		System.out.println(DateUtils.getWeekOfDateSEN("20160612"));
		System.out.println(DateUtils.getWeekOfDateSEN("20160613"));
		System.out.println(DateUtils.getWeekOfDateSEN("20160614"));
		System.out.println(DateUtils.getWeekOfDateSEN("20160615"));
		System.out.println(DateUtils.getWeekOfDateSEN("20160616"));
		System.out.println(DateUtils.getWeekOfDateSEN("20160617"));
		System.out.println(DateUtils.getWeekOfDateSEN("20160618"));
	}

	@Override
	public List<JSONObject> getOftenParams(OrderInfo orderInfo,MemberExp memberExp) {
		List<JSONObject> resultList = new ArrayList<JSONObject>();
		OrderHeader orderHeader = orderInfo.getOrderHeader();
		List<ActivityInstance> list = RuleInit.getActivityInstanceList(ActivityConstant.ACTIVIY_ORDER_OFTEN.getCode());
		for (ActivityInstance activityInstance : list) {
				JSONObject json = RuleInit.getInstanceConditionJson(activityInstance.getActivityInstanceId());
				JSONObject result = new JSONObject();
				Long dayAmount = json.containsKey("ORDER_DAY_AMOUNT")? json.getLong("ORDER_DAY_AMOUNT") : 1L;
				String orderDate = DateUtils.dateStrToNewFormat(orderHeader.getOrderDate(), DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_2);
				BigDecimal sum = orderHeaderRepository.queryEveryDayOrderAmount(memberExp.getMemberId(), orderHeader.getOrderDate());
				BigDecimal now = sum.subtract(orderHeader.getOrderAmount());
				if(now.compareTo(new BigDecimal(dayAmount)) < 0 && sum.compareTo(new BigDecimal(dayAmount)) >=0){
					Long resultCount = orderHeaderRepository.queryCountByAmountAndMemberId(dayAmount, memberExp.getMemberId(),orderDate);
					result.put("ofenCount", resultCount);
					result.put("orderId", orderHeader.getOrderId());
					result.put("externalId", orderHeader.getExternalId());
					result.put("oftenAmount", dayAmount);
					resultList.add(result);
				}
		}
		return resultList;
	}
	
	@Autowired
	public void setStoreRepository(StoreRepository storeRepository) {
		this.storeRepository = storeRepository;
	}

	@Autowired
	public void setOrderHeaderRepository(OrderHeaderRepository orderHeaderRepository) {
		this.orderHeaderRepository = orderHeaderRepository;
	}
	
}
