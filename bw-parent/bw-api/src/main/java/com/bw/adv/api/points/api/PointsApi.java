package com.bw.adv.api.points.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.api.result.ApiResult;

@Path("/points")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface PointsApi {
	
	/**
	 * getMemberPointsInfo:查询会员积分 <br/>
	 * Date: 2015年12月31日 下午2:36:42 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param json
	 * @return
	 */
	@POST
	@Path("/getMemberPointsInfo")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberPointsInfo(String json);
	
	/**
	 * getMemberPointsExchangeInfo:查询会员积分兑换记录 <br/>
	 * Date: 2015年12月31日 下午2:35:28 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param json
	 * @return
	 */
	@POST
	@Path("/getMemberPointsExchangeInfo")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberPointsExchangeInfo(String json);
	
	/**
	 * exchangePointsProductDetail:积分兑换<br/>
	 * Date: 2015年12月31日 下午2:35:54 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param json
	 * @return
	 */
	@POST
	@Path("/exchangePointsProductDetail")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult exchangePointsProductDetail(String json);
	
	/**
	 * getPointsProduct:查询商品兑换信息<br/>
	 * Date: 2015年12月14日 下午9:37:11 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param json
	 * @return
	 */
	@POST
	@Path("/getPointsProduct")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getPointsProduct(String json);

	/**
	 * queryPointsProductDetail:查询积分商城详情<br/>
	 * Date: 2015年12月31日 下午2:36:13 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param goodsShowId
	 * @return
	 */
	@GET
	@Path("/getPointsProductDetail")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ApiResult queryPointsProductDetail(@MatrixParam("goodsShowId")Long goodsShowId,@MatrixParam("gradeId")Long gradeId);

	@POST
	@Path("/getMemberPointsList")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult getMemberPointsList(JSONObject memberPointsParams);

	/**
	 * 获取积分商城商品
	 * 不包含会员相关信息，只是积分商品
	 * @param params
	 * @return
	 */
	@POST
	@Path("/getPointsProductList")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	ApiResult queryPointsProductList(JSONObject params);
}
