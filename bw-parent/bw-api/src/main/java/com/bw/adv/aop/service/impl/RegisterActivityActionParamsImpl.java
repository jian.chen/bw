package com.bw.adv.aop.service.impl;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Component;

import com.bw.adv.aop.service.RegisterActivityActionParams;
import com.bw.adv.module.member.model.Member;

@Component
public class RegisterActivityActionParamsImpl implements RegisterActivityActionParams{

	@Override
	public JSONObject getRegisterParams(Member member) {
		JSONObject json = new JSONObject();
		json.put("channelId", member.getChannelId());
		return json;
	}

	
}
