/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-report
 * File Name:MemberChannelSummaryDayRepository.java
 * Package Name:com.sage.scrm.module.report.repository
 * Date:2015年11月25日下午5:50:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/
package com.bw.adv.module.report.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.order.mapper.OrderRegionSummaryDayMapper;
import com.bw.adv.module.report.order.model.OrderRegionSummaryDay;
import com.bw.adv.module.report.order.model.exp.OrderRegionSummaryDayExp;

import java.util.List;

public interface OrderRegionSummaryDayRepository extends BaseRepository<OrderRegionSummaryDay,OrderRegionSummaryDayMapper>{
	List<OrderRegionSummaryDayExp> findByExp(Example example);

	/**
	 * 
	 * callOrderRegionSummaryDay:(调用订单日统计存储过程). <br/>
	 * Date: 2016年1月14日 下午4:12:57 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	public void callOrderRegionSummaryDay(String dateStr);
}
