package com.bw.adv.module.report.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.model.MemberChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.MemberChannelSummaryDayExp;

public interface MemberChannelSummaryDayMapper extends BaseMapper<MemberChannelSummaryDay> {
	
     List<MemberChannelSummaryDayExp> selectExpByExample(@Param("example")Example example,Page<MemberChannelSummaryDayExp> page);
     
     /**
      * 
      * callMemberChannelSummary:(调用会员渠道统计存储过程). <br/>
      * Date: 2015年11月30日 下午2:58:11 <br/>
      * scrmVersion 1.0
      * @author chuanxue.wei
      * @version jdk1.7
      * @param dateStr
      */
     void callMemberChannelSummary(@Param("dateStr")String dateStr);
}