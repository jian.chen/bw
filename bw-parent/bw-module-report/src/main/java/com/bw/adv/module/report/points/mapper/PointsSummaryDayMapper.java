package com.bw.adv.module.report.points.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.points.model.PointsSummaryDay;
import com.bw.adv.module.report.points.model.exp.PointsSummaryDayExp;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PointsSummaryDayMapper extends BaseMapper<PointsSummaryDay>{
	List<PointsSummaryDayExp> selectByExp(@Param("example") Example example);
	
	PointsSummaryDayExp selectPointByExp(@Param("example") Example example);

	/**
	 * 
	 * callPointsSummaryDay:(调用积分日统计存储过程). <br/>
	 * Date: 2016年1月14日 下午4:26:22 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	 void callPointsSummaryDay(@Param("dateStr")String dateStr);
}