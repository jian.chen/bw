/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-report
 * File Name:MemberChannelSummaryDayRepository.java
 * Package Name:com.sage.scrm.module.report.repository
 * Date:2015年11月25日下午5:50:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/
package com.bw.adv.module.report.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.points.mapper.PointsSummaryDayMapper;
import com.bw.adv.module.report.points.model.PointsSummaryDay;
import com.bw.adv.module.report.points.model.exp.PointsSummaryDayExp;

import java.util.List;

public interface PointsSummaryDayRepository extends BaseRepository<PointsSummaryDay,PointsSummaryDayMapper>{
	List<PointsSummaryDayExp> findByExp(Example example);
	

	PointsSummaryDayExp findPointByExp(Example example);
	/**
	 * 
	 * callPointsSummaryDay:(调用积分日统计存储过程). <br/>
	 * Date: 2016年1月14日 下午4:27:16 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	public void callPointsSummaryDay(String dateStr);
}
