package com.bw.adv.module.report.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.report.mapper.IndexChartShowInstanceMapper;
import com.bw.adv.module.report.model.IndexChartShowInstance;
import com.bw.adv.module.report.repository.IndexChartShowInstanceRepository;

@Repository
public class IndexChartShowInstanceRepositoryImpl extends BaseRepositoryImpl<IndexChartShowInstance, IndexChartShowInstanceMapper> implements IndexChartShowInstanceRepository{

	@Override
	protected Class<IndexChartShowInstanceMapper> getMapperClass() {
		return IndexChartShowInstanceMapper.class;
	}

	@Override
	public void deleteInstanceByUserId(Long userId) {
		this.getMapper().deleteByUserId(userId);
	}

	@Override
	public List<IndexChartShowInstance> selectUserIndexByUserId(Long userId) {
		return this.getMapper().selectUserIndexByUserId(userId);
	}

}
