package com.bw.adv.module.report.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.report.mapper.MemberSummaryAgeMapper;

public interface ReportRepository extends BaseRepository<Member, MemberSummaryAgeMapper>{
	
	/**
	 * 调用会员年龄统计存储过程
	 * @param dateStr
	 */
	public void callAgeSummary(String dateStr);

}
