package com.bw.adv.module.report.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.order.mapper.OrderRegionSummaryDayMapper;
import com.bw.adv.module.report.order.model.OrderRegionSummaryDay;
import com.bw.adv.module.report.order.model.exp.OrderRegionSummaryDayExp;
import com.bw.adv.module.report.repository.OrderRegionSummaryDayRepository;

import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class OrderRegionSummaryDayRepositoryImpl extends BaseRepositoryImpl<OrderRegionSummaryDay,OrderRegionSummaryDayMapper>
	implements OrderRegionSummaryDayRepository{


	@Override
	protected Class<OrderRegionSummaryDayMapper> getMapperClass() {
		return OrderRegionSummaryDayMapper.class;
	}

	@Override
	public List<OrderRegionSummaryDayExp> findByExp(Example example) {
		return this.getMapper().selectByExp(example);
	}

	@Override
	public void callOrderRegionSummaryDay(String dateStr) {
		
		this.getMapper().callOrderRegionSummaryDay(dateStr);
	}

}
