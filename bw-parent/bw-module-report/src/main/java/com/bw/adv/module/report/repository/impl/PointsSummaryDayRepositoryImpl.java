package com.bw.adv.module.report.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.points.mapper.PointsSummaryDayMapper;
import com.bw.adv.module.report.points.model.PointsSummaryDay;
import com.bw.adv.module.report.points.model.exp.PointsSummaryDayExp;
import com.bw.adv.module.report.repository.PointsSummaryDayRepository;

import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class PointsSummaryDayRepositoryImpl extends BaseRepositoryImpl<PointsSummaryDay,PointsSummaryDayMapper>
	implements PointsSummaryDayRepository{

	@Override
	public List<PointsSummaryDayExp> findByExp(Example example) {
		return this.getMapper().selectByExp(example);
	}

	@Override
	protected Class<PointsSummaryDayMapper> getMapperClass() {
		return PointsSummaryDayMapper.class;
	}

	@Override
	public void callPointsSummaryDay(String dateStr) {
		
		this.getMapper().callPointsSummaryDay(dateStr);
		
	}

	@Override
	public PointsSummaryDayExp findPointByExp(Example example) {
		return this.getMapper().selectPointByExp(example);
	}
}
