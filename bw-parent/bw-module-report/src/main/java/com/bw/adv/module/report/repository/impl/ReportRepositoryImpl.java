package com.bw.adv.module.report.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.report.mapper.MemberSummaryAgeMapper;
import com.bw.adv.module.report.repository.ReportRepository;

@Repository
public class ReportRepositoryImpl extends BaseRepositoryImpl<Member, MemberSummaryAgeMapper> implements ReportRepository{
	
	@Override
	protected Class<MemberSummaryAgeMapper> getMapperClass() {
		return MemberSummaryAgeMapper.class;
	}

	@Override
	public void callAgeSummary(String dateStr) {
		this.getMapper().callAgeSummary(dateStr);
	}

}
