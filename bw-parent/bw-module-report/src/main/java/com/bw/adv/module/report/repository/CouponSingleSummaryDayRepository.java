/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-report
 * File Name:CouponSingleSummaryDayRepository.java
 * Package Name:com.sage.scrm.module.report.repository
 * Date:2015年12月17日上午10:22:47
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.coupon.mapper.CouponSingleSummaryDayMapper;
import com.bw.adv.module.report.coupon.model.CouponSingleSummaryDay;
import com.bw.adv.module.report.coupon.model.exp.CouponSingleSummaryDayExp;

/**
 * ClassName:CouponSingleSummaryDayRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 上午10:22:47 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponSingleSummaryDayRepository extends BaseRepository<CouponSingleSummaryDay, CouponSingleSummaryDayMapper>{

	/**
	 * 
	 * findExpByExample:(根据条件查询券统计列表). <br/>
	 * Date: 2015年12月17日 下午3:25:30 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponSingleSummaryDayExp> findExpByExample(Example example,Page<CouponSingleSummaryDayExp> page);
	
	/**
	 * 
	 * findListByMouth:(券统计月查询). <br/>
	 * Date: 2015年12月17日 下午4:09:10 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param mouth
	 * @param page
	 * @return
	 */
	List<CouponSingleSummaryDayExp> findListByMonth(Integer month,Page<CouponSingleSummaryDayExp> page);
	
	/**
	 * 
	 * callCouponSingleSummary:(调用优惠券日统计存储过程). <br/>
	 * Date: 2015年12月17日 下午6:37:50 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	void callCouponSingleSummary(String dateStr);
	
}

