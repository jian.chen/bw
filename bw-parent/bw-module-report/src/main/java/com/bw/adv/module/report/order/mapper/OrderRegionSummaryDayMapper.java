package com.bw.adv.module.report.order.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.order.model.OrderRegionSummaryDay;
import com.bw.adv.module.report.order.model.exp.OrderRegionSummaryDayExp;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderRegionSummaryDayMapper extends BaseMapper<OrderRegionSummaryDay>{
    
    List<OrderRegionSummaryDayExp> selectByExp(@Param("example")Example example);

    /**
     * 
     * callOrderRegionSummaryDay:(调用订单日统计存储过程). <br/>
     * Date: 2016年1月14日 下午4:10:04 <br/>
     * scrmVersion 1.0
     * @author chuanxue.wei
     * @version jdk1.7
     * @param dateStr
     */
    void callOrderRegionSummaryDay(@Param("dateStr")String dateStr);
}