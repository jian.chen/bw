package com.bw.adv.module.report.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.report.model.IndexChartShowInstance;

public interface IndexChartShowInstanceMapper extends BaseMapper<IndexChartShowInstance>{
	
    int deleteByPrimaryKey(Long id);

    int insert(IndexChartShowInstance record);

    int insertSelective(IndexChartShowInstance record);

    IndexChartShowInstance selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(IndexChartShowInstance record);

    int updateByPrimaryKey(IndexChartShowInstance record);
    
    /**
     * 根据用户id删除记录
     * @param userId
     */
    void deleteByUserId(Long userId);
    
    /**
	 * 根据用户id查询仪表盘设置
	 * @param userId
	 * @return
	 */
	List<IndexChartShowInstance> selectUserIndexByUserId(Long userId);
    
}