package com.bw.adv.module.report.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.report.mapper.IndexChartShowInstanceMapper;
import com.bw.adv.module.report.model.IndexChartShowInstance;

public interface IndexChartShowInstanceRepository extends BaseRepository<IndexChartShowInstance, IndexChartShowInstanceMapper>{

	/**
	 * 删除用户首页仪表盘配置
	 * @param userId
	 */
	public void deleteInstanceByUserId(Long userId);
	
	/**
	 * 根据用户id查询仪表盘设置
	 * @param userId
	 * @return
	 */
	public List<IndexChartShowInstance> selectUserIndexByUserId(Long userId);
}
