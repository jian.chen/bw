/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-report
 * File Name:MemberChannelSummaryDayRepositoryImpl.java
 * Package Name:com.sage.scrm.module.report.repository.impl
 * Date:2015年11月25日下午5:52:32
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.mapper.MemberChannelSummaryDayMapper;
import com.bw.adv.module.report.member.model.MemberChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.MemberChannelSummaryDayExp;
import com.bw.adv.module.report.repository.MemberChannelSummaryDayRepository;

/**
 * ClassName:MemberChannelSummaryDayRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月25日 下午5:52:32 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberChannelSummaryDayRepositoryImpl extends BaseRepositoryImpl<MemberChannelSummaryDay, MemberChannelSummaryDayMapper> 
       implements MemberChannelSummaryDayRepository{

	@Override
	protected Class<MemberChannelSummaryDayMapper> getMapperClass() {
		
		return MemberChannelSummaryDayMapper.class;
	}

	@Override
	public List<MemberChannelSummaryDayExp> findExpByExample(Example example,
			Page<MemberChannelSummaryDayExp> page) {
		
		return this.getMapper().selectExpByExample(example, page);
	}

	@Override
	public void callMemberChannelSummary(String dateStr) {
		
		this.getMapper().callMemberChannelSummary(dateStr);
		
	}

}

