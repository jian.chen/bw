/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-report
 * File Name:FansChannelSummaryDayRepositoryImpl.java
 * Package Name:com.sage.scrm.module.report.repository.impl
 * Date:2015年11月30日上午10:01:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.mapper.FansChannelSummaryDayMapper;
import com.bw.adv.module.report.member.model.FansChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.FansChannelSummaryDayExp;
import com.bw.adv.module.report.repository.FansChannelSummaryDayRepository;

/**
 * ClassName:FansChannelSummaryDayRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月30日 上午10:01:50 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class FansChannelSummaryDayRepositoryImpl extends BaseRepositoryImpl<FansChannelSummaryDay, FansChannelSummaryDayMapper> 
	implements FansChannelSummaryDayRepository{

	@Override
	protected Class<FansChannelSummaryDayMapper> getMapperClass() {
		
		return FansChannelSummaryDayMapper.class;
	}

	@Override
	public List<FansChannelSummaryDayExp> findListByExample(Example example,
			Page<FansChannelSummaryDayExp> page) {
		
		return this.getMapper().selectExpByExample(example, page);
	}

	@Override
	public void callFansChannelSummary(String dateStr) {
		
		this.getMapper().callFansChannelSummary(dateStr);
		
	}

}

