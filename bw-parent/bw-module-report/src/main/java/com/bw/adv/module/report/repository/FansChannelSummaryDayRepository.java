/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-report
 * File Name:FansChannelSummaryDayRepository.java
 * Package Name:com.sage.scrm.module.report.repository
 * Date:2015年11月30日上午10:01:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.mapper.FansChannelSummaryDayMapper;
import com.bw.adv.module.report.member.model.FansChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.FansChannelSummaryDayExp;

/**
 * ClassName:FansChannelSummaryDayRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月30日 上午10:01:09 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface FansChannelSummaryDayRepository extends BaseRepository<FansChannelSummaryDay, FansChannelSummaryDayMapper> {

	/**
	 * 
	 * findListByExample:(查询粉丝渠道报表列表). <br/>
	 * Date: 2015年11月30日 上午10:04:38 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<FansChannelSummaryDayExp> findListByExample(Example example,Page<FansChannelSummaryDayExp> page);
	
	/**
	 * 
	 * callMemberChannelSummary:(调用粉丝渠道存储过程). <br/>
	 * Date: 2015年11月30日 下午3:00:38 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	public void callFansChannelSummary(String dateStr);
	
}

