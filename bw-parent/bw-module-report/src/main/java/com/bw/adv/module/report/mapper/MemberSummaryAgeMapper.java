package com.bw.adv.module.report.mapper;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.module.report.model.MemberSummaryAge;

public interface MemberSummaryAgeMapper {
    int deleteByPrimaryKey(Long summaryId);

    int insert(MemberSummaryAge record);

    int insertSelective(MemberSummaryAge record);

    MemberSummaryAge selectByPrimaryKey(Long summaryId);

    int updateByPrimaryKeySelective(MemberSummaryAge record);

    int updateByPrimaryKey(MemberSummaryAge record);
    
    /**
	 * 调用会员年龄统计存储过程
	 * @param dateStr
	 */
	void callAgeSummary(@Param("dateStr")String dateStr);
}