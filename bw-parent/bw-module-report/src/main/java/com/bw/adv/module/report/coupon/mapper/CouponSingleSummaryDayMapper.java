package com.bw.adv.module.report.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.coupon.model.CouponSingleSummaryDay;
import com.bw.adv.module.report.coupon.model.exp.CouponSingleSummaryDayExp;

public interface CouponSingleSummaryDayMapper extends BaseMapper<CouponSingleSummaryDay>{
	
	/**
	 * 
	 * selectListByMouth:(券统计月查询). <br/>
	 * Date: 2015年12月17日 下午4:09:44 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param summaryMonth
	 * @param page
	 * @return
	 */
	List<CouponSingleSummaryDayExp> selectListByMonth(@Param("summaryMonth")Integer summaryMonth,Page<CouponSingleSummaryDayExp> page);
	
	/**
	 * 
	 * selectExpByExample:(根据条件查询券统计列表). <br/>
	 * Date: 2015年12月17日 下午4:09:55 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	List<CouponSingleSummaryDayExp> selectExpByExample(@Param("example")Example example,Page<CouponSingleSummaryDayExp> page);
	
	/**
	 * 
	 * callCouponSingleSummary:(调用优惠券日统计存储过程). <br/>
	 * Date: 2015年12月17日 下午6:35:34 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	void callCouponSingleSummary(@Param("dateStr")String dateStr);

}