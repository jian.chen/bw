/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-report
 * File Name:CouponSingleSummaryDayRepositoryImpl.java
 * Package Name:com.sage.scrm.module.report.repository.impl
 * Date:2015年12月17日上午10:23:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.coupon.mapper.CouponSingleSummaryDayMapper;
import com.bw.adv.module.report.coupon.model.CouponSingleSummaryDay;
import com.bw.adv.module.report.coupon.model.exp.CouponSingleSummaryDayExp;
import com.bw.adv.module.report.repository.CouponSingleSummaryDayRepository;

/**
 * ClassName:CouponSingleSummaryDayRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 上午10:23:35 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class CouponSingleSummaryDayRepositoryImpl extends BaseRepositoryImpl<CouponSingleSummaryDay, CouponSingleSummaryDayMapper>
	implements CouponSingleSummaryDayRepository{

	@Override
	protected Class<CouponSingleSummaryDayMapper> getMapperClass() {
		
		return CouponSingleSummaryDayMapper.class;
	}

	@Override
	public List<CouponSingleSummaryDayExp> findExpByExample(Example example,Page<CouponSingleSummaryDayExp> page) {
		
		return this.getMapper().selectExpByExample(example,page);
	}

	@Override
	public List<CouponSingleSummaryDayExp> findListByMonth(Integer month,
			Page<CouponSingleSummaryDayExp> page) {
		
		return this.getMapper().selectListByMonth(month, page);
	}

	@Override
	public void callCouponSingleSummary(String dateStr) {
		
		this.getMapper().callCouponSingleSummary(dateStr);
	}

}

