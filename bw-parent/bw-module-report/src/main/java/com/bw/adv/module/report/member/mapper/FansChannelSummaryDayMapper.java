package com.bw.adv.module.report.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.model.FansChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.FansChannelSummaryDayExp;

public interface FansChannelSummaryDayMapper extends BaseMapper<FansChannelSummaryDay> {

	List<FansChannelSummaryDayExp> selectExpByExample(@Param("example")Example example,Page<FansChannelSummaryDayExp> page);
	
	/**
	 * 
	 * callFansChannelSummary:(调用粉丝渠道统计存储过程). <br/>
	 * Date: 2015年11月30日 下午2:56:50 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	void callFansChannelSummary(@Param("dateStr")String dateStr);
	
}