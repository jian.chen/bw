/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-report
 * File Name:MemberChannelSummaryDayRepository.java
 * Package Name:com.sage.scrm.module.report.repository
 * Date:2015年11月25日下午5:50:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.mapper.MemberChannelSummaryDayMapper;
import com.bw.adv.module.report.member.model.MemberChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.MemberChannelSummaryDayExp;

/**
 * ClassName:MemberChannelSummaryDayRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月25日 下午5:50:35 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberChannelSummaryDayRepository extends BaseRepository<MemberChannelSummaryDay, MemberChannelSummaryDayMapper> {

	/**
	 * 
	 * findByExample:(根据查询条件查询会员渠道报表信息). <br/>
	 * Date: 2015年11月26日 下午5:23:31 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<MemberChannelSummaryDayExp> findExpByExample(Example example,Page<MemberChannelSummaryDayExp> page);
	
	/**
	 * 
	 * callMemberChannelSummary:(调用会员渠道存储过程). <br/>
	 * Date: 2015年11月30日 下午3:01:58 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	public void callMemberChannelSummary(String dateStr);
	
}

