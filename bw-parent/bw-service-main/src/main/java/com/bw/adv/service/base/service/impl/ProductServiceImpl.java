/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.base.model.ProductCategory;
import com.bw.adv.module.base.model.ProductCategoryItem;
import com.bw.adv.module.base.model.exp.ProductCategoryExp;
import com.bw.adv.module.base.model.exp.ProductCategoryItemExp;
import com.bw.adv.module.base.model.exp.ProductData;
import com.bw.adv.module.base.model.exp.ProductExp;
import com.bw.adv.module.base.repository.ProductCategoryItemRepository;
import com.bw.adv.module.base.repository.ProductCategoryRepository;
import com.bw.adv.module.base.repository.ProductRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.service.base.service.ProductService;


/**
 * ClassName: ProductServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:15:40 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService{
	
	private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);
	
	private ProductRepository productRepository;
	private ProductCategoryRepository productCategoryRepository;
	private ProductCategoryItemRepository productCategoryItemRepository;
	
	@Override
	public BaseRepository<Product, ? extends BaseMapper<Product>> getBaseRepository() {
		return productRepository;
	}

	@Override
	public List<ProductExp> queryListForProduct(Long statusId,Page<ProductExp> page,Map<String,Object> map) {
		return this.productRepository.findProductList(statusId,page,map);
	}

	@Override
	public void insertProducts(List<Product> list) {
		this.productRepository.saveList(list);
	}

	@Override
	@Transactional
	public void deleteProducts(Long productId) {
		this.productCategoryItemRepository.removeByProductId(productId);
		this.productRepository.removeByPk(productId);
	}

	@Override
	public List<Product> queryByExample(Example example, Page<Product> page) {
		return this.productRepository.findByExample(example, page);
	}

	@Override
	public List<Product> queryListForProductByProductCode(Long statusId,String productCode) {
		return this.productRepository.findProductListByProductCode(statusId, productCode);
	}
	
	@Override
	public List<Product> queryByCode(Long statusId,String productCodes) {
		return this.productRepository.findByCode(statusId, productCodes);
	}

	@Override
	public Product queryProductByPrimaryKey(Long statusId, Long productId) {
		return this.productRepository.findProductByPrimaryKey(statusId, productId);
	}

	@Override
	@Transactional
	public void renovateProduct() {
		String rulePath = "masterData/rules/categories-rule.xml";
		renovateCouponType(rulePath);
	}
	
	@Transactional
	private void renovateCouponType(String rulePath) {
		Digester digester = null;
		ClassLoader loader = null;
		ProductData productData = null;
		Product productTmp = null;
		ProductCategory categoryTmp = null;
		List<Product> productOldList = null;//已有的产品列表
		List<Product> productNewList = null;
		List<Product> productUpdateList = null;//已有的产品列表
		List<Product> productSaveList = null;
		
		Map<String, Product> productMap = null;
		List<ProductCategoryExp> categoryExpList = null;
		
		List<ProductCategory> categoryOldList = null;
		List<ProductCategory> categoryNewList = null;
		List<ProductCategory> categoryUpdateList = null;
		List<ProductCategory> categorySaveList = null;
		Map<String, ProductCategory> categoryMap = null;
		ProductCategory productCategory = null;
		Map<String, Product> productNewfilter = null;
		ProductCategoryItem item = null;
		ProductCategoryItemExp itemExp = null;
		List<ProductCategoryItem> itemLIst = null;
		List<ProductCategoryItemExp> itemExpLIst = null;
		File file = null;
		try {
	    	loader = Thread.currentThread().getContextClassLoader();
	    	digester = DigesterLoader.createDigester(loader.getResource(rulePath));
	    	
	    	productData = (ProductData) digester.parse(file);
	    	categoryNewList = new ArrayList<ProductCategory>();
	    	categoryMap = new HashMap<String, ProductCategory>();
	    	categoryExpList = productData.getCategoryExpList();
	    	productMap = new HashMap<String, Product>();
	    	productNewList = new ArrayList<Product>();
	    	productNewfilter = new HashMap<String, Product>();
	    	
	    	itemLIst = new ArrayList<ProductCategoryItem>();
	    	itemExpLIst = new ArrayList<ProductCategoryItemExp>();
	    	
	    	for (ProductCategoryExp categoryExp : categoryExpList) {
	    		productCategory = new ProductCategory();
	    		productCategory.setProductCategoryCode(categoryExp.getProductCategoryCode());
	    		productCategory.setProductCategoryName(categoryExp.getProductCategoryName());
	    		categoryNewList.add(productCategory);//添加产品类别
	    		//类别的产品明细
	    		for (Product product : categoryExp.getProductList()) {
	    			itemExp = new ProductCategoryItemExp();
	    			itemExp.setProductCategoryCode(categoryExp.getProductCategoryCode());
	    			itemExp.setProductCode(product.getProductCode());
	    			itemExpLIst.add(itemExp);
	    			//同一个产品属于多个类别
	    			if(productNewfilter.get(product.getProductCode())==null){
	    				productNewList.add(product);
	    				productNewfilter.put(product.getProductCode(), product);
	    			}
	    		}
			}
	    	categoryOldList = productCategoryRepository.findAll();
	    	productOldList = productRepository.findAllActive();
	    	
	    	//取出原来产品类别
	    	for (ProductCategory category : categoryOldList) {
	    		categoryMap.put(category.getProductCategoryCode(), category);
	    	}
	    	//取出原来产品
	    	for (Product product : productOldList) {
    			productMap.put(product.getProductCode(), product);
    		}
	    	
	    	categoryUpdateList = new ArrayList<ProductCategory>();
	    	categorySaveList = new ArrayList<ProductCategory>();
	    	for (ProductCategory category : categoryNewList) {
	    		categoryTmp = categoryMap.get(category.getProductCategoryCode());
	    		//如果包含，则为原数据重新复制
	    		if(categoryTmp != null){
	    			BeanUtils.copyNotNull(category, categoryTmp);
	    			categoryUpdateList.add(categoryTmp);
	    		}else{
	    			categorySaveList.add(category);
	    		}
	    	}
	    	
	    	productUpdateList = new ArrayList<Product>();
	    	productSaveList = new ArrayList<Product>();
	    	for (Product product : productNewList) {
	    		productTmp = productMap.get(product.getProductCode());
	    		product.setStatusId(StatusConstant.PRODUCT_ACTIVE.getId());
	    		//如果包含，则为原数据重新复制
	    		if(productTmp != null){
	    			BeanUtils.copyNotNull(product, productTmp);
	    			productUpdateList.add(productTmp);
	    		}else{
	    			productSaveList.add(product);
	    		}
	    	}
	    	
	    	if(categorySaveList.size() > 0){
	    		productCategoryRepository.saveList(categorySaveList);
	    	}
	    	if(categoryUpdateList.size() > 0){
	    		productCategoryRepository.updateList(categoryUpdateList);
	    	}
	    	
	    	//更新原来数据
	    	if(productUpdateList.size() > 0){
	    		productRepository.updateList(productUpdateList);
	    	}
	    	//保存新增数据
	    	if(productSaveList.size() > 0){
	    		productRepository.saveList(productSaveList);
	    	}
	    	
	    	
	    	//保存类别明细
	    	categoryOldList = productCategoryRepository.findAll();
	    	productOldList = productRepository.findAllActive();
	    	
	    	//取出原来产品类别
	    	for (ProductCategory category : categoryOldList) {
	    		categoryMap.put(category.getProductCategoryCode(), category);
	    	}
	    	//取出原来产品
	    	for (Product product : productOldList) {
    			productMap.put(product.getProductCode(), product);
    		}
	    	
	    	for (ProductCategoryItemExp itemExp2 : itemExpLIst) {
	    		item = new ProductCategoryItem();
	    		item.setProductCategoryId(categoryMap.get(itemExp2.getProductCategoryCode()).getProductCategoryId());
	    		item.setProductId(productMap.get(itemExp2.getProductCode()).getProductId());
	    		itemLIst.add(item);
	    	}
	    	productCategoryItemRepository.removeAll();
	    	productCategoryItemRepository.saveList(itemLIst);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Autowired
	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Autowired
	public void setProductCategoryRepository(ProductCategoryRepository productCategoryRepository) {
		this.productCategoryRepository = productCategoryRepository;
	}

	@Autowired
	public void setProductCategoryItemRepository(ProductCategoryItemRepository productCategoryItemRepository) {
		this.productCategoryItemRepository = productCategoryItemRepository;
	}

	@Override
	public List<ProductCategoryItem> queryCategoryItemById(Long productId) {
		return this.productCategoryItemRepository.queryCategoryItemById(productId);
	}

}

