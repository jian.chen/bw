package com.bw.adv.service.wechat.msg.model;

import java.util.Map;
/**
 * 消息模板类
 * ClassName: WechatTemplate <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年11月11日 下午3:52:17 <br/>
 * scrmVersion 1.0
 * @author yu.zhang
 * @version jdk1.7
 */
public class WechatTemplate {
	private String template_id;

	private String touser;

	private String url;

	private String topcolor;

	private Map<String, TemplateData> data;

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTopcolor() {
		return topcolor;
	}

	public void setTopcolor(String topcolor) {
		this.topcolor = topcolor;
	}

	public Map<String, TemplateData> getData() {
		return data;
	}

	public void setData(Map<String, TemplateData> data) {
		this.data = data;
	}
}
