package com.bw.adv.service.order.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.repository.ProductRepository;
import com.bw.adv.module.base.repository.StoreRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.coupon.repository.CouponInstanceRepository;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.repository.MemberAccountRepository;
import com.bw.adv.module.member.repository.MemberCardRepository;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.order.enums.OrderOptEnum;
import com.bw.adv.module.order.model.OrderCoupon;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.OrderHeaderExt;
import com.bw.adv.module.order.model.OrderItem;
import com.bw.adv.module.order.model.OrderPaymentMethod;
import com.bw.adv.module.order.model.dto.OrderCouponDto;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.order.model.exp.OrderHeaderExp;
import com.bw.adv.module.order.repository.OrderCouponRepository;
import com.bw.adv.module.order.repository.OrderHeaderExtRepository;
import com.bw.adv.module.order.repository.OrderHeaderRepository;
import com.bw.adv.module.order.repository.OrderItemRepository;
import com.bw.adv.module.order.repository.OrderPaymentMethodRepository;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.coupon.service.CouponService;
import com.bw.adv.service.order.service.OrderHeaderService;

@Service
public class OrderHeaderServiceImpl extends BaseServiceImpl<OrderHeader> implements OrderHeaderService {
	@Value("${is_once_coupon_single}")
	private static String IS_ONCE_COUPON_SINGLE;
	@Value("${is_old_order}")
	private static String IS_OLD_ORDER;
	@Value("${order_day_number}")
	private static Integer ORDER_DAY_NUMBER;

	private OrderHeaderRepository orderHeaderRepository;
	private OrderHeaderExtRepository orderHeaderExtRepository;
	private OrderItemRepository orderItemRepository;
	private OrderCouponRepository orderCouponRepository;
	private OrderPaymentMethodRepository orderPaymentMethodRepository;
	private MemberRepository memberRepository;
	private CodeService codeService;
	private CouponInstanceRepository couponInstanceRepository;
	private MemberAccountRepository memberAccountRepository;
	private StoreRepository storeRepository;
	private CouponService couponService;
	private MemberCardRepository memberCardRepository;
	private ProductRepository productRepository;

	@Override
	public BaseRepository<OrderHeader, ? extends BaseMapper<OrderHeader>> getBaseRepository() {
		return orderHeaderRepository;
	}
	
	@Override
	public List<OrderHeaderExp> queryExpByExample(Example example,Page<OrderHeaderExp> page) {
		return orderHeaderRepository.findExpByExample(example, page);
	}
	
	@Override
	public List<OrderHeaderExp> queryOrderExpByExample(Example example,Page<OrderHeaderExp> page) {
		return orderHeaderRepository.findorderExpByExample(example, page);
	}
	@Override
	public List<OrderHeaderExp> queryOrderExpByMemberCode(String memberCode,
			Page<OrderHeaderExp> page) {
		return orderHeaderRepository.findExpByExa(memberCode, page);
	}
	@Override
	public OrderHeaderExp queryOrderPoints(Long orderId) {
		return orderHeaderRepository.findOrderPointsById(orderId);
	}

	@Override
	public OrderHeaderExp queryOrderCoupon(Long orderId) {
		return orderHeaderRepository.findOrderCouponById(orderId);
	}
	
	@Override
	public List<OrderHeaderExp> queryOrderExpListByExample(Example example,Page<OrderHeaderExp> page) {
		return orderHeaderRepository.findExpListByExa(example, page);
	}
	@Override
	public List<OrderHeaderExp> queryOrderExpListByExa(Example example,Page<OrderHeaderExp> page) {
		return orderHeaderRepository.findExpListByExample(example, page);
	}
	/**
	 * TODO 提价订单
	 * @throws Exception 
	 * @see com.bw.adv.service.order.service.OrderHeaderService#handleSyncOrder(com.bw.adv.module.order.model.dto.OrderHeaderDto)
	 */
	@Override
	@Transactional
	public String handleSyncOrder(OrderInfo orderInfo,String optType) throws Exception{
		//订单
		OrderHeader oldOrderHeader = null;
		OrderHeader orderHeader = null;
		OrderHeaderExt orderHeaderExt = null;
		String orderCode = null;
		//订单明细
		List<OrderItem> orderItemList = null;
		List<OrderItem> orderItemAddList = null;
		//订单优惠
		List<OrderCouponDto> orderCouponDtoList = null;
		List<CouponInstanceExp> instanceExpList = null;
		CouponInstanceExp instanceExp = null;
		List<OrderCoupon> orderCouponList = null;
		//订单付款方式
		List<OrderPaymentMethod> orderPaymentMethodList = null;
		//会员账户信息
		MemberAccount memberAccount =null;
		//会员消费总金额
		BigDecimal totalConsumption =null;
		//会员消费总次数
		Integer totalConsumptionCount =null;
		Member member = null;
		String currentTime = null;
		String happenTime = null;
		String externalId = null;
		List<String> couponInstanceCodesList = null;
		String storeCode = null;
		OrderCoupon orderCoupon = null;
		Store store = null;
		String billInfo = "";
		MemberCard memberCard = null;
		String useInfo = null;
		List<String> codeList = null;
		List<Product> productList = null;
		Map<String,Product> productMap = null;
		orderCouponDtoList = orderInfo.getOrderCoupons();
		//是否优惠券每次只能使用一张
		if(StringUtils.isNotBlank(IS_ONCE_COUPON_SINGLE) && IS_ONCE_COUPON_SINGLE.equals("Y") && orderCouponDtoList != null && orderCouponDtoList.size() > 1){
			throw new MemberException(ResultStatus.COUPON_INSTANCE_USE_ERROR);
		}
		
		orderHeader = new OrderHeader();
		orderHeaderExt = new OrderHeaderExt();
		currentTime = DateUtils.getCurrentTimeOfDb();
		productList = new ArrayList<Product>();
		productMap = new HashMap<String, Product>();
		
		//保存订单主信息
		orderHeader = orderInfo.getOrderHeader();
		externalId = orderHeader.getExternalId();
		storeCode = orderHeader.getStoreCode();
		
		//新增订单
		if(optType.equals(OrderOptEnum.ORDER_OPERATE_WAY_INSERT.getKey())){
			if(ORDER_DAY_NUMBER == null){
				oldOrderHeader = orderHeaderRepository.findByExternalId(externalId);
			}else{
				oldOrderHeader = orderHeaderRepository.findByExternalIdForDayNumber(externalId, ORDER_DAY_NUMBER);
			}
			if(oldOrderHeader != null){
				throw new MemberException(ResultStatus.ORDER_CODE_EXIST_ERROR);
			}
			orderHeader.setStatusId(StatusConstant.ORDER_STATUS_SUCCESS.getId());
		}//取消单
		else if(optType.equals(OrderOptEnum.ORDER_OPERATE_WAY_DELETE.getKey())){
			//是否是原订单取消
			if(IS_OLD_ORDER != null && IS_OLD_ORDER.equals("Y")){
				if(ORDER_DAY_NUMBER == null){
					oldOrderHeader = orderHeaderRepository.findByExternalId(externalId);
				}else{
					oldOrderHeader = orderHeaderRepository.findByExternalIdForDayNumber(externalId, ORDER_DAY_NUMBER);
				}
				if(oldOrderHeader == null){
					throw new MemberException(ResultStatus.ORDER_EXIST_ERROR);
				}
				billInfo = "";
				//如果订单已经取消，直接成功
				if(oldOrderHeader.getStatusId().equals(StatusConstant.ORDER_STATUS_CANCEL.getId())){
					return billInfo;
				}
				oldOrderHeader.setStatusId(StatusConstant.ORDER_STATUS_CANCEL.getId());
				orderHeaderRepository.updateByPkSelective(oldOrderHeader);
				orderInfo.setOrderHeader(oldOrderHeader);
				return billInfo;
			}else{
				if(ORDER_DAY_NUMBER == null){
					oldOrderHeader = orderHeaderRepository.findByExternalId(externalId);
				}else{
					oldOrderHeader = orderHeaderRepository.findByExternalIdForDayNumber(externalId, ORDER_DAY_NUMBER);
				}
				if(oldOrderHeader != null){
					throw new MemberException(ResultStatus.ORDER_CODE_EXIST_ERROR);
				}
				orderHeader.setStatusId(StatusConstant.ORDER_STATUS_CANCEL.getId());
			}
		}//修改单
		else if(optType.equals(OrderOptEnum.ORDER_OPERATE_WAY_UPDATE.getKey())){
			//暂时不考虑修改单
		}else{
			throw new MemberException(ResultStatus.ORDER_SYNC_FILE_MESSING_ERROR);
		}
		
		member = memberRepository.findByMemberCode(orderInfo.getOrderHeaderDto().getMemberCode());
		if(member == null){
			throw new MemberException(ResultStatus.MEMBER_NOT_EXIST_ERROR);
		}
		
		//保存订单使用优惠
		useInfo = validateOrderCoupon(orderInfo, storeCode);
		orderHeader.setUseInfo(useInfo);
		//原订单的创建时间
		happenTime = orderHeader.getCreateTime();
		if(StringUtils.isBlank(happenTime)){
			happenTime = currentTime;
		}
		orderHeader.setMemberId(member.getMemberId());
		orderCode = codeService.generateCode(OrderHeader.class);
		orderHeader.setOrderCode(orderCode);
		orderHeader.setCreateTime(currentTime);
		orderHeader.setHappenTime(happenTime);
		if(StringUtils.isNotBlank(storeCode)){
			store = storeRepository.findByStoreCode(storeCode);
		}
		if(store != null){
			orderHeader.setStoreId(store.getStoreId());
		}
		orderHeaderRepository.save(orderHeader);
		
		//保存订单扩展信息
		orderHeaderExt = orderInfo.getOrderHeaderExt();
		orderHeaderExt.setOrderId(orderHeader.getOrderId());
		orderHeaderExtRepository.save(orderHeaderExt);
		
		//保存订单明细
		orderItemList = orderInfo.getOrderItems();
		if(orderItemList != null){
			orderItemAddList = new ArrayList<OrderItem>();
			codeList = new ArrayList<String>();
			for (OrderItem orderItem : orderItemList) {
				//过滤空的订单明细
				if(StringUtils.isBlank(orderItem.getProductCode()) && orderItem.getAmount() == null && orderItem.getQuantity() == null){
					continue;
				}
				orderItemAddList.add(orderItem);
				codeList.add(orderItem.getProductCode());
			}
			if(codeList.size() > 0){
				productList = productRepository.findByCodeList(codeList);
				for (Product product : productList) {
					productMap.put(product.getProductCode(), product);
				}
			}
			
			for (OrderItem orderItem : orderItemAddList) {
				orderItem.setOrderId(orderHeader.getOrderId());
				if(productMap.get(orderItem.getProductCode()) != null){
					orderItem.setProductId(productMap.get(orderItem.getProductCode()).getProductId());
				}
			}
			if(orderItemAddList.size() >0){
				orderItemRepository.saveList(orderItemAddList);
			}
		}
		
		//保存订单使用优惠
		if(orderCouponDtoList != null){
			instanceExpList = new ArrayList<CouponInstanceExp>();
			couponInstanceCodesList = new ArrayList<String>();
			orderCouponList = new ArrayList<OrderCoupon>();
			for (OrderCouponDto orderCouponDto : orderCouponDtoList) {
				if(StringUtils.isBlank(orderCouponDto.getCouponInstanceCode())){
					continue;
				}
				orderCoupon = new OrderCoupon();
				orderCoupon.setOrderId(orderHeader.getOrderId());
				BeanUtils.copyNotNull(orderCouponDto, orderCoupon);
				orderCouponList.add(orderCoupon);
				couponInstanceCodesList.add(orderCouponDto.getCouponInstanceCode());
			}
			if(couponInstanceCodesList.size() > 0){
				//核销优惠券
				instanceExpList = couponInstanceRepository.findExpListByCodes(couponInstanceCodesList);
				//给订单使用优惠券变赋值优惠券实例ID
				for (int i = 0; i < orderCouponList.size(); i++) {
					orderCouponList.get(i).setCouponInstanceId(instanceExpList.get(i).getCouponInstanceId());
				}
				orderCouponRepository.saveList(orderCouponList);
			}
		}
		
		//保存订单付款方式
		orderPaymentMethodList = orderInfo.getOrderPaymentMethods();
		if(orderPaymentMethodList != null){
			for (OrderPaymentMethod orderPaymentMethod : orderPaymentMethodList) {
				orderPaymentMethod.setOrderId(orderHeader.getOrderId());
			}
			orderPaymentMethodRepository.saveList(orderPaymentMethodList);
		}
		
		memberAccount = memberAccountRepository.findByMemberId(member.getMemberId());
		//更新会员账户信息
		totalConsumption = memberAccount.getTotalConsumption();// 会员消费总金额
		totalConsumptionCount = memberAccount.getTotalConsumptionCount();// 会员消费总次数
		// 首次消费次数置为0
		totalConsumptionCount = totalConsumptionCount == null ? 0 : totalConsumptionCount;
		totalConsumption = totalConsumption == null ? BigDecimal.ZERO : totalConsumption;

		if(optType.equals(OrderOptEnum.ORDER_OPERATE_WAY_INSERT.getKey())){
			totalConsumptionCount++;
			
		}else if(optType.equals(OrderOptEnum.ORDER_OPERATE_WAY_DELETE.getKey())){
			totalConsumptionCount--;
		}
		
		if(orderHeader.getPayAmount() != null){
			totalConsumption = totalConsumption.add(orderHeader.getPayAmount());
		}
		memberAccount.setTotalConsumption(totalConsumption);
		memberAccount.setTotalConsumptionCount(totalConsumptionCount);
		memberAccountRepository.updateByPk(memberAccount);
		
		//更新会员最近消费信息
		member.setLastConsumptionTime(happenTime);
		//保存会员的首次消费时间
		if(StringUtils.isBlank(member.getFirstConsumptionTime())){
			member.setFirstConsumptionTime(currentTime);
		}
		memberRepository.updateByPkSelective(member);
		
		orderInfo.setSaleCount(totalConsumptionCount);
		
		memberCard = memberCardRepository.findByMemberId(member.getMemberId());
		billInfo = "SUCCESS";
		/*billInfo += "订单号:"+externalId+"\r\n";
		if(memberCard != null && StringUtils.isNotBlank(memberCard.getCardNo())){
			billInfo += "会员卡号:" + getEncryptCardNo(memberCard.getCardNo())+"\r\n";
		}else{
			billInfo += "\r\n";
		}
		billInfo += "皇冠数量:"+ (memberAccount.getPointsBalance() == null ? 0 : memberAccount.getPointsBalance().intValue())+"\r\n";*/
		return billInfo;
	}
	
	/**
	 * getEncryptCardNo:(卡号加密隐藏). <br/>
	 * Date: 2016-1-12 上午11:23:33 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param encryptCarNo
	 * @return
	 */
	private String getEncryptCardNo(String cardNo){
		String encryptCardNo = "";
		encryptCardNo = cardNo.substring(0,2);
		for (int i = 0; i < cardNo.substring(2,cardNo.length()-2).length(); i++) {
			encryptCardNo += "*"; 
		}
		encryptCardNo += cardNo.substring(cardNo.length()-2); 
		return encryptCardNo;
	}


	/**
	 * validateOrderCoupon:(校验订单使用优惠券的优惠券，并返回使用的优惠券名称). <br/>
	 * Date: 2016-1-5 下午1:46:18 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param orderInfo
	 * @param storeCode
	 * @param useInfo
	 * @return
	 * @throws Exception
	 */
	private String validateOrderCoupon(OrderInfo orderInfo, String storeCode) throws Exception {
		List<OrderCouponDto> orderCouponDtoList = null;
		List<CouponInstanceExp> instanceExpList = null;
		List<String> couponInstanceCodesList = null;
		CouponInstanceExp instanceExp = null;
		Map<Long, CouponInstanceExp> couponMap = null;
		Map<Long, Integer> couponAmountMap = null;
		String useInfo = "";
		Integer amount = 0;
		
		couponMap = new HashMap<Long, CouponInstanceExp>();
		couponAmountMap = new HashMap<Long, Integer>();
		orderCouponDtoList = orderInfo.getOrderCoupons();
		
		if(orderCouponDtoList != null){
			instanceExpList = new ArrayList<CouponInstanceExp>();
			couponInstanceCodesList = new ArrayList<String>();
			for (OrderCouponDto orderCouponDto : orderCouponDtoList) {
				couponInstanceCodesList.add(orderCouponDto.getCouponInstanceCode());
			}
			if(couponInstanceCodesList.size() > 0){
				//核销优惠券
				couponService.verificationOrderCoupon(couponInstanceCodesList, storeCode, null);
				instanceExpList = couponInstanceRepository.findExpListByCodes(couponInstanceCodesList);
				
				for (int i = 0; i < instanceExpList.size(); i++) {
					instanceExp = instanceExpList.get(i);
					if(couponAmountMap.get(instanceExp.getCouponId()) == null){
						couponAmountMap.put(instanceExp.getCouponId(), 1);
					}else{
						amount = couponAmountMap.get(instanceExp.getCouponId());
						amount++;
						couponAmountMap.put(instanceExp.getCouponId(), amount);
					}
					couponMap.put(instanceExp.getCouponId(), instanceExp);
				}
				
				for (Map.Entry<Long, Integer> entry : couponAmountMap.entrySet()) {  
				    useInfo += couponMap.get(entry.getKey()).getCouponName() + "x" + entry.getValue()+",";
				}
				if(StringUtils.isNotBlank(useInfo)){
					useInfo = useInfo.substring(0,useInfo.length()-1);
				}
			}
		}
		return useInfo;
	}
	
	@Autowired
	public void setOrderHeaderExtRepository(OrderHeaderExtRepository orderHeaderExtRepository) {
		this.orderHeaderExtRepository = orderHeaderExtRepository;
	}

	@Autowired
	public void setOrderHeaderRepository(OrderHeaderRepository orderHeaderRepository) {
		this.orderHeaderRepository = orderHeaderRepository;
	}

	@Autowired
	public void setOrderItemRepository(OrderItemRepository orderItemRepository) {
		this.orderItemRepository = orderItemRepository;
	}

	@Autowired
	public void setOrderPaymentMethodRepository(OrderPaymentMethodRepository orderPaymentMethodRepository) {
		this.orderPaymentMethodRepository = orderPaymentMethodRepository;
	}

	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}

	@Autowired
	public void setOrderCouponRepository(OrderCouponRepository orderCouponRepository) {
		this.orderCouponRepository = orderCouponRepository;
	}
	
	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}
	
	@Autowired
	public void setMemberAccountRepository(MemberAccountRepository memberAccountRepository) {
		this.memberAccountRepository = memberAccountRepository;
	}

	@Autowired
	public void setStoreRepository(StoreRepository storeRepository) {
		this.storeRepository = storeRepository;
	}
	
	@Autowired
	public void setCouponInstanceRepository(CouponInstanceRepository couponInstanceRepository) {
		this.couponInstanceRepository = couponInstanceRepository;
	}

	@Autowired
	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

	@Autowired
	public void setMemberCardRepository(MemberCardRepository memberCardRepository) {
		this.memberCardRepository = memberCardRepository;
	}

	@Autowired
	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public List<OrderItem> queryOrderItemByOrderId(Long orderId, Page<OrderItem> page) {
		return this.orderItemRepository.queryOrderItemByOrderId(orderId, page);
	}

	@Override
	public List<Product> queryProductListForJS() {
		return this.orderItemRepository.queryProductListForJS();
	}

	@Override
	public List<OrderHeaderExp> showShopOrderCount(String storeCode, String date) {
		return this.orderHeaderRepository.showShopOrderCount(storeCode, date);
	}

}