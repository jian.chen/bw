/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SysRoleServiceImpl.java
 * Package Name:com.sage.scrm.service.sys.service.impl
 * Date:2015-8-14下午4:48:43
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.sys.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.sys.model.SysRoleFuncKey;
import com.bw.adv.module.sys.repository.SysRoleFuncRepository;
import com.bw.adv.service.sys.service.SysRoleFuncService;

/**
 * ClassName:SysRoleServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-14 下午4:48:43 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
@Service
public class SysRoleFuncServiceImpl extends BaseServiceImpl<SysRoleFuncKey> implements SysRoleFuncService {

	private SysRoleFuncRepository sysRoleFuncRepository;

	@Override
	public BaseRepository<SysRoleFuncKey, ? extends BaseMapper<SysRoleFuncKey>> getBaseRepository() {

		return sysRoleFuncRepository;
	}

	@Override
	public void insertSysRoleFuncs(List<SysRoleFuncKey> list) {
		for(SysRoleFuncKey sysRoleFuncKey:list){
			this.sysRoleFuncRepository.save(sysRoleFuncKey);
		}

	}

	@Override
	public void updateSysRoleFuncs(List<SysRoleFuncKey> list) {
		for(SysRoleFuncKey sysRoleFuncKey:list){
			this.sysRoleFuncRepository.updateByPkSelective(sysRoleFuncKey);
		}

	}

	@Autowired
	public void setSysRoleFuncRepository(SysRoleFuncRepository sysRoleFuncRepository) {
		this.sysRoleFuncRepository = sysRoleFuncRepository;
	}

	@Override
	public List<Map<String, Object>> queryCheckedSysRoleFuncList(Long roleId) {
		return this.sysRoleFuncRepository.findCheckedSysRoleFuncList(roleId);
	}

	@Override
	public List<SysRoleFuncKey> queryByUserId(Long userId) {
		if(userId == null || userId <= 0)
			throw new IllegalArgumentException("SysRoleFuncServiceImpl: argument[userId] is null");
		return this.sysRoleFuncRepository.findByUserId(userId);
	}

	@Override
	public List<SysRoleFuncKey> queryCheckedSysRoleFuncLists(Long roleId) {
		return this.sysRoleFuncRepository.selectCheckedSysRoleFuncLists(roleId);
	}

}

