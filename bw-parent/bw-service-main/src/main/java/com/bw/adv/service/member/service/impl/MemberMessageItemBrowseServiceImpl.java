package com.bw.adv.service.member.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.member.message.model.MemberMessageItemBrowse;
import com.bw.adv.module.member.message.repository.MemberMessageItemBrowseRepository;
import com.bw.adv.service.member.service.MemberMessageItemBrowseService;

@Service
public class MemberMessageItemBrowseServiceImpl extends BaseServiceImpl<MemberMessageItemBrowse> implements MemberMessageItemBrowseService {

	private MemberMessageItemBrowseRepository memberMessageItemBrowseRepository;
	
	@Override
	public BaseRepository<MemberMessageItemBrowse, ? extends BaseMapper<MemberMessageItemBrowse>> getBaseRepository() {
		
		return memberMessageItemBrowseRepository;
	}
	
	@Autowired
	public void setMemberMessageItemBrowseRepository(
			MemberMessageItemBrowseRepository memberMessageItemBrowseRepository) {
		this.memberMessageItemBrowseRepository = memberMessageItemBrowseRepository;
	}
	
}
