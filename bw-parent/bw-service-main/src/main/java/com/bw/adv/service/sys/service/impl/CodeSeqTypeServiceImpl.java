/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CodeSeqTypeServiceImpl.java
 * Package Name:com.sage.scrm.service.sys.service.impl
 * Date:2016年4月6日下午5:31:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.sys.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.base.model.CodeSeqType;
import com.bw.adv.module.base.repository.CodeSeqTypeRepository;
import com.bw.adv.service.sys.service.CodeSeqTypeService;

/**
 * ClassName:CodeSeqTypeServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年4月6日 下午5:31:18 <br/>
 * scrmVersion 1.0
 * @author   chengdi.cui
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class CodeSeqTypeServiceImpl extends BaseServiceImpl<CodeSeqType> implements CodeSeqTypeService{
	
	private CodeSeqTypeRepository codeSeqTypeRepository;

	@Override
	public BaseRepository<CodeSeqType, ? extends BaseMapper<CodeSeqType>> getBaseRepository() {
		return codeSeqTypeRepository;
	}

	@Autowired
	public void setCodeSeqTypeRepository(CodeSeqTypeRepository codeSeqTypeRepository) {
		this.codeSeqTypeRepository = codeSeqTypeRepository;
	}

	@Override
	public CodeSeqType queryByTypeCode(String typeCode) {
		return codeSeqTypeRepository.queryByTypeCode(typeCode);
	}
	
}

