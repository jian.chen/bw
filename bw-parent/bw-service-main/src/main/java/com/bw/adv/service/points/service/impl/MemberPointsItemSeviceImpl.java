/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberPointsItemSeviceImpl.java
 * Package Name:com.sage.scrm.service.points.service.impl
 * Date:2015年8月20日下午6:17:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.exception.MemberPointsItemException;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.repository.MemberAccountRepository;
import com.bw.adv.module.points.constant.PointsTypeConstant;
import com.bw.adv.module.points.model.MemberPointsItem;
import com.bw.adv.module.points.model.MemberPointsItemRel;
import com.bw.adv.module.points.model.PointsType;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.points.model.exp.MemberPointsItemStoreExp;
import com.bw.adv.module.points.repository.MemberPointsItemRelRepository;
import com.bw.adv.module.points.repository.MemberPointsItemRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;
import com.bw.adv.service.points.service.MemberPointsItemSevice;

/**
 * ClassName:MemberPointsItemSeviceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:17:28 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class MemberPointsItemSeviceImpl extends BaseServiceImpl<MemberPointsItem> implements MemberPointsItemSevice {
	
	@Value("${batch_insert_num}")
	private static Integer BATCH_INSERT_NUM ;
	@Value("${order_get_info_prefix_points}")
	private static String ORDER_GET_INFO_PREFIX_POINTS;

	private MemberPointsItemRepository memberPointsItemRepository;
	private MemberAccountRepository memberAccountRepository;
	private MemberPointsItemRelRepository memberPointsItemRelRepository;
	private WechatMsgQueueHRepository wechatMsgQueueHRepository;
	

	@Override
	public BaseRepository<MemberPointsItem, ? extends BaseMapper<MemberPointsItem>> getBaseRepository() {
		return memberPointsItemRepository;
	}
	
	@Override
	public List<MemberPointsItemExp> queryExpByExample(Example example,Page<MemberPointsItemExp> page) {
		return memberPointsItemRepository.findExpByExample(example,page);
	}
	
	
	@Override
	public List<PointsType> queryPointsTypeList() {
		return memberPointsItemRepository.findPointsTypeList();
	}
	
	
	/**
	 * TODO 积分清理
	 * @see com.bw.adv.service.points.service.MemberPointsItemSevice#cleanPointsItem()
	 */
	@Transactional
	public void cleanPointsItem() {
		String invalidDate = null;
		List<MemberPointsItemExp> itemList = null;
		List<MemberPointsItem> itemUpdateList = null;
		MemberPointsItem item = null;
		String currentTime = null;
		MemberAccount memberAccount = null;
		List<MemberAccount> memberAccountList = null;
		Map<Long, MemberAccount> accountMap = null;
		try {
			invalidDate = DateUtils.getCurrentDateOfDb();
			currentTime = DateUtils.getCurrentTimeOfDb();
			itemList = memberPointsItemRepository.findInvalidDatePointsItems(invalidDate);
			itemUpdateList = new ArrayList<MemberPointsItem>();
			accountMap = new HashMap<Long, MemberAccount>();
			
			for (MemberPointsItemExp exp : itemList) {
				if(accountMap.get(exp.getMemberAccountId()) == null){
					memberAccount = new MemberAccount();
					memberAccount.setMemberAccountId(exp.getMemberAccountId());
					memberAccount.setPointsBalance(exp.getAccountPointsBalance().subtract(exp.getAvailablePointsNumber()));
					memberAccount.setLastPointsTime(currentTime);
					memberAccount.setMemberId(exp.getMemberId());
					accountMap.put(exp.getMemberAccountId(), memberAccount);
				}else{
					memberAccount = accountMap.get(exp.getMemberAccountId());
					memberAccount.setPointsBalance(memberAccount.getPointsBalance().subtract(exp.getAvailablePointsNumber()));
					accountMap.put(exp.getMemberAccountId(), memberAccount);
				}
				
				item = new MemberPointsItem();
				item.setMemberPointsItemId(exp.getMemberPointsItemId());
				item.setAvailablePointsNumber(BigDecimal.ZERO);//可用积分清零
				item.setExpirePointsNumber(exp.getAvailablePointsNumber());//保存过期的积分数量
				item.setUpdateTime(currentTime);
				item.setPointsBalance(exp.getPointsBalance());
				item.setMemberId(exp.getMemberId());
				item.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_EXPIRE.getId());
				itemUpdateList.add(item);
			}
			
			memberAccountList = new ArrayList<MemberAccount>();
			for (MemberAccount account : accountMap.values()) {
				memberAccountList.add(account);
			}
			
			batchUpdate(itemUpdateList,memberAccountList);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new MemberPointsItemException("积分清理异常:" + e.getMessage());
		}
		
	}
	

	/**
	 * batchUpdate:(批量更新). <br/>
	 * Date: 2016-1-14 下午12:14:36 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param itemUpdateList
	 * @param memberAccountList
	 */
	@Transactional
	public void batchUpdate(List<MemberPointsItem> itemUpdateList, List<MemberAccount> memberAccountList){
		if(itemUpdateList.size() > 0){
			batchUpdatePoints(0,itemUpdateList);
		}
		
		if(memberAccountList.size() > 0){
			batchUpdateAccount(0,memberAccountList);
		}
		
	}
	
	/**
	 * subtractPoints:(扣减积分). <br/>
	 * Date: 2016-1-17 下午3:03:53 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId：会员ID
	 * @param usePoints：积分数量
	 * @param type：退单或者兑换使用
	 * @param reason：原因
	 */
	public void subtractPoints(Long memberId,BigDecimal usePoints,String type,String reason){
		subtractPoints(memberId,usePoints,type,reason,null,null);
	}
	
	/**
	 * subtractPoints:(扣减积分). <br/>
	 * Date: 2016-1-17 下午3:03:53 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId：会员ID
	 * @param usePoints：积分数量
	 * @param type：退单或者兑换使用
	 * @param reason：原因
	 */
	public void subtractPoints(Long memberId,BigDecimal usePoints,String type,String reason,Long orderId,String externalId){
		MemberAccount memberAccount = null;
		MemberPointsItem memberPointsItem = null;
		MemberPointsItemRel itemRel = null;
		List<MemberPointsItem> itemList = null;
		List<MemberPointsItem> itemUsedList = null;
		List<MemberPointsItemRel> itemRelList = null;
		String currentTime = null;
		BigDecimal availablePoints = null;
		BigDecimal tmpPoints = null;
		BigDecimal userOldPoints = null;
		int seq = 0;
		int size = 30;
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		itemUsedList = new ArrayList<MemberPointsItem>();
		itemRelList = new ArrayList<MemberPointsItemRel>();
				
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		itemUsedList = new ArrayList<MemberPointsItem>();
		itemRelList = new ArrayList<MemberPointsItemRel>();
		availablePoints = BigDecimal.ZERO;
		
		usePoints = usePoints.abs();//取绝对值
		userOldPoints = usePoints;
		
		
		memberAccount = memberAccountRepository.findByMemberId(memberId);
		if(memberAccount.getPointsBalance() == null){
			memberAccount.setPointsBalance(BigDecimal.ZERO);
		}
		if(type.equals(StatusConstant.MEMBER_POINTS_ITEM_USED.getCode()) && usePoints.compareTo(memberAccount.getPointsBalance()) > 0){
			throw new MemberPointsItemException(ResultStatus.MEMBER_POINTS_NOT_ENOUGH);
		}
		
		memberAccount.setPointsBalance(memberAccount.getPointsBalance().subtract(usePoints));//修改账户积分

		itemList = memberPointsItemRepository.findAvailablePointsItems(memberId);
		memberPointsItem = new MemberPointsItem();
		memberPointsItem.setCreateTime(currentTime);
		memberPointsItem.setHappenTime(currentTime);
		memberPointsItem.setItemPointsNumber(usePoints.negate());
		memberPointsItem.setMemberId(memberId);
		memberPointsItem.setAvailablePointsNumber(new BigDecimal(0));
		memberPointsItem.setUpdateTime(currentTime);
		memberPointsItem.setPointsBalance(memberAccount.getPointsBalance());
		memberPointsItem.setPointsReason(reason);
		memberPointsItem.setPointsTypeId(PointsTypeConstant.POINTS_EXCHANGE.getId());
		
		if(type.equals(StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getCode())){
			memberPointsItem.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getId());
		}else{
			memberPointsItem.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_USED.getId());
		}
		
		memberPointsItem.setOrderId(orderId);
		memberPointsItem.setExternalId(externalId);
		memberPointsItemRepository.save(memberPointsItem);//生成积分明细
		
		for (MemberPointsItem item : itemList) {
			availablePoints = availablePoints.add(item.getAvailablePointsNumber());
			//如果可积分大于使用的积分
			if(item.getAvailablePointsNumber().compareTo(usePoints) >= 0){
				tmpPoints = usePoints;
				item.setAvailablePointsNumber(item.getAvailablePointsNumber().subtract(usePoints));
			}else{
				tmpPoints = item.getAvailablePointsNumber();
				item.setAvailablePointsNumber(BigDecimal.ZERO);
			}
			
			//判断是不是取消单
			if(type.equals(StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getCode())){
				item.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getId());
			}else{
				item.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId());
			}
			item.setUpdateTime(currentTime);
			itemUsedList.add(item);
			
			//积分使用的对应关系
			seq++;
			itemRel = new MemberPointsItemRel();
			itemRel.setPointsItemInId(memberPointsItem.getMemberPointsItemId());
			itemRel.setPointsItemOutId(item.getMemberPointsItemId());
			itemRel.setItemPointsNumber(tmpPoints);
			itemRel.setItemRelTime(currentTime);
			itemRel.setSeq(seq);
			itemRelList.add(itemRel);
			//要使用的积分数，减去已使用的数量
			usePoints = usePoints.subtract(tmpPoints);
			if(availablePoints.compareTo(userOldPoints) >= 0){
				break;
			}
		}
		//判断明细中可用积分是否够用
		if(type.equals(StatusConstant.MEMBER_POINTS_ITEM_USED.getCode()) && availablePoints.compareTo(userOldPoints) < 0){
			throw new MemberPointsItemException(ResultStatus.MEMBER_POINTS_NOT_ENOUGH);
		}
		
		if(itemRelList.size() > 0 ){
			memberPointsItemRelRepository.saveList(itemRelList);
		}
		if(itemUsedList.size() > 0 ){
			memberPointsItemRepository.updateList(itemUsedList);
		}
		//判断是不是取消单
		if(type.equals(StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getCode())){
			memberAccountRepository.updateForSubtractPointsWithCancelOrder(memberId, userOldPoints, currentTime);
		}else{
			memberAccountRepository.updateForSubtractPoints(memberId, userOldPoints, currentTime);//修改会员账号积分
		}
		
		
		
	}
	
	
	
	/**
	 * batchUpdateAccount:(分批更新会员账户). <br/>
	 * Date: 2015-10-10 下午8:25:37 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param index
	 * @param memberAccountList
	 */
	private void batchUpdateAccount(int index,List<MemberAccount> memberAccountList){
		int batchAddSize = 500;
		
		if(BATCH_INSERT_NUM != null && BATCH_INSERT_NUM > 0){
			batchAddSize = BATCH_INSERT_NUM;
		}
		
		if(index < memberAccountList.size()){
			if(index + batchAddSize < memberAccountList.size()){
				memberAccountRepository.updateBatchByCleanPointsBalance(memberAccountList.subList(index, index + batchAddSize));
				//判断下次下标的值是否大于总条数，如果大于则加1否则加batchAddSize
				index = (index + batchAddSize) > memberAccountList.size() ? index++ : (index + batchAddSize);
				batchUpdateAccount(index,memberAccountList);
			}else{
				memberAccountRepository.updateBatchByCleanPointsBalance(memberAccountList.subList(index, memberAccountList.size()));
			}
		}
	}
	
	/**
	 * batchUpdatePoints:(分批更新积分记录). <br/>
	 * Date: 2015-10-10 下午8:25:39 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param index
	 * @param itemUpdateList
	 */
	private void batchUpdatePoints(int index,List<MemberPointsItem> itemUpdateList){
		int batchAddSize = 500;
		MemberPointsItem pointsItem = null;
		List<MemberPointsItem> itemList = null;
		String currentTime = null;
		
		if(BATCH_INSERT_NUM != null && BATCH_INSERT_NUM > 0){
			batchAddSize = BATCH_INSERT_NUM;
		}
		
		if(index < itemUpdateList.size()){
			if(index + batchAddSize < itemUpdateList.size()){
				memberPointsItemRepository.updateList(itemUpdateList.subList(index, index + batchAddSize));
				//判断下次下标的值是否大于总条数，如果大于则加1否则加batchAddSize
				index = (index + batchAddSize) > itemUpdateList.size() ? index++ : (index + batchAddSize);
				batchUpdatePoints(index,itemUpdateList);
			}else{
				memberPointsItemRepository.updateList(itemUpdateList.subList(index, itemUpdateList.size()));
			}
		}
		
		if(itemUpdateList.size() > 0){
			itemList = new ArrayList<MemberPointsItem>();
			currentTime = DateUtils.getCurrentTimeOfDb();
			for (MemberPointsItem item : itemUpdateList) {
				pointsItem = new MemberPointsItem();
				pointsItem.setParentItemId(item.getMemberPointsItemId());
				pointsItem.setItemPointsNumber(item.getExpirePointsNumber().negate());
				pointsItem.setAvailablePointsNumber(BigDecimal.ZERO);
				pointsItem.setMemberId(item.getMemberId());
				pointsItem.setCreateTime(currentTime);
				pointsItem.setHappenTime(currentTime);
				pointsItem.setPointsReason("过期清理");
				pointsItem.setPointsBalance(item.getPointsBalance());
				pointsItem.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_EXPIRE.getId());
				itemList.add(pointsItem);
			}
			memberPointsItemRepository.saveList(itemList);
		}
	}
	
	/**
	 * 保存积分调账信息.
	 */
	@Override
	@Transactional
	public int savePointsRevise(MemberPointsItemExp memberPointsItemExp){
		int result = 0;
		String invalidDate = "";
		String time = "";
		Long memberId = null;
		BigDecimal pointReviseNumber = null;
		BigDecimal pointsBalance = null;      
		MemberPointsItem memberPointsItem = null;
		MemberAccount memberAccount = null;
		MemberPointsItem pointsItem = null;
		
		memberPointsItem = new MemberPointsItem();
		memberAccount = new MemberAccount();
		pointsItem = new MemberPointsItem();
		if(StringUtils.isNotEmpty(memberPointsItemExp.getInvalidDate())){
		    invalidDate = memberPointsItemExp.getInvalidDate();
			invalidDate = invalidDate.replace("-", "");
			memberPointsItem.setInvalidDate(invalidDate);	
		}
		//积分调账数值
		if(memberPointsItemExp.getItemPointsNumber() != null){
			pointReviseNumber = memberPointsItemExp.getItemPointsNumber();			
		}
			        
	    if(memberPointsItemExp.getMemberId() != null){
	        memberId = memberPointsItemExp.getMemberId();		        	
	     }
	    
	    //查询会员积分余额
	    pointsItem = memberPointsItemRepository.findListSearchAuto(memberPointsItemExp.getMemberCode());
	    
	    if(pointsItem != null){
	    	pointsBalance = pointsItem.getPointsBalance();
	    }
	    
	    time = DateUtils.getCurrentTimeOfDb();
	    memberPointsItem.setMemberId(memberId);
	    memberPointsItem.setPointsRuleId(StatusConstant.POINTS_REVISE_RULE_ID.getId());//默认积分调账关联的积分规则
	    memberPointsItem.setActivityInstanceId(StatusConstant.POINTS_REVISE_ACTIVITY_ID.getId());//默认积分调账关联的活动实例
	    memberPointsItem.setCreateTime(time);
	    memberPointsItem.setHappenTime(time);
	    memberPointsItem.setItemPointsNumber(pointReviseNumber);
	    memberPointsItem.setPointsBalance(pointsBalance);
	    memberPointsItem.setStatusId(memberPointsItemExp.getStatusId());
	    if(memberPointsItemExp.getCreateBy() != null){
	    	memberPointsItem.setCreateBy(memberPointsItemExp.getCreateBy());	    	
	    }
	    if(memberPointsItemExp.getPointsTypeId() != null){
	        memberPointsItem.setPointsTypeId(memberPointsItemExp.getPointsTypeId());        	
	    }   
	    if(StringUtils.isNotEmpty(memberPointsItemExp.getPointsReason())){
	        memberPointsItem.setPointsReason(memberPointsItemExp.getPointsReason());        	
	    }
		//新增积分
		if(pointReviseNumber.compareTo(BigDecimal.ZERO) > 0){
			memberPointsItem.setAvailablePointsNumber(pointReviseNumber);
		}
		
		memberAccount.setMemberId(memberId);
		memberAccount.setLastPointsTime(time);
		//会员账号新增积分
		memberPointsItemRepository.updateMemberAccount(memberAccount,pointReviseNumber);
		
		//新增调账记录
		result = memberPointsItemRepository.insertPointsRevise(memberPointsItem);
		
		//扣减积分
		if(pointReviseNumber.compareTo(BigDecimal.ZERO) < 0){
			substrMemberPoints(memberPointsItem,pointReviseNumber);
		}
		  
		return result;
	}
	
	@Override
	public String substrMemberPoints(MemberPointsItem memberPointsItem,BigDecimal pointReviseNumber) {
		String result = "";
		String time = "";
		BigDecimal availablePointsNumber = null;
		List<MemberPointsItemExp> pointsList = null;
		MemberPointsItemRel  memberPointsItemRel = null;
		
		result = "积分调账扣减成功";
		time = DateUtils.getCurrentTimeOfDb();
		availablePointsNumber = pointReviseNumber.abs(); //调账积分负数转正数
		
		//查询会员即将过期的增加积分记录
		memberPointsItem.setCreateTime(DateUtils.getCurrentDateOfDb());//根据创建时间去查询
		pointsList = memberPointsItemRepository.findAddPointsInfo(memberPointsItem);
		
		for(int i = 0; i<pointsList.size();i++){
			
			MemberPointsItemExp mPointsExp = pointsList.get(i);
			mPointsExp.setUpdateBy(memberPointsItem.getCreateBy());
			mPointsExp.setUpdateTime(time);
						
			memberPointsItemRel = new MemberPointsItemRel();
			memberPointsItemRel.setItemRelTime(time);
			memberPointsItemRel.setSeq(i+1);
			
			//查询出的可用积分额度大于等于调账扣减的积分时，更新最近的一条新增积分记录
			if(mPointsExp.getAvailablePointsNumber().compareTo(availablePointsNumber) >= 0){
				
				//增加积分扣除记录关联关系
				memberPointsItemRel.setItemPointsNumber(availablePointsNumber);
				memberPointsItemRel.setPointsItemInId(mPointsExp.getMemberPointsItemId());
				memberPointsItemRel.setPointsItemOutId(memberPointsItem.getMemberPointsItemId());
				memberPointsItemRepository.insertPointsItemRel(memberPointsItemRel);
				
				//更新积分记录表的可用积分额度
				mPointsExp.setAvailablePointsNumber(mPointsExp.getAvailablePointsNumber().subtract(availablePointsNumber));
				memberPointsItemRepository.updateAvailablePointsInfo(mPointsExp);
				
				return result;
			}
			//查询出的可用积分额度小于调账扣减的积分，需要循环扣减新增积分的可用积分，扣完为止
			if(mPointsExp.getAvailablePointsNumber().compareTo(availablePointsNumber) < 0){
				//增加积分扣除记录关联关系
				memberPointsItemRel.setItemPointsNumber(mPointsExp.getAvailablePointsNumber());
				memberPointsItemRel.setPointsItemInId(mPointsExp.getMemberPointsItemId());
				memberPointsItemRel.setPointsItemOutId(memberPointsItem.getMemberPointsItemId());
				memberPointsItemRepository.insertPointsItemRel(memberPointsItemRel);
				
				//更新积分记录可用额度
				availablePointsNumber = availablePointsNumber.subtract(mPointsExp.getAvailablePointsNumber());
				mPointsExp.setAvailablePointsNumber(BigDecimal.ZERO);
				memberPointsItemRepository.updateAvailablePointsInfo(mPointsExp);
			}
		}
		//扣除积分还有剩余，说明增加的积分额度不足
		if(availablePointsNumber.compareTo(BigDecimal.ZERO) > 0){
			throw new MemberPointsItemException("积分可用余额不足");
		}
		
		return result;
	}
	
	public String operatePoints(Long memberId,List<MemberPointsItem> pointsResults) {
		MemberAccount memberAccount = null;
		BigDecimal pointsBalance = BigDecimal.ZERO;
		BigDecimal itemTotal = BigDecimal.ZERO;
		BigDecimal pointsAmount = BigDecimal.ZERO;
		String currentTime = null;
		List<MemberPointsItem> addPointsResults = null;
		String getInfo = null;
		
		if(pointsResults == null || pointsResults.size() == 0){
			return null;
		}
		
		if(pointsResults.size() == 1 && pointsResults.get(0).getItemPointsNumber().compareTo(BigDecimal.ZERO) < 0){
			this.subtractPoints(memberId, pointsResults.get(0).getItemPointsNumber(), StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getCode(), "取消单扣减"+ORDER_GET_INFO_PREFIX_POINTS,pointsResults.get(0).getOrderId(),pointsResults.get(0).getExternalId());
			getInfo = "扣减"+ORDER_GET_INFO_PREFIX_POINTS+pointsResults.get(0).getItemPointsNumber();
			return getInfo;
		}
		
		addPointsResults = new ArrayList<MemberPointsItem>();
		currentTime = DateUtils.getCurrentTimeOfDb();
		memberAccount = memberAccountRepository.findByMemberIdForUpdate(memberId);
		
		pointsBalance = memberAccount.getPointsBalance() == null ? BigDecimal.ZERO : memberAccount.getPointsBalance();
		
		for (MemberPointsItem item : pointsResults){
			pointsAmount = item.getItemPointsNumber();
			pointsBalance = pointsBalance.add(pointsAmount);
			itemTotal = itemTotal.add(pointsAmount);
			
			item.setPointsBalance(pointsBalance);
			item.setItemPointsNumber(pointsAmount);
			item.setAvailablePointsNumber(pointsAmount);
			item.setHappenTime(currentTime);
			item.setCreateTime(currentTime);
			item.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId());
			//有效期是永久，或者未过期
			addPointsResults.add(item);
		}
		
		if(addPointsResults.size() > 0){
			memberPointsItemRepository.saveList(addPointsResults);
			memberAccountRepository.updateForAddPoints(memberId, itemTotal, currentTime);
			getInfo = "getPoints:"+itemTotal;
		}
		
		//添加微信消息
		if(addPointsResults.size() > 0 ){
			sendWechatMsg(memberId, addPointsResults);
		}
		
		return getInfo;
	}
	
	private void sendWechatMsg(Long memberId , List<MemberPointsItem> addPointsResults){
		JSONObject json = null;
		List<WechatMsgQueueH> msgQueueHList = new ArrayList<WechatMsgQueueH>();
		for(MemberPointsItem memberPointsItem : addPointsResults){
			if(!memberPointsItem.getPointsTypeId().equals(PointsTypeConstant.POINTS_TYPE_ORDER.getId())){
				json = new JSONObject();
				String getDate = DateUtils.dateStrToNewFormat(memberPointsItem.getCreateTime(), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss, DateUtils.DATE_PATTERN_YYYYMMDDHHmm_3);
				json.put("reason", memberPointsItem.getPointsReason());
				json.put("getDate", getDate);
				json.put("currentPoints", memberPointsItem.getPointsBalance());
				json.put("getPoints", memberPointsItem.getItemPointsNumber());
				WechatMsgQueueH msgQueueH = new WechatMsgQueueH();
				msgQueueH.setMemberId(memberId);
				msgQueueH.setBusinessType(WechatMsgBusinessTypeEnum.POINTS_UPDATE.getId());
				msgQueueH.setCreateTime(DateUtils.getCurrentTimeOfDb());
				msgQueueH.setMsgContent(json.toString());
				msgQueueH.setIsSend("N");
				msgQueueHList.add(msgQueueH);
			}
		}
		if(msgQueueHList.size() > 0 ){
			wechatMsgQueueHRepository.saveList(msgQueueHList);
		}
		
	}

	@Override
	public List<MemberPointsItemStoreExp> selectStoreExpByMemberCode(String memberCode, int pageSize, int pageNo) {
		return this.memberPointsItemRepository.findStoreExpByMemberCode(memberCode, pageSize, pageNo);
	}


	/**
	 * TODO 查询积分调账记录.
	 */
	@Override
	public List<MemberPointsItemExp> queryPointsReviseList(Example example,Page<MemberPointsItemExp> page) {
		return memberPointsItemRepository.findPointsReviseList(example,page);
	}
	
	@Autowired
	public void setMemberPointsItemRepository(MemberPointsItemRepository memberPointsItemRepository) {
		this.memberPointsItemRepository = memberPointsItemRepository;
	}

	@Autowired
	public void setMemberAccountRepository(MemberAccountRepository memberAccountRepository) {
		this.memberAccountRepository = memberAccountRepository;
	}

	@Autowired
	public void setMemberPointsItemRelRepository(MemberPointsItemRelRepository memberPointsItemRelRepository) {
		this.memberPointsItemRelRepository = memberPointsItemRelRepository;
	}

	@Autowired
	public void setWechatMsgQueueHRepository(WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}

}

