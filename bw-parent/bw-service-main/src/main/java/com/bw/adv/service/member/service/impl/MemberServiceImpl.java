package com.bw.adv.service.member.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.alipay.model.AlipayFans;
import com.bw.adv.module.base.exception.BaseRuntimeException;
import com.bw.adv.module.base.model.Geo;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.repository.GeoRepository;
import com.bw.adv.module.base.repository.OrgRepository;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.common.model.SqlAttribute;
import com.bw.adv.module.common.repository.SqlAttributeRepository;
import com.bw.adv.module.common.utils.MyConstants;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.CouponInstanceReceive;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.coupon.repository.CouponInstanceReceiveRepository;
import com.bw.adv.module.coupon.repository.CouponInstanceRepository;
import com.bw.adv.module.member.constant.ExtAccountTypeConstant;
import com.bw.adv.module.member.constant.GradeFactorsConstant;
import com.bw.adv.module.member.enums.GoodsShowGradeTypeEnums;
import com.bw.adv.module.member.enums.GradeChangeTypeEnums;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.model.MemberGroupItem;
import com.bw.adv.module.member.group.repository.MemberGroupItemRepository;
import com.bw.adv.module.member.group.repository.MemberGroupRepository;
import com.bw.adv.module.member.group.search.MemberGroupItemSearch;
import com.bw.adv.module.member.group.search.MemberGroupSearch;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.ExtAccountBinding;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.GradeConf;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.MemberEntityCard;
import com.bw.adv.module.member.model.MemberExtInfo;
import com.bw.adv.module.member.model.MemberSummaryDay;
import com.bw.adv.module.member.model.exp.GradeConvertRecordExp;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.repository.ExtAccountBindingRepository;
import com.bw.adv.module.member.repository.GradeConvertRecordRepository;
import com.bw.adv.module.member.repository.MemberAccountRepository;
import com.bw.adv.module.member.repository.MemberAddressRepository;
import com.bw.adv.module.member.repository.MemberCardRepository;
import com.bw.adv.module.member.repository.MemberEntityCardRepository;
import com.bw.adv.module.member.repository.MemberExtInfoRepository;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.member.repository.MemberSummaryRepository;
import com.bw.adv.module.member.search.ExtAccountBindingSearch;
import com.bw.adv.module.member.search.GradeConvertRecordSearch;
import com.bw.adv.module.member.search.MemberAccountSearch;
import com.bw.adv.module.member.search.MemberAddressSearch;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.model.MemberTagItem;
import com.bw.adv.module.member.tag.repository.MemberTagItemRepository;
import com.bw.adv.module.member.tag.repository.MemberTagRepository;
import com.bw.adv.module.member.tag.search.MemberTagItemSearch;
import com.bw.adv.module.member.tag.search.MemberTagSearch;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.points.constant.PointsTypeConstant;
import com.bw.adv.module.points.model.MemberPointsItem;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.report.model.MemberAge;
import com.bw.adv.module.report.model.MemberRegion;
import com.bw.adv.module.sms.repository.ComVerificationRepository;
import com.bw.adv.module.sys.model.ComVerification;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.TextMessageUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatCouponMsg;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansMessage;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.repository.WeChatFansRepository;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.points.service.MemberPointsItemSevice;
import com.bw.adv.service.wechat.msg.model.TemplateData;
import com.bw.adv.service.wechat.msg.model.WechatTemplate;
import com.bw.adv.service.wechat.msg.util.WechatApiUtil;
import com.bw.adv.service.wechat.service.WeChatFansService;

import net.sf.json.JSONObject;

//import com.sage.scrm.module.wechat.repository.WeChatFansRepository;

@Service
public class MemberServiceImpl extends BaseServiceImpl<Member>implements MemberService {

	@Value("${member_default_grade_seq}")
	private  static Integer MEMBER_DEFAULT_GRADE_SEQ;
	@Value("${member_default_status}")
	private  static Long MEMBER_DEFAULT_STATUS;
	@Value("${captcha_text}")
	private  static String CAPTCHA_TEXT ;
	@Value("${test_mode}")
	private  static String TEST_MODE ;
	@Value("${vip_top_limit}")
	private  static Long VIP_TOP_LIMIT;
	@Value("${vip_points}")
	private  static Long VIP_POINTS;
	@Value("${memberModifyGradeTemplateId}")
	private static  String memberModifyGradeTemplateId ;
	@Value("${template_Color}")
	private static  String templateColor;
	@Value("${domainName}")
	private static  String domainName;
	@Value("${memberModifyGradeRemark}")
	private static  String memberModifyGradeRemark ;

	@Value("${appId}")
	private static String appId;

	private static  Logger logger = Logger.getLogger(MemberServiceImpl.class);

	private MemberRepository memberRepository;
	private GradeConvertRecordRepository gradeConvertRecordRepository;
	private ExtAccountBindingRepository extAccountBindingRepository;
	private MemberAddressRepository memberAddressRepository;
	private MemberTagRepository memberTagRepository;
	private MemberGroupRepository memberGroupRepository;
	private MemberExtInfoRepository memberExtInfoRepository;
	private MemberAccountRepository memberAccountRepository;
	private MemberTagItemRepository memberTagItemRepository;
	private MemberGroupItemRepository memberGroupItemRepository;
	private GeoRepository geoRepository;
	private OrgRepository orgRepository;
	private SqlAttributeRepository sqlAttributeRepository;
	private CodeService codeService;
	private MemberCardRepository memberCardRepository;
	private MemberSummaryRepository memberSummaryRepository;
	private WeChatFansRepository weChatFansRepository;
	private ComVerificationRepository comVerificationRepository;
	private CouponInstanceRepository couponInstanceRepository;
	private CouponInstanceReceiveRepository couponInstanceReceiveRepository;
	private WechatMsgQueueHRepository wechatMsgQueueHRepository;
	private WeChatFansService weChatFansService;
	private MemberPointsItemSevice memberPointsItemService;
	private MemberEntityCardRepository memberEntityCardRepository;
	private AccessTokenCacheService accessTokenCacheService;
	@Override
	public BaseRepository<Member, ? extends BaseMapper<Member>> getBaseRepository() {
		return memberRepository;
	}

	@Override
	public MemberExp queryEntityPkAuto(Long memberId) {
		MemberExp memberExp = null;
		Member member = null;
		MemberCard memberCard = null;
		Geo geo = null;
		Org org = null;
		memberExp = new MemberExp();
		member = this.memberRepository.findByPk(memberId);
		memberCard = this.memberCardRepository.findByMemberId(memberId);
		if (member != null) {
			geo = this.geoRepository.findByPk(member.getGeoId());
			if (geo != null) {
				member.setGeoName(geo.getGeoName());
			}
			org = this.orgRepository.findByPk(member.getOrgId());
			if (org != null) {
				member.setOrgName(org.getOrgName());
			}
		}
		BeanUtils.copy(member, memberExp);
		memberExp.setCardNo(memberCard.getCardNo());
		return memberExp;
	}

	@Override
	public List<SqlAttribute> querySqlAttrList(Long type) {

		return this.sqlAttributeRepository.findSqlAttrList(type);

	}

	@Override
	public List<GradeConvertRecordExp> queryGradeConvertByMemberId(Long memberId) {
		GradeConvertRecordSearch search = null;

		search = new GradeConvertRecordSearch();
		search.setEqualMemberId(memberId);
		return this.gradeConvertRecordRepository.findListSearchAuto(search);
	}

	@Override
	public MemberExp queryContactInfoByMemberId(Long memberId) {
		ExtAccountBindingSearch search = null;
		MemberExp exp = null;
		List<ExtAccountBinding> list = null;

		exp = new MemberExp();
		search = new ExtAccountBindingSearch();
		search.setEqualMemberId(memberId);
		list = this.extAccountBindingRepository.findListSearchAuto(search);
		if (list != null && list.size() > 0) {
			for (ExtAccountBinding binding : list) {
				if (binding.getExtAccountTypeId() == MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_QQ) {
					exp.setQq(binding.getBindingAccount());
				} else if (binding.getExtAccountTypeId() == MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_SINA) {
					exp.setSina(binding.getBindingAccount());
				} else if (binding.getExtAccountTypeId() == MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_WECHAT) {
					exp.setWechat(binding.getBindingAccount());
				} else if (binding.getExtAccountTypeId() == MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_MSN) {
					exp.setMsn(binding.getBindingAccount());
				} else if (binding.getExtAccountTypeId() == MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_ONLINE) {
					exp.setOnline(binding.getBindingAccount());
				} else if (binding.getExtAccountTypeId() == MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_CC) {
					exp.setCc(binding.getBindingAccount());
				}
			}
		}
		return exp;
	}

	@Override
	public Long queryCountSearchAuto(MemberSearch search) {
		return this.memberRepository.findCountSearchAuto(search);
	}

	@Override
	public List<MemberExp> queryListSearchAuto(MemberSearch search) {
		return this.memberRepository.findListSearchAuto(search);
	}

	@Override
	public List<MemberExp> selectListSearchAutoByOffset(MemberSearch search, Long offset, Long lastId) {
		return this.memberRepository.selectListSearchAutoByOffset(search, offset, lastId);
	}

	@Override
	public List<MemberExp> selectListSearchAutoByOffsetNoCondition(MemberSearch search, Long offset, Long lastId) {
		return this.memberRepository.selectListSearchAutoByOffsetNoCondition(search, offset, lastId);
	}

	@Override
	public List<MemberAddress> queryMemberAddressByMemberId(Long memberId) {
		MemberAddressSearch search = null;
		search = new MemberAddressSearch();
		search.setEqualMemberId(memberId);
		return this.memberAddressRepository.findListSearchAuto(search);
	}

	public MemberAddress queryMemberDefault(Long memberId) {
		MemberAddress result = null;
		List<MemberAddress> mas = null;

		try {

			mas = memberAddressRepository.findMemberDefault(memberId);
			if (mas != null && mas.size() != 0) {
				result = mas.get(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public MemberAddress queryMemberAddressDetail(Long memberAddressId) {

		return this.memberAddressRepository.findMemberAddressDetail(memberAddressId);
	}

	@Override
	@Transactional
	public int insertMemberAddress(MemberAddress memberAddress) {
		MemberAddressSearch search = null;
		MemberAddress oldAddress = null;
		// 若为默认地址，将现有的默认地址修改
		if (MyConstants.MEMBER_ADDRESS_IS_DEFAULT.YES.equals(memberAddress.getIsDefault())) {
			search = new MemberAddressSearch();
			search.setEqualMemberId(memberAddress.getMemberId());
			search.setEqualIsDefault(MyConstants.MEMBER_ADDRESS_IS_DEFAULT.YES);
			oldAddress = this.memberAddressRepository.findEntitySearchAuto(search);
			if (oldAddress != null) {
				oldAddress.setIsDefault(MyConstants.MEMBER_ADDRESS_IS_DEFAULT.NO);
				this.memberAddressRepository.updateByPkSelective(oldAddress);
			}

		}
		return this.memberAddressRepository.saveSelective(memberAddress);
	}

	@Override
	@Transactional
	public int updateMemberAddress(MemberAddress memberAddress) {
		MemberAddressSearch search = null;
		MemberAddress oldAddress = null;
		// 若为默认地址，将现有的默认地址修改
		if (MyConstants.MEMBER_ADDRESS_IS_DEFAULT.YES.equals(memberAddress.getIsDefault())) {
			search = new MemberAddressSearch();
			search.setEqualMemberId(memberAddress.getMemberId());
			search.setEqualIsDefault(MyConstants.MEMBER_ADDRESS_IS_DEFAULT.YES);
			oldAddress = this.memberAddressRepository.findEntitySearchAuto(search);
			if (oldAddress != null) {
				oldAddress.setIsDefault(MyConstants.MEMBER_ADDRESS_IS_DEFAULT.NO);
				this.memberAddressRepository.updateByPkSelective(oldAddress);
			}
		}
		return this.memberAddressRepository.updateByPkSelective(memberAddress);
	}

	@Override
	public int deleteMemberAddress(Long id) {
		return this.memberAddressRepository.removeByPk(id);
	}

	@Override
	@Transactional
	public Boolean verifyEmail(String email) {

		String toMail = email;

		String userName = "simon.yan@sagesoft.cn";
		String password = "Sage@2015";

		String registerId = "" + Math.random() * Math.random();
		String url = "http://localhost:8080/JavaMailChecker/servlet/MailBackServlet?registerId=" + registerId;// 待会用户点在邮箱中点击这个链接回到你的网站。

		Properties props = new Properties();
		props.setProperty("mail.smtp.host", "smtp.sagesoft.cn");
		props.setProperty("mail.smtp.auth", "true");

		Authenticator authenticator = new MyAuthenticator(userName, password);

		javax.mail.Session session = javax.mail.Session.getDefaultInstance(props, authenticator);
		session.setDebug(true);

		try {
			Address from = new InternetAddress(userName);
			Address to = new InternetAddress(toMail);

			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(from);
			msg.setSubject("诚实网站注册");
			msg.setSentDate(new Date());
			msg.setContent("<a href='" + url + "'>点击" + url + "完成注册</a>", "text/html;charset=utf-8");
			msg.setRecipient(RecipientType.TO, to);
			/*
			 * Transport transport = session.getTransport("smtp");
			 * transport.connect("smtp.163.com", userName, password);
			 * transport.sendMessage(msg,msg.getAllRecipients());
			 * transport.close();
			 */
			Transport.send(msg);
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public List<MemberTag> queryMemberTagByMemberId(Long memberId) {
		MemberTagSearch search = null;

		search = new MemberTagSearch();
		search.setEqualMemberId(memberId);
		return this.memberTagRepository.findListSearchAuto(search);
	}

	@Override
	public List<MemberGroup> queryMemberGroupByMemberId(Long memberId) {
		MemberGroupSearch search = null;
		search = new MemberGroupSearch();
		search.setEqualMemberId(memberId);
		return this.memberGroupRepository.findListSearchAuto(search);
	}

	@Override
	public MemberExtInfo queryExtInfoByMemberId(Long memberId) {
		return this.memberExtInfoRepository.findByPk(memberId);
	}

	@Override
	public List<MemberPointsItemExp> queryMemberPointsByMemberId(MemberSearch search) {
		return this.memberRepository.findMemberPointsByMemberId(search);
	}

	@Override
	public List<OrderHeader> queryOrderHeaderByMemberId(MemberSearch search) {
		return this.memberRepository.findOrderHeaderByMemberId(search);
	}

	@Override
	public List<CouponInstance> queryCouponInstanceByMemberId(MemberSearch search) {
		return this.memberRepository.findCouponInstanceByMemberId(search);
	}

	@Override
	public List<WechatFansMessage> queryWechatMessageByMemberId(Long memberId) {
		MemberSearch search = null;
		search = new MemberSearch();
		search.setEqualMemberId(memberId);
		return this.memberRepository.findWechatMessageByMemberId(search);
	}

	@Override
	public List<SmsInstance> querySmsInstanceByMemberId(Long memberId) {
		MemberSearch search = null;
		search = new MemberSearch();
		search.setEqualMemberId(memberId);
		return this.memberRepository.findSmsInstanceByMemberId(search);
	}

	@Override
	public Long queryCountMemberPointsByMemberId(MemberSearch search) {
		return this.memberRepository.findCountMemberPointsByMemberId(search);
	}

	@Override
	public Long queryCountOrderHeaderByMemberId(MemberSearch search) {
		return this.memberRepository.findCountOrderHeaderByMemberId(search);
	}

	@Override
	public Long queryCountCouponInstanceByMemberId(MemberSearch search) {
		return this.memberRepository.findCountCouponInstanceByMemberId(search);
	}

	@Override
	public Long queryCountWechatMessageByMemberId(Long memberId) {
		MemberSearch search = null;
		search = new MemberSearch();
		search.setEqualMemberId(memberId);
		return this.memberRepository.findCountWechatMessageByMemberId(search);
	}

	@Override
	public Long queryCountSmsInstanceByMemberId(Long memberId) {
		MemberSearch search = null;
		search = new MemberSearch();
		search.setEqualMemberId(memberId);
		return this.memberRepository.findCountSmsInstanceByMemberId(search);
	}

	@Override
	@Transactional
	public Long insertMember(Member member) {
		Long memberId = null;
		MemberAccount memberAccount = null;

		// 会员注册的默认等级
		Grade grade = null;
		grade = GradeInit.getGradeBySeq(MEMBER_DEFAULT_GRADE_SEQ);
		if (grade != null) {
			member.setGradeId(grade.getGradeId());
		}

		member.setRegisterTime(DateUtils.getCurrentTimeOfDb());
		member.setStatusId(MEMBER_DEFAULT_STATUS);
		member.setRegisterDate(DateUtils.formatCurrentDate("MMdd"));
		member.setStatusId(MEMBER_DEFAULT_STATUS);// 默认状态
		this.memberRepository.saveSelective(member);
		memberId = member.getMemberId();
		memberAccount = new MemberAccount();
		memberAccount.setMemberId(memberId);
		this.memberAccountRepository.saveSelective(memberAccount);
		// 保存会员卡
		openCard(memberId, memberAccount.getMemberAccountId());
		return memberId;
	}


	@Override
	@Transactional
	public Map<String, Object> registMemberInfo(MemberDto memberDto) {
		Long memberId = null;
		MemberAccount memberAccount = null;
		ExtAccountBinding bing = null;
		String memberCode = null;
		Member member = null;
		WechatFans wechatFans = null;
		AlipayFans alipayFans = null;
		String bindingAccount = null;
		String extAccountTypeCode = null;
		Long extAccountTypeId = null;
		Map<String, Object> map = null;
		try {
			member = new Member();
			map = new HashMap<String, Object>();
			// 校验手机验证码
//			validateVerifyCode(memberDto.getMobile(), memberDto.getComvCode(), null);

			extAccountTypeCode = memberDto.getExtAccountTypeId();
			if(StringUtils.isNotBlank(extAccountTypeCode)){
				extAccountTypeId = ExtAccountTypeConstant
						.getConstantByCode(ExtAccountTypeConstant.class, extAccountTypeCode).getExtAccountTypeId();
			}
			bindingAccount = memberDto.getBindingAccount();

			BeanUtils.copyNotNull(memberDto, member);

			// 保存会员
			memberCode = codeService.generateCode(Member.class);
			member.setMemberCode(memberCode);

			if (MEMBER_DEFAULT_STATUS != null) {
				member.setStatusId(MEMBER_DEFAULT_STATUS);// 配置的会员初始化状态
			} else {
				member.setStatusId(StatusConstant.MEMBER_ACTIVATION.getId());// 默认状态
			}

			member.setRegisterTime(DateUtils.getCurrentTimeOfDb());// 注册时间
			member.setRegisterDate(DateUtils.formatCurrentDate("MMdd"));// 注册日期
			Grade grade = GradeInit.getGradeBySeq(MEMBER_DEFAULT_GRADE_SEQ);
			member.setGradeId(grade.getGradeId());

			if (StringUtils.isBlank(member.getMemberPhoto())) {
				WechatFans wechatFansImage = weChatFansRepository.findWeChatFunsByOpenId(bindingAccount);
				if(wechatFansImage != null){
					member.setMemberPhoto(wechatFansImage.getHeadImgUrl());
				}
			}
			
			this.memberRepository.saveSelective(member);

			memberId = member.getMemberId();
			memberAccount = new MemberAccount();
			memberAccount.setMemberId(memberId);
			this.memberAccountRepository.saveSelective(memberAccount);

			// 微信用户
			if (extAccountTypeCode.equals(ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getCode())) {
				bing = new ExtAccountBinding();
				bing.setBindingAccount(bindingAccount);
				bing.setExtAccountTypeId(extAccountTypeId);
				bing.setBindingDate(DateUtils.formatDate(new Date(), "yyyyMMdd"));
				bing.setMemberId(memberId);
				bing.setStatusId(StatusConstant.MEMBER_ACCOUNT_BINDING.getId());
				this.extAccountBindingRepository.saveSelective(bing);
				
				wechatFans = new WechatFans();
				wechatFans.setOpenid(memberDto.getBindingAccount());
				BeanUtils.copyNotNull(memberDto, wechatFans);
				wechatFans.setMemberId(memberId);
				// 保存or修改微信粉丝
				weChatFansService.saveOrUpdate(wechatFans);
			} // 支付宝用户

			// 保存会员卡
//			if(member.getChannelId().equals(ChannelConstant.BLACK.getId())){
//				String cardNo = memberDto.getEntityCardNo();
//				if(StringUtils.isNotBlank(cardNo)){
//					MemberEntityCard card = memberEntityCardRepository.findCardByCardNo(cardNo, StatusConstant.MEMBER_ENTITY_CARD_ENABLE.getId());
//					if(card != null){
//						openEntityCard(memberId, memberAccount.getMemberAccountId(), cardNo);
//						card.setStatusId(StatusConstant.MEMBER_ENTITY_CARD_DISABLED.getId());
//						memberEntityCardRepository.updateByPkSelective(card);
//					}else{
//						throw new MemberException(ResultStatus.MEMBER_ENTITY_CARD_ERROR);
//					}
//				}else{
//					throw new MemberException(ResultStatus.MEMBER_ENTITY_CARD_EMPTY);
//				}
//			}else{
//				openCard(memberId, memberAccount.getMemberAccountId());
//			}

			map.put("code", "200");
			// 切面需用，勿删
			BeanUtils.copyNotNull(member, memberDto);
			map.put("member", member);
			map.put("memberId", memberId);
			map.put("channelId", member.getChannelId());
		} catch (MemberException e) {
			e.printStackTrace();
			throw new MemberException(e.getResultStatus());
		} catch (Exception e) {
			e.printStackTrace();
			throw new BaseRuntimeException(ResultStatus.SYSTEM_ERROR);
		}
		return map;
	}
	
	@Override
	public Map<String, Object> saveMemberInfo(WechatFans wechatFans) {
		Long memberId = null;
		ExtAccountBinding bing = null;
		String memberCode = null;
		Member member = null;
		String bindingAccount = null;
		Long extAccountTypeId = null;
		Map<String, Object> map = null;
		try {
			member = new Member();
			map = new HashMap<String, Object>();

			bindingAccount = wechatFans.getOpenid();

			extAccountTypeId=ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getId();

			// 保存会员
			memberCode = codeService.generateCode(Member.class);
			member.setMemberCode(memberCode);
			member.setMemberName(wechatFans.getNickName());
			member.setGender(StringUtils.isNotBlank(wechatFans.getSex())?Long.parseLong(wechatFans.getSex()):null);
			if (MEMBER_DEFAULT_STATUS != null) {
				member.setStatusId(MEMBER_DEFAULT_STATUS);// 配置的会员初始化状态
			} else {
				member.setStatusId(StatusConstant.MEMBER_ACTIVATION.getId());// 默认状态
			}

			member.setRegisterTime(DateUtils.getCurrentTimeOfDb());// 注册时间
			member.setRegisterDate(DateUtils.formatCurrentDate("MMdd"));// 注册日期

			if(wechatFans.getHeadImgUrl() != null){
				member.setMemberPhoto(wechatFans.getHeadImgUrl());
			}
			
			this.memberRepository.saveSelective(member);

			memberId = member.getMemberId();

			// 微信用户
			bing = new ExtAccountBinding();
			bing.setBindingAccount(bindingAccount);
			bing.setExtAccountTypeId(extAccountTypeId);
			bing.setBindingDate(DateUtils.formatDate(new Date(), "yyyyMMdd"));
			bing.setMemberId(memberId);
			bing.setStatusId(StatusConstant.MEMBER_ACCOUNT_BINDING.getId());
			this.extAccountBindingRepository.saveSelective(bing);
			
			wechatFans = new WechatFans();
			wechatFans.setOpenid(bindingAccount);
			wechatFans.setMemberId(memberId);
			// 保存or修改微信粉丝
			weChatFansService.saveOrUpdate(wechatFans);

			map.put("code", "200");
			map.put("member", member);
			map.put("memberId", memberId);
		} catch (Exception e) {
			logger.error("saveMember", e);
			throw new MemberException(ResultStatus.SYSTEM_ERROR);
		}
		return map;
	}

	@Transactional
	public void bindBlackCardManually(Member member, String cardNo) throws Exception {
		MemberAccount memberAccount = memberAccountRepository.findByMemberId(member.getMemberId());
		Member persMember = memberRepository.findExpByMemberId(member.getMemberId());
		//WechatFans wf = weChatFansRepository.
		List<ExtAccountBinding> eabs = extAccountBindingRepository.findByMemberIdAndTypeId(member.getMemberId(), ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getId());

		member.setGradeId(GoodsShowGradeTypeEnums.No_3.getCode());
		member.setChannelId(ChannelConstant.BLACK.getId());

		MemberEntityCard card = memberEntityCardRepository.findCardByCardNo(cardNo, StatusConstant.MEMBER_ENTITY_CARD_ENABLE.getId());
		if(card != null){
			memberCardRepository.deleteCardByMemberId(member.getMemberId());
			openEntityCard(member.getMemberId(), memberAccount.getMemberAccountId(), cardNo);
			card.setStatusId(StatusConstant.MEMBER_ENTITY_CARD_DISABLED.getId());
			memberEntityCardRepository.updateByPkSelective(card);
		}else {
			throw new MemberException(ResultStatus.MEMBER_ENTITY_CARD_ERROR);
		}
		memberRepository.updateByPkSelective(member);
		if (eabs.size() > 0){

			WechatFans fan = weChatFansRepository.findWeChatFunsByOpenId(eabs.get(0).getBindingAccount());
			if (fan != null && fan.getIsFans().equals("Y")){
				WechatTemplate template = new WechatTemplate();

				template.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId +
				"&redirect_uri=http%3a%2f%2f"+domainName+"%2fscrm-pe-wechat%2fmemberController%2fwechatEntrance%3fkey%3d5&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect");
				String gradeName = "";
				switch (persMember.getGradeId().intValue()){
					case 1: gradeName = "蓝卡"; break;
					case 2: gradeName = "金卡"; break;
					case 3: gradeName = "铂金卡"; break;
					default: gradeName = "蓝卡";
				}

				String startDate = DateUtils.dateStrToNewFormat(DateUtils.getCurrentDateOfDb(), DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
				template.setTouser(fan.getOpenid());
				template.setTemplate_id(memberModifyGradeTemplateId);

				Map<String, TemplateData> map = new HashMap<String, TemplateData>();
				TemplateData firstTemp = new TemplateData();
				firstTemp.setValue("非常荣幸邀请到您成为我们的黑金卡会员");
				firstTemp.setColor(templateColor);
				map.put("first", firstTemp);

				TemplateData gradebeforTemp = new TemplateData();
				gradebeforTemp.setValue(gradeName);
				gradebeforTemp.setColor(templateColor);
				map.put("grade1", gradebeforTemp);

				TemplateData gradeafterTemp = new TemplateData();
				gradeafterTemp.setColor(templateColor);
				gradeafterTemp.setValue("黑金卡");
				map.put("grade2", gradeafterTemp);

				TemplateData uDateTemp = new TemplateData();
				uDateTemp.setValue(startDate);
				map.put("time", uDateTemp);

				TemplateData remarkTemp = new TemplateData();
				remarkTemp.setValue(memberModifyGradeRemark);
        /*map.put("remark", remarkTemp);*/
				template.setData(map);
                String accessToken=accessTokenCacheService.getAccessTokenCache();
                WechatApiUtil.templateSend(accessToken, template);

			}
		}
	}

	/**
	 * 特殊渠道注册送积分
	 */
	public void pointsAfterRegister(Long memberId) {
		List<MemberPointsItem> pointsResults = new ArrayList<MemberPointsItem>();
		MemberPointsItem memberPointsItem = new MemberPointsItem();
		if(VIP_POINTS == 0L){
			return;
		}
		BigDecimal quantity = new BigDecimal(VIP_POINTS);
		memberPointsItem.setAvailablePointsNumber(quantity);
		memberPointsItem.setHappenTime(DateUtils.getCurrentTimeStr());
		memberPointsItem.setCreateTime(DateUtils.getCurrentTimeStr());
		memberPointsItem.setInvalidDate(DateUtils.addYears(1).substring(0, 4) + "0331");
		memberPointsItem.setMemberId(memberId);
		memberPointsItem.setItemPointsNumber(quantity);
		memberPointsItem.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId());
		memberPointsItem.setPointsTypeId(PointsTypeConstant.POINTS_TYPE_ACTIVITY.getId());
		memberPointsItem.setPointsReason("合作渠道奖励");
		pointsResults.add(memberPointsItem);
		this.memberPointsItemService.operatePoints(memberId, pointsResults);
	}

	/**
	 * TODO 绑定会员
	 * 
	 * @see com.bw.adv.service.member.service.MemberService#bindingMember(com.bw.adv.api.member.dto.MemberDto)
	 */
	@Override
	@Transactional
	public Member bindingMember(MemberDto memberDto) {
		Long memberId = null;
		ExtAccountBinding bing = null;
		String extAccountTypeId = null;
		Member member = null;
		WechatFans wechatFans = null;
		AlipayFans alipayFans = null;
		Long typeId = null;
		List<ExtAccountBinding> bindingList = null;

		// 校验手机验证码
		validateVerifyCode(memberDto.getMobile(), memberDto.getComvCode(), "Y");

		member = memberRepository.findByMobile(memberDto.getMobile());
		if (member == null) {
			throw new MemberException(ResultStatus.MEMBER_NOT_EXIST_ERROR);
		}
		extAccountTypeId = memberDto.getExtAccountTypeId();
		memberId = member.getMemberId();

		typeId = ExtAccountTypeConstant.getConstantByCode(ExtAccountTypeConstant.class, extAccountTypeId)
				.getExtAccountTypeId();
		bindingList = extAccountBindingRepository.findByMemberIdAndTypeId(memberId, typeId);
		// 会员只能有一个微信或支付宝等第三方账号
		if (bindingList != null && bindingList.size() > 0) {
			throw new MemberException(ResultStatus.MOBILE_BINDING_ACCOUNT_EXIST_ERROR);
		}

		bing = new ExtAccountBinding();
		bing.setBindingAccount(memberDto.getBindingAccount());
		bing.setExtAccountTypeId(typeId);
		bing.setBindingDate(DateUtils.getCurrentDateOfDb());
		bing.setMemberId(memberId);
		bing.setStatusId(StatusConstant.MEMBER_ACCOUNT_BINDING.getId());
		this.extAccountBindingRepository.saveSelective(bing);
		if (extAccountTypeId.equals(ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getCode())) {
			wechatFans = new WechatFans();
			wechatFans.setOpenid(memberDto.getBindingAccount());
			BeanUtils.copyNotNull(memberDto, wechatFans);
			wechatFans.setMemberId(memberId);
			wechatFans.setIsFans("Y");
			// 保存or修改微信粉丝
			weChatFansService.saveOrUpdate(wechatFans);
		} 
		return member;
	}

	@Override
	@Transactional
	public Member registMemberNoWechat(Member member, String extAccountTypeId, String account) throws Exception {
		Long memberId = null;
		MemberAccount memberAccount = null;
		ExtAccountBinding bing = null;
		String memberCode = null;
		Grade grade = null;

		// 保存会员
		memberCode = codeService.generateCode(Member.class);
		member.setMemberCode(memberCode);
		// 会员注册的默认等级
		grade = GradeInit.getGradeBySeq(MEMBER_DEFAULT_GRADE_SEQ);
		if (grade != null) {
			member.setGradeId(grade.getGradeId());
		}

		member.setStatusId(MEMBER_DEFAULT_STATUS);// 默认状态
		member.setRegisterDate(DateUtils.formatCurrentDate("MMdd"));
		this.memberRepository.saveSelective(member);

		// 保存会员账户信息
		memberId = member.getMemberId();
		memberAccount = new MemberAccount();
		memberAccount.setMemberId(memberId);
		this.memberAccountRepository.saveSelective(memberAccount);

		// 保存会员外部账户
		bing = new ExtAccountBinding();
		bing.setBindingAccount(account);
		bing.setExtAccountTypeId(ExtAccountTypeConstant
				.getConstantByCode(ExtAccountTypeConstant.class, extAccountTypeId).getExtAccountTypeId());
		bing.setBindingDate(DateUtils.getCurrentDateOfDb());
		bing.setMemberId(memberId);
		bing.setStatusId(StatusConstant.MEMBER_ACCOUNT_BINDING.getId());
		this.extAccountBindingRepository.saveSelective(bing);

		// 保存会员卡
		openCard(memberId, memberAccount.getMemberAccountId());

		return member;
	}

	@Override
	@Transactional
	public int updateMember(Member member) {
		MemberSearch search = null;
		MemberExp memberExp = null;
		int res = -1;

		search = new MemberSearch();
		search.setEqualMobile(member.getMobile());
		memberExp = this.queryEntitySearchAuto(search);
		if (memberExp != null && memberExp.getMemberId().longValue() != member.getMemberId().longValue()) {
			throw new MemberException("手机号已存在！");
		}

		member.setBirthday(DateUtils.formatString(member.getBirthday()));
		res = this.memberRepository.updateByPkSelective(member);
		return res;
	}

	@Override
	@Transactional
	public int updateMemberExtAccount(List<ExtAccountBinding> bindingList, Long memberId) {
		ExtAccountBindingSearch search = null;
		ExtAccountBinding entity = null;

		if (bindingList != null && bindingList.size() > 0) {
			for (ExtAccountBinding binding : bindingList) {
				search = new ExtAccountBindingSearch();
				search.setEqualExtAccountTypeId(binding.getExtAccountTypeId());
				search.setEqualMemberId(memberId);
				entity = this.extAccountBindingRepository.findEntitySearchAuto(search);
				if (entity != null) {
					binding.setBindingId(entity.getBindingId());
					this.extAccountBindingRepository.updateByPkSelective(binding);
				} else {
					binding.setStatusId(StatusConstant.MEMBER_ACCOUNT_BINDING.getId());
					this.extAccountBindingRepository.saveSelective(binding);
				}
			}
		}
		return 0;
	}

	@Override
	@Transactional
	public int insertMemberTag(String memberTagIds, Long memberId) {
		MemberTagItem memberTagItem = null;
		memberTagItem = null;

		String[] memberTagIdArr = memberTagIds.split(",");
		for (String memberTagId : memberTagIdArr) {
			memberTagItem = new MemberTagItem();
			memberTagItem.setMemberId(memberId);
			memberTagItem.setMemberTagId(Long.valueOf(memberTagId));
			this.memberTagItemRepository.saveSelective(memberTagItem);
		}
		return 0;
	}

	@Override
	public int deleteMemberTag(Long memberTagId, Long memberId) {
		MemberTagItemSearch search = null;
		MemberTagItem item = null;
		search = new MemberTagItemSearch();
		search.setEqualMemberId(memberId);
		search.setEqualMemberTagId(memberTagId);
		item = this.memberTagItemRepository.findEntitySearchAuto(search);
		if (item != null) {
			this.memberTagItemRepository.removeByPk(item.getMemberTagItemId());
		}
		return 0;
	}

	@Override
	@Transactional
	public int insertMemberGroup(String memberGroupIds, Long memberId) {
		MemberGroupItem memberGroupItem = null;
		String[] memberGroupIdArr = null;

		memberGroupIdArr = memberGroupIds.split(",");
		for (String memberGroupId : memberGroupIdArr) {
			memberGroupItem = new MemberGroupItem();
			memberGroupItem.setMemberId(memberId);
			memberGroupItem.setMemberGroupId(Long.valueOf(memberGroupId));
			this.memberGroupItemRepository.saveSelective(memberGroupItem);
		}
		return 0;
	}

	@Override
	public int deleteMemberGroup(Long memberGroupId, Long memberId) {
		MemberGroupItemSearch search = null;
		MemberGroupItem item = null;

		search = new MemberGroupItemSearch();
		search.setEqualMemberId(memberId);
		search.setEqualMemberGroupId(memberGroupId);
		item = this.memberGroupItemRepository.findEntitySearchAuto(search);
		if (item != null) {
			this.memberGroupItemRepository.removeByPk(item.getMemberGroupItemId());
		}
		return 0;
	}

	@Override
	public void checkMemberSql(String sql) {
		this.memberRepository.findListBySql(sql + " Limit 0,10");
	}

	@Override
	public void updateMemberTagAndGroup(Long memberId, String memberTagIds, String memberGroupIds) {
		MemberTagItem memberTagItem = null;
		MemberGroupItem memberGroupItem = null;
		String[] memberTagIdArr = null;
		String[] memberGroupIdArr = null;
		List<MemberTagItem> tagList = null;
		List<MemberGroupItem> groupList = null;

		memberTagItemRepository.deleteTagItemByMemberId(memberId);
		if (StringUtils.isNotBlank(memberTagIds)) {
			memberTagIdArr = memberTagIds.split(",");
			tagList = new ArrayList<MemberTagItem>();
			for (String memberTagId : memberTagIdArr) {
				memberTagItem = new MemberTagItem();
				memberTagItem.setMemberId(memberId);
				memberTagItem.setMemberTagId(Long.valueOf(memberTagId));
				tagList.add(memberTagItem);
			}
			memberTagItemRepository.insertBatchMemberTagItem(tagList);
		}
		if (StringUtils.isNotBlank(memberGroupIds)) {
			memberGroupItemRepository.deleteGroupItemByMemberId(memberId);
			memberGroupIdArr = memberGroupIds.split(",");
			groupList = new ArrayList<MemberGroupItem>();
			for (String memberGroupId : memberGroupIdArr) {
				memberGroupItem = new MemberGroupItem();
				memberGroupItem.setMemberId(memberId);
				memberGroupItem.setMemberGroupId(Long.valueOf(memberGroupId));
				groupList.add(memberGroupItem);
			}
			memberGroupItemRepository.insertBatchMemberGroupItem(groupList);
		}
	}

	@Override
	public Member queryMemberByBindingAccount(String extAccountTypeId, String bindingAccount) {
		Member member = null;
		WechatFans wfans = null;
		AlipayFans afans = null;
		ExtAccountBindingSearch search = null;
		// MemberAddress memAddress = null;
		search = new ExtAccountBindingSearch();
		search.setEqualExtAccountTypeId(ExtAccountTypeConstant
				.getConstantByCode(ExtAccountTypeConstant.class, extAccountTypeId).getExtAccountTypeId());
		search.setEqualBindingAccount(bindingAccount);
		ExtAccountBinding binding = this.extAccountBindingRepository.findEntitySearchAuto(search);
		if (binding == null) {
			// throw new MemberException(ResultStatus.MEMBER_NOT_EXIST_ERROR);
			return null;
		}
		member = this.memberRepository.findByPk(binding.getMemberId());
		/*
		 * if(ExtAccountTypeConstant.ACCOUNT_TYPE_WECHAT.getCode().equals(
		 * extAccountTypeId)){ wfans =
		 * weChatFansRepository.findWeChatFunsByOpenId(binding.getBindingAccount
		 * ()); if(wfans != null){ member.setMemberPhoto(wfans.getHeadImgUrl());
		 * } }else
		 * if(ExtAccountTypeConstant.ACCOUNT_TYPE_ALIPAY.getCode().equals(
		 * extAccountTypeId)){ afans =
		 * alipayFansService.queryByUserId(binding.getBindingAccount());
		 * if(afans != null){ member.setMemberPhoto(afans.getAvatar()); } }
		 */
		return member;
	}

	@Override
	public MemberCard queryMemberCardByMemberId(Long memberId) {
		MemberCard memberCard = null;
		memberCard = new MemberCard();
		memberCard = this.memberCardRepository.findByMemberId(memberId);
		return memberCard;
	}

	@Override
	public MemberAccount queryMemberAccountByMemberId(Long memberId) {
		MemberAccountSearch search = null;
		search = new MemberAccountSearch();
		search.setEqualMemberId(memberId);
		return this.memberAccountRepository.findEntitySearchAuto(search);
	}

	@Override
	@Transactional
	public int insertMemberExtInfo(MemberExtInfo memberExtInfo) {
		return this.memberExtInfoRepository.saveSelective(memberExtInfo);
	}

	@Override
	@Transactional
	public int updateMemberExtInfo(MemberExtInfo memberExtInfo) {
		return this.memberExtInfoRepository.updateByPkSelective(memberExtInfo);
	}

	/**
	 * 
	 * saveWechatFans:(新增or修改微信粉丝). <br/>
	 * Date: 2016年4月5日 下午12:47:37 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param fans
	 * @param memberId
	 * @param openId
	 * @param photoUrl
	 */
	private void saveWechatFans(WechatFans fans, Long memberId, String openId, String photoUrl) {
		WechatFans wfans = null;
		WechatFans weChatTemp = null; // 根据account查出的粉丝数据

		weChatTemp = weChatFansRepository.findWeChatFunsByOpenId(openId);
		if (weChatTemp != null) {// 修改
			weChatTemp.setMemberId(memberId);
			if (fans != null) {
				if (fans.getChannelId() != null) {
					weChatTemp.setChannelId(fans.getChannelId());
				}
				if (fans.getQrCodeId() != null) {
					// 二维码ID，用于统计粉丝来源渠道
					weChatTemp.setQrCodeId(fans.getQrCodeId());
				}

				if (fans.getUnionid() != null) {
					weChatTemp.setUnionid(fans.getUnionid());
				}
				if (fans.getNickName() != null) {
					// weChatTemp.setNickName(fans.getNickName());
				}
				if (fans.getSex() != null) {
					weChatTemp.setSex(fans.getSex());
				}
				if (fans.getCty() != null) {
					weChatTemp.setCty(fans.getCty());
				}
				if (fans.getProvince() != null) {
					weChatTemp.setProvince(fans.getProvince());
				}
				if (fans.getCountry() != null) {
					weChatTemp.setCountry(fans.getCountry());
				}
				if (fans.getLanguage() != null) {
					weChatTemp.setLanguage(fans.getLanguage());
				}
			}
			this.weChatFansRepository.updateByPkSelective(weChatTemp);
		} else {// 新增粉丝数据
			wfans = new WechatFans();
			wfans.setOpenid(openId);
			wfans.setAttentionTime(DateUtils.getCurrentTimeOfDb());
			wfans.setIsDelete("0");
			wfans.setIsFans("Y");
			wfans.setMemberId(memberId);
			if (photoUrl != null) {
				wfans.setHeadImgUrl(photoUrl);
			}
			if (fans != null) {
				if (fans.getChannelId() != null) {
					wfans.setChannelId(fans.getChannelId());
				}
				if (fans.getQrCodeId() != null) {
					// 二维码ID，用于统计粉丝来源渠道
					wfans.setQrCodeId(fans.getQrCodeId());
				}

				if (fans.getUnionid() != null) {
					wfans.setUnionid(fans.getUnionid());
				}
				if (fans.getNickName() != null) {
					// wfans.setNickName(fans.getNickName());
				}
				if (fans.getSex() != null) {
					wfans.setSex(fans.getSex());
				}
				if (fans.getCty() != null) {
					wfans.setCty(fans.getCty());
				}
				if (fans.getProvince() != null) {
					wfans.setProvince(fans.getProvince());
				}
				if (fans.getCountry() != null) {
					wfans.setCountry(fans.getCountry());
				}
				if (fans.getLanguage() != null) {
					wfans.setLanguage(fans.getLanguage());
				}
			}

			weChatFansRepository.save(wfans);
		}
	}

	/**
	 * openCard:(开卡). <br/>
	 * Date: 2015-9-24 下午4:58:46 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param memberAccountId
	 * @throws Exception
	 */
	private void openCard(Long memberId, Long memberAccountId) {
		try {
			MemberCard memberCard = null;
			String cardNo = null;
			// 保存会员卡
			cardNo = codeService.generateCode(MemberCard.class);
			memberCard = new MemberCard();
			memberCard.setMemberId(memberId);
			memberCard.setMemberAccountId(memberAccountId);
			memberCard.setCardNo(cardNo);
			memberCard.setGrantDate(DateUtils.formatDate(new Date(), "yyyyMMdd"));
			memberCard.setStatusId(StatusConstant.MEMBER_CARD_CREATED.getId());
			memberCardRepository.save(memberCard);
		} catch (Exception e) {
			e.printStackTrace();
			throw new MemberException("创建会员卡异常");
		}
	}
	
	private void openEntityCard(Long memberId, Long memberAccountId,String entityCardNo){
		try {
			MemberCard memberCard = null;
			String cardNo = null;
			// 保存会员卡
			cardNo = entityCardNo;
			memberCard = new MemberCard();
			memberCard.setMemberId(memberId);
			memberCard.setMemberAccountId(memberAccountId);
			memberCard.setCardNo(cardNo);
			memberCard.setGrantDate(DateUtils.formatDate(new Date(), "yyyyMMdd"));
			memberCard.setStatusId(StatusConstant.MEMBER_CARD_CREATED.getId());
			memberCardRepository.save(memberCard);
		} catch (Exception e) {
			e.printStackTrace();
			throw new MemberException("创建会员卡异常");
		}
	}

	@Override
	public void callMemberSummaryDay(String dateStr, String weekStr, String monthStr) {
		memberRepository.callMemberSummaryDay(dateStr, weekStr, monthStr);
	}

	@Override
	public void insertComVerification(ComVerification comVerification) throws Exception {
		String captcha = null;
		String captchaValue = null;
		String notiMobile = "";
		boolean inland = true;
		comVerification.setCreateTime(DateUtils.getCurrentTimeOfDb());
		comVerification.setExpireTime(DateUtils.addMinutes(30));

		comVerification.setIsComplement("N");
		comVerification.setIsActive("Y");
		comVerification.setIsDelete("N");

		// 测试模式使用通用验证码，不发送短信
		if ("Y".equals(TEST_MODE)) {

			captcha = "123456";

		} else {
			captcha = "" + (int) ((Math.random() * 9 + 1) * 100000);

			captchaValue = CAPTCHA_TEXT.replace("(captcha)", captcha);
			if (comVerification.getMobile().indexOf("00") == 0){
				notiMobile = comVerification.getMobile().substring(2);
				inland = false;
			} else {
				notiMobile = "86" + comVerification.getMobile();
			}
			HashMap pp = TextMessageUtils.testSingleMt(notiMobile, captchaValue, inland);
			if (!"000".equals(pp.get("mterrcode"))) {
				comVerification.setIsPass("E");
				comVerificationRepository.saveSelective(comVerification);
				logger.error("短信发送失败，返回码：" + pp.toString());
				throw new MemberException(ResultStatus.TEXT_MESSAGE_ERROR);// 短信发送异常
			} else {
				comVerification.setIsPass("N");
			}
		}

		comVerification.setVerificationCode(captcha);

		comVerificationRepository.saveSelective(comVerification);
	}

	@Override
	public void updateComVerification(ComVerification comVerification) {
		comVerificationRepository.updateByPkSelective(comVerification);
	}

	@Override
	public List<ComVerification> findComvByExample(Example example) {
		return comVerificationRepository.findByExample(example);
	}

	@Override
	@Transactional
	public ResultStatus activation(MemberDto memberDto, String couponCode) {
		Member mem = null;

		// 根据外部账号查询会员
		mem = queryMemberByBindingAccount(memberDto.getExtAccountTypeId(), memberDto.getBindingAccount());
		if (mem == null) {
			throw new MemberException(ResultStatus.MEMBER_ACTIVE_ERROR);
		} else {
			if (StatusConstant.MEMBER_ACTIVATION.getId().equals(mem.getStatusId())) {
				throw new MemberException(ResultStatus.MEMBER_ACTIVE_ERROR);
			}
		}
		// 验证手机号是否重复（必输）
		validateVerifyCode(memberDto.getMobile(), memberDto.getComvCode(), null);

		memberDto.setMemberId(mem.getMemberId());
		memberDto.setRegisterTime(DateUtils.getCurrentTimeOfDb());// 注册时间
		memberDto.setRegisterDate(DateUtils.formatCurrentDate("MMdd"));// 注册日期
		memberDto.setStatusId(StatusConstant.MEMBER_ACTIVATION.getId());
		memberRepository.updateByPkSelective(memberDto);

		// 判断是否转赠券
		if (!StringUtils.isBlank(couponCode)) {
			return turnCoupon(couponCode, mem);
		}
		return ResultStatus.SUCCESS;
	}

	@Override
	@Transactional
	public void wechatUserEdit(MemberExp memberExp) {
		MemberSearch search = null;
		Example example = null;
		List<ComVerification> comVs = null;
		MemberAddress ma = null;

		// 手机号未修改，不做验证码验证 手机号修改，则需验证
		if (memberExp.getOriginalMobile() != null) {
			memberExp.setOriginalMobile(memberExp.getOriginalMobile().replaceFirst("^0086", ""));
		}
		if (!memberExp.getMobile().equals(memberExp.getOriginalMobile())) {
			// 验证手机号是否重复（必输）
			if (StringUtils.isBlank(memberExp.getMobile())) {
				throw new MemberException(ResultStatus.MEMBER_PHONE_NULL_ERROR);
			} else {
				search = new MemberSearch();
				search.setEqualMobile(memberExp.getMobile());
				Long count = queryCountSearchAuto(search);
				if (count > 0) {
					throw new MemberException(ResultStatus.MEMBER_PHONE_EXIST_ERROR);
				}
			}

			// 验证验证码
			example = new Example();
			Criteria exampleTemp = example.createCriteria();
			exampleTemp.andEqualTo("MOBILE", memberExp.getMobile());
			exampleTemp.andEqualTo("VERIFICATION_CODE", memberExp.getComvCode());
			comVs = findComvByExample(example);
			if (comVs == null || comVs.size() == 0) {
				throw new MemberException(ResultStatus.COMV_MISMATCH_ERROR);
			} else {
				ComVerification comvTemp = comVs.get(0);
				// 验证是否过期 未过期则修改验证码数据
				if (Long.parseLong(DateUtils.getCurrentTimeOfDb()) > Long.parseLong(comvTemp.getExpireTime())) {
					throw new MemberException(ResultStatus.COMV_OVERDUE_ERROR);
				} else {
					comvTemp.setIsPass("Y");
					comvTemp.setIsActive("N");
					comvTemp.setIsComplement("Y");
					comvTemp.setUpdateTime(DateUtils.getCurrentTimeOfDb());

					updateComVerification(comvTemp);
				}
			}

		}

		this.updateMember(memberExp);
	}

	public MemberGroupItemRepository getMemberGroupItemRepository() {
		return memberGroupItemRepository;
	}

	@Transactional
	private ResultStatus turnCoupon(String couponCode, Member member) {
		CouponInstance couponInstance = null;
		String currentTime = null;

		currentTime = DateUtils.getCurrentTimeOfDb();
		couponInstance = couponInstanceRepository.findCouponInstanceByInstanceCode(couponCode);

		if (couponInstance != null) {
			// 状态为分享中，则券可以领取
			if (StatusConstant.COUPON_INSTANCE_SHARE.getId().equals(couponInstance.getStatusId())) {
				CouponInstanceReceive couponInstanceReceive = new CouponInstanceReceive();
				couponInstanceReceive.setCouponInstanceId(couponInstance.getCouponInstanceId());
				couponInstanceReceive.setCouponInstanceSrc(couponInstance.getCouponInstanceUser());
				couponInstanceReceive.setCouponInstanceGet(member.getMemberId());
				couponInstanceReceive.setGetTime(currentTime);
				couponInstanceReceiveRepository.saveSelective(couponInstanceReceive);

				couponInstance.setCouponInstanceUser(member.getMemberId());
				couponInstance.setUpdateTime(currentTime);
				couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
				couponInstanceRepository.updateByPkSelective(couponInstance);

				CouponInstanceExp couponInstanceExp = couponInstanceRepository.findExpByCode(couponCode);
				// 微信消息队列表
				WechatCouponMsg wechatCouponMsg = new WechatCouponMsg();
				wechatCouponMsg.setCouponName(couponInstanceExp.getCouponName());
				wechatCouponMsg.setStartDate(couponInstanceExp.getStartDate());
				wechatCouponMsg.setExpireDate(couponInstanceExp.getExpireDate());
				wechatCouponMsg.setComments(couponInstanceExp.getComments());
				JSONObject json = JSONObject.fromObject(wechatCouponMsg);
				WechatMsgQueueH msgQueueH = new WechatMsgQueueH();
				msgQueueH.setBusinessType(WechatMsgBusinessTypeEnum.GET_COUPON.getId());
				msgQueueH.setCreateTime(currentTime);
				msgQueueH.setMemberId(member.getMemberId());
				msgQueueH.setMsgContent(json.toString());
				msgQueueH.setMsgKey(couponInstanceExp.getCouponInstanceCode());
				msgQueueH.setIsSend("N");
				wechatMsgQueueHRepository.save(msgQueueH);
				return ResultStatus.SUCCESS;
			} else if (StatusConstant.COUPON_INSTANCE_UNUSED.getId().equals(couponInstance.getStatusId())) {
				return ResultStatus.COUPON_INSTANCE_STATUS_RECEIVE_ERROR;
			} else {
				return ResultStatus.COUPON_INSTANCE_STATUS_RECEIVE_ERROR;
			}

		} else {
			return ResultStatus.COUPON_CODE_NULL_ERROR;
		}
	}

	@Override
	public MemberExp queryEntitySearchAuto(MemberSearch search) {
		List<MemberExp> results = this.memberRepository.findListSearchAuto(search);
		return (results == null || results.isEmpty()) ? null : results.get(0);
	}

	@Override
	public MemberSummaryDay selectBySummaryDate(String date) {
		return memberSummaryRepository.selectBySummaryDate(date);
	}

	@Override
	public Long findMemberOrderCount(Long times1, Long times2, Long sort) {
		return memberAccountRepository.findMemberOrderCount(times1, times2, sort);
	}

	@Override
	public List<MemberRegion> findMemberRegion(Page<MemberRegion> page, String sort, String order) {
		return memberRepository.findMemberRegion(page, sort, order);
	}

	@Override
	public Long findAllMemberCount() {
		return memberRepository.findAllMemberCount();
	}

	@Override
	public List<MemberAge> findMemberAge(String startDate, String endDate) {
		return memberRepository.findMemberAge(startDate, endDate);
	}

	@Override
	public MemberExp queryExpByMemberId(Long memberId) {
		MemberExp memberExp = null;
		memberExp = memberRepository.findExpByMemberId(memberId);
		if (memberExp != null) {
			if (memberExp.getPointsBalance() == null) {
				memberExp.setPointsBalance(new BigDecimal(0));
			}
			if (memberExp.getTotalPoints() == null) {
				memberExp.setTotalPoints(new BigDecimal(0));
			}
			if (memberExp.getTotalConsumption() == null) {
				memberExp.setTotalConsumption(new BigDecimal(0));
			}
			if (memberExp.getTotalConsumptionCount() == null) {
				memberExp.setTotalConsumptionCount(0);
			}
		}
		return memberExp;
	}

	@Override
	public Member queryMemberByCode(String memberCode) {
		return memberRepository.findByMemberCode(memberCode);
	}

	@Override
	public MemberExp findMemberExpByCode(String code, String queryType) {
		return memberRepository.findMemberExpByCode(code, queryType);
	}

	/**
	 * validateCaptcha:(校验手机验证码). <br/>
	 * Date: 2016-4-7 下午6:41:04 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param comvCode
	 * @param mobile
	 */
	private void validateVerifyCode(String mobile, String comvCode, String isBinding) {
		MemberSearch search = null;
		Example example = null;
		List<ComVerification> comVs = null;

		if (StringUtils.isBlank(mobile)) {
			throw new MemberException(ResultStatus.MEMBER_PHONE_NULL_ERROR);
		} else {
			if (StringUtils.isBlank(isBinding) || !isBinding.equals("Y")) {
				search = new MemberSearch();
				search.setEqualMobile(mobile);
				Long count = queryCountSearchAuto(search);
				if (count > 0) {
					throw new MemberException(ResultStatus.MEMBER_PHONE_EXIST_ERROR);
				}
			}
		}

		// 验证验证码
		example = new Example();
		Criteria exampleTemp = example.createCriteria();
		exampleTemp.andEqualTo("MOBILE", mobile);
		exampleTemp.andEqualTo("VERIFICATION_CODE", comvCode);
		comVs = findComvByExample(example);
		if (comVs == null || comVs.size() == 0) {
			throw new MemberException(ResultStatus.COMV_MISMATCH_ERROR);
		} else {
			ComVerification comvTemp = comVs.get(0);
			// 验证是否过期 未过期则修改验证码数据
			if (Long.parseLong(DateUtils.getCurrentTimeOfDb()) > Long.parseLong(comvTemp.getExpireTime())) {
				throw new MemberException(ResultStatus.COMV_OVERDUE_ERROR);
			} else {
				comvTemp.setIsPass("Y");
				comvTemp.setIsActive("N");
				comvTemp.setIsComplement("Y");
				comvTemp.setUpdateTime(DateUtils.getCurrentTimeOfDb());
				updateComVerification(comvTemp);
			}
		}
	}
	@Value("${grade_calc_mode}")
	private  static String GRADE_CALC_MODE;
	private  static String REGISTER_TIME = "register_time";
	private  static String FIRST_CONSUMPTION_TIME = "first_consumption_time";
	
	@Override
	public BigDecimal upGrade(Member member) {
		MemberExp memberExp = null;
		GradeConf gradeConf = null;
		int yearFrequency = 0;
		String beginDate = null;
		String endDate = null;
		String calcTime = null;
		BigDecimal gradeFactorsValue = null;
		try {
			
			gradeConf = GradeInit.GRADE_CONF;
			//如果是手动升级
			if(gradeConf.getChangeType().equals(GradeChangeTypeEnums.HANDLE.getId())){
				return null;
			}
			
			if(StringUtils.isBlank(GRADE_CALC_MODE) || GRADE_CALC_MODE.equals(FIRST_CONSUMPTION_TIME)){
				calcTime = member.getFirstConsumptionTime();
			}else if(GRADE_CALC_MODE.equals(REGISTER_TIME)){
				calcTime = member.getRegisterTime();
			}else if(GRADE_CALC_MODE.equals(FIRST_CONSUMPTION_TIME)){
				calcTime = member.getFirstConsumptionTime();
			}
			
			//如果计算时间为空
			if(StringUtils.isBlank(calcTime)){
				return null;
			}
			
			yearFrequency = Integer.parseInt(gradeConf.getUpPeriod());
			beginDate = getBeginDate(yearFrequency,calcTime);
			endDate = DateUtils.getCurrentDateOfDb();
			
			if(Integer.parseInt(endDate.substring(0,4)) - Integer.parseInt(beginDate.substring(0,4)) < yearFrequency){
				endDate = (Integer.parseInt(endDate.substring(0,4))+yearFrequency) +endDate.substring(4);
			}
			//如果是根据积分升级
			if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.POINTS_PERIOD.getId())){
				memberExp = memberRepository.findPeriodPointsByMemberId(member.getMemberId(), beginDate, endDate);
				if(memberExp == null || memberExp.getPointsPeriod() == null){
					return null;
				}
				gradeFactorsValue = memberExp.getPointsPeriod(); 
			}
			
			if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getId())){
				memberExp = memberRepository.findPeriodOrderAmountByMemberId(member.getMemberId(), beginDate, endDate);
				if(memberExp == null || memberExp.getOrderAmountPeriod() == null){
					return null;
				}
				gradeFactorsValue = memberExp.getOrderAmountPeriod(); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gradeFactorsValue;
	}
	
	private String getBeginDate(int yearFrequency,String calcTime){
		String registerDate = null;
		int registerYear = 0;
		String registerDay = null;
		
		String currentDate = null;
		int currentYear = 0;
		String currentDay = null;
		int yearDiff = 0;
		
		String beginDate = null;
		
		registerDate = calcTime.substring(0,8);
		registerYear = Integer.parseInt(registerDate.substring(0,4)); 
		registerDay = registerDate.substring(4); 
		
		currentDate = DateUtils.getCurrentDateOfDb();
		currentYear = Integer.parseInt(currentDate.substring(0,4)); 
		currentDay = currentDate.substring(4);
		
		
		yearDiff = (currentYear - registerYear) % yearFrequency;
		
		//如果是周期年
		if(yearDiff == 0 ){
			//如果计算日大于当前日期，则本还在本周期年中
			if(registerDay.compareTo(currentDay) > 0){
				beginDate = (currentYear - yearFrequency) + registerDay;
			}else{//否则开始新的周期年
				beginDate = currentYear + registerDay;
			}
		}else{//在周期年内
			beginDate = (currentYear - yearDiff) + registerDay;
		}
		
		return beginDate;
	}

	@Autowired
	public void setGeoRepository(GeoRepository geoRepository) {
		this.geoRepository = geoRepository;
	}

	@Autowired
	public void setOrgRepository(OrgRepository orgRepository) {
		this.orgRepository = orgRepository;
	}

	@Autowired
	public void setSqlAttributeRepository(SqlAttributeRepository sqlAttributeRepository) {
		this.sqlAttributeRepository = sqlAttributeRepository;
	}

	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}

	@Autowired
	public void setMemberCardRepository(MemberCardRepository memberCardRepository) {
		this.memberCardRepository = memberCardRepository;
	}

	@Autowired
	public void setMemberSummaryRepository(MemberSummaryRepository memberSummaryRepository) {
		this.memberSummaryRepository = memberSummaryRepository;
	}

	@Autowired
	public void setWeChatFansRepository(WeChatFansRepository weChatFansRepository) {
		this.weChatFansRepository = weChatFansRepository;
	}

	@Autowired
	public void setComVerificationRepository(ComVerificationRepository comVerificationRepository) {
		this.comVerificationRepository = comVerificationRepository;
	}

	@Autowired
	public void setCouponInstanceRepository(CouponInstanceRepository couponInstanceRepository) {
		this.couponInstanceRepository = couponInstanceRepository;
	}

	@Autowired
	public void setCouponInstanceReceiveRepository(CouponInstanceReceiveRepository couponInstanceReceiveRepository) {
		this.couponInstanceReceiveRepository = couponInstanceReceiveRepository;
	}

	@Autowired
	public void setWechatMsgQueueHRepository(WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}

	@Autowired
	public void setMemberGroupItemRepository(MemberGroupItemRepository memberGroupItemRepository) {
		this.memberGroupItemRepository = memberGroupItemRepository;
	}

	@Autowired
	public void setMemberTagItemRepository(MemberTagItemRepository memberTagItemRepository) {
		this.memberTagItemRepository = memberTagItemRepository;
	}

	@Autowired
	public void setMemberAccountRepository(MemberAccountRepository memberAccountRepository) {
		this.memberAccountRepository = memberAccountRepository;
	}

	@Autowired
	public void setMemberExtInfoRepository(MemberExtInfoRepository memberExtInfoRepository) {
		this.memberExtInfoRepository = memberExtInfoRepository;
	}

	@Autowired
	public void setMemberTagRepository(MemberTagRepository memberTagRepository) {
		this.memberTagRepository = memberTagRepository;
	}

	@Autowired
	public void setMemberGroupRepository(MemberGroupRepository memberGroupRepository) {
		this.memberGroupRepository = memberGroupRepository;
	}

	@Autowired
	public void setMemberAddressRepository(MemberAddressRepository memberAddressRepository) {
		this.memberAddressRepository = memberAddressRepository;
	}

	@Autowired
	public void setExtAccountBindingRepository(ExtAccountBindingRepository extAccountBindingRepository) {
		this.extAccountBindingRepository = extAccountBindingRepository;
	}

	@Autowired
	public void setGradeConvertRecordRepository(GradeConvertRecordRepository gradeConvertRecordRepository) {
		this.gradeConvertRecordRepository = gradeConvertRecordRepository;
	}

	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}

	@Autowired
	public void setWeChatFansService(WeChatFansService weChatFansService) {
		this.weChatFansService = weChatFansService;
	}

	@Override
	public Long queryActiveMemberCount() {
		return this.memberRepository.queryActiveMemberCount();
	}

	@Autowired
	public void setMemberPointsItemService(MemberPointsItemSevice memberPointsItemService) {
		this.memberPointsItemService = memberPointsItemService;
	}

	@Autowired
	public void setMemberEntityCardRepository(MemberEntityCardRepository memberEntityCardRepository) {
		this.memberEntityCardRepository = memberEntityCardRepository;
	}
	@Autowired
	public void setAccessTokenCacheService(AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}
}
