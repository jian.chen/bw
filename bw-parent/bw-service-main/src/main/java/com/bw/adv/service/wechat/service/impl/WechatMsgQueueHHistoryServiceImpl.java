/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMsgQueueHHistoryServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年12月16日上午11:31:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.wechat.model.WechatMsgQueueHHistory;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHHistoryRepository;
import com.bw.adv.service.wechat.service.WechatMsgQueueHHistoryService;

/**
 * ClassName:WechatMsgQueueHHistoryServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月16日 上午11:31:46 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatMsgQueueHHistoryServiceImpl extends BaseServiceImpl<WechatMsgQueueHHistory> implements WechatMsgQueueHHistoryService{
	private WechatMsgQueueHHistoryRepository wechatMsgQueueHHistoryRepository;
	@Override
	public void saveAllWechatMsgQueueHHistory(List<WechatMsgQueueHHistory> historys) {
		wechatMsgQueueHHistoryRepository.saveAllWechatMsgQueueHHistory(historys);
	}

	@Override
	public BaseRepository<WechatMsgQueueHHistory, ? extends BaseMapper<WechatMsgQueueHHistory>> getBaseRepository() {
		return wechatMsgQueueHHistoryRepository;
	}

	@Autowired
	public void setWechatMsgQueueHHistoryRepository(WechatMsgQueueHHistoryRepository wechatMsgQueueHHistoryRepository) {
		this.wechatMsgQueueHHistoryRepository = wechatMsgQueueHHistoryRepository;
	}

}

