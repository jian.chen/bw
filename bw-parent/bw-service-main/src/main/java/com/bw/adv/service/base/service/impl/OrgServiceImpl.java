/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.exp.OrgExp;
import com.bw.adv.module.base.repository.OrgRepository;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.service.base.service.OrgService;


/**
 * ClassName: ProductServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:15:40 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Service
public class OrgServiceImpl extends BaseServiceImpl<Org> implements OrgService{

	private OrgRepository orgRepository;	
	
	@Override
	public BaseRepository<Org, ? extends BaseMapper<Org>> getBaseRepository() {
		
		return orgRepository;
	}

	@Override
	public List<Org> queryListForOrg(String useFlag) {
		return this.orgRepository.findOrgList(useFlag);
	}

	@Autowired
	public void setOrgRepository(OrgRepository orgRepository) {
		this.orgRepository = orgRepository;
	}

	@Override
	public List<Org> queryListForParentOrg(Long upOrgId) {
		return this.orgRepository.findParentOrgList(upOrgId);
	}

	@Override
	public List<Org> queryListForChildOrg(Long orgId) {
		return this.orgRepository.findChildOrgList(orgId);
	}


	@Override
	public void deleteOrg(String stopDate,Example example) {
			this.orgRepository.deleteOrg(stopDate,example);
	}

	@Override
	public void deleteStoreOrgId(Example example) {
			this.orgRepository.updateStoreOrgIdByExample(example);
	}

	@Override
	public List<Org> queryOrgByName(String useFlag, String orgName,
			Long pareOrgId) {
		return this.orgRepository.findOrgByName(useFlag, orgName, pareOrgId);
	}

	@Override
	public List<Org> queryOrgForTree() {
		
		return this.orgRepository.findListForTree("Y");
	}

	@Override
	public OrgExp queryOrgById(Long orgId) {
		return this.orgRepository.findOrgById(orgId);
	}

	@Override
	public Org queryOrg(Long orgId) {
		return this.orgRepository.queryOrg(orgId);
	}

}

