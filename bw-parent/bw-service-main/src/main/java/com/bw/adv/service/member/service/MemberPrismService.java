package com.bw.adv.service.member.service;

import java.util.List;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberPrism;

/**
 * 会员棱镜
 * @author cuicd
 *
 */
public interface MemberPrismService {
	
	/**
	 * 查询所有棱镜(分页)
	 * @param templatePage
	 * @return
	 * @author cuicd
	 */
	public List<MemberPrism> queryAllMemberPrism(Page<MemberPrism> templatePage);

}
