/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageGroupItemService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月18日下午4:37:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatMessageGroupItem;

/**
 * ClassName:WechatMessageGroupItemService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:37:56 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMessageGroupItemService extends BaseService<WechatMessageGroupItem> {
	
}

