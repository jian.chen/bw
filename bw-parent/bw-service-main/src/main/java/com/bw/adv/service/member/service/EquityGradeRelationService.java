/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:EquityGradeRelationService.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2016年1月14日下午9:48:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import java.util.List;

import com.bw.adv.module.member.model.EquityGradeRelation;

/**
 * ClassName:EquityGradeRelationService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 下午9:48:40 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
public interface EquityGradeRelationService {
  
	/**
	 * queryInfoById:查看会员权益等级. <br/>
	 * Date: 2016年1月14日 下午5:40:57 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberEquityid
	 * @return
	 */
	List<EquityGradeRelation> queryGradeInfoById(Long memberEquityid);
}

