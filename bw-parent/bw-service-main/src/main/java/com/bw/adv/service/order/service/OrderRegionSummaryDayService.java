package com.bw.adv.service.order.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.order.model.OrderRegionSummaryDay;
import com.bw.adv.module.report.order.model.exp.OrderRegionSummaryDayExp;

import java.util.List;

public interface OrderRegionSummaryDayService extends BaseService<OrderRegionSummaryDay> {
	/**
	 * queryByExp:根据条件查询订单报表数据<br/>
	 * Date: 2015年12月31日 下午2:44:17 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	List<OrderRegionSummaryDayExp> queryByExp(Example example);

	/**
	 * 
	 * callOrderRegionSummaryDay:(调用订单日统计存储过程). <br/>
	 * Date: 2016年1月14日 下午4:14:53 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	void callOrderRegionSummaryDay(String dateStr);
}
