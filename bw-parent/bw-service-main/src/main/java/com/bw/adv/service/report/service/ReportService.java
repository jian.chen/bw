package com.bw.adv.service.report.service;

public interface ReportService{
	
	/**
	 * 调用会员年龄统计存储过程
	 * @param dateStr
	 */
	public void callAgeSummary(String dateStr);

}
