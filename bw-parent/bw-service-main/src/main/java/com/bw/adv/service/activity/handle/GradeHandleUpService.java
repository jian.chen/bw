package com.bw.adv.service.activity.handle;


import java.math.BigDecimal;

import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.Member;


/**
 * ClassName: GradeHandleUpService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-29 上午11:21:20 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface GradeHandleUpService  {
	
	
	/**
	 * upGrade:(根据条件执行，升级操作). <br/>
	 * Date: 2015-9-18 下午3:11:30 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param member：要升级的会员
	 * @param currentGrade：会员当前等级
	 * @param changeGrade：变化后的等级，给改对象赋值使用BeanUtils.copy(grade, changeGrade)方法，否则等级无法变化
	 * @param gradeFactorsValue：促使等级变化的值
	 * @return
	 */
	 BigDecimal upGrade(Member member,Grade currentGrade,Grade changeGrade,BigDecimal gradeFactorsValue);
	
	 
	 /**
	 * operateGradeUpBusiness:(升级后续操作，保存等级记录，保存升级动作). <br/>
	 * Date: 2015-9-19 下午2:04:36 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 */
	public void operateGradeUpBusiness(Long memberId);
	 
}
















