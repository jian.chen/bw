/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMenuClickRecordServiceImpl.java
 * Package Name:com.sage.scrm.module.wechat.service.impl
 * Date:2015年12月9日下午3:58:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/
package com.bw.adv.service.wechat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.wechat.model.WechatMenuClickRecord;
import com.bw.adv.module.wechat.repository.WechatMenuClickRecordRepository;
import com.bw.adv.service.wechat.service.WechatMenuClickRecordService;

/**
 * ClassName:WechatMenuClickRecordServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午3:58:30 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatMenuClickRecordServiceImpl extends BaseServiceImpl<WechatMenuClickRecord> implements WechatMenuClickRecordService{
	private WechatMenuClickRecordRepository wechatMenuClickRecordRepository;
	@Override
	public BaseRepository<WechatMenuClickRecord, ? extends BaseMapper<WechatMenuClickRecord>> getBaseRepository() {
		return wechatMenuClickRecordRepository;
	}
	@Autowired
	public void setWechatMenuClickRecordRepository(WechatMenuClickRecordRepository wechatMenuClickRecordRepository) {
		this.wechatMenuClickRecordRepository = wechatMenuClickRecordRepository;
	}
}

