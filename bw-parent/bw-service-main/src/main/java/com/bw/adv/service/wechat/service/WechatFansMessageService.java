/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansMessageService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月25日下午5:02:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatFansMessage;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;

/**
 * ClassName:WechatFansMessageService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午5:02:25 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansMessageService extends BaseService<WechatFansMessage> {
	
	public void saveWechatFansMessage(Map<String,String> map);
	
}

