/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ComplainServiceImpl.java
 * Package Name:com.sage.scrm.service.complain.service.impl
 * Date:2015年12月29日下午4:48:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.complain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.complain.model.Complain;
import com.bw.adv.module.member.complain.model.ComplainType;
import com.bw.adv.module.member.complain.model.exp.ComplainExp;
import com.bw.adv.module.member.complain.repository.ComplainRepository;
import com.bw.adv.module.member.complain.repository.ComplainTypeRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;
import com.bw.adv.service.complain.service.ComplainService;

import net.sf.json.JSONObject;

/**
 * ClassName:ComplainServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月29日 下午4:48:28 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class ComplainServiceImpl extends BaseServiceImpl<Complain> implements ComplainService{
	private ComplainRepository complainRepository;
	
	private ComplainTypeRepository complainTypeRepository;
	
	private WechatMsgQueueHRepository wechatMsgQueueHRepository;
	
	@Override
	public List<ComplainExp> queryByExa(Example example, Page<ComplainExp> page) {
		
		return complainRepository.findByExa(example, page);
	}
	
	@Override
	public BaseRepository<Complain, ? extends BaseMapper<Complain>> getBaseRepository() {
		return complainRepository;
	}
	@Override
	public ComplainExp queryDetailByPk(Long complainId) {
		
		return complainRepository.findDetailByPk(complainId);
	}
	@Autowired
	public void setComplainRepository(ComplainRepository complainRepository) {
		this.complainRepository = complainRepository;
	}

	/**
	 * 
	 * TODO 关闭建议后给用户发送消息（可选）.
	 * @see com.bw.adv.service.complain.service.ComplainService#sendWcMsgByCloseComplain(com.bw.adv.module.member.complain.model.Complain)
	 */
	@Override
	public void sendWcMsgByCloseComplain(Complain complain) {
		WechatMsgQueueH wechatMsgQueueH = new WechatMsgQueueH();
		JSONObject json = new JSONObject();
		ComplainType complainType = null;
		Long complainTypeId = complain.getComplainTypeId();
		String complainName = null;
		if (complainTypeId != null) {
			complainType = complainTypeRepository.getMapper().selectByPrimaryKey(complain.getComplainTypeId());
			if (complainType != null) {
				complainName = complainType.getComplainTypeName();
			}
		}
		json.put("complainId", complain.getComplainId());
		if (complainName == null) {
			complainName = "";
		}
		json.put("complainType", complainName);
		json.put("complainStatus", StatusConstant.COMPLAIN_CLOSED.getName());
		json.put("createTime", complain.getCreateTime());
		wechatMsgQueueH.setBusinessType(WechatMsgBusinessTypeEnum.CLOSE_COMPLAIN.getId());
		wechatMsgQueueH.setCreateTime(DateUtils.getCurrentTimeOfDb());
		wechatMsgQueueH.setMemberId(complain.getMemberId());
		wechatMsgQueueH.setMsgContent(json.toString());
		wechatMsgQueueH.setMsgKey(complain.getComplainCode());
		wechatMsgQueueH.setIsSend("N");
		wechatMsgQueueHRepository.save(wechatMsgQueueH);
	}
	
	@Autowired
	public void setWechatMsgQueueHRepository(WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}

	@Autowired
	public void setComplainTypeRepository(ComplainTypeRepository complainTypeRepository) {
		this.complainTypeRepository = complainTypeRepository;
	}

}

