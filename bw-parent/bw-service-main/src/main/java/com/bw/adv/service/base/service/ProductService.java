/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;
import java.util.Map;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.base.model.ProductCategoryItem;
import com.bw.adv.module.base.model.exp.ProductExp;
import com.bw.adv.module.common.model.Example;


/**
 * ClassName: ProductService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:16:31 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface ProductService extends BaseService<Product>{

	/**
	 * queryListForProduct:查询产品维护列表. <br/>
	 * Date: 2015-8-14 下午12:14:58 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	List<ProductExp> queryListForProduct(Long statusId,Page<ProductExp> page,Map<String,Object> map);
	
	/**
	 * queryListForProductByProductCode:通过产品编号查询产品列表. <br/>
	 * Date: 2015-9-16 下午7:33:49 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param productCode
	 * @return
	 */
	List<Product> queryListForProductByProductCode(Long statusId,String productCode);
	
	/**
	 * 
	 * queryByCode:(根据多个code查询产品(用于活动)). <br/>
	 * Date: 2015年10月8日 下午8:31:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param statusId
	 * @param productCodes
	 * @return
	 */
	List<Product> queryByCode(Long statusId,String productCodes);
	
	/**
	 * 
	 * queryByExample:根据条件查询产品列表
	 * Date: 2015年9月9日 下午4:30:56 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<Product> queryByExample(Example example,Page<Product> page);
	
	/**
	 * insertProducts:批量新增产品. <br/>
	 * Date: 2015-8-26 上午11:03:16 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param list
	 */
	void insertProducts(List<Product> list);
	
	/**
	 * deleteProducts:删除产品 <br/>
	 * Date: 2015-8-26 上午11:03:34 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param ids
	 * @param statusId
	 */
	void deleteProducts(Long productId);
	
	/**
	 * queryProductByPrimaryKey:(通过id查询产品明细). <br/>
	 * Date: 2015-9-19 下午2:51:15 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param statusId
	 * @param productId
	 * @return
	 */
	Product queryProductByPrimaryKey(Long statusId,Long productId);

	/**
	 * renovateProduct:(更新产品主数据). <br/>
	 * Date: 2015-12-15 下午9:49:59 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	void renovateProduct();
	
	/**
	 * 根据产品id查询产品所属种类明细
	 * @param productId
	 * @return
	 */
	List<ProductCategoryItem> queryCategoryItemById(Long productId);
}

