/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WinningItemService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2015年11月23日下午2:10:38
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.WinningItemExp;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName:WinningItemService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:10:38 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WinningItemService extends BaseService<WinningItem> {
	
	/**
	 * countPersonsCountByActivityIdAndAwardsId:(统计某一个周期某个奖项的获奖人数). <br/>
	 * Date: 2015年11月25日 下午2:04:53 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param awardsId
	 * @param sceneGameInstanceId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	Long countPersonsCountByActivityIdAndAwardsId(Long awardsId,Long sceneGameInstanceId,Long memberId, String startDate, String endDate);
	
	/**
	 * 
	 * findWinningItemListByMemberIdAndActivityId:(查询会员获奖记录). <br/>
	 * Date: 2015年12月15日 上午11:14:37 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<WinningItemExp> findWinningItemListByMemberIdAndActivityId(Example example, Page<WinningItemExp> page);
	/**
	 * 
	 * findWinningItemPersonsByExample:(汇总会员获奖记录). <br/>
	 * Date: 2015年12月15日 上午11:14:37 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	Long findWinningItemPersonsByExample(Example example);
	
	
	/**
	 * 
	 * findLastNum:(查询最近几条中奖纪录). <br/>
	 * Date: 2016年3月22日 下午7:49:11 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param num
	 * @return
	 */
	List<WinningItemExp> findLastNum(Long num);
}

