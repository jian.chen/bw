package com.bw.adv.service.job.service.bean;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.component.job.base.ManagedThreadPoolTaskExecutor;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.model.TaskResult;
import com.bw.adv.module.tools.DateUtils;

public class ComTaskJobBean extends BaseJob {
	
	private static final Logger logger = LoggerFactory.getLogger(ComTaskJobBean.class);
	private ManagedThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Override
	public synchronized void excute() throws Exception {
		init();
		logger.info("系统任务执行开始。。。。。。");
		Map<ComTask, List<Future<TaskResult>>> futureMap = new HashMap<ComTask, List<Future<TaskResult>>>();
		List<ComTask> comTaskList = comTaskRepositoryService.findByStatusId(StatusConstant.JOB_NEW.getId()+","+StatusConstant.JOB_SUCCEED.getId());
		for (ComTask comTask : comTaskList) {
			String currentDate = DateUtils.getCurrentDateOfDb();
			//任务开始时间未到
			if(comTask.getStartDate() != null && currentDate.compareTo(comTask.getStartDate()) < 0){
				logger.debug(comTask.getTaskName() + ",任务执行时间为到");
				continue;
			}
			//任务 已经 过期 
			if(comTask.getEndDate() != null && currentDate.compareTo(comTask.getEndDate()) > 0){
				comTask.setLastStatus(StatusConstant.JOB_EXPIRED.getId().toString());
				comTaskRepositoryService.updateByPkSelective(comTask);
				logger.debug(comTask.getTaskName() + ",任务已经过期");
				continue;
			}
		}
		logger.info("系统任务执行结束。。。。。。");
	}


	
	public void init(){
		this.threadPoolTaskExecutor = this.getApplicationContext().getBean(ManagedThreadPoolTaskExecutor.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.COM_TASK_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}
	
	

}
