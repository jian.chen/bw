package com.bw.adv.service.sys.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.sys.model.SysUserRoleKey;
import com.bw.adv.module.sys.repository.SysUserRoleRepository;
import com.bw.adv.service.sys.service.SysUserRoleService;


@Service
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRoleKey> implements SysUserRoleService {

	private SysUserRoleRepository sysUserRoleRepository;
	

	@Override
	public BaseRepository<SysUserRoleKey, ? extends BaseMapper<SysUserRoleKey>> getBaseRepository() {
		return sysUserRoleRepository;
	}

	@Autowired
	public void setSysUserRoleRepository(SysUserRoleRepository sysUserRoleRepository) {
		this.sysUserRoleRepository = sysUserRoleRepository;
	}

}
