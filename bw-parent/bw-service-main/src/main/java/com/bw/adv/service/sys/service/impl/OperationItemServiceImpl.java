/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SysRoleServiceImpl.java
 * Package Name:com.sage.scrm.service.sys.service.impl
 * Date:2015-8-14下午4:48:43
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.sys.model.OperationItem;
import com.bw.adv.module.sys.repository.OperationItemRepository;
import com.bw.adv.service.sys.service.OperationItemService;

/**
 * ClassName:SysRoleServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-14 下午4:48:43 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class OperationItemServiceImpl extends BaseServiceImpl<OperationItem> implements OperationItemService {

	private OperationItemRepository operationItemRepository;
	
	@Override
	public BaseRepository<OperationItem, ? extends BaseMapper<OperationItem>> getBaseRepository() {
		return operationItemRepository;
	}
	
	@Autowired
	public void setSysRoleRepository(OperationItemRepository operationItemRepository) {
		this.operationItemRepository = operationItemRepository;
	}

	@Override
	public List<OperationItem> findListForOperationItem() {
		
		return this.operationItemRepository.findOperationItemList();
	}


	

}

