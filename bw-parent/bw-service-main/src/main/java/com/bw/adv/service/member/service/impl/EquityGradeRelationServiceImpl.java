/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:EquityGradeRelationServiceImpl.java
 * Package Name:com.sage.scrm.service.member.service.impl
 * Date:2016年1月14日下午9:50:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.member.equity.repository.EquityGradeRelationRepository;
import com.bw.adv.module.member.model.EquityGradeRelation;
import com.bw.adv.service.member.service.EquityGradeRelationService;

/**
 * ClassName:EquityGradeRelationServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 下午9:50:54 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class EquityGradeRelationServiceImpl extends BaseServiceImpl<EquityGradeRelation> implements
		EquityGradeRelationService {
	
    @Autowired
	private EquityGradeRelationRepository egRelationRepository;
	
	@Override
	public List<EquityGradeRelation> queryGradeInfoById(Long memberEquityid) {
		return egRelationRepository.findGradeInfoById(memberEquityid);
	}

	@Override
	public BaseRepository<EquityGradeRelation, ? extends BaseMapper<EquityGradeRelation>> getBaseRepository() {
		return egRelationRepository;
	}

}

