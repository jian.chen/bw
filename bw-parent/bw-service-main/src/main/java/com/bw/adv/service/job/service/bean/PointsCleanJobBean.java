/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PointsCleanJobBean.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2015-10-9下午5:29:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.service.points.service.MemberPointsItemSevice;

/**
 * ClassName:PointsCleanJobBean 积分清理 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-10-9 下午5:29:26 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public class PointsCleanJobBean extends BaseJob {

	
	private MemberPointsItemSevice memberPointsItemSevice;
	
	@Override
	protected void excute(){
		try {
			initService();
			memberPointsItemSevice.cleanPointsItem();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("积分过期任务异常："+e.getMessage());
		}
		
	}
	
	
	private void initService() {
		this.memberPointsItemSevice = this.getApplicationContext().getBean(MemberPointsItemSevice.class);
	}
	
	
	@Override
	protected String getLockCode() {
		return LockCodeConstant.POINTS_EXPIRE_JOB;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

	
}

