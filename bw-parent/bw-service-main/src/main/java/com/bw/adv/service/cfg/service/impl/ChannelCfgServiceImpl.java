package com.bw.adv.service.cfg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.repository.ChannelCfgRepository;
import com.bw.adv.module.cfg.model.ChannelCfg;
import com.bw.adv.module.cfg.model.ChannelCfgExp;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.cfg.service.ChannelCfgService;
/**
 * ClassName: ChannelCfgServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月17日 下午3:48:10 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
@Service
public class ChannelCfgServiceImpl  extends BaseServiceImpl<ChannelCfg> implements ChannelCfgService {

	private ChannelCfgRepository channelCfgRespository;
	
	private final String HAND="hand";
	private final String AUTO="auto";
	
	@Override
	public BaseRepository<ChannelCfg, ? extends BaseMapper<ChannelCfg>> getBaseRepository() {
		return channelCfgRespository;
	}
	
	@Autowired
	public void setChannelCfgRespository(ChannelCfgRepository channelCfgRespository) {
		this.channelCfgRespository = channelCfgRespository;
	}


	@Override
	public ChannelCfg queryChannelCfgByChannelId(Long channelId) {
		return channelCfgRespository.queryChannelCfgByChannelId(channelId);
	}

	@Override
	public List<ChannelCfgExp> queryChannelInstanceByChannelId(Long channelId, Page<ChannelCfgExp> page) {
		return channelCfgRespository.queryChannelInstanceByChannelId(channelId, page);
	}

	@Override
	public void modifyChannelCfgStatus(SysUser sysUser, Long channelCfgId,Long status,String type) {
		ChannelCfg channelCfg = new ChannelCfg();
		channelCfg.setChannelCfgId(channelCfgId);
		channelCfg.setStatusId(status);
		channelCfg.setUpdateTime(DateUtils.getCurrentTimeOfDb());
		if(StatusConstant.ACTIVITY_INSTANCE_BREAK.getId().equals(status)){
			channelCfg.setBreakTime(DateUtils.getCurrentDateOfDb());
		}
		if(type.equals(HAND)){
			channelCfg.setUpdateBy(sysUser.getUserId());
		}
		
		channelCfgRespository.updateByPkSelective(channelCfg);
	}
	
	

	@Override
	public List<ChannelCfgExp> queryChannelCfgExpByChannelId(Long channelId) {
		return channelCfgRespository.queryChannelCfgExpByChannelId(channelId);
	}
	
	
	
	
}
