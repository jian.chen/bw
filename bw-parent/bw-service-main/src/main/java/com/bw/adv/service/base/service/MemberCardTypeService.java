/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.model.MemberCardType;
import com.bw.adv.module.sys.enums.MemberCardTypeNoteEnum;

/**
 * ClassName: OrgService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-18 下午6:11:05 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface MemberCardTypeService extends BaseService<MemberCardType>{

	public List<MemberCardType> queryListForMemberCardType();
	
	public void updateMemberCardType(Long memberCardTypeId,MemberCardTypeNoteEnum memberCardTypeNoteEnum);

}

