/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PointsRuleSevice.java
 * Package Name:com.sage.scrm.service.points.service.impl
 * Date:2015年8月20日下午5:06:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.points.model.PointsConf;

/**
 * ClassName: PointsConfSevice <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016-3-18 下午4:00:42 <br/>
 * scrmVersion standard
 * @author mennan
 * @version jdk1.7
 */
public interface PointsConfSevice extends BaseService<PointsConf> {
	
}

