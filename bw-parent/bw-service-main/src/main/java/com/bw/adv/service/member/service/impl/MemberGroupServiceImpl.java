package com.bw.adv.service.member.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.model.MemberGroupItem;
import com.bw.adv.module.member.group.repository.MemberGroupItemRepository;
import com.bw.adv.module.member.group.repository.MemberGroupRepository;
import com.bw.adv.module.member.group.search.MemberGroupItemSearch;
import com.bw.adv.module.member.group.search.MemberGroupSearch;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.service.member.service.MemberGroupService;

@Service
public class MemberGroupServiceImpl extends BaseServiceImpl<MemberGroup> implements MemberGroupService {

	private MemberRepository memberRepository;
	private MemberGroupRepository memberGroupRepository;
	private MemberGroupItemRepository memberGroupItemRepository;
	
	
	@Override
	public Long queryCountSearchAuto(MemberGroupSearch search) {
		return this.memberGroupRepository.findCountSearchAuto(search);
	}
	@Override
	public List<MemberGroup> queryListSearchAuto(MemberGroupSearch search) {
		return this.memberGroupRepository.findListSearchAuto(search);
	}
	@Override
	public MemberGroup queryEntityPkAuto(Long memberGroupId) {
		return this.memberGroupRepository.findByPk(memberGroupId);
	}
	@Override
	@Transactional
	public int insertMemberGroup(MemberGroup memberGroup) {
		
		return this.memberGroupRepository.saveSelective(memberGroup);
	}
	@Override
	@Transactional
	public int updateMemberGroup(MemberGroup memberGroup) {
		
		return this.memberGroupRepository.updateByPkSelective(memberGroup);
	}
	
	@Override
	@Transactional
	public void saveAllMemberToGroup(MemberGroup memberGroup) {
		this.memberGroupItemRepository.deleteGroupItemByGroupId(memberGroup.getMemberGroupId());
		this.memberGroupRepository.updateByPkSelective(memberGroup);
	}
	@Override
	@Transactional
	public int deleteteMemberGroupById(Long memberGroupId) {
		this.memberGroupItemRepository.deleteGroupItemByGroupId(memberGroupId);
		this.memberGroupRepository.removeByPk(memberGroupId);
		return 0;
	}
	@Override
	@Transactional
	public void deleteMemberGroupItem(Long memberGroupId, Long memberId) {
		MemberGroupItemSearch search = null;
		MemberGroupItem item = null;
		search = new MemberGroupItemSearch();
		search.setEqualMemberGroupId(memberGroupId);
		search.setEqualMemberId(memberId);
		item = memberGroupItemRepository.findEntitySearchAuto(search);
		if(item != null){
			memberGroupItemRepository.removeByPk(item.getMemberGroupItemId());
		}
	}
	
	@Override
	@Transactional
	public Long insertBatchMemberGroupItem(Long memberGroupId,String memberIds) {
		MemberGroupItemSearch search = null;
		
		//已经存在的会员
		List<MemberGroupItem> itemList = null;
		
		//新增的会员列表
		List<MemberGroupItem> addItemList = null;
		
		String[] memberIdArr = null;
		MemberGroupItem item = null;
		
		if(StringUtils.isNotBlank(memberIds) && memberGroupId != null){
			//先查询标签下面已有会员
			search = new MemberGroupItemSearch();
			search.setEqualMemberGroupId(memberGroupId);
			itemList = this.memberGroupItemRepository.findListSearchAuto(search);
			
			//新添加的会员数据和标签已有会员数据去重（重写MemberGroupItem的equal方法）
			memberIdArr = memberIds.split(",");
			addItemList = new ArrayList<MemberGroupItem>();
			for(String memberId : memberIdArr){
				item = new MemberGroupItem();
				item.setMemberId(Long.valueOf(memberId));
				item.setMemberGroupId(memberGroupId);
				addItemList.add(item);
			}
			addItemList.removeAll(itemList);
			
			//执行批量查询
			if(addItemList.size()>0){
				this.memberGroupItemRepository.insertBatchMemberGroupItem(addItemList);
			}
		}
		return (long) (addItemList == null ? 0 :addItemList.size());
		
	}
	@Override
	@Transactional
	public Long excuteMemberGroupBySql(Long memberGroupId) {
		List<Long> list = null;
		MemberGroup group = null;
		MemberGroupItem item = null;
		String sql = null;
		//新增的会员列表
		List<MemberGroupItem> addItemList = null;
		
		group = this.memberGroupRepository.findByPk(memberGroupId);
		if(group != null){
			sql = group.getConditionSql();
		}
		if(sql != null && !"".equals(sql)){
			list = this.memberRepository.findListBySql(sql);
		}
		
		if(list!= null && list.size()>0 && memberGroupId != null){
			//先删除标签下面已有会员
			this.memberGroupItemRepository.deleteGroupItemByGroupId(memberGroupId);
			addItemList = new ArrayList<MemberGroupItem>();
			for(Long memberId : list){
				item = new MemberGroupItem();
				item.setMemberId(Long.valueOf(memberId));
				item.setMemberGroupId(memberGroupId);
				addItemList.add(item);
			}
			//执行批量查询
			if(addItemList.size()>0){
				this.memberGroupItemRepository.insertBatchMemberGroupItem(addItemList);
			}
		}
		return (long) (addItemList == null ? 0 :addItemList.size());
	}
	
	@Override
	@Transactional
	public void excuteMemberGroupJob(MemberGroup group) {
		List<Long> list = null;
		MemberGroupItem item = null;
		String sql = null;
		//新增的会员列表
		List<MemberGroupItem> addItemList = null;
		sql = group.getConditionSql();
		if(sql != null && !"".equals(sql)){
			list = this.memberRepository.findListBySql(sql);
		}
		Long memberGroupId = group.getMemberGroupId();
		if(list!= null && list.size()>0 && memberGroupId != null){
			//先删除标签下面已有会员
			this.memberGroupItemRepository.deleteGroupItemByGroupId(memberGroupId);
			addItemList = new ArrayList<MemberGroupItem>();
			for(Long memberId : list){
				item = new MemberGroupItem();
				item.setMemberId(Long.valueOf(memberId));
				item.setMemberGroupId(memberGroupId);
				addItemList.add(item);
			}
			//执行批量查询
			if(addItemList.size()>0){
				this.memberGroupItemRepository.insertBatchMemberGroupItem(addItemList);
			}
		}
	}

	@Override
	public List<MemberExp> queryMemberListOfGroup(MemberSearch search) {

		return this.memberRepository.findListSearchAuto(search);
	}
	
	@Override
	public Long queryCountMemberListOfGroup(MemberSearch search) {
		
		return this.memberRepository.findCountSearchAuto(search);
	}
	
	public MemberRepository getMemberRepository() {
		return memberRepository;
	}
	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}
	
	@Override
	public BaseRepository<MemberGroup, ? extends BaseMapper<MemberGroup>> getBaseRepository() {
		return  memberGroupRepository;
	}
	@Autowired
	public void setMemberGroupRepository(MemberGroupRepository memberGroupRepository) {
		this.memberGroupRepository = memberGroupRepository;
	}
	public MemberGroupItemRepository getMemberGroupItemRepository() {
		return memberGroupItemRepository;
	}
	@Autowired
	public void setMemberGroupItemRepository(
			MemberGroupItemRepository memberGroupItemRepository) {
		this.memberGroupItemRepository = memberGroupItemRepository;
	}
	
}
