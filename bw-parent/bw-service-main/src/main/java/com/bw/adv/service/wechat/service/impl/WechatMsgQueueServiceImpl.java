/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMsgQueueServiceimpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年12月9日下午9:49:03
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.wechat.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueue;
import com.bw.adv.module.wechat.model.WechatMsgQueueHistory;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueComExp;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueExp;
import com.bw.adv.module.wechat.repository.WechatMsgQueueRepository;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.wechat.msg.util.WechatApiUtil;
import com.bw.adv.service.wechat.service.WechatMsgQueueHistoryService;
import com.bw.adv.service.wechat.service.WechatMsgQueueService;

/**
 * ClassName:WechatMsgQueueServiceimpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午9:49:03 <br/>
 * scrmVersion 1.0
 * 
 * @author yu.zhang
 * @version jdk1.7
 * @see
 */
@Service
public class WechatMsgQueueServiceImpl extends BaseServiceImpl<WechatMsgQueue> implements WechatMsgQueueService {

	protected static final Logger logger = Logger.getLogger(WechatMsgQueueServiceImpl.class);

	@Value("${multi_limit}")
	private static Integer MULTI_LIMIT;
	@Value("${test_mode}")
	private static String TEST_MODE;
	@Value("${wechat_send_msg_max_thread}")
	private static Integer WECHAT_SEND_MSG_MAX_THREAD;
	
	
	private static ExecutorService executorService = null;  
	private static List<BaseThread> threadList = new ArrayList<BaseThread>();
	
	static {
		if (MULTI_LIMIT == null) {
			MULTI_LIMIT = 100;
		}
		if (WECHAT_SEND_MSG_MAX_THREAD == null) {
			WECHAT_SEND_MSG_MAX_THREAD = 2;
		}
	}
	

	private WechatMsgQueueRepository wechatMsgQueueRepository;
	private WechatMsgQueueHistoryService wechatMsgQueueHistoryService;
	private AccessTokenCacheService accessTokenCacheService;

	@Override
	public BaseRepository<WechatMsgQueue, ? extends BaseMapper<WechatMsgQueue>> getBaseRepository() {
		return wechatMsgQueueRepository;
	}

	@Autowired
	public void setWechatMsgQueueRepository(WechatMsgQueueRepository wechatMsgQueueRepository) {
		this.wechatMsgQueueRepository = wechatMsgQueueRepository;
	}

	@Override
	public int deleteByPrimaryKey(Long wechatMsgQueueId) {
		return wechatMsgQueueRepository.deleteByPrimaryKey(wechatMsgQueueId);
	}

	@Override
	public List<WechatMsgQueueExp> findAllWechatMsgQueues(Long limit) {
		return wechatMsgQueueRepository.findAllWechatMsgQueues(limit);
	}

	@Override
	public void deleteAllWechatMsgQueue(List<Long> list) {
		if (list != null && list.size() > 0) {
			wechatMsgQueueRepository.deleteAllWechatMsgQueue(list);
		}
	}

	@Override
	public void updateBatchWechatMsgQueue(List<WechatMsgQueueExp> list) {
		wechatMsgQueueRepository.updateBatchWechatMsgQueue(list);
	}

	@Override
	public void saveWechatMsgQueue(List<WechatMsgQueue> list) {
		if (list != null && list.size() > 0) {
			wechatMsgQueueRepository.saveList(list);
		}
	}

	@Transactional
	public void deleteWechatMsgQueue(List<WechatMsgQueueExp> list) {
		WechatMsgQueueHistory history = null;
		List<WechatMsgQueueHistory> historys = new ArrayList<WechatMsgQueueHistory>();
		List<Long> ids = new ArrayList<Long>();
		for (WechatMsgQueue instance : list) {
			history = new WechatMsgQueueHistory();
			BeanUtils.copy(instance, history);
			ids.add(instance.getWechatMsgQueueId());
			history.setSendTime(DateUtils.getCurrentTimeOfDb());
			history.setIsSend("Y");
			historys.add(history);
		}
		this.deleteAllWechatMsgQueue(ids);
		wechatMsgQueueHistoryService.saveAllWechatMsgQueueHistory(historys);
	}
	

	@Override
	public void sendWechatMsg() {
		sendWechatMsg(0);
	}
	
	
	/**
	 * TODO 发送模板微信消息
	 * @see com.bw.adv.service.wechat.service.WechatMsgQueueService#sendWechatMsg()
	 */
	private void sendWechatMsg(int start) {
		List<WechatMsgQueueExp> list = null;
		List<WechatMsgQueueExp> failList = null;
		String accessToken = null;
		String currentTime = null;
		String errcode = null;
		try {
			
			list = wechatMsgQueueRepository.findExpByLimit(start, MULTI_LIMIT);

			if (null != list && list.size() > 0) {
				failList = new ArrayList<WechatMsgQueueExp>();
				accessToken = accessTokenCacheService.getAccessTokenCache();
				currentTime = DateUtils.getCurrentTimeOfDb();
				for (WechatMsgQueueExp instance : list) {
					JSONObject result = null;
					instance.setSendTime(currentTime);
					//优惠券过期
					if (instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.COUPON_EXPIRE.getId())) {
						result = sendMsgForCouponExpire(instance, accessToken);
					}//优惠券领取
					else if(instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GET_COUPON_EXCHANGE.getId()) || instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GET_COUPON_RECEIVE.getId()) ||
							instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GET_COUPON_ACTIVITY.getId()) || instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GET_COUPON_HANDLE.getId())){
						result = sendMsgForCouponReceive(instance,accessToken);
					}
					
					if(null != result){
						errcode = result.getString("errcode");
						//如果发送失败，更新这条记录
						if(!errcode.equals(StatusConstant.WECHAT_ERRCODE_SUC.getId().toString())){
//							logger.error("微信发送消息失败："+result);
							instance.setResultMsg(result.toString());
							instance.setIsSend("E");
							failList.add(instance);
						}else{
							instance.setIsSend("Y");
						}
					}
					
				}
				if (failList.size() > 0) {
					for (WechatMsgQueueExp failInstance : failList) {
						list.remove(failInstance);
					}
					updateBatchWechatMsgQueue(failList);
				}
				if (list.size() > 0) {
					//测试模式不移历史表
					if(StringUtils.isNotBlank(TEST_MODE) && TEST_MODE.equals("Y")){
						wechatMsgQueueRepository.updateBatchWechatMsgQueue(list);
					}else{
						deleteWechatMsgQueue(list);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			for (WechatMsgQueueExp wechatMsgQueueExp : list) {
				wechatMsgQueueExp.setIsSend("E");
				wechatMsgQueueExp.setResultMsg(e.toString());
			}
			wechatMsgQueueRepository.updateBatchWechatMsgQueue(list);
		}
	}

	
	/**
	 * sendWechatMsgMultiThread:(多线程发送微信消息). <br/>
	 * Date: 2016-5-6 上午11:25:18 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	public void sendWechatMsgMultiThread(){
		int allCount = 0;
		int threadSize = 0;
		try {
			allCount = wechatMsgQueueRepository.findAllCount();
			
			threadSize = allCount/MULTI_LIMIT + (allCount % MULTI_LIMIT > 0 ? 1 : 0); 
			
			if(threadSize > WECHAT_SEND_MSG_MAX_THREAD){
				threadSize = WECHAT_SEND_MSG_MAX_THREAD;
			}
			
			if(threadSize > 0){
				// ======================= 初始化线程 =============================
				logger.info("=======thread size:"+threadSize+"=========runing==========");
				for(int i = 0; i < threadSize; i++){
					threadList.add(new SendWechatMsgThread());
				}
				executorService = Executors.newFixedThreadPool(threadSize);
				for (int i = 0; i < threadSize; i++) {
					BaseThread m = BaseThread.getCurrentThread(threadList, i*MULTI_LIMIT);
					executorService.execute(m);
				}
				logger.info("=======thread size:"+threadSize+"=========end==========");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(executorService != null){
				executorService.shutdown();
			}
		}
	}
	
	/**
	 * sendMsgForCouponExpire:(调用微信，发送模板消息). <br/>
	 * Date: 2016-5-5 下午3:47:18 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param instance
	 * @param accessToken
	 * @return
	 */
	private JSONObject sendMsgForCouponExpire(WechatMsgQueue instance,String accessToken) {
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForCouponExpire(queueComExp, accessToken);
	}
	
	
	/**
	 * sendMsgForCouponReceive:(优惠券领取发送消息). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param instance
	 */
	private JSONObject sendMsgForCouponReceive(WechatMsgQueueExp instance,String accessToken){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForCouponReceive(queueComExp, accessToken);
	}
	
	
	/**
	 * ClassName: SendWechatMsgThread：发送消息线程类 <br/>
	 * Function: TODO ADD FUNCTION. <br/>
	 * date: 2016-5-9 下午4:34:56 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	class SendWechatMsgThread extends BaseThread{
		private int start = 0;
		@Override
		public void setArgs(Object... args) {
			this.start = (Integer) args[0];
		}
		
		@Override
		public void runCode() throws Exception {
			logger.info(Thread.currentThread().getName()+"=======rows:"+start+"=========run==========");
			sendWechatMsg(start);
			logger.info(Thread.currentThread().getName()+"=======rows:"+start+"=========end==========");
		}
		
	}
	
	
	
	@Autowired
	public void setWechatMsgQueueHistoryService(WechatMsgQueueHistoryService wechatMsgQueueHistoryService) {
		this.wechatMsgQueueHistoryService = wechatMsgQueueHistoryService;
	}

	@Autowired
	public void setAccessTokenCacheService(AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}
	
	
}
