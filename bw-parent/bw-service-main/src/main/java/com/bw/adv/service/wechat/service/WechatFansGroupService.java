/**

 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansGroupService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月11日下午6:09:12
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;
import java.util.Map;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.wechat.model.WechatFansGroup;

/**
 * ClassName:WechatFansGroupService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午6:09:12 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansGroupService extends BaseService<WechatFansGroup> {
	
	/**
	 * queryAllGroups:(查询有效分组). <br/>
	 * Date: 2015年8月14日 下午9:22:05 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	public List<WechatFansGroup> queryAllGroups();
	
	/**
	 * queryWechatFansGroup:(根据分组Id查询分组详情). <br/>
	 * Date: 2015年8月20日 下午4:39:11 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param groupId
	 * @return
	 */
	public WechatFansGroup queryWechatFansGroup(String groupId);
	
	/**
	 * 新增分组
	 * saveWechatFansGroup:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年8月31日 下午8:58:13 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param groupNameString
	 * @param groupIdString
	 * @param sysUser
	 */
	public void saveWechatFansGroup(String groupNameString,String groupIdString, SysUser sysUser);
	
	/**
	 * batchUpdateFansToGroup:(批量移动用户分组). <br/>
	 * Date: 2015年9月2日 下午3:34:48 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param openIds
	 * @param wechatFansGroup
	 * @param string 
	 * @return
	 */
	public void batchUpdateFansToGroup(String openIds, String wechatFans, WechatFansGroup wechatFansGroup);
	
	/**
	 * removeWechatDefaultGroup:(过滤掉微信默认组). <br/>
	 * Date: 2015年12月27日 下午12:09:56 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFansGroupList
	 * @return
	 */
	public List<WechatFansGroup> removeWechatDefaultGroup(List<WechatFansGroup> wechatFansGroupList);
	
	/**
	 * updateWechatFansGroup:(更新本地分组名称). <br/>
	 * Date: 2015年12月27日 下午2:13:47 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatGroupId
	 * @param groupName
	 * @param sysUser
	 */
	public void updateWechatFansGroup(String wechatGroupId, String groupName,SysUser sysUser);
	
	/**
	 * deleteWechatFansGroup:(删除分组). <br/>
	 * Date: 2015年12月27日 下午2:30:22 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatGroupId
	 * @param sysUser
	 */
	public void deleteWechatFansGroup(String wechatGroupId, SysUser sysUser);
	
	
}

