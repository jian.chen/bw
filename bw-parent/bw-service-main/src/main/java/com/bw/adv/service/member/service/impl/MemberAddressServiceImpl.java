/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberAddressServiceImpl.java
 * Package Name:com.sage.scrm.service.member.service.impl
 * Date:2015年12月14日下午2:51:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.repository.MemberAddressRepository;
import com.bw.adv.service.member.service.MemberAddressService;

/**
 * ClassName:MemberAddressServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月14日 下午2:51:30 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class MemberAddressServiceImpl extends BaseServiceImpl<MemberAddress> implements MemberAddressService {
	
	private MemberAddressRepository memberAddressRepository;
	

	@Override
	public BaseRepository<MemberAddress, ? extends BaseMapper<MemberAddress>> getBaseRepository() {
		return memberAddressRepository;
	}
	

	@Override
	public MemberAddress queryMemberAddressByMemberId(Long memberId) {
		return memberAddressRepository.findMemberAddressByMemberId(memberId);
	}
		
	
	@Autowired
	public void setMemberAddressRepository(
			MemberAddressRepository memberAddressRepository) {
		this.memberAddressRepository = memberAddressRepository;
	}
	


	
}

