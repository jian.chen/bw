/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberChannelSummaryJobBean.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2015年11月30日下午4:12:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.report.service.MemberChannelSummaryDayService;

/**
 * ClassName:MemberChannelSummaryJobBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月30日 下午4:12:55 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class MemberChannelSummaryJobBean extends BaseJob{
	
	private static final Logger logger = LoggerFactory.getLogger(MemberChannelSummaryJobBean.class);
	
	private MemberChannelSummaryDayService memberChannelSummaryDayService;


	@Override
	protected void excute() throws Exception {
		
		init();
		logger.info("会员渠道报表任务开始");
		String dateStr = DateUtils.addDays(-1);//昨天天日期
		memberChannelSummaryDayService.callMemberChannelSummary(dateStr);
		logger.info(dateStr+"|会员渠道报表任务结束");
		
	}
	
	public void init(){
		this.memberChannelSummaryDayService = this.getApplicationContext().getBean(MemberChannelSummaryDayService.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.MEMBER_CHANNEL_SUMMARY_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		
		return true;
	}
	
	@Autowired
	public void setMemberChannelSummaryDayService(
			MemberChannelSummaryDayService memberChannelSummaryDayService) {
		this.memberChannelSummaryDayService = memberChannelSummaryDayService;
	}


}

