/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SmsChannelService.java
 * Package Name:com.sage.scrm.service.sys.service
 * Date:2015年8月17日下午2:33:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.sys.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.model.SmsContent;

/**
 * ClassName:SmsChannelService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午2:33:09 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SmsChannelService extends BaseService<SmsChannel> {
	
	/**
	 * 查询所有的短信渠道
	 * @return
	 */
	public List<SmsChannel> queryAllSmsChannel();
	
	/**
     * 更新短信渠道账号信息
     * @param smsChannel
     * @return
     */
    public boolean updateSmsChannelAccount(SmsChannel smsChannel);

    /**
     * 测试短信渠道
     * @param ssmChannelId
     * @return
     */
    public boolean testSmsChannel(Long ssmChannelId, SmsContent smsContent);
	
	/**
	 * 
	 * querySmsChannel:查询启用的短信通道
	 * Date: 2015年8月17日 下午3:15:43 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@Deprecated
	public List<SmsChannel> querySmsChannel(Page<SmsChannel> page); 

	/**
	 * 
	 * deleteSmsChannel:删除短信通道（修改状态为不启用）
	 * Date: 2015年8月17日 下午3:19:04 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 */
	public void deleteSmsChannel(Long smsChannelId);
	
	/**
	 * 
	 * deleteSmsChannelList:批量删除
	 * Date: 2015年8月24日 下午6:34:13 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsChannelIds
	 */
	public void deleteSmsChannelList(List<Long> smsChannelIds);
	
	/**
	 * 
	 * querySmsNames:查询所有通道名称
	 * Date: 2015年9月21日 下午5:32:04 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<SmsChannel> querySmsNames();
	

}

