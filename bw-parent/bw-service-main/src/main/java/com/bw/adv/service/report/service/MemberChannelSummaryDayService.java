/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberChannelSummaryDayService.java
 * Package Name:com.sage.scrm.service.report.service
 * Date:2015年11月26日下午5:08:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.report.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.model.MemberChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.MemberChannelSummaryDayExp;

/**
 * ClassName:MemberChannelSummaryDayService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 下午5:08:55 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberChannelSummaryDayService extends BaseService<MemberChannelSummaryDay> {
	
	/**
	 * 
	 * queryByExample:(根据查询条件查询会员渠道报表信息). <br/>
	 * Date: 2015年11月26日 下午5:25:20 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<MemberChannelSummaryDayExp> queryExpByExample(Example example,Page<MemberChannelSummaryDayExp> page);
	
	/**
	 * 
	 * callMemberChannelSummary:(调用会员渠道存储过程). <br/>
	 * Date: 2015年11月30日 下午3:05:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	public void callMemberChannelSummary(String dateStr);

}

