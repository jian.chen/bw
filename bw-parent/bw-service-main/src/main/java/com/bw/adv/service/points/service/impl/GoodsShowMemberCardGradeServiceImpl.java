/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:GoodsShowMemberCardGradeServiceImpl.java
 * Package Name:com.sage.scrm.service.points.service.impl
 * Date:2016年7月18日下午7:19:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.module.goods.mapper.GoodsShowMemberCardGradeMapper;
import com.bw.adv.module.goods.model.GoodsShowMemberCardGrade;
import com.bw.adv.service.points.service.GoodsShowMemberCardGradeService;

/**
 * ClassName:GoodsShowMemberCardGradeServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年7月18日 下午7:19:54 <br/>
 * scrmVersion standard
 * @author   dong.d
 * @version  jdk1.7
 * @see 	 
 */
@Service("GoodsShowMemberCardGradeServiceImpl")
public class GoodsShowMemberCardGradeServiceImpl implements GoodsShowMemberCardGradeService{

	
	@Autowired
	private GoodsShowMemberCardGradeMapper goodsShowMemberCardGradeMapper;
	
	
	
	@Override
	public int saveGoodsShowMemberCardGrade(List<GoodsShowMemberCardGrade> goodsShowMemberCardGradeList) {
		
		if(goodsShowMemberCardGradeList == null || goodsShowMemberCardGradeList.size() == 0){
			throw new RuntimeException("goodsShowMemberCardGradeList is null");
		}
		return goodsShowMemberCardGradeMapper.insertGoodsShowMemverCardGrade(goodsShowMemberCardGradeList);
		
	}



	@Override
	public int updateGoodsShowMemberCardGrade(GoodsShowMemberCardGrade goodsShowMemberCardGrade) {
		
		if(goodsShowMemberCardGrade == null){
			throw new RuntimeException("goodsShowMemberCardGradeList is null");
		}
		return goodsShowMemberCardGradeMapper.updateGoodsShowMemberCardGrade(goodsShowMemberCardGrade);
		
	}



	@Override
	public List<GoodsShowMemberCardGrade> queryGoodsShowMemberCardGradeListByGoodsShowId(
			Long goodsShowId) {
		
		if(goodsShowId == null ){
			throw new RuntimeException("goodsShowId is null");
		}
		List<GoodsShowMemberCardGrade> goodsShowMemberCardGradeList = goodsShowMemberCardGradeMapper.selectGoodsShowMemberCardGradeListByGoodsShowId(goodsShowId);
		return goodsShowMemberCardGradeList;
	}

}

