/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatGroupMessageServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月18日下午4:59:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.wechat.api.ApiWeChat;
import com.bw.adv.module.wechat.api.ApiWechatMaterialUpload;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansGroup;
import com.bw.adv.module.wechat.model.WechatFansLabel;
import com.bw.adv.module.wechat.model.WechatGroupMessage;
import com.bw.adv.module.wechat.model.WechatMessageGroupItem;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.model.WeixinMedia;
import com.bw.adv.module.wechat.repository.WeChatFansRepository;
import com.bw.adv.module.wechat.repository.WechatFansGroupRepository;
import com.bw.adv.module.wechat.repository.WechatFansLabelRepository;
import com.bw.adv.module.wechat.repository.WechatGroupMessageRepository;
import com.bw.adv.module.wechat.repository.WechatMessageGroupItemRepository;
import com.bw.adv.module.wechat.repository.WechatMessageTemplateRepository;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.module.wechat.util.HttpKit;
import com.bw.adv.service.wechat.service.WechatGroupMessageService;

/**
 * ClassName:WechatGroupMessageServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:59:11 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatGroupMessageServiceImpl extends BaseServiceImpl<WechatGroupMessage> implements
		WechatGroupMessageService {
	
	private WechatGroupMessageRepository wechatGroupMessageRepository;
	
	private WechatFansGroupRepository wechatFansGroupRepository;
	
	private WechatFansLabelRepository wechatFansLabelRepository;
	
	private WeChatFansRepository weChatFansRepository;
	
	private AccessTokenCacheService accessTokenCacheService;
	
	private WechatMessageTemplateRepository  wechatMessageTemplateRepository;
	
	private WechatMessageGroupItemRepository wechatMessageGroupItemRepository;
	
	private ApiWechatMaterialUpload wechatMaterialUploadAPI;
	
	@Autowired
	public void setWechatMaterialUploadAPI(
			ApiWechatMaterialUpload wechatMaterialUploadAPI) {
		this.wechatMaterialUploadAPI = wechatMaterialUploadAPI;
	}

	@Override
	public BaseRepository<WechatGroupMessage, ? extends BaseMapper<WechatGroupMessage>> getBaseRepository() {
		return wechatGroupMessageRepository;
	}
	
	@Autowired
	public void setWechatGroupMessageRepository(
			WechatGroupMessageRepository wechatGroupMessageRepository) {
		this.wechatGroupMessageRepository = wechatGroupMessageRepository;
	}
	@Autowired
	public void setWechatFansGroupRepository(
			WechatFansGroupRepository wechatFansGroupRepository) {
		this.wechatFansGroupRepository = wechatFansGroupRepository;
	}
	@Autowired
	public void setWechatFansLabelRepository(
			WechatFansLabelRepository wechatFansLabelRepository) {
		this.wechatFansLabelRepository = wechatFansLabelRepository;
	}
	@Autowired
	public void setWeChatFansRepository(WeChatFansRepository weChatFansRepository) {
		this.weChatFansRepository = weChatFansRepository;
	}
	@Autowired
	public void setAccessTokenCacheService(
			AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}
	@Autowired
	public void setWechatMessageTemplateRepository(
			WechatMessageTemplateRepository wechatMessageTemplateRepository) {
		this.wechatMessageTemplateRepository = wechatMessageTemplateRepository;
	}
	@Autowired
	public void setWechatMessageGroupItemRepository(
			WechatMessageGroupItemRepository wechatMessageGroupItemRepository) {
		this.wechatMessageGroupItemRepository = wechatMessageGroupItemRepository;
	}

	@Override
	public List<?> queryGroupList(String type) {
		List<?> list=null;
		try {
			if(type.equals("1")){//按分组
				list=new ArrayList<WechatFansGroup>();
				list=this.wechatFansGroupRepository.findAllGroups();
			}
			if(type.equals("2")){//按标签
				list=new ArrayList<WechatFansLabel>();
				list=this.wechatFansLabelRepository.findWechatFansLabels();
			}			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		
		return list;
	}





	/**
	 * TODO 微信发送消息，并保存记录（可选）.
	 * @see com.bw.adv.service.wechat.service.WechatGroupMessageService#sendWechatGroupMessage(java.util.Map)
	 */
	@Override
	public void sendWechatGroupMessage(Map<String,Object> paramsMap) {
		WechatFansGroup wechatFansGroup=null;
		List<WechatFans> wechatFansList=null;
		JSONObject fstJsonObject =null;
		JSONObject contentJsonObject =null;
		JSONArray jsonArray=null;
		JSONObject jsonObject =null;//微信接口调用的json参数
		String url=null;
		String  result=null;
		JSONObject resultJson =null;
		WechatMessageTemplate wechatMessageTemplate=null;
		WechatGroupMessage wechatGroupMessage=null;
		WeixinMedia weixinMedia=null;
		String weixinMediaId=null;
		
		try {
			wechatFansList=new ArrayList<WechatFans>();
			wechatMessageTemplate =new WechatMessageTemplate();
			wechatGroupMessage=new WechatGroupMessage();
			resultJson=new JSONObject();
			wechatFansGroup=new WechatFansGroup();
			jsonObject=new JSONObject();
			fstJsonObject=new JSONObject();
			contentJsonObject=new JSONObject();
			jsonArray=new JSONArray();
			weixinMedia=new WeixinMedia();
			
//			System.out.println("pictureUrl:"+paramsMap.get("pictureUrl"));
			/*
			WeixinMedia weixinMedia = wechatMaterialUploadAPI.uploadMedia("image", "http://localhost:8080/scrm-web-main/upload/aa.jpg");
			System.out.println("上传图片的media_id:" + weixinMedia.getMediaId());*/
			

			
			//按分组
			if(paramsMap.get("groupTypeValue").equals("1")){
				wechatFansGroup=wechatFansGroupRepository.findByPk(Long.parseLong((String) paramsMap.get("groupSelectValue")));//选中的分组明细


				//发送类型-文本
				if(paramsMap.get("wechatMessageType").equals("text")){
					
					fstJsonObject.put("is_to_all", false);
					fstJsonObject.put("group_id", wechatFansGroup.getGroupId());
					contentJsonObject.put("content", paramsMap.get("content"));
					
					jsonObject.put("filter", fstJsonObject);
					jsonObject.put("text", contentJsonObject);
					jsonObject.put("msgtype",paramsMap.get("wechatMessageType"));
					
					System.out.println("按分组-文本："+jsonObject.toString());
					
					url =ApiWeChat.SEND_MESSAGE_BY_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
					result=HttpKit.post(url, jsonObject.toString());
				}
				//发送类型-图片
				if(paramsMap.get("wechatMessageType").equals("image")){
//					weixinMedia = wechatMaterialUploadAPI.uploadMedia("image", (String) paramsMap.get("strBackUrl"));
//					System.out.println("上传图片的media_id:" + weixinMedia.getMediaId());
					weixinMediaId="ksIR6IErzNggp283dMJl_roUMnzEERztrBHk8KUnnh94sOFol7gL3v1DW3lYozjF";
					fstJsonObject.put("is_to_all", false);
					fstJsonObject.put("group_id", wechatFansGroup.getGroupId());
					contentJsonObject.put("media_id", weixinMediaId);
					
					jsonObject.put("filter", fstJsonObject);
					jsonObject.put("image", contentJsonObject);
					jsonObject.put("msgtype",paramsMap.get("wechatMessageType"));
					
					System.out.println("按分组-图片："+jsonObject.toString());
					
					url =ApiWeChat.SEND_MESSAGE_BY_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
					result=HttpKit.post(url, jsonObject.toString());
					
				}
				
				System.out.println("result:"+result);
				
				wechatGroupMessage.setWechatFansGroupId(wechatFansGroup.getWechatFansGroupId());//群发消息记录添加粉丝组ID
			}
			//按标签
			if(paramsMap.get("groupTypeValue").equals("2")){
				wechatFansList=weChatFansRepository.findWechatFansListByWechatFansLabelId(Long.parseLong((String) paramsMap.get("groupSelectValue")));
				if(wechatFansList.size()>1){
					for(WechatFans wechatFans:wechatFansList){
						jsonArray.add(wechatFans.getOpenid());
					}
					jsonObject.put("touser", jsonArray);
					//多条发送通过openid
					url =ApiWeChat.SEND_MESSAGE_BY_OPENIDS.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
				}else{
					for(WechatFans wechatFans:wechatFansList){
						jsonObject.put("touser", wechatFans.getOpenid());
					}	
					//单条发送通过客服接口
					url =ApiWeChat.SEND_CUSTOM_MESSAGE.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
				}


				//发送类型-文本
				if(paramsMap.get("wechatMessageType").equals("text")){
					contentJsonObject.put("content", paramsMap.get("content"));
					
					jsonObject.put("msgtype",paramsMap.get("wechatMessageType"));
					jsonObject.put("text", contentJsonObject);
					
					System.out.println("按标签："+jsonObject.toString());
					
					result=HttpKit.post(url, jsonObject.toString());
				}
				//发送类型-图片
				if(paramsMap.get("wechatMessageType").equals("image")){
//					weixinMedia = wechatMaterialUploadAPI.uploadMedia("image", (String) paramsMap.get("strBackUrl"));
//					System.out.println("上传图片的media_id:" + weixinMedia.getMediaId());
					weixinMediaId="ksIR6IErzNggp283dMJl_roUMnzEERztrBHk8KUnnh94sOFol7gL3v1DW3lYozjF";
					
					contentJsonObject.put("media_id",weixinMediaId);
					
					jsonObject.put("msgtype",paramsMap.get("wechatMessageType"));
					jsonObject.put("image", contentJsonObject);
					
					System.out.println("按标签："+jsonObject.toString());
					
					result=HttpKit.post(url, jsonObject.toString());
					
					
				}				
				
				
				
				
				
				System.out.println("result:"+result);
			}
			if(paramsMap.get("groupTypeValue").equals("3")){//所有粉丝
				wechatFansList=weChatFansRepository.findWechatFans();
				
				//发送类型-文本
				if(paramsMap.get("wechatMessageType").equals("text")){
					fstJsonObject.put("is_to_all", true);
					contentJsonObject.put("content", paramsMap.get("content"));
					
					jsonObject.put("filter", fstJsonObject);
					jsonObject.put("text", contentJsonObject);
					jsonObject.put("msgtype",paramsMap.get("wechatMessageType"));
					
					System.out.println("所有粉丝-文本："+jsonObject.toString());
					
					url =ApiWeChat.SEND_MESSAGE_BY_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
					result=HttpKit.post(url, jsonObject.toString());
				}
				//发送类型-图片
				if(paramsMap.get("wechatMessageType").equals("image")){
//					weixinMedia = wechatMaterialUploadAPI.uploadMedia("image", (String) paramsMap.get("strBackUrl"));
//					System.out.println("上传图片的media_id:" + weixinMedia.getMediaId());
					weixinMediaId="ksIR6IErzNggp283dMJl_roUMnzEERztrBHk8KUnnh94sOFol7gL3v1DW3lYozjF";
					
					fstJsonObject.put("is_to_all", false);
					fstJsonObject.put("group_id", wechatFansGroup.getGroupId());
					contentJsonObject.put("media_id", weixinMediaId);
					
					jsonObject.put("filter", fstJsonObject);
					jsonObject.put("image", contentJsonObject);
					jsonObject.put("msgtype",paramsMap.get("wechatMessageType"));
					
					System.out.println("按分组-图片："+jsonObject.toString());
					
					url =ApiWeChat.SEND_MESSAGE_BY_GROUP.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
					result=HttpKit.post(url, jsonObject.toString());
				}	
				
				System.out.println("result:"+result);				
			}
			
			resultJson = JSONObject.fromObject(result);//微信接口返回的结果
			
			wechatMessageTemplate.setStatusId((long)(StatusConstant.WECHAT_ENABLE.getId()));
			wechatMessageTemplate.setContent((String) paramsMap.get("content"));
			wechatMessageTemplate.setWechatMessageType((String) paramsMap.get("wechatMessageType"));
			wechatMessageTemplate.setIsDeleted(WechatTypeConstant.UNDELETED.getCode());
			wechatMessageTemplate.setPictureUrl((String) paramsMap.get("strBackUrl"));
			wechatMessageTemplate.setMediaId(weixinMediaId);
			
			
			
			wechatMessageTemplateRepository.save(wechatMessageTemplate);//保存消息模板
			
			wechatGroupMessage.setWechatMessageTemplateId(wechatMessageTemplate.getWechatMessageTemplateId());
//			wechatGroupMessage.setErrorCode(resultJson.get("errcode")+"");
//			wechatGroupMessage.setErrorMsg((String) resultJson.get("errmsg"));
//			wechatGroupMessage.setMessageId((String) resultJson.get("msg_id"));
			wechatGroupMessage.setStatusId((long)(StatusConstant.WECHAT_ENABLE.getId()));
			wechatGroupMessage.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
			
			wechatGroupMessageRepository.save(wechatGroupMessage);//保存群发消息记录	
			
			if(paramsMap.get("groupTypeValue").equals("1")){//按分组
				wechatFansList=weChatFansRepository.findWechatFansListByWechatFansGroupId(wechatFansGroup.getWechatFansGroupId());
				
				//保存群发消息对象
				saveWechatMessageGroupItemList(wechatFansList,wechatGroupMessage);
				
			}else{//按标签&所有粉丝
				
				//保存群发消息对象
				saveWechatMessageGroupItemList(wechatFansList,wechatGroupMessage);

			}
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
	}
	
	/**
	 * saveWechatMessageGroupItemList:(保存群发消息对象). <br/>
	 * Date: 2015-9-7 上午10:48:28 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param wechatFansList
	 * @param wechatGroupMessage
	 */
	public void saveWechatMessageGroupItemList(List<WechatFans> wechatFansList,WechatGroupMessage wechatGroupMessage){
		List<WechatMessageGroupItem> wechatMessageGroupItemList=null;
		WechatMessageGroupItem wechatMessageGroupItem=null;
		
		try {
			wechatMessageGroupItem=new WechatMessageGroupItem();
			wechatMessageGroupItemList=new ArrayList<WechatMessageGroupItem>();
			for(WechatFans wechatFans:wechatFansList){
				wechatMessageGroupItem.setWechatGroupMessageId(wechatGroupMessage.getWechatGroupMessageId());
				wechatMessageGroupItem.setWechatFansId(wechatFans.getWechatFansId());
				wechatMessageGroupItem.setStatusId((long)(StatusConstant.WECHAT_ENABLE.getId()));
				wechatMessageGroupItem.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
				wechatMessageGroupItemList.add(wechatMessageGroupItem);
			}
			wechatMessageGroupItemRepository.saveList(wechatMessageGroupItemList);//保存群发消息对象
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	
}

