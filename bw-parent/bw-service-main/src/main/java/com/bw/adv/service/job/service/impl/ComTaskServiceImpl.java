/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ComTaskServiceImpl.java
 * Package Name:com.sage.scrm.service.job.service.impl
 * Date:2015年9月5日下午3:07:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.model.exp.ComTaskExp;
import com.bw.adv.module.component.job.repository.ComTaskRepository;
import com.bw.adv.service.job.service.ComTaskService;

/**
 * ClassName:ComTaskServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月5日 下午3:07:51 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class ComTaskServiceImpl extends BaseServiceImpl<ComTask> implements ComTaskService {

	private ComTaskRepository comTaskRepository;
	
	@Override
	public BaseRepository<ComTask, ? extends BaseMapper<ComTask>> getBaseRepository() {
		
		return comTaskRepository;
	}

	
	@Override
	public List<ComTaskExp> queryExpByExample(Example example,Page<ComTaskExp> page) {
		
		return comTaskRepository.findExpByExample(example,page);
	}
	
	@Autowired
	public void setComTaskRepository(ComTaskRepository comTaskRepository) {
		this.comTaskRepository = comTaskRepository;
	}
	

}

