package com.bw.adv.service.exception;

/**
 * Created by jeoy.zhou on 3/10/16.
 */
public class WechatAccountException extends RuntimeException {

    private static final long serialVersionUID = 8596987057669097064L;

    public WechatAccountException(String msg) {
        super(msg);
    }

    public WechatAccountException(Throwable e) {
        super(e);
    }

    public WechatAccountException(String msg, Throwable e) {
        super(msg, e);
    }
}
