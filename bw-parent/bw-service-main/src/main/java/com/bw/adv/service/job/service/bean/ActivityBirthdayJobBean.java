package com.bw.adv.service.job.service.bean;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.bw.adv.module.activity.constant.ActivityConstant;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.repository.ComTaskRepository;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.ActivityActionService;

/**
 * ClassName: 生日活动任务<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-13 上午11:01:25 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class ActivityBirthdayJobBean extends BaseJob {
	
	private ActivityActionService activityActionService; 
	
	private ComTaskRepository comTaskRepository;
	
	@Override
	public void excute() throws Exception {
		List<ActivityInstance> list = null;
		String currentDate = null;
		String serviceUrl = null;
		List<ComTask> comtask = new ArrayList<ComTask>();
		
		try {
			initService();
			
			serviceUrl = ActivityBirthdayJobBean.class.getName();
			comtask = comTaskRepository.findJobTaskByTypeId(serviceUrl);
			if(comtask.get(0).getLastRuntime().startsWith(DateUtils.getCurrentDateOfDb())){
				return;
			}
			
		    //活动对应活动实例
			currentDate = DateUtils.getCurrentTimeOfDb();
			list = RuleInit.getActivityInstanceList(ActivityConstant.ACTIVIY_MEMBER_BIRTHDAY.getCode());
			for (ActivityInstance instance : list) {
				//如果没有过期
				if(StringUtils.isNotBlank(instance.getThruDate())){
					if(instance.getThruDate().compareTo(currentDate) < 0){
						continue;
					}
				}
				activityActionService.addActivityActionMemberBirthday(instance);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("生日活动任务任务执行完成");
	}
	
	private void initService() {
		this.activityActionService = this.getApplicationContext().getBean(ActivityActionService.class);
		this.comTaskRepository = this.getApplicationContext().getBean(ComTaskRepository.class);
	}
	
	@Override
	protected String getLockCode() {
		return LockCodeConstant.MEMBER_BIRHTDAY_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

}
