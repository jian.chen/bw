package com.bw.adv.service.points.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.points.model.PointsSummaryDay;
import com.bw.adv.module.report.points.model.exp.PointsSummaryDayExp;

public interface PointsSummaryDayService extends BaseService<PointsSummaryDay> {
	/**
	 * queryByExp:根据条件查询积分报表数据 <br/>
	 * Date: 2015年12月31日 下午2:45:45 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	List<PointsSummaryDayExp> queryByExp(Example example);
	
	PointsSummaryDayExp queryPointsByExp(Example example);

	/**
	 * 
	 * callPointsSummaryDay:(调用积分日统计存储过程). <br/>
	 * Date: 2016年1月14日 下午4:27:16 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	void callPointsSummaryDay(String dateStr);
}
