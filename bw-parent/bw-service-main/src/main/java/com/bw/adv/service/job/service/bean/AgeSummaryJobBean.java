package com.bw.adv.service.job.service.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.report.service.ReportService;

public class AgeSummaryJobBean extends BaseJob{
	
	private static final Logger logger = LoggerFactory.getLogger(AgeSummaryJobBean.class);
	
	private ReportService reportservice;

	@Override
	public synchronized void excute() throws Exception {
		init();
		logger.info("会员年龄统计报表任务开始");
		String dateStr = DateUtils.addDays(-1);//昨天天日期
		reportservice.callAgeSummary(dateStr);
		logger.info(dateStr+"|会员年龄统计报表任务开始");
	}
	
	public void init(){
		this.reportservice = this.getApplicationContext().getBean(ReportService.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.AGE_SUMMARY_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

	@Autowired
	public void setReportservice(ReportService reportservice) {
		this.reportservice = reportservice;
	}
	
}