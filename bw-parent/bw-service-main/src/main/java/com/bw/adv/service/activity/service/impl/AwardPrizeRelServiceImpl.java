/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:AwardPrizeRelServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015年11月23日下午1:54:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.AwardPrizeRel;
import com.bw.adv.module.activity.model.Awards;
import com.bw.adv.module.activity.repository.AwardPrizeRelRepository;
import com.bw.adv.service.activity.service.AwardPrizeRelService;

/**
 * ClassName:AwardPrizeRelServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午1:54:51 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class AwardPrizeRelServiceImpl extends BaseServiceImpl<AwardPrizeRel> implements AwardPrizeRelService {
	
	private AwardPrizeRelRepository awardPrizeRelRepository;
	
	@Override
	public BaseRepository<AwardPrizeRel, ? extends BaseMapper<AwardPrizeRel>> getBaseRepository() {
		return awardPrizeRelRepository;
	}
	
	@Autowired
	public void setAwardPrizeRelRepository(
			AwardPrizeRelRepository awardPrizeRelRepository) {
		this.awardPrizeRelRepository = awardPrizeRelRepository;
	}
	
}

