/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansBehaviorServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月19日下午3:48:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansBehavior;
import com.bw.adv.module.wechat.repository.WechatFansBehaviorRepository;
import com.bw.adv.service.wechat.service.WeChatFansService;
import com.bw.adv.service.wechat.service.WechatFansBehaviorService;

/**
 * ClassName:WechatFansBehaviorServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午3:48:39 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatFansBehaviorServiceImpl extends BaseServiceImpl<WechatFansBehavior> implements WechatFansBehaviorService {
	
	private WechatFansBehaviorRepository wechatFansBehaviorRepository;
	private WeChatFansService wechatFansService;

	@Override
	public BaseRepository<WechatFansBehavior, ? extends BaseMapper<WechatFansBehavior>> getBaseRepository() {
		return wechatFansBehaviorRepository;
	}
	
	@Override
	public JSONObject save(String wechatFanId,String wechatAccountId,JSONObject paramJson) {
		
		WechatFansBehavior wechatFansBehavior = null;
		String subscribe =null;
		String code =null;
		String msg =null;
		String wechatFansBehaId=null;
		JSONObject resultJson =null;
		
		try {
			wechatFansBehavior = new WechatFansBehavior();
			resultJson =new JSONObject();
			subscribe = paramJson.getInt("subscribe")+"";
			wechatFansBehavior.setWechatAccountId(Long.valueOf(wechatAccountId));
			wechatFansBehavior.setWechatFansId(Long.valueOf(wechatFanId));
			// 1:关注 2:取消关注
			wechatFansBehavior.setBehaviorType(subscribe);
			this.save(wechatFansBehavior);
			
			wechatFansBehaId =wechatFansBehavior.getWechatFansBehaviorId()+"";
			code =ResultStatus.SUCCESS.getCode();
			msg ="同步粉丝行为成功!";
			
		} catch (Exception e) {
			code =ResultStatus.SYSTEM_ERROR.getCode();
			msg =e.getMessage();
		}finally{
			resultJson.put("code", code);
			resultJson.put("id", wechatFansBehaId);
			resultJson.put("msg", msg);
		}
		return resultJson;
	}
	

	@Override
	public void saveUnSubscribeFansBehavior(WechatFans wechatFans) {
		WechatFansBehavior wechatFansBehavior = null;
		try {
			wechatFansBehavior =new WechatFansBehavior();
			// 取消关注  --2
			wechatFansBehavior.setBehaviorType(WechatTypeConstant.UNSUBSCRIBE.getCode());
			wechatFansBehavior.setCreateTime(DateUtils.getCurrentTimeOfDb());
			wechatFansBehavior.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
			wechatFansBehavior.setWechatFansId(wechatFans.getWechatFansId());
			wechatFansBehavior.setWechatAccountId(wechatFans.getWechatAccountId());
			this.save(wechatFansBehavior);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Autowired
	public void setWechatFansBehaviorRepository(
			WechatFansBehaviorRepository wechatFansBehaviorRepository) {
		this.wechatFansBehaviorRepository = wechatFansBehaviorRepository;
	}
	
	@Autowired
	public void setWechatFansService(WeChatFansService wechatFansService) {
		this.wechatFansService = wechatFansService;
	}
}

