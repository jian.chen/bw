/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:AwardsService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2015年11月23日下午1:51:03
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.Awards;
import com.bw.adv.module.activity.model.exp.AwardsExp;

/**
 * ClassName:AwardsService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午1:51:03 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface AwardsService extends BaseService<Awards> {
	
	/**
	 * queryAwardsBySceneGameInstanceId:查询活动实例的奖项明细. <br/>
	 * Date: 2016年1月19日 上午11:33:30 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	List<AwardsExp> queryAwardsBySceneGameInstanceId(Long sceneGameInstanceId);
	
	 /**
	  * queryLuckyAwards:查询活动的谢谢参与奖项. <br/>
	  * Date: 2016年1月22日 下午1:37:56 <br/>
	  * scrmVersion 1.0
	  * @author Tab.Wang
	  * @version jdk1.7
	  * @param sceneGameInstanceId
	  * @return
	  */
	 AwardsExp queryLuckyAwards(Long sceneGameInstanceId);
	 
	 /**
	  * queryAwardsByAwardsId:根据奖项ID,查询奖项信息. <br/>
	  * TODO(这里描述这个方法适用条件 – 可选).<br/>
	  * Date: 2016年3月1日 下午5:15:22 <br/>
	  * scrmVersion 1.0
	  * @author Tab.Wang(*^_^*)
	  * @version jdk1.7
	  * @param awardsId
	  * @return
	  */
	 AwardsExp queryAwardsByAwardsId(Long awardsId);

	 /**
	 * queryByGameCode:(根据游戏编号查询奖项列表，只查询有效的游戏:已保存，已激活). <br/>
	 * Date: 2016-3-31 下午6:25:56 <br/>
	 * scrmVersion standard
	 * @author mennan
	 * @version jdk1.7
	 * @param gameCode
	 * @return
	 */
	List<Awards> queryByGameCode(String gameCode);
	
}

