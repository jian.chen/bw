/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PointsRuleSevice.java
 * Package Name:com.sage.scrm.service.points.service.impl
 * Date:2015年8月20日下午5:06:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.points.model.PointsConf;
import com.bw.adv.module.points.repository.PointsConfRepository;
import com.bw.adv.service.points.service.PointsConfSevice;

/**
 * ClassName: PointsConfSevice <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016-3-18 下午4:00:42 <br/>
 * scrmVersion standard
 * @author mennan
 * @version jdk1.7
 */
@Service
public class PointsConfSeviceImpl extends BaseServiceImpl<PointsConf> implements PointsConfSevice {
	
	private PointsConfRepository pointsConfRepository;
	
	@Override
	public BaseRepository<PointsConf, ? extends BaseMapper<PointsConf>> getBaseRepository() {
		return pointsConfRepository;
	}

	@Autowired
	public void setPointsConfRepository(PointsConfRepository pointsConfRepository) {
		this.pointsConfRepository = pointsConfRepository;
	}
	
}

