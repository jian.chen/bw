/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatKeysRuleServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月24日下午5:39:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.WechatKeysRuleExp;
import com.bw.adv.module.wechat.repository.WechatKeysRuleRepository;
import com.bw.adv.service.wechat.service.WechatKeysRuleService;

/**
 * ClassName:WechatKeysRuleServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午5:39:34 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatKeysRuleServiceImpl extends BaseServiceImpl<WechatKeysRule> implements WechatKeysRuleService {
	protected final Logger logger = Logger.getLogger(KeysGroupServiceImpl.class);
	private WechatKeysRuleRepository wechatKeysRuleRepository;
	
	@Override
	public BaseRepository<WechatKeysRule, ? extends BaseMapper<WechatKeysRule>> getBaseRepository() {
		return wechatKeysRuleRepository;
	}
	

	@Override
	public List<WechatKeysRule> queryWechatKeyRules() {
		return wechatKeysRuleRepository.findWechatKeyRules();
	}
	


	@Override
	public WechatKeysRule queryWechatKeyRule(String content) {
		
		WechatKeysRule wechatKeysRule =null;
		List<WechatKeysRule> keysRuleList = null;
		// 匹配类型
		String matchType = null;
		try {
			keysRuleList = this.queryWechatKeyRules();
			// 关键字默认是--- 未全匹配
			for (WechatKeysRule keysRule : keysRuleList) {
				matchType = keysRule.getMatchType();
				// 如果是未全匹配
				if (matchType.equals(WechatTypeConstant.KEYWORD_MODE_CONTAIN.getCode())) {
					if (keysRule.getKeyword().contains(content)) {
						wechatKeysRule =keysRule;
						return wechatKeysRule;
					}
				} else {
					if (keysRule.getKeyword().equals(content)) {
						wechatKeysRule =keysRule;
						return wechatKeysRule;
					}
				}
			}
			return wechatKeysRule;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return wechatKeysRule;
	}
	
	
	
	@Override
	public WechatKeysRuleExp queryWechatKeyRuleExpByKeyRule(WechatKeysRule wechatKeysRule) {
		return wechatKeysRuleRepository.findWechatKeyRuleExpByKeyRule(wechatKeysRule);
	}

	
	@Autowired
	public void setWechatKeysRuleRepository(
			WechatKeysRuleRepository wechatKeysRuleRepository) {
		this.wechatKeysRuleRepository = wechatKeysRuleRepository;
	}

	
}

