package com.bw.adv.service.cfg.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.cfg.model.ChannelInstanceRecord;
import com.bw.adv.module.cfg.model.ChannelInstanceRecordDto;

/**
 * ClassName: ChannelInstanceRecordService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月28日 下午4:30:51 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
public interface ChannelInstanceRecordService extends BaseService<ChannelInstanceRecord> {
	public List<ChannelInstanceRecordDto> findListByPages(Long channelCfgId,String recordMobile,Page<ChannelInstanceRecordDto> page); 
}