package com.bw.adv.service.member.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.GradeConf;
import com.bw.adv.module.member.repository.GradeConfRepository;
import com.bw.adv.module.member.repository.GradeRepository;
import com.bw.adv.service.member.service.GradeService;

/**
 * ClassName: GradeServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月6日 上午10:48:54 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Service
public class GradeServiceImpl extends BaseServiceImpl<Grade> implements GradeService {
	
	private GradeRepository gradeRepository;
	private GradeConfRepository gradeConfRepository;
	private GradeInit gradeInit;
	
	@Override
	public BaseRepository<Grade, ? extends BaseMapper<Grade>> getBaseRepository() {
		return gradeRepository;
	}
	
	@Override
	public List<Grade> queryGradeList() {
		return gradeRepository.findList();
	}
	
	@Override
	public GradeConf queryGradeConfByPk(Long gradeConfId) {
		return gradeConfRepository.findGradeConfByPk(gradeConfId);
	}

	@Override
	public void updateGradeConf(GradeConf gradeConf) {
		this.gradeConfRepository.updateByPkSelective(gradeConf);
		gradeInit.reLoad();
	}

	
	@Override
	@Transactional
	public void updateGrade(Grade[] grades, GradeConf gradeConf){
		for(Grade grade:grades){
			gradeRepository.updateByPkSelective(grade);
		}
		gradeConf.setGradeConfId(1L);
		gradeConfRepository.updateByPkSelective(gradeConf);
		gradeInit.reLoad();
	}
	
	@Autowired
	public void setGradeRepository(GradeRepository gradeRepository) {
		this.gradeRepository = gradeRepository;
	}
	
	@Autowired
	public void setGradeConfRepository(GradeConfRepository gradeConfRepository) {
		this.gradeConfRepository = gradeConfRepository;
	}

	@Autowired
	public void setGradeInit(GradeInit gradeInit) {
		this.gradeInit = gradeInit;
	}

	@Override
	public List<Grade> queryGradeListForJS() {
		return this.gradeRepository.queryGradeListForJS();
	}


}
