package com.bw.adv.service.job.service.bean;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.service.activity.service.ReleaseInviteMemberAwardService;

/**
 * Created by Ronald on 2016/12/12.
 */
public class ReleaseInviteMemberAwardJobBean extends BaseJob{

    private ReleaseInviteMemberAwardService releaseInviteMemberAwardService;
    @Override
    protected void excute() throws Exception {
        initService();
        releaseInviteMemberAwardService.releaseAward();
    }

    @Override
    protected String getLockCode() {
        return LockCodeConstant.INVITE_MEMBER_JOB_CODE;
    }

    @Override
    protected boolean isNeedLock() {
        return true;
    }
    void initService(){
        releaseInviteMemberAwardService = this.getApplicationContext().getBean(ReleaseInviteMemberAwardService.class);
    }
}

