package com.bw.adv.service.report.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.report.model.IndexChartShowInstance;

public interface IndexChartShowInstanceService extends BaseService<IndexChartShowInstance>{
	
	/**
	 * 更新首页仪表盘个人配置
	 * @param instanceList
	 */
	public void updateIndexCharts(List<IndexChartShowInstance> instanceList,Long userId);

	/**
	 * 根据用户id查询仪表盘设置
	 * @param userId
	 * @return
	 */
	public List<IndexChartShowInstance> findUserIndexByUserId(Long userId);
	
}
