/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberService1.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2015年8月10日下午3:39:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.search.MemberGroupSearch;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;


/**
 * ClassName:MemberGroupService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月10日 下午3:39:09 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberGroupService extends BaseService<MemberGroup>{
	/**
	 * 
	 * queryCountSearchAuto:根据条件查询分组数量 <br/>
	 * Date: 2015年8月13日 上午11:06:47 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	Long queryCountSearchAuto(MemberGroupSearch search);
	
	/**
	 * 
	 * queryListSearchAuto:根据条件查询分组列表 <br/>
	 * Date: 2015年8月13日 上午11:07:15 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	List<MemberGroup> queryListSearchAuto(MemberGroupSearch search);
	
	/**
	 * 
	 * queryEntityPkAuto:根据id查询分组详情 <br/>
	 * Date: 2015年8月13日 上午11:08:29 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @return
	 */
	MemberGroup queryEntityPkAuto(Long memberGroupId);
	
	/**
	 * 
	 * insertMemberGroup:新增会员分组 <br/>
	 * Date: 2015年8月13日 上午11:10:00 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroup
	 * @return
	 */
	int insertMemberGroup(MemberGroup memberGroup);
	
	/**
	 * 
	 * updateMemberGroup:修改会员分组 <br/>
	 * Date: 2015年8月13日 上午11:10:46 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroup
	 * @return
	 */
	int updateMemberGroup(MemberGroup memberGroup);
	
	/**
	 * 
	 * deleteteMemberGroupById:根据id删除分组 <br/>
	 * Date: 2015年8月13日 上午11:11:48 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @return
	 */
	int deleteteMemberGroupById(Long memberGroupId);
	
	/**
	 * 
	 * insertBatchMemberGroup:会员分组批量添加会员，之前的会员继续存在，重复性验证 <br/>
	 * Date: 2015年8月13日 上午11:51:57 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 */
	Long insertBatchMemberGroupItem(Long memberGroupId,String memberIds); 
	
	/**
	 * 
	 * excuteMemberGroupBySql:手动执行分组sql,适用存在sql的分组 <br/>
	 * Date: 2015年8月13日 上午11:53:53 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @return 执行会员数量
	 */
	Long excuteMemberGroupBySql(Long memberGroupId);

	/**
	 * 
	 * queryMemberByGroupId:查询分组下会员列表 <br/>
	 * Date: 2015年8月13日 下午4:59:17 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param groupId
	 * @return
	 */
	List<MemberExp> queryMemberListOfGroup(MemberSearch search);
	
	/**
	 * 
	 * queryCountMemberListOfGroup:根据条件查询分组先会员数量 <br/>
	 * Date: 2015年8月17日 上午11:11:48 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	Long queryCountMemberListOfGroup(MemberSearch search);

	/**
	 * 
	 * deleteMemberGroupItem:根据分组id和会员id删除分组下会员 <br/>
	 * Date: 2015年8月31日 下午5:31:30 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @param memberId
	 */
	void deleteMemberGroupItem(Long memberGroupId, Long memberId);

	/**
	 * 
	 * saveAllMemberToGroup:设置会员分组成员为全部会员 <br/>
	 * Date: 2015年9月1日 下午5:26:36 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroup
	 */
	void saveAllMemberToGroup(MemberGroup memberGroup);

	/**
	 * 
	 * excuteMemberGroupJob:会员分组定时任务 <br/>
	 * Date: 2015年9月29日 下午2:50:38 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param group
	 */
	void excuteMemberGroupJob(MemberGroup group);

	
}

