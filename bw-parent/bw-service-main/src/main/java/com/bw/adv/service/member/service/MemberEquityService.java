/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberService1.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2016年1月13日下午20:15:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.equity.search.MemberEquitySearch;
import com.bw.adv.module.member.model.MemberEquity;
import com.bw.adv.module.member.model.exp.MemberEquityExp;

/**
 * ClassName:MemberEquityService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月13日 下午20:19:09 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberEquityService extends BaseService<MemberEquity>{
	
	/**
	 * queryCountSearchAuto:查询会员权益的总数. <br/>
	 * Date: 2016年1月14日 上午11:33:41 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	Long queryCountSearchAuto(MemberEquitySearch search); 
	
	/**
	 * 
	 * queryListSearchAuto:根据条件查询权益列表 <br/>
	 * Date: 2016年1月13日 下午20:19:09 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	List<MemberEquityExp> queryListSearchAuto(MemberEquitySearch search);
	
	
	/**
	 * queryInfoById:查看会员权益详情. <br/>
	 * Date: 2016年1月14日 下午5:40:57 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberEquityid
	 * @return
	 */
	MemberEquity queryInfoById(Long memberEquityid);
	
	
	/**
	 * updateMemberEquity:更新会员权益信息. <br/>
	 * Date: 2016年1月15日 上午10:22:53 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @return
	 */
	int updateMemberEquity(MemberEquity memberEquity,String gradeIds);
	
	/**
	 * insertMemberEquity:新增会员权益信息. <br/>
	 * Date: 2016年1月15日 下午2:54:10 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberEquity
	 * @param gradeIds
	 * @return
	 */
	int saveMemberEquity(MemberEquity memberEquity,String gradeIds); 
	
	/**
	 * removeMemberEquity:逻辑删除会员权益信息. <br/>
	 * Date: 2016年1月15日 下午4:26:04 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberEquityId
	 * @return
	 */
	int removeMemberEquity(Long memberEquityId);
	
	/**
	 * 根据等级id查询会员权益
	 * selectListByGradeId:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param gradeId
	 * @return
	 */
	List<MemberEquity> selectListByGradeId(Long gradeId,String nowDate);
	
}
