package com.bw.adv.service.member.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.message.model.MemberMessage;
import com.bw.adv.module.member.message.model.MemberMessageItem;
import com.bw.adv.module.member.message.model.NotReadMessage;
import com.bw.adv.module.member.message.repository.MemberMessageItemRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.member.service.MemberMessageItemService;
import com.bw.adv.service.member.service.MemberMessageService;

@Service
public class MemberMessageItemServiceImpl extends BaseServiceImpl<MemberMessageItem> implements MemberMessageItemService {
	
	@Value("${crm_coupon_issue_num}")
	private static String messageNumber;
	private MemberMessageItemRepository memberMessageItemRepository;
	
//	private MemberService memberService;

	private MemberMessageService memberMessageService;

	@Override
	public void insertObj(Long memberId, Long memberMessageId) {
		String currentTime = null;
		MemberMessageItem messageItem = null;
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		messageItem = new MemberMessageItem();
		messageItem.setCreateTime(currentTime);
		messageItem.setIsRead("Y");
		messageItem.setMemberId(memberId);
		messageItem.setMemberMessageId(memberMessageId);
		messageItem.setReadTime(currentTime);
		memberMessageItemRepository.save(messageItem);
		
	}
	
	@Override
	public void insertBatchPer(Long messageid,String ids, String type) {
//		MemberSearch search = null;
//		List<MemberExp> memberList = null;
		Long[] memberIdArr = null;
		String[] memeberids = null;
		Long[] perArr = null;
		Integer times = null;
		MemberMessage message = null;
		String currentTime = null;
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		times = Integer.parseInt(messageNumber);//每批次发放数量
		if(type.equals("1")){//所有会员
//			search = new MemberSearch();
//			memberList = memberService.queryListSearchAuto(search);
//			memberIdArr = new Long[memberList.size()];
//			for(int i=0;i<memberList.size();i++){
//				memberIdArr[i]=memberList.get(i).getMemberId();
//			}
			message = memberMessageService.queryByPk(messageid);
			message.setIsAll("Y");
			message.setUpdateBy(1L);
			message.setUpdateTime(currentTime);
			message.setStatusId(StatusConstant.MEMBERMESSAGE_SENDED.getId());
			message.setSendTime(currentTime);
			memberMessageService.updateByPkSelective(message);
		}else if(type.equals("2")){//部分会员
			memeberids = ids.split(",");
			memberIdArr = strToLongArr(memeberids);
			int IssueNum = memberIdArr.length;//会员数量
			int onlyone = (int)(IssueNum/times);//批次数量
			for(int i = 0;i<onlyone;i++){
				perArr = new Long[times];
				perArr = Arrays.copyOfRange(memberIdArr, i*times, (i+1)*times);
				insertFixedNum(messageid,perArr);
			}
			if(IssueNum%times>0){
				Integer num = IssueNum%times;
				Integer totalNum = IssueNum;
				perArr = new Long[num];
				perArr = Arrays.copyOfRange(memberIdArr, onlyone*times,totalNum);
				insertFixedNum(messageid,perArr);
			}
		}
	}
	
	@Transactional
	public void insertFixedNum(Long messageId,Long[] memberIds){
		List<MemberMessageItem> list = null;
		MemberMessageItem obj = null;
		String currentTime = null;
		MemberMessage message = null;
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		message = memberMessageService.queryByPk(messageId);
		message.setSendTime(currentTime);
		message.setStatusId(StatusConstant.MEMBERMESSAGE_SENDED.getId());
		message.setUpdateBy(1L);
		message.setUpdateTime(currentTime);
		list = new ArrayList<MemberMessageItem>();
		for(int i=0;i<memberIds.length;i++){
			obj = new MemberMessageItem();
			obj.setCreateTime(currentTime);
			obj.setIsRead("N");
			obj.setMemberId(memberIds[i]);
			obj.setMemberMessageId(messageId);
			list.add(obj);
		}
		memberMessageItemRepository.saveList(list);
		memberMessageService.updateByPkSelective(message);
	}
	
	@Override
	@Transactional
	public void deleteByMessageId(Long messageId) {
		MemberMessage message = null;
		message = memberMessageService.queryByPk(messageId);
//		if(!"Y".equals(message.getIsAll())){
		memberMessageItemRepository.deleteByMessageId(messageId);
			
//		}
		message.setIsAll("");
		message.setStatusId(StatusConstant.MEMBERMESSAGE_RECALL.getId());
		message.setUpdateBy(1L);
		message.setUpdateTime(DateUtils.getCurrentTimeOfDb());
		message.setSendTime(null);
		memberMessageService.updateByPk(message);
	}
	/**
	 * strToLongArr:字符串数组转长整形数组 <br/>
	 * Date: 2016年2月26日 下午2:13:32 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param str
	 * @return
	 */
	private Long[] strToLongArr(String[] str){
		Long[] longArr = new Long[str.length];
		for(int i=0;i<str.length;i++){
			longArr[i] = Long.parseLong(str[i]);
		}
		return longArr;
	}
	
	@Override
	public NotReadMessage queryNotReadMessageByMemberId(Long memberId) {
		
		return memberMessageItemRepository.findByMemberId(memberId);
	}

	@Override
	public BaseRepository<MemberMessageItem, ? extends BaseMapper<MemberMessageItem>> getBaseRepository() {
		
		return memberMessageItemRepository;
	}

	@Autowired
	public void setMemberMessageItemRepository(
			MemberMessageItemRepository memberMessageItemRepository) {
		this.memberMessageItemRepository = memberMessageItemRepository;
	}
	
	@Override
	public MemberMessageItem queryByMessageId(Example example) {
		
		return memberMessageItemRepository.findByMessageId(example);
	}
	
//	@Autowired
//	public void setMemberService(MemberService memberService) {
//		this.memberService = memberService;
//	}
	
	@Autowired
	public void setMemberMessageService(MemberMessageService memberMessageService) {
		this.memberMessageService = memberMessageService;
	}


}
