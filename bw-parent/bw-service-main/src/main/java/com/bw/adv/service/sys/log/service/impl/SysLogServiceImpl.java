package com.bw.adv.service.sys.log.service.impl;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sys.constant.LogEventConstant;
import com.bw.adv.module.sys.constant.LogTypeConstant;
import com.bw.adv.module.sys.enums.LogNoteEnum;
import com.bw.adv.module.sys.model.SysLog;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.exp.SysLogExp;
import com.bw.adv.module.sys.repository.SysLogRepository;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.sys.log.service.SysLogService;

@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLog> implements SysLogService{

	private SysLogRepository sysLogRepository;
	
	@Override
	public BaseRepository<SysLog, ? extends BaseMapper<SysLog>> getBaseRepository() {
		return sysLogRepository;
	}

	/**
	 * TODO 记录日志.
	 */
	public void log(LogTypeConstant logTypeConstant, LogEventConstant logEventConstant, LogNoteEnum logNoteEnum, String keyword, String remark, Object source, Object update){
		String logIp = null;
		String logDate = null;
		String logTime = null;
		SysLog sysLog = null;
		
		logDate = DateUtils.getCurrentDateOfDb();
		logTime = DateUtils.formatCurrentDate(DateUtils.DATE_PATTERN_HHmmss);
		sysLog = new SysLog();
		
		System.out.println(logTypeConstant+"******");
		
		sysLog.setLogTypeId(logTypeConstant.getId());
		sysLog.setLogEventId(logEventConstant.getId());
		sysLog.setLogDate(logDate);
		sysLog.setLogTime(logTime);
//		sysLog.setLogOrgPartyId(currentOrgParty.getPartyId());
//		sysLog.setLogPartyId(currentUser.getPartyId());
		sysLog.setLogNode(logNoteEnum.getId());
		sysLog.setLogKeyword(keyword);
		sysLog.setRemark(remark);
		sysLog.setLogOptIp(logIp);
		Map<String, String> changeMap = compareChange(source, update);
		sysLog.setLogSource(changeMap.get("source"));
		sysLog.setLogUpdate(changeMap.get("update"));
		sysLogRepository.save(sysLog);
	}
	
	/**
	 * compareChange:比较两个对象的不同属性值 <br/>
	 * Date: 2015-8-18 下午3:46:29 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param o1
	 * @param o2
	 * @return
	 */
	private Map<String, String> compareChange(Object o1, Object o2){
		Map<String, String> changeMap = new HashMap<String, String>();
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		if(o1 != null && o2 != null){
			//必须为同一个对象
			if(o1.getClass().equals(o2.getClass())){
				Map<String, Field> map1 = BeanUtils.getObjectAllField(o1, false);
				Map<String, Field> map2 = BeanUtils.getObjectAllField(o2, false);
				Set<String> keySet = map1.keySet();
				Field f1 = null;
				Field f2 = null;
				Object v1 = null;
				Object v2 = null;
				String desc = null;
				for(String key : keySet){
					try {
						f1 = map1.get(key);
//						Description anno = f1.getAnnotation(Description.class);
//						if(anno == null){
//							continue;
//						}
//						desc = anno.value();
						f1.setAccessible(true);
						v1 = f1.get(o1);
						desc = f1.getName();
						
						f2 = map2.get(key);
						f2.setAccessible(true);
						v2 = f2.get(o2);
						if(v1 != null && v2 != null){
							if(!v1.equals(v2)){
								list1.add(desc + ":" + v1);
								list2.add(desc + ":" + v2);
							}
						}else if(v1 != null){
							list1.add(desc + ":" + v1);
						}else if(v2 != null){
							list2.add(desc + ":" + v2);
						}
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
		}else if(o1 != null){
			Map<String, Field> map1 = BeanUtils.getObjectAllField(o1, false);
			Set<String> keySet = map1.keySet();
			Field f1 = null;
			Object v1 = null;
			String desc = null;
			for(String key : keySet){
				try {
					f1 = map1.get(key);
//					Description anno = f1.getAnnotation(Description.class);
//					if(anno == null){
//						continue;
//					}
//					desc = anno.value();
					f1.setAccessible(true);
					v1 = f1.get(o1);
					desc = f1.getName();
					
					if(v1 != null){
						list1.add(desc + ":" + v1);
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}else if(o2 != null) {
			Map<String, Field> map1 = BeanUtils.getObjectAllField(o2, false);
			Set<String> keySet = map1.keySet();
			Field f1 = null;
			Object v1 = null;
			String desc = null;
			for(String key : keySet){
				try {
					f1 = map1.get(key);
//					Description anno = f1.getAnnotation(Description.class);
//					if(anno == null){
//						continue;
//					}
//					desc = anno.value();
					f1.setAccessible(true);
					v1 = f1.get(o2);
					desc = f1.getName();
					
					if(v1 != null){
						list2.add(desc + ":" + v1);
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		changeMap.put("source", StringUtils.arrayToDelimitedString(list1.toArray(), "<br/>"));
		changeMap.put("update", StringUtils.arrayToDelimitedString(list2.toArray(), "<br/>"));
		return changeMap;
	}

	@Override
	public List<SysLogExp> queryByExample(Example example,Page<SysLogExp> page) {
		
		return sysLogRepository.findByExample(example,page);
	}
	
	@Override
	public List<SysUser> querySysUserList() {
		/*Page<SysUser> page = new Page<SysUser>();
		return sysUserRepository.findByStatusId(StatusConstant.SYS_USER_ACTIVE.getId(), page);*/
		
		return sysLogRepository.findAllUser();
	}
	
	@Autowired
	public void setSysLogRepository(SysLogRepository sysLogRepository) {
		this.sysLogRepository = sysLogRepository;
	}

	


}
