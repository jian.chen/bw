package com.bw.adv.service.report.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.module.report.repository.ReportRepository;
import com.bw.adv.service.report.service.ReportService;

@Service
public class ReportServiceImpl implements ReportService{
	
	private ReportRepository reportRepository;

	@Override
	public void callAgeSummary(String dateStr) {
		reportRepository.callAgeSummary(dateStr);
	}

	@Autowired
	public void setReportRepository(ReportRepository reportRepository) {
		this.reportRepository = reportRepository;
	}

}
