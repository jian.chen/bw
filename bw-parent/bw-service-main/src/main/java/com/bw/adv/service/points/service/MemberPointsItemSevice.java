/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberPointsItemSevice.java
 * Package Name:com.sage.scrm.service.points.service
 * Date:2015年8月20日下午6:17:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.points.model.MemberPointsItem;
import com.bw.adv.module.points.model.PointsType;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.points.model.exp.MemberPointsItemStoreExp;

import java.math.BigDecimal;
import java.util.List;

/**
 * ClassName:MemberPointsItemSevice <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:17:16 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberPointsItemSevice extends BaseService<MemberPointsItem> {
	
	/**
	 * 
	 * queryExpByExample:根据条件查询会员积分列表信息
	 * Date: 2015年9月1日 下午4:25:06 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<MemberPointsItemExp> queryExpByExample(Example example,Page<MemberPointsItemExp> page);
	
	/**
	 * 
	 * queryPointsTypeList:查看所有积分类型
	 * Date: 2015年9月1日 下午4:25:40 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<PointsType> queryPointsTypeList();

	/**
	 * cleanPointsItem:(积分清理). <br/>
	 * Date: 2015-10-9 下午5:40:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	public void cleanPointsItem();


	/**
	 * subtractPoints:(扣减积分). <br/>
	 * Date: 2016-1-17 下午3:03:53 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId：会员ID
	 * @param usePoints：积分数量
	 * @param type：退单或者兑换使用
	 * @param reason：原因
	 */
	public void subtractPoints(Long memberId,BigDecimal usePoints,String type,String reason);
	
	/**
	 * subtractPoints:(扣减积分). <br/>
	 * Date: 2016-1-17 下午3:03:53 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId：会员ID
	 * @param usePoints：积分数量
	 * @param type：退单或者兑换使用
	 * @param reason：原因
	 */
	public void subtractPoints(Long memberId,BigDecimal usePoints,String type,String reason,Long orderId,String externalId);
	
	/**
	 * savePointsRevise:保存积分调账信息. <br/>
	 * Date: 2016年1月20日 下午1:51:46 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItemExp
	 * @return
	 */
	public int savePointsRevise(MemberPointsItemExp memberPointsItemExp) throws Exception;
	
	/**
	 * queryPointsReviseList:查询积分调账记录. <br/>
	 * Date: 2016年1月20日 下午7:37:55 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<MemberPointsItemExp> queryPointsReviseList(Example example,Page<MemberPointsItemExp> page);
    
	
	/**
	 * substrMemberPoints:扣减积分 <br/>
	 * Date: 2016年1月26日 下午2:47:02 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItem
	 * @return
	 */
	String substrMemberPoints(MemberPointsItem memberPointsItem,BigDecimal pointReviseNumber);

	/**
	 * 根据会员编码查询积分明细列表
	 * @param memberCode
	 * @return
	 */
	List<MemberPointsItemStoreExp> selectStoreExpByMemberCode(String memberCode, int pageSize, int pageNo);
	
	/**
	 * 操作积分
	 * @param memberId
	 * @param pointsResults
	 * @return
	 */
	public String operatePoints(Long memberId,List<MemberPointsItem> pointsResults);
}

