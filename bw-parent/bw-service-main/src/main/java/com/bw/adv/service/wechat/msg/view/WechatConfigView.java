/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:WechatConfigView.java
 * Package Name:com.sage.scrm.module.sys.seach
 * Date:2015年8月21日下午5:14:59
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.msg.view;
/**
 * ClassName:WechatConfigView <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月21日 下午5:14:59 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class WechatConfigView {
	
	private String wechatAccountName;
	
	private String wechatAccountNumber;
	
	private String appId;
	
	private String appSecret;
	
	private String originalId;

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}

	public String getWechatAccountName() {
		return wechatAccountName;
	}

	public void setWechatAccountName(String wechatAccountName) {
		this.wechatAccountName = wechatAccountName;
	}

	public String getWechatAccountNumber() {
		return wechatAccountNumber;
	}

	public void setWechatAccountNumber(String wechatAccountNumber) {
		this.wechatAccountNumber = wechatAccountNumber;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

}

