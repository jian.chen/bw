/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-wechat
 * File Name:WechatMenuClickRecordService.java
 * Package Name:com.sage.scrm.module.wechat.service
 * Date:2015年12月9日下午3:57:48
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/
package com.bw.adv.service.wechat.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatMenuClickRecord;

/**
 * ClassName:WechatMenuClickRecordService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午3:57:48 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMenuClickRecordService extends BaseService<WechatMenuClickRecord>{

}

