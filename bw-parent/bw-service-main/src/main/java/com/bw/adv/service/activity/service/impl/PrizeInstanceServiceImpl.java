/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PrizeInstanceServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015年11月23日下午2:07:57
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.PrizeInstance;
import com.bw.adv.module.activity.model.exp.PrizeInstanceExp;
import com.bw.adv.module.activity.repository.PrizeInstanceRepository;
import com.bw.adv.service.activity.service.PrizeInstanceService;

/**
 * ClassName:PrizeInstanceServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:07:57 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class PrizeInstanceServiceImpl extends BaseServiceImpl<PrizeInstance> implements PrizeInstanceService {
	
	private PrizeInstanceRepository prizeInstanceRepository;
	
	@Override
	public BaseRepository<PrizeInstance, ? extends BaseMapper<PrizeInstance>> getBaseRepository() {
		return prizeInstanceRepository;
	}
	
	@Autowired
	public void setPrizeInstanceRepository(
			PrizeInstanceRepository prizeInstanceRepository) {
		this.prizeInstanceRepository = prizeInstanceRepository;
	}

	@Override
	public PrizeInstanceExp findPrizeInstanceExpByAwardsId(Long awardsId) {
		
		return prizeInstanceRepository.queryPrizeInstanceExpByAwardsId(awardsId);
	}
	
}

