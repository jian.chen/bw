package com.bw.adv.service.activity.handle.impl;


import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.bw.adv.module.member.constant.GradeFactorsConstant;
import com.bw.adv.module.member.enums.GradeChangeTypeEnums;
import com.bw.adv.module.member.exception.GradeException;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.GradeConf;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.handle.GradeHandleSupport;
import com.bw.adv.service.activity.handle.GradeHandleUpService;


/**
 * ClassName: ActivityActionHandleSupport <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-29 上午11:21:20 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public  class GradeHandleUpServiceImpl extends GradeHandleSupport implements GradeHandleUpService {
	
	/**
	 * TODO 升级操作
	 */
	@Override
	public BigDecimal upGrade(Member member,Grade currentGrade,Grade changeGrade,BigDecimal gradeFactorsValue){
		MemberExp memberExp = null;
		GradeConf gradeConf = null;
		int yearFrequency = 0;
		String beginDate = null;
		String endDate = null;
		List<Grade> gradeList = null;
		String calcTime = null;
		try {
			
			gradeConf = GradeInit.GRADE_CONF;
			//如果是手动升级
			if(gradeConf.getChangeType().equals(GradeChangeTypeEnums.HANDLE.getId())){
				return null;
			}
			
			//如果计算时间为空
			if(StringUtils.isBlank(calcTime)){
				return null;
			}
			
			yearFrequency = Integer.parseInt(gradeConf.getUpPeriod());
			beginDate = getBeginDate(yearFrequency,calcTime);
			endDate = DateUtils.getCurrentDateOfDb();
			
			if(Integer.parseInt(endDate.substring(0,4)) - Integer.parseInt(beginDate.substring(0,4)) < yearFrequency){
				endDate = (Integer.parseInt(endDate.substring(0,4))+yearFrequency) +endDate.substring(4);
			}
			gradeList = GradeInit.GRADE_LIST;//得到等级列表
			System.out.println("***********等级升级*************");
			//如果是根据积分升级
			if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.POINTS_PERIOD.getId())){
				System.out.println("***********等级升级************积分***********");
				memberExp = memberRepository.findPeriodPointsByMemberId(member.getMemberId(), beginDate, endDate);
				//值为空等级不变
				if(memberExp == null || memberExp.getPointsPeriod() == null){
					return null;
				}
				gradeFactorsValue = memberExp.getPointsPeriod(); 
				for (Grade grade : gradeList) {
					//如果大于等级的设置值，则满足该等级
					if(gradeFactorsValue.compareTo(new BigDecimal(grade.getGtValue())) >=0 ){
						BeanUtils.copy(grade, changeGrade);
					}
				}
			}
			
			if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getId())){
				System.out.println("***********等级升级************订单金额***********");
				memberExp = memberRepository.findPeriodOrderAmountByMemberId(member.getMemberId(), beginDate, endDate);
				//值为空等级不变
				if(memberExp == null || memberExp.getOrderAmountPeriod() == null){
					return null;
				}
				gradeFactorsValue = memberExp.getOrderAmountPeriod(); 
				for (Grade grade : gradeList) {
					//如果大于等级的设置值，则满足该等级
					if(gradeFactorsValue.compareTo(new BigDecimal(grade.getGtValue())) >=0 ){
						BeanUtils.copy(grade, changeGrade);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new GradeException("等级升级异常:"+e.getMessage());
		}
		return gradeFactorsValue;
	}
	
}
















