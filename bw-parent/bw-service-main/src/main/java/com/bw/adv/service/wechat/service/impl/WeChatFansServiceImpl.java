/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WeChatServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月10日上午11:54:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.common.WechatConstant;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.exp.WechatFansExp;
import com.bw.adv.module.wechat.repository.WeChatFansRepository;
import com.bw.adv.service.wechat.service.WeChatFansService;

/**
 * ClassName:WeChatServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月10日 上午11:54:40 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WeChatFansServiceImpl extends BaseServiceImpl<WechatFans> implements WeChatFansService {
	
	private WeChatFansRepository weChatFansRepository;
	
	private MemberRepository memberRepository;
	
	@Override
	public WechatFans queryWechatFansById(Long id) {
		return this.weChatFansRepository.findWeChatFunsById(id);
	}

	@Override
	public BaseRepository<WechatFans, ? extends BaseMapper<WechatFans>> getBaseRepository() {
		return weChatFansRepository;
	}
	

	@Override
	public List<WechatFans> queryWechatFansByParams(String opendId,String nickName, String sex, String attentionTime,
			String cancelAttentionTime,String orgName,Page<WechatFans> page) {
		return this.weChatFansRepository.findWeChatFansByParams(opendId,nickName,sex,attentionTime,cancelAttentionTime,orgName,page);
	}
	
	@Override
	public JSONObject save(JSONObject jsonObject) {
		
		JSONObject resultJson = null;
		WechatFans wechatFans =null;
		// 返回的主键
		String id =null;
		String code =null;
		String msg =null;
		Long attentionLong =null;
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_PATTERN_YYYYMMDDHHmmss);
		try {
			wechatFans = new WechatFans();
			resultJson =new JSONObject();
			
			wechatFans.setOpenid(jsonObject.getString("openid"));
			wechatFans.setUnionid((String)jsonObject.get("unionid"));
//			wechatFans.setNickName(jsonObject.getString("nickname"));
			wechatFans.setSex(jsonObject.getString("sex"));
			wechatFans.setCty(jsonObject.getString("city"));
			wechatFans.setProvince(jsonObject.getString("province"));
			wechatFans.setCountry(jsonObject.getString("country"));
			wechatFans.setLanguage(jsonObject.getString("language"));
			wechatFans.setHeadImgUrl(jsonObject.getString("headimgurl"));
			wechatFans.setRemark(jsonObject.getString("remark"));
			// 如果用户曾多次关注，则取最后关注时间
			attentionLong =Long.parseLong(jsonObject.getString("subscribe_time")) * WechatConstant.RADIO;
			wechatFans.setAttentionTime(sdf.format(new Date(attentionLong)));
			wechatFans.setIsFans(WechatTypeConstant.IS_WECHATFAN.getCode());
			wechatFans.setCreateTime(DateUtils.getCurrentTimeOfDb());
			// 否删除 
			wechatFans.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
			this.save(wechatFans);
			code =ResultStatus.SUCCESS.getCode();
			msg ="同步微信粉丝信息成功！";
			id =wechatFans.getWechatFansId()+"";
		} catch (Exception e) {
			code =ResultStatus.SYSTEM_ERROR.getCode();
			msg ="同步微信粉丝信息失败！";
		}finally{
			resultJson.put("code", code);
			resultJson.put("id", id);
			resultJson.put("msg", msg);
		}
		return resultJson;
	}
	
	
	public List<WechatFans> transData(List<WechatFans> wechatFans) throws ParseException {
		String sex =null;
		String isFans =null;
		for(WechatFans wechatFan :wechatFans){
			// 性别
			sex =wechatFan.getSex();
			if(!StringUtils.isEmpty(sex)){
				if(WechatTypeConstant.SEX_MALE.getCode().equals(sex)){// 男性
					wechatFan.setSex(WechatTypeConstant.SEX_MALE.getMessage());
				}else if(WechatTypeConstant.SEX_FEMALE.getCode().equals(sex)){// 女
					wechatFan.setSex(WechatTypeConstant.SEX_FEMALE.getMessage());
				}else{// 未知
					wechatFan.setSex(WechatTypeConstant.SEX_UNKNOW.getMessage());
				}
			}else{
				wechatFan.setSex(WechatTypeConstant.SEX_UNKNOW.getMessage());
			}
			
			// 是否粉丝
			isFans =wechatFan.getIsFans();
			if(!StringUtils.isEmpty(isFans)){
				if(WechatTypeConstant.IS_WECHATFAN.getCode().equals(isFans)){
					wechatFan.setIsFans(WechatTypeConstant.IS_WECHATFAN.getMessage());
				}else{
					wechatFan.setIsFans(WechatTypeConstant.IS_NOT_WECHATFAN.getMessage());
				}
			}
		}
		
		return wechatFans;
	}
	
	
	@Override
	public Map<String, String> transferParam(String sex,String fromAttentionTime, String toAttentionTime) {
		
		Map<String,String> map =null;
		String sexId =null;
		try {
			map =new HashMap<String,String>();
			if(!StringUtils.isEmpty(fromAttentionTime)){
				fromAttentionTime = DateUtils.formatString(fromAttentionTime+"000000");
			}
			if(!StringUtils.isEmpty(toAttentionTime)){
				toAttentionTime = DateUtils.formatString(toAttentionTime+"235959");
			}
			if(!StringUtils.isEmpty(sex)){
				if(WechatTypeConstant.SEX_FEMALE.getMessage().equals(sex)){
					sexId =WechatTypeConstant.SEX_FEMALE.getCode();
				}else if(WechatTypeConstant.SEX_MALE.getMessage().equals(sex)){
					sexId =WechatTypeConstant.SEX_MALE.getCode();
				}else if(WechatTypeConstant.SEX_UNKNOW.getMessage().equals(sex)){
					sexId =WechatTypeConstant.SEX_UNKNOW.getCode();
				}
			}
			map.put("sex", sexId);
			map.put("fromAttentionTime", fromAttentionTime);
			map.put("toAttentionTime", toAttentionTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	
	@Override
	public void update(WechatFans wechatFans, JSONObject jsonObject) {
		Long attentionLong =null;
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DATE_PATTERN_YYYYMMDDHHmmss);
		
		if(wechatFans!=null){
			wechatFans.setOpenid(jsonObject.getString("openid"));
			wechatFans.setUnionid((String)jsonObject.get("unionid"));
//			wechatFans.setNickName(jsonObject.getString("nickname"));
			wechatFans.setSex(jsonObject.getString("sex"));
			wechatFans.setCty(jsonObject.getString("city"));
			wechatFans.setProvince(jsonObject.getString("province"));
			wechatFans.setCountry(jsonObject.getString("country"));
			wechatFans.setLanguage(jsonObject.getString("language"));
			wechatFans.setHeadImgUrl(jsonObject.getString("headimgurl"));
			wechatFans.setRemark(jsonObject.getString("remark"));
			// 如果用户曾多次关注，则取最后关注时间
			attentionLong =Long.parseLong(jsonObject.getString("subscribe_time")) * WechatConstant.RADIO;
			wechatFans.setAttentionTime(sdf.format(new Date(attentionLong)));
			wechatFans.setIsFans(WechatTypeConstant.IS_WECHATFAN.getCode());
			wechatFans.setCreateTime(DateUtils.getCurrentTimeOfDb());
			// 否删除 
			wechatFans.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
		}
			this.updateByPk(wechatFans);
		
	}
	

	@Override
	public List<WechatFansExp> modifyWechatFansExp(List<WechatFansExp> wechatFans) {
		String sex =null;
		String isFans =null;
		for(WechatFansExp wechatFan :wechatFans){
			// 性别
			sex =wechatFan.getSex();
			if(!StringUtils.isEmpty(sex)){
				// 男性
				if(WechatTypeConstant.SEX_MALE.getCode().equals(sex)){
					wechatFan.setSex(WechatTypeConstant.SEX_MALE.getMessage());
					// 女
				}else if(WechatTypeConstant.SEX_FEMALE.getCode().equals(sex)){
					wechatFan.setSex(WechatTypeConstant.SEX_FEMALE.getMessage());
					// 未知
				}else{
					wechatFan.setSex(WechatTypeConstant.SEX_UNKNOW.getMessage());
				}
			}
			
			// 是否粉丝
			isFans =wechatFan.getIsFans();
			if(!StringUtils.isEmpty(isFans)){
				if(WechatTypeConstant.IS_WECHATFAN.getCode().equals(isFans)){
					wechatFan.setIsFans(WechatTypeConstant.IS_WECHATFAN.getMessage());
				}else{
					wechatFan.setIsFans(WechatTypeConstant.IS_NOT_WECHATFAN.getMessage());
				}
			}
		}
		
		return wechatFans;
	}
	
	@Override
	public List<WechatFansExp> queryWechatFansExpList(Example example,Page<WechatFansExp> page) {
		return weChatFansRepository.findWeChatFansExpList(example,page);
	}
	
	
	@Override
	public List<WechatFans> queryWeChatFansList(Example example,Page<WechatFans> page) {
		return weChatFansRepository.findWeChatFansList(example,page);
	}
	

	@Override
	public void updateFansAndMem(WechatFans wechatFans) {
		Member mem = new Member();
		
		if(wechatFans.getMemberId() != null){
			mem.setMemberId(wechatFans.getMemberId());
			if(wechatFans.getHeadImgUrl() != null){
				mem.setMemberPhoto(wechatFans.getHeadImgUrl());
				memberRepository.updateByPkSelective(mem);
			}
		}
		
		if(wechatFans.getOpenid() != null){
			weChatFansRepository.updateByOpenIdSelective(wechatFans);
		}
	}
	
	
	@Override
	public WechatFans saveOrUpdate(WechatFans wechatFans) {
		WechatFans weChatTemp = null;
		
		weChatTemp = weChatFansRepository.findWeChatFunsByOpenId(wechatFans.getOpenid());
		if(weChatTemp != null){//修改
			BeanUtils.copyNotNull(wechatFans, weChatTemp);
			weChatTemp.setIsFans("Y");
			if(weChatTemp.getChannelId() != null && weChatTemp.getChannelId() == ChannelConstant.WEIXIN.getId()){
				weChatTemp.setChannelId(null);
			}
			this.weChatFansRepository.updateByPkSelective(weChatTemp);
			return weChatTemp;
		}else{//新增粉丝数据
			wechatFans.setAttentionTime(DateUtils.getCurrentTimeOfDb());
			wechatFans.setIsDelete("0");
			wechatFans.setIsFans("Y");
			weChatFansRepository.save(wechatFans);
			return wechatFans;
		}
	}
	
	@Autowired
	public void setWeChatFansRepository(WeChatFansRepository weChatFansRepository) {
		this.weChatFansRepository = weChatFansRepository;
	}

	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}

}

