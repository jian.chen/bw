/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatKeysRuleService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月24日下午5:36:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.WechatKeysRuleExp;

/**
 * ClassName:WechatKeysRuleService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午5:36:33 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatKeysRuleService extends BaseService<WechatKeysRule> {
	
	/**
	 * queryWechatKeyRules:(查询有效的关键字). <br/>
	 * Date: 2015年8月26日 下午5:31:54 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	public List<WechatKeysRule> queryWechatKeyRules();
	
	/**
	 * queryWechatKeyRule:(根据关键字匹配对应的实体). <br/>
	 * Date: 2015年8月26日 下午5:32:34 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param content
	 * @return
	 */
	public WechatKeysRule queryWechatKeyRule(String content);
	
	/**
	 * queryWechatKeyRuleExpByKeyRule:(查询扩展类). <br/>
	 * Date: 2015年8月26日 下午5:34:21 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatKeysRule
	 * @return
	 */
	public WechatKeysRuleExp queryWechatKeyRuleExpByKeyRule(WechatKeysRule wechatKeysRule);
	
}

