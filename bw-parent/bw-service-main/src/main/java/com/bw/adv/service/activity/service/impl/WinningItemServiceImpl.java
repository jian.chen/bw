/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WinningItemServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015年11月23日下午2:11:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.WinningItemExp;
import com.bw.adv.module.activity.repository.WinningItemRepository;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.service.activity.service.WinningItemService;

/**
 * ClassName:WinningItemServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:11:09 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WinningItemServiceImpl extends BaseServiceImpl<WinningItem> implements WinningItemService {
	
	private WinningItemRepository winningItemRepository;
	
	@Override
	public BaseRepository<WinningItem, ? extends BaseMapper<WinningItem>> getBaseRepository() {
		return winningItemRepository;
	}
	
	@Autowired
	public void setWinningItemRepository(WinningItemRepository winningItemRepository) {
		this.winningItemRepository = winningItemRepository;
	}
	
	@Override
	public Long countPersonsCountByActivityIdAndAwardsId(Long awardsId,Long sceneGameInstanceId,Long memberId, String startDate, String endDate) {
		Example example =null;
		Long personsCount =null;
		example = new Example();
		Criteria criteria = example.createCriteria();
		if(sceneGameInstanceId!=null){
			criteria.andEqualTo("wi.SCENE_GAME_INSTANCE_ID", sceneGameInstanceId);
		}
		if(awardsId!=null){
			criteria.andEqualTo("wi.AWARDS_ID", awardsId);
		}
		if(memberId!=null){
			criteria.andEqualTo("wi.MEMBER_ID", memberId);
		}
		if(!StringUtils.isEmpty(startDate)&&!StringUtils.isEmpty(endDate)){
			criteria.andGreaterThanOrEqualTo("wi.ITEM_DATE", startDate);
			criteria.andLessThanOrEqualTo("wi.ITEM_DATE", endDate);
		}
		
		personsCount = winningItemRepository.findWinningItemPersonsByExample(example);
		return personsCount;
	}

	@Override
	public List<WinningItemExp> findWinningItemListByMemberIdAndActivityId(Example example, Page<WinningItemExp> page) {
		return winningItemRepository.queryWinningItemListByMemberIdAndActivityId(example,page);
	}
	
	@Override
	public List<WinningItemExp> findLastNum(Long num) {
		
		return winningItemRepository.queryItemLastNum(num);
	}

	@Override
	public Long findWinningItemPersonsByExample(Example example) {
		return winningItemRepository.findWinningItemPersonsByExample(example);
	}
	
}

