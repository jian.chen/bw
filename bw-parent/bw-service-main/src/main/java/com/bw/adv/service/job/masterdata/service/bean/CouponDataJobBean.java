package com.bw.adv.service.job.masterdata.service.bean;


import java.util.List;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.repository.ComTaskRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.coupon.service.CouponTypeService;

public class CouponDataJobBean extends BaseJob {
	
	private CouponTypeService couponTypeService;
	private ComTaskRepository comTaskRepository;
	
	@Override
	public void excute() throws Exception {
		initService();
		String serviceUrl = null;
		List<ComTask> comtask = null;
		try {
			serviceUrl = CouponDataJobBean.class.getName();
			comtask = comTaskRepository.findJobTaskByTypeId(serviceUrl);
			if(comtask.get(0).getLastRuntime().startsWith(DateUtils.getCurrentDateOfDb())){
				return;
			}
			couponTypeService.renovateCouponType();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("同步数据修改优惠券类型");
	}
	
	private void initService() {
		this.couponTypeService = this.getApplicationContext().getBean(CouponTypeService.class);
		this.comTaskRepository = this.getApplicationContext().getBean(ComTaskRepository.class);
	}

	@Override
	protected boolean isNeedLock() {
		return false;
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.MASTER_DATA_COUPON;
	}

}
