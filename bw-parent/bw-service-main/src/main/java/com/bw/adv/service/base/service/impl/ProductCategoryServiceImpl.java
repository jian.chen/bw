/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ProductCategoryServiceImpl.java
 * Package Name:com.sage.scrm.service.base.service.impl
 * Date:2016年1月14日上午11:48:45
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.ProductCategory;
import com.bw.adv.module.base.model.ProductCategoryItem;
import com.bw.adv.module.base.repository.ProductCategoryItemRepository;
import com.bw.adv.module.base.repository.ProductCategoryRepository;
import com.bw.adv.service.base.service.ProductCategoryService;

/**
 * ClassName:ProductCategoryServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 上午11:48:45 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class ProductCategoryServiceImpl extends BaseServiceImpl<ProductCategory> implements ProductCategoryService {

	@Autowired
	private ProductCategoryRepository productCategoryRepository;
	
	@Autowired
	private ProductCategoryItemRepository productCategoryItemRepository;
	
	@Override
	public BaseRepository<ProductCategory, ? extends BaseMapper<ProductCategory>> getBaseRepository() {
		return productCategoryRepository;
	}

	@Override
	public List<ProductCategory> queryAll() {
		return productCategoryRepository.findAll();
	}

	@Override
	public List<ProductCategory> queryProductCategoryPage(Page<ProductCategory> page) {
		return productCategoryRepository.findProductCategoryPage(page);
	}

	@Override
	public int findProductCategoryIsMultiple(ProductCategory productCategory) {
		List<ProductCategory> result = productCategoryRepository.findProductCategoryIsMultiple(productCategory);
		return result.size();
	}

	@Override
	@Transactional
	public void removeProductCategory(Long productCategoryId) {
		productCategoryItemRepository.removeByCategoryId(productCategoryId);
		productCategoryRepository.removeByPk(productCategoryId);
	}

	@Override
	public void saveProductCategoryItem(List<ProductCategoryItem> itemList) {
		this.productCategoryItemRepository.saveList(itemList);
	}

	@Override
	public void removeProductCategoryItemByProductId(Long productId) {
		this.productCategoryItemRepository.removeByProductId(productId);
	}

	@Override
	public void saveProductCategory(List<ProductCategory> list) {
		productCategoryRepository.saveList(list);
	}
	
}

