/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SmsTempService.java
 * Package Name:com.sage.scrm.service.base.service
 * Date:2015年8月19日上午11:27:08
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.component.msg.model.SmsTemp;
import com.bw.adv.module.component.msg.model.SmsTempParams;
import com.bw.adv.module.component.msg.model.SmsTempType;
import com.bw.adv.module.component.msg.model.exp.SmsInstanceExp;
import com.bw.adv.module.component.msg.model.exp.SmsTempExp;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.sys.model.SysUser;

/**
 * ClassName:SmsTempService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 上午11:27:08 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SmsTempService extends BaseService<SmsTemp> {
	
	
	/**
	 * 
	 * deleteSmsTempList:批量删除模板（逻辑删除）
	 * Date: 2015年8月27日 上午10:14:53 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTempIds
	 */
	public void deleteSmsTempList(List<Long> smsTempIds);
	
	/**
	 * 
	 * querySmsExpByStatusId:查询有效短信模板List
	 * Date: 2015年9月2日 下午3:24:00 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	public List<SmsTempExp> querySmsExpByStatusId(Page<SmsTempExp> page);
	
	/**
	 * 
	 * querySmsTempTypeById:根据模板类型ID查询模板类型详情
	 * Date: 2015年9月2日 下午3:24:51 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTempTypeId
	 * @return
	 */
	public SmsTempType querySmsTempTypeById(Long smsTempTypeId);
	
	/**
	 * 
	 * queryExpById:根据id查询模板详情（扩展类）
	 * Date: 2015年9月2日 下午3:25:39 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTempId
	 * @return
	 */
	public SmsTempExp queryExpById(Long smsTempId);
	
	/**
	 * 
	 * querySmsInstanceList:查看发送记录
	 * Date: 2015年8月19日 下午1:36:59 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<SmsInstance> querySmsInstanceList();
	
	/**
	 * 
	 * querySmsInstanceByMobile:根据手机号查看发送记录
	 * Date: 2015年8月19日 下午1:37:26 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param mobile
	 * @return
	 */
	public List<SmsInstance> querySmsInstanceByMobile(String mobile);
	
	/**
	 * 
	 * querySmsInstanceByExample:根据输入条件查询短信实例
	 * Date: 2015年8月27日 上午10:58:40 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	public List<SmsInstance> querySmsInstanceByExample(Example example,Page<SmsInstance> page);
	
	/**
	 * 
	 * querySmsInstanceExpByExample:根据输入条件查询短信实例(扩展类)
	 * Date: 2015年8月27日 下午5:13:24 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<SmsInstanceExp> querySmsInstanceExpByExample(Example example,Page<SmsInstanceExp> page);
	
	/**
	 * 
	 * addSmsInstanceList:批量插入短信实例
	 * Date: 2015年8月20日 上午11:46:35 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public int addSmsInstanceList(Long groupId,Long smsTempId);
	
	/**
	 * 
	 * deleteSmsInstanceList:批量删除短信实例
	 * Date: 2015年9月1日 下午10:06:52 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsInstanceIds
	 */
	public void deleteSmsInstanceList(List<Long> smsInstanceIds);
	
	/**
	 * 
	 * querySysUserById:根据id查询系统用户信息
	 * Date: 2015年8月19日 下午3:41:43 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUserId
	 * @return
	 */
	public SysUser querySysUserById(Long sysUserId);
	
	/**
	 * 
	 * queryMemberGroupList:查询已生效的分组列表
	 * Date: 2015年8月20日 下午3:14:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<MemberGroup> queryMemberGroupList();
	
	/**
	 * 
	 * queryParamsList:查询所有短信通道参数类型
	 * Date: 2015年9月5日 下午8:19:23 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<SmsTempParams> queryParamsList();

	/**
	 * querySmsTempByTypeId:(根据模板类型查询目标列表). <br/>
	 * Date: 2015-9-3 下午3:52:18 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param smsTempTypeId
	 * @return
	 */
	public List<SmsTemp> querySmsTempByTypeId(Long smsTempTypeId);
	
	
	public List<SmsTemp> querySmsTempAll();
	
	
}

