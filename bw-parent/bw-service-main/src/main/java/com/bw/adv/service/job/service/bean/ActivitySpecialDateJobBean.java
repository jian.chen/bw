/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivitySpecialDateJobBean.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2015年9月21日下午6:06:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.bw.adv.module.activity.constant.ActivityConstant;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.repository.ComTaskRepository;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.ActivityActionService;

/**
 * ClassName:ActivitySpecialDateJobBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月21日 下午6:06:36 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class ActivitySpecialDateJobBean extends BaseJob {
	
	private ActivityActionService activityActionService; 
	
	private ComTaskRepository comTaskRepository;

	@Override
	protected void excute() throws Exception {
		List<ActivityInstance> list = null;
		String currentDate = null;
		String serviceUrl = null;
		List<ComTask> comtask = null;
		try {
			initService();
			serviceUrl = ActivitySpecialDateJobBean.class.getName();
			comtask = comTaskRepository.findJobTaskByTypeId(serviceUrl);
			if(comtask.get(0).getLastRuntime().startsWith(DateUtils.getCurrentDateOfDb())){
				return;
			}
		    //活动对应活动实例
			currentDate = DateUtils.getCurrentTimeOfDb();
			list = RuleInit.getActivityInstanceList(ActivityConstant.ACTIVIY_SPECIAL_DATE.getCode());
			for (ActivityInstance instance : list) {
				//如果没有过期
				if(StringUtils.isNotBlank(instance.getThruDate())){
					if(instance.getThruDate().compareTo(currentDate) < 0){
						continue;
					}
				}
				activityActionService.addActivityActionSpecialDate(instance);
				
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("积分计算任务执行完成");
	}
	
	private void initService() {
		this.activityActionService = this.getApplicationContext().getBean(ActivityActionService.class);
		this.comTaskRepository = this.getApplicationContext().getBean(ComTaskRepository.class);
	}
	
	@Override
	protected String getLockCode() {
		return LockCodeConstant.MEMBER_SPECIAL_DATE_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

}

