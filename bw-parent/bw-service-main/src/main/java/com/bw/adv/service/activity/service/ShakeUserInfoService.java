package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.ShakeUserInfo;
import com.bw.adv.module.activity.model.UserAndWinning;

public interface ShakeUserInfoService extends BaseService<ShakeUserInfo> {
	
	/**
	 * 根据手机号查询参与者信息是否存在
	 * @param phoneNum
	 * @return
	 */
	ShakeUserInfo queryShakeUserInfoByPhoneNum(String phoneNum);
	
	/**
	 * 产品发布会查询中奖结果
	 * @return
	 */
	List<UserAndWinning> selectAllForUserAndWinning();
}
