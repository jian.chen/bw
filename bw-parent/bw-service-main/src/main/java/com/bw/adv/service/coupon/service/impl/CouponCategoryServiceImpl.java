/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CouponCategoryServiceImpl.java
 * Package Name:com.sage.scrm.service.coupon.service.impl
 * Date:2015年12月28日上午9:49:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service.impl;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.model.CouponCategory;
import com.bw.adv.module.coupon.model.CouponCategoryItem;
import com.bw.adv.module.coupon.model.exp.CouponCategoryItemExp;
import com.bw.adv.module.coupon.repository.CouponCategoryItemRepository;
import com.bw.adv.module.coupon.repository.CouponCategoryRepository;
import com.bw.adv.service.coupon.service.CouponCategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName:CouponCategoryServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月28日 上午9:49:05 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class CouponCategoryServiceImpl extends BaseServiceImpl<CouponCategory>
	implements CouponCategoryService{

	private CouponCategoryRepository couponCategoryRepository;
	
	private CouponCategoryItemRepository couponCategoryItemRepository;
	
	@Override
	public BaseRepository<CouponCategory, ? extends BaseMapper<CouponCategory>> getBaseRepository() {
		
		return couponCategoryRepository;
	}
	

	@Override
	public List<CouponCategoryItemExp> queryList(
			Page<CouponCategoryItemExp> page) {
		
		return couponCategoryItemRepository.findListByStatus(StatusConstant.COUPON_CATEGORY_ITEM_REMOVE.getId(), page);
	}
	
	@Override
	public CouponCategoryItemExp queryDetail(Long couponCategoryItem) {
		
		return couponCategoryItemRepository.findExpByPk(couponCategoryItem);
	}
	
	@Override
	public List<CouponCategory> queryCouponCategoryList(
			Page<CouponCategory> page) {
		
		return couponCategoryRepository.findListByStatus(StatusConstant.COUPON_CATEGORY_REMOVE.getId(), page);
	}
	
	@Override
	public List<CouponCategory> queryCouponCategoryPage(Example example,Page<CouponCategory> page) {
		return couponCategoryRepository.findCouponCategoryPage(example,page);
	}
	
	@Override
	public void saveCouCateItem(CouponCategoryItem couponCategoryItem) {
		
		couponCategoryItemRepository.saveSelective(couponCategoryItem);
	}

	@Override
	public void updateCouCateItemByPk(CouponCategoryItem couponCategoryItem) {
		
		couponCategoryItemRepository.updateByPkSelective(couponCategoryItem);
	}
	

	@Override
	public void deletedCouCateItems(String couponCategoryItems) {
		
		couponCategoryItemRepository.removeList(StatusConstant.COUPON_CATEGORY_ITEM_DISENABLE.getId(), couponCategoryItems);
	}
	

	@Override
	public void deletedCouponCategorys(String couponCategoryIds) {
		
		couponCategoryRepository.removeList(StatusConstant.COUPON_CATEGORY_REMOVE.getId(), couponCategoryIds);
	}


	@Autowired
	public void setCouponCategoryRepository(
			CouponCategoryRepository couponCategoryRepository) {
		this.couponCategoryRepository = couponCategoryRepository;
	}

	@Autowired
	public void setCouponCategoryItemRepository(
			CouponCategoryItemRepository couponCategoryItemRepository) {
		this.couponCategoryItemRepository = couponCategoryItemRepository;
	}


	@Override
	public List<CouponCategoryItemExp> queryListByCouponId(Long couponId) {
		return this.couponCategoryItemRepository.queryListByCouponId(couponId);
	}



}

