/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberService1.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2015年8月10日下午3:39:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.GradeConf;


/**
 * ClassName: GradeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月6日 上午10:49:16 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface GradeService extends BaseService<Grade>{
	
	/**
	 * queryGradeList:查询等级列表 <br/>
	 * Date: 2015年9月6日 上午10:49:23 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	List<Grade> queryGradeList();
	
	
	List<Grade> queryGradeListForJS();
	
	/**
	 * queryGradeConfByPk:查询等级配置信息 <br/>
	 * Date: 2015年9月6日 上午10:51:47 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param gradeConfId
	 * @return
	 */
	GradeConf queryGradeConfByPk(Long gradeConfId);
	
	
	/**
	 * updateGradeConf:修改等级配置 <br/>
	 * Date: 2015年9月6日 上午11:15:11 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param gradeConf
	 */
	void updateGradeConf(GradeConf gradeConf);
	
	
	/**
	 * updateGrade:修改等级信息<br/>
	 * Date: 2015年9月21日 下午2:23:48 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param grades
	 * @param gradeConf
	 */
	void updateGrade(Grade[] grades,GradeConf gradeConf);
}

