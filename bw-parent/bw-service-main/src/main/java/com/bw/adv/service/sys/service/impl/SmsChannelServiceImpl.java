/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SmsChannelServiceImpl.java
 * Package Name:com.sage.scrm.service.sys.service.impl
 * Date:2015年8月17日下午2:41:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.sys.service.impl;


import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.model.SmsContent;
import com.bw.adv.module.sms.repository.SmsChannelRepository;
import com.bw.adv.module.sms.service.SmsServiceManager;
import com.bw.adv.service.exception.SmsChannelException;
import com.bw.adv.service.sys.service.SmsChannelService;

/**
 * ClassName:SmsChannelServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午2:41:22 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class SmsChannelServiceImpl extends BaseServiceImpl<SmsChannel> implements SmsChannelService {

	private SmsChannelRepository smsChannelRepository;
	private SmsServiceManager smsServiceManager;
	
	@Override
	public BaseRepository<SmsChannel, ? extends BaseMapper<SmsChannel>> getBaseRepository() {
		
		return smsChannelRepository;
	}
	
	/**
     * 查询所有的短信渠道
     * @return
     */
    @Override
    public List<SmsChannel> queryAllSmsChannel() {
        return this.smsChannelRepository.queryAllSmsChannel();
    }

	@Override
	public List<SmsChannel> querySmsChannel(Page<SmsChannel> page) {
		
		return smsChannelRepository.findByStatus("Y", page);
	}

	@Override
	public void deleteSmsChannel(Long smsChannelId) {
		
		SmsChannel smsChannel = new SmsChannel();
		smsChannel.setSmsChannelId(smsChannelId);
		smsChannel.setIsActive("N");
		
		smsChannelRepository.updateByPkSelective(smsChannel);
	}
	
	@Override
	public void deleteSmsChannelList(List<Long> smsChannelIds) {
		
		smsChannelRepository.updateIsActiveList(smsChannelIds);
		
	}
	
	@Override
	public List<SmsChannel> querySmsNames() {
		
		return smsChannelRepository.findAllName();
	}

	@Autowired
	public void setSmsChannelRepository(SmsChannelRepository smsChannelRepository) {
		this.smsChannelRepository = smsChannelRepository;
	}

	@Override
	public boolean updateSmsChannelAccount(SmsChannel smsChannel) {
        String isActive = smsChannel.getIsActive();
        if (StringUtils.isNotBlank(isActive) && isActive.equals("Y")) {
            this.smsChannelRepository.updateSmsChannelUnActive();
        } else {
            smsChannel.setIsActive("N");
        }
        return this.smsChannelRepository.updateSmsChannelAccount(smsChannel);
    }

	@Override
	public boolean testSmsChannel(Long ssmChannelId, SmsContent smsContent) {
        SmsChannel smsChannel = this.queryByPk(ssmChannelId);
        if (smsChannel == null) {
            throw new SmsChannelException("no find smsChannel, by smsChannelId: " + ssmChannelId);
        }
        smsContent.setContent(smsChannel.getRemark());
        return smsServiceManager.sendTestMsg(smsChannel, smsContent);
    }

	@Autowired
	public void setSmsServiceManager(SmsServiceManager smsServiceManager) {
		this.smsServiceManager = smsServiceManager;
	}

}

