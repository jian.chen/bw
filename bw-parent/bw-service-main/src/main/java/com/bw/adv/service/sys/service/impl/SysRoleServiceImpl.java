/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SysRoleServiceImpl.java
 * Package Name:com.sage.scrm.service.sys.service.impl
 * Date:2015-8-14下午4:48:43
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.SysFunctionOperation_bak;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysRoleFuncKey;
import com.bw.adv.module.sys.model.SysUserRoleKey;
import com.bw.adv.module.sys.model.exp.SysRoleExp;
import com.bw.adv.module.sys.repository.SysRoleFuncRepository;
import com.bw.adv.module.sys.repository.SysRoleRepository;
import com.bw.adv.module.sys.repository.SysUserRoleRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.exception.SysException;
import com.bw.adv.service.sys.service.SysRoleFuncService;
import com.bw.adv.service.sys.service.SysRoleService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * ClassName:SysRoleServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-14 下午4:48:43 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole> implements SysRoleService {

	private SysRoleRepository sysRoleRepository;
	private SysRoleFuncService sysRoleFuncService;
	private SysRoleFuncRepository sysRoleFuncRepository;
	private SysUserRoleRepository sysUserRoleRepository;

	@Autowired
	public void setSysRoleFuncRepository(SysRoleFuncRepository sysRoleFuncRepository) {
		this.sysRoleFuncRepository = sysRoleFuncRepository;
	}

	@Autowired
	public void setSysUserRoleRepository(SysUserRoleRepository sysUserRoleRepository) {
		this.sysUserRoleRepository = sysUserRoleRepository;
	}

	@Override
	public BaseRepository<SysRole, ? extends BaseMapper<SysRole>> getBaseRepository() {
		return sysRoleRepository;
	}


	@Autowired
	public void setSysRoleRepository(SysRoleRepository sysRoleRepository) {
		this.sysRoleRepository = sysRoleRepository;
	}

	@Override
	public List<SysRole> findListForSysRole(String useFlag,Page<SysRole> page) {
		return this.sysRoleRepository.findSysRoleList(useFlag,page);
	}


	@Override
	public List<SysRole> findSysRoleListByUserId(Long sysUserId) {
		return this.sysRoleRepository.findSysRoleListByUserId(sysUserId);
	}



	@Override
	public SysRoleExp findSysFunctionOperationListByPrimaryKey(Long sysRoleId) {
		return this.sysRoleRepository.findSysFunctionOperationListByPrimaryKey(sysRoleId);
	}


	@Override
	public void saveSysRole() {
		List<SysFunction> sysFunctionList=new ArrayList<SysFunction>();//功能List
		List<SysFunctionOperation_bak> sysFunctionOperationList=new ArrayList<SysFunctionOperation_bak>();//功能操作List

		SysRole sysRole;
		SysRoleFuncKey sysRoleFuncKey;

		List<SysRoleFuncKey> sysRoleFuncKeyList=new ArrayList<SysRoleFuncKey>();

		try {
			sysRole = new SysRole();
			sysRole.setRoleId(4L);
			sysRole.setOrgId((long)1);
			sysRole.setUseFlag("Y");
			sysRole.setRoleName("RoleName1");
			sysRoleRepository.save(sysRole);

			for(int i = 0; i < sysFunctionList.size(); i++){
				for(int j = 0; j<sysFunctionOperationList.size(); j++){
					if(sysFunctionList.get(i).getFunctionId()==sysFunctionOperationList.get(j).getFunctionId()){
						sysRoleFuncKey = new SysRoleFuncKey();
						sysRoleFuncKey.setRoleId(sysRole.getRoleId());
						sysRoleFuncKey.setFunctionId(sysFunctionList.get(i).getFunctionId());
						sysRoleFuncKey.setFunctionOperationId(sysFunctionOperationList.get(j).getOperationItemId());
						sysRoleFuncKeyList.add(sysRoleFuncKey);
					}
				}
			}

			sysRoleFuncService.insertSysRoleFuncs(sysRoleFuncKeyList);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateSysRole() {
		List<SysFunction> sysFunctionList=new ArrayList<SysFunction>();//功能List
		List<SysFunctionOperation_bak> sysFunctionOperationList=new ArrayList<SysFunctionOperation_bak>();//功能操作List

		SysRole sysRole;
		SysRoleFuncKey sysRoleFuncKey;

		List<SysRoleFuncKey> sysRoleFuncKeyList=new ArrayList<SysRoleFuncKey>();

		try {
			sysRole = new SysRole();
			sysRole.setRoleId(4L);
			sysRole.setOrgId((long)1);
			sysRole.setUseFlag("Y");
			sysRole.setRoleName("RoleName1");
			sysRoleRepository.save(sysRole);

			for(int i = 0; i < sysFunctionList.size(); i++){
				for(int j = 0; j<sysFunctionOperationList.size(); j++){
					if(sysFunctionList.get(i).getFunctionId()==sysFunctionOperationList.get(j).getFunctionId()){
						sysRoleFuncKey = new SysRoleFuncKey();
						sysRoleFuncKey.setRoleId(sysRole.getRoleId());
						sysRoleFuncKey.setFunctionId(sysFunctionList.get(i).getFunctionId());
						sysRoleFuncKey.setFunctionOperationId(sysFunctionOperationList.get(j).getOperationItemId());
						sysRoleFuncKeyList.add(sysRoleFuncKey);
					}
				}
			}

			sysRoleFuncService.updateSysRoleFuncs(sysRoleFuncKeyList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	@Transactional
	public Long saveRuleFunction(JSONObject jsonParams) {
		SysRole role = null;
		SysRoleFuncKey sysRoleFuncKey = null;
		List<SysRoleFuncKey> roleFuncList = null;
		JSONArray jsonArray = null;
		JSONObject jsonObject = null;

		roleFuncList = new ArrayList<SysRoleFuncKey>();
		role = new SysRole();
		role.setRoleName(jsonParams.getString("roleName"));
		role.setUseFlag(StatusConstant.SYS_ROLE_ACTIVE.getCode());
		role.setStartDate(DateUtils.getCurrentDateOfDb());
		if(StringUtils.isEmpty(jsonParams.getString("roleId"))){
			//新建角色保存
			sysRoleRepository.save(role);
		}else{
			role.setRoleId(jsonParams.getLong("roleId"));
			//编辑角色保存
			sysRoleRepository.updateByPkSelective(role);
			//删除该角色原有的操作权限
			sysRoleFuncRepository.removeByRoleId(jsonParams.getLong("roleId"));
		}

		jsonArray =JSONArray.fromObject(jsonParams.get("roleFuncArray"));
		for (Object object : jsonArray) {
			jsonObject = JSONObject.fromObject(object);
			sysRoleFuncKey = new SysRoleFuncKey();
			sysRoleFuncKey.setRoleId(role.getRoleId());
			sysRoleFuncKey.setFunctionId(jsonObject.getLong("functionId"));
			sysRoleFuncKey.setFunctionOperationId(jsonObject.getLong("operationItemId"));
			roleFuncList.add(sysRoleFuncKey);
		}

		if(roleFuncList.size()>0){
			//保存新的角色权限
			sysRoleFuncRepository.saveList(roleFuncList);
		}
		return role.getRoleId();

	}


	@Override
	public void deleteSysRoleByRoleId(Long roleId,String stopDate) {
		this.sysRoleRepository.removeByRoleId(roleId,stopDate);
	}

	@Override
	@Transactional
	public void deleteSysRoleByRoleId(Long roleId) {
		List<SysUserRoleKey> roleKeyList = this.sysUserRoleRepository.findByRoleId(roleId);
		if (CollectionUtils.isNotEmpty(roleKeyList)) {
			throw new SysException("该角色已被使用不能删除");
		}
		this.sysRoleFuncRepository.removeByRoleId(roleId);
		this.sysRoleRepository.removeByPk(roleId);
	}


	@Override
	public List<SysRole> findSysRoleListBySysRoleName(String useFlag,Long roleId) {
		return this.sysRoleRepository.findSysRoleListBySysRoleName(useFlag,roleId);
	}

	@Override
	public SysRole findByRoleName(String roleName) {
		return  this.sysRoleRepository.findByRoleName(roleName);
	}
}

