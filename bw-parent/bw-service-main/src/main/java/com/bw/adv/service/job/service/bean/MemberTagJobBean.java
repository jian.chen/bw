package com.bw.adv.service.job.service.bean;


import java.util.List;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.utils.MyConstants;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.search.MemberTagSearch;
import com.bw.adv.service.member.service.MemberTagService;

public class MemberTagJobBean extends BaseJob {
	
	private MemberTagService memberTagService;
	
	@Override
	public void excute() throws Exception {
		logger.info("会员标签定时任务开始");
		initService();
		MemberTagSearch search = null;
		List<MemberTag> result = null;
		try {
			search = new MemberTagSearch();
			search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
			search.setEqualExecuteType(MyConstants.MEMBER_TAG_TYPE.Automatic);
			result = this.memberTagService.queryListSearchAuto(search);
			if(result != null && result.size()>0){
				for(int i=0;i<result.size();i++){
					try {
						memberTagService.excuteMemberTagJob(result.get(i));
					} catch (Exception e) {
						logger.error(e.getMessage()+"-----------标签执行失败，名称为：-----"+result.get(i).getMemberTagName());
						e.printStackTrace();
						continue;
					}
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("会员标签定时任务结束");
	}
	
	private void initService() {
		this.memberTagService = this.getApplicationContext().getBean(MemberTagService.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.MEMBER_TAG_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

}
