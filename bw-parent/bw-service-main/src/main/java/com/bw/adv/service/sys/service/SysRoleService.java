/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SysRoleService.java
 * Package Name:com.sage.scrm.service.sys.service
 * Date:2015-8-14下午4:47:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.sys.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.exp.SysRoleExp;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName:SysRoleService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-14 下午4:47:35 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
@Service
public interface SysRoleService extends BaseService<SysRole> {

	/**
	 * queryListForSysRole:查询角色列表 <br/>
	 * Date: 2015-8-17 下午3:38:49 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 */
	public List<SysRole> findListForSysRole(String useFlag,Page<SysRole> page);

	/**
	 * findSysRoleListByUserId:通过用户ID获得用户角色列表. <br/>
	 * Date: 2015-8-26 上午10:56:34 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param sysUserId
	 * @return
	 */
	public List<SysRole> findSysRoleListByUserId(Long sysUserId);

	/**
	 * findSysRoleListBySysRoleName:根据角色名查询角色列表. <br/>
	 * Date: 2015-9-16 下午4:56:34 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param sysRoleName
	 * @return
	 */
	public List<SysRole> findSysRoleListBySysRoleName(String useFlag,Long roleId);

	/**
	 * findSysFunctionOperationListByPrimaryKey:通过用户角色ID获取功能操作列表. <br/>
	 * Date: 2015-8-26 上午10:57:36 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param sysRoleId
	 * @return
	 */
	public SysRoleExp findSysFunctionOperationListByPrimaryKey(Long sysRoleId);


	/**
	 * saveSysRole:保存用户角色. <br/>
	 * Date: 2015-8-26 上午10:58:51 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 */
	public void saveSysRole();

	/**
	 * updateSysRole:修改用户角色. <br/>
	 * Date: 2015-8-26 上午10:58:54 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 */
	public void updateSysRole();

	/**
	 * saveRuleFunction:新建/编辑 保存角色信息（包括操作权限）. <br/>
	 * Date: 2015-8-31 下午5:26:46 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param json
	 */
	public Long saveRuleFunction(JSONObject json);

	/**
	 * deleteSysRoleByRoleId:删除角色信息(软删除). <br/>
	 * Date: 2015-8-31 下午5:45:02 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param roleId
	 */
	public void deleteSysRoleByRoleId(Long roleId,String stopDate);

	/**
	 * 删除角色信息
	 * @param roleId
	 */
	public void deleteSysRoleByRoleId(Long roleId);

	/**
	 * 根据角色名称查询角色
	 * @param roleName
	 * @return
	 */
	public SysRole findByRoleName(String roleName);

}

