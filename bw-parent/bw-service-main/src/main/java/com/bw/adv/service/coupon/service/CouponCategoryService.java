/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CouponCategoryService.java
 * Package Name:com.sage.scrm.service.coupon.service
 * Date:2015年12月28日上午9:47:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.model.CouponCategory;
import com.bw.adv.module.coupon.model.CouponCategoryItem;
import com.bw.adv.module.coupon.model.exp.CouponCategoryItemExp;

/**
 * ClassName:CouponCategoryService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月28日 上午9:47:54 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponCategoryService extends BaseService<CouponCategory> {
	
	/**
	 * 
	 * queryList:(查询优惠券类别明细List(所有未删除明细)). <br/>
	 * Date: 2015年12月28日 上午10:31:25 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	public List<CouponCategoryItemExp> queryList(Page<CouponCategoryItemExp> page);
	
	/**
	 * 
	 * queryDetail:(根据id查询类别明细(扩展类)详情). <br/>
	 * Date: 2015年12月28日 上午10:31:33 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItem
	 * @return
	 */
	public CouponCategoryItemExp queryDetail(Long couponCategoryItem);
	
	/**
	 * 
	 * saveCouCateItem:(新增优惠券类别明细). <br/>
	 * Date: 2015年12月28日 下午1:11:36 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 */
	public void saveCouCateItem(CouponCategoryItem couponCategoryItem);
	
	/**
	 * 
	 * updateCouCateItemByPk:(修改优惠券类别明细). <br/>
	 * Date: 2015年12月28日 下午1:12:50 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItem
	 */
	public void updateCouCateItemByPk(CouponCategoryItem couponCategoryItem);
	
	/**
	 * 
	 * queryCouponCategoryPage:(查询优惠券类别----我要领劵模块). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<CouponCategory> queryCouponCategoryPage(Example example,Page<CouponCategory> page);
	/**
	 * 
	 * deletedCouCateItems:(批量删除优惠券类别明细). <br/>
	 * Date: 2015年12月28日 下午1:16:58 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItems
	 */
	public void deletedCouCateItems(String couponCategoryItems);
	
	/**
	 * 
	 * queryCouponCategoryList:(查询优惠券类别List(未删除)). <br/>
	 * Date: 2015年12月28日 下午12:07:02 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	public List<CouponCategory> queryCouponCategoryList(Page<CouponCategory> page);
	
	/**
	 * 
	 * deletedCouponCategorys:(批量删除优惠券类别). <br/>
	 * Date: 2015年12月28日 下午1:44:55 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryIds
	 */
	public void deletedCouponCategorys(String couponCategoryIds);
	
	
	/**
	 * 根据优惠券ID查询当季优惠
	 * @param couponId
	 * @return
	 */
	public List<CouponCategoryItemExp> queryListByCouponId(Long couponId);

}

