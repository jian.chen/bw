package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.QuestionCount;
import com.bw.adv.module.activity.model.Questionnaire;
import com.bw.adv.module.activity.model.QuestionnaireInstance;
import com.bw.adv.module.activity.model.QuestionnaireTemplate;
import com.bw.adv.module.activity.model.QuestionnaireTemplateInfo;
import com.bw.adv.module.activity.repository.QuestionnaireInstanceRepository;
import com.bw.adv.module.activity.repository.QuestionnaireRepository;
import com.bw.adv.module.activity.repository.QuestionnaireTemplateInfoRepository;
import com.bw.adv.module.activity.repository.QuestionnaireTemplateRepository;
import com.bw.adv.service.activity.service.QuestionService;

@Service
public class QuestionServiceImpl implements QuestionService{
	
	private QuestionnaireRepository questionnaireRepository;
	
	private QuestionnaireTemplateRepository questionnaireTemplateRepository;
	
	private QuestionnaireTemplateInfoRepository questionnaireTemplateInfoRepository;
	
	private QuestionnaireInstanceRepository questionnaireInstanceRepository;

	@Override
	public int insertQuestion(Questionnaire questionnaire) {
		return this.questionnaireRepository.saveSelective(questionnaire);
	}

	@Override
	public List<QuestionnaireTemplate> queryQuestionTemplate(Long questionnaireId,Page<QuestionnaireTemplate> templatePage) {
		return questionnaireTemplateRepository.selectByQuestionnaireId(questionnaireId,templatePage);
	}
	
	@Override
	public void updateQuestion(Questionnaire questionnaire) {
		questionnaireRepository.updateByPkSelective(questionnaire);
	}

	@Override
	public void deleteQuestion(Long questionnaireId) {
		questionnaireRepository.removeByPk(questionnaireId);
	}

	@Override
	public Questionnaire queryNaire(Long questionnaireId) {
		return questionnaireRepository.findByPk(questionnaireId);
	}
	
	@Override
	public List<Questionnaire> selectAllQuestion() {
		return this.questionnaireRepository.findAll();
	}
	
	@Override
	public int saveTemplate(QuestionnaireTemplate template) {
		return questionnaireTemplateRepository.saveSelective(template);
	}
	
	@Override
	public int insertTemplateInfo(List<QuestionnaireTemplateInfo> info) {
		return questionnaireTemplateInfoRepository.saveList(info);
	}
	
	@Override
	public void deleteTemplate(Long questionnaireTemplateId) {
		questionnaireTemplateRepository.removeByPk(questionnaireTemplateId);
	}
	
	@Override
	public QuestionnaireTemplate queryTemplateById(Long templateId) {
		return questionnaireTemplateRepository.findByPk(templateId);
	}
	
	@Override
	public List<QuestionnaireTemplateInfo> queryTemplateInfoByTemplateId(Long templateId) {
		return questionnaireTemplateInfoRepository.queryTemplateInfoByTemplateId(templateId);
	}
	
	@Autowired
	public void setQuestionnaireRepository(QuestionnaireRepository questionnaireRepository) {
		this.questionnaireRepository = questionnaireRepository;
	}

	@Autowired
	public void setQuestionnaireTemplateRepository(QuestionnaireTemplateRepository questionnaireTemplateRepository) {
		this.questionnaireTemplateRepository = questionnaireTemplateRepository;
	}

	@Autowired
	public void setQuestionnaireTemplateInfoRepository(
			QuestionnaireTemplateInfoRepository questionnaireTemplateInfoRepository) {
		this.questionnaireTemplateInfoRepository = questionnaireTemplateInfoRepository;
	}
	
	@Autowired
	public void setQuestionnaireInstanceRepository(QuestionnaireInstanceRepository questionnaireInstanceRepository) {
		this.questionnaireInstanceRepository = questionnaireInstanceRepository;
	}

	@Override
	public void updateTemplate(QuestionnaireTemplate template) {
		this.questionnaireTemplateRepository.updateByPkSelective(template);
	}

	@Override
	public void deleteTemplateInfoByTemplateId(Long templateId) {
		this.questionnaireTemplateInfoRepository.deleteTemplateInfoByTemplateId(templateId);
	}

	@Override
	public List<Questionnaire> selectAllQuestionWithTemplateCount(Page<Questionnaire> templatePage) {
		return this.questionnaireRepository.selectAllQuestionWithTemplateCount(templatePage);
	}

	@Override
	public List<QuestionnaireTemplate> queryQuestionTemplateNoPage(Long questionnaireId) {
		return this.questionnaireTemplateRepository.queryQuestionTemplateNoPage(questionnaireId);
	}

	@Override
	public List<QuestionCount> selectResultCountForSelect(Long questionnaireId) {
		return this.questionnaireTemplateInfoRepository.selectResultCountForSelect(questionnaireId);
	}

	@Override
	public List<QuestionCount> selectResultCountForMark(Long questionnaireId) {
		return this.questionnaireTemplateInfoRepository.selectResultCountForMark(questionnaireId);
	}

	@Override
	public List<QuestionCount> selectResultCountForQa(Long questionnaireId) {
		return this.questionnaireTemplateInfoRepository.selectResultCountForQa(questionnaireId);
	}

	@Override
	public Long selectCountInfoForMember(Long questionnaireId) {
		return this.questionnaireTemplateInfoRepository.selectCountInfoForMember(questionnaireId);
	}

	@Override
	public Questionnaire findActiveQuestion() {
		return questionnaireRepository.findActiveQuestion();
	}

	@Override
	public Long queryActiveQuestionAmount() {
		return questionnaireRepository.queryActiveQuestionAmount();
	}

	@Override
	public void saveQuestionInstanceList(List<QuestionnaireInstance> instanceList) {
		questionnaireInstanceRepository.saveList(instanceList);
	}

}
