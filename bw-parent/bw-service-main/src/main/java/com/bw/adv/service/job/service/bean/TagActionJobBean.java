package com.bw.adv.service.job.service.bean;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.service.activity.service.TagActionService;

public class TagActionJobBean extends BaseJob {
	
	private TagActionService tagActionService;
	
	@Override
	public void excute() throws Exception {
		initService();
		try {
			tagActionService.scanTagAction();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("打标签任务执行完成");
	}
	
	private void initService() {
		this.tagActionService = this.getApplicationContext().getBean(TagActionService.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.MEMBER_TAG_JOB_BEAN;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

}
