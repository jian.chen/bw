package com.bw.adv.service.coupon.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.CouponInstanceSearch;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;

/**
 * ClassName: CouponInstanceService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 下午2:28:09 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponInstanceService extends BaseService<CouponInstance> {
	
	/**
	 * queryListByDyc:根据条件筛选查询优惠券实例列表<br/>
	 * Date: 2015年8月14日 下午2:32:38 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param mobile
	 * @param getTime
	 * @param usedTime
	 * @param couponCode
	 * @return
	 */
	List<CouponInstanceExp> queryListByDyc(CouponInstanceSearch couponInstanceSearch,Page<CouponInstanceExp> page);
	
	/**
	 * 重载上面的方法，获得不分页结果
	 * @param couponInstanceSearch
	 * @return
	 */
	List<CouponInstanceExp> queryListByDyc(CouponInstanceSearch couponInstanceSearch);
	
	/**
	 * queryListByDyc:根据条件筛选查询优惠券实例 <br/>
	 * Date: 2015年12月24日 下午3:22:29 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	List<CouponInstanceExp> queryListByDyc(Example example);
	
	/**
	 * queryCountByDyc:根据条件查询优惠券实例数量 <br/>
	 * Date: 2015年8月26日 上午11:44:33 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceSearch
	 * @return
	 */
	int queryCountByDyc(CouponInstanceSearch couponInstanceSearch);
	
	/**
	 * queryCoupontByCode:根据优惠券实例编码查询实例<br/>
	 * Date: 2015年8月27日 上午11:59:58 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	CouponInstance queryCoupontByCode(String couponInstanceCode);
	
	/**
	 * queryExpByCode:(查询优惠券扩展信息，根据编号). <br/>
	 * Date: 2015-11-17 下午5:07:00 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	CouponInstanceExp queryExpByCode(String couponInstanceCode);
	
	/**
	 * freezeCouponInstanceByCode:(冻结优惠券实例). <br/>
	 * Date: 2015-12-28 上午10:54:19 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @param storeCode
	 * @return
	 */
	CouponInstanceExp freezeCouponInstanceByCode(String couponInstanceCode,String storeCode);

	/**
	 * queryCouponInstanceExpByExample:(根据示例查询优惠券信息). <br/>
	 * Date: 2015-11-10 下午4:31:58 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	CouponInstanceExp queryCouponInstanceExpByExample(Example example);
	
	
	/**
	 * queryActiveListByMemberId:(查询会员可用的优惠券). <br/>
	 * Date: 2015-11-17 下午4:26:01 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<CouponInstanceExp> queryActiveListByMemberId(Long memberId);
	
	/**
	 * insertCouponIssue:(发放优惠券给会员). <br/>
	 * Date: 2015年12月15日 下午5:04:19 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberId
	 * @param couponList
	 * @param sourceId
	 * @param externalId
	 * @throws Exception
	 */
	void insertCouponInstanceUpdate(Long memberId,List<CouponInstance> couponInstanceList);

	/**
	 * 按照门店和日期查询核销记录用于air助手
	 * queryShopCheckout:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param storeCode
	 * @param useDate
	 * @return
	 */
	List<CouponInstanceExp> queryShopCheckout(String storeCode, String useDate);

	/**
	 * 给实例券分配对应会员
	 * @param memberId
	 * @param exchangeCode
	 */
	void updateCouponInstanceByExchangeCode(Long memberId , String exchangeCode);
}
