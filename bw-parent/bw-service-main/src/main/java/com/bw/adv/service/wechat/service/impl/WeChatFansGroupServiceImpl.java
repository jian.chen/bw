/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WeChatFansGroupServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月11日下午6:14:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.api.ApiWeChatFansGroup;
import com.bw.adv.module.wechat.model.WechatFansGroup;
import com.bw.adv.module.wechat.model.WechatFansGroupItem;
import com.bw.adv.module.wechat.repository.WechatFansGroupRepository;
import com.bw.adv.service.wechat.service.WechatFansGroupItemService;
import com.bw.adv.service.wechat.service.WechatFansGroupService;

/**
 * ClassName:WeChatFansGroupServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午6:14:34 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WeChatFansGroupServiceImpl extends BaseServiceImpl<WechatFansGroup> implements WechatFansGroupService {
	protected  Logger logger = Logger.getLogger(WeChatFansGroupServiceImpl.class);
	private WechatFansGroupRepository wechatFansGroupRepository;
	private ApiWeChatFansGroup apiWeChatFansGroup;
	private WechatFansGroupItemService wechatFansGroupItemService;
	// 默认组
	@Value("${WEHCAT_GROUP_DEFAULT}")
	private static  String wechat_group_default;
	// 黑名单
	@Value("${WEHCAT_GROUP_BLACK}")
	private static  String wechat_group_black;
	// 星标组
	@Value("${WECHAT_GROUP_FLAG}")
	private static  String wechat_group_flag;
	
	@Override
	public BaseRepository<WechatFansGroup, ? extends BaseMapper<WechatFansGroup>> getBaseRepository() {
		return wechatFansGroupRepository;
	}
	
	@Override
	public void saveWechatFansGroup(String groupNameString,String groupIdString, SysUser sysUser) {
		WechatFansGroup wechatFansGroup =null;
			wechatFansGroup =new WechatFansGroup();
			if(sysUser!=null){
				wechatFansGroup.setCreateBy(sysUser.getUserId());
			}
			wechatFansGroup.setCreateTime(DateUtils.getCurrentTimeOfDb());
			wechatFansGroup.setGroupId(groupIdString);
			wechatFansGroup.setGroupName(groupNameString);
			wechatFansGroup.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
			wechatFansGroup.setWechatAccountId(Long.valueOf(1));
			this.save(wechatFansGroup);
	}
	

	@Override
	public void batchUpdateFansToGroup(String openIds,String wechatFans, WechatFansGroup wechatFansGroup) {
		String[] openIdString =null;
		JSONArray openArray =null;
		String toGroupId =null;
		Map<String,String> resultMap =null;
		List<String> wechatFansList =null;
		WechatFansGroupItem wechatFansGroupItem =null;
		
		if(!StringUtils.isEmpty(openIds)){
			openIdString = openIds.split(",");
			openArray = JSONArray.fromObject(openIdString);
		}
		if(!StringUtils.isEmpty(wechatFans)){
			wechatFansList = Arrays.asList(wechatFans.split(","));
		}
		if(null!=wechatFansGroup){
			toGroupId = wechatFansGroup.getGroupId();
		}
		resultMap = apiWeChatFansGroup.batchUpdateFansGroup(openArray, toGroupId);
		// 微信更新成功
		if(resultMap.get("errmsg").equals(WechatTypeConstant.WECHAT_SUCESS_CODE.getCode())){
			for(int i=0; i<wechatFansList.size();i++){
				//置为无效
				wechatFansGroupItem =wechatFansGroupItemService.queryWechatFansGroupItemByFansId(Long.valueOf(wechatFansList.get(i)));
				if(null!=wechatFansGroupItem){
					wechatFansGroupItem.setIsDelete(WechatTypeConstant.DELETED.getCode());
					wechatFansGroupItemService.updateByPk(wechatFansGroupItem);
				}
				//新增 
				WechatFansGroupItem wechatFansGroupItemAdd =new WechatFansGroupItem();
				wechatFansGroupItemAdd.setCreateTime(DateUtils.getCurrentTimeOfDb());
				wechatFansGroupItemAdd.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
				wechatFansGroupItemAdd.setWechatFansId(Long.valueOf(wechatFansList.get(i)));
				wechatFansGroupItemAdd.setWechatFansGroupId(wechatFansGroup.getWechatFansGroupId());
				wechatFansGroupItemService.save(wechatFansGroupItemAdd);
			}
		}
	}
	
	
	@Override
	public List<WechatFansGroup> removeWechatDefaultGroup(List<WechatFansGroup> wechatFansGroupList) {
		List<WechatFansGroup> wechatGroupList =null;
		wechatGroupList =new ArrayList<WechatFansGroup>();
		if(wechatFansGroupList.size()>0){
			for(WechatFansGroup wechatFansGroup:wechatFansGroupList){
				if(wechatFansGroup.getGroupId().equals(wechat_group_default)){
					continue;
				}
				if(wechatFansGroup.getGroupId().equals(wechat_group_black)){
					continue;
				}
				if(wechatFansGroup.getGroupId().equals(wechat_group_flag)){
					continue;
				}
				wechatGroupList.add(wechatFansGroup);
			}
		}
		return wechatGroupList;
	}


	@Override
	public void updateWechatFansGroup(String wechatGroupId, String groupName,SysUser sysUser) {
		WechatFansGroup wechatFansGroup =null;
		if(wechatGroupId!=null){
			wechatFansGroup = this.queryByPk(Long.valueOf(wechatGroupId));
			if(wechatFansGroup!=null){
				wechatFansGroup.setUpdateBy(sysUser.getUserId());
				wechatFansGroup.setUpdatedTime(DateUtils.getCurrentTimeStr());
				wechatFansGroup.setGroupName(groupName);
				this.updateByPk(wechatFansGroup);
			}
		}
		
	}
	

	@Override
	public void deleteWechatFansGroup(String wechatGroupId, SysUser sysUser) {
		WechatFansGroup wechatFansGroup =null;
		if(wechatGroupId!=null){
			wechatFansGroup = this.queryByPk(Long.valueOf(wechatGroupId));
			if(wechatFansGroup!=null){
				wechatFansGroup.setUpdateBy(sysUser.getUserId());
				wechatFansGroup.setIsDelete(WechatTypeConstant.DELETED.getCode());
				wechatFansGroup.setUpdatedTime(DateUtils.getCurrentTimeOfDb());
				this.updateByPk(wechatFansGroup);
			}
		}
		
	}
	
	@Override
	public WechatFansGroup queryWechatFansGroup(String groupId) {
		return wechatFansGroupRepository.findWechatFansGroup(groupId);
	}
	
	@Autowired
	public void setWechatFansGroupRepository(
			WechatFansGroupRepository wechatFansGroupRepository) {
		this.wechatFansGroupRepository = wechatFansGroupRepository;
	}

	@Override
	public List<WechatFansGroup> queryAllGroups() {
		return wechatFansGroupRepository.findAllGroups();
	}
	
	@Autowired
	public void setApiWeChatFansGroup(ApiWeChatFansGroup apiWeChatFansGroup) {
		this.apiWeChatFansGroup = apiWeChatFansGroup;
	}

	@Autowired
	public void setWechatFansGroupItemService(
			WechatFansGroupItemService wechatFansGroupItemService) {
		this.wechatFansGroupItemService = wechatFansGroupItemService;
	}



	
}

