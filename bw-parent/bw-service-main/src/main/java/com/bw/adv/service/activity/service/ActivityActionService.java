/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.ActivityAction;
import com.bw.adv.module.activity.model.ActivityInstance;

/**
 * ClassName: ActivityActionService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-28 下午3:06:23 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface ActivityActionService extends BaseService<ActivityAction>{

	
	/**
	 * scanActivityAction:(扫描活动动作). <br/>
	 * Date: 2015-9-22 上午11:27:32 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	void scanActivityAction();
	
	/**
	 * scanActivityActionTask:(扫描活动任务动作). <br/>
	 * Date: 2015-9-22 上午11:32:55 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	void scanActivityActionTask();
	
	/**
	 * ActivityActionMemberBirthday:(会员生日活动). <br/>
	 * Date: 2015-9-13 上午11:25:54 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param instance 
	 */
	
	void addActivityActionMemberBirthday(ActivityInstance instance);
	
	/**
	 * addActivityActionMemberWake:(会员唤醒活动). <br/>
	 * Date: 2015年9月16日 下午6:04:28 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param instance
	 */
	void addActivityActionMemberWake(ActivityInstance instance);
	
	/**
	 * addActivityActionSpecialDate:(指定日期奖励). <br/>
	 * Date: 2015年9月17日 下午4:52:29 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param instance
	 */
	void addActivityActionSpecialDate(ActivityInstance instance);
	
	/**
	 * 
	 * checkActivityDate:(判断活动是否到期). <br/>
	 * Date: 2015年9月28日 下午12:01:33 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 */
	void checkActivityDate();
}

