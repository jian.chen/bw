/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.Activity;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.model.PhotoScoreResult;
import com.bw.adv.module.activity.model.exp.ActivityInstanceRecordExp;
import com.bw.adv.module.activity.model.exp.ActivityRuleConditionExp;
import com.bw.adv.module.activity.repository.ActivityInstanceRecordRepository;
import com.bw.adv.module.activity.repository.ActivityInstanceRepository;
import com.bw.adv.module.activity.repository.ActivityRepository;
import com.bw.adv.module.activity.repository.PhotoScoreResultRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.rule.constant.ConditionsConstant;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.component.rule.model.Conditions;
import com.bw.adv.module.component.rule.model.ConditionsValue;
import com.bw.adv.module.component.rule.repository.ConditionsValueRepository;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.repository.CouponRepository;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.ActivityService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * ClassName: ActivityServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-27 下午3:17:07 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Service
public class ActivityServiceImpl extends BaseServiceImpl<Activity> implements ActivityService{
	
	private static final String SAVE = "1";
	private static final String UPDATE = "2";

	private ActivityRepository activityRepository;
	private ActivityInstanceRepository activityInstanceRepository;
	private ActivityInstanceRecordRepository activityInstanceRecordRepository;
	private ConditionsValueRepository conditionsValueRepository;
	private RuleInit ruleInit;
	private PhotoScoreResultRepository photoScoreResultRepository;
	private CouponRepository couponRepository;
	
	@Override
	public BaseRepository<Activity, ? extends BaseMapper<Activity>> getBaseRepository() {
		return activityRepository;
	}

	
	/**
	 * findAllActive:查询所有活动<br/>
	 * Date: 2015-8-11 下午2:42:37 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	public List<Activity> queryAll() {
		return activityRepository.findAllActive();
	}
	
	public List<Activity> queryByTypeId(Long typeId) {
		return activityRepository.findActiveByTypeId(typeId);
	}
	
	/**
	 * TODO 发起活动 
	 * @throws Exception 
	 * @see com.bw.adv.service.activity.service.ActivityService#startActivity(net.sf.json.JSONObject, com.bw.adv.module.sys.model.SysUser)
	 */
	@Override
	@Transactional
	public void startActivity(JSONObject jsonObject,SysUser sysUser) throws Exception {
			
			saveOrUpdateActivity(SAVE,jsonObject,sysUser);
	}
	
	/**
	 * TODO 修改活动（可选）.
	 * @throws Exception 
	 * @see com.bw.adv.service.activity.service.ActivityService#updateActivity(net.sf.json.JSONObject, com.bw.adv.module.sys.model.SysUser)
	 */
	@Transactional
	public void updateActivity(JSONObject jsonObject,SysUser sysUser) throws Exception {
		saveOrUpdateActivity(UPDATE,jsonObject,sysUser);
	}
	
	/**
	 * TODO 根据活动实例ID查询活动实例记录，分页.
	 * @see com.bw.adv.service.activity.service.ActivityService#queryActivityInstanceRecordByInstanceId(java.lang.Long, com.bw.adv.core.utils.Page)
	 */
	@Override
	public List<ActivityInstanceRecordExp> queryActivityInstanceRecordByInstanceId(Long activityInstanceId, Page<ActivityInstanceRecordExp> page){
		return activityInstanceRecordRepository.findByActivityInstanceId(activityInstanceId, page);
	}
	

	@Override
	public List<ActivityInstanceRecordExp> queryActivityInstanceRecordByExample(
			Example example, Page<ActivityInstanceRecordExp> page) {
		
		return activityInstanceRecordRepository.findByExample(example, page);
	}
	
	/**
	 * TODO 查询活动所需的条件（可选）.
	 * @see com.bw.adv.service.activity.service.ActivityService#queryActivityConditionByInstanceId(java.lang.Long)
	 */
	@Override
	public List<ActivityRuleConditionExp> queryActivityConditionByInstanceId(Long instanceId) {
		return activityInstanceRepository.findActivityInstanceExpByInstanceId(instanceId);
	}
	
	
	/**
	 * TODO 根据主键查询活动实例
	 * @see com.bw.adv.service.activity.service.ActivityService#queryActivityInstanceByInstanceId(java.lang.Long)
	 */
	@Override
	public ActivityInstance queryActivityInstanceByInstanceId(Long instanceId) {
		return activityInstanceRepository.findByPk(instanceId);
	}

	/**
	 * TODO 查询某个活动的活动实例，分页
	 * @see com.bw.adv.service.activity.service.ActivityService#queryActivityInstanceByActivityId(java.lang.Long, com.bw.adv.core.utils.Page)
	 */
	@Override
	public List<ActivityInstance> queryActivityInstanceByActivityId(Long activityId,Page<ActivityInstance> page) {
		return activityInstanceRepository.findByActivityId(activityId,page);
	}
	
	/**
	 * TODO 查询某个活动的活动实例
	 * @see com.bw.adv.service.activity.service.ActivityService#queryActivityInstanceByActivityId(java.lang.Long)
	 */
	@Override
	public List<ActivityInstance> queryActivityInstanceByActivityId(Long activityId) {
		return activityInstanceRepository.findByActivityId(activityId);
	}
	
	/**
	 * saveOrUpdateActivity:(新增或修改活动实例). <br/>
	 * Date: 2015-9-3 下午6:34:51 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param saveOrUpdate
	 * @param jsonObject
	 * @param sysUser
	 */
	private void saveOrUpdateActivity(String saveOrUpdate,JSONObject jsonObject,SysUser sysUser) throws Exception {
		Long activityInstanceId = null;
		Activity activity = null;
		String activityCode = null;
		ActivityInstance activityInstance = null;
		ConditionsValue conditionsValue = null;
		Map<String, Conditions> conMap = null;
		List<ConditionsValue> conditionsValueList = null;
		String templateType = null;
		JSONObject conditionJson = null;
		String thruDate = null;
		String res = "";
		
		activityCode = jsonObject.getString("activityCode");
		
		conditionJson = JSONObject.fromObject(jsonObject.get("conditionMap"));
		
		//得到活动对应的条件列表
		conMap = RuleInit.getActivityConditions(activityCode);
		if(conMap.get(ConditionsConstant.GIVE_COUPONS.getCode().toString()) != null){
			if(conditionJson.get(ConditionsConstant.GIVE_COUPONS.getCode().toString()) != null){
				String couponTemp =  conditionJson.get(ConditionsConstant.GIVE_COUPONS.getCode().toString()).toString();
				JSONObject couponTempJson = JSONObject.fromObject(couponTemp);
				JSONArray couponIds = couponTempJson.names();

				List<Coupon> couList = couponRepository.findListByIds(couponIds.toString().replace("\"", "'").replace("[", "(").replace("]", ")"));
				
				
				//判断保存的券有没有过期or作废or删除
				if(couList.size() > 0){
					for(Coupon couTemp : couList){
						if(couTemp.getStatusId() != null){
							if(StatusConstant.COUPON_EXPIRE.getId().equals(couTemp.getStatusId())){
								res += couTemp.getCouponName() + StatusConstant.COUPON_EXPIRE.getName();
							}else if(StatusConstant.COUPON_DESTROY.getId().equals(couTemp.getStatusId())){
								res += couTemp.getCouponName() + StatusConstant.COUPON_DESTROY.getName();
							}else if(StatusConstant.COUPON_DELETE.getId().equals(couTemp.getStatusId())){
								res += couTemp.getCouponName() + StatusConstant.COUPON_DELETE.getName();
							}else{
								if(couTemp.getExpireType() != null){
									if("1".equals(couTemp.getExpireType())){
										if(DateUtils.getCurrentDateOfDb().compareTo(couTemp.getThruDate()) > 0){
											res += couTemp.getCouponName()+"已过期！";
										}
									}
								}
							}
						}
					}
				}
				
				if(!"".equals(res)){
					throw new MemberException(res);
				}
			}
			
		}
		conditionsValueList = new ArrayList<ConditionsValue>();
		activity = RuleInit.getActivityByActivityCode(activityCode);//得到活动对象
		
		if(jsonObject.get("templateType") != null){
			if(jsonObject.get("templateType").toString().split(",").length > 1){
				templateType = "both";
			}else{
				templateType = jsonObject.get("templateType").toString();
			}
		}
		
		//新增活动实例
		activityInstance = new ActivityInstance();
		activityInstance.setActivityInstanceId(activityInstanceId);
		activityInstance.setActivityId(activity.getActivityId());
		activityInstance.setActivityInstanceName(jsonObject.get("activityInstanceName")+"");
		activityInstance.setFromDate(jsonObject.get("fromDate").toString().replace("-", ""));
		activityInstance.setValidityType(jsonObject.get("validityType")+"");
		activityInstance.setValidityValue(jsonObject.get("validityValue") == null ? null : jsonObject.get("validityValue").toString().replace("-", ""));
		//结束时间
		if("fixed".equals(jsonObject.get("validityType").toString())){
			activityInstance.setThruDate(jsonObject.get("validityValue").toString().replace("-", ""));
		}else if("period".equals(jsonObject.get("validityType").toString())){
			//获取开始时间之后N天的日期
			thruDate = DateUtils.calcDays(DateUtils.formatString(jsonObject.get("fromDate").toString()), Integer.parseInt(jsonObject.get("validityValue").toString()));
			activityInstance.setThruDate(thruDate);
		}else if("forever".equals(jsonObject.get("validityType").toString())){
			activityInstance.setThruDate("");
		}
		activityInstance.setTemplateType(templateType);
		activityInstance.setTemplateUrl(jsonObject.get("templateUrl")+"");
		activityInstance.setCreateTime(DateUtils.getCurrentTimeOfDb());
		activityInstance.setActivityInstanceDesc(jsonObject.get("activityInstanceDesc")+"");
		
		if(saveOrUpdate.equals(SAVE)){
			activityInstance.setStatusId(StatusConstant.ACTIVITY_INSTANCE_DRAFT.getId());
			activityInstance.setCreateBy(sysUser.getUserId());
			activityInstance.setCreateTime(DateUtils.getCurrentTimeOfDb());
			
			activityInstanceRepository.saveSelective(activityInstance);
		}else if(saveOrUpdate.equals(UPDATE)){
			activityInstanceId = jsonObject.getLong("activityInstanceId");
			conditionsValueRepository.removeByActivityInstanceId(activityInstanceId);
			activityInstance.setActivityInstanceId(activityInstanceId);
			activityInstance.setUpdateBy(sysUser.getUserId());
			activityInstance.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			activityInstanceRepository.updateByPkSelective(activityInstance);
		}
		
		
		
		
		//添加活动对应的条件值
		for (String code : conMap.keySet()) {
			//设置条件值
			if(conditionJson.get(code) != null && StringUtils.isNotBlank(conditionJson.get(code).toString())){
				//新增规则条件值
				conditionsValue = new ConditionsValue();
				conditionsValue.setConditionsValueId(null);
				conditionsValue.setConditionsId(ConditionsConstant.getConstantByCode(ConditionsConstant.class, code).getConditionsId());
				conditionsValue.setActivityInstanceId(activityInstance.getActivityInstanceId());
				conditionsValue.setConditionsValues(conditionJson.get(code).toString());
				conditionsValueList.add(conditionsValue);
			}
		}
		if(conditionsValueList.size() > 0){
			conditionsValueRepository.saveList(conditionsValueList);
		}
		//重新加载初始化条件
		ruleInit.reLoad();
	}
	

	@Override
	public void updateInstanceStatus(SysUser sysUser,Long activityInstanceId) {
		ActivityInstance activityInstance = null;
		
		activityInstance = new ActivityInstance();
		activityInstance.setActivityInstanceId(activityInstanceId);
		activityInstance.setStatusId(StatusConstant.ACTIVITY_INSTANCE_BREAK.getId());
		activityInstance.setUpdateBy(sysUser.getUserId());
		activityInstance.setUpdateTime(DateUtils.getCurrentTimeOfDb());
		activityInstance.setBreakTime(DateUtils.getCurrentDateOfDb());
		activityInstanceRepository.updateByPkSelective(activityInstance);
		
		//重新加载初始化条件
		ruleInit.reLoad();
	}
	
	@Override
	public void savePhotoScoreResult(PhotoScoreResult photoScoreResult) {
		this.photoScoreResultRepository.saveSelective(photoScoreResult);
	}
	
	@Autowired
	public void setConditionsValueRepository(ConditionsValueRepository conditionsValueRepository) {
		this.conditionsValueRepository = conditionsValueRepository;
	}

	@Autowired
	public void setActivityRepository(ActivityRepository activityRepository) {
		this.activityRepository = activityRepository;
	}

	@Autowired
	public void setActivityInstanceRepository(ActivityInstanceRepository activityInstanceRepository) {
		this.activityInstanceRepository = activityInstanceRepository;
	}

	@Autowired
	public void setActivityInstanceRecordRepository(ActivityInstanceRecordRepository activityInstanceRecordRepository) {
		this.activityInstanceRecordRepository = activityInstanceRecordRepository;
	}

	@Autowired
	public void setRuleInit(RuleInit ruleInit) {
		this.ruleInit = ruleInit;
	}

	@Autowired
	public void setPhotoScoreResultRepository(PhotoScoreResultRepository photoScoreResultRepository) {
		this.photoScoreResultRepository = photoScoreResultRepository;
	}

	@Autowired
	public void setCouponRepository(CouponRepository couponRepository) {
		this.couponRepository = couponRepository;
	}
	
	

}

