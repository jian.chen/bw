/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:AwardsSettingServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015年11月23日下午2:02:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.AwardsSetting;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;
import com.bw.adv.module.activity.repository.AwardsSettingRepository;
import com.bw.adv.service.activity.service.AwardsSettingService;

/**
 * ClassName:AwardsSettingServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:02:35 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class AwardsSettingServiceImpl extends BaseServiceImpl<AwardsSetting> implements AwardsSettingService {
	
	private AwardsSettingRepository awardsSettingRepository;
	
	@Override
	public BaseRepository<AwardsSetting, ? extends BaseMapper<AwardsSetting>> getBaseRepository() {
		return awardsSettingRepository;
	}
	
	
	@Override
	public List<AwardsSettingExp> queryAwardsSettingExpByActivityId(Long activityId) {
		return awardsSettingRepository.findAwardsSettingExpByActivityId(activityId);
	}
	@Override
	public AwardsSettingExp queryAwardsSettingExpByAwardsId(Long awardsId) {
		return awardsSettingRepository.findAwardsSettingExpByAwardsId(awardsId);
	}
	
	@Override
	public List<AwardsSetting> queryAwardsSettingsByActivityId(Long activityId) {
		return awardsSettingRepository.findAwardsSettingsByActivityId(activityId);
	}
	
	
	@Autowired
	public void setAwardsSettingRepository(
			AwardsSettingRepository awardsSettingRepository) {
		this.awardsSettingRepository = awardsSettingRepository;
	}


}

