/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:WechatNewsArticles.java
 * Package Name:com.sage.scrm.bk.wechat.model
 * Date:2015年11月11日下午6:00:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.msg.model;
/**
 * ClassName:WechatNewsArticles <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月11日 下午6:00:54 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class WechatNewsArticles {
    private String title;
	private String description;
	private String url;
	private String picurl;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	
	
}

