package com.bw.adv.service.coupon.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.constant.GiftConstant;
import com.bw.adv.module.common.constant.LotteryConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.constant.UnitConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.coupon.exception.CouponException;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.CouponIssue;
import com.bw.adv.module.coupon.model.CouponIssueChannel;
import com.bw.adv.module.coupon.model.ExternalCoupon;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.coupon.model.exp.CouponResult;
import com.bw.adv.module.coupon.repository.CouponInstanceRepository;
import com.bw.adv.module.coupon.repository.CouponIssueChannelRepository;
import com.bw.adv.module.coupon.repository.CouponIssueRepository;
import com.bw.adv.module.coupon.repository.CouponRepository;
import com.bw.adv.module.coupon.repository.ExternalCouponRepository;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberGift;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.RandomUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;
import com.bw.adv.module.wechat.repository.WechatMsgQueueRepository;
import com.bw.adv.service.badge.service.MemberBadgeItemSevice;
import com.bw.adv.service.coupon.service.CouponIssueService;
import com.bw.adv.service.coupon.service.CouponService;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.order.service.MemberGiftService;


/**
 * ClassName: CouponIssueServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 下午2:29:48 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Service
public class CouponIssueServiceImpl extends BaseServiceImpl<CouponIssue> implements CouponIssueService {
	
	private static final Logger logger =LoggerFactory.getLogger(CouponIssueServiceImpl.class);
//	private static final String couponUrl = PropertiesConfInit.getString("webConfig", "crm_coupon_url");
	@Value("${crm_coupon_issue_num}")
	private static String couponIssueNumber;
	private static final String OPT_TYPE_SINGLE = "single";
	private static final String OPT_TYPE_BATCH = "batch";
	
	
	private CouponIssueRepository couponIssueRepository;
	
	private CouponInstanceRepository couponInstanceRepository;
	
	private CouponRepository couponRepository;

	private ExternalCouponRepository externalCouponRepository;
	
	@Autowired
	private MemberBadgeItemSevice memberBadgeItemSevice;

	private CodeService codeService;
	
	private CouponIssueChannelRepository couponIssueChannelRepository;
	
	private MemberService memberService;
	
	private WechatMsgQueueRepository wechatMsgQueueRepository;
	
	private WechatMsgQueueHRepository wechatMsgQueueHRepository;
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private MemberGiftService memberGiftService;
	
	@Override
	public List<CouponIssue> queryList(Long couponId,Page<CouponIssue> page) {
		
		return couponIssueRepository.findCouponIssueList(couponId,page);
	}
	

	@Override
	public BaseRepository<CouponIssue, ? extends BaseMapper<CouponIssue>> getBaseRepository() {
		
		return couponIssueRepository;
	}
	
	@Transactional
	public CouponInstance getMemberBadgeGiftCoupon(Long memberId,Long couponId){
		
		Example example=new Example();
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("STATUS_ID", StatusConstant.COUPON_INSTANCE_UNUSED.getId()).andEqualTo("COUPON_ID", couponId);
		CouponInstance couponInstance = couponInstanceRepository.findCouponInstanceExpByStatusByLimit(example);
		//如果等于空，现有库存没了，没办法兑换
		if(couponInstance==null){
			
			return null;
		}
		
		memberBadgeItemSevice.updateMemberBadgeGiftExchange(memberId);
		couponInstance.setCouponInstanceOwner(memberId);
		couponInstance.setCouponInstanceUser(memberId);
		couponInstance.setGetTime(DateUtils.getCurrentTimeOfDb());
		couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_RECEIVE.getId());
		couponInstanceRepository.updateByPkSelective(couponInstance);
		return couponInstance;
	}
	@Transactional
	public CouponInstance getLotteryGift(Long memberId,Prize prize,AwardsSettingExp awardsSettingExp){
		
		Coupon coupon = couponService.queryByPk(Long.parseLong(prize.getPrizeCode()));
		Example example=new Example();
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("STATUS_ID", StatusConstant.COUPON_INSTANCE_UNUSED.getId()).andEqualTo("COUPON_ID", coupon.getCouponId());
		CouponInstance couponInstance = couponInstanceRepository.findCouponInstanceExpByStatusByLimit(example);
		//如果等于空，现有库存没了，没办法兑换
		if(couponInstance==null){
			saveOrUpdateMemberGiftRecord(couponInstance, memberId, coupon,awardsSettingExp);
			return null;
		}
		memberBadgeItemSevice.updateMemberBadgeGiftExchange(memberId);
		couponInstance.setCouponInstanceOwner(memberId);
		couponInstance.setCouponInstanceUser(memberId);
		couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_RECEIVE.getId());
		couponInstanceRepository.updateByPkSelective(couponInstance);
		saveOrUpdateMemberGiftRecord(couponInstance, memberId, coupon,awardsSettingExp); //如果成功了保存，失败了也要保存记录
		return couponInstance;
	}
	
	private int saveOrUpdateMemberGiftRecord(CouponInstance couponInstance,Long memberId, Coupon coupon,AwardsSettingExp awardsSettingExp) {
		//如果领取成功，增加一次记录
		MemberGift memberGift=new MemberGift();
		if(couponInstance==null){
			memberGift.setMemberId(memberId);
			memberGift.setGiftType("2");//1-实物奖品 2，兑换券
			memberGift.setGiftUnit(UnitConstant.ZHANG.getName());
			memberGift.setCouponId(coupon.getCouponId());
			memberGift.setGiftName(coupon.getCouponName());
			memberGift.setChannelId(ChannelConstant.GET_COUPON_LOTTERY.getId());//徽章兑换
			memberGift.setGiftStatus(GiftConstant.GIFT_STATUS_NOT_RECEIVE.getId()); //未领取
			memberGift.setCreateTime(DateUtils.getCurrentDateOfDb());
			memberGift.setGiftCode(awardsSettingExp.getAwardsCode());
			return memberGiftService.save(memberGift);
		}else{
			memberGift.setMemberId(memberId);
			memberGift.setGiftType("2");//1-实物奖品 2，兑换券
			memberGift.setGiftUnit(UnitConstant.ZHANG.getName());
			memberGift.setCouponId(coupon.getCouponId());
			memberGift.setGiftName(coupon.getCouponName());
			memberGift.setGiftCode(awardsSettingExp.getAwardsCode());//优酷
			memberGift.setChannelId(ChannelConstant.GET_COUPON_LOTTERY.getId());//徽章兑换
			memberGift.setCouponInstanceId(couponInstance.getCouponInstanceId());
			memberGift.setGiftStatus(GiftConstant.GIFT_STATUS_YES_RECEIVE.getId()); //已领取
			return memberGiftService.save(memberGift);
		}
	}

	@Override
	public CouponIssue queryByPk(Long couid) {
		
		return couponIssueRepository.findCouIssByPk(couid);
	}
	public Long[] strToLongArr(String[] str, int start, int size){
		Long[] longArr = new Long[size];
		for(int i=start;i<start + size;i++){
			longArr[i - start] = Long.parseLong(str[i]);
		}
		return longArr;
	}
	@Transactional
	public void insertFixedNumCouponInstance(Integer couponNum,String type,Long[] memberIds,Coupon coupon,String startDate,
			String currentTime,String expireDate,Long sourceId,CouponIssue couponIssue, int sendTemp) throws Exception{
		List<CouponInstance> couponInstances = null;
		String startDateF = DateUtils.dateStrToNewFormat(startDate, DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
		String expireDateF = DateUtils.dateStrToNewFormat(expireDate, DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
		List<String> couponInstancecodes = codeService.generateCode(CouponInstance.class, couponNum);
			couponInstances = new ArrayList<CouponInstance>();
			List<WechatMsgQueueH> wechatMsgQueueHs = new ArrayList<WechatMsgQueueH>();
			for(int j = 0;j < couponNum;j++){
				CouponInstance couponInstance = new CouponInstance();
				couponInstance.setCouponId(coupon.getCouponId());
				couponInstance.setOrgId(coupon.getOrgId());
				couponInstance.setCouponInstanceCode(couponInstancecodes.get(j));
				couponInstance.setCreateTime(currentTime);
				couponInstance.setUpdateTime(currentTime);
				couponInstance.setGetTime(currentTime);
				couponInstance.setStartDate(startDate);
				couponInstance.setExpireDate(expireDate);
				couponInstance.setGetChannelId(sourceId);
				if(type.equals("2")){
					JSONObject json = new JSONObject();
					Member member = memberService.queryByPk(memberIds[j]);
					json.put("couponName", coupon.getCouponName());
					json.put("startDate", startDateF);
					json.put("expireDate", expireDateF);
					json.put("comments", coupon.getComments());
					json.put("memberName",member.getMemberName());
					if (sendTemp == 1) {
						WechatMsgQueueH wechatMsgQueueH = new WechatMsgQueueH();
						wechatMsgQueueH.setBusinessType(WechatMsgBusinessTypeEnum.GET_COUPON_HANDLE.getId());
						wechatMsgQueueH.setCreateTime(currentTime);
						wechatMsgQueueH.setMemberId(memberIds[j]);
						wechatMsgQueueH.setMsgContent(json.toString());
						wechatMsgQueueH.setMsgKey(couponInstancecodes.get(j));
						wechatMsgQueueH.setIsSend("R");
						wechatMsgQueueHs.add(wechatMsgQueueH);
					}
					couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_FFING.getId());
					couponInstance.setCouponInstanceOwner(memberIds[j]);
					couponInstance.setCouponInstanceUser(memberIds[j]);
				}else{
					couponInstance.setExchangeCode(RandomUtils.generateShortUuid());
					couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_CREATE.getId());
				}
				couponInstance.setCouponIssueId(couponIssue.getCouponIssueId());
				couponInstances.add(couponInstance);
			}
			if(!wechatMsgQueueHs.isEmpty()){
				wechatMsgQueueHRepository.saveList(wechatMsgQueueHs);
			}
			couponInstanceRepository.saveList(couponInstances);
			//修改优惠券
			coupon.setAvailableQuantity(coupon.getAvailableQuantity()-couponNum);
			if(coupon.getFirstPublishTime()==null || coupon.getFirstPublishTime().isEmpty()){
				coupon.setFirstPublishTime(DateUtils.getCurrentTimeOfDb());
			}
			coupon.setLastPublishTime(DateUtils.getCurrentTimeOfDb());
			if(coupon.getIssueQuantity()==null){
				coupon.setIssueQuantity((long)couponNum);
			}else{
				coupon.setIssueQuantity(coupon.getIssueQuantity()+couponNum);
			}
			couponRepository.updateByPkSelective(coupon);
	}
	
	
	@Override
	public void insertCouponIssue(Long couponId, Long sourceId,Long couponIssueNum, String memberIds,int personalAmount,MemberSearch search, int sendTemp) throws Exception {
		CouponIssue couponIssue = null;
		Coupon coupon = null;
		CouponIssueChannel couponIssueChannel = null;
		String[] memeberids = null;
		//List<MemberExp> memberList = null;
		String startDate = null;
		String expireDate = null;
		String currentTime = null;
		Integer times = null;
		String type = null;
		Long[] memberIdArr = null;
		int startPos = 0;
		//Long[] perArr = null;

		times = Integer.parseInt(couponIssueNumber);//每批次发放数量
		currentTime = DateUtils.getCurrentTimeOfDb();
		coupon = couponRepository.findByPk(couponId);
		coupon.setStatusId(StatusConstant.COUPON_ISSUEDING.getId());
		couponRepository.updateByPkSelective(coupon);
		//生成优惠券发布记录
		couponIssue = new CouponIssue();
		couponIssue.setCouponId(coupon.getCouponId());
		couponIssue.setOrgId(coupon.getOrgId());
		couponIssue.setStatusId(StatusConstant.COUPON_ISSUE_COMPLETE.getId());
		couponIssue.setQuantity(couponIssueNum);
		couponIssue.setCreateDate(DateUtils.getCurrentDateOfDb());
		couponIssue.setIssueDate(DateUtils.getCurrentDateOfDb());
		couponIssue.setCreateTime(DateUtils.getCurrentTimeOfDb());
		couponIssueRepository.save(couponIssue);
		//生成优惠券推送渠道
		couponIssueChannel = new CouponIssueChannel();
		couponIssueChannel.setChannelId(sourceId);
		couponIssueChannel.setCouponIssueId(couponIssue.getCouponIssueId());
		couponIssueChannelRepository.insertCouponIssueChannel(couponIssueChannel);
		//生成优惠券实例
		if(coupon.getExpireType().equals("1")){
			startDate = coupon.getFromDate();
			expireDate = coupon.getThruDate();
		}else if(coupon.getExpireType().equals("2")){
			startDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate()));
			expireDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate())+Integer.parseInt(coupon.getExpireValue()));
		}
		
		try {
			int onlyone = (int)(couponIssueNum/personalAmount/times);//批次数量
			Long lastId = 0L;
			for(int i = 0;i <= onlyone;i++){
				startPos = i * times;
				if( i == onlyone){
					Integer num = (int) ((couponIssueNum/personalAmount)%times);
					if( num > 0){
						times = num;
					}
				}
				if(memberIds.equals("NONE")){
					type = "1";
				}else if(memberIds.equals("ALL")){
					type = "2";
					search = new MemberSearch();
					search.setEqualStatusId(StatusConstant.MEMBER_ACTIVATION.getId());
					memberIdArr = this.structMemberArr(search, times.longValue(), lastId, memberIds);
					if(memberIdArr.length > 0){
						lastId = memberIdArr[memberIdArr.length-1];
					}
				}else if(memberIds.equals("SOME")){
					type = "2";
					memberIdArr = this.structMemberArr(search, times.longValue(), lastId, memberIds);
					if(memberIdArr.length > 0){
						lastId = memberIdArr[memberIdArr.length-1];
					}
				}else {
					type = "2";
					memeberids = memberIds.split(",");
					memberIdArr = strToLongArr(memeberids, startPos, times);
				}
				for(int t = 0;t < personalAmount;t++){
					insertFixedNumCouponInstance(times, type, memberIdArr, coupon, startDate, currentTime, expireDate, sourceId, couponIssue, sendTemp);
				}
			}
			couponInstanceRepository.updateInstanceToOver();
			wechatMsgQueueHRepository.updateQueueToY();
			coupon.setStatusId(StatusConstant.COUPON_ISSUED.getId());
			couponRepository.updateByPkSelective(coupon);
		} catch (Exception e) {
			couponInstanceRepository.deleteInstanceWithIng();
			wechatMsgQueueRepository.deleteQueueWithR();
			e.printStackTrace();
		}
	}
	
	/**
	 * 构建发券会员数组
	 * structMemberArr: <br/>
	 * Date: 2016年5月11日 下午5:05:22 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param search
	 * @param offset
	 * @param lastId
	 * @param type
	 * @return
	 */
	private Long[] structMemberArr(MemberSearch search,Long offset,Long lastId,String type){
		Long[] memberIdArr = null;
		List<MemberExp> memberList = null;
		memberList = new ArrayList<MemberExp>();
		if(type.equals("ALL")){
			memberList = memberService.selectListSearchAutoByOffsetNoCondition(search, offset, lastId);
		}else if(type.equals("SOME")){
			memberList = memberService.selectListSearchAutoByOffset(search, offset, lastId);
		}
		memberIdArr = new Long[memberList.size()];
		for(int i=0;i < memberList.size();i++){
				memberIdArr[i] = memberList.get(i).getMemberId();
		}
		return memberIdArr;
	}
	
	/**
	 * checkCoupon:校验优惠券 <br/>
	 * Date: 2015年9月18日 下午4:01:38 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param coupon
	 * @param couponNum
	 */
	public ResultStatus checkCoupon(Coupon coupon,Integer couponNum) {
		String currentDate = null;
		String expireDate = null;
		//校验券是否存在
		if (coupon == null) {
			logger.error("优惠券发放失败："+ResultStatus.COUPON_CODE_NULL_ERROR.getMsg());
			return ResultStatus.COUPON_CODE_NULL_ERROR;
		}
		//校验券可用数量是否足够
		if(coupon.getTotalQuantity()!=0){
			if(couponNum.compareTo(coupon.getAvailableQuantity().intValue())>0){
				logger.error("优惠券发放失败："+ResultStatus.COUPON_NOT_ENOUGH.getMsg());
				return ResultStatus.COUPON_NOT_ENOUGH;
			}
		}
		//获取券的到期时间
		if(coupon.getExpireType().equals("1")){
			expireDate = coupon.getThruDate();
		}else if(coupon.getExpireType().equals("2")){
			expireDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate())+Integer.parseInt(coupon.getExpireValue()));
		}
		//校验券是否已过期
		currentDate = DateUtils.formatCurrentDate("yyyyMMdd");
		if (currentDate.compareTo(expireDate) > 0) {
			logger.error("优惠券发放失败："+ResultStatus.COUPON_NOT_AVALIABLE.getMsg());
			return ResultStatus.COUPON_NOT_AVALIABLE;
		}
		return ResultStatus.SUCCESS;
	}
	
	@Override
	@Transactional
	@Deprecated
	public void insertCouponIssue(Long memberId, Map<Long, Integer> couponMap,Long sourceId, String externalId) throws Exception {
		CouponIssue couponIssue = null;
		List<CouponInstance> couponInstances = null;
		CouponInstance couponInstance = null;
		Coupon coupon = null;
		Long couponId = null;
		Set<Long> set = null;
		Iterator<Long> iterator = null;
		String startDate = null;
		String expireDate = null;
		String currentTime = null;
		ResultStatus resultStatus = null;
		
		set = couponMap.keySet();         
		iterator = set.iterator(); 
		couponInstances = new ArrayList<CouponInstance>();
		currentTime = DateUtils.getCurrentTimeOfDb();
		while(iterator.hasNext()){ 
			couponId = iterator.next();
			coupon = couponRepository.findByPk(couponId);
			Integer couponNum = couponMap.get(couponId);
			//校验优惠券
			resultStatus = checkCoupon(coupon,couponNum);
			if(!resultStatus.getCode().equals(ResultStatus.SUCCESS.getCode())){
				continue;
			}
			//生成优惠券发布记录
			couponIssue = new CouponIssue();
			couponIssue.setCouponId(couponId);
			couponIssue.setOrgId(coupon.getOrgId());
			couponIssue.setStatusId(StatusConstant.COUPON_ISSUE_COMPLETE.getId());
			couponIssue.setQuantity((long)couponNum);
			couponIssue.setCreateDate(DateUtils.getCurrentDateOfDb());
			couponIssue.setCreateBy(null);//临时
			couponIssue.setIssueDate(DateUtils.getCurrentDateOfDb());
			couponIssue.setCreateTime(DateUtils.getCurrentTimeOfDb());
			couponIssue.setIssueBy(null);//临时
			couponIssueRepository.save(couponIssue);
			//生成优惠券实例
			if(coupon.getExpireType().equals("1")){
				startDate = coupon.getFromDate();
				expireDate = coupon.getThruDate();
			}else if(coupon.getExpireType().equals("2")){
				startDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate()));
				expireDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate())+Integer.parseInt(coupon.getExpireValue()));
			}
			List<String> couponInstancecodes = codeService.generateCode(CouponInstance.class,couponNum);
			System.out.println("for开始"+DateUtils.getCurrentTimeOfDb());
			for(int i = 0;i < couponNum;i++){
				couponInstance = new CouponInstance();
				couponInstance.setCouponId(couponId);
				couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
				couponInstance.setOrgId(coupon.getOrgId());
				couponInstance.setCouponInstanceCode(couponInstancecodes.get(i));
				couponInstance.setCreateTime(currentTime);
				couponInstance.setUpdateTime(currentTime);
				couponInstance.setGetTime(currentTime);
				couponInstance.setStartDate(startDate);
				couponInstance.setExpireDate(expireDate);
				couponInstance.setGetChannelId(sourceId);
				couponInstance.setCouponIssueId(couponIssue.getCouponIssueId());
				couponInstance.setCouponInstanceOwner(memberId);
				couponInstance.setCouponInstanceUser(memberId);
				couponInstance.setExternalId(externalId);
				couponInstances.add(couponInstance);
			}
			//修改优惠券
			coupon.setAvailableQuantity(coupon.getAvailableQuantity()-couponNum);
			System.out.println(coupon.getFirstPublishTime());
			if(coupon.getFirstPublishTime()==null || coupon.getFirstPublishTime().isEmpty()){
				coupon.setFirstPublishTime(DateUtils.getCurrentTimeOfDb());
			}
			coupon.setLastPublishTime(DateUtils.getCurrentTimeOfDb());
			if(coupon.getIssueQuantity()==null){
				coupon.setIssueQuantity((long)couponNum);
			}else{
				coupon.setIssueQuantity(coupon.getIssueQuantity()+couponNum);
			}
			coupon.setPublishBy("waiter");//临时
			coupon.setStatusId(StatusConstant.COUPON_ISSUED.getId());
			couponRepository.updateByPkSelective(coupon);
		}
		System.out.println(DateUtils.getCurrentTimeOfDb()+"插入开始");
		if(couponInstances.size() > 0){
			couponInstanceRepository.saveList(couponInstances);
		}
		System.out.println("插入结束"+DateUtils.getCurrentTimeOfDb());
		
	}
	
	@Override
	@Transactional
	public String insertCouponIssueSingle(Long memberId, Long couponId,Long sourceId, String externalId,int type) throws Exception{
		/*CouponResult couponResult = null;
		List<CouponResult> couponList = null;
		couponResult = new CouponResult();
		couponResult.setCouponId(Long.valueOf(couponId));
		couponResult.setQuantity(1);
		couponList = new ArrayList<CouponResult>();
		couponList.add(couponResult);*/
		CouponIssue couponIssue = null;
		List<CouponInstance> couponInstances = null;
		CouponInstance couponInstance = null;
		Coupon coupon = null;
		String startDate = null;
		String expireDate = null;
		String currentTime = DateUtils.getCurrentTimeOfDb();
		List<CouponInstanceExp> expList = new ArrayList<CouponInstanceExp>();
		CouponInstanceExp exp = null;
		ResultStatus resultStatus = null;
		JSONObject couponJson = null;
		JSONArray couponArray = null;
		coupon = couponRepository.findByPk(couponId);
		if(coupon.getExpireType().equals("1")){
			startDate = coupon.getFromDate();
			expireDate = coupon.getThruDate();
		}else if(coupon.getExpireType().equals("2")){
			startDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate()));
			expireDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate())+Integer.parseInt(coupon.getExpireValue()));
		}
		resultStatus = checkCoupon(coupon, 1);
		if(!resultStatus.getCode().equals(ResultStatus.SUCCESS.getCode())){
			throw new CouponException(resultStatus);
        }
		couponIssue = new CouponIssue();
		couponIssue.setCouponId(couponId);
		couponIssue.setOrgId(coupon.getOrgId());
		couponIssue.setStatusId(StatusConstant.COUPON_ISSUE_COMPLETE.getId());
		couponIssue.setQuantity((long) 1);
		couponIssue.setCreateDate(DateUtils.getCurrentDateOfDb());
		couponIssue.setCreateBy(null);//临时
		couponIssue.setIssueDate(DateUtils.getCurrentDateOfDb());
		couponIssue.setCreateTime(DateUtils.getCurrentTimeOfDb());
		couponIssue.setIssueBy(null);//临时
		couponIssueRepository.save(couponIssue);
		String couponInstancecode = codeService.generateCode(CouponInstance.class);
		couponInstance = new CouponInstance();
		couponInstance.setCouponId(couponId);
		couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
		couponInstance.setOrgId(coupon.getOrgId());
		couponInstance.setCouponInstanceCode(couponInstancecode);
		couponInstance.setCreateTime(currentTime);
		couponInstance.setUpdateTime(currentTime);
		couponInstance.setGetTime(currentTime);
		couponInstance.setStartDate(startDate);
		couponInstance.setExpireDate(expireDate);
		couponInstance.setGetChannelId(sourceId);
		couponInstance.setCouponIssueId(couponIssue.getCouponIssueId());
		couponInstance.setCouponInstanceOwner(memberId);
		couponInstance.setCouponInstanceUser(memberId);
		couponInstance.setExternalId(externalId);
		couponInstance.setActivityInstanceId(coupon.getActivityId());
		couponInstance.setExternalId(externalId);
		couponInstanceRepository.save(couponInstance);
		exp = new CouponInstanceExp();
		exp.setCouponName(coupon.getCouponName());
		exp.setCouponInstanceCode(couponInstance.getCouponInstanceCode());
		exp.setStartDate(startDate);
		exp.setExpireDate(expireDate);
		exp.setWebchatRemark(coupon.getWebchatRemark());
		expList.add(exp);
		if (coupon.getOriginId() == 2) {
			externalCouponRepository.bindCouponInstanceWithOneExternalCoupon(couponInstance.getCouponInstanceId(), couponId);
		}
		//return insertCouponIssue(memberId, couponList, sourceId, null,type,OPT_TYPE_SINGLE);
		coupon.setAvailableQuantity(coupon.getAvailableQuantity()-1);
		if(coupon.getFirstPublishTime()==null || coupon.getFirstPublishTime().isEmpty()){
				coupon.setFirstPublishTime(DateUtils.getCurrentTimeOfDb());
        }
        coupon.setLastPublishTime(DateUtils.getCurrentTimeOfDb());
        if(coupon.getIssueQuantity()==null){
            coupon.setIssueQuantity((long)1);
        }else{
            coupon.setIssueQuantity(coupon.getIssueQuantity()+1);
        }
        coupon.setPublishBy("waiter");//临时
        coupon.setStatusId(StatusConstant.COUPON_ISSUED.getId());
        couponRepository.updateByPkSelective(coupon);

		couponArray = new JSONArray();
        couponJson = new JSONObject();
        couponJson.put("couponName", coupon.getCouponName());
        couponJson.put("number", 1);
		sendWechatMsg(memberId,expList,type);
		couponArray.add(couponJson);
		return couponArray.toString();
	}
	
	@Transactional
	@Override
	public String insertCouponIssue(Long memberId, List<CouponResult> couponList,Long sourceId, String externalId,int type) throws Exception {
		return insertCouponIssue(memberId, couponList,sourceId, externalId,type,OPT_TYPE_BATCH);
	}
	
	/**
	 * insertCouponIssue:(发送优惠券方法). <br/>
	 * Date: 2016-5-12 下午3:18:02 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 * @throws Exception
	 */
	@Transactional
	private String insertCouponIssue(Long memberId, List<CouponResult> couponList,Long sourceId, String externalId,int type,String optType) throws Exception {
		CouponIssue couponIssue = null;
		List<CouponInstance> couponInstances = null;
		CouponInstance couponInstance = null;
		Coupon coupon = null;
		Long couponId = null;
		String startDate = null;
		String expireDate = null;
		String currentTime = null;
		List<CouponInstanceExp> expList = null;
		CouponInstanceExp exp = null;
		ResultStatus resultStatus = null;
		JSONObject couponJson = null;
		JSONArray couponArray = null;
		
		expList = new ArrayList<CouponInstanceExp>();
		couponInstances = new ArrayList<CouponInstance>();
		currentTime = DateUtils.getCurrentTimeOfDb();
		couponArray = new JSONArray();
		
		for (CouponResult couponResult : couponList) {
			couponId = couponResult.getCouponId();
			coupon = couponRepository.findByPk(couponId);
			Integer couponNum = couponResult.getQuantity();
			resultStatus = checkCoupon(coupon,couponNum); 
			
			//校验优惠券
			if(!resultStatus.getCode().equals(ResultStatus.SUCCESS.getCode())){
				if(optType.equals(OPT_TYPE_SINGLE)){
					throw new CouponException(resultStatus);
				}
				continue;
			}
			
			
			//生成优惠券发布记录
			//if (coupon.getOriginId() == 1) {
				couponIssue = new CouponIssue();
				couponIssue.setCouponId(couponId);
				couponIssue.setOrgId(coupon.getOrgId());
				couponIssue.setStatusId(StatusConstant.COUPON_ISSUE_COMPLETE.getId());
				couponIssue.setQuantity((long) couponNum);
				couponIssue.setCreateDate(DateUtils.getCurrentDateOfDb());
				couponIssue.setCreateBy(null);//临时
				couponIssue.setIssueDate(DateUtils.getCurrentDateOfDb());
				couponIssue.setCreateTime(DateUtils.getCurrentTimeOfDb());
				couponIssue.setIssueBy(null);//临时
				couponIssueRepository.save(couponIssue);

			//生成优惠券实例
			if(coupon.getExpireType().equals("1")){
				startDate = coupon.getFromDate();
				expireDate = coupon.getThruDate();
			}else if(coupon.getExpireType().equals("2")){
				startDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate()));
				expireDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate())+Integer.parseInt(coupon.getExpireValue()));
			}
			List<String> couponInstancecodes = codeService.generateCode(CouponInstance.class,couponNum);
			System.out.println("for开始"+DateUtils.getCurrentTimeOfDb());
			for(int i = 0;i < couponNum;i++){
				couponInstance = new CouponInstance();
				couponInstance.setCouponId(couponId);
				couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
				couponInstance.setOrgId(coupon.getOrgId());
				couponInstance.setCouponInstanceCode(couponInstancecodes.get(i));
				couponInstance.setCreateTime(currentTime);
				couponInstance.setUpdateTime(currentTime);
				couponInstance.setGetTime(currentTime);
				couponInstance.setStartDate(startDate);
				couponInstance.setExpireDate(expireDate);
				couponInstance.setGetChannelId(sourceId);
				couponInstance.setCouponIssueId(couponIssue.getCouponIssueId());
				couponInstance.setCouponInstanceOwner(memberId);
				couponInstance.setCouponInstanceUser(memberId);
				couponInstance.setExternalId(externalId);
				couponInstance.setActivityInstanceId(couponResult.getActivityInstanceId());
				couponInstance.setOrderId(couponResult.getOrderId());
				couponInstance.setExternalId(couponResult.getExternalId());
				couponInstances.add(couponInstance);
				
				//生成优惠扩展信息，发送微信消息时使用
				exp = new CouponInstanceExp();
				exp.setCouponName(coupon.getCouponName());
				exp.setCouponInstanceCode(couponInstancecodes.get(i));
				exp.setStartDate(startDate);
				exp.setExpireDate(expireDate);
				exp.setOrderId(couponResult.getOrderId());
				exp.setWebchatRemark(coupon.getWebchatRemark());
				expList.add(exp);
			}
			//修改优惠券
			coupon.setAvailableQuantity(coupon.getAvailableQuantity()-couponNum);
			System.out.println(coupon.getFirstPublishTime());
			if(coupon.getFirstPublishTime()==null || coupon.getFirstPublishTime().isEmpty()){
				coupon.setFirstPublishTime(DateUtils.getCurrentTimeOfDb());
			}
			coupon.setLastPublishTime(DateUtils.getCurrentTimeOfDb());
			if(coupon.getIssueQuantity()==null){
				coupon.setIssueQuantity((long)couponNum);
			}else{
				coupon.setIssueQuantity(coupon.getIssueQuantity()+couponNum);
			}
			coupon.setPublishBy("waiter");//临时
			coupon.setStatusId(StatusConstant.COUPON_ISSUED.getId());
			couponRepository.updateByPkSelective(coupon);
			
			couponJson = new JSONObject();
			couponJson.put("couponName", coupon.getCouponName());
			couponJson.put("number", couponNum);
			couponArray.add(couponJson);
		}
		
		System.out.println(DateUtils.getCurrentTimeOfDb()+"插入开始");
		if(couponInstances.size() > 0){
			couponInstanceRepository.saveList(couponInstances);
		}
		System.out.println("插入结束"+DateUtils.getCurrentTimeOfDb());
		return couponArray.toString();
	}
	@Transactional
	private String insertGiftCoupon(Long couponId, Set<String> couponIntanceCodes) throws Exception {
		CouponIssue couponIssue = null;
		List<CouponInstance> couponInstances = null;
		CouponInstance couponInstance = null;
		Coupon coupon = null;
		String currentTime = null;
		JSONObject couponJson = null;
		JSONArray couponArray = null;
		
		couponInstances = new ArrayList<CouponInstance>();
		currentTime = DateUtils.getCurrentTimeOfDb();
		couponArray = new JSONArray();
		
		coupon = couponRepository.findByPk(couponId);
		for (String couponIntanceCode : couponIntanceCodes) {
			
			//生成优惠券导入记录
			couponIssue = new CouponIssue();
			couponIssue.setCouponId(couponId);
			couponIssue.setOrgId(coupon.getOrgId());
			couponIssue.setStatusId(StatusConstant.COUPON_ISSUE_WAIT.getId());
			couponIssue.setQuantity((long)couponIntanceCodes.size() );
			couponIssue.setCreateDate(DateUtils.getCurrentDateOfDb());
			couponIssue.setCreateBy(null);//临时
			couponIssue.setIssueDate(DateUtils.getCurrentDateOfDb());
			couponIssue.setCreateTime(DateUtils.getCurrentTimeOfDb());
			couponIssue.setIssueBy(null);//临时
			couponIssueRepository.save(couponIssue);
			
			//List<String> couponInstancecodes = codeService.generateCode(CouponInstance.class,couponNum);
			System.out.println("for开始"+DateUtils.getCurrentTimeOfDb());
				couponInstance = new CouponInstance();
				couponInstance.setCouponId(couponId);
				couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
				couponInstance.setOrgId(coupon.getOrgId());
				couponInstance.setCouponInstanceCode(couponIntanceCode);
				couponInstance.setCreateTime(currentTime);
				couponInstance.setUpdateTime(currentTime);
				//couponInstance.setGetTime(currentTime);
//				couponInstance.setStartDate(startDate);
//				couponInstance.setExpireDate(expireDate);
				//couponInstance.setGetChannelId(sourceId);
				couponInstance.setCouponIssueId(couponIssue.getCouponIssueId());
//				couponInstance.setCouponInstanceOwner(memberId);
//				couponInstance.setCouponInstanceUser(memberId);
				//couponInstance.setExternalId(externalId);
//				couponInstance.setActivityInstanceId(couponResult.getActivityInstanceId());
//				couponInstance.setOrderId(couponResult.getOrderId());
//				couponInstance.setExternalId(couponResult.getExternalId());
				couponInstances.add(couponInstance);
				
			//修改优惠券
			//coupon.setAvailableQuantity(coupon.getAvailableQuantity()-couponNum);
//			System.out.println(coupon.getFirstPublishTime());
//			if(coupon.getFirstPublishTime()==null || coupon.getFirstPublishTime().isEmpty()){
//				coupon.setFirstPublishTime(DateUtils.getCurrentTimeOfDb());
//			}
//			coupon.setLastPublishTime(DateUtils.getCurrentTimeOfDb());
//			if(coupon.getIssueQuantity()==null){
//				coupon.setIssueQuantity((long)couponNum);
//			}else{
//				coupon.setIssueQuantity(coupon.getIssueQuantity()+couponNum);
//			}
//			coupon.setTotalQuantity(coupon.getTotalQuantity()+couponIntanceCodes.size());
//			coupon.setAvailableQuantity(coupon.getAvailableQuantity()+couponIntanceCodes.size());
			coupon.setPublishBy("waiter");//临时
			coupon.setStatusId(StatusConstant.COUPON_NEW.getId());
			couponRepository.updateByPkSelective(coupon);
			
			couponJson = new JSONObject();
			couponJson.put("couponName", coupon.getCouponName());
			couponJson.put("number", couponIntanceCodes.size());
			couponArray.add(couponJson);
		}
		
		System.out.println(DateUtils.getCurrentTimeOfDb()+"插入开始");
		if(couponInstances.size() > 0){
			couponInstanceRepository.saveList(couponInstances);
		}
		System.out.println("插入结束"+DateUtils.getCurrentTimeOfDb());
		return couponArray.toString();
	}



	@Override
	public List<Channel> queryChannelList() {
		return couponIssueChannelRepository.findChannelList();
	}
	
	@Override
	public List<Channel> queryChannelListByDc(Long typeId) {
		return couponIssueChannelRepository.findChannelListByDc(typeId);
	}
	@Override
	public int queryCountByCouponId(Long couponId) {
		return couponIssueRepository.findCouponIssueCount(couponId);
	}
	
	
	/**
	 * sendWechatMsg:(保存获得优惠券消息). <br/>
	 * Date: 2015-12-17 下午3:36:01 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param expList
	 */
	private void sendWechatMsg(Long memberId,List<CouponInstanceExp> expList,int type){
		JSONObject json = null;
		WechatMsgQueueH msgQueueH = null;
		List<WechatMsgQueueH> msgQueueHList = null;
		
		msgQueueHList = new ArrayList<WechatMsgQueueH>();
		for (CouponInstanceExp couponInstanceExp : expList) {
			Member member = memberService.queryByPk(memberId);
			json = new JSONObject();
			msgQueueH = new WechatMsgQueueH();
			String startDate = DateUtils.dateStrToNewFormat(DateUtils.getCurrentTimeOfDb(), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss, DateUtils.DATE_PATTERN_YYYYMMDDHHmm_3);
			String expireDate = DateUtils.dateStrToNewFormat(couponInstanceExp.getExpireDate(), DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
			json.put("couponName", couponInstanceExp.getCouponName());
			json.put("startDate", startDate);
			json.put("expireDate", expireDate);
			json.put("comments", couponInstanceExp.getComments());
			json.put("memberName",member.getMemberName());
			json.put("webchatRemark", couponInstanceExp.getWebchatRemark());
			
			msgQueueH.setBusinessType(type);
			msgQueueH.setCreateTime(DateUtils.getCurrentTimeOfDb());
			msgQueueH.setMemberId(memberId);
			msgQueueH.setMsgContent(json.toString());
			msgQueueH.setMsgKey(couponInstanceExp.getCouponInstanceCode());
			msgQueueH.setIsSend("N");
			msgQueueHList.add(msgQueueH);
		}
		if(msgQueueHList.size() > 0){
			wechatMsgQueueHRepository.saveList(msgQueueHList);
		}
	}
	@Override
	public void insertExternalCoupon(Long couponId, List<ExternalCoupon> externalCoupons, JsonModel result) {
		int count = 0;
		int len = externalCoupons.size();
		//用set去除重复项
		Set<String> couponIntanceCodes=new LinkedHashSet<String>();
		for (int i = 0; i < len; i++){
			couponIntanceCodes.add(externalCoupons.get(i).getCouponInstanceCode());
		}
		Set<String> removeSet=new HashSet<String>();
		for (String couponInstanceCode : couponIntanceCodes) {
			CouponInstance couponInstance = couponInstanceRepository.findCouponInstanceByInstanceCode(couponInstanceCode);
			if(couponInstance!=null&&couponId!=couponInstance.getCouponId()){
				removeSet.add(couponInstanceCode);
			}
		}
		if(!removeSet.isEmpty()){
			couponIntanceCodes.removeAll(removeSet);
		}
		
		try {
			insertGiftCoupon(couponId, couponIntanceCodes);
			result.setStatusSuccess();
			result.setObj(count);
		} catch (Exception e) {
			result.setStatusError();
			result.setMessage(e.getMessage());
		}
	}

	@Autowired
	public void setCouponIssueRepository(CouponIssueRepository couponIssueRepository) {
		this.couponIssueRepository = couponIssueRepository;
	}
	
	@Autowired
	public void setCouponInstanceRepository(CouponInstanceRepository couponInstanceRepository) {
		this.couponInstanceRepository = couponInstanceRepository;
	}

	@Autowired
	public void setCouponRepository(CouponRepository couponRepository) {
		this.couponRepository = couponRepository;
	}

	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}
	
	@Autowired
	public void setCouponIssueChannelRepository(CouponIssueChannelRepository couponIssueChannelRepository) {
		this.couponIssueChannelRepository = couponIssueChannelRepository;
	}

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	@Autowired
	public void setWechatMsgQueueRepository(WechatMsgQueueRepository wechatMsgQueueRepository) {
		this.wechatMsgQueueRepository = wechatMsgQueueRepository;
	}

	@Autowired
	public void setWechatMsgQueueHRepository(WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}

	@Override
	public Long queryCouponIsIssueing() {
		return this.couponIssueRepository.queryCouponIsIssueing();
	}

	public ExternalCouponRepository getExternalCouponRepository() {
		return externalCouponRepository;
	}
	@Autowired
	public void setExternalCouponRepository(ExternalCouponRepository externalCouponRepository) {
		this.externalCouponRepository = externalCouponRepository;
	}
}
