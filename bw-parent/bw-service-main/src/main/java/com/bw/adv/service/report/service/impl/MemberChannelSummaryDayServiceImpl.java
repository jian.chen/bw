/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberChannelSummaryDayServiceImpl.java
 * Package Name:com.sage.scrm.service.report.service.impl
 * Date:2015年11月26日下午5:09:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.report.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.model.MemberChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.MemberChannelSummaryDayExp;
import com.bw.adv.module.report.repository.MemberChannelSummaryDayRepository;
import com.bw.adv.service.report.service.MemberChannelSummaryDayService;

/**
 * ClassName:MemberChannelSummaryDayServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 下午5:09:34 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class MemberChannelSummaryDayServiceImpl extends BaseServiceImpl<MemberChannelSummaryDay> 
	implements MemberChannelSummaryDayService{

	private MemberChannelSummaryDayRepository memberChannelSummaryDayRepository;
	
	@Override
	public BaseRepository<MemberChannelSummaryDay, ? extends BaseMapper<MemberChannelSummaryDay>> getBaseRepository() {
		
		return memberChannelSummaryDayRepository;
	}

	@Override
	public List<MemberChannelSummaryDayExp> queryExpByExample(Example example,
			Page<MemberChannelSummaryDayExp> page) {
		
		return this.memberChannelSummaryDayRepository.findExpByExample(example, page);
	}

	@Override
	public void callMemberChannelSummary(String dateStr) {
		
		this.memberChannelSummaryDayRepository.callMemberChannelSummary(dateStr);
	}

	@Autowired
	public void setMemberChannelSummaryDayRepository(
			MemberChannelSummaryDayRepository memberChannelSummaryDayRepository) {
		this.memberChannelSummaryDayRepository = memberChannelSummaryDayRepository;
	}


}

