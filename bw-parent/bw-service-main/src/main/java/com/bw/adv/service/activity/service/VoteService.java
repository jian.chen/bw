package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.VoteOptions;
import com.bw.adv.module.activity.model.VoteResult;
import com.bw.adv.module.activity.model.VoteTitle;

public interface VoteService {
	
	/**
	 * 查询所有的投票并且带选项数量
	 * @return
	 */
	public List<VoteTitle> findAllVoteAndOptionCount(Page<VoteTitle> templatePage);
	
	/**
	 * 保存投票标题
	 * @param voteTitle
	 */
	public int saveVoteTitle(VoteTitle voteTitle);
	
	/**
	 * 按照id查询投票
	 * @param voteId
	 * @return
	 */
	public VoteTitle findVoteTitleById(Long voteId);
	
	/**
	 * 更新投票
	 * @param votetitle
	 */
	public void updateVoteTitle(VoteTitle votetitle);
	
	/**
	 * 删除投票
	 * @param voteId
	 */
	public void deleteVoteTitle(Long voteId);
	
	/**
	 * 根据投票id查询选项
	 * @param voteId
	 * @param templatePage
	 * @return
	 */
	public List<VoteOptions> findVoteOptionsByTitleId(Long voteId,Page<VoteOptions> templatePage);
	
	/**
	 * 保存选项
	 * @param voteOptions
	 */
	public void saveVoteOptions(VoteOptions voteOptions);
	
	/**
	 * 更新选项
	 * @param voteOptions
	 */
	public void updateVoteOptions(VoteOptions voteOptions);
	
	/**
	 * 按照id查询投票选项
	 * @param optionsId
	 * @return
	 */
	public VoteOptions findOptionsById(Long optionsId);
	
	/**
	 * 删除选项
	 * @param optionsId
	 */
	public void deleteVoteOptions(Long optionsId);
	
	/**
	 * 查询投票回答人数
	 * @param titleId
	 * @return
	 */
	public Long selectCountForVoteResult(Long titleId);
	
	/**
	 * 查询投票的调查结果
	 * @param titleId
	 * @return
	 */
	public List<VoteResult> findVoteResultByTitleId(Long titleId);
	
	/**
	 *	查询激活的投票数量
	 * queryActiveVoteAmount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public Long queryActiveVoteAmount();
	
	/**
	 * 查询激活的投票
	 * findActiveVote:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public VoteTitle findActiveVote();
	
	/**
	 * 保存投票结果
	 * saveVoteResult:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param voteResult
	 */
	public void saveVoteResult(List<VoteResult> voteResult);
	
}
