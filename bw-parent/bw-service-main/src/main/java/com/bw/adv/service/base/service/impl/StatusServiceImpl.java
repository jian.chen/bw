/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.Status;
import com.bw.adv.module.base.repository.OrgRepository;
import com.bw.adv.module.base.repository.StatusRepository;
import com.bw.adv.service.base.service.OrgService;
import com.bw.adv.service.base.service.StatusService;


/**
 * ClassName: ProductServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:15:40 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Service
public class StatusServiceImpl extends BaseServiceImpl<Status> implements StatusService{

	private StatusRepository statusRepository;

	@Override
	public BaseRepository<Status, ? extends BaseMapper<Status>> getBaseRepository() {
		
		return statusRepository;
	}
	
	@Autowired
	public void setStatusRepository(StatusRepository statusRepository) {
		this.statusRepository = statusRepository;
	}	
	



}

