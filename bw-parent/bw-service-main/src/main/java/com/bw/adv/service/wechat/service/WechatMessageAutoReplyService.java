/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageAutoReplyService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月19日下午2:15:13
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;
import java.util.Map;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatMessageAutoReply;
import com.bw.adv.module.wechat.model.exp.WechatMessageAutoReplyExp;

/**
 * ClassName:WechatMessageAutoReplyService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午2:15:13 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMessageAutoReplyService extends BaseService<WechatMessageAutoReply> {
	
	public WechatMessageAutoReplyExp queryWechatMessageAutoReply(String type);
	
	public void sendWechatMessageAutoReply(Map<String,Object> paramsMap);
	
}

