/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.base.model.QrCode;
import com.bw.adv.module.base.model.exp.QrCodeExp;
import com.bw.adv.module.base.repository.ChannelRepository;
import com.bw.adv.module.base.repository.QrCodeRepository;
import com.bw.adv.module.wechat.model.ChannelQrCode;
import com.bw.adv.module.wechat.repository.ChannelQrCodeRepository;
import com.bw.adv.service.base.service.QrCodeService;


/**
 * ClassName: ProductServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:15:40 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Service
public class QrCodeServiceImpl extends BaseServiceImpl<QrCode> implements QrCodeService{

	private QrCodeRepository qrCodeRepository;
	
	private ChannelQrCodeRepository channelQrCodeRepository;
	
	private ChannelRepository channelRepository;
	
	@Override
	public BaseRepository<QrCode, ? extends BaseMapper<QrCode>> getBaseRepository() {
		return qrCodeRepository;
	}	
	
	@Override
	public List<QrCode> queryListForQrCode(String isActive, Page<QrCode> page) {
		return this.qrCodeRepository.findQrCodeList(isActive,page);
	}

	
	@Override
	public QrCodeExp queryQrCodeByPrimaryKey(Long qrCodeId) {
		return this.qrCodeRepository.findQrCodeByPrimaryKey(qrCodeId);
	}

	@Override
	public int removeQrCode(String qrCodeIds) {
		return this.qrCodeRepository.removeQrCode(qrCodeIds);
	}
	@Override
	public QrCode queryQrCodeByQrCode(String qrCodeCode) {
		return qrCodeRepository.findQrCodeByQrCode(qrCodeCode);
	}

	@Override
	public List<Channel> queryChannelQrCodeList() {
		
		List<Channel> result = channelRepository.findList();
		
		return result;
	}

	
	@Autowired
	public void setQrCodeRepository(QrCodeRepository qrCodeRepository) {
		this.qrCodeRepository = qrCodeRepository;
	}

	@Autowired
	public void setChannelQrCodeRepository(
			ChannelQrCodeRepository channelQrCodeRepository) {
		this.channelQrCodeRepository = channelQrCodeRepository;
	}

	@Autowired
	public void setChannelRepository(ChannelRepository channelRepository) {
		this.channelRepository = channelRepository;
	}

}

