package com.bw.adv.service.channel.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;

public interface ChannelService extends BaseService<Channel> {
	public List<Channel> queryList();
	
	public List<Channel> findListByChannelName(String channelName,Page<Channel> page);
	
	public void saveOrUpdateChannel(Channel channel);

	
}
