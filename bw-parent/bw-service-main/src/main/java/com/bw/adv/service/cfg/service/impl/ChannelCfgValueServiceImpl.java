package com.bw.adv.service.cfg.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.base.repository.ChannelCfgValueRepository;
import com.bw.adv.module.base.repository.ChannelInstanceRecordRepository;
import com.bw.adv.module.cfg.model.ChannelCfg;
import com.bw.adv.module.cfg.model.ChannelCfgExp;
import com.bw.adv.module.cfg.model.ChannelCfgValue;
import com.bw.adv.module.cfg.model.ChannelCfgValueExp;
import com.bw.adv.module.cfg.model.ChannelInstanceRecord;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.rule.constant.ConditionsConstant;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.repository.CouponRepository;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.cfg.service.ChannelCfgService;
import com.bw.adv.service.cfg.service.ChannelCfgValueService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Service
public class ChannelCfgValueServiceImpl extends BaseServiceImpl<ChannelCfgValue> implements ChannelCfgValueService {
	
	private ChannelCfgValueRepository channelCfgValueRepository;
	
	private ChannelCfgService channelCfgService;
	private CouponRepository couponRepository;
	
	private ChannelInstanceRecordRepository channelInstanceRecordRepository;
	
	@Override
	public BaseRepository<ChannelCfgValue, ? extends BaseMapper<ChannelCfgValue>> getBaseRepository() {
		return channelCfgValueRepository;
	}
	
	@Transactional
	@Override
	public void modifyCfgConditionValue(SysUser sysUser,JSONObject jsonObject) {
		ChannelCfgValue conditionsValue = null;
		ChannelCfg channelCfg=null;
		Long channelId=null;
		Map<String, String> conMap = null;
		List<ChannelCfgValue> conditionsValueList = null;
		Long channelCfgId=null;
		JSONObject conditionJson = null;
		String thruDate = null;
		String res = "";
		
		conditionJson = JSONObject.fromObject(jsonObject.get("conditionMap"));
		
		conMap = new HashMap<String,String>();
		if(jsonObject.containsKey("pointsOrcoupon")&&jsonObject.getString("pointsOrcoupon").indexOf("coupon")>-1){
			if(conditionJson.get(ConditionsConstant.GIVE_COUPONS.getCode().toString()) != null){
				String couponTemp =  conditionJson.get(ConditionsConstant.GIVE_COUPONS.getCode().toString()).toString();
				JSONObject couponTempJson = JSONObject.fromObject(couponTemp);
				JSONArray couponIds = couponTempJson.names();
				
				List<Coupon> couList = couponRepository.findListByIds(couponIds.toString().replace("\"", "'").replace("[", "(").replace("]", ")"));
				
				//判断保存的券有没有过期or作废or删除
				if(couList.size() > 0){
					for(Coupon couTemp : couList){
						if(couTemp.getStatusId() != null){
							if(StatusConstant.COUPON_EXPIRE.getId().equals(couTemp.getStatusId())){
								res += couTemp.getCouponName() + StatusConstant.COUPON_EXPIRE.getName();
							}else if(StatusConstant.COUPON_DESTROY.getId().equals(couTemp.getStatusId())){
								res += couTemp.getCouponName() + StatusConstant.COUPON_DESTROY.getName();
							}else if(StatusConstant.COUPON_DELETE.getId().equals(couTemp.getStatusId())){
								res += couTemp.getCouponName() + StatusConstant.COUPON_DELETE.getName();
							}else{
								if(couTemp.getExpireType() != null){
									if("1".equals(couTemp.getExpireType())){
										if(DateUtils.getCurrentDateOfDb().compareTo(couTemp.getThruDate()) > 0){
											res += couTemp.getCouponName()+"已过期！";
										}
									}
								}
							}
						}
					}
				}
				if(!"".equals(res)){
					throw new MemberException(res);
				}
				conMap.put(ConditionsConstant.GIVE_COUPONS.getCode(), conditionJson.getString(ConditionsConstant.GIVE_COUPONS.getCode()));
			}
		}
		if(jsonObject.containsKey("pointsOrcoupon")&&jsonObject.getString("pointsOrcoupon").indexOf("points")>-1){
			if(!conditionJson.containsKey(ConditionsConstant.GIVE_POINTS.getCode())||StringUtils.isBlank(conditionJson.getString(ConditionsConstant.GIVE_POINTS.getCode()))){
				throw new MemberException("积分勾选，必须输入注册积分");
			}
			conMap.put(ConditionsConstant.GIVE_POINTS.getCode(), conditionJson.getString(ConditionsConstant.GIVE_POINTS.getCode()));
		}
		
		if(conditionJson.containsKey(ConditionsConstant.MEMBER_GRADE.getCode())&& StringUtils.isNotBlank(conditionJson.getString(ConditionsConstant.MEMBER_GRADE.getCode()))){
			conMap.put(ConditionsConstant.MEMBER_GRADE.getCode(), conditionJson.getString(ConditionsConstant.MEMBER_GRADE.getCode()));
		}
		
		channelCfg=new ChannelCfg();
		
		channelCfg.setChannelInstanceName(jsonObject.get("channelInstanceName")+"");
		channelCfg.setFromDate(jsonObject.get("fromDate").toString().replace("-", ""));
		channelCfg.setValidityType(jsonObject.get("validityType")+"");
		channelCfg.setValidityValue(jsonObject.get("validityValue") == null ? null : jsonObject.get("validityValue").toString().replace("-", ""));
		
		//结束时间
		if("fixed".equals(jsonObject.get("validityType").toString())){
			channelCfg.setThruDate(jsonObject.get("validityValue").toString().replace("-", ""));
		}else if("period".equals(jsonObject.get("validityType").toString())){
			//获取开始时间之后N天的日期
			thruDate = DateUtils.calcDays(DateUtils.formatString(jsonObject.get("fromDate").toString()), Integer.parseInt(jsonObject.get("validityValue").toString()));
			channelCfg.setThruDate(thruDate);
		}else if("forever".equals(jsonObject.get("validityType").toString())){
			channelCfg.setThruDate("");
		}
		channelCfg.setComments(jsonObject.getString("channelDesc"));
		String str_channeCfgId = jsonObject.getString("channelCfgId");
		channelId=jsonObject.getLong("channelId");
		//如果存在就修改，不存在就修改
		//保存
		if(StringUtils.isBlank(str_channeCfgId)){
			channelCfg.setChannelId(channelId);
			channelCfg.setEnable("Y");
			channelCfg.setCreateTime(DateUtils.getCurrentTimeOfDb());
			channelCfg.setCreateBy(sysUser.getUserId());
			channelCfg.setStatusId(StatusConstant.ACTIVITY_INSTANCE_DRAFT.getId());
			this.channelCfgService.saveSelective(channelCfg);
		}else{
			channelCfgId=Long.parseLong(str_channeCfgId);
			channelCfg.setUpdateBy(sysUser.getUserId());
			channelCfg.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			channelCfg.setChannelCfgId(channelCfgId);
			channelCfgService.updateByPkSelective(channelCfg);
			channelCfgValueRepository.removeCoditionByChannelCfgId(channelCfg.getChannelCfgId());
		}
		
		
		conditionsValueList = new ArrayList<ChannelCfgValue>();
		//添加对应的条件值
		for (String code : conMap.keySet()) {
				//新增规则条件值
				conditionsValue = new ChannelCfgValue();
				conditionsValue.setCfgCode(code);
				conditionsValue.setCfgValue(conMap.get(code));
				conditionsValue.setChannelCfgId(channelCfg.getChannelCfgId()); //外键
				conditionsValueList.add(conditionsValue);
		}
		if(conditionsValueList.size() > 0){
			channelCfgValueRepository.saveList(conditionsValueList);
		}
	}
	
	private boolean modifyInstanceStatus(ChannelCfgExp channelCfgExp){
		String currentDate= DateUtils.getCurrentDateOfDb();
		
		//如果已过期
		if(StatusConstant.ACTIVITY_INSTANCE_EXPIRED.getId().equals( channelCfgExp.getStatusId())){
			return false;
		}
		//如果已终止
		if(StatusConstant.ACTIVITY_INSTANCE_BREAK.getId().equals( channelCfgExp.getStatusId())){
			return false;
		}
		
		//如果开始时间还没到
		if(channelCfgExp.getFromDate().compareTo(currentDate) > 0 ){
			return false;
		}
		//如果到期时间已过
		if(StringUtils.isNotBlank(channelCfgExp.getThruDate()) && channelCfgExp.getThruDate().compareTo(currentDate) < 0 ){
			if(!StatusConstant.ACTIVITY_INSTANCE_EXPIRED.getId().equals(channelCfgExp.getStatusId())){
				this.channelCfgService.modifyChannelCfgStatus(null, channelCfgExp.getChannelCfgId(),StatusConstant.ACTIVITY_INSTANCE_EXPIRED.getId(),"auto");
			}
			return false;
		}
		//如果是已创建
		if(StatusConstant.ACTIVITY_INSTANCE_DRAFT.getId().equals( channelCfgExp.getStatusId())){
			//修改为已启用
			this.channelCfgService.modifyChannelCfgStatus(null, channelCfgExp.getChannelCfgId(),StatusConstant.ACTIVITY_INSTANCE_ENABLE.getId(),"auto");
			return true;
		}
		
		return true;
	}
	
	@Override
	public void afterRegiter(Long memberId,Long channelId){
		List<ChannelInstanceRecord> list=null;
		ChannelInstanceRecord channelInstanceRecord=null;
		if(channelId==null){
			return;
		}
		//发送黑金卡微信消息
		
		List<ChannelCfgExp> channelCfgValue = channelCfgService.queryChannelCfgExpByChannelId(channelId);//this.queryConditionByChannelId(channelId);
		if(channelCfgValue==null||channelCfgValue.isEmpty()){
			return;
		}
		list=new ArrayList<ChannelInstanceRecord>();
		for (ChannelCfgExp channelCfgExp : channelCfgValue) {
			List<ChannelCfgValue> channelCfgValues = channelCfgExp.getChannelCfgValues();
			
			if(!modifyInstanceStatus(channelCfgExp)){
				continue;
			}
			channelInstanceRecord=new ChannelInstanceRecord();
			channelInstanceRecord.setChannelCfgId(channelCfgExp.getChannelCfgId());
			channelInstanceRecord.setMemberId(memberId);
			channelInstanceRecord.setCreateTime(DateUtils.getCurrentTimeOfDb());
			for (Iterator<ChannelCfgValue> iterator = channelCfgValues.iterator(); iterator.hasNext();) {
				ChannelCfgValue channelCfgValueExp = iterator.next();
				//注册之后送积分
				if(channelCfgValueExp.getCfgCode().equals(ConditionsConstant.GIVE_POINTS.getCode())){
					channelInstanceRecord.setPoints(channelCfgValueExp.getCfgValue());
				}
				//注册之后送优惠券
				if(channelCfgValueExp.getCfgCode().equals(ConditionsConstant.GIVE_COUPONS.getCode())){
					channelInstanceRecord.setCoupons(channelCfgValueExp.getCfgValue());
				}
				//注册之后等级
				if(channelCfgValueExp.getCfgCode().equals(ConditionsConstant.MEMBER_GRADE.getCode())){
					channelInstanceRecord.setLevels(channelCfgValueExp.getCfgValue());
				}
			}
			list.add(channelInstanceRecord);
		}
		if(!list.isEmpty()){
			this.channelInstanceRecordRepository.saveList(list);
		}
		
		
	}
	@Override
	public List<ChannelCfgValueExp> queryConditionByChannelCfgId(Long channelCfgId) {
		return this.channelCfgValueRepository.queryConditionByChannelCfgId(channelCfgId);
	}
	
	@Autowired
	public void setCouponRepository(CouponRepository couponRepository) {
		this.couponRepository = couponRepository;
	}

	@Autowired
	public void setChannelCfgValueRepository(ChannelCfgValueRepository channelCfgValueRepository) {
		this.channelCfgValueRepository = channelCfgValueRepository;
	}

	@Autowired
	public void setChannelCfgService(ChannelCfgService channelCfgService) {
		this.channelCfgService = channelCfgService;
	}

	@Autowired
	public void setChannelInstanceRecordRepository(ChannelInstanceRecordRepository channelInstanceRecordRepository) {
		this.channelInstanceRecordRepository = channelInstanceRecordRepository;
	}

	
}
