/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SceneGameInstanceService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2016年1月15日下午2:27:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.SceneGameInstance;
import com.bw.adv.module.activity.model.exp.SceneGameInstanceExp;
import com.bw.adv.module.member.search.SceneGameInstanceSearch;
import com.bw.adv.module.sys.model.SysUser;


/**
 * ClassName:SceneGameInstanceService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午2:27:04 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
public interface SceneGameInstanceService extends BaseService<SceneGameInstance> {

	/**
	 * saveSceneGameInstance:新建场景游戏实例. <br/>
	 * Date: 2016年1月17日 下午3:49:00 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param request
	 * @param sceneGameInstance
	 */
	void saveSceneGameInstance(JSONObject jsonObject,SysUser sysUser);
	
	/**
	 * editSceneGameInstance:修改场景游戏实例的信息（包括奖项信息，适用于未激活的场景游戏实例）. <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * Date: 2016年2月22日 上午9:03:21 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang(*^_^*)
	 * @version jdk1.7
	 * @param jsonObject
	 * @param sysUser
	 */
	void editSceneGameInstance(JSONObject jsonObject,SysUser sysUser);
	
	/**
	 * editSceneGameInstanceIsUse:修改场景游戏实例的信息（不包括奖项信息，适用于已激活的场景游戏实例）. <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * Date: 2016年2月22日 上午9:04:57 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang(*^_^*)
	 * @version jdk1.7
	 * @param jsonObject
	 * @param sysUser
	 */
	void editSceneGameInstanceIsUse(JSONObject jsonObject,SysUser sysUser);
	
	/**
	 * 
	 * querySceneGameInstanceByGameId:查询场景活动的实例. <br/>
	 * Date: 2016年1月18日 下午5:17:47 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameId
	 * @return
	 */
	List<SceneGameInstance> querySceneGameInstanceByGameId(Long sceneGameId,Page<SceneGameInstance> page);
	
	/**
	 * editSceneGameInstanceStatus:更新活动实例的状态. <br/>
	 * Date: 2016年1月19日 下午6:43:30 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstance
	 * @return
	 */
	int editSceneGameInstanceStatus(SceneGameInstance sceneGameInstance);
	
	/**
	 * countOpenSceneGameInstance:查询当前活动下激活的实例数量. <br/>
	 * Date: 2016年1月19日 下午8:23:34 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameId
	 * @return
	 */
	int countOpenSceneGameInstance(Long sceneGameId);
	
	/**
	 * queryValidInstanceById:查询有效的活动实例信息. <br/>
	 * Date: 2016年1月21日 下午4:56:36 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
	SceneGameInstance queryValidInstanceById(Long sceneGameInstanceId);
	
	/**
	 * ValidInstanceByStartTime:验证活动是否开始. <br/>
	 * Date: 2016年1月25日 下午3:35:30 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param sceneGameInstanceId
	 * @return
	 */
    boolean ValidInstanceByStartTime(Long sceneGameInstanceId);
    
    /**
     * ValidInstanceByEndTime:验证活动是否结束. <br/>
     * Date: 2016年1月25日 下午3:36:04 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    boolean ValidInstanceByEndTime(Long sceneGameInstanceId);
    
    /**
     * 
     * querySceneGameWinRate:查询抽奖活动中奖率 <br/>
     * Date: 2016年2月17日 下午3:41:43 <br/>
     * scrmVersion 1.0
     * @author simon
     * @version jdk1.7
     * @param sceneGameId
     * @return
     */
    List<SceneGameInstanceExp> querySceneGameWinRate(SceneGameInstanceSearch search);
    
    /**
     * 
     * querySceneGameAwardsWinRate:查询抽奖活动各奖项的中奖率 <br/>
     * Date: 2016年2月17日 下午3:44:20 <br/>
     * scrmVersion 1.0
     * @author simon
     * @version jdk1.7
     * @param sceneGameInstanceId
     * @return
     */
    List<SceneGameInstanceExp> querySceneGameAwardsWinRate(Long sceneGameInstanceId);
    
    /**
     * findValidInstance:查询有效的活动. <br/>
     * TODO(这里描述这个方法适用条件 – 可选).<br/>
     * Date: 2016年3月1日 下午9:02:45 <br/>
     * scrmVersion 1.0
     * @author Tab.Wang(*^_^*)
     * @version jdk1.7
     * @return
     */
    SceneGameInstance queryValidInstance(String sceneGameCode);

    /**
     * queryValidInstanceByCode:查询有效的活动. <br/>
     * TODO(这里描述这个方法适用条件 – 可选).<br/>
     * Date: 2016年3月1日 下午9:02:45 <br/>
     * scrmVersion 1.0
     * @author chenjian(*^_^*)
     * @version jdk1.7
     * @return
     */
	SceneGameInstance queryValidInstanceByCode(String sceneGameInstanceCode);

	SceneGameInstance queryMapsQrCode(Long sceneGameInstanceId);
}

