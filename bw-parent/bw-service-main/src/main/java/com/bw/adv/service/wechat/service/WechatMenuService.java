/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansGroupService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月11日下午6:09:12
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatButton;
import com.bw.adv.module.wechat.model.WechatMenu;

/**
 * ClassName:WechatFansGroupService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午6:09:12 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMenuService extends BaseService<WechatMenu> {
	
	/**
	 * 
	 * queryAllMenu:查询微信菜单 <br/>
	 * Date: 2015年9月5日 下午1:59:51 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @return
	 */
	public List<WechatButton> queryAllMenu();

	/**
	 * 
	 * saveAndPub:保存并发布菜单 <br/>
	 * Date: 2015年9月14日 下午2:29:35 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param list
	 * @param newStr
	 * @throws Exception 
	 */
	public void saveAndPub(String newStr) throws Exception;
	
	public void addMenu(WechatMenu menu)throws Exception;
	
	public List<WechatMenu> findAllMenu()throws Exception;
	
	public int removeByParentId(Long id);
}

