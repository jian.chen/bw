package com.bw.adv.service.coupon.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.coupon.model.CouponType;
import com.bw.adv.module.coupon.model.exp.CouponData;
import com.bw.adv.module.coupon.repository.CouponTypeRepository;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.impl.ProductServiceImpl;
import com.bw.adv.service.coupon.service.CouponTypeService;

/**
 * ClassName: CouponTypeServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-12-15 下午3:45:13 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Service
public class CouponTypeServiceImpl extends BaseServiceImpl<CouponType> implements CouponTypeService {
	
	private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);
	
	@Value("${mater_date_comp_path}")
	private String MATER_DATE_COMP_PATH;
	@Value("${mater_date_promo_path}")
	private String MATER_DATE_PROMO_PATH;
	
	public final static String COUPON_TYPE_PROMO_PREFIX = "promo_";
	public final static String COUPON_TYPE_COMP_PREFIX = "comp_";
	
	
	private CouponTypeRepository couponTypeRepository;
	

	@Override
	public BaseRepository<CouponType, ? extends BaseMapper<CouponType>> getBaseRepository() {
		return couponTypeRepository;
	}
	
	@Override
	public void renovateCouponType() {
		String rulePathPromo = "masterData/rules/promotions-rule.xml";
		String rulePathComp = "masterData/rules/comps-rule.xml";
		renovateCouponType(rulePathPromo, MATER_DATE_PROMO_PATH,COUPON_TYPE_PROMO_PREFIX);
		renovateCouponType(rulePathComp, MATER_DATE_COMP_PATH,COUPON_TYPE_COMP_PREFIX);
	}

	private void renovateCouponType(String rulePath,String dataPath,String prefix) {
		Digester digester = null;
		ClassLoader loader = null;
		CouponData couponData = null;
		CouponType couponTypeTmp = null;
		List<CouponType> couponTypeOldList = null;
		List<CouponType> newCouponTypeList = null;
		List<CouponType> updateCouponTypeList = null;
		List<CouponType> couponTypeMasterList = null;
		Map<String, CouponType> couponTypeMap = null;
		File file = null;
		try {
			
	    	loader = Thread.currentThread().getContextClassLoader();
	    	digester = DigesterLoader.createDigester(loader.getResource(rulePath));
	    	
	    	file = new File(dataPath);
	    	logger.info("*******优惠券"+prefix+"同步时间："+DateUtils.getCurrentTimeOfDb()+"----"+dataPath);
	    	if(!file.exists()){
	    		logger.info("*******优惠券主数据不存在*********");
	    		return;
	    	}
	    	
	    	couponData = (CouponData) digester.parse(file);
	    	couponTypeMasterList = couponData.getCouponTypeList();
	    	
	    	
	    	newCouponTypeList = new ArrayList<CouponType>();
	    	updateCouponTypeList = new ArrayList<CouponType>();
	    	
	    	couponTypeOldList = couponTypeRepository.findAll();
	    	couponTypeMap = new HashMap<String, CouponType>();
	    	
	    	for (CouponType couponType : couponTypeOldList) {
	    		couponTypeMap.put(couponType.getCouponTypeCode(), couponType);
			}
	    	
	    	
	    	for (CouponType couponType : couponTypeMasterList) {
	    		couponType.setCouponTypeCode(prefix+couponType.getCouponTypeCode());
	    		couponTypeTmp = couponTypeMap.get(couponType.getCouponTypeCode());
	    		//如果包含，则为原数据重新复制
	    		if(couponTypeTmp != null){
	    			BeanUtils.copyNotNull(couponType, couponTypeTmp);
	    			updateCouponTypeList.add(couponTypeTmp);
	    		}else{
	    			newCouponTypeList.add(couponType);
	    		}
	    	}
	    	if(updateCouponTypeList.size() > 0){
	    		couponTypeRepository.updateList(updateCouponTypeList);
	    	}
	    	if(newCouponTypeList.size() > 0){
	    		couponTypeRepository.saveList(newCouponTypeList);
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Autowired
	public void setCouponTypeRepository(CouponTypeRepository couponTypeRepository) {
		this.couponTypeRepository = couponTypeRepository;
	}
}
