/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:GoodsShowSeviceImpl.java
 * Package Name:com.sage.scrm.service.points.service.impl
 * Date:2015年12月9日上午11:15:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service.impl;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.coupon.model.exp.CouponResult;
import com.bw.adv.module.goods.model.GoodsExchangeItem;
import com.bw.adv.module.goods.model.GoodsShow;
import com.bw.adv.module.goods.model.GoodsShowCategory;
import com.bw.adv.module.goods.model.exp.GoodsShowExp;
import com.bw.adv.module.goods.repository.GoodsExchangeRepository;
import com.bw.adv.module.goods.repository.GoodsShowCategoryRepository;
import com.bw.adv.module.goods.repository.GoodsShowRepository;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.service.coupon.service.CouponIssueService;
import com.bw.adv.service.points.service.GoodsShowSevice;
import com.bw.adv.service.points.service.MemberPointsItemSevice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * ClassName:GoodsShowSeviceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 上午11:15:06 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class GoodsShowSeviceImpl extends BaseServiceImpl<GoodsShow> 
	implements GoodsShowSevice{

	private GoodsShowRepository goodsShowRepository;
	private GoodsShowCategoryRepository goodsShowCategoryRepository;
	private GoodsExchangeRepository goodsExchangeRepository;
	private CouponIssueService couponIssueService;
	private MemberPointsItemSevice memberPointsItemSevice;
	
	@Override
	public BaseRepository<GoodsShow, ? extends BaseMapper<GoodsShow>> getBaseRepository() {
		return goodsShowRepository;
	}

	
	@Override
	public List<GoodsShowExp> queryList(Page<GoodsShowExp> pageObj, Long statusId) {
		return goodsShowRepository.findListByStatus(pageObj, statusId);
	}
	


	@Override
	public List<GoodsShowCategory> queryGoodsShowCategory(Page<GoodsShowCategory> pageObj) {
		return goodsShowCategoryRepository.findListByStatus(pageObj, StatusConstant.GOODS_SHOW_CATEGORY_ENABLE.getId());
	}


	@Override
	public int deleteGoodsShowCategoryList(String goodsShowCategoryIds) {                 
		return goodsShowCategoryRepository.removeList(goodsShowCategoryIds, StatusConstant.GOODS_SHOW_CATEGORY_DISENABLE.getId());
	}



	@Override
	@Transactional
	public void exchangePointsProduct(GoodsExchangeItem goodsExchangeItem,Long couponId,String source) throws Exception {
		CouponResult couponResult = null;
		List<CouponResult> couponList = null;
		
		couponResult = new CouponResult();
		couponResult.setCouponId(couponId);
		couponResult.setQuantity(1);
		couponList = new ArrayList<CouponResult>();
		couponList.add(couponResult);
		if(null!=source && source.equals("alipay")){
			//couponIssueService.insertCouponIssue(goodsExchangeItem.getMemberId(), couponList, ChannelConstant.GET_COUPON_ALIPAY_EXCHANGE.getId(), null,WechatMsgBusinessTypeEnum.GET_COUPON_EXCHANGE.getId());//发放优惠券
			couponIssueService.insertCouponIssueSingle(goodsExchangeItem.getMemberId(), couponId, ChannelConstant.GET_COUPON_ALIPAY_EXCHANGE.getId(), null, WechatMsgBusinessTypeEnum.GET_COUPON_EXCHANGE.getId());
		}else{
			couponIssueService.insertCouponIssueSingle(goodsExchangeItem.getMemberId(), couponId, ChannelConstant.GET_COUPON_EXCHANGE.getId(), null, WechatMsgBusinessTypeEnum.GET_COUPON_EXCHANGE.getId());
			//couponIssueService.insertCouponIssue(goodsExchangeItem.getMemberId(), couponList, ChannelConstant.GET_COUPON_EXCHANGE.getId(), null,WechatMsgBusinessTypeEnum.GET_COUPON_EXCHANGE.getId());//发放优惠券
		}
		goodsExchangeRepository.saveSelective(goodsExchangeItem);//已使用皇冠
		
		//扣减积分
		memberPointsItemSevice.subtractPoints(goodsExchangeItem.getMemberId(), goodsExchangeItem.getPointsNumber(), StatusConstant.MEMBER_POINTS_ITEM_USED.getCode(), goodsExchangeItem.getRemark());
	}
	
	
	@Override
	public int deleteList(String goodsShowIds) {
		
		return goodsShowRepository.removeList(goodsShowIds, StatusConstant.GOODS_SHOW_REMOVE.getId());
	}
	
	@Override
	public GoodsShowExp queryExpByPk(String goodsShowId) {
		
		return goodsShowRepository.findExpByPk(goodsShowId);
	}

	@Override
	public GoodsShowCategory queryCategoryDetail(Long goodsShowCategoryId) {
		
		return goodsShowCategoryRepository.findByPk(goodsShowCategoryId);
	}

	@Override
	public void updateCategory(GoodsShowCategory goodsShowCategory) {
		
		goodsShowCategoryRepository.updateByPkSelective(goodsShowCategory);
	}
	
	@Override
	public void saveCategory(GoodsShowCategory goodsShowCategory) {
		
		goodsShowCategoryRepository.saveSelective(goodsShowCategory);
		
	}

	@Autowired
	public void setGoodsShowRepository(GoodsShowRepository goodsShowRepository) {
		this.goodsShowRepository = goodsShowRepository;
	}

	@Autowired
	public void setGoodsExchangeRepository(
			GoodsExchangeRepository goodsExchangeRepository) {
		this.goodsExchangeRepository = goodsExchangeRepository;
	}

	@Autowired
	public void setCouponIssueService(CouponIssueService couponIssueService) {
		this.couponIssueService = couponIssueService;
	}

	@Autowired
	public void setGoodsShowCategoryRepository(
			GoodsShowCategoryRepository goodsShowCategoryRepository) {
		this.goodsShowCategoryRepository = goodsShowCategoryRepository;
	}


	@Autowired
	public void setMemberPointsItemSevice(MemberPointsItemSevice memberPointsItemSevice) {
		this.memberPointsItemSevice = memberPointsItemSevice;
	}
	
	
}

