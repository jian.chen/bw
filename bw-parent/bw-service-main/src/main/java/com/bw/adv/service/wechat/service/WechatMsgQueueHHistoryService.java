/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMsgQueueHHistoryService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年12月16日上午11:27:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatMsgQueueHHistory;

/**
 * ClassName:WechatMsgQueueHHistoryService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月16日 上午11:27:22 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMsgQueueHHistoryService extends BaseService<WechatMsgQueueHHistory>{
	public void saveAllWechatMsgQueueHHistory(List<WechatMsgQueueHHistory> historys);
}

