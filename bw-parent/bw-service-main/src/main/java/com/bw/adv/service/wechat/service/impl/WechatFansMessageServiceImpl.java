/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansMessageServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月25日下午5:02:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatFansMessage;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.repository.WechatFansMessageRepository;
import com.bw.adv.service.wechat.service.WechatFansMessageService;

/**
 * ClassName:WechatFansMessageServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午5:02:54 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatFansMessageServiceImpl extends BaseServiceImpl<WechatFansMessage> implements WechatFansMessageService {

	private WechatFansMessageRepository	 wechatFansMessageRepository;

	@Override
	public BaseRepository<WechatFansMessage, ? extends BaseMapper<WechatFansMessage>> getBaseRepository() {
		return wechatFansMessageRepository;
	}
	
	@Override
	public void saveWechatFansMessage(Map<String, String> map) {
		WechatFansMessage WechatFansMessage =null;
		
		WechatFansMessage =new WechatFansMessage();
		if(map.get("content")!=null){
			WechatFansMessage.setContent(map.get("content"));
		}
		if(map.get("mediaId")!=null){
			WechatFansMessage.setMediaId(map.get("mediaId"));
		}
		if(map.get("pictureUrl")!=null){
			WechatFansMessage.setPictureUrl(map.get("pictureUrl"));
		}
		if(map.get("wechatFanId")!=null&&!map.get("wechatFanId").equals("null")){
			WechatFansMessage.setFssFansid(Long.valueOf(map.get("wechatFanId")));
		}
		if(map.get("fromUsername")!=null){
			WechatFansMessage.setFssOpenid(map.get("fromUsername"));
		}
		if(map.get("msgType")!=null){
			WechatFansMessage.setMsgType(map.get("msgType"));
		}
		if(map.get("isReply")!=null){
			WechatFansMessage.setIsReply(map.get("isReply"));
		}
		if(map.get("isKeyWorld")!=null){
			WechatFansMessage.setIsKeyword(map.get("isKeyWorld"));
		}
		if(map.get("isRead")!=null){
			WechatFansMessage.setIsRead(map.get("isRead"));
		}
		WechatFansMessage.setFssCreateddate(DateUtils.getCurrentTimeOfDb());
		this.save(WechatFansMessage);
		
	}
	
	@Autowired
	public void setWechatFansMessageRepository(
			WechatFansMessageRepository wechatFansMessageRepository) {
		this.wechatFansMessageRepository = wechatFansMessageRepository;
	}
	
	
}

