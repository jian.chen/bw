/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SysRoleService.java
 * Package Name:com.sage.scrm.service.sys.service
 * Date:2015-8-14下午4:47:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.sys.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.sys.model.OperationItem;

/**
 * ClassName: OperationItemService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-18 上午11:59:56 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Service
public interface OperationItemService extends BaseService<OperationItem> {


	/**
	 * findListForSysRole:查询操作明细表list. <br/>
	 * Date: 2015-8-18 下午12:00:01 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 */
	List<OperationItem> findListForOperationItem();
	
}

