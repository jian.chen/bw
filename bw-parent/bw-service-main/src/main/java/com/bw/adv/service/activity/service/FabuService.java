package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.module.activity.model.FabuChannel;


public interface FabuService {
	
	public List<FabuChannel> queryAllGroupByChannel();

}
