/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CodeSeqTypeService.java
 * Package Name:com.sage.scrm.service.sys.service
 * Date:2016年4月6日下午5:30:38
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.sys.service;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.base.model.CodeSeqType;

/**
 * ClassName:CodeSeqTypeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年4月6日 下午5:30:38 <br/>
 * scrmVersion 1.0
 * @author   chengdi.cui
 * @version  jdk1.7
 * @see 	 
 */
public interface CodeSeqTypeService extends BaseService<CodeSeqType>{
	
	/**
     * queryByTypeCode:(按编码查询). <br/>
     * @author chengdi.cui
     * @return
     */
    public CodeSeqType queryByTypeCode(@Param("typeCode")String typeCode);

}

