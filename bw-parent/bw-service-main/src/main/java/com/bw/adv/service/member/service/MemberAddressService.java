/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberAddressService.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2015年12月14日下午2:50:53
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.model.MemberAddress;

/**
 * ClassName:MemberAddressService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月14日 下午2:50:53 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberAddressService extends BaseService<MemberAddress> {
	
	/**
	 * queryMemberAddressByMemberId:(根据memberId查询会员受奖地址). <br/>
	 * Date: 2015年12月17日 下午2:05:01 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param valueOf
	 * @return
	 */
	MemberAddress queryMemberAddressByMemberId(Long memberId);
	
	
}

