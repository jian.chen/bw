/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMsgQueueService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年12月9日下午9:48:38
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatMsgQueue;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueExp;
/**
 * ClassName:WechatMsgQueueService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午9:48:38 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMsgQueueService extends BaseService<WechatMsgQueue>{
	public List<WechatMsgQueueExp> findAllWechatMsgQueues(Long limit);
	public int deleteByPrimaryKey(Long wechatMsgQueueId);
	public void deleteWechatMsgQueue(List<WechatMsgQueueExp> list);
	public void deleteAllWechatMsgQueue(List<Long> list);
	public void updateBatchWechatMsgQueue(List<WechatMsgQueueExp> list);
	
	/**
	 * saveWechatMsgQueue:保存消息<br/>
	 * Date: 2016年1月7日 下午7:52:47 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param list
	 */
	void saveWechatMsgQueue(List<WechatMsgQueue> list);
	
	
	
	/**
	 * sendWechatMsg:(发送微信消息). <br/>
	 * Date: 2016-5-5 下午3:41:25 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	void sendWechatMsg();
	
	/**
	 * sendWechatMsgMultiThread:(多线程发送微信消息). <br/>
	 * Date: 2016-5-6 上午11:25:58 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	void sendWechatMsgMultiThread();
}

