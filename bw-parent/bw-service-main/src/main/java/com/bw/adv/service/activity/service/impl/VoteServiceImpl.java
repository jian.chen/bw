package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.VoteOptions;
import com.bw.adv.module.activity.model.VoteResult;
import com.bw.adv.module.activity.model.VoteTitle;
import com.bw.adv.module.activity.repository.VoteOptionsRepository;
import com.bw.adv.module.activity.repository.VoteResultRepository;
import com.bw.adv.module.activity.repository.VoteTitleRepository;
import com.bw.adv.service.activity.service.VoteService;

@Service
public class VoteServiceImpl implements VoteService{
	
	private VoteTitleRepository voteTitleRepository;
	
	private VoteOptionsRepository voteOptionsRepository;
	
	private VoteResultRepository voteResultRepository;

	@Autowired
	public void setVoteOptionsRepository(VoteOptionsRepository voteOptionsRepository) {
		this.voteOptionsRepository = voteOptionsRepository;
	}

	@Autowired
	public void setVoteResultRepository(VoteResultRepository voteResultRepository) {
		this.voteResultRepository = voteResultRepository;
	}

	@Autowired
	public void setVoteTitleRepository(VoteTitleRepository voteTitleRepository) {
		this.voteTitleRepository = voteTitleRepository;
	}
	
	@Override
	public List<VoteTitle> findAllVoteAndOptionCount(Page<VoteTitle> templatePage) {
		return voteTitleRepository.findAllVoteAndOptionCount(templatePage);
	}

	@Override
	public int saveVoteTitle(VoteTitle voteTitle) {
		return this.voteTitleRepository.saveSelective(voteTitle);
	}

	@Override
	public VoteTitle findVoteTitleById(Long voteId) {
		return this.voteTitleRepository.findByPk(voteId);
	}

	@Override
	public void updateVoteTitle(VoteTitle votetitle) {
		this.voteTitleRepository.updateByPkSelective(votetitle);
	}

	@Override
	public void deleteVoteTitle(Long voteId) {
		this.voteTitleRepository.removeByPk(voteId);
	}

	@Override
	public List<VoteOptions> findVoteOptionsByTitleId(Long voteId, Page<VoteOptions> templatePage) {
		return this.voteOptionsRepository.findVoteOptionsByTitleId(voteId, templatePage);
	}

	@Override
	public void saveVoteOptions(VoteOptions voteOptions) {
		this.voteOptionsRepository.saveSelective(voteOptions);
	}

	@Override
	public void updateVoteOptions(VoteOptions voteOptions) {
		this.voteOptionsRepository.updateByPkSelective(voteOptions);
	}

	@Override
	public VoteOptions findOptionsById(Long optionsId) {
		return this.voteOptionsRepository.findByPk(optionsId);
	}

	@Override
	public void deleteVoteOptions(Long optionsId) {
		this.voteOptionsRepository.removeByPk(optionsId);
	}

	@Override
	public Long selectCountForVoteResult(Long titleId) {
		return this.voteResultRepository.selectCountForVoteResult(titleId);
	}

	@Override
	public List<VoteResult> findVoteResultByTitleId(Long titleId) {
		return this.voteResultRepository.findVoteResultByTitleId(titleId);
	}

	@Override
	public Long queryActiveVoteAmount() {
		return this.voteTitleRepository.queryActiveVoteAmount();
	}

	@Override
	public VoteTitle findActiveVote() {
		return this.voteTitleRepository.findActiveVote();
	}

	@Override
	public void saveVoteResult(List<VoteResult> voteResult) {
		this.voteResultRepository.saveList(voteResult);
	}

}
