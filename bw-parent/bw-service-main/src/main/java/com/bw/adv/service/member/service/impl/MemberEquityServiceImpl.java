package com.bw.adv.service.member.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.member.equity.repository.EquityGradeRelationRepository;
import com.bw.adv.module.member.equity.repository.MemberEquityRepository;
import com.bw.adv.module.member.equity.search.MemberEquitySearch;
import com.bw.adv.module.member.model.EquityGradeRelation;
import com.bw.adv.module.member.model.MemberEquity;
import com.bw.adv.module.member.model.exp.MemberEquityExp;
import com.bw.adv.service.member.service.MemberEquityService;

@Service
public class MemberEquityServiceImpl extends BaseServiceImpl<MemberEquity> implements MemberEquityService {
    
	private MemberEquityRepository memberEquityRepository;
	
	@Autowired
	private EquityGradeRelationRepository egRelationRepository;
	
	@Override
	public List<MemberEquityExp> queryListSearchAuto(MemberEquitySearch search) {
		/*String startTime = "";
		String endTime = "";
		List<MemberEquityExp> equityList = null;
		
		equityList = memberEquityRepository.findListSearchAuto(search);
		for(MemberEquityExp mex : equityList){
			if(!StringUtil.isEmpty(mex.getEquityStarttime()) && !StringUtil.isEmpty(mex.getEquityEndtime())){
			   startTime = DateUtils.dateStrToNewFormat(mex.getEquityStarttime(),DateUtils.DATE_PATTERN_YYYYMMDD,DateUtils.DATE_PATTERN_YYYYMMDD_2);
			   endTime = DateUtils.dateStrToNewFormat(mex.getEquityEndtime(),DateUtils.DATE_PATTERN_YYYYMMDD,DateUtils.DATE_PATTERN_YYYYMMDD_2);
			   mex.setEquityStarttime(startTime+" 至 "+endTime);					
			}else{
				mex.setEquityStarttime("-");	
			}
			if(StringUtil.isEmpty(mex.getGradeName())){
				mex.setGradeName("-");
			}
		}
		return equityList;*/
		return memberEquityRepository.findListSearchAuto(search);
	}
	
	@Override
	public Long queryCountSearchAuto(MemberEquitySearch search) {
		return memberEquityRepository.findCountSearchAuto(search);
	}
	
	@Override
	public MemberEquity queryInfoById(Long memberEquityid) {
		return memberEquityRepository.findInfoById(memberEquityid);
	}

	@Override
	public int updateMemberEquity(MemberEquity memberEquity,String gradeIds) {
		String starttime = null;
		String endtime = null;
		
		starttime = memberEquity.getEquityStarttime();
		starttime = starttime.replace("-", "");
		endtime = memberEquity.getEquityEndtime();
		endtime = endtime.replace("-", "");
		memberEquity.setEquityStarttime(starttime);
		memberEquity.setEquityEndtime(endtime);
		
		//删除旧的权益和等级的关系数据
		egRelationRepository.deleteByEquityId(memberEquity.getMemberEquityId());
		
		//维护新的关系数据
		if(gradeIds.length() > 0){
			for(int i = 0; i < gradeIds.split(",").length; i++ ){
				EquityGradeRelation egRelation = new EquityGradeRelation();
				egRelation.setEquityId(memberEquity.getMemberEquityId());
				egRelation.setGradeId(Long.parseLong(gradeIds.split(",")[i]));
				egRelationRepository.saveSelective(egRelation);
			}			
		}
		
		return memberEquityRepository.updateMemberEquity(memberEquity);
	}
	
	@Override
	public int saveMemberEquity(MemberEquity memberEquity, String gradeIds) {
		int equityid = 0;
		String starttime = null;
		String endtime = null;
		
		starttime = memberEquity.getEquityStarttime();
		starttime = starttime.replace("-", "");
		endtime = memberEquity.getEquityEndtime();
		endtime = endtime.replace("-", "");
		memberEquity.setEquityStarttime(starttime);
		memberEquity.setEquityEndtime(endtime);
		
		//新增会员权益信息
		equityid = memberEquityRepository.insertMemberEquity(memberEquity);		
		
		//维护会员权益与会员等级间的关系
		if(gradeIds.length() > 0){
			for(int i = 0; i < gradeIds.split(",").length; i++ ){
				EquityGradeRelation egRelation = new EquityGradeRelation();
				egRelation.setEquityId(memberEquity.getMemberEquityId());
				egRelation.setGradeId(Long.parseLong(gradeIds.split(",")[i]));
				egRelationRepository.saveSelective(egRelation);
			}
		}
		
		return equityid;
	}

	@Override
	public int removeMemberEquity(Long memberEquityId) {
		MemberEquity memberEquity= new MemberEquity();
		memberEquity.setMemberEquityId(memberEquityId);
		memberEquity.setStatusId(StatusConstant.MEMBER_EQUITY_DISENABLE.getId().toString());
		return memberEquityRepository.removeMemberEquity(memberEquity);
	}

	@Override
	public BaseRepository<MemberEquity, ? extends BaseMapper<MemberEquity>> getBaseRepository() {
		return memberEquityRepository;
	}
	
	@Autowired 
	public void setMemberEquityRepository(
			MemberEquityRepository memberEquityRepository) {
		this.memberEquityRepository = memberEquityRepository;
	}

	@Override
	public List<MemberEquity> selectListByGradeId(Long gradeId,String nowDate) {
		return this.memberEquityRepository.selectListByGradeId(gradeId,nowDate);
	}


}
