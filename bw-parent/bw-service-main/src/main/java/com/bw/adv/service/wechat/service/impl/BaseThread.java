/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:BaseThread.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2016-5-5下午5:02:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.List;

/**
 * ClassName:BaseThread <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016-5-5 下午5:02:42 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public abstract class BaseThread implements Runnable{

	/** 线程出错后重新运行次数 */  
    private int errorTimes = 0;  
  
    /** 最多同时执行任务数 */  
//    private static int maxThread = 2;  
  
    /** 线程使用状态：false未使用、true使用中 */  
    private String thread_states = "false";  
      
  
  
    /**  
     * 得到空闲线程、并传递参数 
     * @param threads 线程池 
     * @param args 参数  
     * @return 
     */  
    protected static BaseThread getCurrentThread(List<BaseThread> threads, Object ... args) {  
        for(BaseThread baseThread:threads){  
            if(baseThread.thread_states.equals("false")){  
            	baseThread.thread_states = "true";// 标记使用中  
            	baseThread.setArgs(args);
                return baseThread;  
            }
        }
        try {
            Thread.sleep(1500L);
        } catch (Exception e) {
            e.printStackTrace();  
        }  
        return getCurrentThread(threads, args);  
    }  
      
    public void close(){  
        this.thread_states = "false";  
    }  
  
    /** 执行代码 */  
    public abstract void runCode() throws Exception;  
      
    /** 设置参数 */  
    public abstract void setArgs(Object ... args);
      
    /** 异常获取、重新执行代码  */  
    @Override
    public void run() {
        try {
            runCode();
            this.close();  
        } catch (Exception e) {  
            e.printStackTrace();
            if(getErrorTimes()>0){  
                setErrorTimes(getErrorTimes() - 1);   
                run();
            }  
            this.close();  
        }
    }     
      
      
  
    // --------------------------------------  
  
    public int getErrorTimes() {  
        return errorTimes;  
    }  
  
    public void setErrorTimes(int errorTimes) {  
        this.errorTimes = errorTimes;  
    }  
  
//    public static int getMaxThread() {  
//        return maxThread;
//    }  
//  
//    public static void setMaxThread(int maxThread) {  
//    	BaseThread.maxThread = maxThread;  
//    }  
  
    public String getThread_states() {  
        return thread_states;  
    }
  
    public void setThread_states(String threadStates) {  
        thread_states = threadStates;  
    }
	
}

