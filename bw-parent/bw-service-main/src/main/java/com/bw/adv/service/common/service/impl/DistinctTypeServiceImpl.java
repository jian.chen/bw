/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.model.DistinctType;
import com.bw.adv.module.common.repository.DistinctTypeRepository;
import com.bw.adv.service.common.service.DistinctTypeService;

/**
 * ClassName:DistinctTypeService <br/>
 * Date: 2015-8-11 下午1:50:36 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class DistinctTypeServiceImpl extends BaseServiceImpl<DistinctType> implements DistinctTypeService{

	private DistinctTypeRepository distinctTypeRepository;
	
	@Override
	public BaseRepository<DistinctType, ? extends BaseMapper<DistinctType>> getBaseRepository() {
		return distinctTypeRepository;
	}

	@Autowired
	public void setDistinctTypeRepository(DistinctTypeRepository distinctTypeRepository) {
		this.distinctTypeRepository = distinctTypeRepository;
	}

}

