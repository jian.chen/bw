/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WeChatFansGroupServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月11日下午6:14:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.sys.repository.WechatAccountRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.api.ApiWeChatMenu;
import com.bw.adv.module.wechat.model.WechatAccount;
import com.bw.adv.module.wechat.model.WechatButton;
import com.bw.adv.module.wechat.model.WechatMenu;
import com.bw.adv.module.wechat.repository.WechatMenuRepository;
import com.bw.adv.service.wechat.service.WechatMenuService;

/**
 * ClassName:WeChatFansGroupServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午6:14:34 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatMenuServiceImpl extends BaseServiceImpl<WechatMenu> implements WechatMenuService {
	protected final Logger logger = Logger.getLogger(WechatMenuServiceImpl.class);
	private WechatMenuRepository wechatMenuRepository;
	private WechatAccountRepository wechatAccountRepository;
	private CodeService codeService;
	private ApiWeChatMenu apiWeChatMenu;


	@Override
	public BaseRepository<WechatMenu, ? extends BaseMapper<WechatMenu>> getBaseRepository() {
		return wechatMenuRepository;
	}

	@Override
	public List<WechatButton> queryAllMenu() {
		
		return wechatMenuRepository.findAllMenu();
	}
	
	public WechatMenuRepository getWechatMenuRepository() {
		return wechatMenuRepository;
	}
	@Autowired
	public void setWechatMenuRepository(WechatMenuRepository wechatMenuRepository) {
		this.wechatMenuRepository = wechatMenuRepository;
	}
	
	@Override
	@Transactional
	public void saveAndPub(String newStr) throws Exception {
		//删除所有菜单
		this.wechatMenuRepository.removeAllMenu();
		 JSONObject jsonobject = JSONObject.fromObject(newStr);
		 List<WechatButton> subList = null;
		 WechatMenu wechatMenu = null;
		 WechatButton subButton = null;
		 List<WechatAccount> accountList = null;
		 WechatAccount account = null;
		 Long wechatMenuId = null;
		   //获取一个json数组
		   JSONArray array = jsonobject.getJSONArray("button");
		   for (int i = 0; i < array.size(); i++) {   
	            JSONObject object = (JSONObject)array.get(i);  
	            WechatButton button = (WechatButton)JSONObject.toBean(object, WechatButton.class);
	            if(button != null){
	            	wechatMenu = dualButtonToMenu(button);
	            	accountList = this.wechatAccountRepository.findAll();
	            	if(accountList != null){
	            		account = accountList.get(0);
	            		wechatMenu.setWechatAccountId(account.getWechatAccountId());
	            	}
	            	wechatMenu.setSort(i+1);
	            	wechatMenu.setPareWechatMenuId(null);
	            	this.wechatMenuRepository.saveSelective(wechatMenu);
	            	wechatMenuId = wechatMenu.getWechatMenuId();
	            	subList = button.getSub_button();
	            	if(subList !=null && subList.size()>0){
	            		for(int j=0;j<subList.size();j++){
	            			subButton = subList.get(j);
	            			wechatMenu = dualButtonToMenu(subButton);
	            			if(account != null){
	    	            		wechatMenu.setWechatAccountId(account.getWechatAccountId());
	    	            	}
	    	            	wechatMenu.setSort(j+1);
	    	            	wechatMenu.setPareWechatMenuId(wechatMenuId);
	    	            	this.wechatMenuRepository.saveSelective(wechatMenu);
	            		}
	            	}
	            }  
		   }
		   
		   this.apiWeChatMenu.menuCreate(newStr);
	}

	private WechatMenu dualButtonToMenu(WechatButton button) throws Exception {
		WechatMenu menu = new WechatMenu();
		if(StringUtils.isNotEmpty(button.getMedia_id())){
			menu.setMessageTemplateId(Long.valueOf(button.getMedia_id()));
 		}
		menu.setWechatMenuName(button.getMenu_name());
		menu.setMsgType(button.getType());
		menu.setMenuKey(button.getMenu_key());
		menu.setUrl(button.getUrl());
		menu.setCreateTime(DateUtils.getCurrentTimeOfDb());
		menu.setWechatMenuCode(codeService.generateCode(WechatMenu.class));
		menu.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
		return menu;
	}
	
	public int removeByParentId(Long id){
		return this.wechatMenuRepository.removeByParentId(id);
	}
	
	public List<WechatMenu> findAllMenu()throws Exception{
		return this.wechatMenuRepository.findAllMenus();
	}
	
	@Override
	@Transactional
	public void addMenu(WechatMenu menu) throws Exception {
		this.wechatMenuRepository.save(menu);
	}
	
	public WechatAccountRepository getWechatAccountRepository() {
		return wechatAccountRepository;
	}

	@Autowired
	public void setWechatAccountRepository(
			WechatAccountRepository wechatAccountRepository) {
		this.wechatAccountRepository = wechatAccountRepository;
	}
	public CodeService getCodeService() {
		return codeService;
	}

	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}
	
	public ApiWeChatMenu getApiWeChatMenu() {
		return apiWeChatMenu;
	}
	@Autowired
	public void setApiWeChatMenu(ApiWeChatMenu apiWeChatMenu) {
		this.apiWeChatMenu = apiWeChatMenu;
	}

}

