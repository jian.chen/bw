package com.bw.adv.service.sys.service.impl;

import java.security.KeyManagementException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.sys.repository.WechatAccountConfigRepository;
import com.bw.adv.module.sys.repository.WechatAccountRepository;
import com.bw.adv.module.wechat.api.ApiWeChat;
import com.bw.adv.module.wechat.common.enums.WechatAccountConfigStatus;
import com.bw.adv.module.wechat.model.WechatAccount;
import com.bw.adv.module.wechat.model.WechatAccountConfig;
import com.bw.adv.module.wechat.model.exp.WechatAccountExp;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.exception.WechatAccountException;
import com.bw.adv.service.sys.service.WechatAccountService;
import com.bw.adv.service.wechat.msg.view.WechatConfigView;

@Service
public class WechatAccountServiceImpl extends BaseServiceImpl<WechatAccount> implements WechatAccountService {

	private static final long WECHAT_CONFIG_ID = 1l;
    private static final long WECHAT_ACCOUNT_ID = 1l;
    private static final Logger LOGGER = Logger.getLogger(WechatAccountServiceImpl.class);
	
	private WechatAccountRepository wechatAccountRepository;
	private WechatAccountConfigRepository wechatAccountConfigRepository;
	private AccessTokenCacheService accessTokenCacheService;
	
	/**
     * 初始化加载数据库中的wechat配置参数
     */
    @PostConstruct
    private void init() {
        WechatAccountConfig wechatAccountConfig = wechatAccountConfigRepository.findByPk(WECHAT_CONFIG_ID);
        if (wechatAccountConfig == null
                || StringUtils.isBlank(wechatAccountConfig.getAppId())
                || StringUtils.isBlank(wechatAccountConfig.getAppSecret())) {
            LOGGER.error("wechatconfig error, no config or no [appId, appSecret]");
        }
        ApiWeChat.WECHAT_CONFIG = new ApiWeChat.WeChatConfig(wechatAccountConfig.getAppId(), wechatAccountConfig.getAppSecret());
    }
	
	@Override
	public BaseRepository<WechatAccount, ? extends BaseMapper<WechatAccount>> getBaseRepository() {
		return wechatAccountRepository;
	}
	
	@Override
	public WechatAccountConfig queryWechatAccountByPk(Long wechatAccountId) {
		
		return wechatAccountConfigRepository.findByWechatAccountId(wechatAccountId);
	}
	
	/**
	 * 
	 * 保存配置信息.
	 * @see com.bw.adv.service.sys.service.WechatAccountService#saveWechatAccountConfig(com.bw.adv.module.wechat.model.WechatAccountConfig)
	 */
	@Override
	public int saveWechatAccountConfig(WechatAccountConfig wechatAccountConfig) {
        WechatAccountConfig wechatcog = null;
        //查询公众账号的配置信息
        wechatcog = wechatAccountConfigRepository.findByWechatAccountId(WECHAT_CONFIG_ID);
        if (wechatcog == null)
            throw new WechatAccountException("no default wechat config");
        wechatcog.setAppId(wechatAccountConfig.getAppId());
        wechatcog.setAppSecret(wechatAccountConfig.getAppSecret());
        wechatcog.setConnectStatus(WechatAccountConfigStatus.NOT_CONNECTED.toString());
        return wechatAccountConfigRepository.updateByPkSelective(wechatcog);
    }
	
	@Override
	public WechatAccountExp queryExpByPk(Long wechatAccountId) {
		
		return wechatAccountRepository.findExpByPk(wechatAccountId);
	}

	
	@Override
	public WechatAccountExp queryWechatAccount() {
		List<WechatAccountExp> expList = null;
		expList = wechatAccountRepository.findExp();
		if(expList.size()>0){
			return expList.get(0);
		}
		return null;
	}
	
	
	@Autowired
	public void setWechatAccountRepository(
			WechatAccountRepository wechatAccountRepository) {
		this.wechatAccountRepository = wechatAccountRepository;
	}

	@Autowired
	public void setWechatAccountConfigRepository(
			WechatAccountConfigRepository wechatAccountConfigRepository) {
		this.wechatAccountConfigRepository = wechatAccountConfigRepository;
	}

	@Override
	public int updateWechatAccountAndConfig(WechatConfigView wechatConfigView) {
        int result = 0;
        WechatAccount wechatAccount = new WechatAccount();
        WechatAccountConfig wechatAccountConfig = new WechatAccountConfig();
        wechatAccount.setWechatAccountName(wechatConfigView.getWechatAccountName());
        wechatAccount.setAccountNumber(wechatConfigView.getWechatAccountNumber());
        wechatAccount.setOriginalId(wechatConfigView.getOriginalId());
        wechatAccount.setWechatAccountId(WECHAT_ACCOUNT_ID);
        wechatAccountConfig.setAppId(wechatConfigView.getAppId());
        wechatAccountConfig.setAppSecret(wechatConfigView.getAppSecret());
        wechatAccountConfig.setWechatAccountConfigId(WECHAT_CONFIG_ID);
        result += this.updateByPkSelective(wechatAccount);
        result += this.saveWechatAccountConfig(wechatAccountConfig);
        // 更新缓存中的wechat相关信息
        ApiWeChat.WeChatConfig weChatConfig = new ApiWeChat.WeChatConfig(wechatConfigView.getAppId(), wechatConfigView.getAppSecret());
        ApiWeChat.WECHAT_CONFIG = weChatConfig;
        return result;
    }

	@Override
	public boolean testWechatConnect() {
        WechatAccountConfig config = new WechatAccountConfig();
        config.setWechatAccountConfigId(WECHAT_CONFIG_ID);
        config.setConnectStatus(WechatAccountConfigStatus.CONNECTED.getKey().toString());
        try {
            accessTokenCacheService.forceRefreshAccessToken(true);
        } catch (KeyManagementException e) {
            config.setConnectStatus(WechatAccountConfigStatus.NOT_CONNECTED.getKey().toString());
            wechatAccountConfigRepository.updateWechatConnectStatus(config);
            return false;
        }
        wechatAccountConfigRepository.updateWechatConnectStatus(config);
        return true;
    }

	@Autowired
	public void setAccessTokenCacheService(AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}

}
