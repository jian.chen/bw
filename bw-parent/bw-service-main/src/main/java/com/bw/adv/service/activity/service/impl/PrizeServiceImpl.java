/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PrizeServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015年11月23日下午2:05:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.MemberWinningAccord;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.activity.repository.PrizeRepository;
import com.bw.adv.service.activity.service.PrizeService;

/**
 * ClassName:PrizeServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:05:36 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class PrizeServiceImpl extends BaseServiceImpl<Prize> implements PrizeService {
	
	private PrizeRepository prizeRepository;
	
	@Override
	public BaseRepository<Prize, ? extends BaseMapper<Prize>> getBaseRepository() {
		return prizeRepository;
	}
	

	@Override
	public Prize queryPrizeByAwardId(Long awardsId) {
		return prizeRepository.findPrizeByAwardId(awardsId);
	}
	
	@Autowired
	public void setPrizeRepository(PrizeRepository prizeRepository) {
		this.prizeRepository = prizeRepository;
	}

}

