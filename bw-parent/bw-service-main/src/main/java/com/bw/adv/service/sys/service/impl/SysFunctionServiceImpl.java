/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SysRoleServiceImpl.java
 * Package Name:com.sage.scrm.service.sys.service.impl
 * Date:2015-8-14下午4:48:43
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.sys.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.exp.SysFunctionExp;
import com.bw.adv.module.sys.repository.SysFunctionRepository;
import com.bw.adv.service.sys.service.SysFunctionService;

/**
 * ClassName:SysRoleServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-14 下午4:48:43 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
@Service
public class SysFunctionServiceImpl extends BaseServiceImpl<SysFunction> implements SysFunctionService {

	private SysFunctionRepository sysFunctionRepository;

	@Override
	public List<SysFunction> findListForSysFunction(Long systemId) {
		return this.sysFunctionRepository.findSysFunctionList(systemId);
	}

	@Override
	public BaseRepository<SysFunction, ? extends BaseMapper<SysFunction>> getBaseRepository() {
		return sysFunctionRepository;
	}
	@Autowired
	public void setSysFunctionRepository(SysFunctionRepository sysFunctionRepository) {
		this.sysFunctionRepository = sysFunctionRepository;
	}

	@Override
	public List<SysFunction> findListForSysFunctionByPid(Long parentid) {
		return this.sysFunctionRepository.findSysFunctionListByParentid(parentid);
	}

	@Override
	public List<SysFunction> findListForSysFunctionByUserId(Long userId, Long systemId) {
		return this.sysFunctionRepository.findSysFunctionListByUserId(userId, systemId);
	}

	@Override
	public Map<String, Object> findSysFunctionByUser(HttpServletRequest request,
													 HttpServletResponse response,List<SysFunction> functionList) {
		String menu="";
		Map<String,Object> map=new HashMap<String,Object>();
		List<SysFunction> parentList = new ArrayList<SysFunction>();
		List<SysFunction> list = new ArrayList<SysFunction>();
		try {
			response.setCharacterEncoding("utf-8");
			if(functionList!=null){
				//功能权限
				for (SysFunction function : functionList) {
					if(function.getPareFunction()!=null){
						list.add(function);
					}else{
						parentList.add(function);
					}
				}
				parentList =sortParentListBySeq(parentList);

				for(SysFunction pareSysFunction:parentList){
					menu+="<li class=\""+pareSysFunction.getFunctionIcon()+"\"><span>"+pareSysFunction.getFunctionName()+"</span></li>";
					menu+="<ol style=\"display: none;\">";
					for(SysFunction sysFunction:list){
						if(pareSysFunction.getFunctionId()==sysFunction.getPareFunction()){
							menu+="<li data-url=\""+sysFunction.getUrl()+"\">"+sysFunction.getFunctionName()+"</li>";
						}
					}
					menu+="</ol>";
				}

				map.put("menu", menu);
			}else{
				System.out.println("未获取用户信息！");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * sortParentListBySeq:(父级菜单排序). <br/>
	 * Date: 2015年9月23日 下午5:33:02 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param parentList
	 * @return
	 */
	private List<SysFunction> sortParentListBySeq(List<SysFunction> parentList) {
		Map<Integer,Object> map =null;
		List<Integer> list =null;
		List<SysFunction> parentSortList = null;
		parentSortList =new ArrayList<SysFunction>();
		list =new ArrayList<Integer>();
		map =new HashMap<Integer,Object>();
		for(SysFunction sysFunction:parentList){
			map.put(sysFunction.getSeq(), sysFunction);
			list.add(sysFunction.getSeq());
		}
		if(list.size() > 0){
			Collections.sort(list);
			for(Integer i:list){
				parentSortList.add((SysFunction)map.get(i));
			}
		}
		return parentSortList;
	}

	@Override
	public List<SysFunctionExp> selectSysFunctionListAndOperate(Long systemId) {
		return this.sysFunctionRepository.selectSysFunctionListAndOperate(systemId);
	}

	@Override
	public List<SysFunctionExp> selectSecondFunction(Long systemId) {
		return this.sysFunctionRepository.selectSecondFunction(systemId);
	}

}

