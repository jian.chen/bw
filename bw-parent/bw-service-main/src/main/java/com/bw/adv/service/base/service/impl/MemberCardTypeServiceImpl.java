/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.base.repository.MemberCardTypeRepository;
import com.bw.adv.module.member.model.MemberCardType;
import com.bw.adv.module.sys.enums.MemberCardTypeNoteEnum;
import com.bw.adv.service.base.service.MemberCardTypeService;


/**
 * ClassName: ProductServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:15:40 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Service
public class MemberCardTypeServiceImpl extends BaseServiceImpl<MemberCardType> implements MemberCardTypeService{

	private MemberCardTypeRepository memberCardTypeRepository;

	@Override
	public BaseRepository<MemberCardType, ? extends BaseMapper<MemberCardType>> getBaseRepository() {
		
		return memberCardTypeRepository;
	}
	

	@Autowired
	public void setMemberCardTypeRepository(
			MemberCardTypeRepository memberCardTypeRepository) {
		this.memberCardTypeRepository = memberCardTypeRepository;
	}

	@Override
	public List<MemberCardType> queryListForMemberCardType() {
		
		return this.memberCardTypeRepository.findMemberCardTypeList();
	}

	@Override
	public void updateMemberCardType(Long memberCardTypeId,MemberCardTypeNoteEnum memberCardTypeNoteEnum) {
		MemberCardType memberCardType=null;
		String cardStyle=null;
		String imgUrl=null;
		String memberCardDesc=null;
		
		memberCardType.setMemberCardTypeId(memberCardTypeId);
		memberCardType.setMemberCardTypeCode(memberCardTypeNoteEnum.getId());
		memberCardType.setMemberCardTypeName(memberCardTypeNoteEnum.getDesc(memberCardTypeNoteEnum.getId()));
//		memberCardType.setCardStyle(cardStyle);
//		memberCardType.setImgUrl(imgUrl);
//		memberCardType.setMemberCardDesc(memberCardDesc);
		memberCardTypeRepository.updateByPkSelective(memberCardType);
		
	}
	




}

