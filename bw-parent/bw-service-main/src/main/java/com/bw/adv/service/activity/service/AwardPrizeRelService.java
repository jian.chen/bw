/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:AwardPrizeRelService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2015年11月23日下午1:54:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.AwardPrizeRel;

/**
 * ClassName:AwardPrizeRelService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午1:54:15 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface AwardPrizeRelService extends BaseService<AwardPrizeRel> {
	
	
	
}

