/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:sysFunction.java
 * Package Name:com.sage.scrm.service.sys.service
 * Date:2015-8-17下午4:20:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.sys.service;


import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.sys.model.SysRoleFuncKey;

/**
 * ClassName:sysFunction <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-17 下午4:20:39 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
@Service
public interface SysRoleFuncService extends BaseService<SysRoleFuncKey> {

	void insertSysRoleFuncs(List<SysRoleFuncKey> list);

	void updateSysRoleFuncs(List<SysRoleFuncKey> list);

	/**
	 * queryCheckedSysRoleFuncList:通过角色id查询该角色选中的功能操作list. <br/>
	 * Date: 2015-9-2 上午11:31:11 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param roleId
	 * @return
	 */
	public List<Map<String,Object>> queryCheckedSysRoleFuncList(Long roleId);

	/**
	 * 查询角色选中的权限
	 * @param roleId
	 * @return
	 */
	public List<SysRoleFuncKey> queryCheckedSysRoleFuncLists(Long roleId);

	/**
	 * 根据用户Id，查询该用户角色所包含的功能并集
	 * 由于sql使用group by
	 * 所以返回对象中的roleId不建议使用
	 * @param userId
	 * @return
	 */
	public List<SysRoleFuncKey> queryByUserId(Long userId);


}

