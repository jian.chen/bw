/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberWinningAccordServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015年11月23日上午11:59:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.MemberWinningAccord;
import com.bw.adv.module.activity.repository.MemberWinningAccordRepository;
import com.bw.adv.service.activity.service.MemberWinningAccordService;

/**
 * ClassName:MemberWinningAccordServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 上午11:59:21 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class MemberWinningAccordServiceImpl extends BaseServiceImpl<MemberWinningAccord> implements MemberWinningAccordService {
	
	private MemberWinningAccordRepository memberWinningAccordRepository;
	
	@Override
	public BaseRepository<MemberWinningAccord, ? extends BaseMapper<MemberWinningAccord>> getBaseRepository() {
		return memberWinningAccordRepository;
	}
	
	@Autowired
	public void setMemberWinningAccordRepository(
			MemberWinningAccordRepository memberWinningAccordRepository) {
		this.memberWinningAccordRepository = memberWinningAccordRepository;
	}
	
	@Override
	public int queryMemberSurplusNumber(Long memberId, Long activityId,Long qrCodeId) {
		return memberWinningAccordRepository.findMemberSurplusNumber(memberId,activityId,qrCodeId);
	}

	@Override
	public void updateOneByMember(Long memberId, Long activityId,Long qrCodeId) {
		
		memberWinningAccordRepository.updateOneByMember(memberId, activityId,qrCodeId);
	}

	@Override
	public MemberWinningAccord queryResultByMemberIdAndActivityId(Long memberId, Long activityId,Long qrCodeId) {
		return this.memberWinningAccordRepository.queryResultByMemberIdAndActivityId(memberId, activityId,qrCodeId);
	}

	@Override
	public int updateMemberAccordWithVersion(Long memberWinningAccordId,Long qrCodeId, Long version, String useTime) {
		return this.memberWinningAccordRepository.updateMemberAccordWithVersion(memberWinningAccordId,qrCodeId, version, useTime);
	}
	
	@Override
	public int updateMemberAccordMapsVersion(Long memberWinningAccordId,String qrCode, String useTime) {
		return this.memberWinningAccordRepository.updateMemberAccordMapsVersion(memberWinningAccordId,qrCode, useTime);
	}

	@Override
	public Long queryMemberSurplusNumberToday(Long memberId, Long activityId,Long qrCodeId, String day) {
		return this.memberWinningAccordRepository.findMemberSurplusNumberToday(memberId, activityId,qrCodeId, day);
	}

	@Override
	public Long queryMemberUseChance(Long memberId, Long activityId,Long qrCodeId, String someDay) {
		return this.memberWinningAccordRepository.queryMemberUseChance(memberId, activityId,qrCodeId, someDay);
	}

	@Override
	public MemberWinningAccord queryResultToday(Long memberId, Long activityId,Long qrCodeId,String day) {
		return this.memberWinningAccordRepository.queryResultToday(memberId, activityId,qrCodeId,day);
	}

	@Override
	public MemberWinningAccord queryMemberMapsQrCode(Long memberId,Long activityId,String qrCode,String isUse) {
		return memberWinningAccordRepository.queryMemberMapsQrCode(memberId,activityId,qrCode,isUse);
	}
	
}

