/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-parse
 * File Name:WechatSendMesageHJobBean.java
 * Package Name:com.sage.scrm.bk.job.service.bean
 * Date:2015年12月15日下午9:07:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueComExp;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueHExp;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.wechat.msg.util.WechatApiUtil;
import com.bw.adv.service.wechat.service.WechatMsgQueueHService;

/**
 * ClassName:WechatSendMesageHJobBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月15日 下午9:07:41 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	
 */
public class WechatSendMesageHJobBean extends BaseJob{
	@Value("${limit}")
	private static String limit;
	private WechatMsgQueueHService wechatMsgQueueHService;
	private AccessTokenCacheService accessTokenCacheService;
	@Override
	protected void excute() throws Exception {
		List<WechatMsgQueueHExp> list = null;
		List<WechatMsgQueueHExp> failList = null;
		String accessToken = null;
		String currentTime = null;
		try {
			initService();
			if(StringUtils.isEmpty(limit)){
				limit = "100";
			}
			list = wechatMsgQueueHService.findAllWechatMsgQueueHs(Long.valueOf(limit));
			if(null != list && list.size() > 0){
				accessToken = accessTokenCacheService.getAccessTokenCache();
				failList = new ArrayList<WechatMsgQueueHExp>();
				currentTime = DateUtils.getCurrentTimeOfDb();
				for (WechatMsgQueueHExp instance : list) {
					JSONObject result = null;
					instance.setSendTime(currentTime);
					if(instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GET_COUPON_EXCHANGE.getId()) || instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GET_COUPON_RECEIVE.getId()) ||
							instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GET_COUPON_ACTIVITY.getId()) || instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GET_COUPON_HANDLE.getId())){
						result = sendMsgForCouponReceive(instance,accessToken);
					}else if(instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.ORDER_SALE.getId())){
						result = sendMsgForConsumption(instance,accessToken);
					}else if (instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.CLOSE_COMPLAIN.getId())) {
						result = sendMsgForCloseComplain(instance,accessToken);
					}
					/*else if(instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.COUPON_EXPIRE.getId())){
						result = sendMsgForCouponExpire(instance);
					}*/
					//会员等级变更
					else if(instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.GRADE_UPDATE.getId())){
						result = sendMsgForMemberModifyGrade(instance, accessToken);
					}//会员积分变更
					else if(instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.POINTS_UPDATE.getId())){
						result = sendMsgForMemberModifyPoints(instance, accessToken);
					}
					if(null != result){
						String errcode = result.getString("errcode");
						//如果发送失败，更新这条记录
						if(!errcode.equals(StatusConstant.WECHAT_ERRCODE_SUC.getId().toString())){
//							logger.error("微信发送消息失败："+result);
							instance.setResultMsg(result+"");
							failList.add(instance);
						}
					}
				}
				if(failList.size() > 0){
					for(WechatMsgQueueHExp failInstance : failList){
						list.remove(failInstance);
						failInstance.setIsSend("E");
					}
					wechatMsgQueueHService.updateBatchWechatMsgQueueH(failList);
				}
				if(list.size() > 0 ){
					wechatMsgQueueHService.deleteWechatMsgQueueH(list);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("微信发送消息任务执行完成");
	}

	/**
	 * sendMsgForCloseComplain:(关闭建议发送消息). <br/>
	 * Date: 2016年7月8日 上午11:05:45 <br/>
	 * scrmVersion standard
	 * @author lutianshui
	 * @version jdk1.7
	 * @param instance
	 * @param accessToken
	 * @return
	 */
	private JSONObject sendMsgForCloseComplain(WechatMsgQueueHExp instance, String accessToken) {
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForCloseComplain(queueComExp, accessToken);
	}

	/**
	 * sendMsgForCouponReceive:(优惠券领取发送消息). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param instance
	 */
	private JSONObject sendMsgForCouponReceive(WechatMsgQueueHExp instance,String accessToken){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForCouponReceive(queueComExp, accessToken);
	}
	
	/**
	 * sendMsgForConsumption:(订单消费发送消息). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param instance
	 */
	private JSONObject sendMsgForConsumption(WechatMsgQueueHExp instance,String accessToken){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForConsumption(queueComExp, accessToken);
	}
	
	
	
	/**
	 * sendMsgFormemberModifyGrade:(会员级别变更发送消息). <br/>
	 * Date: 2016年7月11日 下午4:57:55 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param instance
	 * @param accessToken
	 * @return
	 */
	private JSONObject sendMsgForMemberModifyGrade(WechatMsgQueueHExp instance,String accessToken){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForMemberModifyGrade(queueComExp, accessToken);
	}
	
	
	/**
	 * sendMsgFormemberModifyGrade:(会员积分变更发送消息). <br/>
	 * Date: 2016年7月11日 下午4:57:55 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param instance
	 * @param accessToken
	 * @return
	 */
	private JSONObject sendMsgForMemberModifyPoints(WechatMsgQueueHExp instance,String accessToken){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForMemberModifyPoints(queueComExp, accessToken);
	}
	
	
	
	
	
	/**
	 * 
	 * sendMsgForCouponExpire:(优惠券过期发送消息). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param instance
	 * @return
	 *//*
	private JSONObject sendMsgForCouponExpire(WechatMsgQueueHExp instance){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForCouponExpire(queueComExp, accessTokenCacheService.getAccessTokenCache());
	}*/

	private void initService() {
		this.wechatMsgQueueHService = this.getApplicationContext().getBean(WechatMsgQueueHService.class);
		this.accessTokenCacheService = this.getApplicationContext().getBean(AccessTokenCacheService.class);
	}
	
	@Override
	protected String getLockCode() {
		return LockCodeConstant.WECHAT_SEND_MESAGE_H_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

}

