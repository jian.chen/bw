/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageTemplateServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月18日下午4:46:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.wechat.model.WechatTemplateMessageItem;
import com.bw.adv.module.wechat.repository.WechatTemplateMessageItemRepository;
import com.bw.adv.service.wechat.service.WechatTemplateMessageItemService;

@Service
public class WechatTemplateMessageItemServiceImpl extends BaseServiceImpl<WechatTemplateMessageItem> implements WechatTemplateMessageItemService {

	private WechatTemplateMessageItemRepository wechatTemplateMessageItemRepository;

	@Override
	public BaseRepository<WechatTemplateMessageItem, ? extends BaseMapper<WechatTemplateMessageItem>> getBaseRepository() {
		return wechatTemplateMessageItemRepository;
	}
	
	@Autowired
	public void setWechatTemplateMessageItemRepository(
			WechatTemplateMessageItemRepository wechatTemplateMessageItemRepository) {
		this.wechatTemplateMessageItemRepository = wechatTemplateMessageItemRepository;
	}

	@Override
	public WechatTemplateMessageItem queryWechatTemplateMessageItemByTemplateMessId(
			Long templateMessId) {
		return this.wechatTemplateMessageItemRepository.findWechatTemplateMessageItemByTemplateMessId(templateMessId);
	}
	

}

