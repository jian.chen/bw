/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SceneGameService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2016年1月15日下午1:42:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.SceneGame;

/**
 * ClassName:SceneGameService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午1:42:35 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
public interface SceneGameService extends BaseService<SceneGame> {

	/**
	 * 
	 * queryAllSceneGame:查询所有的场景游戏 <br/>
	 * Date: 2016年1月15日 下午1:46:53 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @return
	 */
	List<SceneGame> queryAllSceneGame();
}

