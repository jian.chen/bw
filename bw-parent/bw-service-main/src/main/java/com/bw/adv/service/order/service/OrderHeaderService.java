package com.bw.adv.service.order.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.OrderItem;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.order.model.exp.OrderHeaderExp;


public interface OrderHeaderService extends BaseService<OrderHeader> {

	public List<OrderHeaderExp> queryExpByExample(Example example,Page<OrderHeaderExp> page);
	
	/**
	 * queryOrderExpByExample:根据条件查询订单详情<br/>
	 * Date: 2015年11月5日 下午5:10:36 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<OrderHeaderExp> queryOrderExpByExample(Example example,Page<OrderHeaderExp> page);
	
	/**
	 * queryOrderExpListByExample:根据条件查询订单 <br/>
	 * Date: 2015年12月31日 下午2:40:36 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<OrderHeaderExp> queryOrderExpListByExample(Example example,Page<OrderHeaderExp> page);
	
	/**
	 * queryOrderExpListByExa:根据条件查询订单 <br/>
	 * Date: 2016年1月5日 下午7:25:31 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<OrderHeaderExp> queryOrderExpListByExa(Example example,Page<OrderHeaderExp> page);
	/**
	 * queryOrderExpByMemberCode:根据会员编号查询订单 <br/>
	 * Date: 2015年12月31日 下午2:41:21 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberCode
	 * @param page
	 * @return
	 */
	public List<OrderHeaderExp> queryOrderExpByMemberCode(String memberCode,Page<OrderHeaderExp> page);
	
	/**
	 * queryOrderPoints:根据ID查询订单（积分） <br/>
	 * Date: 2015年12月31日 下午2:41:42 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param orderId
	 * @return
	 */
	public OrderHeaderExp queryOrderPoints(Long orderId);
	
	/**
	 * queryOrderCoupon:根据ID查询订单（优惠券） <br/>
	 * Date: 2015年12月31日 下午2:42:27 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param orderId
	 * @return
	 */
	public OrderHeaderExp queryOrderCoupon(Long orderId);
	/**
	 * handleSyncOrder:(同步订单). <br/>
	 * Date: 2015-9-15 下午3:15:03 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param orderHeaderDto
	 */
	public String handleSyncOrder(OrderInfo orderInfo,String optType) throws Exception;
	
	/**
	 * 
	 * queryOrderItemByOrderId:按订单id查询订单明细. <br/>
	 * Date: 2016年5月27日 下午3:59:50 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param orderId
	 * @param page
	 * @return
	 */
	public List<OrderItem> queryOrderItemByOrderId(Long orderId,Page<OrderItem> page);
	
	/**
	 * 
	 * queryProductListForJS:查询产品id和产品名称. <br/>
	 * Date: 2016年5月29日 下午3:42:15 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @return
	 */
	public List<Product> queryProductListForJS();

	/**
	 * air助手
	 * showShopOrderCount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param storeCode
	 * @param date
	 * @return
	 */
	List<OrderHeaderExp> showShopOrderCount(String storeCode, String date);
}
