/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageGroupItemServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月18日下午4:38:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.wechat.model.WechatMessageGroupItem;
import com.bw.adv.module.wechat.repository.WechatMessageGroupItemRepository;
import com.bw.adv.service.wechat.service.WechatMessageGroupItemService;

/**
 * ClassName:WechatMessageGroupItemServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:38:36 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatMessageGroupItemServiceImpl extends BaseServiceImpl<WechatMessageGroupItem> implements WechatMessageGroupItemService {
	
	private WechatMessageGroupItemRepository wechatMessageGroupItemRepository;
	
	@Override
	public BaseRepository<WechatMessageGroupItem, ? extends BaseMapper<WechatMessageGroupItem>> getBaseRepository() {
		return wechatMessageGroupItemRepository;
	}
	
	@Autowired
	public void setWechatMessageGroupItemRepository(
			WechatMessageGroupItemRepository wechatMessageGroupItemRepository) {
		this.wechatMessageGroupItemRepository = wechatMessageGroupItemRepository;
	}
	
}

