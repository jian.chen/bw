/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceRelayServiceImpl.java
 * Package Name:com.sage.scrm.bk.coupon.service.impl
 * Date:2015年11月19日下午5:48:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.coupon.model.CouponInstanceRelay;
import com.bw.adv.module.coupon.repository.CouponInstanceRelayRepository;
import com.bw.adv.service.coupon.service.CouponInstanceRelayService;

/**
 * ClassName:CouponInstanceRelayServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:48:54 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class CouponInstanceRelayServiceImpl extends BaseServiceImpl<CouponInstanceRelay> implements CouponInstanceRelayService{
	private CouponInstanceRelayRepository couponInstanceRelayRepository;
	@Override
	public BaseRepository<CouponInstanceRelay, ? extends BaseMapper<CouponInstanceRelay>> getBaseRepository() {
		return couponInstanceRelayRepository;
	}
	@Autowired
	public void setCouponInstanceRelayRepository(CouponInstanceRelayRepository couponInstanceRelayRepository) {
		this.couponInstanceRelayRepository = couponInstanceRelayRepository;
	}
	@Override
	public int querySeq(String openId, Long couponInstanceId) {
		int seq = 0;
		seq = couponInstanceRelayRepository.querySeq(openId, couponInstanceId);
		return seq;
	}
	@Override
	public int queryNumber(String openId, Long couponInstanceId,int seq) {
		int number = 0;
		number = couponInstanceRelayRepository.queryNumber(openId, couponInstanceId,seq);
		return number;
	}
	
}

