/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PointsRuleSeviceImpl.java
 * Package Name:com.sage.scrm.service.points.service.impl
 * Date:2015年8月20日下午5:09:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.points.model.PointsRule;
import com.bw.adv.module.points.repository.PointsRuleRepository;
import com.bw.adv.service.points.service.PointsRuleSevice;

/**
 * ClassName:PointsRuleSeviceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午5:09:11 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class PointsRuleSeviceImpl extends BaseServiceImpl<PointsRule> implements PointsRuleSevice {

	private PointsRuleRepository pointsRuleRepository;
	
	@Override
	public BaseRepository<PointsRule, ? extends BaseMapper<PointsRule>> getBaseRepository() {
		
		return pointsRuleRepository;
	}
	
	@Override
	public List<PointsRule> queryPointsRuleList(String isActive,Page<PointsRule> page) {
		
		return pointsRuleRepository.findByIsActive(isActive,page);
	}

	
	@Override
	public int deleteByPk(Long pointsRuleId) {
		PointsRule pointRule = new PointsRule();
		
		pointRule.setIsActive("N");
		pointRule.setPointsRuleId(pointsRuleId);
		
		return pointsRuleRepository.updateByPkSelective(pointRule);
	}
	
	@Override
	public List<PointsRule> queryByPointsTypeId(Long pointsTypeId) {
		return pointsRuleRepository.findByPointsTypeId(pointsTypeId);
	}
	
	@Autowired
	public void setPointsRuleRepository(PointsRuleRepository pointsRuleRepository) {
		this.pointsRuleRepository = pointsRuleRepository;
	}

	@Override
	public void deletePointsRuleByPointsRuleId(Long pointsRuleId) {
		pointsRuleRepository.removePointsRuleByPointsRuleId(pointsRuleId);
	}

	@Override
	public List<PointsRule> queryByPointsRuleName(String isActive,
			String pointsRuleName) {
		return this.pointsRuleRepository.findByPointsRuleName(isActive, pointsRuleName);
	}	



}

