/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WeChatService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月10日上午11:43:44
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.exp.WechatFansExp;

/**
 * ClassName:WeChatService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月10日 上午11:43:44 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WeChatFansService extends BaseService<WechatFans>{
	
	/**
	 * 
	 * queryWeChatFansList:(根据参数查询粉丝列表 ). <br/>
	 * Date: 2015年8月29日 下午2:11:15 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	List<WechatFans> queryWeChatFansList(Example example,Page<WechatFans> page);
	
	/**
	 * queryWechatFansExpList:(根据参数查询粉丝分组信息). <br/>
	 * Date: 2015年12月22日 下午2:57:58 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<WechatFansExp> queryWechatFansExpList(Example example,Page<WechatFansExp> page);
	
	/**
	 * 主键查询
	 * Date: 2015年8月10日 下午6:13:05 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param id
	 * @return
	 */
	public WechatFans queryWechatFansById(Long id);
	
	/**
	 * 查询粉丝详情和列表
	 * Date: 2015年8月10日 下午6:13:27 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param opendId
	 * @param nickName
	 * @param sex
	 * @param attentionTime
	 * @param cancelAttentionTime
	 * @param page
	 * @return
	 */
	public List<WechatFans> queryWechatFansByParams(String opendId,String nickName,String sex,
			String attentionTime,String cancelAttentionTime,String orgName,Page<WechatFans> page);
	
	/**
	 * save:(关注的粉丝同步到本地库). <br/>
	 * Date: 2015年8月19日 下午3:34:38 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param resultJson
	 */
	public JSONObject save(JSONObject resultJson);
	
	/**
	 * transTimeToDate:(格式化时间). <br/>
	 * Date: 2015年8月30日 下午3:22:59 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFans
	 * @return
	 * @throws ParseException 
	 */
	List<WechatFans> transData(List<WechatFans> wechatFans) throws ParseException;

	/**
	 * text转化为id 
	 * Date: 2015年8月30日 下午5:06:41 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param sex
	 * @param toAttentionTime 
	 * @param fromAttentionTime 
	 * @return
	 */
	Map<String, String> transferParam(String sex, String fromAttentionTime, String toAttentionTime);
	
	/**
	 * update:(更新已有粉丝信息). <br/>
	 * Date: 2015年12月21日 上午11:50:39 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFans
	 * @param resultJson
	 */
	void update(WechatFans wechatFans, JSONObject resultJson);
	
	/**
	 * modifyWechatFansExp:(修改粉丝展示分组信息). <br/>
	 * Date: 2015年12月22日 下午3:26:25 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFans
	 * @return
	 */
	List<WechatFansExp> modifyWechatFansExp(List<WechatFansExp> wechatFans);
	
	/**
	 * 
	 * updateFansAndMem:(更新粉丝信息及会员头像). <br/>
	 * Date: 2016年3月2日 下午6:35:09 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param wechatFans
	 */
	void updateFansAndMem(WechatFans wechatFans);
	
	
	/**
	 * saveOrUpdate:(新增或更新粉丝记录). <br/>
	 * Date: 2016-4-7 下午4:18:20 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param wechatFans
	 * @return
	 */
	public WechatFans saveOrUpdate(WechatFans wechatFans);
}

