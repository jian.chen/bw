package com.bw.adv.service.job.service.bean;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueue;
import com.bw.adv.service.coupon.service.CouponInstanceService;
import com.bw.adv.service.wechat.service.WechatMsgQueueService;

public class ExpiredCouponsJobBean extends BaseJob{
	private CouponInstanceService couponInstanceService;
	private WechatMsgQueueService wechatMsgQueueService;
	


	@Override
	protected void excute() throws Exception {
		List<CouponInstanceExp> couponList = null;
		Example example = null;
		Criteria criteria = null;
		String expiredDate = null;
		try {
			initService();
			example = new Example();
			criteria = example.createCriteria();
			criteria.andEqualTo("couin.EXPIRE_DATE", expiredDate);
			criteria.andIsNotNull("couin.COUPON_INSTANCE_USER");
			criteria.andLessThan("couin.STATUS_ID", StatusConstant.COUPON_INSTANCE_USED.getId());
			couponList = couponInstanceService.queryListByDyc(example);
			sendWechatMsg(couponList);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	private void sendWechatMsg(List<CouponInstanceExp> expList){
		JSONObject json = null;
		WechatMsgQueue msgQueue = null;
		List<WechatMsgQueue> msgQueueList = null;
		List<WechatMsgQueue> msgQueueSendList = null;
		Integer times = null;
		
		msgQueueList = new ArrayList<WechatMsgQueue>();
		for (CouponInstanceExp couponInstanceExp : expList) {
			json = new JSONObject();
			msgQueue = new WechatMsgQueue();
			
			json.put("couponName", couponInstanceExp.getCouponName());
			json.put("startDate", couponInstanceExp.getStartDate());
			json.put("expireDate", DateUtils.dateStrToNewFormat(couponInstanceExp.getExpireDate(), "yyyyMMdd", "yyyy-MM-dd"));
			json.put("comments", couponInstanceExp.getComments());
			
			msgQueue.setBusinessType(WechatMsgBusinessTypeEnum.COUPON_EXPIRE.getId());
			msgQueue.setCreateTime(DateUtils.getCurrentTimeOfDb());
			msgQueue.setMemberId(couponInstanceExp.getCouponInstanceUser());
			msgQueue.setMsgContent(json.toString());
			msgQueue.setMsgKey(couponInstanceExp.getCouponInstanceCode());
			msgQueue.setIsSend("N");
			msgQueueList.add(msgQueue);
		}
		if(!msgQueueList.isEmpty()){
			int couponNum = msgQueueList.size();
			int onlyone = couponNum/times;//批次数量
			for(int i = 0;i<onlyone;i++){
				msgQueueSendList = getFixedList(msgQueueList, times,i*times);
				wechatMsgQueueService.saveWechatMsgQueue(msgQueueSendList);
			}
			if(couponNum%times>0){
				int num = couponNum%times;
				msgQueueSendList = getFixedList(msgQueueList, num,onlyone*times);
				wechatMsgQueueService.saveWechatMsgQueue(msgQueueSendList);
			}
		}
		
	}
	/**
	 * getFixedList:genj<br/>
	 * Date: 2016年2月24日 下午2:23:11 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param list
	 * @param num
	 * @param begin
	 * @return
	 */
	public List<WechatMsgQueue> getFixedList(List<WechatMsgQueue> list,int num ,int begin){
		List<WechatMsgQueue> newlist = null;
		newlist = new ArrayList<WechatMsgQueue>();
		for(int i=0;i<num;i++){
			newlist.add(list.get(begin+i));
		}
		return newlist;
	}
	
	private void initService() {
		this.couponInstanceService = this.getApplicationContext().getBean(CouponInstanceService.class);
		this.wechatMsgQueueService = this.getApplicationContext().getBean(WechatMsgQueueService.class);
	}
	@Override
	protected String getLockCode() {
		return LockCodeConstant.WECHAT_SEND_MESAGE_H_EXPIRE_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}
	
}
