/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageTemplateService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月18日下午4:46:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.model.WeixinMedia;

/**
 * ClassName:WechatMessageTemplateService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:46:09 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMessageTemplateService extends BaseService<WechatMessageTemplate> {
	
	/**
	 * 保存图文信息到本地数据
	 * saveImageTextInfo:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年9月9日 下午4:21:42 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param mediaList
	 * @param dataJson
	 * @param wechatReponseJson 
	 * @param filePathList 
	 * @param strBackUrl 
	 * @return 
	 */
	String saveImageTextInfo(List<WeixinMedia> mediaList, JSONArray dataJson, JSONObject wechatReponseJson, String strBackUrl, List<String> filePathList);
	
	/**
	 * 单条图文消息以及多图文消息的父级消息
	 * queryParentImageText:(查询图文信息). <br/>
	 * Date: 2015年9月10日 下午3:07:24 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	List<WechatMessageTemplate> queryParentImageText();
	
	/**
	 * querySubImageText:(查询图文消息详情). <br/>
	 * Date: 2015年9月10日 下午3:31:46 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	List<WechatMessageTemplate> querySubImageTextList(String wechatMessageTemplateId);
	
	/**
	 * 
	 * deleteWechatMessageTemplate:(删除 ). <br/>
	 * Date: 2015年9月14日 上午10:56:50 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatMessageTemplate
	 */
	void deleteWechatMessageTemplate(WechatMessageTemplate wechatMessageTemplate);
}

