/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PrizeService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2015年11月23日下午2:05:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.Prize;

/**
 * ClassName:PrizeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:05:06 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface PrizeService extends BaseService<Prize> {
	
	/**
	 * queryPrizeByAwardId:(根据awardsId查询Prize). <br/>
	 * Date: 2015年12月15日 下午5:44:08 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param awardsId
	 * @return
	 */
	Prize queryPrizeByAwardId(Long awardsId);
	
}

