/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageTemplateService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月18日下午4:46:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatTemplateMessage;

public interface WechatTemplateMessageService extends BaseService<WechatTemplateMessage> {
	
	/**
	 * queryWechatTemplateMessageList:查询模板消息列表. <br/>
	 * Date: 2015-9-2 下午3:51:19 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 */
	public  List<WechatTemplateMessage> queryWechatTemplateMessageList();
	
}

