/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:AwardsServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015年11月23日下午1:51:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.Awards;
import com.bw.adv.module.activity.model.exp.AwardsExp;
import com.bw.adv.module.activity.repository.AwardsRepository;
import com.bw.adv.service.activity.service.AwardsService;

/**
 * ClassName:AwardsServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午1:51:51 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class AwardsServiceImpl extends BaseServiceImpl<Awards> implements AwardsService {
	
	private AwardsRepository awardsRepository;
	
	
	@Override
	public BaseRepository<Awards, ? extends BaseMapper<Awards>> getBaseRepository() {
		return awardsRepository;
	}
	
	
	@Autowired
	public void setAwardsRepository(AwardsRepository awardsRepository) {
		this.awardsRepository = awardsRepository;
	}


	@Override
	public List<AwardsExp> queryAwardsBySceneGameInstanceId(Long sceneGameInstanceId) {
		return awardsRepository.findAwardsBySceneGameInstanceId(sceneGameInstanceId);
	}


	@Override
	public AwardsExp queryLuckyAwards(Long sceneGameInstanceId) {
		return awardsRepository.findLuckyAwards(sceneGameInstanceId);
	}


	@Override
	public AwardsExp queryAwardsByAwardsId(Long awardsId) {
		return awardsRepository.findAwardsByAwardsId(awardsId);
	}


	/**
	 * TODO 根据游戏编号查询奖项列表
	 * @see com.bw.adv.service.activity.service.AwardsService#queryByGameCode(java.lang.String)
	 */
	@Override
	public List<Awards> queryByGameCode(String gameCode) {
		return awardsRepository.findByGameCode(gameCode);
	}
}

