/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberPointsItemSeviceImpl.java
 * Package Name:com.sage.scrm.service.points.service.impl
 * Date:2015年8月20日下午6:17:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.badge.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.badge.model.MemberBadge;
import com.bw.adv.module.badge.model.MemberBadgeItem;
import com.bw.adv.module.badge.model.exp.MemberBadgeExt;
import com.bw.adv.module.badge.repository.MemberBadgeItemRepository;
import com.bw.adv.module.badge.repository.MemberBadgeRepository;
import com.bw.adv.module.common.constant.LotteryConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.badge.service.MemberBadgeItemSevice;

/**
 * ClassName:MemberPointsItemSeviceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:17:28 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class MemberBadgeItemSeviceImpl extends BaseServiceImpl<MemberBadgeItem> implements MemberBadgeItemSevice {
	

	private MemberBadgeItemRepository memberBadgeItemRepository;
	private MemberBadgeRepository memberBadgeRepository;
	
	

	@Override
	public BaseRepository<MemberBadgeItem, ? extends BaseMapper<MemberBadgeItem>> getBaseRepository() {
		return memberBadgeItemRepository;
	}
	
	@Autowired
	public void setMemberBadgeItemRepository(MemberBadgeItemRepository memberBadgeItemRepository) {
		this.memberBadgeItemRepository = memberBadgeItemRepository;
	}

	@Autowired
	public void setMemberBadgeRepository(MemberBadgeRepository memberBadgeRepository) {
		this.memberBadgeRepository = memberBadgeRepository;
	}

	@Override
	public int saveMemberBadgeByMemberId(Long memberId, Long activityId,Long awardsId) {
		MemberBadge memberBadge=memberBadgeRepository.findMemberBadgeByMemberId(memberId,awardsId);
		if(memberBadge==null){
			memberBadge=new MemberBadge();
			memberBadge.setBadageId(awardsId);
			memberBadge.setMemberId(memberId);
			memberBadge.setBadgeAvailableNum(1);
			memberBadge.setBadgeTotalNum(1);
			memberBadge.setCreateTime(DateUtils.getCurrentTimeOfDb());
			memberBadgeRepository.save(memberBadge);
			
		}else{
			//数量加一
			memberBadge.setBadgeAvailableNum((memberBadge.getBadgeAvailableNum()==null?0:memberBadge.getBadgeAvailableNum())+1);
			memberBadge.setBadgeTotalNum((memberBadge.getBadgeTotalNum()==null?0:memberBadge.getBadgeTotalNum())+1);
			memberBadge.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			memberBadgeRepository.updateByPkSelective(memberBadge);
		}
		MemberBadgeItem badgeItem=new MemberBadgeItem();
		badgeItem.setMemberBadageId(memberBadge.getMemberBadgeId());
		badgeItem.setAwardsId(awardsId);
		badgeItem.setAvailableBadgeNumber(1);
		badgeItem.setItemBadgeNumber(1);
		badgeItem.setCreateTime(DateUtils.getCurrentTimeOfDb());
		badgeItem.setSceneGameInstanceId(activityId);
		memberBadgeItemRepository.save(badgeItem);
		return 0;
	}

	@Override
	public List<MemberBadgeExt> queryMemberBadgeExtByMemberId(Long memberId) {
		return memberBadgeRepository.findMemberBadgeExtByMemberId(memberId);
	}

	@Override
	public boolean validMemberBadgeQualifications(Long memberId) {
		List<MemberBadgeExt> memberBadgeExts = this.queryMemberBadgeExtByMemberId(memberId);
		List<String> memberBadgeCode=new ArrayList<String>();
		for (MemberBadgeExt memberBadgeExt : memberBadgeExts) {
			if(memberBadgeExt.getBadgeAvailableNum()!=null&&memberBadgeExt.getBadgeAvailableNum()>0){
				memberBadgeCode.add(memberBadgeExt.getAwardsCode());
			}
		}
		//判断是否有兑换条件
		String memberBadgeArr[]=new String[]{LotteryConstant.LOTTERY_TYPE_WHEAT_HEAD.getCode(),
				LotteryConstant.LOTTERY_TYPE_YAKIMA.getCode(),LotteryConstant.LOTTERY_TYPE_BEECH.getCode(),LotteryConstant.LOTTERY_TYPE_WATER.getCode()};
		boolean isQualify=true;
		for (String arr1 : memberBadgeArr) {
			
			boolean isflag=false;
			for (String arr2 : memberBadgeCode) {
				if(StringUtils.equals(arr1, arr2)){
					isflag=true;
				}
			}
			if(!isflag){
				isQualify=false;
				break;
			}
		}
		
		return isQualify;
	}

	@Override
	public int updateMemberBadgeGiftExchange(Long memberId) {
		return memberBadgeRepository.updateMemberBadgeGiftExchange(memberId);
	}

}

