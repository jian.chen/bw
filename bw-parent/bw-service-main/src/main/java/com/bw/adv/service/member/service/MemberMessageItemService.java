/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberService1.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2015年8月10日下午3:39:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.member.message.model.MemberMessageItem;
import com.bw.adv.module.member.message.model.NotReadMessage;


/**
 * ClassName: MemberMessageItemService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年2月26日 上午10:56:37 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface MemberMessageItemService extends BaseService<MemberMessageItem>{
	
	/**
	 * insertBatchPer:批量插入数据 <br/>
	 * Date: 2016年2月26日 上午11:08:08 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param ids
	 * @param type
	 * @return
	 */
	void insertBatchPer(Long messageid,String ids,String type);
	
	/**
	 * insertObj:插入一条已读消息<br/>
	 * Date: 2016年3月2日 下午3:31:25 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memeberId
	 * @param memberMessageId
	 */
	void insertObj(Long memeberId,Long memberMessageId);
	
	/**
	 * deleteByMessageId:撤回会员消息 <br/>
	 * Date: 2016年2月26日 下午4:01:10 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param messageId
	 */
	void deleteByMessageId(Long messageId);
	
	/**
	 * queryByMessageId:根据消息Id查询消息详情 <br/>
	 * Date: 2016年2月29日 下午9:58:29 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 * @return
	 */
	MemberMessageItem queryByMessageId(Example example);
	
	/**
	 * queryNotReadMessageByMemberId:根据会员Id查询未读消息<br/>
	 * Date: 2016年3月2日 下午4:19:46 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	NotReadMessage queryNotReadMessageByMemberId(Long memberId);
}

