/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ProductCategoryService.java
 * Package Name:com.sage.scrm.service.base.service
 * Date:2016年1月14日上午11:48:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.ProductCategory;
import com.bw.adv.module.base.model.ProductCategoryItem;

/**
 * ClassName:ProductCategoryService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 上午11:48:09 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
public interface ProductCategoryService extends BaseService<ProductCategory> {
	
	/**
	 * 
	 * queryAll:查询所有的产品类别 <br/>
	 * Date: 2016年1月14日 下午12:02:22 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @return
	 */
	List<ProductCategory> queryAll();
	
	/**
	 * queryProductCategoryPage:分页查询产品类别<br/>
	 * Date: 2016年1月14日 下午12:20:34 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	List<ProductCategory> queryProductCategoryPage(Page<ProductCategory> page);
	
	/**
	 * findProductCategoryIsMultiple:(查询产品分类和名称是否重复). <br/>
	 * @author chengdi.cui
	 * @param productCategory
	 * @return
	 */
	public int findProductCategoryIsMultiple(ProductCategory productCategory);
	
	/**
	 * 删除产品类别与关系
	 * removeProductCategory:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param productCategoryId
	 */
	public void removeProductCategory(Long productCategoryId);
	
	public void saveProductCategoryItem(List<ProductCategoryItem> itemList);
	
	/**
	 * 根据产品id删除产品类别关系（用于更新产品）
	 * removeProductCategoryItemByProductId:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param productId
	 */
	public void removeProductCategoryItemByProductId(Long productId);
	
	/**
	 * 保存产品类别
	 * saveProductCategory:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param list
	 */
	public void saveProductCategory(List<ProductCategory> list);
	
}

