package com.bw.adv.service.wechat.msg.model;
/**
 * 模板数据类
 * ClassName: TemplateData <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年11月11日 下午3:51:51 <br/>
 * scrmVersion 1.0
 * @author yu.zhang
 * @version jdk1.7
 */
public class TemplateData {
	private String value;

	private String color;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
