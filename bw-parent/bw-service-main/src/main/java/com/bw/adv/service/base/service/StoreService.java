/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:StoreService.java
 * Package Name:com.sage.scrm.service.base.service
 * Date:2015年8月18日下午7:14:20
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.model.exp.StoreExp;

import java.util.List;

/**
 * ClassName:StoreService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午7:14:20 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface StoreService extends BaseService<Store> {
	
	/**
	 * 
	 * queryStoreByStatusId:查看有效门店(扩展类)
	 * Date: 2015年9月2日 下午3:18:06 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	public List<Store> queryStoreByStatusId(Page<Store> page);
	
	/**
	 * 
	 * deleteStoreByPk:删除门店（逻辑删除）
	 * Date: 2015年8月26日 上午11:16:43 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param storeId
	 * @return
	 */
	public int deleteStoreByPk(Long storeId);
	
	/**
	 * 
	 * queryOrgByPk:查看地域详细信息
	 * Date: 2015年8月26日 上午11:17:07 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param OrgId
	 * @return
	 */
	public Org queryOrgByPk(Long OrgId);
	
	/**
	 * 
	 * deleteStoreList:批量删除门店（逻辑删除）
	 * Date: 2015年8月26日 上午11:16:18 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param storeIds
	 * @return
	 */
	public int deleteStoreList(List<Long> storeIds);
	
	/**
	 * 
	 * queryById:根据ID查询门店详情
	 * Date: 2015年9月3日 下午3:54:44 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param storeId
	 * @return
	 */
	public StoreExp queryById(Long storeId);
	
	/**
	 * 
	 * queryByCode:(根据编号查门店). <br/>
	 * Date: 2015年9月29日 下午7:19:16 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param code
	 * @return
	 */
	public Store queryByCode(String code);

	/**
	 * renovateStore:(主数据更新). <br/>
	 * Date: 2015-12-29 下午5:38:35 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	public void renovateStore();

	/**
	 * queryByOrgId:(根据orgid查询门店). <br/>
	 * @author chengdi.cui
	 * @param orgId
	 * @return
	 */
	public Store queryByOrgId(Long orgId);

	/**
	 * 批量保存门店
	 * saveStoreList:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param list
	 */
	public void saveStoreList(List<Store> list);
	
	/**
	 * 查询门店列表
	 * @return
	 */
	public List<Store> queryStoreList();
	
}

