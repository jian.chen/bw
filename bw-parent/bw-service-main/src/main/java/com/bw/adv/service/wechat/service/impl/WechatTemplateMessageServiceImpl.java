/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageTemplateServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月18日下午4:46:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.wechat.model.WechatTemplateMessage;
import com.bw.adv.module.wechat.repository.WechatTemplateMessageRepository;
import com.bw.adv.service.wechat.service.WechatTemplateMessageService;

@Service
public class WechatTemplateMessageServiceImpl extends BaseServiceImpl<WechatTemplateMessage> implements WechatTemplateMessageService {

	private WechatTemplateMessageRepository wechatTemplateMessageRepository;

	@Override
	public BaseRepository<WechatTemplateMessage, ? extends BaseMapper<WechatTemplateMessage>> getBaseRepository() {
		return wechatTemplateMessageRepository;
	}
	@Autowired
	public void setWechatTemplateMessageRepository(
			WechatTemplateMessageRepository wechatTemplateMessageRepository) {
		this.wechatTemplateMessageRepository = wechatTemplateMessageRepository;
	}
	@Override
	public List<WechatTemplateMessage> queryWechatTemplateMessageList() {
		return this.wechatTemplateMessageRepository.findWechatTemplateMessageList();
	}
	

}

