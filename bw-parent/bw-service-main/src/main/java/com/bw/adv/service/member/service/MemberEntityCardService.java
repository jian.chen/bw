/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberEntityCardService.java
 * Date:
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/
package com.bw.adv.service.member.service;
import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberEntityCard;
import com.bw.adv.module.member.model.MemberEntityCardIssue;
import com.bw.adv.module.member.model.exp.MemberEntityCardExp;

/**
 * ClassName:MemberEntityCardService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-11-16 下午5:36:15 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberEntityCardService extends BaseService<MemberEntityCard>{

    
	/**
	 * 根据卡数量生成实体卡
	 * @param count 生成数量
	 * @param userName 操作人
	 * @param batch 批次号
	 * @return
	 */
    int saveList(int count,String userName,String batch);
    
    /**
     * 根据批次号查询实体卡的批次信息
     * @param batch 批次号
     * @return 实体卡批次集合
     */
    List<MemberEntityCardIssue> findMemberCardByBatch(String batch,Page<MemberEntityCardIssue> pageObj);
    
    
    /**
     * 根据批次表主键查询实体卡的详细信息
     * @param issueId id
     * @return 实体卡集合
     */
    List<MemberEntityCard> findMemberCardByIssueId(Long issueId,Page<MemberEntityCard> pageObj);
    /**
     * 导出Excel
     * @param issueId
     * @return
     */
    List<MemberEntityCardExp> getMemberCardByIssueIdToExcel(Long issueId);
    
    /**
     * 根据issueId更新状态为已制卡
     * @param t
     * @return
     */
    int makeMemberEntityCard(Long issueId);
    
}

