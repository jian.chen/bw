package com.bw.adv.service.sys.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.sys.model.SysRoleData;
import com.bw.adv.module.sys.model.SysRoleDataType;
import com.bw.adv.module.sys.model.exp.RoleDataConfigExp;
import com.bw.adv.module.sys.model.exp.RoleDataRequest;

import java.util.List;
import java.util.Map;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public interface SysRoleDataService extends BaseService<SysRoleData>{

    public List<SysRoleData> queryByRoleId(Long roleId);

    public List<SysRoleData> queryByUserId(Long userId);

    public List<SysRoleDataType> queryAllDataType();

    public Map<Integer, SysRoleDataType> getCacheDataTypeMap();

    public Map<Integer, SysRoleDataType> getNoCacheDataTypeMap();

    /**
     * 根据角色ID展现对应的数据权限配置
     * roleId可为空
     * @param roleId
     * @return
     */
    public List<RoleDataConfigExp> getRoleDataConfig(Long roleId);

    /**
     * 保存对应的角色数据权限配置
     * @param roleDataRequest
     */
    public void saveRoleDataConfig(RoleDataRequest roleDataRequest);

}
