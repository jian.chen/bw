/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:FansChannelSummaryJobBean.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2015年11月30日下午4:24:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.report.service.FansChannelSummaryDayService;
import com.bw.adv.service.report.service.MemberChannelSummaryDayService;

/**
 * ClassName:FansChannelSummaryJobBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月30日 下午4:24:24 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class FansChannelSummaryJobBean extends BaseJob {
	
	private static final Logger logger = LoggerFactory.getLogger(FansChannelSummaryJobBean.class);
	
	private FansChannelSummaryDayService fansChannelSummaryDayService;

	@Override
	protected void excute() throws Exception {
		
		init();
		logger.info("粉丝渠道报表任务开始");
		String dateStr = DateUtils.addDays(-1);//昨天日期
		fansChannelSummaryDayService.callFansChannelSummary(dateStr);
		logger.info(dateStr+"|粉丝渠道报表任务结束");
		
	}
	
	public void init(){
		this.fansChannelSummaryDayService = this.getApplicationContext().getBean(FansChannelSummaryDayService.class);
	}

	@Override
	protected String getLockCode() {
		
		return LockCodeConstant.FANS_CHANNEL_SUMMARY_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		
		return true;
	}

	@Autowired
	public void setFansChannelSummaryDayService(
			FansChannelSummaryDayService fansChannelSummaryDayService) {
		this.fansChannelSummaryDayService = fansChannelSummaryDayService;
	}
	
	

}

