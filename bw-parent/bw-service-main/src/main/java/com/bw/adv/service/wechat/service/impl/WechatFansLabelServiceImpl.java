/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansLabelServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月17日上午10:49:45
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.List;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatFansLabel;
import com.bw.adv.module.wechat.repository.WechatFansLabelRepository;
import com.bw.adv.service.wechat.service.WechatFansLabelService;

/**
 * ClassName:WechatFansLabelServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 上午10:49:45 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatFansLabelServiceImpl extends BaseServiceImpl<WechatFansLabel> implements WechatFansLabelService {
	
	private WechatFansLabelRepository wechatFansLabelRepository;
	
	@Override
	public BaseRepository<WechatFansLabel, ? extends BaseMapper<WechatFansLabel>> getBaseRepository() {
		return wechatFansLabelRepository;
	}
	
	@Override
	public void saveFansLabel(String labelName,SysUser sysUser) {
		WechatFansLabel wechatFansLabel =null;
		wechatFansLabel =new WechatFansLabel();
		if(null !=sysUser){
			wechatFansLabel.setCreateBy(sysUser.getUserId());
		}
		wechatFansLabel.setLabelCode("007");
		wechatFansLabel.setCreateDate(DateUtils.getCurrentTimeOfDb());
		wechatFansLabel.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
		wechatFansLabel.setStatusId(StatusConstant.WECHAT_ENABLE.getId());
		if(!StringUtils.isEmpty(labelName)){
			wechatFansLabel.setLabelName(labelName);
		}
		this.save(wechatFansLabel);
	}
	

	@Override
	public List<WechatFansLabel> queryWechatFansLabelByLabelName(String labelName) {
		return wechatFansLabelRepository.queryWechatFansLabelByLabelName(labelName);
	}
	
	@Autowired
	public void setWechatFansLabelRepository(
			WechatFansLabelRepository wechatFansLabelRepository) {
		this.wechatFansLabelRepository = wechatFansLabelRepository;
	}
	
	@Override
	public List<WechatFansLabel> queryWechatFansLabels() {
		return wechatFansLabelRepository.findWechatFansLabels();
	}


}

