package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.ShakeUserInfo;
import com.bw.adv.module.activity.model.UserAndWinning;
import com.bw.adv.module.activity.repository.ShakeUserInfoRepository;
import com.bw.adv.service.activity.service.ShakeUserInfoService;

@Service
public class ShakeUserInfoServiceImpl extends BaseServiceImpl<ShakeUserInfo> implements ShakeUserInfoService {

	private ShakeUserInfoRepository shakeUserInfoRepository;
	
	@Override
	public BaseRepository<ShakeUserInfo, ? extends BaseMapper<ShakeUserInfo>> getBaseRepository() {
		return shakeUserInfoRepository;
	}
	
	@Override
	public ShakeUserInfo queryShakeUserInfoByPhoneNum(String phoneNum) {
		return shakeUserInfoRepository.findShakeUserInfoByPhoneNum(phoneNum);
	}
	
	
	
	@Autowired
	public void setShakeUserInfoRepository(
			ShakeUserInfoRepository shakeUserInfoRepository) {
		this.shakeUserInfoRepository = shakeUserInfoRepository;
	}

	@Override
	public List<UserAndWinning> selectAllForUserAndWinning() {
		return this.shakeUserInfoRepository.selectAllForUserAndWinning();
	}
	
	
	

	
}
