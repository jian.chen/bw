/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:LotteryServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015年11月24日下午2:51:17
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.bw.adv.commons.cache.redis.JedisService;
import com.bw.adv.module.activity.enums.AccordTypeEnum;
import com.bw.adv.module.activity.model.AwardsSetting;
import com.bw.adv.module.activity.model.MemberWinningAccord;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.activity.model.PrizeInstance;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;
import com.bw.adv.module.activity.model.exp.PrizeInstanceExp;
import com.bw.adv.module.activity.repository.MemberWinningAccordRepository;
import com.bw.adv.module.base.exception.ActivityException;
import com.bw.adv.module.common.constant.LotteryConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.repository.CouponRepository;
import com.bw.adv.module.points.model.MemberPointsItem;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatCouponMsg;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.repository.WeChatFansRepository;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;
import com.bw.adv.service.activity.service.AwardsSettingService;
import com.bw.adv.service.activity.service.LotteryService;
import com.bw.adv.service.activity.service.MemberWinningAccordService;
import com.bw.adv.service.activity.service.PrizeInstanceService;
import com.bw.adv.service.activity.service.PrizeService;
import com.bw.adv.service.activity.service.WinningItemService;
import com.bw.adv.service.badge.service.MemberBadgeItemSevice;
import com.bw.adv.service.coupon.service.CouponInstanceService;
import com.bw.adv.service.coupon.service.CouponIssueService;

/**
 * ClassName:LotteryServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月24日 下午2:51:17 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class LotteryServiceImpl implements LotteryService{
	
	private static final Logger logger = LoggerFactory.getLogger(LotteryServiceImpl.class);
	private static final Map<String, Long> WINNING_ITEM_PERSONS = new HashMap<String, Long>();
	//@Value("${every_day_lotter_limit}")
	private int everyDayLotterLimit=1;
	@Autowired
	private JedisService jedisService;
	private static String CURRENT_DATE;
	
	private MemberWinningAccordService memberWinningAccordService;
	private AwardsSettingService awardsSettingService;
	private WinningItemService winningItemService;
	private PrizeInstanceService prizeInstanceService;
	private PrizeService prizeService;
	private CouponInstanceService couponInstanceService;
	@Autowired
	private CouponIssueService couponIssueService;
	private CouponRepository couponRepository;
	private WechatMsgQueueHRepository wechatMsgQueueHRepository;
	private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	private MemberWinningAccordRepository memberWinningAccordRepository;
	private WeChatFansRepository weChatFansRepository;
	private MemberBadgeItemSevice memberBadgeItemSevice;
	@Value("${ATTEND_AWARD_ID}")
	private String ATTEND_PRIZE_ID;
	@Value("${PRIZE_ENTITY_TYPE_ID}")
	private String PRIZE_ENTITY_TYPE_ID;
	@Value("${PRIZE_COUPON_TYPE_ID}")
	private String PRIZE_COUPON_TYPE_ID;
	@Value("${PRIZE_CARD_TYPE_ID}")
	private String PRIZE_CARD_TYPE_ID;
	@Value("${activity_end_time}")
	private String ACTIVITY_END_TIME ;
	@Value("${is_every_day_accord}")
	private String IS_EVERY_DAY_ACCORD;
	
	
	
	/**
	 * ************************
	 * 1.检查会员抽奖资格
	 * 2.如果符合资格，进行抽奖活动，不符合，那么抛出抽奖资格不符合.
	 * 	 如果符合资格
	 * 
	 */
	@Override
	public WinningItem extractMemberAwards(Long memberId, Long activityId,Long qrCodeId) {
		WinningItem winningItem = null;
		MemberWinningAccord memberWinningAccord = null;
		int accordSize = 0;
		String currentTime = null;
		List<MemberWinningAccord> accordList = null;
		
		if(ACTIVITY_END_TIME.compareTo(DateUtils.getCurrentTimeOfDb()) < 0){
			throw new ActivityException(ActivityException.ACTIVITY_END);
		}
		
		// 如果会员抽奖资格次数<=0
		accordSize = memberWinningAccordService.queryMemberSurplusNumber(memberId, activityId,qrCodeId);
		if(accordSize<=0){
			throw new ActivityException(ActivityException.NO_QUALIFICATION);
		}
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		
		if(IS_EVERY_DAY_ACCORD.equals("Y")){
			accordList = memberWinningAccordRepository.findAccordListCurrentDate(memberId, activityId,qrCodeId,"someday",AccordTypeEnum.NO_CONDITION.getId());
			//查询当天是否有抽奖机会，如果没有则新增一次抽奖机会
			if(accordList == null || accordList.size() < everyDayLotterLimit){
				memberWinningAccord = new MemberWinningAccord();
				memberWinningAccord.setMemberId(memberId);
				memberWinningAccord.setSceneId(activityId);
				memberWinningAccord.setIsUsed("N");
				memberWinningAccord.setCreateTime(currentTime);
				memberWinningAccord.setAccordType(1);
				memberWinningAccordService.save(memberWinningAccord);
			}
		}
		
		if(memberWinningAccord == null){
			memberWinningAccord = this.memberWinningAccordService.queryResultByMemberIdAndActivityId(memberId, activityId,qrCodeId);
		}
		
		if(memberWinningAccord == null){
			throw new ActivityException(ActivityException.NO_QUALIFICATION);
		}
		Long lastVersion = memberWinningAccord.getVersion();
		int rows = this.memberWinningAccordService.updateMemberAccordWithVersion(memberWinningAccord.getMemberWinningAccordId(),qrCodeId, lastVersion, currentTime);
		if(rows <= 0){
			throw new ActivityException(ActivityException.NO_QUALIFICATION);
		}
		
		Map<Long, AwardsSetting> currentMap = calculateMonthProbability(activityId,memberId);
		//System.out.println("currentMap:"+currentMap);
		// 获奖的awardsId
		Long awardsId = extractTagertPrize(currentMap);
		try {
			winningItem = extractLastAwards(activityId, memberId, awardsId);
		} catch (ActivityException e) {
			// 如果没有奖品实例
			if(ActivityException.NO_PRIZE_INSTANCE.equals(e.getErrorData())){
				// 那么只抽取参与奖
				winningItem = extractAttendAwards(activityId,memberId,ATTEND_PRIZE_ID);
			}
		}
		//修改此会员抽奖资格
		memberWinningAccordService.updateOneByMember(memberId, activityId,qrCodeId);
		
		return winningItem;
	}
	
	/**
	 * 执行卡牌抽奖
	 * @PE
	 */
	@Override
	public AwardsSettingExp extractMemberAwardsEveryMonth(Long memberId, Long activityId,Long qrCodeId) {
		AwardsSettingExp awardsSettingExp = null;
		MemberWinningAccord memberWinningAccord = null;
		Long accordSize = 0L;
		// 如果会员抽奖资格次数<=0
		String currentTime = DateUtils.getCurrentTimeOfDb();
		String currentDate=currentTime.substring(0, 6);//YYYYMM  每个月，每个二维码，只能抽一次
		accordSize = memberWinningAccordService.queryMemberSurplusNumberToday(memberId, activityId,qrCodeId, currentDate);
		if(accordSize <= 0L){
			throw new ActivityException(ActivityException.NO_QUALIFICATION);
		}
		memberWinningAccord = this.memberWinningAccordService.queryResultToday(memberId, activityId,qrCodeId,currentDate);
		if(memberWinningAccord == null){
			throw new ActivityException(ActivityException.NO_QUALIFICATION);
		}
		Long lastVersion = memberWinningAccord.getVersion();
		int rows = this.memberWinningAccordService.updateMemberAccordWithVersion(memberWinningAccord.getMemberWinningAccordId(),qrCodeId, lastVersion, currentTime);
		if(rows <= 0){
			throw new ActivityException(ActivityException.NO_QUALIFICATION);
		}
		// 获奖的awardsId
		try {
			//Map<Long, AwardsSetting> currentMap = calculateDayProbability(activityId,memberId);
			Long awardsId = extractTagertPrize(activityId,memberId);
			awardsSettingExp = extractLastAwardsEveryDay(activityId, memberId, awardsId);
		} catch (ActivityException e) {
			// 如果没有奖品实例,那么只抽取积分5                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
			throw e;
		}
		return awardsSettingExp;
	}
	
	/**
	 * 执行maps抽奖
	 * @PE
	 */
	@Override
	public AwardsSettingExp extractMemberAwardsMaps(Long memberId, Long activityId,String qrCode) {
		AwardsSettingExp awardsSettingExp = null;
		MemberWinningAccord memberWinningAccord = null;
		// 如果会员抽奖资格次数<=0
		String currentTime = DateUtils.getCurrentTimeOfDb();
		memberWinningAccord= memberWinningAccordService.queryMemberMapsQrCode(memberId, activityId, qrCode,"N");
		if(memberWinningAccord == null){
			throw new ActivityException(ActivityException.NO_QUALIFICATION);
		}
		int rows = this.memberWinningAccordService.updateMemberAccordMapsVersion(memberWinningAccord.getMemberWinningAccordId(),qrCode, currentTime);
		if(rows <= 0){
			throw new ActivityException(ActivityException.NO_QUALIFICATION);
		}
		// 获奖的awardsId
		try {
			Map<Long, AwardsSetting> currentMap = calculateMapsProbability(activityId);
			Long awardsId = extractTagertPrize(currentMap);
			awardsSettingExp = extractLastAwardsEveryDay(activityId, memberId, awardsId);
		} catch (ActivityException e) {
			throw e;
		}
		return awardsSettingExp;
	}
	
	@Override
	public Long queryMemberLotteryChance(Long memberId,Long activityId, Long qrCodeId) {
		List<MemberWinningAccord> accordList = null;
		MemberWinningAccord memberWinningAccord = null;
		String currentTime = DateUtils.getCurrentTimeOfDb();
		String currentDate=currentTime.substring(0, 6);//YYYYMM  每个月，每个二维码，只能抽一次
		Long accordSize = 0L;
		if(StringUtils.isNotBlank(IS_EVERY_DAY_ACCORD) && IS_EVERY_DAY_ACCORD.equals("Y")){
			accordList = memberWinningAccordRepository.findAccordListCurrentDate(memberId, activityId,qrCodeId,currentDate,AccordTypeEnum.NO_CONDITION.getId());
			if(accordList == null || accordList.size() < everyDayLotterLimit){
				memberWinningAccord = new MemberWinningAccord();
				memberWinningAccord.setMemberId(memberId);
				memberWinningAccord.setSceneId(activityId);
				memberWinningAccord.setIsUsed("N");
				memberWinningAccord.setQrCodeId(qrCodeId);
				memberWinningAccord.setCreateDate(currentDate);
				memberWinningAccord.setCreateTime(currentTime);
				memberWinningAccord.setAccordType(1);
				memberWinningAccord.setVersion(1L);
				memberWinningAccordService.save(memberWinningAccord);
			}
		}
		accordSize = memberWinningAccordService.queryMemberSurplusNumberToday(memberId, activityId,qrCodeId, currentDate);
		return accordSize;
	}
	
	/**
	 * TODO 新增抽奖机会（可选）.
	 * @see com.sage.scrm.api.activity.api.ActivityApi#addMemberWinningAccord(java.lang.String)
	 */
	@Override
	public void addMemberWinningAccord(Long memberId,Long activityId,String mapsQrCode) {
		MemberWinningAccord memberWinningAccord = null;
		//查询当天是否有过改类型的任务资格，如果没有则新增一次
		memberWinningAccord = new MemberWinningAccord();
		memberWinningAccord.setMemberId(memberId);
		memberWinningAccord.setSceneId(activityId);
		memberWinningAccord.setIsUsed("N");
		memberWinningAccord.setCreateTime(DateUtils.getCurrentDateOfDb());
		memberWinningAccord.setMapsQrCode(mapsQrCode);
		memberWinningAccordService.save(memberWinningAccord);
		
	}
	
	
	
	/**
	 * extractAttendAwards:(没有奖品实例，做未中奖处理). <br/>
	 * Date: 2015年12月8日 上午10:24:21 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @param memberId
	 * @param attendPrizeId
	 * @return
	 */
	private WinningItem extractAttendAwards(Long activityId, Long memberId,String attendPrizeId) {
		
		WinningItem winningItem = new WinningItem();
		String cacheKey = DateUtils.getCurrentDateOfDb() + "_" + attendPrizeId;
		synchronized(attendPrizeId){
			winningItem.setActivityId(activityId);
			winningItem.setMemberId(memberId);
			winningItem.setAwardsId(Long.valueOf(attendPrizeId));
			winningItem.setItemDate(DateUtils.getCurrentDateOfDb());
			winningItem.setItemTime(DateUtils.getCurrentTimeOfDb());
			
			winningItemService.save(winningItem);
			updateCountWinningItemToCache(cacheKey);
		}
		
		return winningItem;
	}

	/**
	 * extractLastAwards:(根据所中奖项抽取奖品实例). <br/>
	 * Date: 2015年11月26日 上午10:25:00 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @param memberId
	 * @param awardsId
	 * @return
	 */
	private WinningItem extractLastAwards(Long activityId, Long memberId,Long awardsId) {
		Prize prize =null;
		List<CouponInstance> couponInstanceList =null;
		List<MemberPointsItem> pointsResults =null;
		MemberPointsItem memberPointsItem =null;
		CouponInstance couponInstance =null;
		memberPointsItem =new MemberPointsItem();
		WinningItem winningItem = new WinningItem();
		pointsResults =new ArrayList<MemberPointsItem>();
		couponInstanceList =new ArrayList<CouponInstance>();
		Coupon coupon = null;
		String startDate = null;
		String expireDate = null;
		WechatMsgQueueH msgQueueH = null;
		if(StringUtils.isBlank(awardsId+"")){
			throw new ActivityException(ActivityException.NO_PRIZE_INSTANCE);
		}
		String cacheKey = DateUtils.getCurrentDateOfDb() + "_" + awardsId;
		
		synchronized(awardsId){
			PrizeInstanceExp prizeInstanceExp = prizeInstanceService.findPrizeInstanceExpByAwardsId(awardsId);
			if(prizeInstanceExp==null){
				throw new ActivityException(ActivityException.NO_PRIZE_INSTANCE);
			}
			prizeInstanceExp.setStatusId(StatusConstant.PRIZE_INSTANCE_EXTRACTED.getId());
			PrizeInstance prizeInstance = new PrizeInstance();
			BeanUtils.copy(prizeInstanceExp, prizeInstance);
			prizeInstanceService.updateByPk(prizeInstance);
			// 如果抽中的是积分或者是优惠券，则发放到会员账户中
			prize =prizeService.queryPrizeByAwardId(awardsId);
			// 如果是优惠券
			if(new Integer(PRIZE_COUPON_TYPE_ID).equals(prize.getPrizeType())){
				couponInstance = couponInstanceService.queryByPk(prizeInstanceExp.getExchangeCode1());
				coupon = couponRepository.findByPk(couponInstance.getCouponId());
				couponInstanceList.add(couponInstance);
				
				// 将优惠券放进会员账户中
				couponInstanceService.insertCouponInstanceUpdate(memberId, couponInstanceList);
				
				
				
				//微信模板消息
				msgQueueH = new WechatMsgQueueH();
				msgQueueH.setBusinessType(WechatMsgBusinessTypeEnum.GET_COUPON_ACTIVITY.getId());
				msgQueueH.setCreateTime(DateUtils.getCurrentTimeOfDb());
				msgQueueH.setMemberId(memberId);
				msgQueueH.setMsgKey(couponInstance.getCouponInstanceCode());
				msgQueueH.setIsSend("N");
				
				if(coupon.getExpireType().equals("1")){
					startDate = coupon.getFromDate();
					expireDate = coupon.getThruDate();
				}else if(coupon.getExpireType().equals("2")){
					startDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate()));
					expireDate = DateUtils.addDays(Integer.parseInt(coupon.getAfterDate())+Integer.parseInt(coupon.getExpireValue()));
				}
				
				//修改券有效期
				couponInstance.setStartDate(startDate);
				couponInstance.setExpireDate(expireDate);
				couponInstanceService.updateByPkSelective(couponInstance);
				
				startDate = DateUtils.dateStrToNewFormat(startDate, DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
				expireDate = DateUtils.dateStrToNewFormat(expireDate, DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
				
				WechatCouponMsg wechatCouponMsg = new WechatCouponMsg();
				wechatCouponMsg.setCouponName(coupon.getCouponName());
				wechatCouponMsg.setStartDate(startDate);
				wechatCouponMsg.setExpireDate(expireDate);
				wechatCouponMsg.setComments(coupon.getComments());
				
				JSONObject json = JSONObject.fromObject(wechatCouponMsg);
				msgQueueH.setMsgContent(json.toString());
				
				wechatMsgQueueHRepository.saveSelective(msgQueueH);
				
				// 如果是王冠(相当于积分)
			}else if(new Integer(PRIZE_ENTITY_TYPE_ID).equals(prize.getPrizeType())){
				BigDecimal quantity =new BigDecimal(prizeInstanceExp.getExchangeCode2());
				memberPointsItem.setAvailablePointsNumber(quantity);
				memberPointsItem.setHappenTime(DateUtils.getCurrentTimeStr());
				memberPointsItem.setCreateTime(DateUtils.getCurrentTimeStr());
				memberPointsItem.setInvalidDate(DateUtils.addYears(1));
				memberPointsItem.setExternalId(prizeInstanceExp.getPrizeInstanceId().toString());
				memberPointsItem.setMemberId(memberId);
				memberPointsItem.setItemPointsNumber(quantity);
				memberPointsItem.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId());
				memberPointsItem.setPointsReason("欧冠抽奖");
				pointsResults.add(memberPointsItem);
			}
			
			winningItem.setPrizeId(prizeInstanceExp.getPrizeId());
			winningItem.setPrizeName(prizeInstanceExp.getPrizeName());
			winningItem.setPrizeInstanceId(prizeInstanceExp.getPrizeInstanceId());
			winningItem.setActivityId(activityId);
			winningItem.setMemberId(memberId);
			winningItem.setAwardsId(awardsId);
			winningItem.setItemDate(DateUtils.getCurrentDateOfDb());
			winningItem.setItemTime(DateUtils.getCurrentTimeOfDb());
			
			winningItemService.save(winningItem);
			updateCountWinningItemToCache(cacheKey);
		}
		
		return winningItem;
	}
	
	/**
	 * 分发抽到的奖品
	 * @param activityId
	 * @param memberId
	 * @param awardsId
	 * @return
	 */
	private AwardsSettingExp extractLastAwardsEveryDay(Long activityId, Long memberId,Long awardsId) {
		Prize prize =null;
		WinningItem winningItem = new WinningItem();
		if(StringUtils.isBlank(awardsId+"")){
			throw new ActivityException(ActivityException.NO_PRIZE_INSTANCE);
		}
		String cacheKey = DateUtils.getCurrentDateOfDb() + "_" + awardsId;
		AwardsSettingExp awardsSettingExp =null;
		synchronized(awardsId){
			// 如果抽中的是实物品或者是优惠券，则发放到会员账户中
			awardsSettingExp = awardsSettingService.queryAwardsSettingExpByAwardsId(awardsId);
			prize =prizeService.queryPrizeByAwardId(awardsId);
			if(new Integer(PRIZE_ENTITY_TYPE_ID).equals(prize.getPrizeType())){
				
			}
			// 如果抽中的是优惠券，则发放到会员账户中
			if(new Integer(PRIZE_COUPON_TYPE_ID).equals(prize.getPrizeType())){
				CouponInstance couponInstance = couponIssueService.getLotteryGift(memberId, prize,awardsSettingExp);
				if(couponInstance!=null){
					awardsSettingExp.setCouponCodeInstance(couponInstance.getCouponInstanceCode());
				}
			}
			// 如果抽中的是徽章，则发放到会员账户中
			if(new Integer(PRIZE_CARD_TYPE_ID).equals(prize.getPrizeType())){
				memberBadgeItemSevice.saveMemberBadgeByMemberId(memberId,activityId,awardsId);
			}
			winningItem.setPrizeId(prize.getPrizeId());
			winningItem.setPrizeName(prize.getPrizeName());
			//winningItem.setActivityId(activityId);
			winningItem.setSceneGameInstanceId(activityId);
			winningItem.setMemberId(memberId);
			winningItem.setAwardsId(awardsId);
			winningItem.setItemDate(DateUtils.getCurrentDateOfDb());
			winningItem.setItemTime(DateUtils.getCurrentTimeOfDb());
			winningItemService.saveSelective(winningItem);
			updateCountWinningItemToCache(cacheKey);
		}
		return awardsSettingExp;
	}

	/**
	 * 
	 * extractTagertPrize:(抽取奖项). <br/>
	 * Date: 2015年11月25日 下午4:34:04 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param currentMap
	 * @return
	 */
	private Long extractTagertPrize(Map<Long, AwardsSetting> currentMap) {
		
		Double totalRate =0d;
		Map<Integer,Long> orignalAwardIds =new HashMap<Integer,Long>();
		Integer orignalSort =new Integer(0);
		List<Double> orignalprobability =new ArrayList<Double>();
		for(Map.Entry<Long, AwardsSetting> entry : currentMap.entrySet()){
			orignalAwardIds.put(orignalSort, entry.getKey());
			orignalprobability.add(new Double(entry.getValue().getProbability()+""));
			orignalSort++;
		}
		// 计算总概率，这样可以保证不一定总概率是1
		for(Double rate1:orignalprobability){
			totalRate +=rate1;
		}
		
		double tempSumRate =0d;
		List<Double> sortList =new ArrayList<Double>();
		// 计算每个物品在总概率的基础下的概率情况
		for(double rate:orignalprobability){
			tempSumRate+=rate;
			sortList.add(tempSumRate/totalRate);
		}
		
		double nextDouble = Math.random();
		sortList.add(nextDouble);
	    Collections.sort(sortList);
		
	    Integer sort = sortList.indexOf(nextDouble);
	    Long awardIds = orignalAwardIds.get(sort);
		
		return awardIds;
	}
	
	private Long extractTagertPrize(Long activityId,Long memberId){
		List<AwardsSettingExp> awardsSettingList =null;
		Map<Long, AwardsSetting> probabilityMap = null;
		
		String airCleanerId = jedisService.rpop("airCleanerId");
		
		if(airCleanerId!=null&&StringUtils.isNotBlank("nil")){
			return Long.parseLong( airCleanerId);
		}
		
		probabilityMap = new HashMap<Long, AwardsSetting>();
		String awardsCacheKey="awardsCacheKey"+activityId;
		boolean awardsExist = jedisService.exists(awardsCacheKey);
		if(!awardsExist){
			awardsSettingList = awardsSettingService.queryAwardsSettingExpByActivityId(activityId);
			String awardsJson = JSON.toJSONString(awardsSettingList);
			jedisService.set(awardsCacheKey, awardsJson);
		}else{
			String awardsJson = jedisService.get(awardsCacheKey);
			awardsSettingList = JSON.parseArray(awardsJson, AwardsSettingExp.class);
		}
		//1000秒之后过期
		jedisService.expire(awardsCacheKey, 1000);
		String totalPersonCacheKey="totalPersonCacheKey"+activityId;
		boolean totalPersonExist = jedisService.exists(totalPersonCacheKey);
		Long totalPersons = null;
		//将人数放入redis
		if(!totalPersonExist){
			totalPersons = winningItemService.countPersonsCountByActivityIdAndAwardsId(null,activityId,null,null,null);
			jedisService.set(totalPersonCacheKey, String.valueOf(totalPersons));
		}else{
			totalPersons = Long.parseLong(jedisService.get(totalPersonCacheKey));
		}
		//300秒之后过期
		jedisService.expire(totalPersonCacheKey,300);
		jedisService.incr(totalPersonCacheKey);
		// 活动id对应的奖项设置
		System.out.println(jedisService.get(totalPersonCacheKey));
		AwardsSettingExp aircleanAwards=null;
		AwardsSettingExp wheatHeadAwards=null;
		
		for (AwardsSettingExp wardsSetting : awardsSettingList) {
			//如果是空气净化器，则取第规则
			if(StringUtils.equals(wardsSetting.getAwardsCode(), LotteryConstant.LOTTERY_TYPE_AIR_CLEANER.getCode())){
				aircleanAwards=wardsSetting;
			}
			//如果是榉木
			if(StringUtils.equals(wardsSetting.getAwardsCode(), LotteryConstant.LOTTERY_TYPE_WHEAT_HEAD.getCode())){
				wheatHeadAwards=wardsSetting;
			}
			probabilityMap.put(wardsSetting.getAwardsId(), wardsSetting);
		}
		if(aircleanAwards!=null){
			//判断库存
			Integer persions = aircleanAwards.getPersions();
			//判断空气净化器
			Long airCleanerCycle= Long.parseLong(aircleanAwards.getCycleValue());
			//判断当前是否是第cycle个人
			if(airCleanerCycle!=0&&(totalPersons+1)%airCleanerCycle==0){
				//判断库存
				Long airCleanValue=winningItemService.countPersonsCountByActivityIdAndAwardsId(aircleanAwards.getAwardsId(),activityId,null,null,null);//判断已经中奖的
				//如果库存好过
				if(!(airCleanValue.compareTo((long)persions)>=0)){
					if(wheatHeadAwards!=null){
						Long wheats=winningItemService.countPersonsCountByActivityIdAndAwardsId(wheatHeadAwards.getAwardsId(),activityId,memberId,null,null);
						if(wheats>0){
							jedisService.lpush("airCleanerId", String.valueOf(aircleanAwards.getAwardsId()));
						}
					}else{
						//让此人中奖
						return aircleanAwards.getAwardsId();
					}
				}
			}
		}
		if(wheatHeadAwards!=null){
			Long wheatHeadCycle= Long.parseLong(wheatHeadAwards.getCycleValue());
			if((totalPersons+1)%wheatHeadCycle==0){
				return wheatHeadAwards.getAwardsId();
			}
		}
		Long awardsId = extractTagertPrize(probabilityMap);
		return awardsId;
	}
	
	/**
	 * 根据配置计算当日各奖项的中奖概率
	 * calculateDayProbability:(). <br/>
	 * Date: 2015年11月24日 下午4:01:41 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 * ************************************************************* 
	 *	思路:   1.  首先根据activityId查询出某一个活动对应奖项的概率以及获奖人数等.
	 *		  2.  将map中的id对应的awardsSetting放入到map中
	 *		  3.  统计出对应奖项的周期内已经获奖的人数并将其与奖项设置中的人数对比，将已将没有获奖人数的奖项remove掉
	 *		  4.  剩余的map中就是用户可能中奖的范围
	 *		  5.  计算这些奖项的概率		
	 * 
	 */
	private Map<Long, AwardsSetting> calculateMapsProbability(Long activityId) {
		List<AwardsSetting> awardsSettingList =null;
		Map<Long, AwardsSetting> probabilityMap = null;
		AwardsSetting as = null;
		
		probabilityMap = new HashMap<Long, AwardsSetting>();
		String day = DateUtils.getCurrentDateOfDb();
		// 活动id对应的奖项设置
		awardsSettingList = awardsSettingService.queryAwardsSettingsByActivityId(activityId);
		
		for (AwardsSetting wardsSetting : awardsSettingList) {
			probabilityMap.put(wardsSetting.getAwardsId(), wardsSetting);
		}
		
		Long persons = null;
		List<Long> removeAwardsList = new ArrayList<Long>();
		String startDate = null; 
		String endDate = null;
		String cacheKey = null;
		
		for(Map.Entry<Long, AwardsSetting> entry : probabilityMap.entrySet()){
			as = entry.getValue();
			cacheKey = day + "_" + as.getAwardsId();
			persons = readWinningItemFromCache(cacheKey);
			logger.info("奖项ID:" + as.getAwardsId() + "=================当前计算周期的中奖人数==============:" + persons);
			if(persons ==null){
				persons = winningItemService.countPersonsCountByActivityIdAndAwardsId(as.getAwardsId(),activityId,null,startDate,endDate);
				addCountWinningItemToCache(cacheKey,persons);
			}
			
			if(as.getProbability().compareTo(BigDecimal.ZERO) == -1){
				removeAwardsList.add(as.getAwardsId());
			}
		}
		
		// 去除已经无关的获奖奖项awardId
		for (Long removeId : removeAwardsList) {
			probabilityMap.remove(removeId);
		}
		
		return probabilityMap;
	}


	/**
	 * 根据配置计算当日各奖项的中奖概率
	 * calculateDayProbability:(). <br/>
	 * Date: 2015年11月24日 下午4:01:41 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 * ************************************************************* 
	 *	思路:   1.  首先根据activityId查询出某一个活动对应奖项的概率以及获奖人数等.
	 *		  2.  将map中的id对应的awardsSetting放入到map中
	 *		  3.  统计出对应奖项的周期内已经获奖的人数并将其与奖项设置中的人数对比，将已将没有获奖人数的奖项remove掉
	 *		  4.  剩余的map中就是用户可能中奖的范围
	 *		  5.  计算这些奖项的概率		
	 * 
	 */
	private Map<Long, AwardsSetting> calculateMonthProbability(Long activityId,Long memberId) {
		List<AwardsSettingExp> awardsSettingList =null;
		Map<Long, AwardsSetting> probabilityMap = null;
		AwardsSetting as = null;
		
		probabilityMap = new HashMap<Long, AwardsSetting>();
		String day = DateUtils.getCurrentDateOfDb();
		String awardsCacheKey="awardsCacheKey"+activityId;
		boolean awardsExist = jedisService.exists(awardsCacheKey);
		if(!awardsExist){
			awardsSettingList = awardsSettingService.queryAwardsSettingExpByActivityId(activityId);
			String awardsJson = JSON.toJSONString(awardsSettingList);
			jedisService.set(awardsCacheKey, awardsJson);
		}else{
			String awardsJson = jedisService.get(awardsCacheKey);
			awardsSettingList = JSON.parseArray(awardsJson, AwardsSettingExp.class);
		}
		
		for (AwardsSetting wardsSetting : awardsSettingList) {
			probabilityMap.put(wardsSetting.getAwardsId(), wardsSetting);
		}
		
		//统计人数
		Long totalPersons = winningItemService.countPersonsCountByActivityIdAndAwardsId(null,activityId,null,null,null);
		
		Long persons = null;
		List<Long> removeAwardsList = new ArrayList<Long>();
		String startDate = null; 
		String endDate = null;
		String cacheKey = null;
		
		for(Map.Entry<Long, AwardsSetting> entry : probabilityMap.entrySet()){
			as = entry.getValue();
			cacheKey = day + "_" + as.getAwardsId();
			persons = readWinningItemFromCache(cacheKey);
			logger.info("奖项ID:" + as.getAwardsId() + "=================当前计算周期的中奖人数==============:" + persons);
			if(persons ==null){
				//如果有数量限制，进行排序
				persons = winningItemService.countPersonsCountByActivityIdAndAwardsId(as.getAwardsId(),activityId,null,startDate,endDate);
				addCountWinningItemToCache(cacheKey,persons);
			}
			
			/**
			 * *******************
			 * 	两个条件:
			 *  1.获奖人数超过设置数(设置0为无限)
			 *  2.奖品的剩余数量没有了
			 */
			if (persons >= as.getPersions() && as.getPersions()!= -1) {
				removeAwardsList.add(as.getAwardsId());
			}
			if(as.getProbability().compareTo(BigDecimal.ZERO) == 0){
				removeAwardsList.add(as.getAwardsId());
			}
		}
		
		// 去除已经无关的获奖奖项awardId
		for (Long removeId : removeAwardsList) {
			probabilityMap.remove(removeId);
		}
		/*
		 *如果没有没有奖品实例，提示 
		 */
		boolean empty = probabilityMap.isEmpty();
		if(empty){
			throw new ActivityException(ActivityException.NO_PRIZE_INSTANCE);
		}
		return probabilityMap;
	}
	
	private void addCountWinningItemToCache(String key, Long persons){
		Lock lock = readWriteLock.writeLock();
		try{
			lock.lock();
			WINNING_ITEM_PERSONS.put(key, persons);
		}finally{
			lock.unlock();
		}
	}
	
	private Long readWinningItemFromCache(String key){
		String day = DateUtils.getCurrentDateOfDb();
		Lock lock = readWriteLock.readLock();
		Long count = null;
		try{
			lock.lock();
			count = WINNING_ITEM_PERSONS.get(key);
		}finally{
			lock.unlock();
		}
		if(count == null && !day.equals(CURRENT_DATE)){
			clearCountWinningItemToCache();
			CURRENT_DATE = day;
		}
		return count;
	}
	
	
	private void clearCountWinningItemToCache(){
		Lock lock = readWriteLock.writeLock();
		try{
			lock.lock();
			WINNING_ITEM_PERSONS.clear();
		}finally{
			lock.unlock();
		}
	}
	
	private void updateCountWinningItemToCache(String key){
		Lock lock = readWriteLock.writeLock();
		try{
			lock.lock();
			Long count =  WINNING_ITEM_PERSONS.get(key);
			if(count == null){
				count = 0l;
			}
			WINNING_ITEM_PERSONS.put(key, count + 1);
		}finally{
			lock.unlock();
		}
	}
	
	@Autowired
	public void setMemberWinningAccordService(MemberWinningAccordService memberWinningAccordService) {
		this.memberWinningAccordService = memberWinningAccordService;
	}
	
	@Autowired
	public void setAwardsSettingService(AwardsSettingService awardsSettingService) {
		this.awardsSettingService = awardsSettingService;
	}
	
	@Autowired
	public void setWinningItemService(WinningItemService winningItemService) {
		this.winningItemService = winningItemService;
	}

	@Autowired
	public void setPrizeInstanceService(PrizeInstanceService prizeInstanceService) {
		this.prizeInstanceService = prizeInstanceService;
	}
	
	@Autowired
	public void setPrizeService(PrizeService prizeService) {
		this.prizeService = prizeService;
	}
	
	@Autowired
	public void setCouponInstanceService(CouponInstanceService couponInstanceService) {
		this.couponInstanceService = couponInstanceService;
	}

	@Autowired
	public void setCouponRepository(CouponRepository couponRepository) {
		this.couponRepository = couponRepository;
	}

	@Autowired
	public void setWechatMsgQueueHRepository(
			WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}

	@Autowired
	public void setMemberWinningAccordRepository(
			MemberWinningAccordRepository memberWinningAccordRepository) {
		this.memberWinningAccordRepository = memberWinningAccordRepository;
	}
	@Autowired
	public void setMemberBadgeItemSevice(MemberBadgeItemSevice memberBadgeItemSevice) {
		this.memberBadgeItemSevice = memberBadgeItemSevice;
	}

}

