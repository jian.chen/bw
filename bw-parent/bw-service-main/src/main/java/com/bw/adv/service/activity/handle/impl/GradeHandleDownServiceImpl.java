package com.bw.adv.service.activity.handle.impl;


import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bw.adv.module.member.constant.GradeFactorsConstant;
import com.bw.adv.module.member.enums.GradeChangeTypeEnums;
import com.bw.adv.module.member.exception.GradeException;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.GradeConf;
import com.bw.adv.module.member.model.GradeConvertRecord;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.repository.GradeConvertRecordRepository;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.handle.GradeHandleDownService;


/**
 * ClassName: ActivityActionHandleSupport <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-29 上午11:21:20 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public  class GradeHandleDownServiceImpl implements GradeHandleDownService{
	
	private static final Logger logger = LoggerFactory.getLogger(GradeHandleDownServiceImpl.class);
	
	@Value("${member_default_grade_seq}")
	private Integer memberDefaultGrade;
	private String gradeCalcMode=null;
	private final static String REGISTER_TIME = "register_time";
	private final static String FIRST_CONSUMPTION_TIME = "first_consumption_time";
	
	
	private MemberRepository memberRepository;
	private GradeConvertRecordRepository gradeConvertRecordRepository;
	
	public void operateGradeDownBusiness(){
		GradeConf gradeConf = null;
		int yearFrequency = 0;
		String beginDate = null;
		String endDate = null;
		List<Grade> gradeList = null;
		String currentMonthDay = null;
		List<MemberExp> memberExpList = null;
		BigDecimal gradeFactorsValue = null;
		Grade currentGrade = null;
		Grade changeGrade = null;
		Member memberGrade = null;
		GradeConvertRecord gradeConvertRecord = null;
		try {
			
			gradeConf = GradeInit.GRADE_CONF;
			//如果是手动升级
			if(gradeConf.getChangeType().equals(GradeChangeTypeEnums.HANDLE.getId())){
				logger.info("降级业务：降级方式"+GradeChangeTypeEnums.HANDLE.getDesc());
				return;
			}
			if(gradeConf.getIsActive().equals("N")){
				logger.info("等级功能已停用");
				return;
			}
			logger.info("降级业务：降级方式"+GradeChangeTypeEnums.AUTO.getDesc());
			
			yearFrequency = Integer.parseInt(gradeConf.getUpPeriod());
			currentMonthDay = DateUtils.formatCurrentDate("MMdd");
			
			beginDate = DateUtils.addYears(-yearFrequency);
			endDate = DateUtils.getCurrentDateOfDb();
			
			gradeList = GradeInit.GRADE_LIST;//得到等级列表
			
			
			if(StringUtils.isBlank(gradeCalcMode) || gradeCalcMode.equals(FIRST_CONSUMPTION_TIME)){
				if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.POINTS_PERIOD.getId())){
					memberExpList = memberRepository.findPeriodPointsByConsumeDate(currentMonthDay, beginDate, endDate);
				}
				if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getId())){
					memberExpList = memberRepository.findPeriodOrderAmountByConsumeDate(currentMonthDay, beginDate, endDate);
				}
			}else if(gradeCalcMode.equals(REGISTER_TIME)){
				if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.POINTS_PERIOD.getId())){
					memberExpList = memberRepository.findPeriodPoints(currentMonthDay, beginDate, endDate);
				}
				if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getId())){
					memberExpList = memberRepository.findPeriodOrderAmount(currentMonthDay, beginDate, endDate);
				}
			}
			
			if(memberExpList == null || memberExpList.size() <= 0){
				return;
			}
			
			changeGrade = GradeInit.getGradeBySeq(memberDefaultGrade);
			
			for (MemberExp memberExp : memberExpList) {
				currentGrade = GradeInit.getGradeByPk(memberExp.getGradeId());
				if(currentGrade == null){
					continue;
				}
				//如果是根据积分升级
				if(gradeConf.getGradeFactorsId() == GradeFactorsConstant.POINTS_PERIOD.getId()){
					gradeFactorsValue = memberExp.getPointsPeriod(); 
				}
				
				if(gradeConf.getGradeFactorsId() == GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getId()){
					gradeFactorsValue = memberExp.getOrderAmountPeriod(); 
				}
				
				if(gradeFactorsValue == null){
					gradeFactorsValue = BigDecimal.ZERO;
				}
				
				for (Grade grade : gradeList) {
					//如果大于等级的设置值，则满足该等级
					if(gradeFactorsValue.compareTo(new BigDecimal(grade.getRetainValue())) >=0 ){
						changeGrade = grade;
					}
				}
				
				//降级
				if(changeGrade != null && !currentGrade.getGradeId().equals(changeGrade.getGradeId())){
					if(changeGrade.getSeq() <  currentGrade.getSeq()){
						//修改会员等级
						memberGrade = new Member();
						memberGrade.setMemberId(memberExp.getMemberId());
						memberGrade.setGradeId(changeGrade.getGradeId());
						memberRepository.updateByPkSelective(memberGrade);
						
						gradeConvertRecord = new GradeConvertRecord();
						gradeConvertRecord.setGcDate(DateUtils.getCurrentDateOfDb());
						gradeConvertRecord.setGradeFactorsId(gradeConf.getGradeFactorsId());
						gradeConvertRecord.setSrcGradeId(currentGrade.getGradeId());
						gradeConvertRecord.setTargetGradeId(changeGrade.getGradeId());
						gradeConvertRecord.setUpDownFlag("D");//升降级标示U:升级，D:降级
						gradeConvertRecord.setGradeFactorsValue(gradeFactorsValue);
						gradeConvertRecord.setMemberId(memberExp.getMemberId());
						if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.POINTS_PERIOD.getId())){
							gradeConvertRecord.setGradeFactorsDesc(GradeFactorsConstant.POINTS_PERIOD.getName()+"为"+gradeFactorsValue+",降级为"+changeGrade.getGradeName());
						}
						if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getId())){
							gradeConvertRecord.setGradeFactorsDesc(GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getName()+"为"+gradeFactorsValue+",降级为"+changeGrade.getGradeName());
						}
						gradeConvertRecord.setCreateTime(DateUtils.getCurrentTimeOfDb());
						gradeConvertRecordRepository.save(gradeConvertRecord);
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new GradeException("等级升级异常:"+e.getMessage());
		}
		
	}

	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}

	@Autowired
	public void setGradeConvertRecordRepository(GradeConvertRecordRepository gradeConvertRecordRepository) {
		this.gradeConvertRecordRepository = gradeConvertRecordRepository;
	}

}
















