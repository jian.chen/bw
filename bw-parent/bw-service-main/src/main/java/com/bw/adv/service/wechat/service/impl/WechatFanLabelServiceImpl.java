/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFanLabelServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年9月3日下午3:38:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatFanLabel;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.exp.WechatFanLabelExp;
import com.bw.adv.module.wechat.repository.WechatFanLabelRepository;
import com.bw.adv.service.wechat.service.WechatFanLabelService;

/**
 * ClassName:WechatFanLabelServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月3日 下午3:38:15 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatFanLabelServiceImpl extends BaseServiceImpl<WechatFanLabel> implements WechatFanLabelService {
	
	private WechatFanLabelRepository wechatFanLabelRepository;
	
	
	@Override
	public BaseRepository<WechatFanLabel, ? extends BaseMapper<WechatFanLabel>> getBaseRepository() {
		return wechatFanLabelRepository;
	}
	
	
	@Override
	public List<WechatFanLabelExp> handleData(List<WechatFanLabelExp> wechatFanLabelExpList,String labelId) {
		List<WechatFanLabelExp> wechatFanLabelList =null;
		List<WechatFanLabelExp> replaceWechatFanLabelList =null;
		replaceWechatFanLabelList =new ArrayList<WechatFanLabelExp>();
		
		if(!StringUtils.isEmpty(labelId)){
			for(WechatFanLabelExp fansLabel:wechatFanLabelExpList){
				String Label ="";
				List<Long> labelIds =new ArrayList<Long>();
				wechatFanLabelList = this.queryWechatFanLabelList(fansLabel.getWechatFansId());
				if(null !=wechatFanLabelList){
					for(int i=0;i<wechatFanLabelList.size();i++){
						labelIds.add(wechatFanLabelList.get(i).getWechatFansLabelList().get(0).getWechatFansLabelId());
						if(i<wechatFanLabelList.size()-1){
							Label += wechatFanLabelList.get(i).getWechatFansLabelList().get(0).getLabelName()+",";
						}else{
							Label += wechatFanLabelList.get(i).getWechatFansLabelList().get(0).getLabelName();
						}
					}
				}
				fansLabel.setWechatFansLabel(Label);
				// 如果没有包含特定的标签
				if(!labelIds.contains(Long.valueOf(labelId))){
					continue;
				}else{
					replaceWechatFanLabelList.add(fansLabel);
				}
			}
		}else{
			for(WechatFanLabelExp fansLabel:wechatFanLabelExpList){
				String Label ="";
				wechatFanLabelList = this.queryWechatFanLabelList(fansLabel.getWechatFansId());
				if(null !=wechatFanLabelList){
					for(int i=0;i<wechatFanLabelList.size();i++){
						if(i<wechatFanLabelList.size()-1){
							Label += wechatFanLabelList.get(i).getWechatFansLabelList().get(0).getLabelName()+",";
						}else{
							Label += wechatFanLabelList.get(i).getWechatFansLabelList().get(0).getLabelName();
						}
					}
				}
				fansLabel.setWechatFansLabel(Label);
				replaceWechatFanLabelList.add(fansLabel);
			}
		}
		return replaceWechatFanLabelList;
	}
	
	@Override
	public void saveFanLabelsRelative(String wechatFansId, List<String> labelIds) {
		List<WechatFanLabelExp> wechatFanLabelList =null;
		
		wechatFanLabelList = this.queryWechatFanLabelList(Long.valueOf(wechatFansId));
		if(wechatFanLabelList.size()>0){
			// 之前的标签关系置为空
			for(WechatFanLabelExp wechatFanLabelExp:wechatFanLabelList){
				WechatFanLabel wechatFanLabel = this.queryByPk(wechatFanLabelExp.getWechatFanLabelId());
				wechatFanLabel.setUpdateTime(DateUtils.getCurrentTimeOfDb());
				wechatFanLabel.setIsDelete(WechatTypeConstant.DELETED.getCode());
				this.updateByPk(wechatFanLabel);
			}
		}
		// 添加现有的标签关系 
		for(String wechatLabelId:labelIds){
			WechatFanLabel wechatFanLabel =new WechatFanLabel();
			wechatFanLabel.setCreatedTime(DateUtils.getCurrentTimeOfDb());
			wechatFanLabel.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
			wechatFanLabel.setWechatFansId(Long.valueOf(wechatFansId));
			wechatFanLabel.setWechatFansLabelId(Long.valueOf(wechatLabelId));
			this.save(wechatFanLabel);
		}
		
	}

	@Override
	public void batchAddLabelToFans(String tagId, List<String> wechatFansList) {
		List<WechatFanLabelExp> wechatFansLabels =null;
		for(String wechatFansId:wechatFansList){
			wechatFansLabels = this.queryWechatFanLabelList(Long.valueOf(wechatFansId));
			List<Long> existLabelId =new ArrayList<Long>();
			for(WechatFanLabelExp labelInfo :wechatFansLabels){
				existLabelId.add(labelInfo.getWechatFansLabelList().get(0).getWechatFansLabelId());
			}
			// 如果粉丝标签不存在
			if(!existLabelId.contains(Long.valueOf(tagId))){
				// 添加
				WechatFanLabel wechatFanLabel =new WechatFanLabel();
				wechatFanLabel.setCreatedTime(DateUtils.getCurrentTimeOfDb());
				wechatFanLabel.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
				wechatFanLabel.setWechatFansId(Long.valueOf(wechatFansId));
				wechatFanLabel.setWechatFansLabelId(Long.valueOf(tagId));
				this.save(wechatFanLabel);
			}
			
		}
		
	}
	
	@Override
	public List<WechatFanLabelExp> transToLabelExpList(List<WechatFans> wechatFans) {
		List<WechatFanLabelExp> wechatFansLabels =null;
		wechatFansLabels =new ArrayList<WechatFanLabelExp>();
		for(WechatFans wechatFan:wechatFans){
			WechatFanLabelExp wechatFanLabelExp =new WechatFanLabelExp();
			wechatFanLabelExp.setWechatFansId(wechatFan.getWechatFansId());
			wechatFanLabelExp.setNickName(wechatFan.getNickName());
			wechatFanLabelExp.setOpenid(wechatFan.getOpenid());
			wechatFanLabelExp.setHeadImgUrl(wechatFan.getHeadImgUrl());
			wechatFansLabels.add(wechatFanLabelExp);
		} 
		return wechatFansLabels;
	}


	
	@Override
	public List<WechatFanLabelExp> queryWechatFanLabelExp(Long wechatFansLabelId, Long wechatFansId ,Page<WechatFanLabelExp> page) {
		return wechatFanLabelRepository.findWechatFanLabelExp(wechatFansLabelId,wechatFansId,page);
	}
	
	

	@Override
	public List<WechatFanLabelExp> queryWechatFanLabelList(Long wechatFansId) {
		return wechatFanLabelRepository.findWechatFanLabelList(wechatFansId);
	}
	
	@Autowired
	public void setWechatFanLabelRepository(WechatFanLabelRepository wechatFanLabelRepository) {
		this.wechatFanLabelRepository = wechatFanLabelRepository;
	}



}

