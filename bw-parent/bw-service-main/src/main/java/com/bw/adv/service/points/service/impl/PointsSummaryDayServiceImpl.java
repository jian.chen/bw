package com.bw.adv.service.points.service.impl;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.points.model.PointsSummaryDay;
import com.bw.adv.module.report.points.model.exp.PointsSummaryDayExp;
import com.bw.adv.module.report.repository.PointsSummaryDayRepository;
import com.bw.adv.service.points.service.PointsSummaryDayService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PointsSummaryDayServiceImpl extends BaseServiceImpl<PointsSummaryDay> implements
		PointsSummaryDayService {
	private PointsSummaryDayRepository pointsSummaryDayRepository;
	@Override
	public List<PointsSummaryDayExp> queryByExp(Example example) {
		return this.pointsSummaryDayRepository.findByExp(example);
	}

	@Override
	public BaseRepository<PointsSummaryDay, ? extends BaseMapper<PointsSummaryDay>> getBaseRepository() {
		return pointsSummaryDayRepository;
	}
	@Override
	public PointsSummaryDayExp queryPointsByExp(Example example) {
		return pointsSummaryDayRepository.findPointByExp(example);
	}
	@Override
	public void callPointsSummaryDay(String dateStr) {
		
		pointsSummaryDayRepository.callPointsSummaryDay(dateStr);
		
	}

	@Autowired
	public void setPointsSummaryDayRepository(
			PointsSummaryDayRepository pointsSummaryDayRepository) {
		this.pointsSummaryDayRepository = pointsSummaryDayRepository;
	}

}
