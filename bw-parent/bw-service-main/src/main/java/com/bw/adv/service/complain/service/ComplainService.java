/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ComplainService.java
 * Package Name:com.sage.scrm.service.complain.service
 * Date:2015年12月29日下午4:47:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.complain.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.complain.model.Complain;
import com.bw.adv.module.member.complain.model.exp.ComplainExp;

import java.util.List;

/**
 * ClassName:ComplainService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月29日 下午4:47:25 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface ComplainService extends BaseService<Complain>{
	
	List<ComplainExp> queryByExa(Example example,Page<ComplainExp> page);

	ComplainExp queryDetailByPk(Long complainId);

	void sendWcMsgByCloseComplain(Complain complain);

}

