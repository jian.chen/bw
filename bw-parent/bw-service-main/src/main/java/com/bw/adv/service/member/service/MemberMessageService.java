/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberService1.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2015年8月10日下午3:39:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.message.model.MemberMessage;
import com.bw.adv.module.member.message.model.exp.MemberMessageExp;


public interface MemberMessageService extends BaseService<MemberMessage>{
	
	/**
	 * queryList:查询消息中心列表 <br/>
	 * Date: 2016年2月22日 下午3:24:26 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param Page
	 * @return
	 */
	List<MemberMessage> queryList(Page<MemberMessage> Page);
	
	/**
	 * saveObj:保存消息<br/>
	 * Date: 2016年2月23日 下午3:49:49 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessage
	 * @return
	 */
	int saveObj(MemberMessage memberMessage);
	
	/**
	 * updateMessage:更新会员消息 <br/>
	 * Date: 2016年2月25日 下午2:58:46 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessage
	 * @return
	 */
	int updateMessage(MemberMessage memberMessage);
	
	/**
	 * updateStatus:修改会员消息状态为已删除 <br/>
	 * Date: 2016年2月25日 下午4:40:54 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 */
	void updateStatus(Long memberMessageId);
	
	/**
	 * queryListByMemberId:根据会员Id查询会员消息 <br/>
	 * Date: 2016年2月28日 下午11:41:16 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @param page
	 * @return
	 */
	List<MemberMessageExp> queryListByMemberId(Long memberId,Page<MemberMessageExp> Page);
	
	
}

