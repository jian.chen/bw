package com.bw.adv.service.cfg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.repository.ChannelInstanceRecordRepository;
import com.bw.adv.module.cfg.model.ChannelInstanceRecord;
import com.bw.adv.module.cfg.model.ChannelInstanceRecordDto;
import com.bw.adv.service.cfg.service.ChannelInstanceRecordService;
/**
 * ClassName: ChannelCfgServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月17日 下午3:48:10 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
@Service
public class ChannelInstanceRecordServiceImpl  extends BaseServiceImpl<ChannelInstanceRecord> implements ChannelInstanceRecordService {

	private ChannelInstanceRecordRepository channelInstanceRecordRepository;
	
	@Override
	public BaseRepository<ChannelInstanceRecord, ? extends BaseMapper<ChannelInstanceRecord>> getBaseRepository() {
		return channelInstanceRecordRepository;
	}
	@Override
	public List<ChannelInstanceRecordDto> findListByPages(Long channelCfgId,String recordMobile,Page<ChannelInstanceRecordDto> page) {
		return channelInstanceRecordRepository.findListByPages(channelCfgId,recordMobile,page);
	}
	
	
	@Autowired
	public void setChannelInstanceRecordRepository(ChannelInstanceRecordRepository channelInstanceRecordRepository) {
		this.channelInstanceRecordRepository = channelInstanceRecordRepository;
	}



	
	
	
	
}
