/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceUseRecordServiceImpl.java
 * Package Name:com.sage.scrm.bk.coupon.service.impl
 * Date:2015年11月19日下午5:58:58
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service.impl;

import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.coupon.model.CouponInstanceUseRecord;
import com.bw.adv.module.coupon.repository.CouponInstanceUseRecordRepository;
import com.bw.adv.service.coupon.service.CouponInstanceUseRecordService;

/**
 * ClassName:CouponInstanceUseRecordServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:58:58 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class CouponInstanceUseRecordServiceImpl extends BaseServiceImpl<CouponInstanceUseRecord> implements CouponInstanceUseRecordService{
	private CouponInstanceUseRecordRepository couponInstanceUseRecordRepository;
	@Override
	public BaseRepository<CouponInstanceUseRecord, ? extends BaseMapper<CouponInstanceUseRecord>> getBaseRepository() {
		return couponInstanceUseRecordRepository;
	}
	public void setCouponInstanceUseRecordRepository(CouponInstanceUseRecordRepository couponInstanceUseRecordRepository) {
		this.couponInstanceUseRecordRepository = couponInstanceUseRecordRepository;
	}

}

