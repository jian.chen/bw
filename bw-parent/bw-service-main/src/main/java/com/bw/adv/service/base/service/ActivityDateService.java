/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityDateService.java
 * Package Name:com.sage.scrm.service.base.service
 * Date:2015年8月27日下午7:13:45
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.ActivityDate;

/**
 * ClassName:ActivityDateService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月27日 下午7:13:45 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface ActivityDateService extends BaseService<ActivityDate> {
	
	/**
	 * 
	 * queryActivityDateByIsActive:查询有效的活动日期
	 * Date: 2015年9月2日 下午4:05:17 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	public List<ActivityDate> queryActivityDateByIsActive(Page<ActivityDate> page);
	
	/**
	 * 
	 * deleteActivityDateList:批量删除活动日期(逻辑删除)
	 * Date: 2015年9月2日 下午4:05:49 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param activityDateIds
	 */
	public void deleteActivityDateList(List<Long> activityDateIds);

	/**
	 * queryAllActiveDate:(查询所有启用日期列表). <br/>
	 * Date: 2015-9-3 下午2:57:18 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	public List<ActivityDate> queryAllActiveDate();
	
}

