/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:KeysGroupService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月24日下午5:30:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.KeysGroup;
import com.bw.adv.module.wechat.model.exp.KeysGroupExp;

/**
 * ClassName:KeysGroupService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午5:30:34 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface KeysGroupService extends BaseService<KeysGroup> {
	
	/**
	 * keywordReplay:(是否包含关键字). <br/>
	 * Date: 2015年8月26日 下午2:44:19 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param content
	 * @return
	 */
	public boolean keywordReplay(String content);
	
	/**
	 * queryKeysGroupExpBykeysGroup:(查询扩展类). <br/>
	 * Date: 2015年8月26日 下午6:15:36 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param keysGroup
	 * @return
	 */
	public KeysGroupExp queryKeysGroupExpBykeysGroup(KeysGroup keysGroup);
}

