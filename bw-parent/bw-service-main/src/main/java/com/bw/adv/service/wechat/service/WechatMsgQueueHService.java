/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMsgQueueHService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年12月16日上午11:12:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueHExp;

/**
 * ClassName:WechatMsgQueueHService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月16日 上午11:12:55 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatMsgQueueHService extends BaseService<WechatMsgQueueH>{
	public List<WechatMsgQueueHExp> findAllWechatMsgQueueHs(Long limit);
	public void deleteWechatMsgQueueH(List<WechatMsgQueueHExp> list);
	public void deleteAllWechatMsgQueueH(List<Long> list);
	public void updateBatchWechatMsgQueueH(List<WechatMsgQueueHExp> list);
	/**
	 * saveWechatMsgQueueH:保存消息 <br/>
	 * Date: 2016年1月7日 下午4:32:38 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param list
	 */
	public void saveWechatMsgQueueH(List<WechatMsgQueueH> list);
}

