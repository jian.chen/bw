/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceBrowseServiceImpl.java
 * Package Name:com.sage.scrm.bk.coupon.service.impl
 * Date:2015年11月19日下午5:18:13
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.coupon.model.CouponInstanceBrowse;
import com.bw.adv.module.coupon.repository.CouponInstanceBrowseRepository;
import com.bw.adv.service.coupon.service.CouponInstanceBrowseService;

/**
 * ClassName:CouponInstanceBrowseServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:18:13 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */	
@Service
public class CouponInstanceBrowseServiceImpl extends BaseServiceImpl<CouponInstanceBrowse> implements CouponInstanceBrowseService{
	private CouponInstanceBrowseRepository couponInstanceBrowseRepository;
	@Override
	public BaseRepository<CouponInstanceBrowse, ? extends BaseMapper<CouponInstanceBrowse>> getBaseRepository() {
		return couponInstanceBrowseRepository;
	}
	
	@Autowired
	public void setCouponInstanceBrowseRepository(CouponInstanceBrowseRepository couponInstanceBrowseRepository) {
		this.couponInstanceBrowseRepository = couponInstanceBrowseRepository;
	}
	
}

