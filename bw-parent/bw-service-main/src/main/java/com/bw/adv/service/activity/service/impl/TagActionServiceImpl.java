package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.ActivityTagAction;
import com.bw.adv.module.activity.repository.ActivityTagActionRepository;
import com.bw.adv.service.activity.service.TagActionService;

@Service
public class TagActionServiceImpl extends BaseServiceImpl<ActivityTagAction> implements TagActionService{
	
	
	private ActivityTagActionRepository activityTagActionRepository;
	

	@Override
	public BaseRepository<ActivityTagAction, ? extends BaseMapper<ActivityTagAction>> getBaseRepository() {
		return activityTagActionRepository;
	}

	@Autowired
	public void setActivityTagActionRepository(ActivityTagActionRepository activityTagActionRepository) {
		this.activityTagActionRepository = activityTagActionRepository;
	}

	
	@Override
	public void scanTagAction() {
		List<ActivityTagAction> tagActionList = activityTagActionRepository.findByIsNotCountWithOrderByHappenTime();
	}
	
}
