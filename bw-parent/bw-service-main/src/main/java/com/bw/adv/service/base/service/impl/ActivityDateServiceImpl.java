/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityDateServiceImpl.java
 * Package Name:com.sage.scrm.service.base.service.impl
 * Date:2015年8月27日下午7:13:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ActivityDateMapper;
import com.bw.adv.module.base.model.ActivityDate;
import com.bw.adv.module.base.repository.ActivityDateRepository;
import com.bw.adv.service.base.service.ActivityDateService;

/**
 * ClassName:ActivityDateServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月27日 下午7:13:54 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class ActivityDateServiceImpl extends BaseServiceImpl<ActivityDate> implements ActivityDateService {

	private ActivityDateRepository activityDateRepository;
	
	@Override
	public BaseRepository<ActivityDate, ? extends BaseMapper<ActivityDate>> getBaseRepository() {
		
		return activityDateRepository;
	}

	@Override
	public List<ActivityDate> queryActivityDateByIsActive(
			Page<ActivityDate> page) {
		
		return activityDateRepository.findByIsActive("Y",page);
	}
	
	@Override
	public void deleteActivityDateList(List<Long> activityDateIds) {
		
		activityDateRepository.updateIsActiveList("N",activityDateIds);
		
	}
	
	/**
	 * TODO 查询所有启用日期列表
	 * @see com.bw.adv.service.base.service.ActivityDateService#queryAll()
	 */
	@Override
	public List<ActivityDate> queryAllActiveDate() {
		return activityDateRepository.findByIsActive("Y");
	}
	
	
	@Autowired
	public void setActivityDateRepository(ActivityDateRepository activityDateRepository) {
		this.activityDateRepository = activityDateRepository;
	}

}

