package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.QuestionCount;
import com.bw.adv.module.activity.model.Questionnaire;
import com.bw.adv.module.activity.model.QuestionnaireInstance;
import com.bw.adv.module.activity.model.QuestionnaireTemplate;
import com.bw.adv.module.activity.model.QuestionnaireTemplateInfo;

public interface QuestionService {
	
	/**
	 * 新建问卷
	 * @param questionnaire
	 */
	public int insertQuestion(Questionnaire questionnaire);
	
	/**
	 * 查询问卷
	 * @param questionnaireId
	 * @return
	 */
	public Questionnaire queryNaire(Long questionnaireId);

	/**
	 * 查询问卷题目
	 * @param questionnaireId
	 * @return
	 */
	public List<QuestionnaireTemplate> queryQuestionTemplate(Long questionnaireId,Page<QuestionnaireTemplate> templatePage);
	
	/**
	 * 查询问卷题目（不带分页）
	 * @param questionnaireId
	 * @return
	 */
	public List<QuestionnaireTemplate> queryQuestionTemplateNoPage(Long questionnaireId);
	
	/**
	 * 更新问卷
	 * @param questionnaire
	 */
	public void updateQuestion(Questionnaire questionnaire);
	
	/**
	 * 删除问卷
	 * @param questionnaireId
	 */
	public void deleteQuestion(Long questionnaireId);
	
	/**
     * 查询所有问卷
     * @return
     */
    public List<Questionnaire> selectAllQuestion();
    
    /**
     * 查询所有问卷包含题目数量
     * @return
     */
    public List<Questionnaire> selectAllQuestionWithTemplateCount(Page<Questionnaire> templatePage);
    
    /**
     * 保存题目
     * @param template
     * @return
     */
    public int saveTemplate(QuestionnaireTemplate template);
    
    /**
     * 更新题目
     * @param template
     */
    public void updateTemplate(QuestionnaireTemplate template);
    
    /**
     * 保存题目选项
     * @param info
     * @return
     */
    public int insertTemplateInfo(List<QuestionnaireTemplateInfo> info); 
    
    /**
     * 删除题目
     * @param questionnaireTemplateId
     */
    public void deleteTemplate(Long questionnaireTemplateId);
    
    /**
     * 查询问卷题目
     * @param templateId
     * @return
     */
    public QuestionnaireTemplate queryTemplateById(Long templateId);
    
    /**
     * 查询题目选项
     * @param templateId
     * @return
     */
    public List<QuestionnaireTemplateInfo> queryTemplateInfoByTemplateId(Long templateId);
    
    /**
     * 删除题目选项
     * @param templateId
     */
    public void deleteTemplateInfoByTemplateId(Long templateId);
    
    /**
     * 查询选择题统计结果
     * @param templateId
     * @return
     */
    public List<QuestionCount> selectResultCountForSelect(Long questionnaireId);
    
    /**
     * 查询打分题统计结果
     * @param templateId
     * @return
     */
    public List<QuestionCount> selectResultCountForMark(Long questionnaireId);
    
    /**
     * 查询问答题统计结果
     * @param templateId
     * @return
     */
    public List<QuestionCount> selectResultCountForQa(Long questionnaireId);
    
    /**
     * 查询文件一共回答的人数
     * @param templateId
     * @return
     */
	public Long selectCountInfoForMember(Long questionnaireId);
	
	/**
	 * 查询启用问卷
	 * findActiveQuestion:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public Questionnaire findActiveQuestion();
	
	/**
	 * 查询启用问卷数量
	 * queryActiveQuestionAmount:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	public Long queryActiveQuestionAmount();
	
	/**
	 * 保存问卷结果
	 * saveQuestionInstanceList:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param instanceList
	 */
	public void saveQuestionInstanceList(List<QuestionnaireInstance> instanceList);
	
}
