/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberService1.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2015年8月10日下午3:39:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.message.model.MemberMessageItemBrowse;


/**
 * ClassName: MemberMessageItemBrowseService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年3月4日 下午2:15:07 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface MemberMessageItemBrowseService extends BaseService<MemberMessageItemBrowse>{

}

