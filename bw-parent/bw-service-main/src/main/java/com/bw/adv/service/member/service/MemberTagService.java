/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberService1.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2015年8月10日下午3:39:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import java.util.List;

import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.model.MemberTagCondition;
import com.bw.adv.module.member.tag.search.MemberTagSearch;
import com.bw.adv.module.order.model.dto.OrderInfo;


/**
 * ClassName:MemberTagService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月10日 下午3:39:09 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberTagService extends BaseService<MemberTag>{
	/**
	 * 
	 * queryCountSearchAuto:根据条件查询标签数量 <br/>
	 * Date: 2015年8月13日 上午11:06:47 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	Long queryCountSearchAuto(MemberTagSearch search);
	
	/**
	 * 
	 * queryListSearchAuto:根据条件查询标签列表 <br/>
	 * Date: 2015年8月13日 上午11:07:15 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	List<MemberTag> queryListSearchAuto(MemberTagSearch search);
	
	/**
	 * 
	 * queryEntityPkAuto:根据id查询标签详情 <br/>
	 * Date: 2015年8月13日 上午11:08:29 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @return
	 */
	MemberTag queryEntityPkAuto(Long memberTagId);
	
	/**
	 * 
	 * insertMemberTag:新增会员标签 <br/>
	 * Date: 2015年8月13日 上午11:10:00 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTag
	 * @return
	 */
	int insertMemberTag(MemberTag memberTag);
	
	/**
	 * 
	 * updateMemberTag:修改会员标签 <br/>
	 * Date: 2015年8月13日 上午11:10:46 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTag
	 * @return
	 */
	int updateMemberTag(MemberTag memberTag);
	
	/**
	 * 
	 * deleteteMemberTagById:根据id删除标签 <br/>
	 * Date: 2015年8月13日 上午11:11:48 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @return
	 */
	int deleteteMemberTagById(Long memberTagId);
	
	/**
	 * 
	 * insertBatchMemberTag:会员标签批量添加会员，之前的会员继续存在，重复性验证 <br/>
	 * Date: 2015年8月13日 上午11:51:57 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 */
	Long insertBatchMemberTagItem(Long memberTagId,String memberIds); 
	
	/**
	 * 
	 * excuteMemberTagBySql:手动执行标签sql,适用存在sql的标签 <br/>
	 * Date: 2015年8月13日 上午11:53:53 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @return 执行会员数量
	 */
	Long excuteMemberTagBySql(Long memberTagId);

	/**
	 * 
	 * queryMemberByTagId:查询标签下会员列表 <br/>
	 * Date: 2015年8月13日 下午4:59:17 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param tagId
	 * @return
	 */
	List<MemberExp> queryMemberListOfTag(MemberSearch search);
	
	/**
	 * 
	 * queryCountMemberListOfTag:根据条件查询标签先会员数量 <br/>
	 * Date: 2015年8月17日 上午11:11:48 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	Long queryCountMemberListOfTag(MemberSearch search);

	/**
	 * 
	 * deleteMemberTagItem:根据标签id和会员id删除标签下会员 <br/>
	 * Date: 2015年8月31日 下午5:31:30 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @param memberId
	 */
	void deleteMemberTagItem(Long memberTagId, Long memberId);

	/**
	 * 
	 * saveAllMemberToTag:设置会员标签成员为全部会员 <br/>
	 * Date: 2015年9月1日 下午5:26:36 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTag
	 */
	void saveAllMemberToTag(MemberTag memberTag);

	/**
	 * 
	 * excuteMemberTagJob:会员标签定时执行 <br/>
	 * Date: 2015年9月29日 下午3:15:42 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param tag
	 */
	void excuteMemberTagJob(MemberTag tag);
	
	/**
	 * 删除标签会员关系
	 * @param memberTagId
	 */
	void deleteMemberTagItemByTagId(Long memberTagId);
	
	/**
	 * 保存标签对应条件
	 * @param conditionList
	 */
	void saveTagCondition(List<MemberTagCondition> conditionList);
	
	/**
	 * 保存标签对应条件
	 * @param conditionList
	 */
	void updateTagCondition(List<MemberTagCondition> conditionList,Long memberTagId);
	
	/**
	 * 删除标签条件
	 */
	void deleteConditionByTagId(Long memberTagId);
	
	/**
	 * 查询有自动更新节点的标签
	 * @param type
	 * @param node
	 * @return
	 */
	List<MemberTag> findTagByTypeAndNode(String type,String node);
	
	/**
	 * 查询标签对应的条件
	 * @param tagId
	 * @return
	 */
	List<MemberTagCondition> findConditionByTagId(Long tagId);
	
	/**
	 * 执行手动标签
	 * @param memberTagId
	 */
	void setTagManel(Long memberTagId);
	
	/**
	 * 构建切面标签Action(新建会员)
	 */
	void structureTagAopNewMember(MemberDto memberDto);
	
	/**
	 * 构建切面标签Action(更新会员)
	 */
	void structureTagAopUpdateMember(MemberDto memberDto);
	
	/**
	 * 构建切面标签Action(同步订单)
	 */
	void structureTagAopHandleOrder(MemberExp memberExp,OrderInfo orderInfo);
	
	/**
	 * 根据标签状态查询状态列表
	 * @param statusId
	 * @return
	 * @author cuicd
	 */
	List<MemberTag> findMemberTagByStatusId(Long statusId);
	
}

