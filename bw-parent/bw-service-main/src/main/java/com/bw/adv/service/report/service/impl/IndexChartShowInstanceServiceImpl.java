package com.bw.adv.service.report.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.report.model.IndexChartShowInstance;
import com.bw.adv.module.report.repository.IndexChartShowInstanceRepository;
import com.bw.adv.service.report.service.IndexChartShowInstanceService;

@Service
public class IndexChartShowInstanceServiceImpl extends BaseServiceImpl<IndexChartShowInstance> implements IndexChartShowInstanceService{

	private IndexChartShowInstanceRepository indexChartShowInstanceRepository;
	
	@Override
	public BaseRepository<IndexChartShowInstance, ? extends BaseMapper<IndexChartShowInstance>> getBaseRepository() {
		return indexChartShowInstanceRepository;
	}

	@Autowired
	public void setIndexChartShowInstanceRepository(IndexChartShowInstanceRepository indexChartShowInstanceRepository) {
		this.indexChartShowInstanceRepository = indexChartShowInstanceRepository;
	}

	@Override
	public void updateIndexCharts(List<IndexChartShowInstance> instanceList,Long userId) {
		this.indexChartShowInstanceRepository.deleteInstanceByUserId(userId);
		this.indexChartShowInstanceRepository.saveList(instanceList);
	}

	@Override
	public List<IndexChartShowInstance> findUserIndexByUserId(Long userId) {
		return this.indexChartShowInstanceRepository.selectUserIndexByUserId(userId);
	}
	
}
