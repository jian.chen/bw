/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:OrderRegionSummaryDayJobBean.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2016年1月14日下午4:00:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.order.service.OrderRegionSummaryDayService;

/**
 * ClassName:OrderRegionSummaryDayJobBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 下午4:00:33 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class OrderRegionSummaryDayJobBean extends BaseJob {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderRegionSummaryDayJobBean.class);
	
	private OrderRegionSummaryDayService orderRegionSummaryDayService;

	@Override
	protected void excute() throws Exception {
		
		init();
		logger.info("订单报表任务开始");
		String dateStr = DateUtils.addDays(-1);//昨天日期
		orderRegionSummaryDayService.callOrderRegionSummaryDay(dateStr);
		logger.info(dateStr+"|订单报表任务结束");
		
	}
	
	public void init(){
		this.orderRegionSummaryDayService = this.getApplicationContext().getBean(OrderRegionSummaryDayService.class);
	}

	@Override
	protected String getLockCode() {
		
		return LockCodeConstant.ORDER_REGION_SUMMARY_DAY_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		
		return true;
	}

	@Autowired
	public void setOrderRegionSummaryDayService(
			OrderRegionSummaryDayService orderRegionSummaryDayService) {
		this.orderRegionSummaryDayService = orderRegionSummaryDayService;
	}
	
	

}

