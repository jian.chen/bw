package com.bw.adv.service.order.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.constant.GiftConstant;
import com.bw.adv.module.common.constant.LotteryConstant;
import com.bw.adv.module.common.constant.UnitConstant;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.member.model.MemberGift;
import com.bw.adv.module.member.model.exp.MemberGiftExt;
import com.bw.adv.module.order.repository.MemberGiftRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.order.service.MemberGiftService;

@Service
public class MemberGiftServiceImpl extends BaseServiceImpl<MemberGift> implements MemberGiftService {

	@Autowired
	private MemberGiftRepository memberGiftRepository;
	@Override
	public BaseRepository<MemberGift, ? extends BaseMapper<MemberGift>> getBaseRepository() {
		return memberGiftRepository;
	}
	
	@Override
	public int saveOrUpdateMemberGiftRecord(CouponInstance couponInstance,Long memberId, Coupon coupon) {
		//如果领取成功，增加一次记录
		MemberGift memberGift=new MemberGift();
		if(couponInstance==null){
			MemberGift memberGiftRecord = countNotReceiveMemberGiftRecord(memberId, coupon.getCouponId());
			if(memberGiftRecord==null){
				memberGift.setMemberId(memberId);
				memberGift.setGiftType("2");//1-实物奖品 2，兑换券
				memberGift.setGiftUnit(UnitConstant.ZHANG.getName());
				memberGift.setCouponId(coupon.getCouponId());
				memberGift.setGiftName(coupon.getCouponName());
				memberGift.setChannelId(ChannelConstant.GET_COUPON_EXCHANGE.getId());//徽章兑换
				memberGift.setGiftStatus(GiftConstant.GIFT_STATUS_NOT_RECEIVE.getId()); //未领取
				memberGift.setCreateTime(DateUtils.getCurrentDateOfDb());
				return memberGiftRepository.save(memberGiftRecord);
			}else{
				memberGiftRecord.setUpdateTime(DateUtils.getCurrentDateOfDb());
				return memberGiftRepository.updateByPkSelective(memberGiftRecord);
			}
		}else{
			memberGift.setMemberId(memberId);
			memberGift.setGiftType("2");//1-实物奖品 2，兑换券
			memberGift.setGiftUnit(UnitConstant.ZHANG.getName());
			memberGift.setCouponId(coupon.getCouponId());
			memberGift.setGiftName(coupon.getCouponName());
			memberGift.setGiftCode(LotteryConstant.LOTTERY_TYPE_YOUKU_MONTH_MEMBER.getCode());//优酷
			memberGift.setChannelId(ChannelConstant.GET_COUPON_EXCHANGE.getId());//徽章兑换
			memberGift.setCouponInstanceId(couponInstance.getCouponInstanceId());
			memberGift.setCreateTime(DateUtils.getCurrentDateOfDb());
			memberGift.setGiftStatus(GiftConstant.GIFT_STATUS_YES_RECEIVE.getId()); //已领取
			return memberGiftRepository.save(memberGift);
		}
	}
	@Override
	public MemberGift countNotReceiveMemberGiftRecord(Long memberId, Long couponId){
		
		return memberGiftRepository.getCountNotReceiveMemberGiftRecord(memberId,couponId);
	}

	@Override
	public List<MemberGiftExt> findReceiveMemberGiftRecord(Long memberId) {
		return memberGiftRepository.queryReceiveMemberGiftRecord(memberId);
	}

	@Override
	public List<MemberGift> findReceiveMemberGiftByGiftCode(String giftCode,Long memberId) {
		return memberGiftRepository.queryReceiveMemberGiftByGiftCode(giftCode, memberId);
	}
}