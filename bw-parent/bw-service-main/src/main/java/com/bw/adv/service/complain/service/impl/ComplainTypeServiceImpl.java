/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ComplainTypeServiceImpl.java
 * Package Name:com.sage.scrm.service.complain.service.impl
 * Date:2015年12月29日下午5:07:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.complain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.member.complain.model.ComplainType;
import com.bw.adv.module.member.complain.repository.ComplainTypeRepository;
import com.bw.adv.service.complain.service.ComplainTypeService;

/**
 * ClassName:ComplainTypeServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月29日 下午5:07:05 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class ComplainTypeServiceImpl extends BaseServiceImpl<ComplainType> implements ComplainTypeService{
	private ComplainTypeRepository complainTypeRepository;
	
	@Override
	public BaseRepository<ComplainType, ? extends BaseMapper<ComplainType>> getBaseRepository() {
		return complainTypeRepository;
	}
	
	@Override
	public List<ComplainType> queryAllComplainType(){
		return complainTypeRepository.findAllComplainType();
	}
	@Autowired
	public void setComplainTypeRepository(ComplainTypeRepository complainTypeRepository) {
		this.complainTypeRepository = complainTypeRepository;
	}

}

