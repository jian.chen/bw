/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFanLabelService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年9月3日下午3:32:08
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.model.WechatFanLabel;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.exp.WechatFanLabelExp;

/**
 * ClassName:WechatFanLabelService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月3日 下午3:32:08 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFanLabelService extends BaseService<WechatFanLabel> {
	
	/**
	 * queryWechatFanLabelExp:(查询粉丝标签). <br/>
	 * Date: 2015年9月3日 下午3:58:44 <br/>
	 * scrmVersion 1.0
	 * @author june	
	 * @version jdk1.7
	 * @return
	 */
	public List<WechatFanLabelExp> queryWechatFanLabelExp(Long wechatFansLabelId,Long wechatFansId,Page<WechatFanLabelExp> page);
	
	/**
	 * handleData:(组装粉丝标签). <br/>
	 * Date: 2015年9月3日 下午6:26:42 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFanLabelExpList
	 * @param labelId 
	 * @return
	 */
	public List<WechatFanLabelExp> handleData(List<WechatFanLabelExp> wechatFanLabelExpList, String labelId);
	
	/**
	 * 
	 * queryWechatFanLabelList:(根据粉丝id查询对应的标签). <br/>
	 * Date: 2015年9月3日 下午6:30:27 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFansId
	 * @return
	 */
	public List<WechatFanLabelExp> queryWechatFanLabelList(Long wechatFansId);
	
	/**
	 * saveFanLabelsRelative:(保存粉丝标签关系). <br/>
	 * Date: 2015年9月4日 下午9:57:15 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFansId
	 * @param addTagIdArr
	 */
	public void saveFanLabelsRelative(String wechatFansId, List<String> labelIds);
	
	/**
	 * 
	 * batchAddLabelToFans:(批量添加label). <br/>
	 * Date: 2015年9月5日 下午3:38:43 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param tagId
	 * @param wechatFansList
	 */
	public void batchAddLabelToFans(String tagId, List<String> wechatFansList);
	
	/**
	 * transToLabelExpList:(转化为wechatFansLabelExp). <br/>
	 * Date: 2015年9月5日 下午4:35:40 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFans
	 * @return
	 */
	public List<WechatFanLabelExp> transToLabelExpList(List<WechatFans> wechatFans);
	
	
	
	
}	

