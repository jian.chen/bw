/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansLabelService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月17日上午10:48:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.wechat.model.WechatFansLabel;

/**
 * ClassName:WechatFansLabelService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 上午10:48:42 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansLabelService extends BaseService<WechatFansLabel> {
	
	/**
	 * queryWechatFansLabels:(查询所有粉丝标签). <br/>
	 * Date: 2015年8月17日 下午1:51:36 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @return
	 */
	List<WechatFansLabel> queryWechatFansLabels();
	
	/**
	 * 
	 * saveFansLabel:(保存粉丝标签). <br/>
	 * Date: 2015年9月4日 下午2:12:31 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param labelName
	 * @param sysyUser 
	 */
	void saveFansLabel(String labelName, SysUser sysyUser);
	
	/**
	 * queryWechatFansLabel:(根据标签 名称查询标签信息列表). <br/>
	 * Date: 2015年9月4日 下午5:30:05 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param labelName
	 * @return
	 */
	List<WechatFansLabel> queryWechatFansLabelByLabelName(String labelName);
}

