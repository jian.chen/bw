/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CouponSingleSummaryJobBeen.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2015年12月17日下午6:40:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.report.service.CouponSingleSummaryDayService;
import com.bw.adv.service.report.service.MemberChannelSummaryDayService;

/**
 * ClassName:CouponSingleSummaryJobBeen <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 下午6:40:51 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class CouponSingleSummaryJobBeen extends BaseJob {
	
	private static final Logger logger = LoggerFactory.getLogger(CouponSingleSummaryJobBeen.class);
	
	private CouponSingleSummaryDayService CouponSingleSummaryDayService;

	@Override
	protected void excute() throws Exception {
		
		init();
		logger.info("优惠券日统计报表任务开始");
		String dateStr = DateUtils.addDays(-1);//昨天天日期
		CouponSingleSummaryDayService.callCouponSingleSummary(dateStr);
		logger.info(dateStr+"|优惠券日统计报表任务结束");
	}
	
	public void init(){
		this.CouponSingleSummaryDayService = this.getApplicationContext().getBean(CouponSingleSummaryDayService.class);
	}

	@Override
	protected String getLockCode() {
		
		return LockCodeConstant.COUPON_SINGLE_SUMMARY_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		
		return true;
	}

	@Autowired
	public void setCouponSingleSummaryDayService(
			CouponSingleSummaryDayService couponSingleSummaryDayService) {
		CouponSingleSummaryDayService = couponSingleSummaryDayService;
	}
	
	

}

