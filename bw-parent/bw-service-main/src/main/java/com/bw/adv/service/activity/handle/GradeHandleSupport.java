package com.bw.adv.service.activity.handle;


import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.module.activity.constant.ActivityConstant;
import com.bw.adv.module.activity.enums.IsCountEnum;
import com.bw.adv.module.activity.model.ActivityAction;
import com.bw.adv.module.activity.repository.ActivityActionRepository;
import com.bw.adv.module.component.rule.constant.ConditionsConstant;
import com.bw.adv.module.member.constant.GradeFactorsConstant;
import com.bw.adv.module.member.enums.GradeChangeTypeEnums;
import com.bw.adv.module.member.exception.GradeException;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.GradeConf;
import com.bw.adv.module.member.model.GradeConvertRecord;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.repository.GradeConvertRecordRepository;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;

import net.sf.json.JSONObject;


/**
 * ClassName: ActivityActionHandleSupport <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-29 上午11:21:20 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public abstract class GradeHandleSupport implements GradeHandleUpService{
	
	private static final Logger logger =LoggerFactory.getLogger(GradeHandleSupport.class);
	
	protected MemberRepository memberRepository;
	protected GradeConvertRecordRepository gradeConvertRecordRepository;
	protected ActivityActionRepository activityActionRepository;
	protected WechatMsgQueueHRepository wechatMsgQueueHRepository;

	/**
	 * operateActivityActionGrade:(操作等级升级). <br/>
	 * Date: 2015-8-29 上午11:22:46 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityAction
	 */
	@Override
	@Transactional
	public final void operateGradeUpBusiness(Long memberId) {
		MemberExp memberExp = null;
		Member memberGrade = null;
		GradeConf gradeConf = GradeInit.GRADE_CONF;
		Grade currentGrade = null;//用户当前等级
		Grade changeGrade = null;//用户当前等级
		gradeConf = GradeInit.GRADE_CONF;
		ActivityAction activityAction = null;
		String currentTime = null;
		JSONObject jsonParams = null;
		GradeConvertRecord gradeConvertRecord  = null;//等级转换记录
		BigDecimal gradeFactorsValue = null;
		try {
			//如果是手动升级
			if(gradeConf.getChangeType().equals(GradeChangeTypeEnums.HANDLE.getId())){
				return;
			}
			if(gradeConf.getIsActive().equals("N")){
				logger.info("等级功能已停用");
				return;
			}
			
			memberExp = memberRepository.findMemberExpByMemberId(memberId);
			
			currentGrade = GradeInit.getGradeByPk(memberExp.getGradeId());
			if(currentGrade == null){
				return;
			}
			changeGrade = new Grade();
			BeanUtils.copy(currentGrade, changeGrade);
			gradeFactorsValue = upGrade(memberExp,currentGrade,changeGrade,gradeFactorsValue);
			
			//如果跟原等级不一样
			if(changeGrade != null && !currentGrade.getGradeId().equals(changeGrade.getGradeId())){
				//如果新等级大于原等级
				if(changeGrade.getSeq() >  currentGrade.getSeq()){
					System.out.println("***************升级结果**********"+changeGrade.getSeq());
					currentTime = DateUtils.getCurrentTimeOfDb();
					//修改会员等级
					memberGrade = new Member();
					memberGrade.setMemberId(memberExp.getMemberId());
					memberGrade.setGradeId(changeGrade.getGradeId());
					memberRepository.updateByPkSelective(memberGrade);
					
					gradeConvertRecord = new GradeConvertRecord();
					gradeConvertRecord.setGcDate(DateUtils.getCurrentDateOfDb());
					gradeConvertRecord.setGradeFactorsId(gradeConf.getGradeFactorsId());
					gradeConvertRecord.setSrcGradeId(currentGrade.getGradeId());
					gradeConvertRecord.setTargetGradeId(changeGrade.getGradeId());
					gradeConvertRecord.setUpDownFlag("U");//升降级标示U:升级，D:降级
					gradeConvertRecord.setGradeFactorsValue(gradeFactorsValue);
					gradeConvertRecord.setMemberId(memberExp.getMemberId());
					if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.POINTS_PERIOD.getId())){
						gradeConvertRecord.setGradeFactorsDesc(GradeFactorsConstant.POINTS_PERIOD.getName()+"达到"+gradeFactorsValue+",升级"+changeGrade.getGradeName());
					}
					if(gradeConf.getGradeFactorsId().equals(GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getId())){
						gradeConvertRecord.setGradeFactorsDesc(GradeFactorsConstant.ORDER_AMOUNT_PERIOD.getName()+"达到"+gradeFactorsValue+",升级"+changeGrade.getGradeName());
					}
					gradeConvertRecord.setCreateTime(currentTime);
					gradeConvertRecordRepository.save(gradeConvertRecord);
					
					
					jsonParams = new JSONObject();
					jsonParams.put(ConditionsConstant.MEMBER_GRADE.getCode(), changeGrade.getSeq());
					jsonParams.put(ConditionsConstant.MEMBER_GROUP.getCode(),StringUtils.isBlank(memberExp.getMemberGroupIds())?"":memberExp.getMemberGroupIds() );
					jsonParams.put(ConditionsConstant.MEMBER_TAG.getCode(), StringUtils.isBlank(memberExp.getMemberTagIds())?"":memberExp.getMemberTagIds());
					
					//添加升级活动动作
					activityAction = new ActivityAction();
					activityAction.setActivityCode(ActivityConstant.ACTIVIY_MEMBER_GRADE_UP.getCode());
					activityAction.setMemberId(memberId);
					activityAction.setHappenTime(currentTime);
					activityAction.setCreateTime(currentTime);
					activityAction.setIsCount(IsCountEnum.IS_COUNT_N.getId());
					activityAction.setRuleOptParams(jsonParams.toString());
					activityActionRepository.save(activityAction);
					
					//发送微信模版消息
					sendWechatMsg(memberExp, gradeConvertRecord);
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("等级升级异常:"+e.getMessage());
			throw new GradeException("等级升级异常:"+e.getMessage());
		}
	}
	
	
	
	
	/**
	 * 
	 * sendWechatMsg:(添加发送消息). <br/>
	 * Date: 2016年7月11日 下午6:18:11 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 */
	private void sendWechatMsg(MemberExp memberExp,GradeConvertRecord record){
		JSONObject json = null;
		json = new JSONObject();
		WechatMsgQueueH msgQueueH = new WechatMsgQueueH();
		String changeDate = DateUtils.dateStrToNewFormat(record.getGcDate(), DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
		Grade gradeSrc = GradeInit.getGradeByPk(record.getSrcGradeId());
		Grade gradeTarget = GradeInit.getGradeByPk(record.getTargetGradeId());
		json.put("changeDate", changeDate);
		json.put("gradebefor", gradeSrc.getGradeName());
		json.put("gradeafter", gradeTarget.getGradeName());
		json.put("memberName", memberExp.getMemberName());
		
		msgQueueH.setBusinessType(WechatMsgBusinessTypeEnum.GRADE_UPDATE.getId());
		msgQueueH.setCreateTime(DateUtils.getCurrentTimeOfDb());
		msgQueueH.setMemberId(memberExp.getMemberId());
		msgQueueH.setMsgContent(json.toString());
		msgQueueH.setIsSend("N");
		
		wechatMsgQueueHRepository.save(msgQueueH);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * getBeginDate:(得到周期时间段的开始时间). <br/>
	 * Date: 2015-9-17 下午10:20:38 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param yearFrequency
	 * @param calcTime
	 * @return
	 */
	protected static final String getBeginDate(int yearFrequency,String calcTime){
		String registerDate = null;
		int registerYear = 0;
		String registerDay = null;
		
		String currentDate = null;
		int currentYear = 0;
		String currentDay = null;
		int yearDiff = 0;
		
		String beginDate = null;
		
		registerDate = calcTime.substring(0,8);
		registerYear = Integer.parseInt(registerDate.substring(0,4)); 
		registerDay = registerDate.substring(4); 
		
		currentDate = DateUtils.getCurrentDateOfDb();
		currentYear = Integer.parseInt(currentDate.substring(0,4)); 
		currentDay = currentDate.substring(4);
		
		
		yearDiff = (currentYear - registerYear) % yearFrequency;
		
		//如果是周期年
		if(yearDiff == 0 ){
			//如果计算日大于当前日期，则本还在本周期年中
			if(registerDay.compareTo(currentDay) > 0){
				beginDate = (currentYear - yearFrequency) + registerDay;
			}else{//否则开始新的周期年
				beginDate = currentYear + registerDay;
			}
		}else{//在周期年内
			beginDate = (currentYear - yearDiff) + registerDay;
		}
		
		return beginDate;
	}

	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}

	@Autowired
	public void setGradeConvertRecordRepository(GradeConvertRecordRepository gradeConvertRecordRepository) {
		this.gradeConvertRecordRepository = gradeConvertRecordRepository;
	}

	@Autowired
	public void setActivityActionRepository(ActivityActionRepository activityActionRepository) {
		this.activityActionRepository = activityActionRepository;
	}

	@Autowired
	public void setWechatMsgQueueHRepository(WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}
	
	
}
	












