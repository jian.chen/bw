package com.bw.adv.service.coupon.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.coupon.model.CouponType;

/**
 * ClassName: CouponTypeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-12-15 下午3:45:13 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface CouponTypeService extends BaseService<CouponType> {
	
	
	/**
	 * renovateCouponType:(更新优惠券类型信息).<br/>
	 * Date: 2015-12-15 下午6:18:59 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	void renovateCouponType();
	
}
