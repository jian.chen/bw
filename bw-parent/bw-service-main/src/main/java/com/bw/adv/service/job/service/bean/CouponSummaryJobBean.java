package com.bw.adv.service.job.service.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.coupon.service.CouponService;

public class CouponSummaryJobBean extends BaseJob {

	private static final Logger logger = LoggerFactory.getLogger(CouponSummaryJobBean.class);
	private CouponService couponService;
	
	@Override
	protected void excute() throws Exception {
		init();
		logger.info("优惠券报表任务开始");
		String dateStr = DateUtils.addDays(-1);//今天日期
		String weekStr = DateUtils.addDays(-8);//七天前的日期
		String monthStr = DateUtils.getLastDayOfMonth(DateUtils.addMonths(-1));//上个月最后一天的日期
		couponService.callCouponSummaryDay(dateStr, weekStr, monthStr);
		logger.info("优惠券报表任务结束");
		
	}
	public void init(){
		this.couponService = this.getApplicationContext().getBean(CouponService.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.COUPON_SUMMARY_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

}
