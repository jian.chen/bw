/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:WechatNews.java
 * Package Name:com.sage.scrm.bk.wechat.model
 * Date:2015年11月11日下午5:57:17
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.msg.model;

import java.util.List;
import java.util.Map;

/**
 * ClassName:WechatNews <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月11日 下午5:57:17 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class WechatNews {
	private String touser;
	private String msgtype;
	
    private Map<String, List<WechatNewsArticles>> news;

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public Map<String, List<WechatNewsArticles>> getNews() {
		return news;
	}

	public void setNews(Map<String, List<WechatNewsArticles>> news) {
		this.news = news;
	}
    
}

