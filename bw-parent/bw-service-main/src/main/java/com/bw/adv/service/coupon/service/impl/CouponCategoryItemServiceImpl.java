/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CouponCategoryItemServiceImpl.java
 * Package Name:com.sage.scrm.service.coupon.service.impl
 * Date:2015年12月28日下午2:55:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.coupon.model.CouponCategoryItem;
import com.bw.adv.module.coupon.repository.CouponCategoryItemRepository;
import com.bw.adv.service.coupon.service.CouponCategoryItemService;

/**
 * ClassName:CouponCategoryItemServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月28日 下午2:55:29 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class CouponCategoryItemServiceImpl extends BaseServiceImpl<CouponCategoryItem> implements CouponCategoryItemService{
	
	private CouponCategoryItemRepository couponCategoryItemRepository;
	
	@Override
	public BaseRepository<CouponCategoryItem, ? extends BaseMapper<CouponCategoryItem>> getBaseRepository() {
		return couponCategoryItemRepository;
	}
	
	@Autowired
	public void setCouponCategoryItemRepository(CouponCategoryItemRepository couponCategoryItemRepository) {
		this.couponCategoryItemRepository = couponCategoryItemRepository;
	}

}

