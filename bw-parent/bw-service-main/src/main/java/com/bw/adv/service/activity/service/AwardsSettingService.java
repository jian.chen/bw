/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:AwardsSettingService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2015年11月23日下午2:02:03
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.AwardsSetting;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;

/**
 * ClassName:AwardsSettingService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:02:03 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface AwardsSettingService extends BaseService<AwardsSetting> {
	
	/**
	 * queryAwardsSettingsByActivityId:(根据活动id查询出奖项设置). <br/>
	 * Date: 2015年11月24日 下午4:48:42 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 */
	List<AwardsSetting> queryAwardsSettingsByActivityId(Long activityId);
	
	/**
	 * 
	 * queryAwardsSettingExpByActivityId:(根据活动id查询奖项的扩展类). <br/>
	 * Date: 2015年11月26日 下午9:10:05 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param activityId
	 * @return
	 */
	List<AwardsSettingExp> queryAwardsSettingExpByActivityId(Long activityId);

	AwardsSettingExp queryAwardsSettingExpByAwardsId(Long awardsId);
	
}

