/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SceneGameInstanceServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2016年1月15日下午2:27:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.AwardPrizeRel;
import com.bw.adv.module.activity.model.Awards;
import com.bw.adv.module.activity.model.AwardsSetting;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.activity.model.PrizeInstance;
import com.bw.adv.module.activity.model.SceneGameInstance;
import com.bw.adv.module.activity.model.exp.SceneGameInstanceExp;
import com.bw.adv.module.activity.repository.AwardPrizeRelRepository;
import com.bw.adv.module.activity.repository.AwardsRepository;
import com.bw.adv.module.activity.repository.AwardsSettingRepository;
import com.bw.adv.module.activity.repository.PrizeInstanceRepository;
import com.bw.adv.module.activity.repository.PrizeRepository;
import com.bw.adv.module.activity.repository.SceneGameInstanceRepository;
import com.bw.adv.module.base.enums.EnumDateCycle;
import com.bw.adv.module.base.exception.ActivityException;
import com.bw.adv.module.base.exception.ErrorData;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.member.search.SceneGameInstanceSearch;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.StringUtil;
import com.bw.adv.service.activity.service.SceneGameInstanceService;

/**
 * ClassName:SceneGameInstanceServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午2:27:33 <br/>
 * scrmVersion 1.0
 * 
 * @author Tab.Wang
 * @version jdk1.7
 * @see
 */
@Service
public class SceneGameInstanceServiceImpl extends BaseServiceImpl<SceneGameInstance> implements SceneGameInstanceService {

	@Autowired
	private SceneGameInstanceRepository sceneGameInstanceRepository;

	@Autowired
	private AwardsRepository awardsRepository;

	@Autowired
	private AwardsSettingRepository awardsSettingRepository;

	@Autowired
	private PrizeRepository prizeRepository;

	@Autowired
	private AwardPrizeRelRepository awardPrizeRelRepository;

	@Autowired
	private PrizeInstanceRepository prizeInstanceRepository;

	/*
	 * private static String couponIssueNumber =
	 * PropertiesConfInit.getString("webConfig", "crm_coupon_issue_num");
	 */
	private static String couponIssueNumber = "500";

	@Override
	public BaseRepository<SceneGameInstance, ? extends BaseMapper<SceneGameInstance>> getBaseRepository() {
		return sceneGameInstanceRepository;
	}

	@Override
	@Transactional
	public void saveSceneGameInstance(JSONObject jsonObject, SysUser sysUser) {

		List<PrizeInstance> prizeInstanceList = new ArrayList<PrizeInstance>();
		Awards award = null;
		AwardsSetting awardsSetting = null;
		Prize prize = null;
		AwardPrizeRel awardPrizeRel = null;
		PrizeInstance prizeInstance = null;
		List<Map<String, Object>> listMap = null;
		SceneGameInstance sceneGameInstance = null;
		List<SceneGameInstance> instanceList = null;
		
		
		instanceList = sceneGameInstanceRepository.findBySceneGameId(jsonObject.getLong("sceneGameId"));
		
		// 1.保存场景游戏实例============================================
		sceneGameInstance = new SceneGameInstance();
		// 对象值填充
		sceneGameInstance.setInstanceName(jsonObject.getString("instanceName"));// 活动实例名称
		sceneGameInstance.setSceneGameId(jsonObject.getLong("sceneGameId"));// 获取游戏Id
		sceneGameInstance.setSceneGameInstanceCode(jsonObject.getString("sceneGameInstanceCode"));// 获取游戏Id
		sceneGameInstance.setStartTime(DateUtils.dateStrToNewFormat(jsonObject.getString("startTime"), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2, DateUtils.DATE_PATTERN_YYYYMMDDHHmmss));// 活动开始时间
		// DateUtils.dateStrToNewFormat
		sceneGameInstance.setEndTime(DateUtils.dateStrToNewFormat(jsonObject.getString("endTime"), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2, DateUtils.DATE_PATTERN_YYYYMMDDHHmmss));// 活动结束时间
		sceneGameInstance.setLuckyDrawType(jsonObject.getString("luckyDrawType"));// 抽奖机会类型
		sceneGameInstance.setLuckyDrawValue(Integer.parseInt(jsonObject.getString("luckyDrawValue")));// 抽奖机会次数
		sceneGameInstance.setInstanceDetail(jsonObject.getString("instanceDetail"));// 活动详情
		sceneGameInstance.setInstanceDesc(jsonObject.getString("instanceDesc"));// 规则说明
		sceneGameInstance.setStatusId(StatusConstant.SCENE_GAME_INSTANCE_SAVE.getId());// 状态,
																						// 刚新建的的活动状态为已保存
		sceneGameInstance.setCreateBy(sysUser.getUserId());// 创建人
		sceneGameInstance.setUpdateBy(sysUser.getUserId());// 修改人，首次创建创建人和修改人为同一人
		sceneGameInstance.setIsAccord("N");// 是否有依据，N表示没有依据，Y表示有依据
		sceneGameInstance.setCreateTime(DateUtils.getCurrentTimeOfDb());// 实例创建时间
		sceneGameInstance.setUpdateTime(DateUtils.getCurrentTimeOfDb());// 实例修改时间
		sceneGameInstance.setSptIsuseforgroupmember(jsonObject.getString("sptIsuseforgroupmember"));// 是否使用分组
		sceneGameInstance.setSptIsusememberlabel(jsonObject.getString("sptIsusememberlabel"));// 是否使用标签
		sceneGameInstance.setSptIssharefamily(jsonObject.getString("sptIssharefamily"));// 是否家庭共享
		sceneGameInstance.setSptProducer(jsonObject.getString("sptProducer"));
		sceneGameInstance.setSptGrades(jsonObject.getString("sptGrades"));
		if(StringUtils.isNotBlank((String)jsonObject.get("isNeedPoints"))){
			sceneGameInstance.setIsNeedPoints(jsonObject.getString("isNeedPoints"));
		}else{
			sceneGameInstance.setIsNeedPoints("N");
		}
		if(StringUtils.isNotBlank((String)jsonObject.get("needPointsAmount"))){
			sceneGameInstance.setNeedPointsAmount(new BigDecimal((String)jsonObject.get("needPointsAmount")));
		}else{
			sceneGameInstance.setNeedPointsAmount(BigDecimal.ZERO);
		}
		
		// 保存分组
		if (sceneGameInstance.getSptIsuseforgroupmember() != null && "1".equals(sceneGameInstance.getSptIsuseforgroupmember())) {
			sceneGameInstance.setSptMgoid(Long.parseLong(jsonObject.getString("sptMgoid")));
		}
		// 保存标签
		if (sceneGameInstance.getSptIsusememberlabel() != null && "1".equals(sceneGameInstance.getSptIsusememberlabel())) {
			sceneGameInstance.setSptMlrid(jsonObject.getString("sptMlrids"));
		}
		sceneGameInstance.setSptClues(jsonObject.getString("sptClues"));
		// 保存实例
		sceneGameInstanceRepository.save(sceneGameInstance);

		// 2.场景游戏奖励奖项保存============================================
		listMap = JSON.parseObject(jsonObject.getString("awards"), new TypeReference<List<Map<String, Object>>>() {
		});
		for (Map<String, Object> awardMap : listMap) {
			award = new Awards();
			award.setSceneGameInstanceId(sceneGameInstance.getSceneGameInstanceId());
			award.setAwardsName(StringUtil.getStr(awardMap.get("awardsName")));// 奖项名称
			award.setAwardsCode(StringUtil.getStr(awardMap.get("awardsCode")));
			// 保存奖项
			int saveAwardCount = awardsRepository.saveSelective(award);
			if (saveAwardCount > 0) {
				// 保存奖项设置
				awardsSetting = new AwardsSetting();
				awardsSetting.setAwardsId(award.getAwardsId());
				awardsSetting.setCycle(Long.parseLong(StringUtil.getStr(awardMap.get("cycle"))));// 获取奖项周期
				awardsSetting.setPersions(Integer.parseInt(StringUtil.getStr(awardMap.get("persions"))));// 获取奖项的获奖人数
				// awardsSetting.setCycleValue(StringUtil.getStr(awardMap.get("cycleValue")));//获取奖项周期抽奖次数
				// 若奖项有限制
				if (Long.parseLong(StringUtil.getStr(awardMap.get("cycle"))) == EnumDateCycle.EVERY.getDateCycleId()) {
					awardsSetting.setCycleValue(StringUtil.getStr(awardMap.get("cycleValue")));// 获取奖项的获奖人数
				}
				awardsSetting.setProbability(new BigDecimal(StringUtil.getStr(awardMap.get("probability"))));// 获取中奖概率
				awardsSetting.setAwardsType(StringUtil.getStr(awardMap.get("awardsType")));// common
																							// 表示正常的奖项;lucky
																							// 表示谢谢参与奖
				awardsSettingRepository.save(awardsSetting);
			}
			if (saveAwardCount > 0) {
				// 保存奖品
				prize = new Prize();
				prize.setAwardsId(award.getAwardsId());
				prize.setPrizeName(StringUtil.getStr(awardMap.get("prizeName")));// 奖品名称
				prize.setPrizeType(Integer.parseInt(StringUtil.getStr(awardMap.get("prizeType"))));// 奖品类型
				prize.setPrizeCode(StringUtil.getStr(awardMap.get("prizeCode")));// 奖品编码
				prize.setTotalQuantity(Integer.parseInt(StringUtil.getStr(awardMap.get("persions"))));// 奖品总数
				prize.setSurplusQuantity(Integer.parseInt(StringUtil.getStr(awardMap.get("persions"))));// 奖品剩余量
				//prize.setEqualScore(StringUtil.getStr(awardMap.get("equalscore")));// 奖品积分
				int savePrizeCount = prizeRepository.save(prize);
				if (savePrizeCount > 0) {
					// 保存奖项和奖品之间的关系
					awardPrizeRel = new AwardPrizeRel();
					awardPrizeRel.setAwardsId(award.getAwardsId());
					awardPrizeRel.setPrizeId(prize.getPrizeId());
					awardPrizeRelRepository.save(awardPrizeRel);
					// 若奖品为实物奖品，则还需生成对应的奖品实例
					if (prize.getPrizeType() == 1 || prize.getPrizeType() == 4) {// 1表示实物奖品；2表示优惠券
						for (int i = 0; i < prize.getTotalQuantity(); i++) {
							prizeInstance = new PrizeInstance();
							prizeInstance.setPrizeId(prize.getPrizeId());
							prizeInstance.setStatusId(StatusConstant.PRIZE_INSTANCE_UNEXTRACT.getId());
							prizeInstanceList.add(prizeInstance);
						}
					}

				}
			}

		}
		// 实例奖品保存
		batchaInsert(prizeInstanceList);
	}

	@Override
	public List<SceneGameInstance> querySceneGameInstanceByGameId(Long sceneGameId, Page<SceneGameInstance> page) {
		// 更新互动状态
		sceneGameInstanceRepository.updateStatusOutDate();
		// 查询活动
		return sceneGameInstanceRepository.findSceneGameInstanceByGameId(sceneGameId, page);
	}

	@Override
	@Transactional
	public void editSceneGameInstance(JSONObject jsonObject, SysUser sysUser) {

		List<PrizeInstance> prizeInstanceList = new ArrayList<PrizeInstance>();
		List<Map<String, Object>> listMap = null;
		Awards award = null;
		AwardsSetting awardsSetting = null;
		Prize prize = null;
		AwardPrizeRel awardPrizeRel = null;
		PrizeInstance prizeInstance = null;
		// 1.保存场景游戏实例============================================
		SceneGameInstance sceneGameInstance = new SceneGameInstance();
		// 对象值填充
		sceneGameInstance.setSceneGameInstanceId(Long.parseLong(jsonObject.getString("sceneGameInstanceId")));
		sceneGameInstance.setSceneGameInstanceCode(jsonObject.getString("sceneGameInstanceCode"));// 活动实例编码
		sceneGameInstance.setInstanceName(jsonObject.getString("instanceName"));// 活动实例名称
		sceneGameInstance.setSceneGameId(jsonObject.getLong("sceneGameId"));// 获取游戏Id
		sceneGameInstance.setStartTime(DateUtils.dateStrToNewFormat(jsonObject.getString("startTime"), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2, DateUtils.DATE_PATTERN_YYYYMMDDHHmmss));// 活动开始时间
		// DateUtils.dateStrToNewFormat
		sceneGameInstance.setEndTime(DateUtils.dateStrToNewFormat(jsonObject.getString("endTime"), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2, DateUtils.DATE_PATTERN_YYYYMMDDHHmmss));// 活动结束时间
		sceneGameInstance.setLuckyDrawType(jsonObject.getString("luckyDrawType"));// 抽奖机会类型
		sceneGameInstance.setLuckyDrawValue(Integer.parseInt(jsonObject.getString("luckyDrawValue")));// 抽奖机会次数
		sceneGameInstance.setInstanceDetail(jsonObject.getString("instanceDetail"));// 活动详情
		sceneGameInstance.setInstanceDesc(jsonObject.getString("instanceDesc"));// 规则说明
		sceneGameInstance.setStatusId(StatusConstant.SCENE_GAME_INSTANCE_SAVE.getId());// 状态,
																						// 刚新建的的活动状态为已保存
		// sceneGameInstance.setCreateBy(sysUser.getUserId());//创建人
		sceneGameInstance.setUpdateBy(sysUser.getUserId());// 修改人，首次创建创建人和修改人为同一人
		sceneGameInstance.setIsAccord("N");// 是否有依据，N表示没有依据，Y表示有依据
		// sceneGameInstance.setCreateTime(DateUtils.getCurrentTimeOfDb());//实例创建时间
		sceneGameInstance.setUpdateTime(DateUtils.getCurrentTimeOfDb());// 实例修改时间
		sceneGameInstance.setSptIsuseforgroupmember(jsonObject.getString("sptIsuseforgroupmember"));// 是否使用分组
		sceneGameInstance.setSptIsusememberlabel(jsonObject.getString("sptIsusememberlabel"));// 是否使用标签
		sceneGameInstance.setSptIssharefamily(jsonObject.getString("sptIssharefamily"));// 是否家庭共享
		sceneGameInstance.setSptProducer(jsonObject.getString("sptProducer"));
		sceneGameInstance.setSptGrades(jsonObject.getString("sptGrades"));
		
		if(StringUtils.isNotBlank((String)jsonObject.get("isNeedPoints"))){
			sceneGameInstance.setIsNeedPoints(jsonObject.getString("isNeedPoints"));
		}else{
			sceneGameInstance.setIsNeedPoints("N");
		}
		if(StringUtils.isNotBlank((String)jsonObject.get("needPointsAmount"))){
			sceneGameInstance.setNeedPointsAmount(new BigDecimal((String)jsonObject.get("needPointsAmount")));
		}else{
			sceneGameInstance.setNeedPointsAmount(BigDecimal.ZERO);
		}
		
		// 保存分组
		if (sceneGameInstance.getSptIsuseforgroupmember() != null && "1".equals(sceneGameInstance.getSptIsuseforgroupmember())) {
			sceneGameInstance.setSptMgoid(Long.parseLong(jsonObject.getString("sptMgoid")));
		} else {
			sceneGameInstance.setSptMgoid(null);// 将分组Id大数据清空
		}
		// 保存标签
		if (sceneGameInstance.getSptIsusememberlabel() != null && "1".equals(sceneGameInstance.getSptIsusememberlabel())) {
			sceneGameInstance.setSptMlrid(jsonObject.getString("sptMlrids"));
		} else {
			sceneGameInstance.setSptMlrid(null);// 将标签信息清空
		}
		sceneGameInstance.setSptClues(jsonObject.getString("sptClues"));
		// 保存实例
		sceneGameInstanceRepository.updateByPkSelective(sceneGameInstance);

		// 删除奖项奖品关系
//		awardPrizeRelRepository.removeRelBySceneGameInstanceId(sceneGameInstance.getSceneGameInstanceId());
		// 删除奖品实例
//		prizeInstanceRepository.removePrizeInstanceById(sceneGameInstance.getSceneGameInstanceId());
//		// 删除奖品
//		prizeRepository.removePrize(sceneGameInstance.getSceneGameInstanceId());
//		// 删除奖项设置
//		awardsSettingRepository.removeAwardsSettings(sceneGameInstance.getSceneGameInstanceId());
//		// 删除奖项
//		awardsRepository.removeAwards(sceneGameInstance.getSceneGameInstanceId());

		// 2.场景游戏奖励奖项保存============================================
		listMap = JSON.parseObject(jsonObject.getString("awards"), new TypeReference<List<Map<String, Object>>>() {});
		for (Map<String, Object> awardMap : listMap) {
			award = new Awards();
			award.setSceneGameInstanceId(sceneGameInstance.getSceneGameInstanceId());
			award.setAwardsName(StringUtil.getStr(awardMap.get("awardsName")));// 奖项名称
			award.setAwardsCode(StringUtil.getStr(awardMap.get("awardsCode")));
			// 保存奖项
			int saveAwardCount = awardsRepository.saveSelective(award);
			if (saveAwardCount > 0) {
				// 保存奖项设置
				awardsSetting = new AwardsSetting();
				awardsSetting.setAwardsId(award.getAwardsId());
				awardsSetting.setCycle(Long.parseLong(StringUtil.getStr(awardMap.get("cycle"))));// 获取奖项周期
				// awardsSetting.setCycleValue(StringUtil.getStr(awardMap.get("cycleValue")));//获取奖项周期抽奖次数
				// 若奖项限制为所有，则获奖人数则为奖项库存
				if (Long.parseLong(StringUtil.getStr(awardMap.get("cycle"))) == EnumDateCycle.ALL.getDateCycleId()) {
					awardsSetting.setPersions(Integer.parseInt(StringUtil.getStr(awardMap.get("persions"))));// 获取奖项的获奖人数
				} else {
					awardsSetting.setCycleValue(StringUtil.getStr(awardMap.get("cycleValue")));// 获取奖项的参数值
				}
				awardsSetting.setProbability(new BigDecimal(StringUtil.getStr(awardMap.get("probability"))));// 获取中奖概率
				awardsSetting.setAwardsType(StringUtil.getStr(awardMap.get("awardsType")));// common
																							// 表示正常的奖项;lucky
																							// 表示谢谢参与奖
				awardsSettingRepository.save(awardsSetting);
			}
			if (saveAwardCount > 0) {
				// 保存奖品
				prize = new Prize();
				prize.setAwardsId(award.getAwardsId());
				prize.setPrizeName(StringUtil.getStr(awardMap.get("prizeName")));// 奖品名称
				prize.setPrizeType(Integer.parseInt(StringUtil.getStr(awardMap.get("prizeType"))));// 奖品类型
				prize.setPrizeCode(StringUtil.getStr(awardMap.get("prizeCode")));// 奖品编码
				prize.setTotalQuantity(Integer.parseInt(StringUtil.getStr(awardMap.get("persions"))));// 奖品总数
				prize.setSurplusQuantity(Integer.parseInt(StringUtil.getStr(awardMap.get("persions"))));// 奖品剩余量
				//prize.setEqualScore(StringUtil.getStr(awardMap.get("equalscore")));// 奖品积分
				int savePrizeCount = prizeRepository.save(prize);
				if (savePrizeCount > 0) {
					// 保存奖项和奖品之间的关系
					awardPrizeRel = new AwardPrizeRel();
					awardPrizeRel.setAwardsId(award.getAwardsId());
					awardPrizeRel.setPrizeId(prize.getPrizeId());
					awardPrizeRelRepository.save(awardPrizeRel);
					// 若奖品为实物奖品，则还需生成对应的奖品实例
					if (prize.getPrizeType() == 1) {// 1表示实物奖品；2表示优惠券；3表示谢谢参与；4表示积分
						for (int i = 0; i < prize.getTotalQuantity(); i++) {
							prizeInstance = new PrizeInstance();
							prizeInstance.setPrizeId(prize.getPrizeId());
							prizeInstance.setStatusId(StatusConstant.PRIZE_INSTANCE_UNEXTRACT.getId());
							prizeInstanceList.add(prizeInstance);
						}
					}

				}
			}

		}
		// 分批更新
		batchaInsert(prizeInstanceList);
	}

	// 分批插入
	private void batchaInsert(List<PrizeInstance> prizeInstanceList) {
		Integer times = null;// 每次分批执行的条数
		List<PrizeInstance> newPrizeInstanceList = null;
		if (prizeInstanceList != null) {
			int count = prizeInstanceList.size();
			times = Integer.parseInt(couponIssueNumber);// 每批次发放数量
			int onlyone = (count / times);// 批次数量
			// 整除部分
			for (int i = 0; i < onlyone; i++) {
				newPrizeInstanceList = new ArrayList<PrizeInstance>();
				newPrizeInstanceList.addAll(prizeInstanceList.subList(times * i, (i + 1) * times));
				prizeInstanceRepository.batchaddPrizeInstance(newPrizeInstanceList);
			}
			// 余数部分
			if (count % times > 0) {
				newPrizeInstanceList = new ArrayList<PrizeInstance>();
				newPrizeInstanceList.addAll(prizeInstanceList.subList(times * onlyone, count));
				prizeInstanceRepository.batchaddPrizeInstance(newPrizeInstanceList);
			}
		}
	}

	@Override
	public int editSceneGameInstanceStatus(SceneGameInstance sceneGameInstance) {
		return sceneGameInstanceRepository.updateSceneGameInstanceStatus(sceneGameInstance);
	}

	@Override
	public int countOpenSceneGameInstance(Long sceneGameId) {
		return sceneGameInstanceRepository.countOpenSceneGameInstance(sceneGameId);
	}

	@Override
	public SceneGameInstance queryValidInstanceById(Long sceneGameInstanceId) {
		return sceneGameInstanceRepository.findValidInstanceById(sceneGameInstanceId);
	}
	@Override
	public SceneGameInstance queryMapsQrCode(Long sceneGameInstanceId) {
		return sceneGameInstanceRepository.findValidInstanceById(sceneGameInstanceId);
	}
	@Override
	public SceneGameInstance queryValidInstanceByCode(String sceneGameInstanceCode) {
		List<SceneGameInstance> sceneGameInstances = sceneGameInstanceRepository.findValidInstanceByCode(sceneGameInstanceCode);
		if(sceneGameInstances.isEmpty()){
			return null;
		}
		for (SceneGameInstance sceneGameInstance : sceneGameInstances) {
			
			//活动状态必须激活
			if(sceneGameInstance.getStatusId().compareTo(StatusConstant.SCENE_GAME_INSTANCE_ENABLE.getId())!=0){
				continue;
			}
			String currentDate=DateUtils.getCurrentTimeOfDb();
			//如果开始时间还没到
			if(sceneGameInstance.getStartTime().compareTo(currentDate) > 0 ){
				ActivityException exception=new ActivityException(ActivityException.ACTIVITY_START);
				exception.setResultStatus(ResultStatus.ACTIVITY_START);
				throw exception;
			}
			//如果到期时间已过
			if(sceneGameInstance.getEndTime().compareTo(currentDate) < 0 ){
				ActivityException exception=new ActivityException(ActivityException.ACTIVITY_END);
				exception.setResultStatus(ResultStatus.ACTIVITY_END);
				throw exception;
			}
			
			return sceneGameInstance;
		}
		return null;
	}

	@Override
	public boolean ValidInstanceByStartTime(Long sceneGameInstanceId) {
		int i = sceneGameInstanceRepository.findValidInstanceByStartTime(sceneGameInstanceId);
		if (i == 1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean ValidInstanceByEndTime(Long sceneGameInstanceId) {
		int i = sceneGameInstanceRepository.findValidInstanceByEndTime(sceneGameInstanceId);
		if (i == 1) {
			return true;
		}
		return false;
	}

	@Override
	public List<SceneGameInstanceExp> querySceneGameWinRate(SceneGameInstanceSearch search) {
		return sceneGameInstanceRepository.findSceneGameWinRate(search.getLikeInstanceName(), search.getGreaterEqualCreateTime(), search.getLessEqualCreateTime());
	}

	@Override
	public List<SceneGameInstanceExp> querySceneGameAwardsWinRate(Long sceneGameInstanceId) {

		return sceneGameInstanceRepository.findSceneGameAwardsWinRate(sceneGameInstanceId);
	}
	public void editSceneGameInstanceIsUse(JSONObject jsonObject, SysUser sysUser) {

		// 1.保存场景游戏实例============================================
		SceneGameInstance sceneGameInstance = new SceneGameInstance();
		// 对象值填充
		sceneGameInstance.setSceneGameInstanceId(Long.parseLong(jsonObject.getString("sceneGameInstanceId")));
		sceneGameInstance.setInstanceName(jsonObject.getString("instanceName"));// 活动实例名称
		sceneGameInstance.setSceneGameId(jsonObject.getLong("sceneGameId"));// 获取游戏Id
		sceneGameInstance.setStartTime(DateUtils.dateStrToNewFormat(jsonObject.getString("startTime"), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2, DateUtils.DATE_PATTERN_YYYYMMDDHHmmss));// 活动开始时间
		// DateUtils.dateStrToNewFormat
		sceneGameInstance.setEndTime(DateUtils.dateStrToNewFormat(jsonObject.getString("endTime"), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2, DateUtils.DATE_PATTERN_YYYYMMDDHHmmss));// 活动结束时间
		sceneGameInstance.setLuckyDrawType(jsonObject.getString("luckyDrawType"));// 抽奖机会类型
		sceneGameInstance.setLuckyDrawValue(Integer.parseInt(jsonObject.getString("luckyDrawValue")));// 抽奖机会次数
		sceneGameInstance.setInstanceDetail(jsonObject.getString("instanceDetail"));// 活动详情
		sceneGameInstance.setInstanceDesc(jsonObject.getString("instanceDesc"));// 规则说明
		// sceneGameInstance.setStatusId(StatusConstant.SCENE_GAME_INSTANCE_SAVE.getId());//状态,
		// 刚新建的的活动状态为已保存
		// sceneGameInstance.setCreateBy(sysUser.getUserId());//创建人
		sceneGameInstance.setUpdateBy(sysUser.getUserId());// 修改人，首次创建创建人和修改人为同一人
		sceneGameInstance.setIsAccord("N");// 是否有依据，N表示没有依据，Y表示有依据
		// sceneGameInstance.setCreateTime(DateUtils.getCurrentTimeOfDb());//实例创建时间
		sceneGameInstance.setUpdateTime(DateUtils.getCurrentTimeOfDb());// 实例修改时间
		sceneGameInstance.setSptIsuseforgroupmember(jsonObject.getString("sptIsuseforgroupmember"));// 是否使用分组
		sceneGameInstance.setSptIsusememberlabel(jsonObject.getString("sptIsusememberlabel"));// 是否使用标签
		sceneGameInstance.setSptIssharefamily(jsonObject.getString("sptIssharefamily"));// 是否家庭共享
		sceneGameInstance.setSptProducer(jsonObject.getString("sptProducer"));
		sceneGameInstance.setSptGrades(jsonObject.getString("sptGrades"));
		if(StringUtils.isNotBlank((String)jsonObject.get("isNeedPoints"))){
			sceneGameInstance.setIsNeedPoints(jsonObject.getString("isNeedPoints"));
		}else{
			sceneGameInstance.setIsNeedPoints("N");
		}
		if(StringUtils.isNotBlank((String)jsonObject.get("needPointsAmount"))){
			sceneGameInstance.setNeedPointsAmount(new BigDecimal((String)jsonObject.get("needPointsAmount")));
		}else{
			sceneGameInstance.setNeedPointsAmount(BigDecimal.ZERO);
		}
		
		
		
		// 保存分组
		if (sceneGameInstance.getSptIsuseforgroupmember() != null && "1".equals(sceneGameInstance.getSptIsuseforgroupmember())) {
			sceneGameInstance.setSptMgoid(Long.parseLong(jsonObject.getString("sptMgoid")));
		} else {
			sceneGameInstance.setSptMgoid(null);// 将分组Id大数据清空
		}
		// 保存标签
		if (sceneGameInstance.getSptIsusememberlabel() != null && "1".equals(sceneGameInstance.getSptIsusememberlabel())) {
			sceneGameInstance.setSptMlrid(jsonObject.getString("sptMlrids"));
		} else {
			sceneGameInstance.setSptMlrid(null);// 将标签信息清空
		}
		sceneGameInstance.setSptClues(jsonObject.getString("sptClues"));
		// 保存实例
		sceneGameInstanceRepository.updateByPkSelective(sceneGameInstance);
	}

	@Override
	public SceneGameInstance queryValidInstance(String sceneGameCode) {
		return sceneGameInstanceRepository.findValidInstance(sceneGameCode);
	}
}
