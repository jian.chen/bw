package com.bw.adv.service.job.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.component.job.model.ComTaskType;


public class ComTaskTypeConstant extends AbstractConstant<ComTaskType> {
	
	public static final ComTaskTypeConstant SYSTEM_LOOP_TASK = new ComTaskTypeConstant(1L, "SYSTEM_LOOP_TASK", "系统线程");
	public static final ComTaskTypeConstant ACITVITY_SEND_POINTS = new ComTaskTypeConstant(2L, "ACITVITY_SEND_POINTS", "活动送积分");
	public static final ComTaskTypeConstant BUSINESS_RULE_EXPIRED = new ComTaskTypeConstant(3L, "BUSINESS_RULE_EXPIRED", "业务规则过期");
	public static final ComTaskTypeConstant ACTIVEITY_SEND_MAIL = new ComTaskTypeConstant(101L, "ACTIVEITY_SEND_MAIL", "活动发邮件");
	public static final ComTaskTypeConstant SEND_MAIL = new ComTaskTypeConstant(102L, "SEND_MAIL", "发邮件");
	public static final ComTaskTypeConstant FREEZE_POINTS_MAIL = new ComTaskTypeConstant(103L, "FREEZE_POINTS_MAIL", "积分冻结发邮件");
	public static final ComTaskTypeConstant ACITVITY_SEND_COUPON = new ComTaskTypeConstant(102L, "ACITVITY_SEND_COUPON", "活动送优惠券");
	public static final ComTaskTypeConstant MODIFY_PASSWORD_MAIL = new ComTaskTypeConstant(105L, "MODIFY_PASSWORD_MAIL", "修改密码");
	

	public ComTaskTypeConstant(Long id, String code, String name) {
		super(id, code, name);
	}
	
	
	@Override
	public ComTaskType getInstance() {
		ComTaskType comTaskType = new ComTaskType();
		comTaskType.setTaskTypeId(id);
		comTaskType.setTaskTypeName(name);
		return comTaskType;
	}

}
