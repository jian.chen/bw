/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:GoodsShowMemberCardGradeService.java
 * Package Name:com.sage.scrm.service.points.service
 * Date:2016年7月18日下午7:16:27
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.goods.model.GoodsShowMemberCardGrade;

/**
 * ClassName:GoodsShowMemberCardGradeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年7月18日 下午7:16:27 <br/>
 * scrmVersion standard
 * @author   dong.d
 * @version  jdk1.7
 * @see 	 
 */
public interface GoodsShowMemberCardGradeService{

	
	/**
	 * saveGoodsShowMemberCardGrade:(新增积分兑换要求). <br/>
	 * Date: 2016年7月18日 下午7:17:46 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param goodsShowMemberCardGradeList
	 * @return
	 */
	int saveGoodsShowMemberCardGrade(List<GoodsShowMemberCardGrade> goodsShowMemberCardGradeList);
	
	
	
	
	/**
	 * 
	 * updateGoodsShowMemberCardGrade:(更新积分兑换要求). <br/>
	 * Date: 2016年7月18日 下午8:31:23 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param goodsShowMemberCardGradeList
	 * @return
	 */
	int updateGoodsShowMemberCardGrade(GoodsShowMemberCardGrade goodsShowMemberCardGrade);
	
	
	
	/**
	 * queryGoodsShowMemberCardGradeListByGoodsShowId:(根据积分兑换单ID获取对用兑换关系列表). <br/>
	 * Date: 2016年7月19日 上午11:33:44 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param goodsShowId
	 * @return
	 */
	List<GoodsShowMemberCardGrade> queryGoodsShowMemberCardGradeListByGoodsShowId(Long goodsShowId);
	
}

