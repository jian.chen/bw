/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansGroupItemServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月20日下午4:24:14
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansGroup;
import com.bw.adv.module.wechat.model.WechatFansGroupItem;
import com.bw.adv.module.wechat.model.exp.WechatFansExp;
import com.bw.adv.module.wechat.model.exp.WechatFansGroupItemExp;
import com.bw.adv.module.wechat.repository.WechatFansGroupItemRepository;
import com.bw.adv.service.wechat.service.WechatFansGroupItemService;
import com.bw.adv.service.wechat.service.WechatFansGroupService;

/**
 * ClassName:WechatFansGroupItemServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午4:24:14 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatFansGroupItemServiceImpl extends BaseServiceImpl<WechatFansGroupItem> implements WechatFansGroupItemService {
	// 默认组
	@Value("${WEHCAT_GROUP_DEFAULT}")
	private static String wechat_group_default;
	
	private WechatFansGroupItemRepository wechatFansGroupItemRepository;
	
	private WechatFansGroupService wechatFansGroupService;

	@Override
	public BaseRepository<WechatFansGroupItem, ? extends BaseMapper<WechatFansGroupItem>> getBaseRepository() {
		return wechatFansGroupItemRepository;
	}
	
	@Override
	public void save(String wechatFanId, WechatFansGroup wechatFansGroup) {
		WechatFansGroupItem wechatFansGroupItem =null;
		if(wechatFansGroup !=null){
			wechatFansGroupItem =new WechatFansGroupItem();
			wechatFansGroupItem.setWechatFansGroupId(wechatFansGroup.getWechatFansGroupId());
			wechatFansGroupItem.setWechatFansId(Long.valueOf(wechatFanId));
			wechatFansGroupItem.setCreateTime(DateUtils.getCurrentTimeOfDb());
			wechatFansGroupItem.setUpdatedTime(DateUtils.getCurrentDateOfDb());
			wechatFansGroupItem.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
			this.save(wechatFansGroupItem);
		}
	}
	
	@Override
	public List<WechatFansExp> transToGroupItemExp(List<WechatFans> wechatFans,String wechatGroupId) {
		List<WechatFansExp> replaceWechatFansExp =null;
		replaceWechatFansExp =new ArrayList<WechatFansExp>();
		if(!StringUtils.isEmpty(wechatGroupId)){
			for(WechatFans wechatFan:wechatFans){
				WechatFansExp wechatFansExp =new WechatFansExp();
				BeanUtils.copy(wechatFan, wechatFansExp);
				WechatFansGroupItem wechatFansGroupItem =this.queryWechatFansGroupItemByFansId(wechatFan.getWechatFansId());
				if(null!=wechatFansGroupItem){
					if(wechatFansGroupItem.getWechatFansGroupId().equals(Long.valueOf(wechatGroupId))){
						wechatFansExp.setWechatFansGroupId(wechatFansGroupItem.getWechatFansGroupId());
						replaceWechatFansExp.add(wechatFansExp);
					}
				}
			}
		}else{
			for(WechatFans wechatFan:wechatFans){
				WechatFansExp wechatFansExp =new WechatFansExp();
				BeanUtils.copy(wechatFan, wechatFansExp);
				WechatFansGroupItem wechatFansGroupItem =this.queryWechatFansGroupItemByFansId(wechatFan.getWechatFansId());
				if(null!=wechatFansGroupItem){
					wechatFansExp.setWechatFansGroupId(wechatFansGroupItem.getWechatFansGroupId());
				}
				replaceWechatFansExp.add(wechatFansExp);
			}
		}
		return replaceWechatFansExp;
	}
	
	/**
	 * 1.查询出列表信息
	 * 2.信息置为无效
	 * 3.将信息放到为分组中
	 * TODO 简单描述该方法的实现功能（可选）.
	 * @see com.bw.adv.service.wechat.service.WechatFansGroupItemService#deleteWechatFansGroupItems(java.lang.String)
	 */
	@Override
	public void deleteWechatFansGroupItems(String wechatGroupId) {
		List<WechatFansGroupItem> list =null;
		WechatFansGroupItem wechatFansGroupItem =null;
		WechatFansGroup wechatFansGroup =null;
		list = this.queryWechatFansGroupItemsByGroupId(Long.valueOf(wechatGroupId));
		wechatFansGroup = wechatFansGroupService.queryWechatFansGroup(wechat_group_default);
		if(list.size()>0){
			// 将删除组批量移到默认组
			for(WechatFansGroupItem item:list){
				wechatFansGroupItem =new WechatFansGroupItem();
				wechatFansGroupItem.setCreateTime(DateUtils.getCurrentTimeOfDb());
				wechatFansGroupItem.setIsDelete(WechatTypeConstant.UNDELETED.getCode());
				wechatFansGroupItem.setWechatFansGroupId(wechatFansGroup.getWechatFansGroupId());
				wechatFansGroupItem.setWechatFansId(item.getWechatFansId());
				this.save(wechatFansGroupItem);
			}
			// 将删除组信息置为无效
			for(WechatFansGroupItem item:list){
				item.setIsDelete(WechatTypeConstant.DELETED.getCode());
				item.setUpdatedTime(DateUtils.getCurrentTimeOfDb());
				this.updateByPk(item);
			}
		}
	}

	
	private List<WechatFansGroupItem> queryWechatFansGroupItemsByGroupId(Long wechatGroupId) {
		return wechatFansGroupItemRepository.findWechatFansGroupItemsByGroupId(wechatGroupId);
	}

	@Override
	public WechatFansGroupItem queryWechatFansGroupItemByFansId(Long wechatId) {
		return wechatFansGroupItemRepository.findWechatFansGroupItemByFansId(wechatId);
	}
	
	@Override
	public List<WechatFansGroupItemExp> queryWechatFansGroupItemByGroupId(Long groupId) {
		return wechatFansGroupItemRepository.WechatFansGroupItemByGroupId(groupId);
	}
	
	@Autowired
	public void setWechatFansGroupItemRepository(
			WechatFansGroupItemRepository wechatFansGroupItemRepository) {
		this.wechatFansGroupItemRepository = wechatFansGroupItemRepository;
	}

	@Autowired
	public void setWechatFansGroupService(WechatFansGroupService wechatFansGroupService) {
		this.wechatFansGroupService = wechatFansGroupService;
	}


}

