/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MealPeriodService.java
 * Package Name:com.sage.scrm.service.base.service
 * Date:2016年5月23日下午5:32:12
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.MealPeriod;

/**
 * ClassName:MealPeriodService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月23日 下午5:32:12 <br/>
 * scrmVersion 1.0
 * @author   zhangqi
 * @version  jdk1.7
 * @see 	 
 */
public interface MealPeriodService extends BaseService<MealPeriod> {
	
	/**
	 * 
	 * queryMealPeriodByStatusId:查看有效时段. <br/>
	 * Date: 2016年5月23日 下午5:33:59 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	public List<MealPeriod> queryMealPeriodByStatusId(Page<MealPeriod> page);
	
	/**
	 * 
	 * selectByPrimaryKey:按时段id查询时段信息. <br/>
	 * Date: 2016年5月24日 下午1:47:50 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodId
	 * @return
	 */
	public MealPeriod selectByPrimaryKey(Long mealPeriodId);

	/**
	 * 
	 * insertSelective:新增时段. <br/>
	 * Date: 2016年5月24日 上午10:39:21 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param meal
	 * @return
	 */
	public int insertSelective(MealPeriod meal);
	
	/**
	 * 
	 * updateMealPeriod:修改时段. <br/>
	 * Date: 2016年5月24日 下午2:48:43 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param meal
	 * @return
	 */
	public int updateMealPeriod(MealPeriod meal);
	
	/**
	 * 
	 * deleteMealPeriodList:批量删除时段（逻辑删除）. <br/>
	 * Date: 2016年5月24日 下午3:15:41 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodIds
	 * @return
	 */
	public int deleteMealPeriodList(String mealPeriodIds);
	
	/**
	 * 
	 * selectByMealPeriodNameById:查询时段名称是否有相同的(排除自身的时段名称). <br/>
	 * Date: 2016年5月24日 下午4:00:15 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodName
	 * @param mealPeriodId
	 * @return
	 */
	public Long selectByMealPeriodNameById(String mealPeriodName,Long mealPeriodId);
}

