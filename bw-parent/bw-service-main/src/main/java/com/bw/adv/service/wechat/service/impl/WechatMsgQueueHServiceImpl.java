/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMsgQueueHServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年12月16日上午11:28:47
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.model.WechatMsgQueueHHistory;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueHExp;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;
import com.bw.adv.service.wechat.service.WechatMsgQueueHHistoryService;
import com.bw.adv.service.wechat.service.WechatMsgQueueHService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * ClassName:WechatMsgQueueHServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月16日 上午11:28:47 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatMsgQueueHServiceImpl extends BaseServiceImpl<WechatMsgQueueH> implements WechatMsgQueueHService{

	private WechatMsgQueueHRepository wechatMsgQueueHRepository;
	private WechatMsgQueueHHistoryService wechatMsgQueueHHistoryService;
	@Override
	public List<WechatMsgQueueHExp> findAllWechatMsgQueueHs(Long limit) {
		return wechatMsgQueueHRepository.findAllWechatMsgQueueHs(limit);
	}

	@Override
	public void deleteWechatMsgQueueH(List<WechatMsgQueueHExp> list) {
		WechatMsgQueueHHistory history = null;
		List<WechatMsgQueueHHistory> historys = new ArrayList<WechatMsgQueueHHistory>();
		List<Long> ids = new ArrayList<Long>();
		for(WechatMsgQueueH instance : list){
			history = new WechatMsgQueueHHistory();
			BeanUtils.copy(instance, history);
			ids.add(instance.getWechatMsgQueueId());
			history.setSendTime(DateUtils.getCurrentTimeOfDb());
			history.setIsSend("Y");
			historys.add(history);
		}
		this.deleteAllWechatMsgQueueH(ids);
		wechatMsgQueueHHistoryService.saveAllWechatMsgQueueHHistory(historys);
	}

	public void updateBatchWechatMsgQueueH(List<WechatMsgQueueHExp> list){
		wechatMsgQueueHRepository.updateBatchWechatMsgQueueH(list);
	}
	
	@Override
	public void deleteAllWechatMsgQueueH(List<Long> ids) {
		if(ids != null && ids.size() > 0){
			wechatMsgQueueHRepository.deleteAllWechatMsgQueueH(ids);
		}
	}
	@Override
	public void saveWechatMsgQueueH(List<WechatMsgQueueH> list) {
		if(list == null || list.size() <= 0){
			return;
		}
		wechatMsgQueueHRepository.saveList(list);
	}
	@Override
	public BaseRepository<WechatMsgQueueH, ? extends BaseMapper<WechatMsgQueueH>> getBaseRepository() {
		return wechatMsgQueueHRepository;
	}

	@Autowired
	public void setWechatMsgQueueHRepository(WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}
	
	@Autowired
	public void setWechatMsgQueueHHistoryService(WechatMsgQueueHHistoryService wechatMsgQueueHHistoryService) {
		this.wechatMsgQueueHHistoryService = wechatMsgQueueHHistoryService;
	}

}

