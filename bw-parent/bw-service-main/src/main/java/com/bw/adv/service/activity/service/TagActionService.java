package com.bw.adv.service.activity.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.ActivityTagAction;

public interface TagActionService extends BaseService<ActivityTagAction>{
	
	/**
	 * scanActivityAction:(扫描标签动作)
	 */
	void scanTagAction();

}
