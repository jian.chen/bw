package com.bw.adv.service.order.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.member.model.MemberGift;
import com.bw.adv.module.member.model.exp.MemberGiftExt;


public interface MemberGiftService extends BaseService<MemberGift> {
	public int saveOrUpdateMemberGiftRecord(CouponInstance couponInstance,Long memberId,Coupon coupon);

	MemberGift countNotReceiveMemberGiftRecord(Long memberId, Long couponId);
	

	/**
	 * 统计我的奖品
	 * @param memberId
	 * @return
	 */
	List<MemberGiftExt> findReceiveMemberGiftRecord(Long memberId);
	
	/**
	 * 统计我的奖品根据奖品
	 * @param memberId
	 * @return
	 */
	List<MemberGift> findReceiveMemberGiftByGiftCode(String giftCode,Long memberId);
	
}
