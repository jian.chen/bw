package com.bw.adv.service.activity.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.ActivityBirthdayRecord;

public interface ActivityBirthdayRecordService extends BaseService<ActivityBirthdayRecord>{
	
	/**
	 * 根据会员id，活动实例id，创建年份查找记录
	 * @param memberId
	 * @param activityInstanceId
	 * @param createYear
	 * @ret
	 */
	List<ActivityBirthdayRecord> findRecordByIdAndMember(Long memberId,Long activityInstanceId,String createYear);
	
	

}
