/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceReceiveServiceImpl.java
 * Package Name:com.sage.scrm.bk.coupon.service.impl
 * Date:2015年11月24日下午9:06:13
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.coupon.model.CouponInstanceReceive;
import com.bw.adv.module.coupon.repository.CouponInstanceReceiveRepository;
import com.bw.adv.service.coupon.service.CouponInstanceReceiveService;

/**
 * ClassName:CouponInstanceReceiveServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月24日 下午9:06:13 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class CouponInstanceReceiveServiceImpl extends BaseServiceImpl<CouponInstanceReceive> implements CouponInstanceReceiveService{
	private CouponInstanceReceiveRepository couponInstanceReceiveRepository;
	@Override
	public BaseRepository<CouponInstanceReceive, ? extends BaseMapper<CouponInstanceReceive>> getBaseRepository() {
		return couponInstanceReceiveRepository;
	}
	@Autowired
	public void setCouponInstanceReceiveRepository(CouponInstanceReceiveRepository couponInstanceReceiveRepository) {
		this.couponInstanceReceiveRepository = couponInstanceReceiveRepository;
	}

}

