/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberWinningAccordService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2015年11月23日上午11:58:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.MemberWinningAccord;

/**
 * ClassName:MemberWinningAccordService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 上午11:58:28 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberWinningAccordService extends BaseService<MemberWinningAccord>{
	
	/**
	 * 
	 * queryMemberSurplusNumber:(统计符合条件的会员抽奖资格的次数). <br/>
	 * Date: 2015年11月24日 下午3:20:05 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberId
	 * @param activityId
	 * @return
	 */
	int queryMemberSurplusNumber(Long memberId, Long activityId,Long qrCodeId);
	
	/**
	 * 查询会员当天抽奖资格
	 * @param memberId
	 * @param activityId
	 * @param day
	 * @return
	 */
	Long queryMemberSurplusNumberToday(Long memberId ,Long activityId,Long qrCodeId, String day);
	
	/**
	 * 当天使用了的次数
	 * @param memberId
	 * @param activityId
	 * @param qrCodeId 二维码id
	 * @param someDay
	 * @return
	 */
	Long queryMemberUseChance(Long memberId, Long activityId,Long qrCodeId,String someDay);
	
	/**
	 * 
	 * updateOneByMember:(每抽奖一次，根据活动id,修改抽奖资格(创建时间早的优先修改)). <br/>
	 * Date: 2016年3月17日 下午4:35:52 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param memberId
	 * @param activityId
	 */
	void updateOneByMember(Long memberId, Long activityId,Long qrCodeId);
	
	/**
     * 根据会员id和活动id查询抽奖资格
     * @param memberId
     * @param activityId
     * @return
     */
    MemberWinningAccord queryResultByMemberIdAndActivityId(Long memberId,Long activityId,Long qrCodeId);
    
    MemberWinningAccord queryResultToday(Long memberId,Long activityId,Long qrCodeId,String day);
    
    /**
     * 更新抽奖资格表
     * @param memberWinningAccordId
     * @param version
     * @param useTime
     * @return
     */
    int updateMemberAccordWithVersion(Long memberWinningAccordId,Long qrCodeId,Long version,String useTime);

	MemberWinningAccord queryMemberMapsQrCode(Long memberId,Long activityId, String qrCode,String isUse);

	int updateMemberAccordMapsVersion(Long memberWinningAccordId,String qrCode, String useTime);
	
}

