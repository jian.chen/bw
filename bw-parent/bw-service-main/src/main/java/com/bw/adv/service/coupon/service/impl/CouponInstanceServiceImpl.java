package com.bw.adv.service.coupon.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.repository.StoreRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.coupon.exception.CouponException;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.CouponInstanceSearch;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.coupon.repository.CouponInstanceRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueueH;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHRepository;
import com.bw.adv.service.coupon.service.CouponInstanceService;
import com.bw.adv.service.coupon.service.CouponService;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * ClassName: CouponInstanceServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 下午2:29:48 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Service
public class CouponInstanceServiceImpl extends BaseServiceImpl<CouponInstance> implements CouponInstanceService {
	
	private static final Logger logger =LoggerFactory.getLogger(CouponInstanceServiceImpl.class);

	private CouponInstanceRepository couponInstanceRepository;
	private CouponService couponService;
	private StoreRepository storeRepository;

	private WechatMsgQueueHRepository wechatMsgQueueHRepository;

	@Override
	public List<CouponInstanceExp> queryListByDyc(CouponInstanceSearch couponInstanceSearch,Page<CouponInstanceExp> page) {
		String couponIssueStartTime = null;
		String couponIssueEndTime = null;
		
		Example example = new Example();
		Criteria criteria =  example.createCriteria();
		criteria.andEqualTo("cou.COUPON_ID", couponInstanceSearch.getCouponId());
		if(couponInstanceSearch.getMemberMobile()!=null && couponInstanceSearch.getMemberMobile()!=""){
			criteria.andEqualTo("mem.mobile", couponInstanceSearch.getMemberMobile());
		}
		if(couponInstanceSearch.getIssueStartTime()!=null && couponInstanceSearch.getIssueStartTime()!=""){
			couponIssueStartTime = couponInstanceSearch.getIssueStartTime();
			couponIssueStartTime = couponIssueStartTime.replaceAll("-", "");
			criteria.andGreaterThanOrEqualTo("couis.ISSUE_DATE", couponIssueStartTime);
		}
		if(couponInstanceSearch.getIssueEndTime()!=null && couponInstanceSearch.getIssueEndTime()!=""){
			couponIssueEndTime = couponInstanceSearch.getIssueEndTime();
			couponIssueEndTime = couponIssueEndTime.replaceAll("-", "");
			criteria.andLessThanOrEqualTo("couis.ISSUE_DATE", couponIssueEndTime);
		}
		if(couponInstanceSearch.getCouponInstanceCode()!=null && couponInstanceSearch.getCouponInstanceCode()!=""){
			criteria.andEqualTo("couin.COUPON_INSTANCE_CODE", couponInstanceSearch.getCouponInstanceCode());
		} 
		if(!couponInstanceSearch.getIssueStatus().equals("0")){
			criteria.andEqualTo("couis.STATUS_ID", couponInstanceSearch.getIssueStatus());
		}
		if(couponInstanceSearch.getStatusId() != 0L){
			criteria.andEqualTo("couin.STATUS_ID", couponInstanceSearch.getStatusId());
		}
		return couponInstanceRepository.findCouponInstanceList(example,page);
	}
	
	@Override
	public List<CouponInstanceExp> queryListByDyc(CouponInstanceSearch couponInstanceSearch) {
		String couponIssueStartTime = null;
		String couponIssueEndTime = null;
		
		Example example = new Example();
		Criteria criteria =  example.createCriteria();
		criteria.andEqualTo("cou.COUPON_ID", couponInstanceSearch.getCouponId());
		if(couponInstanceSearch.getMemberMobile()!=null && couponInstanceSearch.getMemberMobile()!=""){
			criteria.andEqualTo("mem.mobile", couponInstanceSearch.getMemberMobile());
		}
		if(couponInstanceSearch.getIssueStartTime()!=null && couponInstanceSearch.getIssueStartTime()!=""){
			couponIssueStartTime = couponInstanceSearch.getIssueStartTime();
			couponIssueStartTime = couponIssueStartTime.replaceAll("-", "");
			criteria.andGreaterThanOrEqualTo("couis.ISSUE_DATE", couponIssueStartTime);
		}
		if(couponInstanceSearch.getIssueEndTime()!=null && couponInstanceSearch.getIssueEndTime()!=""){
			couponIssueEndTime = couponInstanceSearch.getIssueEndTime();
			couponIssueEndTime = couponIssueEndTime.replaceAll("-", "");
			criteria.andLessThanOrEqualTo("couis.ISSUE_DATE", couponIssueEndTime);
		}
		if(couponInstanceSearch.getCouponInstanceCode()!=null && couponInstanceSearch.getCouponInstanceCode()!=""){
			criteria.andEqualTo("couin.COUPON_INSTANCE_CODE", couponInstanceSearch.getCouponInstanceCode());
		} 
		if(!couponInstanceSearch.getIssueStatus().equals("0")){
			criteria.andEqualTo("couis.STATUS_ID", couponInstanceSearch.getIssueStatus());
		}
		if(couponInstanceSearch.getStatusId() != 0L){
			criteria.andEqualTo("couin.STATUS_ID", couponInstanceSearch.getStatusId());
		}
		return couponInstanceRepository.findCouponInstanceList(example);
	}
	
	@Override
	public List<CouponInstanceExp> queryListByDyc(Example example) {
		
		return couponInstanceRepository.findCouponInstanceListByDyc(example);
	}

	@Override
	public BaseRepository<CouponInstance, ? extends BaseMapper<CouponInstance>> getBaseRepository() {
		return couponInstanceRepository;
	}
	
	@Override
	public int queryCountByDyc(CouponInstanceSearch couponInstanceSearch) {
		
		Map<String, Object> params = null;
		
		params = new HashMap<String, Object>();
		params.put("mobile", couponInstanceSearch.getMemberMobile());
		params.put("issueStartTime", couponInstanceSearch.getIssueStartTime());
		params.put("issueEndTime", couponInstanceSearch.getIssueEndTime());
		params.put("couponCode", couponInstanceSearch.getCouponCode());
		return couponInstanceRepository.findCountByDyc(params);
	}
	
	
	@Override
	public void insertCouponInstanceUpdate(Long memberId,List<CouponInstance> couponInstanceList){
		for(CouponInstance couponInstance:couponInstanceList){
			couponInstance.setCouponInstanceOwner(memberId);
			couponInstance.setCouponInstanceUser(memberId);
			couponInstance.setGetTime(DateUtils.getCurrentTimeOfDb());
			//未使用
			couponInstance.setStatusId(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
			couponInstance.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			this.updateByPk(couponInstance);
		}
	}

	@Override
	public CouponInstance queryCoupontByCode(String couponInstanceCode) {
		return couponInstanceRepository.findCouponInstanceByInstanceCode(couponInstanceCode);
	}
	
	
	@Override
	public CouponInstanceExp queryCouponInstanceExpByExample(Example example) {
		return couponInstanceRepository.findCouponInstanceExpByExample(example);
	}

		
	/**
	 * TODO 查询会员可用的优惠券.
	 * @see com.bw.adv.service.coupon.service.CouponInstanceService#queryActiveListByMemberId(java.lang.Long)
	 */
	@Override
	public List<CouponInstanceExp> queryActiveListByMemberId(Long memberId) {
		return couponInstanceRepository.findActiveListByMemberId(memberId);
	}
	
	@Override
	public CouponInstanceExp queryExpByCode(String couponInstanceCode) {
		return couponInstanceRepository.findExpByCode(couponInstanceCode);
	}
	
	/**
	 * TODO 查询并冻结优惠券实例（可选）.
	 * @see com.bw.adv.service.coupon.service.CouponInstanceService#freezeCouponInstanceByCode(java.lang.String)
	 */
	@Override
	public CouponInstanceExp freezeCouponInstanceByCode(String couponInstanceCode,String storeCode) {
		CouponInstanceExp instanceExp = null;
		CouponInstance instance = null;
		String currentTime = null;
		Store store = null;
		
		instanceExp = couponInstanceRepository.findExpByCode(couponInstanceCode);
		
		if(StringUtils.isBlank(storeCode)){
			throw new CouponException(ResultStatus.COUPON_INSTANCE_STORE_ERROR);
		}
		store = storeRepository.findByStoreCode(storeCode);
		
		
		//验证优惠券是否可用
		couponService.checkCouponInstance(instanceExp, store);
		
		if(StatusConstant.COUPON_INSTANCE_FREEZE.getId().equals(instanceExp.getStatusId())) {
            logger.error("优惠券验证失败："+ResultStatus.COUPON_INSTANCE_STATUS_FREEZE_ERROR.getMsg());
            throw new CouponException(ResultStatus.COUPON_INSTANCE_STATUS_FREEZE_ERROR);
        }
		
		//冻结优惠券
		instance = new CouponInstance();
		currentTime = DateUtils.getCurrentTimeOfDb();
		instance.setCouponInstanceId(instanceExp.getCouponInstanceId());
		instance.setStatusId(StatusConstant.COUPON_INSTANCE_FREEZE.getId());
		instance.setUpdateTime(currentTime);
		instance.setUsedTime(currentTime);
		if(store != null){
			instance.setStoreId(store.getStoreId());
		}
		couponInstanceRepository.updateByPkSelective(instance);
		//扩展类的状态也修改掉
		instanceExp.setStatusId(StatusConstant.COUPON_INSTANCE_FREEZE.getId());
		return instanceExp;
	}

	@Transactional
	@Override
	public void updateCouponInstanceByExchangeCode(Long memberId, String exchangeCode) {
		if(memberId == null || StringUtils.isBlank(exchangeCode)){
			throw new CouponException("非法请求参数");
		}
		CouponInstance couponInstance = couponInstanceRepository.getCouponInstanceInfoByExchangeCode(exchangeCode);
		if(couponInstance == null){
			throw new CouponException("无效的兑换码");
		}
		if(couponInstance.getCouponInstanceOwner() != null ){
			throw new CouponException("该兑换码已失效");
		}

		int count = couponInstanceRepository.updateCouponInstanceByExchangeCode(memberId, exchangeCode);
		if(count == 0){
			throw new CouponException("兑换失败");
		}else{
			sendMsg(memberId, couponInstance);
		}
	}

	private void sendMsg(Long memberId, CouponInstance couponInstance) {
		JSONObject json = null;
		WechatMsgQueueH msgQueueH = null;

		try {
			json = new JSONObject();
			msgQueueH = new WechatMsgQueueH();
			String startDate = DateUtils.dateStrToNewFormat(couponInstance.getStartDate(),
					DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
			String expireDate = DateUtils.dateStrToNewFormat(couponInstance.getExpireDate(),
					DateUtils.DATE_PATTERN_YYYYMMDD, DateUtils.DATE_PATTERN_YYYYMMDD_3);
			json.put("couponName", couponInstance.getCouponName());
			json.put("startDate", startDate);
			json.put("expireDate", expireDate);

			msgQueueH.setBusinessType(WechatMsgBusinessTypeEnum.GET_COUPON_EXCHANGE.getId());
			msgQueueH.setCreateTime(DateUtils.getCurrentTimeOfDb());
			msgQueueH.setMemberId(memberId);
			msgQueueH.setMsgContent(json.toString());
			msgQueueH.setMsgKey(couponInstance.getCouponInstanceCode());
			msgQueueH.setIsSend("N");
			wechatMsgQueueHRepository.save(msgQueueH);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<CouponInstanceExp> queryShopCheckout(String storeCode, String useDate) {
		return this.couponInstanceRepository.queryShopCheckout(storeCode, useDate);
	}
	
	@Autowired
	public void setCouponInstanceRepository(CouponInstanceRepository couponInstanceRepository) {
		this.couponInstanceRepository = couponInstanceRepository;
	}

	@Autowired
	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

	@Autowired
	public void setStoreRepository(StoreRepository storeRepository) {
		this.storeRepository = storeRepository;
	}
	@Autowired
	public void setWechatMsgQueueHRepository(WechatMsgQueueHRepository wechatMsgQueueHRepository) {
		this.wechatMsgQueueHRepository = wechatMsgQueueHRepository;
	}
}
