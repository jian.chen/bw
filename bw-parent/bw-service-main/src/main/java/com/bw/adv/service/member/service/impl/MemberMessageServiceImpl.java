package com.bw.adv.service.member.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.member.message.model.MemberMessage;
import com.bw.adv.module.member.message.model.exp.MemberMessageExp;
import com.bw.adv.module.member.message.repository.MemberMessageRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.member.service.MemberMessageService;

@Service
public class MemberMessageServiceImpl extends BaseServiceImpl<MemberMessage> implements MemberMessageService {
	
	private MemberMessageRepository memberMessageRepository;
	
	


	@Override
	public List<MemberMessage> queryList(Page<MemberMessage> Page) {
		
		return memberMessageRepository.findList(Page);
	}

	@Override
	public int saveObj(MemberMessage memberMessage) {
		String currentTime = null;
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		memberMessage.setCreateBy(1L);
		memberMessage.setCreateTime(currentTime);
		memberMessage.setUpdateBy(1L);
		memberMessage.setUpdateTime(currentTime);
		memberMessage.setStatusId(StatusConstant.MEMBERMESSAGE_NOTREAD.getId());
		return getBaseRepository().save(memberMessage);
	}
	
	@Override
	public int updateMessage(MemberMessage memberMessage) {
		memberMessage.setUpdateBy(1L);
		memberMessage.setUpdateTime(DateUtils.getCurrentTimeOfDb());
		return getBaseRepository().updateByPkSelective(memberMessage);
	}
	
	@Override
	public void updateStatus(Long memberMessageId) {
		memberMessageRepository.updateStatus(memberMessageId);
		
	}
	
	@Override
	public List<MemberMessageExp> queryListByMemberId(Long memberId,
			Page<MemberMessageExp> Page) {
		
		return memberMessageRepository.findListByMemberId(memberId, Page);
	}
	@Override
	public BaseRepository<MemberMessage, ? extends BaseMapper<MemberMessage>> getBaseRepository() {
		
		return memberMessageRepository;
	}

	@Autowired
	public void setMemberMessageRepository(
			MemberMessageRepository memberMessageRepository) {
		this.memberMessageRepository = memberMessageRepository;
	}
	
}
