/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberCardService.java
 * Package Name:MemberGroupServiceImpl
 * Date:2015-11-16下午5:36:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.model.MemberCard;

/**
 * ClassName:MemberCardService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-11-16 下午5:36:15 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberCardService extends BaseService<MemberCard>{

	
	/**
	 * queryByCardNo:(根据卡号查询会员卡). <br/>
	 * Date: 2015-11-16 下午5:43:25 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param cardNo
	 * @return
	 */
	MemberCard queryByCardNo(String cardNo);
	
	/**
	 * queryByMemberId:(查询会员的会员卡). <br/>
	 * Date: 2016-1-13 上午11:01:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberCard queryByMemberId(Long memberId);
	
	
}

