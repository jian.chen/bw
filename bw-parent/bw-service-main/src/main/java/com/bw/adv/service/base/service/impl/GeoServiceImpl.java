/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:GeoServiceImpl.java
 * Package Name:com.sage.scrm.service.base.service.impl
 * Date:2015年8月26日下午4:05:37
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.base.model.Geo;
import com.bw.adv.module.base.repository.GeoRepository;
import com.bw.adv.service.base.service.GeoService;

/**
 * ClassName:GeoServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月26日 下午4:05:37 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class GeoServiceImpl extends BaseServiceImpl<Geo> implements GeoService {
	
	private GeoRepository geoRepository;
	
	@Override
	public BaseRepository<Geo, ? extends BaseMapper<Geo>> getBaseRepository() {
		
		return geoRepository;
	}

	@Override
	public List<Geo> queryByParentId(Long geoId) {
		
		return geoRepository.findByParentId(geoId);
	}

	@Autowired
	public void setGeoRepository(GeoRepository geoRepository) {
		this.geoRepository = geoRepository;
	}

	

}

