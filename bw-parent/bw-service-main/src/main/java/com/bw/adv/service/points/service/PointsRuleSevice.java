/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PointsRuleSevice.java
 * Package Name:com.sage.scrm.service.points.service.impl
 * Date:2015年8月20日下午5:06:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.points.model.PointsRule;

/**
 * ClassName:PointsRuleSevice <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午5:06:16 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface PointsRuleSevice extends BaseService<PointsRule> {
	
	/**
	 * queryPointsRuleList:查询积分规则列表. <br/>
	 * Date: 2015-9-2 上午10:14:19 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param isActive
	 * @param page
	 * @return
	 */
	public List<PointsRule> queryPointsRuleList(String isActive,Page<PointsRule> page);
	

	public int deleteByPk(Long pointsRuleId);
	
	/**
	 * deletePointsRuleByPointsRuleId:删除积分规则(软删除). <br/>
	 * Date: 2015-9-2 上午10:15:45 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param pointsRuleId
	 */
	public void deletePointsRuleByPointsRuleId(Long pointsRuleId);

	/**
	 * queryByPointsTypeId:(根据积分类型查询积分规则). <br/>
	 * Date: 2015-8-26 上午11:34:53 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param pointsTypeId
	 * @return
	 */
	List<PointsRule> queryByPointsTypeId(Long pointsTypeId);

	/**
	 * queryByPointsRuleName:(通过积分规则名称查找). <br/>
	 * Date: 2015-9-22 下午4:11:26 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param isActive
	 * @param pointsRuleName
	 * @return
	 */
	List<PointsRule> queryByPointsRuleName(String isActive,String pointsRuleName); 
}

