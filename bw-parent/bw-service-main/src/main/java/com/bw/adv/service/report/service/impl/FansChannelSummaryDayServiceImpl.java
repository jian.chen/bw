/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:FansChannelSummaryDayServiceImpl.java
 * Package Name:com.sage.scrm.service.report.service.impl
 * Date:2015年11月30日上午10:08:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.report.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.model.FansChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.FansChannelSummaryDayExp;
import com.bw.adv.module.report.repository.FansChannelSummaryDayRepository;
import com.bw.adv.service.report.service.FansChannelSummaryDayService;

/**
 * ClassName:FansChannelSummaryDayServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月30日 上午10:08:09 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class FansChannelSummaryDayServiceImpl extends BaseServiceImpl<FansChannelSummaryDay> 
	implements FansChannelSummaryDayService{
	
	private FansChannelSummaryDayRepository fansChannelSummaryDayRepository;
	
	@Override
	public BaseRepository<FansChannelSummaryDay, ? extends BaseMapper<FansChannelSummaryDay>> getBaseRepository() {
		
		return fansChannelSummaryDayRepository;
	}

	@Override
	public List<FansChannelSummaryDayExp> queryListByExample(Example example,
			Page<FansChannelSummaryDayExp> page) {
		
		return this.fansChannelSummaryDayRepository.findListByExample(example, page);
	}
	

	@Override
	public void callFansChannelSummary(String dateStr) {
		
		this.fansChannelSummaryDayRepository.callFansChannelSummary(dateStr);
	}

	public FansChannelSummaryDayRepository getFansChannelSummaryDayRepository() {
		return fansChannelSummaryDayRepository;
	}

	@Autowired
	public void setFansChannelSummaryDayRepository(
			FansChannelSummaryDayRepository fansChannelSummaryDayRepository) {
		this.fansChannelSummaryDayRepository = fansChannelSummaryDayRepository;
	}

	
	

}

