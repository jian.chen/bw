/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:PrizeInstanceService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2015年11月23日下午2:07:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.PrizeInstance;
import com.bw.adv.module.activity.model.exp.PrizeInstanceExp;

/**
 * ClassName:PrizeInstanceService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月23日 下午2:07:25 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface PrizeInstanceService extends BaseService<PrizeInstance> {
	
	/**
	 * findPrizeInstanceExpByAwardsId:(根据所中奖项抽取奖品实例). <br/>
	 * Date: 2015年11月26日 上午11:01:51 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param awardsId
	 * @return
	 */
	PrizeInstanceExp findPrizeInstanceExpByAwardsId(Long awardsId);
	
}

