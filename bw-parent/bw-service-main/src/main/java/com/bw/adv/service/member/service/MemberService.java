/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberService1.java
 * Package Name:com.sage.scrm.service.member.service
 * Date:2015年8月10日下午3:39:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.model.SqlAttribute;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.model.ExtAccountBinding;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.MemberExtInfo;
import com.bw.adv.module.member.model.MemberSummaryDay;
import com.bw.adv.module.member.model.exp.GradeConvertRecordExp;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.report.model.MemberAge;
import com.bw.adv.module.report.model.MemberRegion;
import com.bw.adv.module.sys.model.ComVerification;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansMessage;


/**
 * ClassName:MemberService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月10日 下午3:39:09 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberService extends BaseService<Member>{
	
	/**
	 * 
	 * queryCountSearchAuto:根据条件查询会员数量. <br/>
	 * Date: 2015年8月11日 下午7:11:16 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	Long queryCountSearchAuto(MemberSearch search);
	
	/**
	 * 查询所有会员数量
	 * Date: 2016年5月5日 下午2:26:23 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @return
	 */
	Long queryActiveMemberCount();
	
	/**
	 * 
	 * queryListSearchAuto:根据条件查询会员列表. <br/>
	 * Date: 2015年8月11日 下午7:12:16 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	List<MemberExp> queryListSearchAuto(MemberSearch search);
	
	
	/**
	 * 偏移量分批查询
	 * selectListSearchAutoByOffset: <br/>
	 * Date: 2016年5月11日 下午4:36:40 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param search
	 * @param offset
	 * @param lastId
	 * @return
	 */
	List<MemberExp> selectListSearchAutoByOffset(MemberSearch search,Long offset,Long lastId);
	
	List<MemberExp> selectListSearchAutoByOffsetNoCondition(MemberSearch search,Long offset,Long lastId);
	
	/**
	 * 
	 * queryEntitySearchAuto:根据条件查询会员详情 <br/>
	 * Date: 2015年8月18日 上午11:52:49 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	MemberExp queryEntitySearchAuto(MemberSearch search);
	
	/**
	 * 
	 * queryEntityPkAuto:查询基本信息 <br/>
	 * Date: 2015年8月11日 下午7:12:34 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberExp queryEntityPkAuto(Long memberId);
	
	/**
	 * 
	 * queryGradeConvertByMemberId:查询等级变动记录 <br/>
	 * Date: 2015年8月11日 下午7:12:50 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<GradeConvertRecordExp> queryGradeConvertByMemberId(Long memberId);
	
	/**
	 * 
	 * queryContactInfoByMemberId:查询联系信息 <br/>
	 * Date: 2015年8月11日 下午7:13:16 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberExp queryContactInfoByMemberId(Long memberId);
	
	/**
	 * 
	 * queryMemberAddressByMemberId:查询联系地址列表 <br/>
	 * Date: 2015年8月11日 下午7:13:32 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<MemberAddress> queryMemberAddressByMemberId(Long memberId);
	
	/**
	 * 
	 * queryMemberDefault:(查询会员默认地址). <br/>
	 * Date: 2015年12月15日 下午12:03:00 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberAddress queryMemberDefault(Long memberId);
	
	/**
	 * 
	 * queryMemberAddressDetail:查询联系地址详情 <br/>
	 * Date: 2015年8月26日 下午6:41:46 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberAddressId
	 * @return
	 */
	MemberAddress queryMemberAddressDetail(Long memberAddressId);
	
	/**
	 * 
	 * insertMemberAddress:联系地址的新增 <br/>
	 * Date: 2015年8月11日 下午7:13:47 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberAddress
	 * @return
	 */
	int insertMemberAddress(MemberAddress memberAddress);
	
	/**
	 * 
	 * updateMemberAddress:联系地址的修改 <br/>
	 * Date: 2015年8月11日 下午7:14:04 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberAddress
	 * @return
	 */
	int updateMemberAddress(MemberAddress memberAddress);
	
	/**
	 * 
	 * deleteMemberAddress:联系地址的删除 <br/>
	 * Date: 2015年8月11日 下午7:14:25 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param id
	 * @return
	 */
	int deleteMemberAddress(Long id);
	
	/**
	 * 
	 * verifyEmail:邮箱验证 <br/>
	 * Date: 2015年8月11日 下午7:14:43 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param email
	 * @return
	 */
	Boolean verifyEmail(String email);
	
	/**
	 * 
	 * queryMemberTagByMemberId:查询会员标签 <br/>
	 * Date: 2015年8月11日 下午7:14:59 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<MemberTag> queryMemberTagByMemberId(Long memberId);
	
	/**
	 * 
	 * queryMemberGroupByMemberId:查询会员分组 <br/>
	 * Date: 2015年8月11日 下午7:15:18 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<MemberGroup> queryMemberGroupByMemberId(Long memberId);
	
	/**
	 * 
	 * queryExtInfoByMemberId:查询其他信息 <br/>
	 * Date: 2015年8月11日 下午7:15:37 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberExtInfo queryExtInfoByMemberId(Long memberId);
	
	/**
	 * 
	 * queryMemberPointsByMemberId:查询积分历史 <br/>
	 * Date: 2015年8月11日 下午7:15:59 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param happenTime
	 * @return
	 */
	List<MemberPointsItemExp> queryMemberPointsByMemberId(MemberSearch search);
	
	/**
	 * 
	 * queryOrderHeaderByMemberId:查询消费记录 <br/>
	 * Date: 2015年8月11日 下午7:16:12 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<OrderHeader> queryOrderHeaderByMemberId(MemberSearch search);
	
	/**
	 * 
	 * queryCouponInstanceByMemberId:查询优惠劵 <br/>
	 * Date: 2015年8月11日 下午7:16:25 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<CouponInstance> queryCouponInstanceByMemberId(MemberSearch search);
	
	/**
	 * 
	 * queryWechatMessageByMemberId:微信交互记录 <br/>
	 * Date: 2015年8月11日 下午7:16:38 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<WechatFansMessage> queryWechatMessageByMemberId(Long memberId);
	
	//呼叫中心记录
	
	/**
	 * 
	 * querySmsInstanceByMemberId:短信发送记录 <br/>
	 * Date: 2015年8月11日 下午7:16:58 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<SmsInstance> querySmsInstanceByMemberId(Long memberId);
	
	/**
	 * 
	 * queryCountMemberPointsByMemberId:查询积分历史总量 <br/>
	 * Date: 2015年8月11日 下午7:15:59 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param happenTime
	 * @return
	 */
	Long queryCountMemberPointsByMemberId(MemberSearch search);
	
	/**
	 * 
	 * queryCountOrderHeaderByMemberId:查询消费记录总量 <br/>
	 * Date: 2015年8月11日 下午7:16:12 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	Long queryCountOrderHeaderByMemberId(MemberSearch search);
	
	/**
	 * 
	 * queryCountCouponInstanceByMemberId:查询优惠劵总量 <br/>
	 * Date: 2015年8月11日 下午7:16:25 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	Long queryCountCouponInstanceByMemberId(MemberSearch search);
	
	/**
	 * 
	 * queryCountWechatMessageByMemberId:微信交互记录总量 <br/>
	 * Date: 2015年8月11日 下午7:16:38 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	Long queryCountWechatMessageByMemberId(Long memberId);
	
	//呼叫中心记录
	
	/**
	 * 
	 * queryCountSmsInstanceByMemberId:短信发送记录总量 <br/>
	 * Date: 2015年8月11日 下午7:16:58 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	Long queryCountSmsInstanceByMemberId(Long memberId);
	
	
	/**
	 * 
	 * insertMember:新增会员基本信息，同时新增会员账户信息 <br/>
	 * Date: 2015年8月12日 下午4:35:08 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param member
	 * @return
	 */
	Long insertMember(Member member);
	
	/**
	 * 
	 * updateMember:编辑会员基本信息 <br/>
	 * Date: 2015年8月12日 下午4:38:00 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param member
	 * @return
	 */
	int updateMember(Member member);
	
	
	/**
	 * 
	 * updateMemberExtAccount:新增或编辑会员联系信息 <br/>
	 * Date: 2015年8月12日 下午4:50:41 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param bindingList
	 * @param memberId
	 * @return
	 */
	int updateMemberExtAccount(List<ExtAccountBinding> bindingList, Long memberId);
	
	/**
	 * 
	 * insertMemberTag:会员添加标签 <br/>
	 * Date: 2015年8月12日 下午4:58:18 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @param memberId
	 * @return
	 */
	int insertMemberTag(String memberTagIds, Long memberId);
	
	/**
	 * 
	 * deleteMemberTag:会员删除标签 <br/>
	 * Date: 2015年8月12日 下午4:58:43 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @param memberId
	 * @return
	 */
	int deleteMemberTag(Long memberTagId, Long memberId);
	
	/**
	 * 
	 * insertMemberGroup:会员添加分组 <br/>
	 * Date: 2015年8月12日 下午4:59:13 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @param memberId
	 * @return
	 */
	int insertMemberGroup(String memberGroupIds, Long memberId);
	
	/**
	 * 
	 * deleteMemberGroup:会员删除分组 <br/>
	 * Date: 2015年8月12日 下午4:59:35 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @param memberId
	 * @return
	 */
	int deleteMemberGroup(Long memberGroupId, Long memberId);
	
	/**
	 * 
	 * insertMemberExtInfo:新增会员扩展信息 <br/>
	 * Date: 2015年8月12日 下午5:04:17 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberExtInfo
	 * @return
	 */
	int insertMemberExtInfo(MemberExtInfo memberExtInfo);
	
	/**
	 * 
	 * insertMemberExtInfo:修改会员扩展信息 <br/>
	 * Date: 2015年8月12日 下午5:04:17 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberExtInfo
	 * @return
	 */
	int updateMemberExtInfo(MemberExtInfo memberExtInfo);

	
	/**
	 * registMemberInfo:(会员注册). <br/>
	 * Date: 2016-4-22 下午10:15:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	public Map<String, Object> registMemberInfo(MemberDto memberDto);
	/**
	 * saveMemberInfo:(会员保存). <br/>
	 * Date:  <br/>
	 * scrmVersion 1.0
	 * @author chenjian
	 * @version jdk1.7
	 * @param wechatFans
	 * @return
	 */
	public Map<String, Object> saveMemberInfo(WechatFans wechatFans);

	
	/**
	 * bindingMember:(绑定会员). <br/>
	 * Date: 2016-4-7 下午8:26:20 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberDto
	 * @return
	 */
	public Member bindingMember(MemberDto memberDto);
	
	/**
	 * registMemberNoWechat:(注册（无微信）并开卡). <br/>
	 * Date: 2015-9-15 下午2:56:09 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param member
	 * @param extAccountTypeId
	 * @param account
	 * @param fans
	 * @return
	 */
	Member registMemberNoWechat(Member member, String extAccountTypeId, String account)throws Exception;

	/**
	 * 
	 * queryMemberByBindingAccount:根据外部账号查询会员信息 <br/>
	 * Date: 2015年8月19日 上午11:43:00 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param extAccountTypeId
	 * @param bindingAccount
	 * @return
	 */
	Member queryMemberByBindingAccount(String extAccountTypeId,
			String bindingAccount);

	/**
	 * 
	 * queryMemberAccountByMemberId:根据会员id查询会员账户信息 <br/>
	 * Date: 2015年8月19日 上午11:53:34 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberAccount queryMemberAccountByMemberId(Long memberId);

	/**
	 * 
	 * updateMemberTagAndGroup: 更新会员的标签和分组<br/>
	 * Date: 2015年8月25日 上午10:29:01 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param memberTagIds
	 * @param memberGroupIds
	 */
	void updateMemberTagAndGroup(Long memberId, String memberTagIds,
			String memberGroupIds);

	/**
	 * 
	 * checkMemberSql:会员sql校验 <br/>
	 * Date: 2015年8月28日 上午11:23:56 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param sql
	 */
	void checkMemberSql(String sql);

	/**
	 * 
	 * querySqlAttrList:查询sql编辑器的属性列表 <br/>
	 * Date: 2015年8月28日 上午11:56:39 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param type
	 */
	List<SqlAttribute> querySqlAttrList(Long type);

	void callMemberSummaryDay(String dateStr,String weekStr,String monthStr);
	/**
	 * queryMemberCardByMemberId:(通过memberId查询会员卡信息). <br/>
	 * Date: 2015-10-28 上午11:36:45 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberCard queryMemberCardByMemberId(Long memberId);
	
	 /**
     * 根据日期查找统计结果
     * @param date
     * @return
     */
    MemberSummaryDay selectBySummaryDate(String date);

    /**
     * 
     * insertComVerificationRepository:(新增一条验证码信息). <br/>
     * Date: 2015年11月11日 上午10:56:08 <br/>
     * scrmVersion 1.0
     * @author chuanxue.wei
     * @version jdk1.7
     * @param mobile
     * @return
     */
    void insertComVerification(ComVerification comVerification) throws Exception;
    
    /**
     * 
     * updateComVerification:(修改验证码信息). <br/>
     * Date: 2015年11月11日 上午11:15:42 <br/>
     * scrmVersion 1.0
     * @author chuanxue.wei
     * @version jdk1.7
     * @return
     */
    void updateComVerification(ComVerification comVerification);
    
    /**
     * 
     * findComvByExample:(根据查询条件查询验证码信息). <br/>
     * Date: 2015年11月11日 上午11:42:20 <br/>
     * scrmVersion 1.0
     * @author chuanxue.wei
     * @version jdk1.7
     * @param example
     * @return
     */
    List<ComVerification> findComvByExample(Example example);
    
    /**
     * 查询用户消费次数
     * @param times
     * @return
     */
    Long findMemberOrderCount(Long times1,Long times2,Long sort);
    
    /**
     * 查询用户地区分部
     * @param page
     * @return
     */
    List<MemberRegion> findMemberRegion(Page<MemberRegion> page,String sort,String order);
    
    /**
     * 查询当前所有会员数量
     * @return
     */
    Long findAllMemberCount();
    
    /**
     * 查询会员年龄日报
     * @param startDate
     * @param endDate
     * @return
     */
    List<MemberAge> findMemberAge(String startDate,String endDate);
    
    /**
     * 
     * activation:(微信会员激活). <br/>
     * Date: 2015年12月10日 下午4:58:55 <br/>
     * scrmVersion 1.0
     * @author chuanxue.wei
     * @version jdk1.7
     * @param memberDto
     * @return
     */
    ResultStatus activation(MemberDto memberDto,String couponCode);
    
    
    /**
     * 
     * wechatUserEdit:(微信 会员个人信息修改). <br/>
     * Date: 2015年12月14日 下午9:35:53 <br/>
     * scrmVersion 1.0
     * @author chuanxue.wei
     * @version jdk1.7
     * @param memberDto
     */
    void wechatUserEdit(MemberExp memberExp);
    
    
    /**
     * queryExpByMemberId:(根据会员ID查询会员信息). <br/>
     * Date: 2015-12-30 下午1:05:53 <br/>
     * scrmVersion 1.0
     * @author mennan
     * @version jdk1.7
     * @param memberId
     * @return
     */
    MemberExp queryExpByMemberId(Long memberId);
    
    /**
     * 
     * queryMemberByCode:(根据会员编号查询会员). <br/>
     * Date: 2016年1月9日 上午1:39:39 <br/>
     * scrmVersion 1.0
     * @author yu.zhang
     * @version jdk1.7
     * @param memberCode
     * @return
     */
    Member queryMemberByCode(String memberCode);

	/**
	 * 根据手机号查询会员扩展信息
	 * @param code
	 * @param queryType
	 * @return
	 */
	MemberExp findMemberExpByCode(String code, String queryType);
	
	/**
	 * 注册完成后的积分操作
	 * @param memberId
	 */
	void pointsAfterRegister(Long memberId);
	
	/**
	 * 查询会员累计积分余额
	 * @param member
	 * @return
	 */
	BigDecimal upGrade(Member member);

	/**
	 * 手动绑定黑卡
	 */
    void  bindBlackCardManually(Member member, String cardNo) throws Exception;
}

