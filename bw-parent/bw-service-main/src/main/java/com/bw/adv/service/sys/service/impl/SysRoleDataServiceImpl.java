package com.bw.adv.service.sys.service.impl;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.sys.enums.SysRoleDataFilterType;
import com.bw.adv.module.sys.model.SysRoleData;
import com.bw.adv.module.sys.model.SysRoleDataType;
import com.bw.adv.module.sys.model.exp.RoleDataConfigExp;
import com.bw.adv.module.sys.model.exp.RoleDataRequest;
import com.bw.adv.module.sys.repository.SysRoleDataRepository;
import com.bw.adv.module.sys.repository.SysRoleDataTypeRepository;
import com.bw.adv.service.sys.service.SysRoleDataService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
@Service
public class SysRoleDataServiceImpl extends BaseServiceImpl<SysRoleData> implements SysRoleDataService {

    private SysRoleDataRepository sysRoleDataRepository;
    private SysRoleDataTypeRepository sysRoleDataTypeRepository;
    private Map<Integer, SysRoleDataType> dataTypeMap;

    @PostConstruct
    private void init() {
        getCacheDataTypeMap();
    }

    @Override
    public BaseRepository<SysRoleData, ? extends BaseMapper<SysRoleData>> getBaseRepository() {
        return this.sysRoleDataRepository;
    }

    @Override
    public List<SysRoleData> queryByRoleId(Long roleId) {
        return this.sysRoleDataRepository.queryByRoleId(roleId);
    }

    @Override
    public List<SysRoleData> queryByUserId(Long userId) {
        return this.sysRoleDataRepository.queryByUserId(userId);
    }

    @Override
    public List<SysRoleDataType> queryAllDataType() {
        return this.sysRoleDataTypeRepository.findAll();
    }

    @Override
    public Map<Integer, SysRoleDataType> getCacheDataTypeMap() {
        if(MapUtils.isEmpty(this.dataTypeMap)) {
            List<SysRoleDataType> dataTypes = this.queryAllDataType();
            this.dataTypeMap = new HashMap<Integer, SysRoleDataType>();
            for(SysRoleDataType sysRoleDataType : dataTypes) {
                dataTypeMap.put(sysRoleDataType.getDataTypeId(), sysRoleDataType);
            }
        }
        return this.dataTypeMap;
    }

    @Override
    public Map<Integer, SysRoleDataType> getNoCacheDataTypeMap() {
        List<SysRoleDataType> dataTypes = this.queryAllDataType();
        if(CollectionUtils.isEmpty(dataTypes)) {
            this.dataTypeMap = Maps.newHashMap();
            return this.dataTypeMap;
        }
        Map<Integer, SysRoleDataType> tmpMap = Maps.newHashMap();
        for(SysRoleDataType sysRoleDataType : dataTypes) {
            tmpMap.put(sysRoleDataType.getDataTypeId(), sysRoleDataType);
        }
        this.dataTypeMap = tmpMap;
        return this.dataTypeMap;
    }

    @Override
    public List<RoleDataConfigExp> getRoleDataConfig(Long roleId) {
        Map<Integer, SysRoleDataType> map = getCacheDataTypeMap();
        List<RoleDataConfigExp> result = Lists.newArrayList();
        Map<Integer, SysRoleData> roleDataMap = null;
        if(roleId != null && roleId > 0) {
            List<SysRoleData> roleDatas = queryByRoleId(roleId);
            if(CollectionUtils.isNotEmpty(roleDatas)) {
                roleDataMap = Maps.newHashMap();
                for(SysRoleData sysRoleData : roleDatas) {
                    roleDataMap.put(sysRoleData.getDataTypeId(), sysRoleData);
                }
            }

        }
        
        for(SysRoleDataType sysRoleDataType : map.values()) {
            RoleDataConfigExp roleDataConfigExp = new RoleDataConfigExp(sysRoleDataType);
            roleDataConfigExp.setConfigFields(Lists.newArrayList(sysRoleDataType.getFieldName().split(",")));
            SysRoleData sysRoleData = null;
            if(roleDataMap != null
                    && (sysRoleData = roleDataMap.get(sysRoleDataType.getDataTypeId())) != null
                    && StringUtils.isNotBlank(sysRoleData.getOperationParams())) {
                roleDataConfigExp.setUnDisplayFields(Lists.newArrayList(sysRoleData.getOperationParams().split(",")));
            }else {
                roleDataConfigExp.setUnDisplayFields(Lists.<String>newArrayList());
            }
            result.add(roleDataConfigExp);
        }
        return result;
    }

    @Override
    public void saveRoleDataConfig(RoleDataRequest roleDataRequest) {
        if(roleDataRequest == null)
            throw new IllegalArgumentException("convert argument[roleDataRequest] is null");
        if(MapUtils.isEmpty(roleDataRequest.getConfigMap())){
        	this.sysRoleDataRepository.deleteByRoleId(roleDataRequest.getRoleId());
        	return;
        }
        Map<Integer, List<String>> configMap = roleDataRequest.getConfigMap();
        List<SysRoleData> roleDatas = Lists.newArrayListWithExpectedSize(configMap.size());
        for(Integer dataTypeId : configMap.keySet()) {
            List<String> params = configMap.get(dataTypeId);
            if(CollectionUtils.isEmpty(params))
                throw new IllegalArgumentException("convert argument[roleDataRequest.configMap.params] is null");
            SysRoleData sysRoleData = new SysRoleData();
            sysRoleData.setRoleId(roleDataRequest.getRoleId());
            sysRoleData.setFilterTypeId(SysRoleDataFilterType.NOT_DISPLAY.getKey());
            sysRoleData.setOperationParams(StringUtils.join(params, ","));
            sysRoleData.setStatus(1);
            sysRoleData.setDataTypeId(dataTypeId);
            roleDatas.add(sysRoleData);
        }
        // 先删除所有权限所有该角色的数据权限配置，然后再新增
        this.sysRoleDataRepository.deleteByRoleId(roleDataRequest.getRoleId());
        this.sysRoleDataRepository.batchInsert(roleDatas);
    }

    @Autowired
    public void setSysRoleDataRepository(SysRoleDataRepository sysRoleDataRepository) {
        this.sysRoleDataRepository = sysRoleDataRepository;
    }

    @Autowired
    public void setSysRoleDataTypeRepository(SysRoleDataTypeRepository sysRoleDataTypeRepository) {
        this.sysRoleDataTypeRepository = sysRoleDataTypeRepository;
    }
}
