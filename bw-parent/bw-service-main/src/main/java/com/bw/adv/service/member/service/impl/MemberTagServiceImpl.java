package com.bw.adv.service.member.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.api.member.dto.MemberDto;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.constant.ActivityConstant;
import com.bw.adv.module.activity.model.ActivityTagAction;
import com.bw.adv.module.activity.repository.ActivityTagActionRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.coupon.repository.CouponInstanceRepository;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.repository.MemberAccountRepository;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.model.MemberTagCondition;
import com.bw.adv.module.member.tag.model.MemberTagItem;
import com.bw.adv.module.member.tag.repository.MemberTagConditionRepository;
import com.bw.adv.module.member.tag.repository.MemberTagItemRepository;
import com.bw.adv.module.member.tag.repository.MemberTagRepository;
import com.bw.adv.module.member.tag.search.MemberTagItemSearch;
import com.bw.adv.module.member.tag.search.MemberTagSearch;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.order.repository.OrderHeaderRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.member.service.MemberTagService;

import net.sf.json.JSONObject;

@Service
public class MemberTagServiceImpl extends BaseServiceImpl<MemberTag> implements MemberTagService {
	
	private static final String NO_COUNT = "N";

	private MemberRepository memberRepository;
	
	private MemberTagRepository memberTagRepository;
	
	private MemberTagItemRepository memberTagItemRepository;
	
	private MemberTagConditionRepository memberTagConditionRepository;
	
	private ActivityTagActionRepository activityTagActionRepository;
	
	private MemberAccountRepository memberAccountRepository;
	
	private CouponInstanceRepository couponInstanceRepository;
	
	private OrderHeaderRepository orderHeaderRepository;
	
	@Override
	public Long queryCountSearchAuto(MemberTagSearch search) {
		return this.memberTagRepository.findCountSearchAuto(search);
	}
	
	@Override
	public List<MemberTag> queryListSearchAuto(MemberTagSearch search) {
		return this.memberTagRepository.findListSearchAuto(search);
	}
	
	@Override
	public MemberTag queryEntityPkAuto(Long memberTagId) {
		return this.memberTagRepository.findByPk(memberTagId);
	}
	
	@Override
	@Transactional
	public int insertMemberTag(MemberTag memberTag) {
		
		return this.memberTagRepository.saveSelective(memberTag);
	}
	
	@Override
	@Transactional
	public int updateMemberTag(MemberTag memberTag) {
		
		return this.memberTagRepository.updateByPkSelective(memberTag);
	}
	
	@Override
	@Transactional
	public void saveAllMemberToTag(MemberTag memberTag) {
		this.memberTagItemRepository.deleteTagItemByTagId(memberTag.getMemberTagId());
		this.memberTagRepository.updateByPkSelective(memberTag);
	}
	
	@Override
	@Transactional
	public int deleteteMemberTagById(Long memberTagId) {
		this.memberTagItemRepository.deleteTagItemByTagId(memberTagId);
		this.memberTagRepository.removeByPk(memberTagId);
		return 0;
	}
	
	@Override
	@Transactional
	public void deleteMemberTagItem(Long memberTagId, Long memberId) {
		MemberTagItemSearch search = null;
		MemberTagItem item = null;
		search = new MemberTagItemSearch();
		search.setEqualMemberTagId(memberTagId);
		search.setEqualMemberId(memberId);
		item = memberTagItemRepository.findEntitySearchAuto(search);
		if(item != null){
			memberTagItemRepository.removeByPk(item.getMemberTagItemId());
		}
	}
	
	@Override
	@Transactional
	public Long insertBatchMemberTagItem(Long memberTagId,String memberIds) {
		MemberTagItemSearch search = null;
		//已经存在的会员
		List<MemberTagItem> itemList = null;
		//新增的会员列表
		List<MemberTagItem> addItemList = null;
		String[] memberIdArr = null;
		MemberTagItem item = null;
		String currentTime = null;
		
		
		
		if(StringUtils.isNotBlank(memberIds) && memberTagId != null){
			//先查询标签下面已有会员
			search = new MemberTagItemSearch();
			search.setEqualMemberTagId(memberTagId);
			itemList = this.memberTagItemRepository.findListSearchAuto(search);
			currentTime = DateUtils.getCurrentTimeOfDb();
			
			//新添加的会员数据和标签已有会员数据去重（重写MemberTagItem的equal方法）
			memberIdArr = memberIds.split(",");
			addItemList = new ArrayList<MemberTagItem>();
			for(String memberId : memberIdArr){
				item = new MemberTagItem();
				item.setMemberId(Long.valueOf(memberId));
				item.setMemberTagId(memberTagId);
				item.setCreateTime(currentTime);
				addItemList.add(item);
			}
			addItemList.removeAll(itemList);
			
			//执行批量查询
			if(addItemList.size()>0){
				this.memberTagItemRepository.insertBatchMemberTagItem(addItemList);
			}
		}
		return (long) (addItemList == null ? 0 :addItemList.size());
		
	}
	
	@Override
	@Transactional
	public Long excuteMemberTagBySql(Long memberTagId) {
		List<Long> list = null;
		MemberTag tag = null;
		MemberTagItem item = null;
		String sql = null;
		//新增的会员列表
		List<MemberTagItem> addItemList = null;
		
		tag = this.memberTagRepository.findByPk(memberTagId);
		if(tag != null){
			sql = tag.getConditionSql();
		}
		list = this.memberRepository.findListBySql(sql);
		
		
		if(list!= null && list.size()>0 && memberTagId != null){
			//先删除标签下面已有会员
			this.memberTagItemRepository.deleteTagItemByTagId(memberTagId);
			addItemList = new ArrayList<MemberTagItem>();
			for(Long memberId : list){
				item = new MemberTagItem();
				item.setMemberId(Long.valueOf(memberId));
				item.setMemberTagId(memberTagId);
				addItemList.add(item);
			}
			//执行批量查询
			if(addItemList.size()>0){
				this.memberTagItemRepository.insertBatchMemberTagItem(addItemList);
			}
		}
		return (long) (addItemList == null ? 0 :addItemList.size());
	}
	
	@Override
	@Transactional
	public void excuteMemberTagJob(MemberTag tag) {
		List<Long> list = null;
		MemberTagItem item = null;
		String sql = null;
		//新增的会员列表
		List<MemberTagItem> addItemList = null;
		sql = tag.getConditionSql();
		if(sql != null && !"".equals(sql)){
			list = this.memberRepository.findListBySql(sql);
		}
		Long memberTagId = tag.getMemberTagId();
		if(list!= null && list.size()>0 && memberTagId != null){
			//先删除标签下面已有会员
			this.memberTagItemRepository.deleteTagItemByTagId(memberTagId);
			addItemList = new ArrayList<MemberTagItem>();
			for(Long memberId : list){
				item = new MemberTagItem();
				item.setMemberId(Long.valueOf(memberId));
				item.setMemberTagId(memberTagId);
				addItemList.add(item);
			}
			//执行批量查询
			if(addItemList.size()>0){
				this.memberTagItemRepository.insertBatchMemberTagItem(addItemList);
			}
		}
	}
	
	@Override
	public void deleteMemberTagItemByTagId(Long memberTagId) {
		this.memberTagItemRepository.deleteTagItemByTagId(memberTagId);
	}

	@Override
	public List<MemberExp> queryMemberListOfTag(MemberSearch search) {

		return this.memberRepository.findListSearchAuto(search);
	}
	
	@Override
	public Long queryCountMemberListOfTag(MemberSearch search) {
		
		return this.memberRepository.findCountSearchAuto(search);
	}
	
	public MemberRepository getMemberRepository() {
		return memberRepository;
	}
	
	@Override
	public void saveTagCondition(List<MemberTagCondition> conditionList) {
		this.memberTagConditionRepository.saveList(conditionList);
	}
	
	@Override
	public void updateTagCondition(List<MemberTagCondition> conditionList,Long memberTagId) {
		this.memberTagConditionRepository.deleteConditionByTagId(memberTagId);
		this.memberTagConditionRepository.saveList(conditionList);
	}
	
	@Override
	public void deleteConditionByTagId(Long memberTagId){
		this.memberTagConditionRepository.deleteConditionByTagId(memberTagId);
	}
	
	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}
	
	@Override
	public BaseRepository<MemberTag, ? extends BaseMapper<MemberTag>> getBaseRepository() {
		return  memberTagRepository;
	}
	
	@Autowired
	public void setMemberTagRepository(MemberTagRepository memberTagRepository) {
		this.memberTagRepository = memberTagRepository;
	}
	
	public MemberTagItemRepository getMemberTagItemRepository() {
		return memberTagItemRepository;
	}
	
	@Autowired
	public void setMemberTagItemRepository(
			MemberTagItemRepository memberTagItemRepository) {
		this.memberTagItemRepository = memberTagItemRepository;
	}
	
	@Autowired
	public void setMemberTagConditionRepository(MemberTagConditionRepository memberTagConditionRepository) {
		this.memberTagConditionRepository = memberTagConditionRepository;
	}

	@Override
	public List<MemberTag> findTagByTypeAndNode(String type, String node) {
		return this.memberTagRepository.findTagByTypeAndNode(type, node);
	}

	@Override
	public List<MemberTagCondition> findConditionByTagId(Long tagId) {
		return this.memberTagConditionRepository.findConditionByTagId(tagId);
	}
	@Autowired
	public void setActivityTagActionRepository(ActivityTagActionRepository activityTagActionRepository) {
		this.activityTagActionRepository = activityTagActionRepository;
	}
	
	@Autowired
	public void setMemberAccountRepository(MemberAccountRepository memberAccountRepository) {
		this.memberAccountRepository = memberAccountRepository;
	}
	
	@Autowired
	public void setCouponInstanceRepository(CouponInstanceRepository couponInstanceRepository) {
		this.couponInstanceRepository = couponInstanceRepository;
	}
	
	@Autowired
	public void setOrderHeaderRepository(OrderHeaderRepository orderHeaderRepository) {
		this.orderHeaderRepository = orderHeaderRepository;
	}

	@Override
	public void setTagManel(Long memberTagId) {
		Map<String,Object> param = new HashMap<String, Object>();
		List<Long> selectTypeList = new ArrayList<Long>();
		List<MemberTagItem> addItemList = null; //新增的会员列表
		MemberTagItem item = null;
		//先把这个标签状态改为执行中
		MemberTag memberTag = new MemberTag();
		memberTag.setMemberTagId(memberTagId);
		memberTag.setStatusId(StatusConstant.MEMBER_TAG_SETTING.getId());
		this.updateByPkSelective(memberTag);
		
		List<MemberTagCondition> tagCondition = this.memberTagConditionRepository.findConditionByTagId(memberTagId);
		if(tagCondition.size()>0){
			for (MemberTagCondition memberTagCondition : tagCondition) {
				param.put(memberTagCondition.getDistinctConditionRule(), memberTagCondition.getDistinctConditionValue());
				selectTypeList.add(memberTagCondition.getDistinctConditionId());
			}
		}
		
		List<Member> memberList = this.memberTagConditionRepository.findMemberIdByConditionMember(param,selectTypeList);
		
		
		if(memberList.size()>0){
			
			addItemList = new ArrayList<MemberTagItem>();
			for(Member member : memberList){
				item = new MemberTagItem();
				item.setMemberId(member.getMemberId());
				item.setMemberTagId(memberTagId);
				item.setCreateTime(DateUtils.getCurrentTimeOfDb());
				addItemList.add(item);
			}
			
			//执行批量插入
			if(addItemList.size()>0){
				this.memberTagItemRepository.insertMemberTagItemIgnoreSame(addItemList);
			}
		}
		
		MemberTag tag = new MemberTag();
		tag.setMemberTagId(memberTagId);
		tag.setStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_USE.getId());
		memberTagRepository.updateByPkSelective(tag);
	}

	@Override
	public void structureTagAopNewMember(MemberDto memberDto) {
		List<MemberTag> registerTag = this.findTagByTypeAndNode("2", "1");
		if(registerTag.size()>0){
			List<ActivityTagAction> tagActionList = new ArrayList<ActivityTagAction>();
			JSONObject tagParams = new JSONObject();
			for (MemberTag memberTag : registerTag) {
				List<MemberTagCondition> tagCondition = this.findConditionByTagId(memberTag.getMemberTagId());
				ActivityTagAction tagAction = new ActivityTagAction();
				for (MemberTagCondition memberTagCondition : tagCondition) {
					tagParams.put("tagId", memberTagCondition.getMemberTagId());
					if(memberTagCondition.getDistinctConditionRule().equals("mobile")){
						tagParams.put("mobile", memberDto.getMobile().toString());
						tagParams.put("mobileCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("gender")){
						tagParams.put("gender", (memberDto.getGender()+"").toString());
						tagParams.put("genderCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("age")){
						if(StringUtils.isNotBlank(memberDto.getBirthday())){
							tagParams.put("age", (Long.parseLong(DateUtils.getCurrentDateOfDb().substring(0, 4)) - Long.parseLong(memberDto.getBirthday().substring(0, 4)))+"");
						}else{
							tagParams.put("age","-1");
						}
						tagParams.put("ageCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("birthday")){
						tagParams.put("birthday", memberDto.getBirthday());
						tagParams.put("birthdayCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("register")){
						tagParams.put("register", memberDto.getRegisterTime().substring(0, 8));
						tagParams.put("registerCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("channel")){
						if(memberDto.getChannelId() != null){
							tagParams.put("channel", memberDto.getGradeId().toString());
						}else{
							tagParams.put("channel", "0");
						}
						tagParams.put("channelCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("grade")){
						if(memberDto.getGradeId() != null){
							tagParams.put("grade", memberDto.getGradeId().toString());
						}else{
							tagParams.put("grade", "0");
						}
						tagParams.put("gradeCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
				}
				tagAction.setMemberId(memberDto.getMemberId());
				tagAction.setActivityCode(ActivityConstant.TAG_NEW_MEMBER.getCode());
				tagAction.setRuleOptParams(tagParams.toString());
				tagAction.setHappenTime(DateUtils.getCurrentTimeOfDb());
				tagAction.setCreateTime(DateUtils.getCurrentTimeOfDb());
				tagAction.setIsCount(NO_COUNT);
				tagActionList.add(tagAction);
			}
			activityTagActionRepository.saveList(tagActionList);
		}
	}

	@Override
	public void structureTagAopUpdateMember(MemberDto memberDto) {
		Member member = memberRepository.findByPk(memberDto.getMemberId());
		List<MemberTag> registerTag = this.findTagByTypeAndNode("2", "2");
		if(registerTag.size()>0){
			List<ActivityTagAction> tagActionList = new ArrayList<ActivityTagAction>();
			JSONObject tagParams = new JSONObject();
			for (MemberTag memberTag : registerTag) {
				List<MemberTagCondition> tagCondition = this.findConditionByTagId(memberTag.getMemberTagId());
				ActivityTagAction tagAction = new ActivityTagAction();
				for (MemberTagCondition memberTagCondition : tagCondition) {
					tagParams.put("tagId", memberTagCondition.getMemberTagId());
					//基本资料
					if(memberTagCondition.getDistinctConditionRule().equals("mobile")){
						tagParams.put("mobile", member.getMobile().toString());
						tagParams.put("mobileCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("gender")){
						tagParams.put("gender", (member.getGender()+"").toString());
						tagParams.put("genderCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("age")){
						if(StringUtils.isNotBlank(member.getBirthday())){
							tagParams.put("age", (Long.parseLong(DateUtils.getCurrentDateOfDb().substring(0, 4)) - Long.parseLong(member.getBirthday().substring(0, 4)))+"");
						}else{
							tagParams.put("age","-1");
						}
						tagParams.put("ageCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("birthday")){
						tagParams.put("birthday", member.getBirthday());
						tagParams.put("birthdayCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("register")){
						tagParams.put("register", member.getRegisterTime().substring(0, 8));
						tagParams.put("registerCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("channel")){
						if(member.getChannelId() != null){
							tagParams.put("channel", member.getGradeId().toString());
						}else{
							tagParams.put("channel", "0");
						}
						tagParams.put("channelCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("grade")){
						if(member.getGradeId() != null){
							tagParams.put("grade", member.getGradeId().toString());
						}else{
							tagParams.put("grade", "0");
						}
						tagParams.put("gradeCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("orderLast")){
						if(StringUtils.isNotBlank(member.getLastConsumptionTime())){
							tagParams.put("orderLast", member.getLastConsumptionTime().substring(0, 8));
						}else{
							tagParams.put("orderLast", "0");
						}
						String lastStart = DateUtils.addDays(0 - Integer.parseInt(memberTagCondition.getDistinctConditionValue().split(",")[1]));
						String lastEnd = DateUtils.addDays(0 - Integer.parseInt(memberTagCondition.getDistinctConditionValue().split(",")[0]));
						tagParams.put("orderLastCondition", lastStart+","+lastEnd);
						continue;
					}
					//会员资料
					if(memberTagCondition.getDistinctConditionRule().equals("points")){
						MemberAccount memberAccount = memberAccountRepository.findByMemberId(member.getMemberId());
						tagParams.put("points", memberAccount.getPointsBalance());
						tagParams.put("pointsCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("couponFree")){
						Long count = couponInstanceRepository.selectInstanceCountByMemberIdAndStatusId(member.getMemberId(), StatusConstant.COUPON_INSTANCE_UNUSED.getId());
						tagParams.put("couponFree", count);
						tagParams.put("couponFreeCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("couponUsed")){
						Long count = couponInstanceRepository.selectInstanceCountByMemberIdAndStatusId(member.getMemberId(), StatusConstant.COUPON_INSTANCE_USED.getId());
						tagParams.put("couponUsed", count);
						tagParams.put("couponUsedCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					//交易数据
					if(memberTagCondition.getDistinctConditionRule().equals("orderStore")){
						List<OrderHeader> orderHeaderList = orderHeaderRepository.selectOrderHeaderByMemberIdForStoreId(member.getMemberId(),memberTagCondition.getDistinctConditionValue());
						tagParams.put("orderStore", orderHeaderList.size()+"");
						tagParams.put("orderStoreCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("orderCount")){
						String startTime = memberTagCondition.getDistinctConditionValue().split(",")[0];
						String endTime = memberTagCondition.getDistinctConditionValue().split(",")[1];
						Long countStart = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[2]);
						Long countEnd = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[3]);
						List<OrderHeader> orderHeaderList = orderHeaderRepository.selectOrderCount(member.getMemberId(), startTime, endTime, countStart, countEnd);
						tagParams.put("orderCount", orderHeaderList.size()+"");
						tagParams.put("orderCountCondition", memberTagCondition.getDistinctConditionValue());
					}
					if(memberTagCondition.getDistinctConditionRule().equals("orderGrand")){
						String startTime = memberTagCondition.getDistinctConditionValue().split(",")[0];
						String endTime = memberTagCondition.getDistinctConditionValue().split(",")[1];
						Long sumStart = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[2]);
						Long sumEnd = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[3]);
						List<OrderHeader> orderHeaderList = orderHeaderRepository.selectOrderGrand(member.getMemberId(), startTime, endTime, sumStart, sumEnd);
						tagParams.put("orderGrand", orderHeaderList.size()+"");
						tagParams.put("orderGrandCondition", memberTagCondition.getDistinctConditionValue());
					}
					if(memberTagCondition.getDistinctConditionRule().equals("orderOne")){
						String startTime = memberTagCondition.getDistinctConditionValue().split(",")[0];
						String endTime = memberTagCondition.getDistinctConditionValue().split(",")[1];
						Long oneStart = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[2]);
						Long oneEnd = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[3]);
						List<OrderHeader> orderHeaderList = orderHeaderRepository.selectOrderOne(member.getMemberId(), startTime, endTime, oneStart, oneEnd);
						tagParams.put("orderOne", orderHeaderList.size()+"");
						tagParams.put("orderOneCondition", memberTagCondition.getDistinctConditionValue());
					}
				}
				tagAction.setMemberId(member.getMemberId());
				tagAction.setActivityCode(ActivityConstant.TAG_UPDATE_MEMBER.getCode());
				tagAction.setRuleOptParams(tagParams.toString());
				tagAction.setHappenTime(DateUtils.getCurrentTimeOfDb());
				tagAction.setCreateTime(DateUtils.getCurrentTimeOfDb());
				tagAction.setIsCount(NO_COUNT);
				tagActionList.add(tagAction);
			}
			activityTagActionRepository.saveList(tagActionList);
		}
	}
	
	@Override
	public void structureTagAopHandleOrder(MemberExp memberExp,OrderInfo orderInfo) {
		Member member = memberRepository.findByPk(memberExp.getMemberId());
		List<MemberTag> registerTag = this.findTagByTypeAndNode("2", "3");
		if(registerTag.size()>0){
			List<ActivityTagAction> tagActionList = new ArrayList<ActivityTagAction>();
			JSONObject tagParams = new JSONObject();
			for (MemberTag memberTag : registerTag) {
				List<MemberTagCondition> tagCondition = this.findConditionByTagId(memberTag.getMemberTagId());
				ActivityTagAction tagAction = new ActivityTagAction();
				for (MemberTagCondition memberTagCondition : tagCondition) {
					tagParams.put("tagId", memberTagCondition.getMemberTagId());
					//基本资料
					if(memberTagCondition.getDistinctConditionRule().equals("mobile")){
						tagParams.put("mobile", member.getMobile().toString());
						tagParams.put("mobileCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("gender")){
						tagParams.put("gender", (member.getGender()+"").toString());
						tagParams.put("genderCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("age")){
						if(StringUtils.isNotBlank(member.getBirthday())){
							tagParams.put("age", (Long.parseLong(DateUtils.getCurrentDateOfDb().substring(0, 4)) - Long.parseLong(member.getBirthday().substring(0, 4)))+"");
						}else{
							tagParams.put("age","-1");
						}
						tagParams.put("ageCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("birthday")){
						tagParams.put("birthday", member.getBirthday());
						tagParams.put("birthdayCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("register")){
						tagParams.put("register", member.getRegisterTime().substring(0, 8));
						tagParams.put("registerCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("channel")){
						if(member.getChannelId() != null){
							tagParams.put("channel", member.getGradeId().toString());
						}else{
							tagParams.put("channel", "0");
						}
						tagParams.put("channelCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("grade")){
						if(member.getGradeId() != null){
							tagParams.put("grade", member.getGradeId().toString());
						}else{
							tagParams.put("grade", "0");
						}
						tagParams.put("gradeCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("orderLast")){
						tagParams.put("orderLast", member.getLastConsumptionTime().substring(0, 8));
						tagParams.put("orderLastCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					//会员资料
					if(memberTagCondition.getDistinctConditionRule().equals("points")){
						MemberAccount memberAccount = memberAccountRepository.findByMemberId(member.getMemberId());
						tagParams.put("points", memberAccount.getPointsBalance());
						tagParams.put("pointsCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("couponFree")){
						Long count = couponInstanceRepository.selectInstanceCountByMemberIdAndStatusId(member.getMemberId(), StatusConstant.COUPON_INSTANCE_UNUSED.getId());
						tagParams.put("couponFree", count);
						tagParams.put("couponFreeCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("couponUsed")){
						Long count = couponInstanceRepository.selectInstanceCountByMemberIdAndStatusId(member.getMemberId(), StatusConstant.COUPON_INSTANCE_USED.getId());
						tagParams.put("couponUsed", count);
						tagParams.put("couponUsedCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					//交易数据
					if(memberTagCondition.getDistinctConditionRule().equals("orderStore")){
						List<OrderHeader> orderHeaderList = orderHeaderRepository.selectOrderHeaderByMemberIdForStoreId(member.getMemberId(),memberTagCondition.getDistinctConditionValue());
						tagParams.put("orderStore", orderHeaderList.size());
						tagParams.put("orderStoreCondition", memberTagCondition.getDistinctConditionValue());
						continue;
					}
					if(memberTagCondition.getDistinctConditionRule().equals("orderCount")){
						String startTime = memberTagCondition.getDistinctConditionValue().split(",")[0];
						String endTime = memberTagCondition.getDistinctConditionValue().split(",")[1];
						Long countStart = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[2]);
						Long countEnd = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[3]);
						List<OrderHeader> orderHeaderList = orderHeaderRepository.selectOrderCount(member.getMemberId(), startTime, endTime, countStart, countEnd);
						tagParams.put("orderCount", orderHeaderList.size());
						tagParams.put("orderCountCondition", memberTagCondition.getDistinctConditionValue());
					}
					if(memberTagCondition.getDistinctConditionRule().equals("orderGrand")){
						String startTime = memberTagCondition.getDistinctConditionValue().split(",")[0];
						String endTime = memberTagCondition.getDistinctConditionValue().split(",")[1];
						Long sumStart = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[2]);
						Long sumEnd = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[3]);
						List<OrderHeader> orderHeaderList = orderHeaderRepository.selectOrderGrand(member.getMemberId(), startTime, endTime, sumStart, sumEnd);
						tagParams.put("orderGrand", orderHeaderList.size());
						tagParams.put("orderGrandCondition", memberTagCondition.getDistinctConditionValue());
					}
					if(memberTagCondition.getDistinctConditionRule().equals("orderOne")){
						String startTime = memberTagCondition.getDistinctConditionValue().split(",")[0];
						String endTime = memberTagCondition.getDistinctConditionValue().split(",")[1];
						Long oneStart = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[2]);
						Long oneEnd = Long.parseLong(memberTagCondition.getDistinctConditionValue().split(",")[3]);
						List<OrderHeader> orderHeaderList = orderHeaderRepository.selectOrderOne(member.getMemberId(), startTime, endTime, oneStart, oneEnd);
						tagParams.put("orderOne", orderHeaderList.size());
						tagParams.put("orderOneCondition", memberTagCondition.getDistinctConditionValue());
					}
				}
				tagAction.setMemberId(memberExp.getMemberId());
				tagAction.setActivityCode(ActivityConstant.TAG_ORDER.getCode());
				tagAction.setRuleOptParams(tagParams.toString());
				tagAction.setHappenTime(DateUtils.getCurrentTimeOfDb());
				tagAction.setCreateTime(DateUtils.getCurrentTimeOfDb());
				tagAction.setIsCount(NO_COUNT);
				tagActionList.add(tagAction);
			}
			activityTagActionRepository.saveList(tagActionList);
		}
	}

	@Override
	public List<MemberTag> findMemberTagByStatusId(Long statusId) {
		return this.memberTagRepository.findMemberTagByStatusId(statusId);
	}
	
}
