/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:KeysGroupServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月24日下午5:31:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.wechat.service.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.wechat.model.KeysGroup;
import com.bw.adv.module.wechat.model.WechatKeysRule;
import com.bw.adv.module.wechat.model.exp.KeysGroupExp;
import com.bw.adv.module.wechat.repository.KeysGroupRepository;
import com.bw.adv.service.wechat.service.KeysGroupService;
import com.bw.adv.service.wechat.service.WechatKeysRuleService;

/**
 * ClassName:KeysGroupServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午5:31:09 <br/>
 * scrmVersion 1.0
 * 
 * @author june
 * @version jdk1.7
 * @see
 */
@Service
public class KeysGroupServiceImpl extends BaseServiceImpl<KeysGroup> implements KeysGroupService {

	protected final Logger logger = Logger.getLogger(KeysGroupServiceImpl.class);

	private KeysGroupRepository keysGroupRepository;
	private WechatKeysRuleService wechatKeysRuleService;

	@Override
	public BaseRepository<KeysGroup, ? extends BaseMapper<KeysGroup>> getBaseRepository() {
		return keysGroupRepository;
	}

	@Override
	public boolean keywordReplay(String content) {

		List<WechatKeysRule> keysRuleList = null;
		// 匹配类型
		String matchType = null;
		try {
			keysRuleList = wechatKeysRuleService.queryWechatKeyRules();
			// 关键字默认是--- 未全匹配
			for (WechatKeysRule keysRule : keysRuleList) {
				matchType = keysRule.getMatchType();
				// 如果是未全匹配
				if (matchType.equals(WechatTypeConstant.KEYWORD_MODE_CONTAIN.getCode())) {
					if (keysRule.getKeyword().contains(content)) {
						return true;
					}
				} else {
					if (keysRule.getKeyword().equals(content)) {
						return true;
					}
				}
			}
			return false;
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	

	@Override
	public KeysGroupExp queryKeysGroupExpBykeysGroup(KeysGroup keysGroup) {
		return keysGroupRepository.findKeysGroupExpBykeysGroup(keysGroup);
	}
	
	@Autowired
	public void setKeysGroupRepository(KeysGroupRepository keysGroupRepository) {
		this.keysGroupRepository = keysGroupRepository;
	}

	@Autowired
	public void setWechatKeysRuleService(
			WechatKeysRuleService wechatKeysRuleService) {
		this.wechatKeysRuleService = wechatKeysRuleService;
	}

}
