/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.exp.OrgExp;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName: OrgService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-18 下午6:11:05 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface OrgService extends BaseService<Org>{

	/**
	 * queryListForOrg:查询区域列表(ORG_TYPE_ID为空). <br/>
	 * Date: 2015-8-26 下午3:01:35 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 */
	public List<Org> queryListForOrg(String useFlag);
	
	/**
	 * queryOrgByName:(查找同一父节点下的同名的org). <br/>
	 * Date: 2015-9-19 下午8:03:41 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param useFlag
	 * @param orgName
	 * @param pareOrgId
	 * @return
	 */
	public List<Org> queryOrgByName(String useFlag,String orgName,Long pareOrgId);
	
	public OrgExp queryOrgById(Long orgId);
	
	/**
	 * 根据orgId查询
	 * @param orgId
	 * @return
	 */
	public Org queryOrg(Long orgId);
	/**
	 * queryListForParentOrg:查询 与指定区域以外的所有区域. <br/>
	 * Date: 2015-8-26 下午3:02:02 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param upOrgId
	 * @return
	 */
	public List<Org> queryListForParentOrg(Long orgId);
	
	/**
	 * queryListForChildOrg:查询指定区域及其所有子区域. <br/>
	 * Date: 2015-8-27 下午4:24:56 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param orgId
	 * @return
	 */
	public List<Org> queryListForChildOrg(Long orgId);	
	
	
	/**
	 * deleteOrg:删除区域信息. <br/>
	 * Date: 2015-9-2 上午11:19:58 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param example
	 */
	public void deleteOrg(String stopDate,Example example);
	
	/**
	 * deleteStoreOrgId:清除门店区域id. <br/>
	 * Date: 2015-9-2 上午11:20:47 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param example
	 */
	public void deleteStoreOrgId(Example example);
	
	/**
	 * 
	 * queryOrgForTree:查询区域树(所有启用状态的区域). <br/>
	 * Date: 2015年9月25日 下午2:47:15 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<Org> queryOrgForTree();
	
	
}

