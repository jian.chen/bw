/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CouponSingleSummaryDayServiceImpl.java
 * Package Name:com.sage.scrm.service.report.service.impl
 * Date:2015年12月17日上午10:27:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.report.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.coupon.model.CouponSingleSummaryDay;
import com.bw.adv.module.report.coupon.model.exp.CouponSingleSummaryDayExp;
import com.bw.adv.module.report.repository.CouponSingleSummaryDayRepository;
import com.bw.adv.service.report.service.CouponSingleSummaryDayService;

/**
 * ClassName:CouponSingleSummaryDayServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 上午10:27:25 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class CouponSingleSummaryDayServiceImpl extends BaseServiceImpl<CouponSingleSummaryDay>
	implements CouponSingleSummaryDayService{
	
	private CouponSingleSummaryDayRepository couponSingleSummaryDayRepository;

	@Override
	public BaseRepository<CouponSingleSummaryDay, ? extends BaseMapper<CouponSingleSummaryDay>> getBaseRepository() {
		
		return couponSingleSummaryDayRepository;
	}
	
	@Override
	public List<CouponSingleSummaryDayExp> queryExpByExample(Example example,Page<CouponSingleSummaryDayExp> page) {
		
		return couponSingleSummaryDayRepository.findExpByExample(example, page);
	}
	
	
	@Override
	public List<CouponSingleSummaryDayExp> queryListByMonth(Integer month,
			Page<CouponSingleSummaryDayExp> page) {
		
		return couponSingleSummaryDayRepository.findListByMonth(month, page);
	}
	
	@Override
	public void callCouponSingleSummary(String dateStr) {
		
		couponSingleSummaryDayRepository.callCouponSingleSummary(dateStr);
		
	}

	@Autowired
	public void setCouponSingleSummaryDayRepository(
			CouponSingleSummaryDayRepository couponSingleSummaryDayRepository) {
		this.couponSingleSummaryDayRepository = couponSingleSummaryDayRepository;
	}
	

}

