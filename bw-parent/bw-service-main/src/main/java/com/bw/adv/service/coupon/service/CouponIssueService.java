package com.bw.adv.service.coupon.service;

import java.util.List;
import java.util.Map;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.CouponIssue;
import com.bw.adv.module.coupon.model.CouponOrigin;
import com.bw.adv.module.coupon.model.ExternalCoupon;
import com.bw.adv.module.coupon.model.exp.CouponResult;
import com.bw.adv.module.member.search.MemberSearch;

/**
 * ClassName: CouponIssueService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 下午2:28:09 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponIssueService extends BaseService<CouponIssue> {
	
	/**
	 * queryList:查询优惠券发布/推送列表  <br/>
	 * Date: 2015年8月13日 上午10:33:51 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	List<CouponIssue> queryList(Long couponId,Page<CouponIssue> page);
	
	/**
	 * queryByPk:根据主键查询优惠券发布/推送列表<br/>
	 * Date: 2015年8月13日 上午10:33:26 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couid
	 * @return
	 */
	CouponIssue queryByPk(Long couid);
	
	/**
	 * 领取兑换商品
	 * @param memberId
	 * @param couponId
	 * @return
	 */
	public CouponInstance getMemberBadgeGiftCoupon(Long memberId,Long couponId);
	
	/**
	 * 领取兑换商品
	 * @param memberId
	 * @param Prize
	 * @return
	 */
	public CouponInstance getLotteryGift(Long memberId,Prize prize,AwardsSettingExp awardsSettingExp);
	
	/**
	 * insertCouponIssue:发放优惠券给会员 <br/>
	 * Date: 2015年8月12日 下午3:54:30 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponId
	 * @param sourceId
	 * @param couponIssueNum
	 * @param memberIds
	 * @throws Exception 
	 */
	void insertCouponIssue(Long couponId,Long sourceId,Long couponIssueNum,String memberIds,int personalAmount,MemberSearch search, int sendTemp) throws Exception;
	/**
	 * insertCouponIssue:导入外部券优惠券 <br/>
	 * Date: 2015年8月12日 下午3:54:30 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponId
	 * @param externalCoupons
	 * @param result
	 * @throws Exception
	 */
	void insertExternalCoupon(Long couponId, List<ExternalCoupon> externalCoupons, JsonModel result);
	/**
	 * insertCouponIssue:发放优惠券给会员<br/>
	 * Date: 2015年9月8日 上午11:36:36 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @param couponMap
	 * @param sourceId
	 * @param externalId
	 * @param storeId
	 */
	void insertCouponIssue(Long memberId,Map<Long, Integer> couponMap,Long sourceId,String externalId) throws Exception;

	ResultStatus checkCoupon(Coupon coupon,Integer couponNum);
	
	/**
	 * insertCouponIssue:发放优惠券给会员 <br/>
	 * Date: 2015年12月31日 下午2:38:42 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @param couponList
	 * @param sourceId
	 * @param externalId
	 * @throws Exception
	 */
	String insertCouponIssue(Long memberId,List<CouponResult> couponList,Long sourceId,String externalId,int type) throws Exception;
	
	
	/**
	 * insertCouponIssueSingle:(发放优惠券会员,单张 ). <br/>
	 * Date: 2016-5-12 下午3:18:54 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param couponId
	 * @param sourceId
	 * @param externalId
	 * @param type
	 * @return
	 * @throws Exception 
	 */
	public String insertCouponIssueSingle(Long memberId, Long couponId,Long sourceId, String externalId,int type) throws Exception;
	/**
	 * queryChannelList:查询渠道列表 <br/>
	 * Date: 2015年8月25日 下午3:03:53 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	List<Channel> queryChannelList();
	
	/**
	 * queryChannelListByDc:根据优惠券类型查询渠道列表 <br/>
	 * Date: 2015年11月18日 下午3:11:51 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param typeId
	 * @return
	 */
	List<Channel> queryChannelListByDc(Long typeId);
	/**
	 * queryCount:查询优惠券发布数量 <br/>
	 * Date: 2015年8月25日 下午5:58:59 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponId
	 * @return
	 */
	int queryCountByCouponId(Long couponId);
	
	/**
	 * 查询正在下发的优惠券数量
	 * queryCouponIsIssueing: <br/>
	 * Date: 2016年5月16日 下午6:42:46 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @return
	 */
	Long queryCouponIsIssueing();
}
