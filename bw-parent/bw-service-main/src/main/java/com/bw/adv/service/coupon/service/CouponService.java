package com.bw.adv.service.coupon.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.coupon.model.*;
import com.bw.adv.module.coupon.model.exp.CouponExp;
import com.bw.adv.module.coupon.model.exp.CouponForUse;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.coupon.model.exp.CouponInstanceRelayExp;
import com.bw.adv.module.coupon.model.exp.CouponRangeExp;

/**
 * ClassName: CouponService <br/>
 * Function: 优惠券service层 <br/>
 * date: 2015年8月10日 下午4:53:00 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponService extends BaseService<Coupon> {
	
	/**
	 * queryCouponList:查询优惠券列表 <br/>
	 * Date: 2015年8月11日 下午7:47:24 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	List<CouponExp> queryCouponList(Page<Coupon> page);
	
	/**
	 * insertCoupon:新增优惠券 <br/>
	 * Date: 2015年8月11日 下午7:47:42 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param coupon
	 * @return
	 * @throws Exception 
	 */
	int insertCoupon(Coupon coupon,String equalOrgId) throws Exception;
	
	/**
	 * 
	 * queryCouponByCategoryID:(我要领劵模块--- 根据优惠券类别查询优惠券). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponExp> queryCouponByCategoryID(Example example);
	
	/**
	 * queryByPk:根据主键查询优惠券 <br/>
	 * Date: 2015年8月11日 下午7:47:58 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponid
	 * @return
	 */
	Coupon queryByPk(Long couponid);
	
	/**
	 * updateCoupon:跟新优惠券<br/>
	 * Date: 2015年8月11日 下午7:48:17 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param coupon
	 * @param equalOrgId TODO
	 * @return
	 */
	int updateCoupon(Coupon coupon, String equalOrgId);
	
	/**
	 * queryCouLByName:根据优惠券名称查询优惠券列表 <br/>
	 * Date: 2015年8月11日 下午7:48:33 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponName
	 * @return
	 */
	List<CouponExp> queryCouLByName(Example example,Page<Coupon> page);
	
	
	/**
	 * removeCoupons:批量删除优惠券 <br/>
	 * Date: 2015年8月13日 下午7:58:30 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponid
	 */
	String removeCoupons(String couponid);
	
	/**
	 * updateByCouponCode:核销优惠券 <br/>
	 * Date: 2015年8月17日 下午5:23:52 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponCode
	 * @return
	 */
	int updateByCouponCode(String couponCode,String status);
	
	/**
	 * queryCouponNum:查询优惠券数量 <br/>
	 * Date: 2015年8月20日 上午10:22:25 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	int queryCouponNum(String couponName);
	
	/**
	 * verificationCoupon:核销优惠券 <br/>
	 * Date: 2015年9月24日 下午4:26:26 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceCodes
	 * @param storeCode
	 * @param externalId
	 * @throws Exception
	 */
	void verificationCoupon(List<String> couponInstanceCodes,String storeCode,String externalId) throws Exception;
	
	/**
	 * verificationOrderCoupon:同步订单核销优惠券 <br/>
	 * Date: 2015年9月24日 下午4:26:26 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param couponInstanceCodes
	 * @param storeCode
	 * @param externalId
	 * @throws Exception
	 */
	void verificationOrderCoupon(List<String> couponInstanceCodes,String storeCode,String externalId) throws Exception;
	
	/**
	 * verificationCoupon:(scrm核销优惠券). <br/>
	 * Date: 2015年9月29日 下午7:32:19 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstance
	 * @return TODO
	 */
	JsonModel verificationCoupon(CouponInstance couponInstance,Long ststusId);
	
	/**
	 * queryCouponRange:查询优惠券区域/门店 <br/>
	 * Date: 2015年9月24日 下午4:28:22 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponId
	 * @return
	 */
	CouponRangeExp queryCouponRange(Long couponId);
	
	/**
	 * 调用优惠券统计存储过程
	 * @param dateStr
	 * @param weekStr
	 * @param monthStr
	 */
	void callCouponSummaryDay(String dateStr,String weekStr,String monthStr);
	
    
    
    /**
	 * checkCouponInstance:(校验券是否有效). <br/>
	 * Date: 2015年9月29日 上午11:26:05 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceExp
	 * @param store
	 * @return
	 */
	public void checkCouponInstance(CouponInstanceExp couponInstanceExp,Store store);
	
	/**
	 * checkOrderCouponInstance:(同步订单校验券是否有效). <br/>
	 * Date: 2015年9月29日 上午11:26:05 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param couponInstanceExp
	 * @param store
	 * @return
	 */
	public void checkOrderCouponInstance(CouponInstanceExp couponInstanceExp,Store store);
	
	/**
     * 查询优惠券统计数据
     * @param date
     * @return
     */
    CouponSummaryDay selectBySummaryDate(String date);
    
    /**
     * selectCouponBusinessType:查询优惠券业务类型<br/>
     * Date: 2015年12月28日 下午3:09:30 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @return
     */
    List<CouponBusinessType> selectCouponBusinessType();
    
    /**
     * selectCouponBusinessType:查询优惠券业务类型分页<br/>
     * Date: 2015年12月28日 下午4:59:56 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param page
     * @return
     */
    List<CouponBusinessType> selectCouponBusinessType(Page<CouponBusinessType> page);
    
    /**
     * selectAllActive:查询优惠券类型<br/>
     * Date: 2015年12月28日 下午3:10:05 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @return
     */
    List<CouponType> selectAllActive();
    
    /**
     * selectTypeByPk:根据主键查询优惠券类型 <br/>
     * Date: 2015年12月28日 下午3:10:26 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param couponTypeId
     * @return
     */
    CouponType selectTypeByPk(Long couponTypeId);
    
    /**
     * selectCouponBusinessByPk:根据主键查询优惠券业务类型<br/>
     * Date: 2015年12月28日 下午3:10:48 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param couponBusinessTypeId
     * @return
     */
    CouponBusinessType selectCouponBusinessByPk(Long couponBusinessTypeId);
    
    /**
     * insertCouponBusinessType:新增优惠券业务类型 <br/>
     * Date: 2015年12月28日 下午5:30:07 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param couponBusinessType
     * @throws Exception 
     */
    int insertCouponBusinessType(CouponBusinessType couponBusinessType) throws Exception;
    
    /**
     * updateCouponBusinessType:修改优惠券业务类型 <br/>
     * Date: 2015年12月28日 下午8:31:42 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param couponBusinessType
     * @return
     */
    int updateCouponBusinessType(CouponBusinessType couponBusinessType);
    
    /**
     * removeCouponBusinessType:删除业务类型 <br/>
     * Date: 2015年12月28日 下午8:56:46 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param couponBusinessTypeId
     * @return
     */
    int removeCouponBusinessType(Long couponBusinessTypeId);
    
    /**
     * 
     * selectCouponInstanceRelayList:查询转发记录列表(条件). <br/>
     * Date: 2016年5月31日 下午7:10:05 <br/>
     * scrmVersion 1.0
     * @author zhangqi
     * @version jdk1.7
     * @param relayExp
     * @param pageObj
     * @return
     */
    public Long selectCouponInstanceRelayList(CouponInstanceRelayExp relayExp, Page<CouponInstanceRelayExp> pageObj);

    /**
     * 
     * couponrollback:优惠券退回. <br/>
     * Date: 2016年6月1日 下午5:36:27 <br/>
     * scrmVersion 1.0
     * @author zhangqi
     * @version jdk1.7
     * @param couponInstanceId
     * @param srcId
     * @return
     */
    public int couponrollback(Long couponInstanceId, Long srcId);
    
    /**
     * 查询优惠券使用情况
     * @param couponInstanceCode
     * @return
     */
    List<CouponForUse> queryCouponInstanceForUse(String couponInstanceCode);
	/**
	 * 查询券使用类型
	 * queryCouponIsIssueing: <br/>
	 * @version jdk1.7
	 * @return
	 */
	List<CouponOrigin> getCouponOriginList();
	/**
	 * 查询第三方商户列表
	 * queryCouponIsIssueing: <br/>
	 * @version jdk1.7
	 * @return
	 */
	List<ExternalCouponMerchant> getExternalCouponMerchantsPaged(int page, int rows);

	Coupon findCouponByCode(String code);
}
