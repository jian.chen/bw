/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ComplainTypeService.java
 * Package Name:com.sage.scrm.service.complain.service
 * Date:2015年12月29日下午5:06:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.complain.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.complain.model.ComplainType;

/**
 * ClassName:ComplainTypeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月29日 下午5:06:29 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface ComplainTypeService extends BaseService<ComplainType>{
	List<ComplainType> queryAllComplainType();
}

