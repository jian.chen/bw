package com.bw.adv.service.sys.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.sys.model.SysLicense;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.SysUserRoleKey;

public interface SysUserService extends BaseService<SysUser> {


	/**
	 * 更新系统用户的状态
	 * @param sysUserIds
	 * @param status
	 * @return
	 */
	public boolean changeSysUserStatus(List<Long> sysUserIds, Long status);

	/**
	 * 查询所有的系统用户
	 * @param page
	 * @return
	 */
	public List<SysUser> queryAllByPage(Page<SysUser> page);

	/**
	 * 
	 * querySysUserByStatusId:查询有效用户列表
	 * Date: 2015年8月14日 下午3:34:04 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @param statusId
	 * @return
	 */
	public List<SysUser> querySysUserByStatusId(Long statusId, Page<SysUser> page);
	
	/**
	 * 
	 * deleteSysUser:删除用户（逻辑删除）
	 * Date: 2015年8月17日 下午1:57:02 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param SysUserId
	 */
	public void deleteSysUser(Long sysUserId);
	
	/**
	 * 
	 * deleteSysUserList:批量删除用户(逻辑删除)
	 * Date: 2015年8月25日 下午5:19:47 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUserIds
	 */
	@Deprecated
	public void deleteSysUserList(List<Long> sysUserIds,String type);
	
	/**
	 * 
	 * querySysRole:查询有效角色
	 * Date: 2015年8月17日 下午1:58:01 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<SysRole> querySysRole();
	
	/**
	 * 
	 * save:保存用户信息&&用户角色信息
	 * Date: 2015年9月2日 下午2:37:33 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUser
	 * @param roleId
	 * @return
	 */
	public int save(SysUser sysUser,Long roleId);
	
	/**
	 * 保存用户选择角色
	 * @param sysUser
	 * @param selectRoleIds
	 */
	public void saveUserForRole(SysUser sysUser,String selectRoleIds);
	
	/**
	 * 
	 * querySysUserRoleByUserId:根据用户Id查询用户角色
	 * Date: 2015年9月2日 下午2:38:21 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param userId
	 * @return
	 */
	public SysUserRoleKey querySysUserRoleByUserId(Long userId);

	public List<SysUserRoleKey> querySysUserRoleListByUserId(Long userId);
	
	/**
	 * 
	 * updateSysUser:修改用户信息&&用户角色信息
	 * Date: 2015年9月2日 下午2:39:09 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUser
	 * @param roleId
	 * @return
	 */
	public int updateSysUser(SysUser sysUser,Long roleId);
	
	/**
	 * 
	 * querySysRoleByPk:根据Id查询角色信息
	 * Date: 2015年9月2日 下午2:39:33 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysRoleId
	 * @return
	 */
	public SysRole querySysRoleByPk(Long sysRoleId);
	
	/**
	 * 
	 * querySysUserByUserName:根据用户名称查询用户信息
	 * Date: 2015年9月2日 下午2:41:01 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param name
	 * @return
	 */
	public SysUser querySysUserByUserName(String name);
	
	/**
	 * 
	 * queryUserNames:查询数据库中所有系统用户的用户名
	 * Date: 2015年9月14日 下午2:36:30 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<SysUser> queryUserNames();
	
	/**
	 * 获取当前系统激活信息
	 * @return
	 */
	public SysLicense getSysActiveInfo();

	/**
	 * 更新系统激活信息
	 * @param sysLicense
	 */
	public int updateByPkSelective(SysLicense sysLicense);
	
	/**
	 * 更新用户信息
	 * @param sysUser
	 * @param selectRoleIds
	 */
	public void updateUserForRole(SysUser sysUser,String selectRoleIds);
	
}
