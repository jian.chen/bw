/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageAutoReplyServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月19日下午2:15:44
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatMessageAutoReply;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.model.exp.WechatMessageAutoReplyExp;
import com.bw.adv.module.wechat.repository.WechatMessageAutoReplyRepository;
import com.bw.adv.module.wechat.repository.WechatMessageTemplateRepository;
import com.bw.adv.service.wechat.service.WechatMessageAutoReplyService;

/**
 * ClassName:WechatMessageAutoReplyServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午2:15:44 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatMessageAutoReplyServiceImpl extends BaseServiceImpl<WechatMessageAutoReply> implements WechatMessageAutoReplyService {
	
	private WechatMessageAutoReplyRepository wechatMessageAutoReplyRepository;
	
	private WechatMessageTemplateRepository  wechatMessageTemplateRepository;
	
	@Override
	public BaseRepository<WechatMessageAutoReply, ? extends BaseMapper<WechatMessageAutoReply>> getBaseRepository() {
		return wechatMessageAutoReplyRepository;
	}
	
	@Override
	public WechatMessageAutoReplyExp queryWechatMessageAutoReply(String type) {
		return wechatMessageAutoReplyRepository.findWechatMessageAutoReply(type);
	}
	
	@Autowired
	public void setWechatMessageAutoReplyRepository(
			WechatMessageAutoReplyRepository wechatMessageAutoReplyRepository) {
		this.wechatMessageAutoReplyRepository = wechatMessageAutoReplyRepository;
	}
	@Autowired
	public void setWechatMessageTemplateRepository(
			WechatMessageTemplateRepository wechatMessageTemplateRepository) {
		this.wechatMessageTemplateRepository = wechatMessageTemplateRepository;
	}

	@Override
	@Transactional
	public void sendWechatMessageAutoReply(Map<String, Object> paramsMap) {
		WechatMessageTemplate wechatMessageTemplate=null;
		WechatMessageAutoReply wechatMessageAutoReply=null;
		
		try {
			 wechatMessageTemplate=new WechatMessageTemplate();
			 wechatMessageTemplate.setContent((String) paramsMap.get("content"));
			 wechatMessageTemplate.setWechatMessageType((String) paramsMap.get("wechatMessageType"));
			 wechatMessageTemplate.setStatusId((long)(StatusConstant.WECHAT_ENABLE.getId()));
			 wechatMessageTemplate.setIsDeleted(WechatTypeConstant.UNDELETED.getCode());
			 wechatMessageTemplate.setPictureUrl((String) paramsMap.get("strBackUrl"));
			 wechatMessageTemplate.setCreateTime(DateUtils.getCurrentTimeOfDb());
			 wechatMessageTemplate.setCreateBy(Long.parseLong(paramsMap.get("userid")+""));
			 
			 wechatMessageTemplateRepository.save(wechatMessageTemplate);
			 
			 wechatMessageAutoReply=new WechatMessageAutoReply();
			 wechatMessageAutoReply.setWechatMessageTemplateId(wechatMessageTemplate.getWechatMessageTemplateId());
			 wechatMessageAutoReply.setIsDeleted(WechatTypeConstant.UNDELETED.getCode());
			 wechatMessageAutoReply.setReplytype((String) paramsMap.get("replytype"));
			 wechatMessageAutoReply.setCreateDate(DateUtils.getCurrentTimeOfDb());
			 wechatMessageAutoReply.setCreateBy(Long.parseLong(paramsMap.get("userid")+""));
			 
			 wechatMessageAutoReplyRepository.removeAllWechatMessageAutoReply((String) paramsMap.get("replytype"));//软删除所有未删除标示的
			 wechatMessageAutoReplyRepository.save(wechatMessageAutoReply);
			 
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		
	}
	
}

