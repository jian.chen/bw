package com.bw.adv.service.sys.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatAccount;
import com.bw.adv.module.wechat.model.WechatAccountConfig;
import com.bw.adv.module.wechat.model.exp.WechatAccountExp;
import com.bw.adv.service.wechat.msg.view.WechatConfigView;

/**
 * 
* @ClassName: WechatAccountService
* @Description: 微信账号配置信息
* @author chuanxue.wei   
* @date 2015年8月13日 下午7:54:58
*
 */
public interface WechatAccountService extends BaseService<WechatAccount> {

	/**
	 * 
	* @Title: queryWechatAccountByPk 
	* @Description: 查看微信账户配置信息
	* @param @param wechatAccountId
	* @param @return    设定文件 
	* @return WechatAccount    返回类型 
	* @throws
	 */
	public WechatAccountConfig queryWechatAccountByPk(Long wechatAccountId);
	
	/**
	 * 
	 * saveWechatAccountConfig:保存微信账号配置信息
	 * Date: 2015年8月14日 上午10:34:38 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param wechatAccountConfig
	 * @return
	 */
	public int saveWechatAccountConfig(WechatAccountConfig wechatAccountConfig);
	
	/**
	 * 
	 * queryExpByPk:根据id查询扩展类
	 * Date: 2015年9月2日 下午1:36:46 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param wechatAccountId
	 * @return
	 */
	public WechatAccountExp queryExpByPk(Long wechatAccountId);
	
	/**
	 * 
	 * queryWechatAccount:查询微信账号信息列表中第一条数据
	 * Date: 2015年9月2日 下午1:37:14 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public WechatAccountExp queryWechatAccount();
	
	/**
	 * 修改会员账号信息以及配置
	 * @param wechatConfigView
	 * @return
	 */
	public int updateWechatAccountAndConfig(WechatConfigView wechatConfigView);

	/**
	 * 测试wechat连接
	 * @return
	 */
	public boolean testWechatConnect();
	
	
}
