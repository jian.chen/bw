package com.bw.adv.service.sys.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.sys.enums.SysSystemEnum;
import com.bw.adv.module.sys.model.SysLicense;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.SysUserRoleKey;
import com.bw.adv.module.sys.repository.SysLicenseRepository;
import com.bw.adv.module.sys.repository.SysRoleRepository;
import com.bw.adv.module.sys.repository.SysUserRepository;
import com.bw.adv.module.sys.repository.SysUserRoleRepository;
import com.bw.adv.service.sys.service.SysUserService;


@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser> implements SysUserService {
	
	private SysUserRepository sysUserRepository;
	
	private SysRoleRepository sysRoleRepository;
	
	private SysUserRoleRepository sysUserRoleRepository;
	
	private SysLicenseRepository sysLicenseRepository;
	
	@Override
	public BaseRepository<SysUser, ? extends BaseMapper<SysUser>> getBaseRepository() {
		return sysUserRepository;
	}


	@Override
	public boolean changeSysUserStatus(List<Long> sysUserIds, Long status) {
		return this.sysUserRepository.updateStatusIdList(status, sysUserIds) > 0;
	}

	@Override
	public List<SysUser> queryAllByPage(Page<SysUser> page){
		return sysUserRepository.findByStatusId(null, page);
	}

	@Override
	public List<SysUser> querySysUserByStatusId(Long statusId, Page<SysUser> page) {
		return sysUserRepository.findByStatusId(statusId, page);
	}

	@Override
	public void deleteSysUser(Long sysUserId) {
		SysUser sysUser = new SysUser();
		sysUser.setStatusId(StatusConstant.SYS_USER_REMOVE.getId());
		sysUser.setUserId(sysUserId);
		sysUserRepository.updateByPkSelective(sysUser);
	}
	
	@Override
	@Deprecated
	public void deleteSysUserList(List<Long> sysUserIds,String type) {
		Long status=null;
		if(type.equals("0")){
			status=StatusConstant.SYS_USER_REMOVE.getId();
		}else{
			status=StatusConstant.SYS_USER_ACTIVE.getId();
		}
		sysUserRepository.updateStatusIdList(status, sysUserIds);
	}

	

	@Override
	public List<SysRole> querySysRole() {
		return sysRoleRepository.findByFlag("Y");
	}
	
	@Override
	@Transactional
	public int save(SysUser sysUser,Long roleId){
		int i = 0;
		int j = 0;
		Long id = 0l;
		SysUserRoleKey sysUserRoleKey = null;
		sysUserRoleKey = new SysUserRoleKey();
		sysUser.setStatusId(StatusConstant.SYS_USER_ACTIVE.getId());
		i = sysUserRepository.save(sysUser);
		id = sysUser.getUserId();
		sysUserRoleKey.setRoleId(roleId);
		sysUserRoleKey.setUserId(id);
		
		j = sysUserRoleRepository.saveSelective(sysUserRoleKey);
		return i+j;
	}
	
	@Override
	public void saveUserForRole(SysUser sysUser, String selectRoleIds) {
		Long id = 0L;
		SysUserRoleKey sysUserRoleKey = null;
		List<SysUserRoleKey> roleList = null;
		roleList = new ArrayList<SysUserRoleKey>();
		sysUser.setStatusId(StatusConstant.SYS_USER_ACTIVE.getId());
		sysUserRepository.save(sysUser);
		id = sysUser.getUserId();
		String[] array = selectRoleIds.split(","); 
		for (int i = 0; i < array.length; i++) {
			sysUserRoleKey = new SysUserRoleKey();
			sysUserRoleKey.setRoleId(Long.parseLong(array[i]));
			sysUserRoleKey.setUserId(id);
			roleList.add(sysUserRoleKey);
		}
		sysUserRoleRepository.saveList(roleList);
		
	}

	@Override
	@Transactional
	public int updateSysUser(SysUser sysUser,Long roleId){
		SysUserRoleKey sysUserRole = new SysUserRoleKey();
		sysUserRole.setUserId(sysUser.getUserId());
		sysUserRole.setRoleId(roleId);
		sysUserRoleRepository.removeByUserId(sysUser.getUserId());
		sysUserRoleRepository.save(sysUserRole);
		return sysUserRepository.updateByPkSelective(sysUser);
	}
	
	@Override
	public void updateUserForRole(SysUser sysUser,String selectRoleIds){
		SysUserRoleKey sysUserRoleKey = null;
		List<SysUserRoleKey> roleList = null;
		
		roleList = new ArrayList<SysUserRoleKey>();
		sysUserRoleRepository.removeByUserId(sysUser.getUserId());
		if ((sysUser.getSystemId() == null || sysUser.getSystemId().equals(SysSystemEnum.AIR.getSystemId()))
				&& sysUser.getOrgId() != null) {
			sysUser.setOrgId(null);
		}
		sysUserRepository.updateByPkSelective(sysUser);
		
		String[] array = selectRoleIds.split(","); 
		for (int i = 0; i < array.length; i++) {
			sysUserRoleKey = new SysUserRoleKey();
			sysUserRoleKey.setRoleId(Long.parseLong(array[i]));
			sysUserRoleKey.setUserId(sysUser.getUserId());
			roleList.add(sysUserRoleKey);
		}
		sysUserRoleRepository.saveList(roleList);
		
	}
	
	@Override
	public SysUserRoleKey querySysUserRoleByUserId(Long userId) {
		List<SysUserRoleKey> sysUserRoles = sysUserRoleRepository.findByUserId(userId);
		if(sysUserRoles.size()!=0){
			return sysUserRoles.get(0);
		}
		return null;
	}

	@Override
	public List<SysUserRoleKey> querySysUserRoleListByUserId(Long userId) {
		return sysUserRoleRepository.findByUserId(userId);
	}
	
	@Override
	public SysRole querySysRoleByPk(Long sysRoleId) {
		return sysRoleRepository.findByPk(sysRoleId);
	}
	
	@Override
	public SysUser querySysUserByUserName(String name) {
		return sysUserRepository.findByUserName(name);
	}

	@Override
	public List<SysUser> queryUserNames() {
		return sysUserRepository.findAll();
	}

	@Override
	public SysLicense getSysActiveInfo() {
		return sysLicenseRepository.getActiveInfo();
	}
	
	@Override
	public int updateByPkSelective(SysLicense sysLicense) {
		return sysLicenseRepository.updateByPkSelective(sysLicense);
	}
	
	@Autowired
	public void setSysRoleRepository(SysRoleRepository sysRoleRepository) {
		this.sysRoleRepository = sysRoleRepository;
	}
	
	@Autowired
	public void setSysUserRepository(SysUserRepository sysUserRepository) {
		this.sysUserRepository = sysUserRepository;
	}

	@Autowired
	public void setSysUserRoleRepository(SysUserRoleRepository sysUserRoleRepository) {
		this.sysUserRoleRepository = sysUserRoleRepository;
	}

	@Autowired
	public void setSysLicenseRepository(SysLicenseRepository sysLicenseRepository) {
		this.sysLicenseRepository = sysLicenseRepository;
	}

}
