/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceReceiveService.java
 * Package Name:com.sage.scrm.bk.coupon.service
 * Date:2015年11月24日下午9:05:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.coupon.model.CouponInstanceReceive;

/**
 * ClassName:CouponInstanceReceiveService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月24日 下午9:05:39 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponInstanceReceiveService extends BaseService<CouponInstanceReceive>{

}

