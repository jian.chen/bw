/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:WechatApiTemplateSendUtil.java
 * Package Name:com.sage.scrm.bk.wechat.utils
 * Date:2015年11月11日下午3:55:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.wechat.msg.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.api.ApiWeChat;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueComExp;
import com.bw.adv.module.wechat.util.HttpKit;
import com.bw.adv.service.wechat.msg.model.TemplateData;
import com.bw.adv.service.wechat.msg.model.WechatNews;
import com.bw.adv.service.wechat.msg.model.WechatNewsArticles;
import com.bw.adv.service.wechat.msg.model.WechatTemplate;
import com.bw.adv.service.wechat.msg.model.WechatTextTemplate;

/**
 * ClassName:WechatApiTemplateSendUtil <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月11日 下午3:55:04 <br/>
 * scrmVersion 1.0
 * 
 * @author yu.zhang
 * @version jdk1.7
 * @see
 */
public class WechatApiUtil {
	protected static  Logger logger = Logger
			.getLogger(WechatApiUtil.class);
	@Value("${articles_title}")
	private static  String articlesTitle;
	@Value("${articles_title}")
	private static  String articlesDescription;
	@Value("${articles_url}")
	private static  String articlesUrl;
	@Value("${articles_picurl}")
	private static  String articlesPicurl;
	@Value("${jsapi_ticket}")
	private static  String jsapi_ticket;

	@Value("${template_Color}")
	private static  String templateColor;
	@Value("${domainName}")
	private static  String domainName;

	@Value("${appId}")
	private static  String appId;;
	@Value("${couponTemplateId}")
	private static  String couponTemplateId;
	@Value("${couponFirst}")
	private static  String couponFirst;
	@Value("${couponRemark}")
	private static  String couponRemark;;
	@Value("${orderTemplateId}")
	private static  String orderTemplateId;
	@Value("${orderFirst}")
	private static  String orderFirst;
	@Value("${orderRemark}")
	private static  String orderRemark;
	@Value("${orderRemark}")
	private static  String textLink;;
	@Value("${couponExpireTemplateId}")
	private static  String couponExpireTemplateId;
	@Value("${couponExpireFirst}")
	private static  String couponExpireFirst;
	@Value("${couponExpireRemark}")
	private static  String couponExpireRemark;

	@Value("${closeComplainTemplateId}")
	private static  String closeComplainTemplateId;
	@Value("${serviceInfoBefore}")
	private static  String serviceInfoBefore;
	@Value("${serviceInfoAfter}")
	private static  String serviceInfoAfter;
	@Value("${closeComplainRemark}")
	private static  String closeComplainRemark;

	@Value("${memberModifyGradeTemplateId}")
	private static  String memberModifyGradeTemplateId;
	@Value("${memberModifyGradeFirst}")
	private static  String memberModifyGradeFirst;
	@Value("${memberModifyGradeRemark}")
	private static  String memberModifyGradeRemark;

	@Value("${memberModifyPointsTemplateId}")
	private static  String memberModifyPointsTemplateId;
	@Value("${memberModifyPointsFirst}")
	private static  String memberModifyPointsFirst;
	@Value("${memberModifyPointsRemark}")
	private static  String memberModifyPointsRemark;

	// 发送模板消息
	public static JSONObject templateSend(String accessToken,
			WechatTemplate data) throws Exception {
		// logger.info(JSONObject.toJSONString(data));
		String result = HttpKit.post(
				ApiWeChat.SEND_TEMPLATE.replace("ACCESS_TOKEN", accessToken),
				JSONObject.toJSONString(data));
		if (StringUtils.isNotEmpty(result)) {
			return JSONObject.parseObject(result);
		}
		return null;
	}

	/**
	 * newsSend:(扫码发送图文消息) scrmVersion 1.0
	 * 
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param accessToken
	 * @param instance
	 * @param openId
	 * @return
	 * @throws Exception
	 */
	public static JSONObject sendMessageForScan(String accessToken,String openId, String code) throws Exception {
		WechatNews wechatNews = new WechatNews();
		wechatNews.setTouser(openId);
		wechatNews.setMsgtype("news");
		WechatNewsArticles newsArticles = new WechatNewsArticles();
		newsArticles.setTitle(articlesTitle + "\n" + code);
		newsArticles.setDescription(articlesDescription);
		newsArticles.setUrl(articlesUrl);
		newsArticles.setPicurl(articlesPicurl);
		List<WechatNewsArticles> articles = new ArrayList<WechatNewsArticles>();
		articles.add(newsArticles);
		Map<String, List<WechatNewsArticles>> map = new HashMap<String, List<WechatNewsArticles>>();
		map.put("articles", articles);
		wechatNews.setNews(map);
		String url = ApiWeChat.SEND_CUSTOM_MESSAGE.replace("ACCESS_TOKEN",
				accessToken);
		String result = HttpKit.post(url, JSONObject.toJSONString(wechatNews));
		if (StringUtils.isNotEmpty(result)) {
			return JSONObject.parseObject(result);
		}
		return null;
	}

	/**
	 * 
	 * textSend:(发送文本消息). <br/>
	 * Date: 2015年12月17日 下午4:13:15 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param accessToken
	 * @param instance
	 * @param openId
	 * @return
	 * @throws Exception
	 */
	public static JSONObject textSend(String accessToken, String openId)
			throws Exception {
		WechatTextTemplate textTemplate = new WechatTextTemplate();
		textTemplate.setTouser(openId);
		textTemplate.setMsgtype("text");
		String content = textLink.replace("APPID", appId).replace("DOMAINNAME",
				domainName);
		Map<String, String> map = new HashMap<String, String>();
		map.put("content", content);
		textTemplate.setText(map);
		String url = ApiWeChat.SEND_CUSTOM_MESSAGE.replace("ACCESS_TOKEN",
				accessToken);
		String result = HttpKit
				.post(url, JSONObject.toJSONString(textTemplate));
		if (StringUtils.isNotEmpty(result)) {
			return JSONObject.parseObject(result);
		}
		return null;
	}

	/**
	 * 
	 * sendMsgForCouponReceive:(优惠券领取模板消息). <br/>
	 * Date: 2015年12月17日 上午10:17:49 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param accessToken
	 * @return
	 * @throws Exception
	 */
	public static JSONObject sendMsgForCouponReceive(
			WechatMsgQueueComExp instance, String accessToken) {
		JSONObject obj = JSONObject.parseObject(instance.getMsgContent());
		String openId = instance.getOpenId();
		String couponName = obj.getString("couponName");
		String memberName = obj.getString("memberName");
		String couponCode = instance.getMsgKey();
		String startDate = obj.getString("startDate");
		String webchatRemark = obj.getString("webchatRemark");
		webchatRemark = StringUtils.isBlank(webchatRemark) ? couponRemark
				: webchatRemark;
		// String expireDate = obj.getString("expireDate");
		JSONObject result = null;
		String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx506f4b12c6e6292a&redirect_uri="
				+ "http://"
				+ domainName
				+ "/scrm-pe-wechat/index.html#!/src/couponInfo/"
				+ couponCode
				+ "&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
		WechatTemplate template = new WechatTemplate();
		template.setUrl(url);
		template.setTouser(openId);
		template.setTemplate_id(couponTemplateId);
		Map<String, TemplateData> map = new HashMap<String, TemplateData>();

		TemplateData first = new TemplateData();
		first.setColor(templateColor);
		first.setValue(couponFirst);
		map.put("first", first);
		// 领取人
		TemplateData name = new TemplateData();
		name.setColor(templateColor);
		name.setValue(memberName);
		map.put("keyword1", name);
		// 礼品
		TemplateData code = new TemplateData();
		code.setColor(templateColor);
		code.setValue(couponName);
		map.put("keyword2", code);
		// 领取时间
		TemplateData eDate = new TemplateData();
		eDate.setColor(templateColor);
		eDate.setValue(startDate);
		map.put("keyword3", eDate);

		TemplateData remark = new TemplateData();
		remark.setColor(templateColor);
		remark.setValue(webchatRemark);
		map.put("remark", remark);
		template.setData(map);
		try {
			result = templateSend(accessToken, template);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return result;
	}

	/**
	 * 
	 * sendMsgForConsumption:(发送订单消费记录模板消息). <br/>
	 * scrmVersion 1.0
	 * 
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param accessToken
	 * @return
	 * @throws Exception
	 */
	public static JSONObject sendMsgForConsumption(
			WechatMsgQueueComExp instance, String accessToken) {
		JSONObject obj = JSONObject.parseObject(instance.getMsgContent());
		JSONObject result = null;
		String openId = instance.getOpenId();
		/*
		 * String url =
		 * "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx506f4b12c6e6292a&redirect_uri="
		 * +
		 * "http://"+domainName+"/scrm-pe-wechat/index.html#!/src/purchaseHistory"
		 * +"&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
		 */
		String url = obj.getString("storeUrl");
		String orderAmount = obj.getString("payAmount");
		String storeName = obj.getString("storeName");
		String orderDate = obj.getString("orderDate");
		WechatTemplate template = new WechatTemplate();
		template.setUrl(url);
		template.setTouser(openId);
		template.setTemplate_id(orderTemplateId);
		Map<String, TemplateData> map = new HashMap<String, TemplateData>();

		TemplateData first = new TemplateData();
		first.setColor(templateColor);
		first.setValue(orderFirst);
		map.put("first", first);
		// 消费门店
		TemplateData store = new TemplateData();
		store.setColor(templateColor);
		store.setValue(storeName);
		map.put("keyword1", store);
		// 消费金额
		TemplateData amount = new TemplateData();
		amount.setColor(templateColor);
		amount.setValue(orderAmount + " 元");
		map.put("keyword2", amount);
		// 支付方式
		TemplateData payMethod = new TemplateData();
		payMethod.setColor(templateColor);
		payMethod.setValue("--");
		map.put("keyword3", payMethod);
		// 赠送积分
		TemplateData points = new TemplateData();
		points.setColor(templateColor);
		points.setValue(orderAmount);
		map.put("keyword4", points);
		// 消费时间
		TemplateData date = new TemplateData();
		date.setColor(templateColor);
		date.setValue(orderDate);
		map.put("keyword5", date);

		TemplateData remark = new TemplateData();
		remark.setColor(templateColor);
		remark.setValue(orderRemark);
		map.put("remark", remark);
		template.setData(map);
		try {
			result = templateSend(accessToken, template);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return result;
	}

	/**
	 * sendMsgForCouponExpire:(优惠券过期发送模板). <br/>
	 * Date: 2016年1月6日 下午5:26:28 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param instance
	 * @param accessToken
	 * @return
	 */
	public static JSONObject sendMsgForCouponExpire(
			WechatMsgQueueComExp instance, String accessToken) {
		JSONObject obj = JSONObject.parseObject(instance.getMsgContent());
		String openId = instance.getOpenId();
		String couponName = obj.getString("couponName");
		String couponCode = instance.getMsgKey();
		String thruDate = obj.getString("expireDate");
		JSONObject result = null;
		String url = "";
		WechatTemplate template = new WechatTemplate();
		template.setUrl(url);
		template.setTouser(openId);
		template.setTemplate_id(couponExpireTemplateId);
		Map<String, TemplateData> map = new HashMap<String, TemplateData>();

		TemplateData first = new TemplateData();
		first.setColor(templateColor);
		first.setValue(couponExpireFirst);
		map.put("first", first);

		TemplateData name = new TemplateData();
		name.setColor(templateColor);
		name.setValue(couponName);
		map.put("keyword1", name);

		TemplateData code = new TemplateData();
		code.setColor(templateColor);
		code.setValue(couponCode);
		map.put("keyword2", code);

		TemplateData tDate = new TemplateData();
		tDate.setColor(templateColor);
		tDate.setValue(thruDate);
		map.put("keyword3", tDate);

		TemplateData remark = new TemplateData();
		remark.setColor(templateColor);
		remark.setValue(couponExpireRemark);
		map.put("remark", remark);
		template.setData(map);
		try {
			result = templateSend(accessToken, template);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return result;
	}

	public static JSONObject getTicket(String accessToken) throws Exception {
		String jsonStr = HttpKit.get(jsapi_ticket.replace("ACCESS_TOKEN",
				accessToken));
		return JSONObject.parseObject(jsonStr);
	}

	/**
	 * sendMsgFormemberModifyGrade:(会员级别变更模版). <br/>
	 * Date: 2016年7月11日 下午3:52:14 <br/>
	 * scrmVersion standard
	 * 
	 * @author dong.d
	 * @version jdk1.7
	 * @param instance
	 * @param accessToken
	 * @return
	 */
	public static JSONObject sendMsgForMemberModifyGrade(
			WechatMsgQueueComExp instance, String accessToken) {
		JSONObject obj = JSONObject.parseObject(instance.getMsgContent());
		String openId = instance.getOpenId();
		String gradebefor = obj.getString("gradebefor");
		String gradeafter = obj.getString("gradeafter");
		String uDate = obj.getString("changeDate");
		JSONObject result = null;
		WechatTemplate template = new WechatTemplate();
		template.setTouser(openId);
		template.setTemplate_id(memberModifyGradeTemplateId);
		Map<String, TemplateData> map = new HashMap<String, TemplateData>();

		TemplateData firstTemp = new TemplateData();
		firstTemp.setColor(templateColor);
		firstTemp.setValue(memberModifyGradeFirst
				.replace("(param)", gradeafter));
		map.put("first", firstTemp);

		TemplateData gradebeforTemp = new TemplateData();
		gradebeforTemp.setColor(templateColor);
		gradebeforTemp.setValue(gradebefor);
		map.put("grade1", gradebeforTemp);

		TemplateData gradeafterTemp = new TemplateData();
		gradeafterTemp.setColor(templateColor);
		gradeafterTemp.setValue(gradeafter);
		map.put("grade2", gradeafterTemp);

		TemplateData uDateTemp = new TemplateData();
		uDateTemp.setColor(templateColor);
		uDateTemp.setValue(uDate);
		map.put("time", uDateTemp);

		TemplateData remarkTemp = new TemplateData();
		remarkTemp.setColor(templateColor);
		remarkTemp.setValue(memberModifyGradeRemark);
		/* map.put("remark", remarkTemp); */
		template.setData(map);
		try {
			result = templateSend(accessToken, template);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return result;
	}

	/**
	 * sendMsgForMemberModifyPoints:(会员积分变更模版). <br/>
	 * Date: 2016年7月11日 下午5:21:51 <br/>
	 * scrmVersion standard
	 * 
	 * @author dong.d
	 * @version jdk1.7
	 * @param instance
	 * @param accessToken
	 * @return
	 */
	public static JSONObject sendMsgForMemberModifyPoints(
			WechatMsgQueueComExp instance, String accessToken) {
		JSONObject obj = JSONObject.parseObject(instance.getMsgContent());
		String openId = instance.getOpenId();
		String reason = obj.getString("reason");// 原因
		String getDate = obj.getString("getDate"); // 获取时间
		String currentPoints = obj.getString("currentPoints");// 当前积分
		String getPoints = obj.getString("getPoints");// 获得积分
		JSONObject result = null;
		WechatTemplate template = new WechatTemplate();
		template.setTouser(openId);
		template.setTemplate_id(memberModifyPointsTemplateId);
		Map<String, TemplateData> map = new HashMap<String, TemplateData>();

		TemplateData firstTemp = new TemplateData();
		firstTemp.setColor(templateColor);
		firstTemp.setValue(memberModifyPointsFirst);
		map.put("first", firstTemp);

		TemplateData gDateTemp = new TemplateData();
		gDateTemp.setColor(templateColor);
		gDateTemp.setValue(getDate);
		map.put("keyword1", gDateTemp);

		TemplateData getPointsTemp = new TemplateData();
		getPointsTemp.setColor(templateColor);
		getPointsTemp.setValue(getPoints);
		map.put("keyword2", getPointsTemp);

		TemplateData reasonTemp = new TemplateData();
		reasonTemp.setColor(templateColor);
		reasonTemp.setValue(reason);
		map.put("keyword3", reasonTemp);

		TemplateData currentPointsTemp = new TemplateData();
		currentPointsTemp.setColor(templateColor);
		currentPointsTemp.setValue(currentPoints);
		map.put("keyword4", currentPointsTemp);

		TemplateData remarkTemp = new TemplateData();
		remarkTemp.setColor(templateColor);
		remarkTemp.setValue(memberModifyPointsRemark);
		map.put("remark", remarkTemp);
		template.setData(map);
		try {
			result = templateSend(accessToken, template);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return result;
	}

	/**
	 * sendMsgForCloseComplain:(关闭建议发送消息). <br/>
	 * Date: 2016年7月8日 上午11:07:24 <br/>
	 * scrmVersion standard
	 * 
	 * @author lutianshui
	 * @version jdk1.7
	 * @param queueComExp
	 * @param accessToken
	 * @return
	 */
	public static JSONObject sendMsgForCloseComplain(
			WechatMsgQueueComExp instance, String accessToken) {

		JSONObject obj = JSONObject.parseObject(instance.getMsgContent());
		String openId = instance.getOpenId();
		String complainId = obj.getString("complainId");
		String complainType = obj.getString("complainType");
		String complainStatus = obj.getString("complainStatus");
		String createTime = obj.getString("createTime");
		String complainCode = instance.getMsgKey();
		JSONObject result = null;
		String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx506f4b12c6e6292a&redirect_uri="
				+ "http://"
				+ domainName
				+ "/scrm-pe-wechat/index.html#!/src/findComplainForSatisfaction/"
				+ complainId
				+ "&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
		WechatTemplate template = new WechatTemplate();
		template.setUrl(url);
		template.setTouser(openId);
		template.setTemplate_id(closeComplainTemplateId);
		Map<String, TemplateData> map = new HashMap<String, TemplateData>();

		TemplateData serviceInfo = new TemplateData();
		serviceInfo.setColor(templateColor);
		serviceInfo.setValue(serviceInfoBefore + complainCode
				+ serviceInfoAfter);
		map.put("serviceInfo", serviceInfo);

		TemplateData serviceType = new TemplateData();
		serviceType.setColor(templateColor);
		serviceType.setValue(complainType);
		map.put("serviceType", serviceType);

		TemplateData serviceStatus = new TemplateData();
		serviceStatus.setColor(templateColor);
		serviceStatus.setValue(complainStatus);
		map.put("serviceStatus", serviceStatus);

		TemplateData time = new TemplateData();
		String templateTime = DateUtils.dateStrToNewFormat(createTime,
				DateUtils.DATE_PATTERN_YYYYMMDDHHmmss,
				DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2);
		time.setColor(templateColor);
		time.setValue(templateTime);
		map.put("time", time);

		TemplateData remark = new TemplateData();
		remark.setColor(templateColor);
		remark.setValue(closeComplainRemark);
		map.put("remark", remark);
		template.setData(map);
		try {
			result = templateSend(accessToken, template);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return result;
	}
}
