/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceUseRecordService.java
 * Package Name:com.sage.scrm.bk.coupon.service
 * Date:2015年11月19日下午5:58:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.coupon.model.CouponInstanceUseRecord;

/**
 * ClassName:CouponInstanceUseRecordService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:58:09 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponInstanceUseRecordService extends BaseService<CouponInstanceUseRecord>{

}

