/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CouponSingleSummaryDayService.java
 * Package Name:com.sage.scrm.service.report.service
 * Date:2015年12月17日上午10:26:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.report.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.coupon.model.CouponSingleSummaryDay;
import com.bw.adv.module.report.coupon.model.exp.CouponSingleSummaryDayExp;

/**
 * ClassName:CouponSingleSummaryDayService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 上午10:26:56 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponSingleSummaryDayService extends BaseService<CouponSingleSummaryDay> {

	/**
	 * 
	 * queryExpByExample:(根据条件查询券统计列表). <br/>
	 * Date: 2015年12月17日 下午3:27:54 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponSingleSummaryDayExp> queryExpByExample(Example example,Page<CouponSingleSummaryDayExp> page);
	
	/**
	 * 
	 * queryListByMonth:(券统计月查询). <br/>
	 * Date: 2015年12月17日 下午4:14:26 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param month
	 * @param page
	 * @return
	 */
	List<CouponSingleSummaryDayExp> queryListByMonth(Integer month,Page<CouponSingleSummaryDayExp> page);
	
	/**
	 * 
	 * callCouponSingleSummary:(调用优惠券日统计存储过程). <br/>
	 * Date: 2015年12月17日 下午6:36:49 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	void callCouponSingleSummary(String dateStr);
}

