package com.bw.adv.service.activity.service;

import com.bw.adv.module.component.rule.result.ActivityRuleResult;

import java.util.List;

/**
 * Created by Ronald on 2016/12/12.
 */
public interface ReleaseInviteMemberAwardService {

    /**
     * 发放邀请礼
     */
    public void releaseAward();
}
