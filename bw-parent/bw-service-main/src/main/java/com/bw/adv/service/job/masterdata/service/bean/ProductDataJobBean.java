package com.bw.adv.service.job.masterdata.service.bean;


import java.util.List;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.repository.ComTaskRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.ProductService;

public class ProductDataJobBean extends BaseJob {
	
	private ProductService productService;
	private ComTaskRepository comTaskRepository;
	
	@Override
	public void excute() throws Exception {
		initService();
		String serviceUrl = null;
		List<ComTask> comtask = null;
		try {
			serviceUrl = ProductDataJobBean.class.getName();
			comtask = comTaskRepository.findJobTaskByTypeId(serviceUrl);
			if(comtask.get(0).getLastRuntime().startsWith(DateUtils.getCurrentDateOfDb())){
				return;
			}
			productService.renovateProduct();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("同步数据修改产品信息");
	}
	
	private void initService() {
		this.productService = this.getApplicationContext().getBean(ProductService.class);
		this.comTaskRepository = this.getApplicationContext().getBean(ComTaskRepository.class);
	}


	@Override
	protected boolean isNeedLock() {
		return false;
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.MASTER_DATA_PRODUCT;
	}

}
