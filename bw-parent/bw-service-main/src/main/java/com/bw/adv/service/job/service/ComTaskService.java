/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ComTaskService.java
 * Package Name:com.sage.scrm.service.job.service
 * Date:2015年9月5日下午3:07:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.model.exp.ComTaskExp;

/**
 * ClassName:ComTaskService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月5日 下午3:07:05 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface ComTaskService extends BaseService<ComTask>{
	/**
	 * 
	 * queryAllExp:根据查询条件查询所有定时任务(扩展类，关联com_task_type表)
	 * Date: 2015年9月5日 下午3:22:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	public List<ComTaskExp> queryExpByExample(Example example,Page<ComTaskExp> page);

}

