/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberPointsItemSevice.java
 * Package Name:com.sage.scrm.service.points.service
 * Date:2015年8月20日下午6:17:16
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.badge.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.activity.model.Prize;
import com.bw.adv.module.badge.model.MemberBadgeItem;
import com.bw.adv.module.badge.model.exp.MemberBadgeExt;

/**
 * ClassName:MemberPointsItemSevice <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午6:17:16 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberBadgeItemSevice extends BaseService<MemberBadgeItem> {

	/**
	 * 保存徽章到用户
	 * @param memberId
	 * @param prize
	 */
	int saveMemberBadgeByMemberId(Long memberId, Long activityId,Long awardsId);

	/**
	 * 根据会员id查询徽章
	 * @param memberId
	 * @return
	 */
	List<MemberBadgeExt> queryMemberBadgeExtByMemberId(Long memberId);
	
	/**
	 * 判断会员是否存在兑换条件
	 * @param memberId
	 * @return
	 */
	boolean validMemberBadgeQualifications(Long memberId);
	
	/**
	 * 更新会员徽章（进行兑换）
	 * @param memberId
	 * @return
	 */
	int updateMemberBadgeGiftExchange(Long memberId);
}

