/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberCardService.java
 * Package Name:MemberGroupServiceImpl
 * Date:2015-11-16下午5:36:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.repository.MemberCardRepository;
import com.bw.adv.service.member.service.MemberCardService;

/**
 * ClassName:MemberCardService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-11-16 下午5:36:15 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class MemberCardServiceImpl extends BaseServiceImpl<MemberCard> implements MemberCardService{
	
	private MemberCardRepository memberCardRepository;
	
	@Override
	public BaseRepository<MemberCard, ? extends BaseMapper<MemberCard>> getBaseRepository() {
		return memberCardRepository;
	}

	@Override
	public MemberCard queryByCardNo(String cardNo) {
		return memberCardRepository.findByCardNo(cardNo);
	}
	
	@Override
	public MemberCard queryByMemberId(Long memberId) {
		return memberCardRepository.findByMemberId(memberId);
	}
	

	@Autowired
	public void setMemberCardRepository(MemberCardRepository memberCardRepository) {
		this.memberCardRepository = memberCardRepository;
	}

	
}

