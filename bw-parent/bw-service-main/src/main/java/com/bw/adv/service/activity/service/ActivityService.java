/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import java.util.List;

import net.sf.json.JSONObject;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.Activity;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.model.PhotoScoreResult;
import com.bw.adv.module.activity.model.exp.ActivityInstanceRecordExp;
import com.bw.adv.module.activity.model.exp.ActivityRuleConditionExp;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sys.model.SysUser;

/**
 * ClassName:ActivityService <br/>
 * Date: 2015-8-11 下午1:50:36 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface ActivityService extends BaseService<Activity>{

	/**
	 * findAllActive:查询所有活动<br/>
	 * Date: 2015-8-11 下午2:42:37 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<Activity> queryAll();
	
	/**
	 * 根据活动类型查询
	 * @param TypeId
	 * @return
	 */
	List<Activity> queryByTypeId(Long typeId);
	
	/**
	 * startActivity:发起活动  <br/>
	 * Date: 2015-8-25 下午8:58:03 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param jsonObject
	 * @param sysUser
	 */
	void startActivity(JSONObject jsonObject,SysUser sysUser) throws Exception;
	
	/**
	 * queryActivityInstanceByActivityId:查询活动的实例<br/>
	 * Date: 2015-8-26 下午6:31:54 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityId
	 */
	List<ActivityInstance> queryActivityInstanceByActivityId(Long activityId);
	
	
	/**
	 * queryActivityInstanceByActivityId:(根据活动ID查询活动实例，分页). <br/>
	 * Date: 2015-8-26 下午6:37:12 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityId
	 * @param page
	 * @return
	 */
	List<ActivityInstance> queryActivityInstanceByActivityId(Long activityId,Page<ActivityInstance> page);
	
	
	/**
	 * queryActivityInstanceRecordByInstanceId:(根据活动实例ID查询活动实例记录，分页). <br/>
	 * Date: 2015-8-27 下午3:17:10 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityInstanceId
	 * @param page
	 * @return
	 */
	public List<ActivityInstanceRecordExp> queryActivityInstanceRecordByInstanceId(Long activityInstanceId, Page<ActivityInstanceRecordExp> page);
	
	/**
	 * 
	 * queryActivityInstanceRecordByExample:(根据example查询活动实例记录，分页). <br/>
	 * Date: 2015年9月30日 下午3:02:25 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<ActivityInstanceRecordExp> queryActivityInstanceRecordByExample(Example example, Page<ActivityInstanceRecordExp> page);
	/**
	 * queryActivityConditionByInstanceId:(查询活动的条件). <br/>
	 * Date: 2015-9-1 下午6:10:01 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param instanceId
	 * @return
	 */
	List<ActivityRuleConditionExp> queryActivityConditionByInstanceId(Long instanceId);
	
	/**
	 * queryActivityInstanceByInstanceId:(查询活动实例). <br/>
	 * Date: 2015-9-1 下午6:14:11 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param instanceId
	 * @return
	 */
	ActivityInstance queryActivityInstanceByInstanceId(Long instanceId);
	
	/**
	 * updateActivity:(修改活动实例). <br/>
	 * Date: 2015-9-3 下午6:37:36 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param jsonObject
	 * @param sysUser
	 */
	public void updateActivity(JSONObject jsonObject,SysUser sysUser) throws Exception;
	
	/**
	 * 
	 * updateInstanceStatus:手动终止活动
	 * Date: 2015年9月18日 下午5:40:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 */
	public void updateInstanceStatus(SysUser sysUser,Long activityInstanceId);
	
	/**
	 * 保存照片分析结果
	 * @param photoScoreResult
	 */
	public void savePhotoScoreResult(PhotoScoreResult photoScoreResult);
	
}

