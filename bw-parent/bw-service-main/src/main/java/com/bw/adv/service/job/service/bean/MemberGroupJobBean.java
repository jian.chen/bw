package com.bw.adv.service.job.service.bean;


import java.util.List;

import org.springframework.stereotype.Component;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.utils.MyConstants;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.search.MemberGroupSearch;
import com.bw.adv.service.activity.service.ActivityActionService;
import com.bw.adv.service.member.service.MemberGroupService;
public class MemberGroupJobBean extends BaseJob {
	
	private MemberGroupService memberGroupService;
	
	@Override
	public void excute() throws Exception {
		logger.info("会员分组定时任务开始");
		initService();
		MemberGroupSearch search = null;
		List<MemberGroup> result = null;
		try {
			search = new MemberGroupSearch();
			search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
			search.setEqualExecuteType(MyConstants.MEMBER_GROUP_TYPE.Dynamic);
			result = this.memberGroupService.queryListSearchAuto(search);
			if(result != null && result.size()>0){
				for(int i=0;i<result.size();i++){
					try {
						memberGroupService.excuteMemberGroupJob(result.get(i));
					} catch (Exception e) {
						logger.error(e.getMessage()+"-----------分组执行失败，名称为：-----"+result.get(i).getMemberGroupName());
						e.printStackTrace();
						continue;
					}
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("会员分组定时任务结束");
	}
	
	private void initService() {
		this.memberGroupService = this.getApplicationContext().getBean(MemberGroupService.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.MEMBER_GROUP_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

}
