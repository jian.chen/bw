/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:FansChannelSummaryDayService.java
 * Package Name:com.sage.scrm.service.report.service
 * Date:2015年11月30日上午10:06:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.report.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.member.model.FansChannelSummaryDay;
import com.bw.adv.module.report.member.model.exp.FansChannelSummaryDayExp;

/**
 * ClassName:FansChannelSummaryDayService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月30日 上午10:06:24 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface FansChannelSummaryDayService extends BaseService<FansChannelSummaryDay>{

	/**
	 * 
	 * queryListByExample:(查询粉丝渠道报表列表). <br/>
	 * Date: 2015年11月30日 上午10:07:25 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<FansChannelSummaryDayExp> queryListByExample(Example example,Page<FansChannelSummaryDayExp> page);
	
	/**
	 * 
	 * callFansChannelSummary:(调用粉丝渠道存储过程). <br/>
	 * Date: 2015年11月30日 下午3:06:04 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param dateStr
	 */
	public void callFansChannelSummary(String dateStr);
	
}

