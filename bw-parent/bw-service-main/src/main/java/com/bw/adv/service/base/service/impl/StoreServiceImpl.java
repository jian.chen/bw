/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:StoreServiceImpl.java
 * Package Name:com.sage.scrm.service.base.service.impl
 * Date:2015年8月18日下午7:15:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.model.exp.StoreExp;
import com.bw.adv.module.base.repository.OrgRepository;
import com.bw.adv.module.base.repository.StoreRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.coupon.model.exp.StoreData;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.service.base.service.StoreService;

/**
 * ClassName:StoreServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午7:15:24 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class StoreServiceImpl extends BaseServiceImpl<Store> implements StoreService {

	private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);
	
	
	private StoreRepository storeRepository;
	private OrgRepository orgRepository;
	
	@Override
	public BaseRepository<Store, ? extends BaseMapper<Store>> getBaseRepository() {
		return storeRepository;
	}

	@Override
	public List<Store> queryStoreByStatusId(Page<Store> page) {
		return storeRepository.findByStatusId(StatusConstant.STORE_ACTIVE.getId(),page);
	}
	
	@Override
	public int deleteStoreByPk(Long storeId){
		Store store = new Store();
		store.setStoreId(storeId);
		store.setStatusId(StatusConstant.STORE_REMOVE.getId());
		return storeRepository.updateByPkSelective(store);
	}
	
	@Override
	public int deleteStoreList(List<Long> storeIds) {
		return storeRepository.deleteStatusIdList(StatusConstant.STORE_REMOVE.getId(),storeIds);
	}

	@Override
	public Org queryOrgByPk(Long OrgId) {
		return orgRepository.findByPk(OrgId);
	}


	@Override
	public StoreExp queryById(Long storeId) {
		return storeRepository.findByPk(storeId);
	}
	

	@Override
	public Store queryByCode(String code) {
		return storeRepository.findByStoreCode(code);
	}

	@Override
	public void renovateStore() {
		String rulePath = "masterData/rules/store-rule.xml";
		Digester digester = null;
		ClassLoader loader = null;
		StoreData storeData = null; 
		List<Store> storeList = null;
		List<Store> storeOldList = null;
		Map<String, Store> storeMap = null;
		List<Store> storeNewList = null;
		List<Store> storeUpdateList = null;
		Store storeTmp = null;
		File file = null;
		String storeName = null;
		String storeNameEn = null;
		try {
	    	loader = Thread.currentThread().getContextClassLoader();
	    	digester = DigesterLoader.createDigester(loader.getResource(rulePath));
	    	
	    	storeData = (StoreData) digester.parse(file);
	    	storeList = storeData.getStoreList();
	    	
	    	
	    	storeNewList = new ArrayList<Store>();
	    	storeUpdateList = new ArrayList<Store>();
	    	
	    	storeMap = new HashMap<String, Store>();
	    	storeOldList = storeRepository.findAll();
	    	
	    	for (Store store : storeOldList) {
	    		storeMap.put(store.getStoreCode(), store);
			}
	    	
	    	for (Store storeMaster : storeList) {
	    		storeTmp = storeMap.get(storeMaster.getStoreCode());
	    		storeMaster.setStatusId(StatusConstant.STORE_ACTIVE.getId());
	    		storeName = storeMaster.getStoreName(); 
	    		if(StringUtils.isNotBlank(storeName) && storeName.lastIndexOf("-") > 0){
	    			storeNameEn = storeName.substring(0,storeName.lastIndexOf("-"));;
	    			storeName = storeName.substring(storeName.lastIndexOf("-")+1);
	    			storeMaster.setField5(storeNameEn);
	    			storeMaster.setStoreName(storeName);
	    		}
	    		if(storeTmp != null){
	    			BeanUtils.copyNotNull(storeMaster, storeTmp);
	    			storeUpdateList.add(storeTmp);
	    		}else{
	    			storeNewList.add(storeMaster);
	    		}
			}
	    	if(storeUpdateList.size() > 0){
	    		storeRepository.updateList(storeUpdateList);
	    	}
	    	if(storeNewList.size() > 0){
	    		storeRepository.saveList(storeNewList);
	    	}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Store> queryStoreList() {
		return storeRepository.findStoreList();
	}

	@Override
	public Store queryByOrgId(Long orgId) {
		return this.storeRepository.queryByOrgId(orgId);
	}

	@Override
	public void saveStoreList(List<Store> list) {
		this.storeRepository.saveList(list);
	}

	@Autowired
	public void setStoreRepository(StoreRepository storeRepository) {
		this.storeRepository = storeRepository;
	}

	@Autowired
	public void setOrgRepository(OrgRepository orgRepository) {
		this.orgRepository = orgRepository;
	}

}

