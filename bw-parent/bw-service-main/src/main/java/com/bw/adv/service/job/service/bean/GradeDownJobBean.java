/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:GradeJobBean.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2015-9-17下午2:02:37
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.service.activity.handle.GradeHandleDownService;

/**
 * ClassName:GradeJobBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-9-17 下午2:02:37 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public class GradeDownJobBean extends BaseJob{
	
	GradeHandleDownService gradeHandleDownService;

	@Override
	protected void excute() throws Exception {
		gradeHandleDownService.operateGradeDownBusiness();
	}

	@Override
	protected String getLockCode() {
		return null;
	}

	@Override
	protected boolean isNeedLock() {
		return false;
	}

	@Autowired
	public void setGradeHandleDownService(GradeHandleDownService gradeHandleDownService) {
		this.gradeHandleDownService = gradeHandleDownService;
	}
	
}

