package com.bw.adv.service.job.service.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.member.service.MemberService;

public class MemberSummaryJobBean extends BaseJob{
	
	private static final Logger logger = LoggerFactory.getLogger(MemberSummaryJobBean.class);
	private MemberService memberService;

	@Override
	public synchronized void excute() throws Exception {
		init();
		logger.info("会员报表任务开始");
		String dateStr = DateUtils.addDays(-1);//昨天天日期
		String weekStr = DateUtils.addDays(-8);//七天前的日期
		String monthStr = DateUtils.getLastDayOfMonth(DateUtils.addMonths(-1));//上个最后一天的日期
		memberService.callMemberSummaryDay(dateStr, weekStr, monthStr);
		logger.info(dateStr+"|"+weekStr+"|"+monthStr+"|会员报表任务结束");
	}
	
	public void init(){
		this.memberService = this.getApplicationContext().getBean(MemberService.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.MEMBER_SUMMARY_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}
	
}
