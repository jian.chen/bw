package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.module.activity.model.InviteMemberRecord;
import com.bw.adv.module.activity.repository.ActivityInstanceRecordRepository;
import com.bw.adv.module.activity.repository.InviteMemberRecordRepository;
import com.bw.adv.module.component.rule.result.ActivityRuleResult;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.ReleaseInviteMemberAwardService;

/**
 * Created by Ronald on 2016/12/12.
 */
@Service
public class ReleaseInviteMemberAwardServiceImpl implements ReleaseInviteMemberAwardService{
    @Autowired
    private ActivityInstanceRecordRepository activityInstanceRecordRepository;

    @Autowired
    private InviteMemberRecordRepository inviteMemberRecordRepository;

    @Override
    public void releaseAward() {
        List<InviteMemberRecord> records = inviteMemberRecordRepository.getAllValidInviteMemberRecordOfToday(DateUtils.formatCurrentDate("yyyyMMdd"));
        for (InviteMemberRecord r: records){
            ActivityRuleResult result = new ActivityRuleResult();
            //RuleDroolsTools.setResult(result, r.getMemberId(), r.getActivityInstanceId(), r.getHappenTime(), "新会员邀请奖励");
            r.setIsReleased(true);
            r.setReleaseAt(DateUtils.formatCurrentDate("yyyyMMddHHmmss"));
            inviteMemberRecordRepository.updateByPk(r);
        }
    }
}
