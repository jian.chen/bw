/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:sysFunction.java
 * Package Name:com.sage.scrm.service.sys.service
 * Date:2015-8-17下午4:20:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.sys.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.exp.SysFunctionExp;

/**
 * ClassName:sysFunction <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-17 下午4:20:39 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
@Service
public interface SysFunctionService extends BaseService<SysFunction> {

	/**
	 * findListForSysFunction:查询所有的功能List. <br/>
	 * Date: 2015-9-2 上午11:29:07 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 */
	List<SysFunction> findListForSysFunction(Long systemId);

	List<SysFunction> findListForSysFunctionByPid(Long parentid);

	/**
	 * findListForSysFunctionByUserId:通过用户id查询用户功能list. <br/>
	 * Date: 2015-9-2 上午11:26:03 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param userId
	 * @return
	 */
	List<SysFunction> findListForSysFunctionByUserId(Long userId, Long systemId);

	/**
	 * findSysFunctionByUser:(获取角色对应权限的操作菜单). <br/>
	 * Date: 2015年9月23日 下午2:55:45 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @param user
	 * @return
	 */
	Map<String, Object> findSysFunctionByUser(HttpServletRequest request,HttpServletResponse response,List<SysFunction> functionList);


	/**
	 * 查询所有权限和按钮
	 * @return
	 */
	List<SysFunctionExp> selectSysFunctionListAndOperate(Long systemId);

	/**
	 * 查询2级菜单
	 * @return
	 */
	List<SysFunctionExp> selectSecondFunction(Long systemId);

}

