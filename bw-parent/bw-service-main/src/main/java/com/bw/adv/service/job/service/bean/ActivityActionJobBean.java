package com.bw.adv.service.job.service.bean;


import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.service.activity.service.ActivityActionService;

public class ActivityActionJobBean extends BaseJob {
	
	private ActivityActionService activityActionService; 
	
	@Override
	public void excute() throws Exception {
		initService();
		try {
			activityActionService.scanActivityAction();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("积分计算任务执行完成");
	}
	
	private void initService() {
		this.activityActionService = this.getApplicationContext().getBean(ActivityActionService.class);
	}

	@Override
	protected String getLockCode() {
		return LockCodeConstant.MEMBER_POINTS_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}

}
