/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberCardGradeService.java
 * Package Name:com.sage.scrm.service.base.service
 * Date:2015年9月6日下午4:40:49
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.member.model.MemberCardGrade;

/**
 * ClassName:MemberCardGradeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月6日 下午4:40:49 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberCardGradeService extends BaseService<MemberCardGrade>{
	
	public List<MemberCardGrade> queryAll();
	
	/**
	 * 
	 * updateSrcByPk:根据ID修改src和styleId两个字段
	 * Date: 2015年9月7日 上午11:15:58 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberCardGrade
	 * @return
	 */
	public int updateSrcByPk(MemberCardGrade memberCardGrade);

}

