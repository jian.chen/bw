/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:sysFunction.java
 * Package Name:com.sage.scrm.service.sys.service
 * Date:2015-8-17下午4:20:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.sys.service;


import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.sys.model.SysFunctionOperation;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName:sysFunction <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-17 下午4:20:39 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
@Service
public interface SysFunctionOperationService extends BaseService<SysFunctionOperation> {

	/**
	 * 获取所有的默认功能操作
	 * @return
	 */
	public List<SysFunctionOperation> queryDefaultFunctionOperations();

	/**
	 * 获取所有的功能操作
	 * @return
	 */
	public List<SysFunctionOperation> queryAllFunctionOperation();

	/**
	 * 查询用户的操作权限
	 * @param userId
	 * @return
	 */
	public List<SysFunctionOperation> queryButtonFunctionOperationsByUserId(Long userId);

}

