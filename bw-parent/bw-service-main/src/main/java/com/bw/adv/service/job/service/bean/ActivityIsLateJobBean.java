/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityIsLateJobBean.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2015年9月28日上午11:45:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.service.activity.service.ActivityActionService;

/**
 * ClassName:ActivityIsLateJobBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月28日 上午11:45:34 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class ActivityIsLateJobBean extends BaseJob{
	
	private ActivityActionService activityActionService;

	@Override
	protected void excute() throws Exception {
		initService();
		
		try {
			activityActionService.checkActivityDate();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		
		logger.info("活动是否过期任务执行完成");
	}

	private void initService(){
		this.activityActionService = this.getApplicationContext().getBean(ActivityActionService.class);
	}
	
	@Override
	protected String getLockCode() {
		
		return LockCodeConstant.ACTIVITY_IS_LATE_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		
		return true;
	}

}

