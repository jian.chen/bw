/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceBrowseService.java
 * Package Name:com.sage.scrm.bk.coupon.service
 * Date:2015年11月19日下午5:17:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.coupon.model.CouponInstanceBrowse;

/**
 * ClassName:CouponInstanceBrowseService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:17:42 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponInstanceBrowseService extends BaseService<CouponInstanceBrowse>{

}

