package com.bw.adv.service.member.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberPrism;
import com.bw.adv.module.member.repository.MemberPrismConditionRepository;
import com.bw.adv.module.member.repository.MemberPrismRepository;
import com.bw.adv.service.member.service.MemberPrismService;

@Service
public class MemberPrismServiceImpl implements MemberPrismService{
	
	private MemberPrismRepository memberPrismReposiotry;
	
	private MemberPrismConditionRepository memberPrismConditionRepository;

	@Autowired
	public void setMemberPrismReposiotry(MemberPrismRepository memberPrismReposiotry) {
		this.memberPrismReposiotry = memberPrismReposiotry;
	}

	@Autowired
	public void setMemberPrismConditionRepository(MemberPrismConditionRepository memberPrismConditionRepository) {
		this.memberPrismConditionRepository = memberPrismConditionRepository;
	}

	@Override
	public List<MemberPrism> queryAllMemberPrism(Page<MemberPrism> templatePage) {
		return this.memberPrismReposiotry.selectAllPrism(templatePage);
	}
	
}
