/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatSendMesageJobBean.java
 * Package Name:com.sage.scrm.service.job.service.bean
 * Date:2015年12月9日下午6:57:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.job.service.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.module.common.constant.LockCodeConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.enums.WechatMsgBusinessTypeEnum;
import com.bw.adv.module.wechat.model.WechatMsgQueue;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueComExp;
import com.bw.adv.module.wechat.model.exp.WechatMsgQueueExp;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.wechat.msg.util.WechatApiUtil;
import com.bw.adv.service.wechat.service.WechatMsgQueueService;

/**
 * ClassName:WechatSendMesageJobBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午6:57:05 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class WechatSendMesageJobBean extends BaseJob{
	@Value("${limit}")
	private static String limit ;
	@Value("${send_wechat_msg_time_period}")
	private static String EXECUTE_TIME_PERIOD;
	
	private WechatMsgQueueService wechatMsgQueueService;
	private AccessTokenCacheService accessTokenCacheService;
	
	
	@Override
	protected void excute() throws Exception {
		try {
			initService();
			//判断是否执行时间满足时间
			if(!checkTimePeriod(EXECUTE_TIME_PERIOD)){
				return;
			}
			wechatMsgQueueService.sendWechatMsgMultiThread();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
//		logger.info("微信发送消息任务执行完成");
	}
	
	
	
	@Deprecated
	protected void excute_bak() throws Exception {
		List<WechatMsgQueueExp> list = null;
		List<WechatMsgQueueExp> failList = null;
		String accessToken = null;
		String currentTime = null;
		try {
			initService();
			list = wechatMsgQueueService.findAllWechatMsgQueues(Long.valueOf(limit));
			if(StringUtils.isEmpty(limit)){
				limit = "100";
			}
			
			//判断是否执行时间满足时间
			if(!checkTimePeriod(EXECUTE_TIME_PERIOD)){
				return;
			}
			
			if(null != list && list.size() > 0){
				failList = new ArrayList<WechatMsgQueueExp>();
				accessToken = accessTokenCacheService.getAccessTokenCache();
				currentTime = DateUtils.getCurrentTimeOfDb();
				for (WechatMsgQueueExp instance : list) {
					JSONObject result = null;
					instance.setSendTime(currentTime);
					if(instance.getBusinessType().equals(WechatMsgBusinessTypeEnum.COUPON_EXPIRE.getId())){
						result = sendMsgForCouponExpire(instance,accessToken);
						String errcode = result.getString("errcode");
						logger.info(
								"------任务消息----WechatSendMesageJobBean--->memberId:"+instance.getMemberId()
								+",openId:"+instance.getOpenId()
								+",msgKey:"+instance.getMsgKey()
								+",businessType:"+instance.getBusinessType()
								+",result:"+result
								);
						//如果发送失败，更新这条记录
						if(!errcode.equals(StatusConstant.WECHAT_ERRCODE_SUC.getId().toString())){
							instance.setResultMsg(result+"");
							failList.add(instance);
						}
					}
				}
				if(failList.size() > 0){
					for(WechatMsgQueueExp failInstance : failList){
						list.remove(failInstance);
						failInstance.setIsSend("E");
					}
					wechatMsgQueueService.updateBatchWechatMsgQueue(failList);
				}
				if(list.size() > 0){
					wechatMsgQueueService.deleteWechatMsgQueue(list);
				}
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		logger.info("微信发送消息任务执行完成");
	}
	
	
	private static boolean checkTimePeriod(String timePeriod){
		String startTime = null;
		String endTime = null;
		String currentTime = null;
		
		//加时间段的判断
		if(StringUtils.isNotBlank(timePeriod) && timePeriod.split("-").length == 2){
			startTime = timePeriod.split("-")[0];
			endTime = timePeriod.split("-")[1];
			currentTime = DateUtils.getCurrentTimeStr();
			//如果开始时间大约结束时间
			if(startTime.compareTo(endTime) >= 0){
				if(currentTime.compareTo(endTime) > 0 && currentTime.compareTo(startTime) < 0){
					return false;
				}
			}
			if(startTime.compareTo(endTime) <= 0){
				if(!(currentTime.compareTo(startTime) > 0 && currentTime.compareTo(endTime) < 0)){
					return false;
				}
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(checkTimePeriod("080000-220000"));
	}

	/*//优惠券领取
	private JSONObject sendMsgForCouponReceive(WechatMsgQueueExp instance){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForCouponReceive(queueComExp, accessTokenCacheService.getAccessTokenCache());
	}
	
	//消费提醒
	private JSONObject sendMsgForConsumption(WechatMsgQueue instance){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForConsumption(queueComExp, accessTokenCacheService.getAccessTokenCache());
	}*/
	
	/**
	 * 
	 * sendMsgForCouponExpire:(优惠券过期发送消息). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param instance
	 * @return
	 */
	private JSONObject sendMsgForCouponExpire(WechatMsgQueue instance,String accessToken){
		WechatMsgQueueComExp queueComExp = new WechatMsgQueueComExp();
		BeanUtils.copy(instance, queueComExp);
		return WechatApiUtil.sendMsgForCouponExpire(queueComExp, accessToken);
	}
	
	private void initService() {
		this.wechatMsgQueueService = this.getApplicationContext().getBean(WechatMsgQueueService.class);
		this.accessTokenCacheService = this.getApplicationContext().getBean(AccessTokenCacheService.class);
	}
	
	
	@Override
	protected String getLockCode() {
		return LockCodeConstant.WECHAT_SEND_MESAGE_JOB_CODE;
	}

	@Override
	protected boolean isNeedLock() {
		return true;
	}
	

}

