package com.bw.adv.service.sys.log.service;


import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.msg.model.SmsTempParams;
import com.bw.adv.module.sys.constant.LogEventConstant;
import com.bw.adv.module.sys.constant.LogTypeConstant;
import com.bw.adv.module.sys.enums.LogNoteEnum;
import com.bw.adv.module.sys.model.SysLog;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.exp.SysLogExp;

public interface SysLogService extends BaseService<SysLog>{
	
	/**
	 * log:(记录日志). <br/>
	 * Date: 2015-8-18 下午3:49:28 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * 
	 * @param logTypeConstant：日志类型
	 * @param logEventConstant：日志事件
	 * @param logNode：日志节点
	 * @param keyword：关键字
	 * @param remark：备注
	 * @param source：原始bean
	 * @param update：更新后bean
	 */
	public void log(LogTypeConstant logTypeConstant, LogEventConstant logEventConstant, LogNoteEnum logNoteEnum, String keyword, String remark, Object source, Object update);
	
	/**
	 * 
	 * queryByExample:根据条件查询日志list
	 * Date: 2015年8月30日 下午4:24:46 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	public List<SysLogExp> queryByExample(Example example,Page<SysLogExp> page);
	
	/**
	 * 
	 * querySysUserList:查看用户列表
	 * Date: 2015年8月30日 下午8:56:20 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<SysUser> querySysUserList();
	
	
	
}
