/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMsgQueueHistoryServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年12月9日下午9:45:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.wechat.model.WechatMsgQueueHistory;
import com.bw.adv.module.wechat.repository.WechatMsgQueueHistoryRepository;
import com.bw.adv.service.wechat.service.WechatMsgQueueHistoryService;

/**
 * ClassName:WechatMsgQueueHistoryServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 下午9:45:29 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatMsgQueueHistoryServiceImpl extends BaseServiceImpl<WechatMsgQueueHistory> implements WechatMsgQueueHistoryService {
	private WechatMsgQueueHistoryRepository wechatMsgQueueHistoryRepository;
	
	public void saveAllWechatMsgQueueHistory(List<WechatMsgQueueHistory> historys){
		wechatMsgQueueHistoryRepository.saveAllWechatMsgQueueHistory(historys);
	}
	
	@Override
	public BaseRepository<WechatMsgQueueHistory, ? extends BaseMapper<WechatMsgQueueHistory>> getBaseRepository() {
		return wechatMsgQueueHistoryRepository;
	}
	
	@Autowired
	public void setWechatMsgQueueHistoryRepository(WechatMsgQueueHistoryRepository wechatMsgQueueHistoryRepository) {
		this.wechatMsgQueueHistoryRepository = wechatMsgQueueHistoryRepository;
	}
}

