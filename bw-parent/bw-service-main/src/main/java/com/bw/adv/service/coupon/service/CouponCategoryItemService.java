/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:CouponCategoryItemService.java
 * Package Name:com.sage.scrm.service.coupon.service
 * Date:2015年12月28日下午2:54:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.coupon.service;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.coupon.model.CouponCategoryItem;

/**
 * ClassName:CouponCategoryItemService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月28日 下午2:54:41 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponCategoryItemService extends BaseService<CouponCategoryItem>{

}

