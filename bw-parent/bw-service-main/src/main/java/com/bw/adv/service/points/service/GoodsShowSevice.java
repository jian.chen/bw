/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:GoodsShowSevice.java
 * Package Name:com.sage.scrm.service.points.service
 * Date:2015年12月9日上午11:12:49
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.points.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.goods.model.GoodsExchangeItem;
import com.bw.adv.module.goods.model.GoodsShow;
import com.bw.adv.module.goods.model.GoodsShowCategory;
import com.bw.adv.module.goods.model.exp.GoodsShowExp;

/**
 * ClassName:GoodsShowSevice <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 上午11:12:49 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface GoodsShowSevice extends BaseService<GoodsShow> {

	/**
	 * 
	 * queryList:(根据状态查询商品展示列表(不等于传入状态的所有数据)). <br/>
	 * Date: 2015年12月9日 上午11:14:20 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param pageObj
	 * @param statusId
	 * @return
	 */
	public List<GoodsShowExp> queryList(Page<GoodsShowExp> pageObj,Long statusId);
	
	/**
	 * 
	 * deleteList:(批量删除商品展示(逻辑删除)). <br/>
	 * Date: 2015年12月9日 下午4:16:57 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowIds
	 * @return
	 */
	public int deleteList(String goodsShowIds);
	
	/**
	 * 
	 * queryExpByPk:(根据Id查询商品展示详细信息). <br/>
	 * Date: 2015年12月10日 下午2:59:17 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowId
	 * @return
	 */
	public GoodsShowExp queryExpByPk(String goodsShowId);
	
	/**
	 * 
	 * queryGoodsShowCategory:(查询所有有效的商品展示类别). <br/>
	 * Date: 2015年12月23日 下午4:42:25 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<GoodsShowCategory> queryGoodsShowCategory(Page<GoodsShowCategory> pageObj);
	
	/**
	 * 
	 * queryCategoryDetail:(查询商品展示类型详情). <br/>
	 * Date: 2015年12月25日 上午11:04:38 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowCategoryId
	 * @return
	 */
	public GoodsShowCategory queryCategoryDetail(Long goodsShowCategoryId);
	
	/**
	 * 
	 * updateCategory:(修改商品展示类型). <br/>
	 * Date: 2015年12月25日 上午11:05:59 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowCategory
	 */
	public void updateCategory(GoodsShowCategory goodsShowCategory);
	
	/**
	 * 
	 * saveCategory:(新建商品展示类型). <br/>
	 * Date: 2015年12月25日 上午11:11:09 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowCategory
	 */
	public void saveCategory(GoodsShowCategory goodsShowCategory);
	
	/**
	 * 
	 * deleteGoodsShowCategoryList:(批量删除商品展示类别). <br/>
	 * Date: 2015年12月23日 下午4:43:46 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowCategoryIds
	 * @return
	 */
	public int deleteGoodsShowCategoryList(String goodsShowCategoryIds);
	
	/**
	 * exchangePointsProduct:积分兑换 <br/>
	 * Date: 2015年12月31日 下午2:47:19 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param goodsExchangeItem
	 * @param couponId
	 * @throws Exception
	 */
	public void exchangePointsProduct(GoodsExchangeItem goodsExchangeItem,Long couponId,String source) throws Exception;
}

