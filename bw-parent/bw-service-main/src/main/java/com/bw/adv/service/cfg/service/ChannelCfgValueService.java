package com.bw.adv.service.cfg.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.cfg.model.ChannelCfgValue;
import com.bw.adv.module.cfg.model.ChannelCfgValueExp;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.sys.model.SysUser;

import net.sf.json.JSONObject;
/**
 * ClassName: ChannelCfgValueService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月21日 下午4:15:55 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
public interface ChannelCfgValueService extends BaseService<ChannelCfgValue> {

	/**
	 * 更新或创建配置条件
	 * updateCfgConditionValue:(说明). <br/>
	 * Date: 2016年11月21日 <br/>
	 * scrmVersion 11.2
	 * @author cjian
	 * @version jdk1.7
	 * @param json
	 */
	public void modifyCfgConditionValue(SysUser sysUser,JSONObject json);
	
	/**
	 * 注册之后送积分或者优惠券
	 * afterRegiter:(注册后). <br/>
	 * Date: 2016年11月21日 <br/>
	 * scrmVersion 11.2
	 * @author cjian
	 * @version jdk1.7
	 * @param memberDto
	 */
	public void afterRegiter(Long memberId,Long channelId);
	
	
	/**
	 * 根据配置查询条件:(说明). <br/>
	 * Date: 2016年11月24日 <br/>
	 * scrmVersion 11.2
	 * @author cjian
	 * @version jdk1.7
	 * @param channelCfgId
	 * @return
	 */
	public List<ChannelCfgValueExp> queryConditionByChannelCfgId(Long channelCfgId);
	
}