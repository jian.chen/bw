/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:LotteryService.java
 * Package Name:com.sage.scrm.service.activity.service
 * Date:2015年11月24日下午2:36:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service;

import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.activity.model.exp.AwardsSettingExp;

/**
 * ClassName:LotteryService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月24日 下午2:36:06 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface LotteryService {
	
	/**
	 * 
	 * extractMemberAwards:(会员抽奖方法). <br/>
	 * memberId 会员id  activityId:活动id
	 * Date: 2015年11月24日 下午2:49:42 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberId
	 * @param activityId
	 * @return
	 */
	public WinningItem extractMemberAwards(Long memberId, Long activityId,Long qrCodeId);
	
	/**
	 * 抽奖@PE
	 * @param memberId
	 * @param activityId
	 * @param qrCodeCode
	 * @return
	 */
	public AwardsSettingExp extractMemberAwardsMaps(Long memberId, Long activityId,String qrCode);
	
	/**
	 * addMemberWinningAccord:(新增抽奖机会). <br/>
	 * Date: 2016-6-15 下午5:16:19 <br/>
	 * scrmVersion standard
	 * @author mennan
	 * @version jdk1.7
	 * @param openid
	 * @param activityId
	 */
	public void addMemberWinningAccord(Long memberId,Long activityId,String mapsQrCode);
	
	/**
	 * 查询并可能操作抽奖机会
	 * @param memberId
	 * @param activityId
	 * @return
	 */
	public Long queryMemberLotteryChance(Long memberId,Long activityId,Long qrCodeId);

	/**
	 * 执行徽章抽奖
	 * @param memberId
	 * @param qrCodeId
	 * @param activityId
	 * @return
	 */
	AwardsSettingExp extractMemberAwardsEveryMonth(Long memberId, Long qrCodeId,Long activityId);
	
	
}

