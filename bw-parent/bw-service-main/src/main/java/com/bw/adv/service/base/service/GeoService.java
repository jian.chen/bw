/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:GeoService.java
 * Package Name:com.sage.scrm.service.base.service
 * Date:2015年8月26日下午4:03:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.base.model.Geo;

/**
 * ClassName:GeoService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月26日 下午4:03:28 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface GeoService extends BaseService<Geo> {
	
	/**
	 * 
	 * queryByParentId:根据父节点Id查询子区域信息
	 * Date: 2015年8月26日 下午4:04:50 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param geoId
	 * @return
	 */
	public List<Geo> queryByParentId(Long geoId);

}

