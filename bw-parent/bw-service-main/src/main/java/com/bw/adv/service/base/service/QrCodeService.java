/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service;


import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.base.model.QrCode;
import com.bw.adv.module.base.model.exp.QrCodeExp;
import com.bw.adv.module.wechat.model.ChannelQrCode;


/**
 * ClassName: QrCodeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-11-3 下午5:01:19 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface QrCodeService extends BaseService<QrCode>{

	/**
	 * queryListForQrCode:(查询二维码列表). <br/>
	 * Date: 2015-11-3 下午5:01:24 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param isActive
	 * @param page
	 * @return
	 */
	List<QrCode> queryListForQrCode(String isActive,Page<QrCode> page);
	
	/**
	 * queryQrCodeByPrimaryKey:(查询二维码明细). <br/>
	 * Date: 2015-11-5 下午3:01:01 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param orCodeId
	 * @return
	 */
	QrCodeExp queryQrCodeByPrimaryKey(Long qrCodeId);
	
	/**
	 * removeQrCode:(删除二维码). <br/>
	 * Date: 2015-11-5 下午5:45:35 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param qrCodeId
	 * @return
	 */
	int removeQrCode(String qrCodeIds);
	
	/**
	 * 
	 * queryChannelQrCodeList:(查询所有有效的二维码类型list). <br/>
	 * Date: 2015年11月26日 下午6:45:46 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	List<Channel> queryChannelQrCodeList();

	/**
	 * 
	 * queryChannelQrCodeList:(查询所有有效的二维码类型list). <br/>
	 * Date: 2015年11月26日 下午6:45:46 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	QrCode queryQrCodeByQrCode(String qrCodeCode);
	
}

