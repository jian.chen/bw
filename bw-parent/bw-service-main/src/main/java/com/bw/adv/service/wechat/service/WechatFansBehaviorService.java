/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansBehaviorService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月19日下午3:45:10
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import net.sf.json.JSONObject;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansBehavior;

/**
 * ClassName:WechatFansBehaviorService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午3:45:10 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansBehaviorService extends BaseService<WechatFansBehavior> {
	
	/**
	 * 关注--
	 * save:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年8月28日 下午4:25:15 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFanId
	 * @param wechatAccountId
	 * @param resultJson
	 * @return
	 */
	public JSONObject save(String wechatFanId, String wechatAccountId, JSONObject resultJson);
	
	/**
	 * 
	 * saveUnSubscribeFansBehavior:(取消关注). <br/>
	 * Date: 2015年8月28日 下午4:25:31 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param fromUserName
	 * @param wechatAccountId
	 */
	public void saveUnSubscribeFansBehavior(WechatFans wechatFans);
	
}

