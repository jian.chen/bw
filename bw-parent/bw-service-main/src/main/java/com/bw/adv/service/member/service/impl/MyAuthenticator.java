/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MyAuthenticator.java
 * Package Name:com.sage.scrm.service.member.service.impl
 * Date:2015年8月17日下午5:47:58
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.member.service.impl;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * ClassName:MyAuthenticator <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午5:47:58 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class MyAuthenticator extends Authenticator{
	private String userName;  
	    private String password;  
	  
	    public MyAuthenticator(String userName, String password){  
	        this.userName = userName;  
	        this.password = password;  
	    }  
	  
	    @Override  
	    protected PasswordAuthentication getPasswordAuthentication() {  
	        return new PasswordAuthentication(userName, password);  
	    }  
}

