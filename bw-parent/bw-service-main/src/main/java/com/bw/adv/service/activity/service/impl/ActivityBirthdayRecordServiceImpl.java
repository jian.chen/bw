package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.ActivityBirthdayRecord;
import com.bw.adv.module.activity.repository.ActivityBirthdayRecordRepository;
import com.bw.adv.service.activity.service.ActivityBirthdayRecordService;

@Service
public class ActivityBirthdayRecordServiceImpl extends BaseServiceImpl<ActivityBirthdayRecord> implements ActivityBirthdayRecordService{

	private ActivityBirthdayRecordRepository activityBirthdayRecordRepository;
	
	@Override
	public BaseRepository<ActivityBirthdayRecord, ? extends BaseMapper<ActivityBirthdayRecord>> getBaseRepository() {
		return activityBirthdayRecordRepository;
	}

	@Override
	public List<ActivityBirthdayRecord> findRecordByIdAndMember(Long memberId, Long activityInstanceId,
			String createYear) {
		return activityBirthdayRecordRepository.findRecordByIdAndMember(memberId, activityInstanceId, createYear);
	}

	@Autowired
	public void setActivityBirthdayRecordRepository(ActivityBirthdayRecordRepository activityBirthdayRecordRepository) {
		this.activityBirthdayRecordRepository = activityBirthdayRecordRepository;
	}
	
}
