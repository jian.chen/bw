/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MealPeriodServiceImpl.java
 * Package Name:com.sage.scrm.service.base.service.impl
 * Date:2016年5月23日下午5:38:45
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.MealPeriod;
import com.bw.adv.module.base.repository.MealPeriodRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.MealPeriodService;

/**
 * ClassName:MealPeriodServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月23日 下午5:38:45 <br/>
 * scrmVersion 1.0
 * @author   zhangqi
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class MealPeriodServiceImpl extends BaseServiceImpl<MealPeriod> implements MealPeriodService {
	
	private MealPeriodRepository mealPeriodRepository;
	
	@Autowired
	public void setMealPeriodRepository(MealPeriodRepository mealPeriodRepository) {
		this.mealPeriodRepository = mealPeriodRepository;
	}

	@Override
	public BaseRepository<MealPeriod, ? extends BaseMapper<MealPeriod>> getBaseRepository() {
		return mealPeriodRepository;
	}

	@Override
	public List<MealPeriod> queryMealPeriodByStatusId(Page<MealPeriod> page) {
		return this.mealPeriodRepository.findByStatusId(StatusConstant.MEAL_ACTIVE.getId(), page);
	}

	@Override
	public MealPeriod selectByPrimaryKey(Long mealPeriodId) {
		return this.mealPeriodRepository.selectByPrimaryKey(mealPeriodId);
	}

	@Override
	public int insertSelective(MealPeriod meal) {
		meal.setCreateTime(DateUtils.getCurrentTimeOfDb());//创建时间
		meal.setStatusId(StatusConstant.MEAL_ACTIVE.getId());//状态为“正常”
		return this.mealPeriodRepository.saveSelective(meal);
	}

	@Override
	public int updateMealPeriod(MealPeriod meal) {
		meal.setUpdateTime(DateUtils.getCurrentTimeOfDb());//修改时间
		return this.mealPeriodRepository.updateByPkSelective(meal);
	}

	@Override
	public int deleteMealPeriodList(String mealPeriodIds) {
		List<Long> mealPeriodIdList = null;
		String[] ids = null;
		
		mealPeriodIdList = new ArrayList<Long>();
		ids = mealPeriodIds.split(",");
		for(int i=0;i<ids.length;i++){
			mealPeriodIdList.add(Long.parseLong(ids[i]));
		}
		return this.mealPeriodRepository.deleteStatusIdList(mealPeriodIdList);
	}

	@Override
	public Long selectByMealPeriodNameById(String mealPeriodName, Long mealPeriodId) {
		return this.mealPeriodRepository.selectByMealPeriodNameById(mealPeriodName, mealPeriodId);
	}
}

