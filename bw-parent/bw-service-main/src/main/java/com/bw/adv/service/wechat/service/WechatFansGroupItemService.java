/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatFansGroupItemService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月20日下午4:23:45
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansGroup;
import com.bw.adv.module.wechat.model.WechatFansGroupItem;
import com.bw.adv.module.wechat.model.exp.WechatFansExp;
import com.bw.adv.module.wechat.model.exp.WechatFansGroupItemExp;

/**
 * ClassName:WechatFansGroupItemService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午4:23:45 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatFansGroupItemService extends BaseService<WechatFansGroupItem> {
	
	/**
	 * save:(添加微信粉丝组明细). <br/>
	 * Date: 2015年8月20日 下午4:51:49 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFanId
	 * @param wechatFansGroup
	 */
	void save(String wechatFanId, WechatFansGroup wechatFansGroup);
	
	/**
	 * queryWechatFansGroupItemByGroupId:(根据分组查询微信粉丝信息). <br/>
	 * Date: 2015年9月1日 下午4:38:34 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param groupId
	 * @return
	 */
	public List<WechatFansGroupItemExp> queryWechatFansGroupItemByGroupId(Long groupId);
	
	/**
	 * 根据wechatFansId查询对用信息
	 * queryWechatFansGroupItemByFansId:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年9月2日 下午4:25:25 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param long1
	 * @return
	 */
	WechatFansGroupItem queryWechatFansGroupItemByFansId(Long wechatId);
	
	/**
	 * transToGroupItemExp:(扩展粉丝所属组). <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatFans
	 * @param wechatGroupId
	 * @return
	 */
	List<WechatFansExp> transToGroupItemExp(List<WechatFans> wechatFans,String wechatGroupId);
	
	/**
	 * deleteWechatFansGroupItems:(删除分组信息). <br/>
	 * Date: 2015年12月27日 下午4:51:59 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param wechatGroupId
	 */
	void deleteWechatFansGroupItems(String wechatGroupId);
	
}

