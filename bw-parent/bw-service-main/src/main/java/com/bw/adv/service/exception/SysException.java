package com.bw.adv.service.exception;

/**
 * Created by jeoy.zhou on 4/8/16.
 */
public class SysException extends RuntimeException {

    public SysException() {
    }

    public SysException(String message) {
        super(message);
    }

    public SysException(String message, Throwable cause) {
        super(message, cause);
    }

    public SysException(Throwable cause) {
        super(cause);
    }
}
