/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:WechatTextTemplate.java
 * Package Name:com.sage.scrm.bk.wechat.model
 * Date:2015年12月17日下午4:19:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.msg.model;

import java.util.Map;

/**
 * ClassName:WechatTextTemplate <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 下午4:19:42 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class WechatTextTemplate {
	private String touser;
	
	private String msgtype;
	
    private Map<String, String> text;

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public Map<String, String> getText() {
		return text;
	}

	public void setText(Map<String, String> text) {
		this.text = text;
	}

}

