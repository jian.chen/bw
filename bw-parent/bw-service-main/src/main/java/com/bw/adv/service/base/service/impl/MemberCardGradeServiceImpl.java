/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:MemberCardGradeServiceImpl.java
 * Package Name:com.sage.scrm.service.base.service.impl
 * Date:2015年9月6日下午4:41:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.base.repository.MemberCardGradeRepository;
import com.bw.adv.module.member.model.MemberCardGrade;
import com.bw.adv.service.base.service.MemberCardGradeService;

/**
 * ClassName:MemberCardGradeServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月6日 下午4:41:36 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class MemberCardGradeServiceImpl extends BaseServiceImpl<MemberCardGrade> implements MemberCardGradeService {

	private MemberCardGradeRepository memberCardGradeRepository;
	
	@Override
	public BaseRepository<MemberCardGrade, ? extends BaseMapper<MemberCardGrade>> getBaseRepository() {
		
		return memberCardGradeRepository;
	}
	
	@Override
	public List<MemberCardGrade> queryAll() {
		
		return memberCardGradeRepository.findAll();
	}
	
	@Override
	public int updateSrcByPk(MemberCardGrade memberCardGrade) {
		
		return memberCardGradeRepository.updateSrcByPk(memberCardGrade);
	}

	@Autowired
	public void setMemberCardGradeRepository(
			MemberCardGradeRepository memberCardGradeRepository) {
		this.memberCardGradeRepository = memberCardGradeRepository;
	}


}

