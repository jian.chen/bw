/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SceneGameServiceImpl.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2016年1月15日下午1:43:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.model.SceneGame;
import com.bw.adv.module.activity.repository.SceneGameRepository;
import com.bw.adv.service.activity.service.SceneGameService;

/**
 * ClassName:SceneGameServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午1:43:04 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class SceneGameServiceImpl extends BaseServiceImpl<SceneGame> implements SceneGameService {
	
	
	@Autowired
	private SceneGameRepository sceneGameRepository;
	
	@Override
	public BaseRepository<SceneGame, ? extends BaseMapper<SceneGame>> getBaseRepository() {
		return sceneGameRepository;
	}

	@Override
	public List<SceneGame> queryAllSceneGame() {
		return sceneGameRepository.findAll();
	}


}

