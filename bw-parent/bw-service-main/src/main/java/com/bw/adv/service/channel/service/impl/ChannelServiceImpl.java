package com.bw.adv.service.channel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.base.repository.ChannelRepository;
import com.bw.adv.module.cfg.model.ChannelCfgExp;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.service.channel.service.ChannelService;
@Service
public class ChannelServiceImpl extends BaseServiceImpl<Channel> implements ChannelService {
	
	private ChannelRepository channelRepository;
	
	
	@Override
	public List<Channel> queryList() {
		return channelRepository.findList();
	}
	
	@Override
	public List<Channel> findListByChannelName(String channelName,Page<Channel> page) {
		return channelRepository.findListByChannelName(channelName,page);
	}
	
	@Override
	public void saveOrUpdateChannel(Channel channel) {
		Long codes = channelRepository.findCountChannelByChannelCode(channel.getChannelCode(),channel.getChannelId());
		if(codes>0){
			throw new MemberException("渠道编码必须唯一");
		}
		Long names = channelRepository.findCountChannelByChannelName(channel.getChannelName(),channel.getChannelId());
		if(names>0){
			throw new MemberException("渠道名称必须唯一");
		}
		if(channel.getChannelId()==null){
			channelRepository.save(channel);
		}else{
			channelRepository.updateByPk(channel);
		}
	}

	@Override
	public BaseRepository<Channel, ? extends BaseMapper<Channel>> getBaseRepository() {
		return channelRepository;
	}

	@Autowired
	public void setChannelRepository(ChannelRepository channelRepository) {
		this.channelRepository = channelRepository;
	}


	


}
