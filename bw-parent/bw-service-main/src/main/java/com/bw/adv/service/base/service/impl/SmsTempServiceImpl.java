/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SmsTempServiceImpl.java
 * Package Name:com.sage.scrm.service.base.service.impl
 * Date:2015年8月19日上午11:27:52
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.base.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.component.msg.model.SmsTemp;
import com.bw.adv.module.component.msg.model.SmsTempParams;
import com.bw.adv.module.component.msg.model.SmsTempType;
import com.bw.adv.module.component.msg.model.exp.SmsInstanceExp;
import com.bw.adv.module.component.msg.model.exp.SmsTempExp;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.sms.repository.SmsInstanceRepository;
import com.bw.adv.module.sms.repository.SmsTempParamsRepository;
import com.bw.adv.module.sms.repository.SmsTempRepository;
import com.bw.adv.module.sms.repository.SmsTempTypeRepository;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.repository.SysUserRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.SmsTempService;

/**
 * ClassName:SmsTempServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 上午11:27:52 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class SmsTempServiceImpl extends BaseServiceImpl<SmsTemp> implements SmsTempService {

	private SmsTempRepository smsTempRepository;
	
	private SmsTempTypeRepository smsTempTypeRepository;
	
	private SmsInstanceRepository smsInstanceRepository;
	
	private SysUserRepository sysUserRepository;
	
	private SmsTempParamsRepository smsTempParamsRepository;
	
	@Override
	public BaseRepository<SmsTemp, ? extends BaseMapper<SmsTemp>> getBaseRepository() {
		return smsTempRepository;
	}

	
	@Override
	public void deleteSmsTempList(List<Long> smsTempIds) {
		
		smsTempRepository.removeSmsTempList(StatusConstant.SMS_TEMP_IS_REMOVE.getId(),smsTempIds);
	}


	@Override
	public List<SmsTempExp> querySmsExpByStatusId(Page<SmsTempExp> page) {
		return smsTempRepository.findExpByStatusId(page);
	}


	
	
	@Override
	public SmsTempType querySmsTempTypeById(Long smsTempTypeId) {
		
		return smsTempTypeRepository.findByPk(smsTempTypeId);
	}
	
	@Override
	public List<SmsInstance> querySmsInstanceList() {
		
		return smsInstanceRepository.findAll();
	}

	
	@Override
	public List<SmsInstance> querySmsInstanceByMobile(String mobile) {
		
		return smsInstanceRepository.findByMobile(mobile);
	}

	@Override
	public List<SmsInstance> querySmsInstanceByExample(Example example,
			Page<SmsInstance> page) {
		
		return smsInstanceRepository.findByExample(example, page);
	}

	@Override
	public List<SmsInstanceExp> querySmsInstanceExpByExample(Example example,Page<SmsInstanceExp> page) {
		
		return smsInstanceRepository.findExpByExample(example, page);
	}

	
	@Override
	public SysUser querySysUserById(Long sysUserId){
		return sysUserRepository.findByPk(sysUserId);
	}
	
	@Override
	public int addSmsInstanceList(Long groupId,Long smsTempId) {
		List<Member> memberList = null;
		SmsTemp smsTemp = null;
		SmsInstance temp = null;
		List<SmsInstance> smsInstanceList = new ArrayList<SmsInstance>();
		int result = 0;
		
		smsTemp = smsTempRepository.findByPk(smsTempId);
		if(groupId == null){
			memberList = smsTempRepository.findAllMember();
		}else{
			memberList = smsTempRepository.findByGroupId(groupId);
		}
		if(memberList.size() == 0){
			return 1;
		}else{
			for(int i=0;i<memberList.size();i++){
				temp = new SmsInstance();
				temp.setSmsTempId(smsTempId);
				temp.setSmsChannelId(smsTemp.getSmsChannelId());
				temp.setContent(smsTemp.getContent());
				temp.setCreateTime(DateUtils.getCurrentTimeOfDb());
				temp.setIsSend("N");
				
				temp.setMemberId(memberList.get(i).getMemberId());
				temp.setMobile(memberList.get(i).getMobile());
				smsInstanceList.add(temp);
			}
			try {
				smsInstanceRepository.saveList(smsInstanceList);
				result = 1;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}
		
	}

	@Override
	public void deleteSmsInstanceList(List<Long> smsInstanceIds) {
		
		//smsTempRepository.removeSmsTempList(StatusConstant.SMS_TEMP_IS_REMOVE.getId(),smsInstanceIds);
		
	}

	@Override
	public List<MemberGroup> queryMemberGroupList(){
		return smsTempRepository.findGroupByStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_USE.getId());
	}
	

	@Override
	public SmsTempExp queryExpById(Long smsTempId) {
		
		return smsTempRepository.findExpById(smsTempId);
	}


	@Override
	public List<SmsTempParams> queryParamsList() {
		
		return smsTempParamsRepository.findAll();
	}

	/**
	 * TODO 根据模板类型查询模板列表
	 * @see com.bw.adv.service.base.service.SmsTempService#querySmsTempByTypeId(java.lang.Long)
	 */
	@Override
	public List<SmsTemp> querySmsTempByTypeId(Long smsTempTypeId) {
		return smsTempRepository.findSmsTempByTypeId(smsTempTypeId);
	}
	
	@Override
	public List<SmsTemp> querySmsTempAll(){
		return smsTempRepository.querySmsTempAll();
	}
	
	@Autowired
	public void setSmsTempRepository(SmsTempRepository smsTempRepository) {
		this.smsTempRepository = smsTempRepository;
	}

	
	@Autowired
	public void setSmsTempTypeRepository(SmsTempTypeRepository smsTempTypeRepository) {
		this.smsTempTypeRepository = smsTempTypeRepository;
	}

	@Autowired
	public void setSmsInstanceRepository(SmsInstanceRepository smsInstanceRepository) {
		this.smsInstanceRepository = smsInstanceRepository;
	}


	@Autowired
	public void setSysUserRepository(SysUserRepository sysUserRepository) {
		this.sysUserRepository = sysUserRepository;
	}

	@Autowired
	public void setSmsTempParamsRepository(
			SmsTempParamsRepository smsTempParamsRepository) {
		this.smsTempParamsRepository = smsTempParamsRepository;
	}
	
	
	
}

