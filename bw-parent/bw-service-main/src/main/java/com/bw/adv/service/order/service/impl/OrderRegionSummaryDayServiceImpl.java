package com.bw.adv.service.order.service.impl;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.report.order.model.OrderRegionSummaryDay;
import com.bw.adv.module.report.order.model.exp.OrderRegionSummaryDayExp;
import com.bw.adv.module.report.repository.OrderRegionSummaryDayRepository;
import com.bw.adv.service.order.service.OrderRegionSummaryDayService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OrderRegionSummaryDayServiceImpl extends BaseServiceImpl<OrderRegionSummaryDay> implements
		OrderRegionSummaryDayService {
	private OrderRegionSummaryDayRepository orderRegionSummaryDayRepository;
	@Override
	public List<OrderRegionSummaryDayExp> queryByExp(Example example) {
		return this.orderRegionSummaryDayRepository.findByExp(example);
	}

	@Override
	public BaseRepository<OrderRegionSummaryDay, ? extends BaseMapper<OrderRegionSummaryDay>> getBaseRepository() {
		return orderRegionSummaryDayRepository;
	}

	@Override
	public void callOrderRegionSummaryDay(String dateStr) {
		
		orderRegionSummaryDayRepository.callOrderRegionSummaryDay(dateStr);
		
	}

	@Autowired
	public void setOrderRegionSummaryDayRepository(
			OrderRegionSummaryDayRepository orderRegionSummaryDayRepository) {
		this.orderRegionSummaryDayRepository = orderRegionSummaryDayRepository;
	}


}
