/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatMessageTemplateServiceImpl.java
 * Package Name:com.sage.scrm.service.wechat.service.impl
 * Date:2015年8月18日下午4:46:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service.impl;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.model.WeixinMedia;
import com.bw.adv.module.wechat.repository.WechatMessageTemplateRepository;
import com.bw.adv.service.wechat.service.WechatMessageTemplateService;

/**
 * ClassName:WechatMessageTemplateServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:46:46 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class WechatMessageTemplateServiceImpl extends BaseServiceImpl<WechatMessageTemplate> implements WechatMessageTemplateService {

	private WechatMessageTemplateRepository wechatMessageTemplate;
	
	@Override
	public BaseRepository<WechatMessageTemplate, ? extends BaseMapper<WechatMessageTemplate>> getBaseRepository() {
		return wechatMessageTemplate;
	}
	
	@Autowired
	public void setWechatMessageTemplate(
			WechatMessageTemplateRepository wechatMessageTemplate) {
		this.wechatMessageTemplate = wechatMessageTemplate;
	}
	
	/**
	 * 处理思路:
	 * 1.首先取到第一个图文，作为parent_id
	 * 2.子图文依次存入
	 * 注意点：type类型为news thumb_media_id存入微信返回的media_id
	 *      如闻上传成功后，微信返回一个media_id 将media_id更新到本地的父级的图文中
	 * TODO 简单描述该方法的实现功能（可选）.
	 * @see 
	 */
	@Override
	public String saveImageTextInfo(List<WeixinMedia> mediaList, JSONArray data, JSONObject wechatReponseJson, String strBackUrl, List<String> filePathList) {
		Long firstId =null;
		WeixinMedia weixinMedia1 =null;
		JSONObject jsonObject1 =null;
		String path=null;
		// 第一个封面的weixinmedia
		weixinMedia1 = mediaList.get(0);
		// 第一个封面对用的数据 
		jsonObject1 = (JSONObject)data.get(0);
		// 第一个图片的url
		path = strBackUrl+filePathList.get(0);
		firstId =this.saveFirstImageText(weixinMedia1,jsonObject1,path);
		if(mediaList.size()>1){
			for(int i=1;i<mediaList.size();i++){
				JSONObject jsonObject =(JSONObject)data.get(i);
				String picUrl = filePathList.get(i);
				WechatMessageTemplate wechatMessageTemplate =new WechatMessageTemplate();
				wechatMessageTemplate.setAuthor((String)jsonObject.get("ImageTextAuthor"));
				wechatMessageTemplate.setContent((String)jsonObject.get("ImageTextBody"));
				wechatMessageTemplate.setCreateTime(DateUtils.getCurrentTimeOfDb());
				wechatMessageTemplate.setIsCover(jsonObject.getInt("ImageTextCover")+"");
				wechatMessageTemplate.setIsDeleted(WechatTypeConstant.UNDELETED.getCode());
				wechatMessageTemplate.setLinkUrl((String)jsonObject.get("ImageTextLink"));
				wechatMessageTemplate.setPictureUrl(strBackUrl+picUrl);
				wechatMessageTemplate.setParentId(firstId);
				wechatMessageTemplate.setStatusId(StatusConstant.WECHAT_ENABLE.getId());
				wechatMessageTemplate.setThumbId(mediaList.get(i).getMediaId());
				wechatMessageTemplate.setTitle((String)jsonObject.get("ImageTextTitle"));
				wechatMessageTemplate.setWechatAccountId(Long.valueOf(1));
				wechatMessageTemplate.setWechatMessageType(WechatTypeConstant.WECHAT_MESSAGE_TYPE_NEWS.getCode());
				this.save(wechatMessageTemplate);
			}
		}
		return firstId+"";
	}
	
	/**
	 * saveFirstImageText:(保存信息). <br/>
	 * Date: 2015年9月9日 下午8:15:27 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param weixinMedia1
	 * @param path 
	 * @param strBackUrl 
	 * @param jsonObject1
	 * @return
	 */
	private Long saveFirstImageText(WeixinMedia weixinMedia1,JSONObject json,String path) {
		System.out.println("测试陆家sndfd"+path);
		// 缩略图id
		String thumbId =null;
		// 标题
		String imageTextTitle =null;
		// 作者
		String imageTextAuthor =null;
		// 摘要
		String imageTextDigest =null;
		// 是否封面
		String imageTextCover =null;
		// 内容
		String imageTextBody =null;
		// 跳转url
		String imageTextLink =null;
		// 主键id
		Long primaryKeyId =null;
		
		WechatMessageTemplate wechatMessageTemplate =null;
		wechatMessageTemplate =new WechatMessageTemplate();
		// 缩略图id
		thumbId =weixinMedia1.getMediaId();
		// 标题
		imageTextTitle = (String)json.get("ImageTextTitle");
		// 作者
		imageTextAuthor = (String)json.get("ImageTextAuthor");
		// 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
		imageTextDigest = (String)json.get("ImageTextDigest");
		// 是否显示封面，0为false，即不显示，1为true，即显示
		imageTextCover =json.getInt("ImageTextCover")+"";
		// 图文消息的具体内容，支持HTML标签
		imageTextBody =(String)json.get("ImageTextBody");
		// 图文消息的原文地址
		imageTextLink =(String)json.get("ImageTextLink");
		// 保存
		wechatMessageTemplate.setAuthor(imageTextAuthor);
		wechatMessageTemplate.setContent(imageTextBody);
		wechatMessageTemplate.setCreateTime(DateUtils.getCurrentTimeOfDb());
		wechatMessageTemplate.setDescription(imageTextDigest);
		wechatMessageTemplate.setIsCover(imageTextCover);
		wechatMessageTemplate.setIsDeleted(WechatTypeConstant.UNDELETED.getCode());
		wechatMessageTemplate.setLinkUrl(imageTextLink);
		wechatMessageTemplate.setPictureUrl(path);
		wechatMessageTemplate.setStatusId(StatusConstant.WECHAT_ENABLE.getId());
		wechatMessageTemplate.setThumbId(thumbId);
		wechatMessageTemplate.setTitle(imageTextTitle);
		wechatMessageTemplate.setWechatAccountId(Long.valueOf(1));
		wechatMessageTemplate.setWechatMessageType(WechatTypeConstant.WECHAT_MESSAGE_TYPE_NEWS.getCode());
		this.save(wechatMessageTemplate);
		primaryKeyId =wechatMessageTemplate.getWechatMessageTemplateId();
		return primaryKeyId;
	}
	

	@Override
	public void deleteWechatMessageTemplate(WechatMessageTemplate wechatMessageTemplate) {
		if(null!=wechatMessageTemplate){
			wechatMessageTemplate.setIsDeleted(WechatTypeConstant.DELETED.getCode());
			wechatMessageTemplate.setStatusId(StatusConstant.WECHAT_DISENABLE.getId());
			this.updateByPk(wechatMessageTemplate);
		}
	}
	
	@Override
	public List<WechatMessageTemplate> queryParentImageText() {
		return wechatMessageTemplate.findParentImageText();
	}
	
	@Override
	public List<WechatMessageTemplate> querySubImageTextList(String wechatMessageTemplateId) {
		return wechatMessageTemplate.findSubImageTextList(wechatMessageTemplateId);
	}

}

