package com.bw.adv.service.member.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.MemberEntityCard;
import com.bw.adv.module.member.model.MemberEntityCardIssue;
import com.bw.adv.module.member.model.exp.MemberEntityCardExp;
import com.bw.adv.module.member.repository.MemberEntityCardIssueRepository;
import com.bw.adv.module.member.repository.MemberEntityCardRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.member.service.MemberEntityCardService;

@Service
public class MemberEntityCardServiceImpl extends BaseServiceImpl<MemberEntityCard>implements MemberEntityCardService {

	private MemberEntityCardRepository memberEntityCardRepository;
	private MemberEntityCardIssueRepository memberEntityCardIssueRepository;
	private CodeService codeService;

	@Override
	public BaseRepository<MemberEntityCard, ? extends BaseMapper<MemberEntityCard>> getBaseRepository() {
		return memberEntityCardRepository;
	}

	
	
	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}

	
	@Override
	public int saveList(int count,String userName,String batch) {
		MemberEntityCardIssue memberEntityCardIssue=null;
		List<String> memberCardCodes =null;
		List<MemberEntityCard> memberCards=null;
		try {
			/*
			 * 生成批次号，得到批次主键
			 */
			memberEntityCardIssue=saveMemberCardIssue(count, userName, batch);
			memberCardCodes = this.codeService.generateCode(MemberCard.class, count);
			memberCards=new ArrayList<MemberEntityCard>();
			for (String memberCardCode : memberCardCodes) {
				MemberEntityCard memberEntityCard=new MemberEntityCard();
				memberEntityCard.setIssueId(memberEntityCardIssue.getIssueId());
				memberEntityCard.setCardNo(memberCardCode);
				memberEntityCard.setStatusId(StatusConstant.MEMBER_ENTITY_CARD_ENABLE.getId());
				memberCards.add(memberEntityCard);
			}
			this.memberEntityCardRepository.saveList(memberCards);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	@Override
	public List<MemberEntityCard> findMemberCardByIssueId(Long issueId,Page<MemberEntityCard> pageObj) {
		return this.memberEntityCardRepository.selectMemberCardByIssueId(issueId,pageObj);
	}

	/**
	 * 先生成批次主标
	 * @param count 生成数量
	 * @param userName 当前操作用户
	 * @param batch 生成批次号
	 */
	private MemberEntityCardIssue saveMemberCardIssue(int count,String userName,String batch){
		MemberEntityCardIssue memberEntityCardIssue=null;
		try {
			memberEntityCardIssue=new MemberEntityCardIssue();
			memberEntityCardIssue.setBatch(batch);
			memberEntityCardIssue.setCount(count);
			memberEntityCardIssue.setCreateBy(userName);
			memberEntityCardIssue.setCreateTime(DateUtils.getCurrentDateOfDb());
			memberEntityCardIssue.setStatusId(StatusConstant.MEMBER_ENTITY_CARD_NO.getId());
			this.memberEntityCardIssueRepository.insertMemberCardGetId(memberEntityCardIssue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberEntityCardIssue;
	}
	
	@Override
	public List<MemberEntityCardIssue> findMemberCardByBatch(String batch,Page<MemberEntityCardIssue> pageObj) {
		return this.memberEntityCardIssueRepository.selectMemerCardByBatch(batch,pageObj);
	}
	/**
	 * 导出Excel，生成所需的MemberEntityCardExp
	 */
	@Override
	public List<MemberEntityCardExp> getMemberCardByIssueIdToExcel(Long issueId) {
		List<MemberEntityCard> memberCardByIssueId = this.memberEntityCardRepository.selectMemberCardByIssueId(issueId,null);
		if(memberCardByIssueId==null||memberCardByIssueId.size()==0)
			return null;
		List<MemberEntityCardExp> memberEntityCardExp=new ArrayList<MemberEntityCardExp>();
		for (MemberEntityCard memberEntityCard : memberCardByIssueId) {
			MemberEntityCardExp entityCardExp=new MemberEntityCardExp();
			entityCardExp.copyProperty(memberEntityCard);
			memberEntityCardExp.add(entityCardExp);
		}
		
		return memberEntityCardExp;
	}


	
	@Override
	public int makeMemberEntityCard(Long issueId){
		
		MemberEntityCardIssue memberEntityCardIssue=new MemberEntityCardIssue();
		memberEntityCardIssue.setIssueId(issueId);
		
		memberEntityCardIssue.setStatusId(StatusConstant.MEMBER_ENTITY_CARD_YES.getId());
		return memberEntityCardIssueRepository.updateByPkSelective(memberEntityCardIssue);
	}
	

	@Autowired
	public void setMemberEntityCardRepository(MemberEntityCardRepository memberEntityCardRepository) {
		this.memberEntityCardRepository = memberEntityCardRepository;
	}
	
	@Autowired
	public void setMemberEntityCardIssueRepository(MemberEntityCardIssueRepository memberEntityCardIssueRepository) {
		this.memberEntityCardIssueRepository = memberEntityCardIssueRepository;
	}





	
	
}
