package com.bw.adv.service.exception;

/**
 * Created by jeoy.zhou on 3/14/16.
 */
public class SmsChannelException extends RuntimeException{

    private static final long serialVersionUID = 4349005570547025527L;

    public SmsChannelException(String msg) {
        super(msg);
    }

    public SmsChannelException(Throwable e) {
        super(e);
    }

    public SmsChannelException(String msg, Throwable e) {
        super(msg, e);
    }
}
