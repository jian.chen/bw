/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:SysRoleServiceImpl.java
 * Package Name:com.sage.scrm.service.sys.service.impl
 * Date:2015-8-14下午4:48:43
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.service.sys.service.impl;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.sys.model.SysFunctionOperation;
import com.bw.adv.module.sys.repository.SysFunctionOperationRepository;
import com.bw.adv.service.sys.service.SysFunctionOperationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName:SysRoleServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-14 下午4:48:43 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
@Service
public class SysFunctionOperationServiceImpl extends BaseServiceImpl<SysFunctionOperation> implements SysFunctionOperationService {

	private SysFunctionOperationRepository sysFunctionOperationRepository;

	@Override
	public List<SysFunctionOperation> queryDefaultFunctionOperations() {
		return sysFunctionOperationRepository.queryDefaultFunctionOperations();
	}

	@Override
	public List<SysFunctionOperation> queryAllFunctionOperation() {
		return sysFunctionOperationRepository.queryFunctionOperationsByType(null);
	}

	@Override
	public List<SysFunctionOperation> queryButtonFunctionOperationsByUserId(Long userId) {
		return sysFunctionOperationRepository.selectButtonOperationByUserId(userId);
	}

	@Override
	public BaseRepository<SysFunctionOperation, ? extends BaseMapper<SysFunctionOperation>> getBaseRepository() {
		return sysFunctionOperationRepository;
	}
	@Autowired
	public void setSysFunctionOperationRepository(
			SysFunctionOperationRepository sysFunctionOperationRepository) {
		this.sysFunctionOperationRepository = sysFunctionOperationRepository;
	}


}

