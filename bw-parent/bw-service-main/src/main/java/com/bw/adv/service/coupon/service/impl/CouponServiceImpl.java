package com.bw.adv.service.coupon.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.repository.StoreRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.coupon.exception.CouponException;
import com.bw.adv.module.coupon.model.*;
import com.bw.adv.module.coupon.model.exp.CouponExp;
import com.bw.adv.module.coupon.model.exp.CouponForUse;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.coupon.model.exp.CouponInstanceRelayExp;
import com.bw.adv.module.coupon.model.exp.CouponRangeExp;
import com.bw.adv.module.coupon.repository.*;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.coupon.service.CouponInstanceService;
import com.bw.adv.service.coupon.service.CouponService;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * ClassName: CouponServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月16日 上午11:56:04 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Service
public class CouponServiceImpl extends BaseServiceImpl<Coupon> implements CouponService {
	
	private static final Logger logger =LoggerFactory.getLogger(CouponServiceImpl.class);
	
	private CouponRepository couponRepository;
	
	private CouponRangeRepository couponRangeRepository;
	
	private CouponInstanceRepository couponInstanceRepository;
	
	private CodeService codeService;
	
	private StoreRepository storeRepository;
	
	//private CouponInstanceService couponInstanceService;
	
	private CouponSummaryRepository couponSummaryRepository;
	
	private CouponBusinessTypeRepository couponBusinessTypeRepository;
	
	private CouponTypeRepository couponTypeRepository;
	
	private CouponVerificationFilterDefault couponVerificationFilterDefault;
	
	private CouponInstanceRelayRepository couponInstanceRelayRepository;
	
	private CouponInstanceReceiveRepository couponInstanceReceiveRepository;

	private CouponOriginRepository couponOriginRepository;

	private ExternalCouponMerchantRepository externalCouponMerchantRepository;
	@Override
	public BaseRepository<Coupon, ? extends BaseMapper<Coupon>> getBaseRepository() {
		return couponRepository;
	}
	
	

	@Override
	public List<CouponExp> queryCouponList(Page<Coupon> page) {
		return couponRepository.findList(page);
	}

	@Transactional
	@Override
	public int insertCoupon(Coupon coupon,String equalOrgId) throws Exception {
		int couponid = 0;
		String thruDate = null;
		String fromDate = null;
		Long[] orgIds = null;
		
		thruDate = coupon.getThruDate();
		thruDate = thruDate.replaceAll("-", "");
		fromDate = coupon.getFromDate();
		fromDate = fromDate.replaceAll("-", "");
		//
		coupon.setThruDate(thruDate);
		coupon.setFromDate(fromDate);
		coupon.setAvailableQuantity(coupon.getTotalQuantity());
		coupon.setCouponCode(codeService.generateCode(Coupon.class));
		coupon.setStatusId(StatusConstant.COUPON_NEW.getId());
		coupon.setCreateBy("waiter");//临时
		coupon.setCreateTime(DateUtils.getCurrentTimeOfDb());
		coupon.setIsActive("Y");//临时
		couponid = couponRepository.insertCoupon(coupon);
		//新增优惠券的使用范围记录
		orgIds = stringParseLongArr(equalOrgId);
		for(int i=0;i<orgIds.length;i++){
			CouponRange couponRange = new CouponRange();
			couponRange.setCouponId(coupon.getCouponId());
			couponRange.setOrgId(orgIds[i]);
			couponRangeRepository.saveSelective(couponRange);//可做批量优化
		}
		return couponid;
	}

	@Override
	public Coupon queryByPk(Long couponid) {
		return couponRepository.findByPk(couponid);
	}

	@Override
	@Transactional
	public int updateCoupon(Coupon coupon, String equalOrgId) {
		String thruDate = null;
		String fromDate = null;
		Long[] orgIds = null;
		//修改
		thruDate = coupon.getThruDate();
		thruDate = thruDate.replaceAll("-", "");
		fromDate = coupon.getFromDate();
		fromDate = fromDate.replaceAll("-", "");
		coupon.setThruDate(thruDate);
		coupon.setFromDate(fromDate);
		coupon.setAvailableQuantity(coupon.getTotalQuantity());
		couponRangeRepository.deleteByCouponId(coupon.getCouponId());
		//新增优惠券的使用范围记录
		orgIds = stringParseLongArr(equalOrgId);
		for(int i=0;i<orgIds.length;i++){
			CouponRange couponRange = new CouponRange();
			couponRange.setCouponId(coupon.getCouponId());
			couponRange.setOrgId(orgIds[i]);
			couponRangeRepository.saveSelective(couponRange);//可做批量优化
		}
		return couponRepository.updateCoupon(coupon);
	}

	@Override
	public List<CouponExp> queryCouLByName(Example example,Page<Coupon> page) {
		
		return couponRepository.findCouponListByName(example,page);
	}
	


	@Transactional
	@Override
	public String removeCoupons(String couponid) {
		String[] cousarr = null;
		List<Coupon> coupons = null;
		String result = null;
		
		coupons = new ArrayList<Coupon>();
		cousarr = couponid.split(",");
		for(int i = 0;i < cousarr.length;i++){
			Coupon coupon = new Coupon();
			coupon = couponRepository.findByPk(Long.parseLong(cousarr[i]));
			if(!coupon.getStatusId().equals(StatusConstant.COUPON_ISSUED.getId())){
				coupon.setStatusId(StatusConstant.COUPON_DELETE.getId());
				coupons.add(coupon);
			}
		}
		if(!coupons.isEmpty()){
			couponRepository.removeCouponsByPk(coupons);
			result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
		}else{
			result = "{\"code\":\"0\",\"message\":\"已发放优惠券不能删除!\"}";
		}
		return result;
	}

	
	/**
	 * checkCouponInstance:(校验券是否有效). <br/>
	 * Date: 2015年9月29日 上午11:26:05 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceExp
	 * @param store
	 * @return
	 */
	@Override
	public void checkCouponInstance(CouponInstanceExp couponInstanceExp,Store store) {
		checkCouponInstance(couponInstanceExp,store,"N");
	}
	
	@Override
	public void checkOrderCouponInstance(CouponInstanceExp couponInstanceExp,Store store) {
		checkCouponInstance(couponInstanceExp,store,"Y");
	}
	
	private void checkCouponInstance(CouponInstanceExp couponInstanceExp,Store store,String isOrder) {
		String currentDate = null;
		List<CouponRangeExp> couponRanges = null;
		boolean flag = false;
		//校验券是否存在
		if (couponInstanceExp == null) {
			logger.error("优惠券核销失败："+ResultStatus.COUPONINSTANCE_CODE_NULL_ERROR.getMsg());
			throw new CouponException(ResultStatus.COUPONINSTANCE_CODE_NULL_ERROR);
		}
		// 使用和作废都只能是未使用或使用中状态下的优惠券实例
		if(StatusConstant.COUPON_INSTANCE_USED.getId().equals(couponInstanceExp.getStatusId())) {
			logger.error("优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_USED_ERROR.getMsg());
			throw new CouponException(ResultStatus.COUPON_INSTANCE_STATUS_USED_ERROR);
		}
		if(StatusConstant.COUPON_INSTANCE_EXPIRE.getId().equals(couponInstanceExp.getStatusId())) {
			logger.error("优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_EXPIRE_ERROR.getMsg());
			throw new CouponException(ResultStatus.COUPON_INSTANCE_STATUS_EXPIRE_ERROR);
		}
		if(StatusConstant.COUPON_INSTANCE_DESTROY.getId().equals(couponInstanceExp.getStatusId())) {
			logger.error("优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_DESTROY_ERROR.getMsg());
			throw new CouponException(ResultStatus.COUPON_INSTANCE_STATUS_DESTROY_ERROR);
		}
		//校验券是否过期
		currentDate = DateUtils.formatCurrentDate("yyyyMMdd");
		
		//订单的优惠券不校验是否过期
		if (StringUtils.isNotBlank(isOrder)&& isOrder.equals("N") && StringUtils.isNotBlank(couponInstanceExp.getExpireDate()) && currentDate.compareTo(couponInstanceExp.getExpireDate()) > 0) {
			logger.error("优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_EXPIRE_ERROR.getMsg());
			throw new CouponException(ResultStatus.COUPON_INSTANCE_STATUS_EXPIRE_ERROR);
		}
		
		if(StringUtils.isNotBlank(couponInstanceExp.getStartDate()) && currentDate.compareTo(couponInstanceExp.getStartDate()) < 0) {
			logger.error("优惠券核销失败："+ResultStatus.COUPON_INSTANCE_NOT_BEGIN_DATE.getMsg());
			throw new CouponException(ResultStatus.COUPON_INSTANCE_NOT_BEGIN_DATE);
		}
		
		//校验券的门店
		couponRanges = couponRangeRepository.findByCouponId(couponInstanceExp.getCouponId());
		if(!couponRanges.isEmpty()){
			if(store == null || store.getOrgId()==null){
				logger.error("优惠券核销失败："+ResultStatus.COUPON_STORE_ERROR.getMsg());
				throw new CouponException(ResultStatus.COUPON_STORE_ERROR);
			}
			for(int i = 0; i < couponRanges.size(); i++){
				if(store.getOrgId().equals(couponRanges.get(i).getOrgId())){
					flag = true;
				}
			}
			if(!flag){
				logger.error("优惠券核销失败："+ResultStatus.COUPON_ORG_NULL_ERROR.getMsg());
				throw new CouponException(ResultStatus.COUPON_ORG_NULL_ERROR);
			}
		}
	}


	@Override
	public int updateByCouponCode(String couponCode,String status) {
		
		return couponRepository.updateByCouponCode(couponCode,status);
	}

	@Override
	public int queryCouponNum(String couponName) {
		
		return couponRepository.findCouponNum(couponName);
	}
	
	@Override
	@Transactional
	public void verificationCoupon(List<String> couponInstanceCodes,String storeCode,String externalId) throws Exception{
		verificationCoupon(couponInstanceCodes,storeCode,externalId,"N");
	}
	
	@Override
	@Transactional
	public void verificationOrderCoupon(List<String> couponInstanceCodes,String storeCode,String externalId) throws Exception{
		verificationCoupon(couponInstanceCodes,storeCode,externalId,"Y");
	}
	
	
	@Transactional
	private void verificationCoupon(List<String> couponInstanceCodes,String storeCode,String externalId,String isOrder) throws Exception{
		CouponInstanceExp couponInstanceExp = null;
		Example example = null;
		Criteria criteria = null;
		Coupon coupon = null;
		List<Long> idList = null;
		Map<Long,Long> map = null;
		Map<String,Object> couponInMap = null;
		Set<Long> set = null;
		Iterator<Long> iterator = null;
		Long couponId = null;
		Store store = null;
		
		
		if(couponVerificationFilterDefault != null){
			couponInstanceCodes = couponVerificationFilterDefault.verificationBefore(couponInstanceCodes, storeCode, externalId);
		}
		
		if(couponInstanceCodes==null || couponInstanceCodes.size() < 1){
			return;
		}
		
		store = storeRepository.findByStoreCode(storeCode);
		couponInMap = new HashMap<String, Object>();
		idList = new ArrayList<Long>();
		map = new HashMap<Long, Long>();
		for(int i=0;i<couponInstanceCodes.size();i++){
			example = new Example();
			criteria = example.createCriteria();
			criteria.andEqualTo("COUPON_INSTANCE_CODE", couponInstanceCodes.get(i));
			couponInstanceExp = couponInstanceRepository.findCouponInstanceExpByExample(example);
			
			//校验优惠券
			if(isOrder != null && isOrder.equals("Y")){
				checkOrderCouponInstance(couponInstanceExp,store);
			}else{
				checkCouponInstance(couponInstanceExp,store);
			}
			
			idList.add(couponInstanceExp.getCouponInstanceId());
			//生成map,key为优惠券ID，value为优惠券实例数量
			if(map.get(couponInstanceExp.getCouponId())==null){
				map.put(couponInstanceExp.getCouponId(), 1L);
			}else{
				map.put(couponInstanceExp.getCouponId(), map.get(couponInstanceExp.getCouponId())+1);
			}
		}
		//修改优惠券实例(新增的参数对应在这里加条件)
		couponInMap.put("idList", idList);
		couponInMap.put("storeId", store == null ? null : store.getStoreId());
		couponInMap.put("statusId", StatusConstant.COUPON_INSTANCE_USED.getId());
		couponInMap.put("updateTime", DateUtils.getCurrentTimeOfDb());
		couponInMap.put("usedTime", DateUtils.getCurrentTimeOfDb());
		couponInMap.put("externalId", externalId);
		couponInstanceRepository.batchUpdateCouponInstanceBatch(couponInMap);
		//修改对应优惠券
		set = map.keySet();         
		iterator = set.iterator(); 
		while(iterator.hasNext()){ 
			couponId = iterator.next();
			coupon = couponRepository.findByPk(couponId);
			if(coupon.getUsedQuantity()==null){
				coupon.setUsedQuantity(map.get(couponId));
			}else{
				coupon.setUsedQuantity(coupon.getUsedQuantity()+map.get(couponId));
			}
			this.updateByPkSelective(coupon);
		}
	}
	
	/**
	 * scrmcheckCouponInstance:(scrm校验优惠券). <br/>
	 * Date: 2015年9月29日 下午8:00:50 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstance
	 * @return
	 */
	public JsonModel scrmcheckCouponInstance(CouponInstance couponInstance,Long ststusId) {
		JsonModel jsonModel = null;
		String currentDate = null;
		
		//校验券是否存在
		if (couponInstance == null) {
			jsonModel = new JsonModel(0,"优惠券核销失败："+ResultStatus.COUPONINSTANCE_CODE_NULL_ERROR.getMsg());
			return jsonModel;
		}
		// 使用和作废都只能是未使用或使用中状态下的优惠券实例
		if(ststusId.equals(StatusConstant.COUPON_INSTANCE_USED.getId()) && StatusConstant.COUPON_INSTANCE_USED.getId().equals(couponInstance.getStatusId())) {
			jsonModel = new JsonModel(0,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_USED_ERROR.getMsg());
			return jsonModel;
        }
		if(ststusId.equals(StatusConstant.COUPON_INSTANCE_UNUSED.getId()) && StatusConstant.COUPON_INSTANCE_UNUSED.getId().equals(couponInstance.getStatusId())) {
			jsonModel = new JsonModel(0,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_USEDBACK_ERROR.getMsg());
			return jsonModel;
        }
		if(StatusConstant.COUPON_INSTANCE_EXPIRE.getId().equals(couponInstance.getStatusId())) {
			jsonModel = new JsonModel(0,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_EXPIRE_ERROR.getMsg());
			return jsonModel;
        }
		if(StatusConstant.COUPON_INSTANCE_DESTROY.getId().equals(couponInstance.getStatusId())) {
			jsonModel = new JsonModel(0,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_STATUS_DESTROY_ERROR.getMsg());
			return jsonModel;
        }
		//校验券是否过期
		currentDate = DateUtils.formatCurrentDate("yyyyMMdd");
		if (currentDate.compareTo(couponInstance.getExpireDate()) > 0) {
			jsonModel = new JsonModel(0,"优惠券核销失败："+ResultStatus.COUPON_NOT_AVALIABLE.getMsg());
			return jsonModel;
		}
		if(StringUtils.isNotBlank(couponInstance.getStartDate()) && currentDate.compareTo(couponInstance.getStartDate()) < 0) {
			jsonModel = new JsonModel(0,"优惠券核销失败："+ResultStatus.COUPON_INSTANCE_NOT_BEGIN_DATE.getMsg());
			return jsonModel;
		}
		jsonModel = new JsonModel(1,"优惠券核销成功!");
		return jsonModel;
	}
	
	@Override
	@Transactional
	public JsonModel verificationCoupon(CouponInstance couponInstance,Long ststusId) {
		Coupon coupon = null;
		JsonModel jsonModel = null;
		jsonModel = scrmcheckCouponInstance(couponInstance,ststusId);
		if(jsonModel.getStatus()==1){
			couponInstance.setStatusId(ststusId);
			couponInstance.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			couponInstance.setUsedTime(DateUtils.getCurrentTimeOfDb());
			couponInstanceRepository.updateByPkSelective(couponInstance);
			
			//修改优惠券
			coupon = this.queryByPk(couponInstance.getCouponId());
			if(coupon.getUsedQuantity()==null){
				coupon.setUsedQuantity(1L);
			}else{
				coupon.setUsedQuantity(coupon.getUsedQuantity()+1L);
			}
			this.updateByPkSelective(coupon);
		}
		return jsonModel;
	}

	@Override
	public CouponRangeExp queryCouponRange(Long couponId) {
		CouponRangeExp couponRangeExp = null;
		List<CouponRangeExp> couponRanges = null;
		String orgIds = null;
		String orgNames = null;
		
		couponRanges = couponRangeRepository.findByCouponId(couponId);
		for(int i=0;i<couponRanges.size();i++){
			if(i==0){
				orgIds = couponRanges.get(i).getOrgId().toString();
				orgNames = couponRanges.get(i).getOrgName();
			}else{
				orgIds = orgIds+","+couponRanges.get(i).getOrgId();
				orgNames = orgNames+","+couponRanges.get(i).getOrgName();
			}
		}
		couponRangeExp = new CouponRangeExp();
		couponRangeExp.setOrgIds(orgIds);
		couponRangeExp.setOrgName(orgNames);
		return couponRangeExp;
	}
	
	/**
	 * stringParseLongArr:String转换为Long[]<br/>
	 * Date: 2015年9月24日 上午11:47:04 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param str
	 * @return
	 */
	public Long[] stringParseLongArr(String str){
		if(str.isEmpty()){
			return new Long[0];
		}else{
			String[] strs = str.split(",");
			Long[] num = new Long[strs.length];
			for (int idx = 0; idx < strs.length; idx++) {
				num[idx] = Long.parseLong(strs[idx]);
			}
			return num;
		}
	}
	
	@Override
	public CouponSummaryDay selectBySummaryDate(String date) {
		return couponSummaryRepository.selectBySummaryDate(date);
	}
	
	@Override
	public List<CouponExp> queryCouponByCategoryID(Example example){
		return couponRepository.findCouponListByCategoryID(example);
	}
	
	@Override
	public void callCouponSummaryDay(String dateStr, String weekStr, String monthStr) {
		couponRepository.callCouponSummaryDay(dateStr, weekStr, monthStr);
	}
	
	@Override
	public List<CouponBusinessType> selectCouponBusinessType() {
		
		return couponBusinessTypeRepository.findList();
	}
	@Override
	public List<CouponBusinessType> selectCouponBusinessType(Page<CouponBusinessType> page) {
		
		return couponBusinessTypeRepository.findList(page);
	}
	
	@Override
	public CouponBusinessType selectCouponBusinessByPk(Long couponBusinessTypeId) {
		
		return couponBusinessTypeRepository.findByPk(couponBusinessTypeId);
	}

	@Override
	public List<CouponType> selectAllActive() {
		
		return couponTypeRepository.findAllActive();
	}

	@Override
	public CouponType selectTypeByPk(Long couponTypeId) {
		
		return couponTypeRepository.findByPk(couponTypeId);
	}

	@Override
	public int insertCouponBusinessType(CouponBusinessType couponBusinessType) throws Exception {
		String currentTime = null;
		currentTime = DateUtils.getCurrentTimeOfDb();
		couponBusinessType.setCouponBusinessTypeCode(codeService.generateCode(CouponBusinessType.class));
		couponBusinessType.setCreateTime(currentTime);
		couponBusinessType.setUpdateTime(currentTime);
		couponBusinessType.setIsActive("Y");
		couponBusinessType.setSystemId(1L);
		return couponBusinessTypeRepository.save(couponBusinessType);
	}
	

	@Override
	public int updateCouponBusinessType(CouponBusinessType couponBusinessType) {
		
		return couponBusinessTypeRepository.updateByPkSelective(couponBusinessType);
	}
	@Override
	public int removeCouponBusinessType(Long couponBusinessTypeId) {
		
		return couponBusinessTypeRepository.removeByPk(couponBusinessTypeId);
	}
	@Autowired
	public void setSysUserRepository(CouponRepository couponRepository) {
		this.couponRepository = couponRepository;
	}
	
	@Autowired
	public void setCouponRangeRepository(CouponRangeRepository couponRangeRepository) {
		this.couponRangeRepository = couponRangeRepository;
	}

	@Autowired
	public void setCouponRepository(CouponRepository couponRepository) {
		this.couponRepository = couponRepository;
	}

	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}

	@Autowired
	public void setCouponInstanceRepository(
			CouponInstanceRepository couponInstanceRepository) {
		this.couponInstanceRepository = couponInstanceRepository;
	}
	@Autowired
	public void setExternalCouponMerchantRepository(ExternalCouponMerchantRepository externalCouponMerchantRepository) {
		this.externalCouponMerchantRepository = externalCouponMerchantRepository;
	}

	@Autowired
	public void setStoreRepository(StoreRepository storeRepository) {
		this.storeRepository = storeRepository;
	}

	/*@Autowired
	public void setCouponInstanceService(CouponInstanceService couponInstanceService) {
		this.couponInstanceService = couponInstanceService;
	}*/

	@Autowired
	public void setCouponSummaryRepository(CouponSummaryRepository couponSummaryRepository) {
		this.couponSummaryRepository = couponSummaryRepository;
	}


	@Autowired
	public void setCouponBusinessTypeRepository(
			CouponBusinessTypeRepository couponBusinessTypeRepository) {
		this.couponBusinessTypeRepository = couponBusinessTypeRepository;
	}

	@Autowired
	public void setCouponTypeRepository(CouponTypeRepository couponTypeRepository) {
		this.couponTypeRepository = couponTypeRepository;
	}

	@Autowired
	public void setCouponInstanceRelayRepository(CouponInstanceRelayRepository couponInstanceRelayRepository) {
		this.couponInstanceRelayRepository = couponInstanceRelayRepository;
	}
	
	@Autowired
	public void setCouponInstanceReceiveRepository(CouponInstanceReceiveRepository couponInstanceReceiveRepository) {
		this.couponInstanceReceiveRepository = couponInstanceReceiveRepository;
	}
	@Autowired
	public void setCouponOriginRepository(CouponOriginRepository couponOriginRepository) {
		this.couponOriginRepository = couponOriginRepository;
	}

	@Override
	public Long selectCouponInstanceRelayList(CouponInstanceRelayExp relayExp,
			Page<CouponInstanceRelayExp> pageObj) {		
		Long count = null;
		List<CouponInstanceRelayExp> list =null;
		
		System.out.println("StatusId001="+relayExp.getStatusId());
		
		if(relayExp.getStatusId()==1){//转发中
			if(StringUtils.isNotBlank(relayExp.getGetCode())){//接收人
				relayExp.setGetCode(null);
			}
			if(StringUtils.isNotBlank(relayExp.getStartGetTime())){//接收时间  开始		
				relayExp.setStartGetTime(null);
			}
			if(StringUtils.isNotBlank(relayExp.getEndGetTime())){//接收时间  结束
				relayExp.setEndGetTime(null);
			}
			relayExp.setStatusId(StatusConstant.COUPON_INSTANCE_SHARE.getId());//状态为'转发中'
			
			count = this.couponInstanceRelayRepository.selectCouponInstanceRelayCount(relayExp);
			list = this.couponInstanceRelayRepository.selectCouponInstanceRelayList(relayExp, pageObj);
		}else if(relayExp.getStatusId()==2){//已转发
			if(StringUtils.isNotBlank(relayExp.getCreateTimeStart())){			
				relayExp.setCreateTimeStart(DateUtils.formatString(relayExp.getCreateTimeStart()));
			}
			if(StringUtils.isNotBlank(relayExp.getCreateTimeEnd())){
				relayExp.setCreateTimeEnd(DateUtils.formatString(relayExp.getCreateTimeEnd()));
			}
			if(StringUtils.isNotBlank(relayExp.getStartGetTime())){			
				relayExp.setStartGetTime(DateUtils.formatString(relayExp.getStartGetTime()+"000000"));
			}
			if(StringUtils.isNotBlank(relayExp.getEndGetTime())){
				relayExp.setEndGetTime(DateUtils.formatString(relayExp.getEndGetTime()+"000000"));
			}
			relayExp.setStatusId(null);
			
			count = this.couponInstanceRelayRepository.selectCouponInstanceReceiveCount(relayExp);
			list = this.couponInstanceRelayRepository.selectCouponInstanceReceiveList(relayExp, pageObj);
		}else{//已退回
			if(StringUtils.isNotBlank(relayExp.getCreateTimeStart())){			
				relayExp.setCreateTimeStart(DateUtils.formatString(relayExp.getCreateTimeStart()));
			}
			if(StringUtils.isNotBlank(relayExp.getCreateTimeEnd())){
				relayExp.setCreateTimeEnd(DateUtils.formatString(relayExp.getCreateTimeEnd()));
			}
			if(StringUtils.isNotBlank(relayExp.getStartGetTime())){			
				relayExp.setStartGetTime(DateUtils.formatString(relayExp.getStartGetTime()+"000000"));
			}
			if(StringUtils.isNotBlank(relayExp.getEndGetTime())){
				relayExp.setEndGetTime(DateUtils.formatString(relayExp.getEndGetTime()+"000000"));
			}
			relayExp.setStatusId(null);
			
			count = this.couponInstanceRelayRepository.selectCouponInstanceRollbackCount(relayExp);
			list = this.couponInstanceRelayRepository.selectCouponInstanceRollbackList(relayExp, pageObj);
		}
		
		return count;
	}	
	
	@Override
	public int couponrollback(Long couponInstanceId, Long srcId) {	
		int count = 0;
		int updateCount = 0;
		int saveCount = 0;
		CouponInstance instance = null;
		CouponInstanceReceive record = null;
		
		//修改优惠券实例
		instance = new CouponInstance();
		instance.setCouponInstanceId(couponInstanceId);
		instance.setStatusId(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
		
		updateCount = this.couponInstanceRepository.updateInstance(instance);
		
		//添加退回优惠券记录
		record = new CouponInstanceReceive();
		record.setCouponInstanceId(couponInstanceId);//优惠券实例ID
		record.setCouponInstanceSrc(srcId);//优惠券转发人
		record.setCouponInstanceGet(srcId);//优惠券领取人
		record.setGetTime(DateUtils.getCurrentTimeOfDb());//获得时间
		
		saveCount = this.couponInstanceReceiveRepository.insertSelective(record);
		
		if(updateCount > 0 && saveCount > 0){
			count = 1;
		}
		return count;
	}

	@Override
	public List<ExternalCouponMerchant> getExternalCouponMerchantsPaged(int page, int rows) {
		Page<ExternalCouponMerchant> p = new Page<ExternalCouponMerchant>();
		p.setPageNo(page);
		p.setPageSize(rows);
		return this.externalCouponMerchantRepository.queryExternalCouponMerchantList(p);
	}

	@Override
	public List<CouponOrigin> getCouponOriginList() {
		return couponOriginRepository.getCouponOriginList();
	}


	@Override
	public List<CouponForUse> queryCouponInstanceForUse(String couponInstanceCode) {
		return this.couponInstanceRepository.queryCouponInstanceForUse(couponInstanceCode);
	}


	@Autowired
	public void setCouponVerificationFilterDefault(CouponVerificationFilterDefault couponVerificationFilterDefault) {
		this.couponVerificationFilterDefault = couponVerificationFilterDefault;
	}



	@Override
	public Coupon findCouponByCode(String code) {
		return couponRepository.findCouponByCode(code);
	}

}
