package com.bw.adv.service.activity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.module.activity.model.FabuChannel;
import com.bw.adv.module.activity.repository.FabuRepository;
import com.bw.adv.service.activity.service.FabuService;


@Service
public class FabuServiceImpl implements FabuService{
	
	private FabuRepository fabuRepository;

	@Override
	public List<FabuChannel> queryAllGroupByChannel() {
		return fabuRepository.queryAllGroupByChannel();
	}

	@Autowired
	public void setFabuRepository(FabuRepository fabuRepository) {
		this.fabuRepository = fabuRepository;
	}

}
