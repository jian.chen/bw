package com.bw.adv.service.cfg.service;

import java.util.List;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.cfg.model.ChannelCfg;
import com.bw.adv.module.cfg.model.ChannelCfgExp;
import com.bw.adv.module.sys.model.SysUser;
/**
 * ClassName: ChannelCfgService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月21日 下午4:23:02 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
public interface ChannelCfgService extends BaseService<ChannelCfg> {
	
	/**
	 * 根据渠道查询配置信息
	 * queryChannelCfgByChannelId:(说明). <br/>
	 * Date: 2016年11月21日 <br/>
	 * scrmVersion 11.2
	 * @author cjian
	 * @version jdk1.7
	 * @param channelId
	 * @return
	 */
	public ChannelCfg queryChannelCfgByChannelId(Long channelId);

	public List<ChannelCfgExp> queryChannelInstanceByChannelId(Long channelId, Page<ChannelCfgExp> activityPage);

	public void modifyChannelCfgStatus(SysUser sessionUser, Long channelCfgId,Long status,String type);
	
	
	public List<ChannelCfgExp> queryChannelCfgExpByChannelId(Long channelId);
	
}