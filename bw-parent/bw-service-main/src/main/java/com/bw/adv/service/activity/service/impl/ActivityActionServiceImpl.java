/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:ActivityService.java
 * Package Name:com.sage.scrm.service.activity.service.impl
 * Date:2015-8-11下午1:50:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.activity.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.module.activity.constant.ActivityConstant;
import com.bw.adv.module.activity.enums.ActivityDateTypeEnum;
import com.bw.adv.module.activity.model.ActivityAction;
import com.bw.adv.module.activity.model.ActivityActionTask;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.repository.ActivityActionRepository;
import com.bw.adv.module.activity.repository.ActivityActionTaskRepository;
import com.bw.adv.module.activity.repository.ActivityInstanceRepository;
import com.bw.adv.module.base.model.ActivityDate;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.rule.constant.ConditionsConstant;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.ActivityActionService;
import com.bw.adv.service.base.service.ActivityDateService;

/**
 * ClassName:ActivityService <br/>
 * Date: 2015-8-11 下午1:50:36 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Service
public class ActivityActionServiceImpl extends BaseServiceImpl<ActivityAction> implements ActivityActionService{

	protected static final Logger logger = Logger.getLogger(ActivityActionServiceImpl.class);
	// 指定日期奖励
	@Value("${batch_insert_num}")
	private static Integer BATCH_INSERT_NUM;
	@Value("${activityActionScanSize}")
	private static Integer activityActionScanSize;
		
	
	private ActivityActionRepository activityActionRepository;
	private ActivityActionTaskRepository activityActionTaskRepository;
	
	
	private ActivityInstanceRepository activityInstanceRepository;
	private ActivityDateService activityDateService;
	private RuleInit ruleInit;
	
	
	@Override
	public BaseRepository<ActivityAction, ? extends BaseMapper<ActivityAction>> getBaseRepository() {
		return activityActionRepository;
	}
	
	@Override
	public void scanActivityAction() {
		List<ActivityAction> activityActionList = activityActionRepository.findByIsNotCountWithOrderByHappenTime(activityActionScanSize);
	}
	
	public void scanActivityActionTask() {
		List<ActivityActionTask> activityActionList = activityActionTaskRepository.findByIsNotCountWithOrderByHappenTime(activityActionScanSize);
	}
	
	
	/**
	 * TODO 会员唤醒设置（可选）.
	 * @see com.bw.adv.service.activity.service.ActivityActionService#addActivityActionMemberWake(com.bw.adv.module.activity.model.ActivityInstance)
	 */
	@Override
	public void addActivityActionMemberWake(ActivityInstance instance) {
		List<Member> memberList = null;
		ActivityActionTask activityAction = null;
		List<ActivityActionTask> activityActionList = null;
		String currentTime = null;
		JSONObject json = null;//活动规则条件
		JSONObject jsonParams = null;//活动规则条件
		String wakeDate = null;// 会员唤醒设置
		
		jsonParams = new JSONObject();
		json = RuleInit.getInstanceConditionJson(instance.getActivityInstanceId());
		// 获取唤醒设置
		wakeDate = json.get(ConditionsConstant.WAKE_DATE.getCode()).toString();
		memberList = activityInstanceRepository.findActivityWake(wakeDate);
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		int onlyone = (int)(memberList.size()/BATCH_INSERT_NUM);//批次数量
		for(int i = 0;i<onlyone;i++){
			activityActionList = new ArrayList<ActivityActionTask>();
			for(int j = 0;j < BATCH_INSERT_NUM;j++){
				activityAction = new ActivityActionTask();
				jsonParams = new JSONObject();
				activityAction.setActivityCode(ActivityConstant.ACTIVIY_MEMBER_WAKE.getCode());
				activityAction.setMemberId(memberList.get(i*BATCH_INSERT_NUM+j).getMemberId());
				activityAction.setCreateTime(currentTime);
				activityAction.setHappenTime(currentTime);
				activityAction.setIsCount("N");
				jsonParams.put("memberCode", memberList.get(i*BATCH_INSERT_NUM+j).getMemberCode());
				jsonParams.put("MEMBER_GROUP", memberList.get(i*BATCH_INSERT_NUM+j).getMemberGroupIds());
				jsonParams.put("MEMBER_TAG", memberList.get(i*BATCH_INSERT_NUM+j).getMemberTagIds());
				jsonParams.put("ACTIVITY_INSTANCE_ID", instance.getActivityInstanceId());
				activityAction.setRuleOptParams(jsonParams.toString());
				activityActionList.add(activityAction);
			}
			if(activityActionList.size() > 0){
				activityActionTaskRepository.saveList(activityActionList);
			}
		}
		
		if(memberList.size()%BATCH_INSERT_NUM>0){
			activityActionList = new ArrayList<ActivityActionTask>();
			for(int i=0;i<memberList.size()%BATCH_INSERT_NUM;i++){
				activityAction = new ActivityActionTask();
				jsonParams = new JSONObject();
				activityAction.setActivityCode(ActivityConstant.ACTIVIY_MEMBER_WAKE.getCode());
				activityAction.setMemberId(memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberId());
				activityAction.setCreateTime(currentTime);
				activityAction.setHappenTime(currentTime);
				activityAction.setIsCount("N");
				jsonParams.put("memberCode", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberCode());
				jsonParams.put("MEMBER_GROUP", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberGroupIds());
				jsonParams.put("MEMBER_TAG", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberTagIds());
				jsonParams.put("ACTIVITY_INSTANCE_ID", instance.getActivityInstanceId());
				activityAction.setRuleOptParams(jsonParams.toString());
				activityActionList.add(activityAction);
			}
			if(activityActionList.size() > 0){
				activityActionTaskRepository.saveList(activityActionList);
			}
		}
		
	}
	
	/**
	 * TODO 会员生日活动（可选）.
	 * @see com.bw.adv.service.activity.service.ActivityActionService#ActivityActionMemberBirthday()
	 */
	@Override
	public void addActivityActionMemberBirthday(ActivityInstance instance) {
		List<Member> memberList = null;
		ActivityActionTask activityAction = null;
		List<ActivityActionTask> activityActionList = null;
		String currentTime = null;
		JSONObject json = null;//活动规则条件
		JSONObject jsonParams = null;//活动规则条件
		String sendDate = null;
		
		jsonParams = new JSONObject();
		json = RuleInit.getInstanceConditionJson(instance.getActivityInstanceId());
		sendDate = json.get(ConditionsConstant.SEND_DATE.getCode()).toString();
		
		memberList = activityInstanceRepository.findActivityBirthday(sendDate);
		currentTime = DateUtils.getCurrentTimeOfDb();

		int onlyone = (int)(memberList.size()/BATCH_INSERT_NUM);//批次数量
		
		for(int i = 0;i<onlyone;i++){
			activityActionList = new ArrayList<ActivityActionTask>();
			for(int j = 0;j < BATCH_INSERT_NUM;j++){
				activityAction = new ActivityActionTask();
				jsonParams = new JSONObject();
				activityAction.setActivityCode(ActivityConstant.ACTIVIY_MEMBER_BIRTHDAY.getCode());
				activityAction.setMemberId(memberList.get(i*BATCH_INSERT_NUM+j).getMemberId());
				activityAction.setCreateTime(currentTime);
				activityAction.setHappenTime(currentTime);
				activityAction.setIsCount("N");
				jsonParams.put("memberCode", memberList.get(i*BATCH_INSERT_NUM+j).getMemberCode());
				jsonParams.put("birthday", memberList.get(i*BATCH_INSERT_NUM+j).getBirthday());
				jsonParams.put("MEMBER_GROUP", memberList.get(i*BATCH_INSERT_NUM+j).getMemberGroupIds());
				jsonParams.put("MEMBER_TAG", memberList.get(i*BATCH_INSERT_NUM+j).getMemberTagIds());
				jsonParams.put("ACTIVITY_INSTANCE_ID", instance.getActivityInstanceId());
				jsonParams.put("grade", memberList.get(i * BATCH_INSERT_NUM + j).getGradeId());
				activityAction.setRuleOptParams(jsonParams.toString());
				activityActionList.add(activityAction);
			}
			
			if(activityActionList.size() > 0){
				activityActionTaskRepository.saveList(activityActionList);
			}
		}
		
		if(memberList.size()%BATCH_INSERT_NUM>0){
			activityActionList = new ArrayList<ActivityActionTask>();
			for(int i=0;i<memberList.size()%BATCH_INSERT_NUM;i++){
				activityAction = new ActivityActionTask();
				jsonParams = new JSONObject();
				activityAction.setActivityCode(ActivityConstant.ACTIVIY_MEMBER_BIRTHDAY.getCode());
				activityAction.setMemberId(memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberId());
				activityAction.setCreateTime(currentTime);
				activityAction.setHappenTime(currentTime);
				activityAction.setIsCount("N");
				jsonParams.put("memberCode", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberCode());
				jsonParams.put("birthday", memberList.get(onlyone*BATCH_INSERT_NUM+i).getBirthday());
				jsonParams.put("MEMBER_GROUP", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberGroupIds());
				jsonParams.put("MEMBER_TAG", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberTagIds());
				jsonParams.put("ACTIVITY_INSTANCE_ID", instance.getActivityInstanceId());
				jsonParams.put("grade", memberList.get(onlyone * BATCH_INSERT_NUM + i).getGradeId());
				activityAction.setRuleOptParams(jsonParams.toString());
				activityActionList.add(activityAction);
			}

			if(activityActionList.size() > 0){
				activityActionTaskRepository.saveList(activityActionList);
			}
		}
	}
	
	/**
	 * 
	 * 指定日期奖励
	 * @see com.bw.adv.service.activity.service.ActivityActionService#addActivityActionSpecialDate(com.bw.adv.module.activity.model.ActivityInstance)
	 */
	@Override
	public void addActivityActionSpecialDate(ActivityInstance instance) {
		List<Member> memberList = null;
		ActivityActionTask activityAction = null;
		List<ActivityActionTask> activityActionList = null;
		String currentTime = null;
		JSONObject json = null;//活动规则条件
		JSONObject jsonParams = null;//活动规则条件
		String appointDateType = null;// 指定日期
		ActivityDate activityDate =null;// 指定日期
		String fromDate =null;// 指定日期的开始日期
		String currentDate =null;// 当前日期
		boolean flag =false;
		
		currentTime = DateUtils.getCurrentTimeOfDb();
		activityActionList = new ArrayList<ActivityActionTask>();
		
		jsonParams = new JSONObject();
		json = RuleInit.getInstanceConditionJson(instance.getActivityInstanceId());
		// 获取有效的memberList
		// 获取指定日期
		appointDateType = json.get(ConditionsConstant.APPOINT_DATE.getCode()).toString();
		activityDate =activityDateService.queryByPk(appointDateType);
		flag =compareCurrentDateAndSpecialDate(activityDate);
		
		if(!flag){
			return;
		}
		memberList = activityInstanceRepository.findActivitySpecial();
		int onlyone = (int)(memberList.size()/BATCH_INSERT_NUM);//批次数量
		for(int i = 0;i<onlyone;i++){
			activityActionList = new ArrayList<ActivityActionTask>();
			for(int j = 0;j < BATCH_INSERT_NUM;j++){
				activityAction = new ActivityActionTask();  
				jsonParams = new JSONObject();
				
				activityAction.setActivityCode(ActivityConstant.ACTIVIY_SPECIAL_DATE.getCode());
				activityAction.setMemberId(memberList.get(i*BATCH_INSERT_NUM+j).getMemberId());
				activityAction.setCreateTime(currentTime);
				activityAction.setHappenTime(currentTime);
				activityAction.setIsCount("N");
				jsonParams.put("memberCode", memberList.get(i*BATCH_INSERT_NUM+j).getMemberCode());
				jsonParams.put("MEMBER_GROUP", memberList.get(i*BATCH_INSERT_NUM+j).getMemberGroupIds());
				jsonParams.put("MEMBER_TAG", memberList.get(i*BATCH_INSERT_NUM+j).getMemberTagIds());
				jsonParams.put("ACTIVITY_INSTANCE_ID", instance.getActivityInstanceId());
				activityAction.setRuleOptParams(jsonParams.toString());
				activityActionList.add(activityAction);
			}
			if(activityActionList.size() > 0){
				activityActionTaskRepository.saveList(activityActionList);
			}
		}
		
		if(memberList.size()%BATCH_INSERT_NUM >0){
			activityActionList = new ArrayList<ActivityActionTask>();
			for(int i=0;i<memberList.size()%BATCH_INSERT_NUM;i++){
				activityAction = new ActivityActionTask();  
				jsonParams = new JSONObject();
				
				activityAction.setActivityCode(ActivityConstant.ACTIVIY_SPECIAL_DATE.getCode());
				activityAction.setMemberId(memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberId());
				activityAction.setCreateTime(currentTime);
				activityAction.setHappenTime(currentTime);
				activityAction.setIsCount("N");
				jsonParams.put("memberCode", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberCode());
				jsonParams.put("MEMBER_GROUP", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberGroupIds());
				jsonParams.put("MEMBER_TAG", memberList.get(onlyone*BATCH_INSERT_NUM+i).getMemberTagIds());
				jsonParams.put("ACTIVITY_INSTANCE_ID", instance.getActivityInstanceId());
				activityAction.setRuleOptParams(jsonParams.toString());
				activityActionList.add(activityAction);
			}
			if(activityActionList.size() > 0){
				activityActionTaskRepository.saveList(activityActionList);
			}
		}
	}
	
	private boolean compareCurrentDateAndSpecialDate(ActivityDate activityDate) {
		String activityDateType=null;
		boolean flag =false;
		String compareDate =null;
		activityDateType =activityDate.getActivityDateType();
		String dateValueType=null;
		String currentDate  =DateUtils.formatDate(new Date(), DateUtils.DATE_PATTERN_YYYYMMDD);
		// 固定日期
		if(ActivityDateTypeEnum.FIXED.getId().equals(activityDateType)){
			if(currentDate.equals(activityDate.getFromDate())){
				flag =true;
			}
		// 滚动日期
		}else if(ActivityDateTypeEnum.CYCLE.getId().equals(activityDateType)){
			// 年，月，日
			dateValueType = activityDate.getDateValue().split("/")[1];
			if(dateValueType.equals(ActivityDateTypeEnum.YEAR.getDesc())){
				compareDate =DateUtils.formatDate(new Date(), DateUtils.DATE_PATTERN_YYYY)+DateUtils.formatString(activityDate.getFromDate());
				if(currentDate.equals(compareDate)){
					flag =true;
				}
			}
			
			if(dateValueType.equals(ActivityDateTypeEnum.MONTH.getDesc())){
				compareDate = DateUtils.formatDate(new Date(), DateUtils.DATE_PATTERN_YYYY)+DateUtils.formatDate(new Date(), DateUtils.DATE_PATTERN_MM)+activityDate.getFromDate();
				if(currentDate.equals(compareDate)){
					flag =true;
				}
			}
			
			if(dateValueType.equals(ActivityDateTypeEnum.WEEK.getDesc())){
				if(DateUtils.getCurrentWeekDaySEN().equals(ActivityDateTypeEnum.MON.getDesc())){
					compareDate =ActivityDateTypeEnum.MON.getId();
				}else if(DateUtils.getCurrentWeekDaySEN().equals(ActivityDateTypeEnum.TUE.getDesc())){
					compareDate =ActivityDateTypeEnum.TUE.getId();
				}else if(DateUtils.getCurrentWeekDaySEN().equals(ActivityDateTypeEnum.WED.getDesc())){
					compareDate =ActivityDateTypeEnum.WED.getId();
				}else if(DateUtils.getCurrentWeekDaySEN().equals(ActivityDateTypeEnum.THU.getDesc())){
					compareDate =ActivityDateTypeEnum.THU.getId();
				}else if(DateUtils.getCurrentWeekDaySEN().equals(ActivityDateTypeEnum.FRI.getDesc())){
					compareDate =ActivityDateTypeEnum.FRI.getId();
				}else if(DateUtils.getCurrentWeekDaySEN().equals(ActivityDateTypeEnum.SAT.getDesc())){
					compareDate =ActivityDateTypeEnum.SAT.getId();
				}else{
					compareDate =ActivityDateTypeEnum.SUN.getId();
				} 
				// 星期相等
				if(compareDate.equals(activityDate.getFromDate())){
					flag =true;
				}
			}
		}
		return flag;
	}

	public void checkActivityDate() {
		List<ActivityInstance> activityInstanceList = null;
		//当前日期
		String currentDate = null;
		ActivityInstance acInsTemp = null;
		
		//查询所有活动实例
		activityInstanceList = activityInstanceRepository.findAll();
		
		// 当前日期
		currentDate =DateUtils.formatDate(new Date(), DateUtils.DATE_PATTERN_YYYYMMDD);
		
		//遍历 已创建和已启用 的活动
		for(int i=0;i<activityInstanceList.size();i++){
			acInsTemp = new ActivityInstance();
			acInsTemp.setActivityInstanceId(activityInstanceList.get(i).getActivityInstanceId());
			//已创建活动
			if(StatusConstant.ACTIVITY_INSTANCE_DRAFT.getId().longValue() == activityInstanceList.get(i).getStatusId().longValue()){
				//开始时间早于或等于当前日期,修改状态为启用
				if(currentDate.compareTo(activityInstanceList.get(i).getFromDate()) >= 0){
					acInsTemp.setStatusId(StatusConstant.ACTIVITY_INSTANCE_ENABLE.getId());
					activityInstanceRepository.updateByPkSelective(acInsTemp);
					ruleInit.reLoad();
				}
				//结束日期早于当前日期,修改状态为已过期
				if(StringUtils.isNotBlank(activityInstanceList.get(i).getThruDate())){
					if(currentDate.compareTo(activityInstanceList.get(i).getThruDate()) > 0){
						acInsTemp.setStatusId(StatusConstant.ACTIVITY_INSTANCE_EXPIRED.getId());
						activityInstanceRepository.updateByPkSelective(acInsTemp);
						ruleInit.reLoad();
					}
				}
			}
			//已启用活动
			if(StatusConstant.ACTIVITY_INSTANCE_ENABLE.getId().longValue() == activityInstanceList.get(i).getStatusId().longValue()){
				//结束日期早于当前日期,修改状态为已过期
				if(StringUtils.isNotBlank(activityInstanceList.get(i).getThruDate())){
					if(currentDate.compareTo(activityInstanceList.get(i).getThruDate()) == 1){
						acInsTemp.setStatusId(StatusConstant.ACTIVITY_INSTANCE_EXPIRED.getId());
						activityInstanceRepository.updateByPkSelective(acInsTemp);
						ruleInit.reLoad();
					}
				}
			}
		}
		
	}
	
	
	
	@Autowired
	public void setActivityInstanceRepository(ActivityInstanceRepository activityInstanceRepository) {
		this.activityInstanceRepository = activityInstanceRepository;
	}

	@Autowired
	public void setActivityActionRepository(ActivityActionRepository activityActionRepository) {
		this.activityActionRepository = activityActionRepository;
	}
	
	@Autowired
	public void setActivityDateService(ActivityDateService activityDateService) {
		this.activityDateService = activityDateService;
	}
	
	@Autowired
	public void setActivityActionTaskRepository(
			ActivityActionTaskRepository activityActionTaskRepository) {
		this.activityActionTaskRepository = activityActionTaskRepository;
	}
	@Autowired
	public void setRuleInit(RuleInit ruleInit) {
		this.ruleInit = ruleInit;
	}

	public static void main(String[] args) {
		System.out.println(BATCH_INSERT_NUM);
	}

}

