/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatImageTemplate.java
 * Package Name:com.sage.scrm.service.wechat.msg.model
 * Date:2016年5月24日下午2:48:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.msg.model;

import java.util.Map;

/**
 * ClassName:WechatImageTemplate <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月24日 下午2:48:11 <br/>
 * scrmVersion 1.0
 * @author   chengdi.cui
 * @version  jdk1.7
 * @see 	 
 */
public class WechatImageTemplate {
	
	private String touser;
	
	private String msgtype;
	
    private Map<String, String> image;

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public Map<String, String> getImage() {
		return image;
	}

	public void setImage(Map<String, String> image) {
		this.image = image;
	}

}

