/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-service-main
 * File Name:WechatGroupMessageService.java
 * Package Name:com.sage.scrm.service.wechat.service
 * Date:2015年8月18日下午4:58:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.service.wechat.service;

import java.util.List;
import java.util.Map;

import com.bw.adv.core.service.BaseService;
import com.bw.adv.module.wechat.model.WechatGroupMessage;

/**
 * ClassName:WechatGroupMessageService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午4:58:39 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public interface WechatGroupMessageService extends BaseService<WechatGroupMessage> {
	
	public List<?> queryGroupList(String type);
	
	/**
	 * sendWechatGroupMessage:(微信发送消息并保存记录). <br/>
	 * Date: 2015-9-6 上午11:55:14 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param paramsMap
	 */
	public void sendWechatGroupMessage(Map<String,Object> paramsMap);
	
}

