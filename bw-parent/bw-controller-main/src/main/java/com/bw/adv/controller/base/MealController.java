/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:MealController.java
 * Package Name:com.sage.scrm.controller.base
 * Date:2016年5月23日下午5:05:13
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.MealPeriod;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.model.exp.StoreExp;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.search.MemberGroupSearch;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.MealPeriodService;

/**
 * ClassName:MealController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月23日 下午5:05:13 <br/>
 * scrmVersion 1.0
 * @author   zhangqi
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/mealController")
@SuppressWarnings("unused")
public class MealController {
	
	private MealPeriodService mealPeriodService;
	
	/**
	 * 
	 * showMealPeriodList:查询时段列表. <br/>
	 * Date: 2016年5月23日 下午5:29:44 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showMealPeriodList")
	@ResponseBody
	private Map<String, Object> showMealPeriodList(int page,int rows){
		List<MealPeriod> mealPeriodList = null;
		Page<MealPeriod> pageObj = null;
		Map<String, Object> map = null;
		
		try {
			map = new HashMap<String, Object>();
			pageObj = new Page<MealPeriod>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			mealPeriodList = mealPeriodService.queryMealPeriodByStatusId(pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return map;
	}
	
	/**
	 * 
	 * showMealPeriodById:查看时段详情. <br/>
	 * Date: 2016年5月24日 下午1:45:12 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showMealPeriodById")
	@ResponseBody
	public JsonModel showMealPeriodById(Long mealPeriodId){
		JsonModel result = null;
		Map<String,Object> map = null;
		MealPeriod meal = null;
		
		try {
			result = new JsonModel();
			map = new HashMap<String, Object>();
			meal = mealPeriodService.selectByPrimaryKey(mealPeriodId);	
			map.put("meal", meal);
			result.setObj(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}	
	
	/**
	 * 
	 * showCountByNameById:查询时段名称是否有相同的(排除自身的时段名称). <br/>
	 * Date: 2016年5月24日 下午4:24:18 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodName
	 * @param mealPeriodId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showCountByNameById")
	@ResponseBody
	public JsonModel showCountByNameById(String mealPeriodName,Long mealPeriodId) {
		JsonModel result = null;
		Long count = null;
		
		try {
			count = this.mealPeriodService.selectByMealPeriodNameById(mealPeriodName, mealPeriodId);
			if(count>0){
				result = new JsonModel(JsonModel.Status_Error,"时段名称已存在，请修改时段名称");
				return result;
			}
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * saveMealPeriod:新建时段. <br/>
	 * Date: 2016年5月24日 下午1:11:54 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param meal
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMealPeriod")
	@ResponseBody
	public JsonModel saveMealPeriod(MealPeriod meal){
		JsonModel result = null;
		Long count = 0L;
		int insertCount=0;
		
		try {			
			if(StringUtils.isNotBlank(meal.getMealPeriodName())){
				count = this.mealPeriodService.selectByMealPeriodNameById(meal.getMealPeriodName(),null);
				if(count > 0){
					result = new JsonModel(JsonModel.Status_Error,"时段名称已存在，请修改时段名称");
					return result;
				}
			}
			
			insertCount = mealPeriodService.insertSelective(meal);
			if(insertCount > 0){
				result = new JsonModel(JsonModel.Status_Success,"新建成功！");
			}else{
				result = new JsonModel(JsonModel.Status_Error, "新建失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return result;
	}
	
	/**
	 * 
	 * editMealPeriod:编辑时段. <br/>
	 * Date: 2016年5月24日 下午2:42:00 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param meal
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editMealPeriod")
	@ResponseBody
	public JsonModel editMealPeriod(MealPeriod meal){
		JsonModel result = null;
		Long count = 0L;
		int updateCount = 0;
		
		try {			
			if(StringUtils.isNotBlank(meal.getMealPeriodName())){
				count = this.mealPeriodService.selectByMealPeriodNameById(meal.getMealPeriodName(), meal.getMealPeriodId());
				if(count > 0){
					result = new JsonModel(JsonModel.Status_Error,"时段名称已存在，请修改时段名称");
					return result;
				}
			}
			
			updateCount = mealPeriodService.updateMealPeriod(meal);
			if(updateCount > 0){
				result = new JsonModel(JsonModel.Status_Success,"编辑成功！");
			}else{
				result = new JsonModel(JsonModel.Status_Error, "编辑失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return result;
	}

	/**
	 * 
	 * removeMealPeriod:批量删除时段（逻辑删除）. <br/>
	 * Date: 2016年5月24日 下午3:12:11 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "removeMealPeriod")
	@ResponseBody
	public JsonModel removeMealPeriod(String mealPeriodIds) {
		JsonModel result = null;
		int deleteCount = 0;		
		
		try {			
			deleteCount = mealPeriodService.deleteMealPeriodList(mealPeriodIds);
			if(deleteCount > 0){
				result = new JsonModel(JsonModel.Status_Success,"删除成功！");
			}else{
				result = new JsonModel(JsonModel.Status_Error, "删除失败，请稍后重试！");
			}	
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMealPeriodListSelect:查询时段信息. <br/>
	 * Date: 2016年5月25日 下午6:39:39 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMealPeriodListSelect")
	@ResponseBody
	public List<MealPeriod> showMealPeriodListSelect() {
		List<MealPeriod> result = null;
		
		try {
			result = this.mealPeriodService.queryMealPeriodByStatusId(null);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	@Autowired
	public void setMealPeriodService(MealPeriodService mealPeriodService) {
		this.mealPeriodService = mealPeriodService;
	}
}

