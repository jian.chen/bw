/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:StoreController.java
 * Package Name:com.sage.scrm.controller.base
 * Date:2015年8月26日上午10:50:01
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.model.exp.StoreExp;
import com.bw.adv.module.common.init.GeoInit;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.OrgService;
import com.bw.adv.service.base.service.StoreService;

/**
 * ClassName:StoreController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月26日 上午10:50:01 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/storeController")
@SuppressWarnings("unused")
public class StoreController {
	
	private StoreService storeService;
	
	private OrgService orgService;
	
	/**
	 * 
	 * showStoreList:查看门店列表
	 * Date: 2015年8月26日 上午11:05:39 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showStoreList")
	@ResponseBody
	private Map<String, Object> showStoreList(int page,int rows){
		List<Store> storeList = null;
		Page<Store> pageObj = null;
		Map<String, Object> map = null;
		
		try {
			map = new HashMap<String, Object>();
			pageObj = new Page<Store>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			storeList = storeService.queryStoreByStatusId(pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * 
	 * showStoreById:查看门店详情
	 * Date: 2015年8月26日 上午11:10:36 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param storeId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showStoreById")
	@ResponseBody
	public Map<String, Object> showStoreById(Long storeId){
		Map<String, Object> result = null;
		StoreExp store = null;
		GeoInit geoInit = null;
		Long provinceId = null;
		String cityName = null;
		String provinceName = null;
		Org org = null;
		
		try {
			result = new HashMap<String, Object>();
			
			store = storeService.queryById(storeId);
			
			if(store.getPareOrgId() != null){
				
				org = orgService.queryByPk(store.getPareOrgId());
				store.setPareOrgName(org.getOrgName());
			}

			if(store.getGeoId()!=null){
				cityName = GeoInit.getGeoByGeoId(store.getGeoId()).getGeoName();
				provinceName = GeoInit.getParentGeoByGeoId(store.getGeoId()).getGeoName();
				provinceId = GeoInit.getParentGeoByGeoId(store.getGeoId()).getGeoId();
			}
			
			result.put("store", store);
			result.put("provinceName", provinceName);
			result.put("provinceId", provinceId);
			result.put("cityName", cityName);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	/**
	 * 
	 * editStore:保存门店修改信息
	 * Date: 2015年8月26日 上午11:13:12 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param store
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editStore")
	@ResponseBody
	public JsonModel editStore(StoreExp store){
		JsonModel result = null;
		Org storeOrg = null;
		Store storeTemp = null;
		
		try {
			
			storeTemp = storeService.queryByCode(store.getStoreCode());
			//判断门店编号是否重复
			if(storeTemp == null || storeTemp.getStoreId().equals(store.getStoreId())){
				storeOrg = new Org();
				//如果门店已有区域信息，则修改；否则判断区域是否设置，设置则新建一条区域信息
				if(store.getOrgId() != null){
					if(store.getPareOrgId() != null){
						storeOrg.setOrgId(store.getOrgId());
						storeOrg.setPareOrgId(store.getPareOrgId());
						storeOrg.setOrgName(store.getStoreName());
						storeOrg.setOrgTypeId(4l);
						
						orgService.updateByPkSelective(storeOrg);
					}else{
						orgService.removeByPk(store.getOrgId());
					}
				}else{
					if(store.getPareOrgId() != null){
						storeOrg.setOrgTypeId(4l);
						storeOrg.setUseFlag("Y");
						storeOrg.setPareOrgId(store.getPareOrgId());
						storeOrg.setOrgName(store.getStoreName());
						storeOrg.setStartDate(DateUtils.getCurrentDateOfDb());
						
						orgService.saveSelective(storeOrg);
						store.setOrgId(storeOrg.getOrgId());
					}
				}
				
				storeService.updateByPkSelective(store);
				result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			}else{
				result = new JsonModel(JsonModel.Status_Error,"此编号已存在，请重新输入！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * saveStore:保存新建门店
	 * Date: 2015年8月26日 下午2:18:22 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param store
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveStore")
	@ResponseBody
	public JsonModel saveStore(StoreExp store){
		JsonModel result = null;
		Org storeOrg = null;
		Store storeTemp = null;
		
		try {
			storeTemp = storeService.queryByCode(store.getStoreCode());
			if(storeTemp == null){
				//如果设置了门店区域，则新建一条区域信息
				if(store.getPareOrgId() != null){
					storeOrg = new Org();
					storeOrg.setOrgTypeId(4l);
					storeOrg.setUseFlag("Y");
					storeOrg.setPareOrgId(store.getPareOrgId());
					storeOrg.setOrgName(store.getStoreName());
					storeOrg.setStartDate(DateUtils.getCurrentDateOfDb());
					
					orgService.saveSelective(storeOrg);
					store.setOrgId(storeOrg.getOrgId());
				}
				
				storeService.saveSelective(store);
				
				result = new JsonModel(JsonModel.Status_Success,"新建成功！");
			}else{
				result = new JsonModel(JsonModel.Status_Error, "此编号已存在，请重新输入！");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * removeStoreList:批量删除门店（逻辑删除）
	 * Date: 2015年8月26日 上午11:34:26 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param storeIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "removeStoreList")
	@ResponseBody
	public int removeStoreList(String storeIds){
		List<Long> storeIdList = null;
		String[] ids = null;
		int result = 0;
		
		try {
			storeIdList = new ArrayList<Long>();
			ids = storeIds.split(",");
			for(int i=0;i<ids.length;i++){
				storeIdList.add(Long.parseLong(ids[i]));
			}
			
			storeService.deleteStoreList(storeIdList);
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public StoreService getStoreService() {
		return storeService;
	}
	
	@Autowired
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}

	@Autowired
	public void setOrgService(OrgService orgService) {
		this.orgService = orgService;
	}

	

}

