package com.bw.adv.controller.member;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.filter.model.DataGridFilterModel;
import com.bw.adv.filter.model.JsonFilterModel;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.model.SqlAttribute;
import com.bw.adv.module.common.model.TreeMenuModel;
import com.bw.adv.module.common.utils.MyConstants;
import com.bw.adv.module.common.utils.TreeUtil;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.CouponSummaryDay;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.model.ExtAccountBinding;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.model.MemberExtInfo;
import com.bw.adv.module.member.model.MemberSummaryDay;
import com.bw.adv.module.member.model.charts.Attribute;
import com.bw.adv.module.member.model.charts.Attributes;
import com.bw.adv.module.member.model.charts.Attvalue;
import com.bw.adv.module.member.model.charts.Edge;
import com.bw.adv.module.member.model.charts.Gexf;
import com.bw.adv.module.member.model.charts.Graph;
import com.bw.adv.module.member.model.charts.Node;
import com.bw.adv.module.member.model.exp.GradeConvertRecordExp;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.StringUtil;
import com.bw.adv.module.wechat.model.WechatFansMessage;
import com.bw.adv.service.coupon.service.CouponService;
import com.bw.adv.service.member.service.MemberService;

/**
 * Hello world!
 * 
 */
@Controller
@RequestMapping("/memberController")
public class MemberController {
	
	protected Log log = LogFactory.getLog(this.getClass());
	private MemberService memberService;
	private CouponService couponService;
	
	private CodeService codeService;
	
	//private SysFunctionService 
	@RequestMapping(method = RequestMethod.GET, value = "testTree")
	@ResponseBody
	public List<TreeMenuModel> testTree() {
		List<TreeMenuModel> treeList = new ArrayList<TreeMenuModel>();
		List<TreeMenuModel> result = new ArrayList<TreeMenuModel>();
		try {
			TreeMenuModel nodes = new TreeMenuModel();
			nodes.setId("10001");
			nodes.setPid("");
			nodes.setName("全部");
			result.add(nodes);
			for(int i=1;i<10;i++){
				TreeMenuModel node = new TreeMenuModel();
				node.setId(""+i);
				node.setPid("10001");
				node.setName("二级节点"+i);
				result.add(node);
				for(int j=1;j<100;j++){
					TreeMenuModel node2 = new TreeMenuModel();
					node2.setId(""+i+j);
					node2.setPid(""+i);
					node2.setName("三级节点"+j);
					result.add(node2);
				}
			}
			treeList = TreeUtil.dualWithTreeModel(result);  
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return treeList;
	}

	/**
	 * 
	 * checkMemberSql:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年8月28日 上午11:18:16 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param sql
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "checkMemberSql")
	@ResponseBody
	public JsonModel checkMemberSql(String sql) {
		JsonModel result = null;
		try {
			this.memberService.checkMemberSql(sql);
			result = new JsonModel(JsonModel.Status_Success,"sql校验成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
			result = new JsonModel(JsonModel.Status_Error,"sql校验失败！");
		}
		return result;
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "showSqlAttrList")
	@ResponseBody
	public List<SqlAttribute> showSqlAttrList(Long type) {
		List<SqlAttribute> result = null;
		try {
			result = this.memberService.querySqlAttrList(type);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new ArrayList<SqlAttribute>();
		return result;
	}
	
	/**
	 * 
	 * showMemberList:根据条件查询会员列表 <br/>
	 * Date: 2015年8月14日 下午2:12:47 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @param sort
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showMemberList")
	@ResponseBody
	public DataGridModel<MemberExp> showMemberList(MemberSearch search,String sort,String order) {
		DataGridFilterModel<MemberExp> result = null;
		List<MemberExp> list = null;
		Long count = null;
		String memberGroupIds = null;
		String memberTagIds = null;
		String orgIds = null;
		try {
			if(StringUtils.isNotBlank(sort) && sort.equals("registerTime")){
				search.setSort("member.REGISTER_TIME");
			}
			if(StringUtils.isNotBlank(sort) && sort.equals("pointsBalance")){
				search.setSort("account.POINTS_BALANCE");
			}
			memberGroupIds = search.getMemberGroupIds();
			memberTagIds = search.getMemberTagIds();
			orgIds = search.getEqualOrgId();
			
			search.setGreaterEqualBirthday(DateUtils.formatString(search.getGreaterEqualBirthday()));
			if(StringUtils.isNotBlank(search.getLessEqualBirthday())){
				search.setLessEqualBirthday(DateUtils.formatString(search.getLessEqualBirthday()+"235959"));
			}
			search.setGreaterEqualRegisterTime(DateUtils.formatString(search.getGreaterEqualRegisterTime()));
			if(StringUtils.isNotBlank(search.getLessEqualRegisterTime())){
				search.setLessEqualRegisterTime(DateUtils.formatString(search.getLessEqualRegisterTime()+"235959"));
			}
			
			if(StringUtils.isNotBlank(memberGroupIds)){
				search.setInMemberGroupIds(memberGroupIds.split(","));
			}
			if(StringUtils.isNotBlank(memberTagIds)){
				search.setInMemberTagIds(memberTagIds.split(","));
			}
			if(StringUtils.isNotBlank(orgIds)){
				search.setInOrgId(orgIds.split(","));
			}
			count = this.memberService.queryCountSearchAuto(search);
			list = this.memberService.queryListSearchAuto(search);
			result = new DataGridFilterModel<MemberExp>(list, count);
			
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
			result = new DataGridFilterModel<MemberExp>("查询列表失败,请重试!");
		}
		return result;
	}
	
	/**
	 * 根据条件查询会员list
	 * showMemberListAndSum: <br/>
	 * Date: 2016年5月10日 下午4:19:33 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/showMemberListAndSum")
	@ResponseBody
	public JsonModel showMemberListAndSum(MemberSearch search){
		JsonModel model = null;
		List<MemberExp> list = null;
		String memberGroupIds = null;
		String memberTagIds = null;
		String orgIds = null;
		try {
			model = new JsonModel();
			list = new ArrayList<MemberExp>();
			memberGroupIds = search.getMemberGroupIds();
			memberTagIds = search.getMemberTagIds();
			orgIds = search.getEqualOrgId();
			
			search.setGreaterEqualBirthday(DateUtils.formatString(search.getGreaterEqualBirthday()));
			if(StringUtils.isNotBlank(search.getLessEqualBirthday())){
				search.setLessEqualBirthday(DateUtils.formatString(search.getLessEqualBirthday()+"235959"));
			}
			search.setGreaterEqualRegisterTime(DateUtils.formatString(search.getGreaterEqualRegisterTime()));
			if(StringUtils.isNotBlank(search.getLessEqualRegisterTime())){
				search.setLessEqualRegisterTime(DateUtils.formatString(search.getLessEqualRegisterTime()+"235959"));
			}
			
			if(StringUtils.isNotBlank(memberGroupIds)){
				search.setInMemberGroupIds(memberGroupIds.split(","));
			}
			if(StringUtils.isNotBlank(memberTagIds)){
				search.setInMemberTagIds(memberTagIds.split(","));
			}
			if(StringUtils.isNotBlank(orgIds)){
				search.setInOrgId(orgIds.split(","));
			}
			list = this.memberService.queryListSearchAuto(search);
			model.setObj(list);
			model.setStatusSuccess();
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			model.setStatusError();
			return model;
		}
	}
	
	
	/**
	 * 
	 * showMemberDetail:根据会员id查询会员基本信息 <br/>
	 * Date: 2015年8月14日 下午2:13:18 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberDetail")
	@ResponseBody
	public JsonFilterModel showMemberDetail(Long memberId) {
		MemberExp memberExp = null;
		JsonFilterModel model = new JsonFilterModel();
		try {
			memberExp = this.memberService.queryEntityPkAuto(memberId);
			model.setObj(memberExp);
			model.setStatusSuccess();
			model.setMessage("query success");
		}catch (Exception ex) {
			ex.printStackTrace();
			model.setStatusError();
			model.setMessage("query error");
		}
		return model;
	}
	
	/**
	 * 
	 * showMemberGradeConvert:查询会员等级转换记录 <br/>
	 * Date: 2015年8月21日 上午11:43:00 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberGradeConvert")
	@ResponseBody
	public DataGridModel<GradeConvertRecordExp> showMemberGradeConvert(Long memberId) {
		List<GradeConvertRecordExp> list = null;
		DataGridModel<GradeConvertRecordExp> result = null;
		try {
			list = this.memberService.queryGradeConvertByMemberId(memberId);
			result =  new DataGridModel<GradeConvertRecordExp>(list, (long)(list==null?0:list.size()));
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberContactInfo:根据会员id查询会员的联系信息 <br/>
	 * Date: 2015年8月14日 下午2:13:57 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberContactInfo")
	@ResponseBody
	public Map<String,Object> showMemberContactInfo(Long memberId) {
		Map<String,Object> result = null;
		MemberExp member = null;
		List<MemberAddress> addressList = null;
		try {
			result = new HashMap<String, Object>();
			member = this.memberService.queryContactInfoByMemberId(memberId);
			addressList =  this.memberService.queryMemberAddressByMemberId(memberId);
			result.put("memberContact", member);
			result.put("rows", addressList);
			result.put("total", addressList==null?0:addressList.size());
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberTagAndGroup:根据会员id查询会员的标签和分组 <br/>
	 * Date: 2015年8月14日 下午2:14:26 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberTagAndGroup")
	@ResponseBody
	public Map<String,Object> showMemberTagAndGroup(Long memberId) {
		Map<String,Object> result = null;
		List<MemberTag> tagList = null;
		List<MemberGroup> groupList = null;
		try {
			result = new HashMap<String, Object>();
			tagList = this.memberService.queryMemberTagByMemberId(memberId);
			groupList =  this.memberService.queryMemberGroupByMemberId(memberId);
			result.put("tagList", tagList);
			result.put("groupList", groupList);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberExtInfo:根据会员id查询会员其他信息 <br/>
	 * Date: 2015年8月14日 下午2:15:28 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberExtInfo")
	@ResponseBody
	public MemberExtInfo showMemberExtInfo(Long memberId) {
		MemberExtInfo result = null;
		try {
			result = this.memberService.queryExtInfoByMemberId(memberId);
			return result==null?new MemberExtInfo():result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberPoints:根据条件查询会员积分历史 <br/>
	 * Date: 2015年8月14日 下午2:16:05 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param search
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/showMemberPoints")
	@ResponseBody
	public Map<String,Object> showMemberPoints(@RequestParam("memberId") Long memberId,MemberSearch search) {
		Map<String,Object> result = null;
		List<MemberPointsItemExp> list = null;
		Long total = null;
		try {
			search.setHappenTime(DateUtils.formatString(search.getHappenTime()));
			search.setEqualMemberId(memberId);
			total = this.memberService.queryCountMemberPointsByMemberId(search);
			list = this.memberService.queryMemberPointsByMemberId(search);
			result =  new HashMap<String, Object>();
			result.put("total", total);
			result.put("rows", list);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberOrderHeader:根据条件查询会员的消费记录 <br/>
	 * Date: 2015年8月14日 下午2:23:06 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/showMemberOrderHeader")
	@ResponseBody
	public Map<String,Object> showMemberOrderHeader(@RequestParam("memberId") Long memberId, MemberSearch search) {
		List<OrderHeader> list = null;
		Map<String,Object> result = null;
		Long total = null;
		try {
			search.setEqualMemberId(memberId);
			search.setOrderDate(DateUtils.formatString(search.getOrderDate()));
			total = this.memberService.queryCountOrderHeaderByMemberId(search);
			list = this.memberService.queryOrderHeaderByMemberId(search);
			result =  new HashMap<String, Object>();
			result.put("total", total);
			result.put("rows", list);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberCouponInstance:根据条件查询会员的优惠劵信息 <br/>
	 * Date: 2015年8月14日 下午2:48:26 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param search
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/showMemberCouponInstance")
	@ResponseBody
	public Map<String,Object> showMemberCouponInstance(@RequestParam("memberId") Long memberId,MemberSearch search) {
		List<CouponInstance> list = null;
		Map<String,Object> result = null;
		Long total = null;
		try {
			search.setEqualMemberId(memberId);
			search.setUsedTime(DateUtils.formatString(search.getUsedTime()));
			total = this.memberService.queryCountCouponInstanceByMemberId(search);
			list = this.memberService.queryCouponInstanceByMemberId(search);
			result =  new HashMap<String, Object>();
			result.put("total", total);
			result.put("rows", list);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberWechatMessage:查询会员微信互动消息 <br/>
	 * Date: 2015年8月14日 下午2:48:53 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberWechatMessage")
	@ResponseBody
	public Map<String,Object> showMemberWechatMessage(Long memberId) {
		List<WechatFansMessage> list = null;
		Map<String,Object> result = null;
		Long total = null;
		try {
			total = this.memberService.queryCountWechatMessageByMemberId(memberId);
			list = this.memberService.queryWechatMessageByMemberId(memberId);
			result =  new HashMap<String, Object>();
			result.put("total", total);
			result.put("rows", list);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberSmsInstance:查询会员短信发送记录 <br/>
	 * Date: 2015年8月14日 下午2:49:24 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberSmsInstance")
	@ResponseBody
	public Map<String,Object> showMemberSmsInstance(Long memberId) {
		List<SmsInstance> list = null;
		Map<String,Object> result = null;
		Long total = null;
		try {
			total = this.memberService.queryCountSmsInstanceByMemberId(memberId);
			list = this.memberService.querySmsInstanceByMemberId(memberId);
			result =  new HashMap<String, Object>();
			result.put("total", total);
			result.put("rows", list);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "showMemberAddressDetail")
	@ResponseBody
	public MemberAddress showMemberAddressDetail(Long memberAddressId) {
		MemberAddress result = null;
		String[] arr = null;
		try {
			result = this.memberService.queryMemberAddressDetail(memberAddressId);
			result.setAreaName(result.getProvinceName()+result.getCityName()
					+result.getDistrictName());
			if(result != null && StringUtils.isNotBlank(result.getPersonPhone())){
				arr = result.getPersonPhone().split("_");
				result.setPersonPhoneZone(arr[0]);
				result.setPersonPhoneNum(arr[1]);
			}
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	
	/**
	 * 
	 * saveMember:保存会员基本信息 <br/>
	 * Date: 2015年8月14日 下午3:06:38 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param member
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMember")
	@ResponseBody
	public JsonModel saveMember(Member member) {
		JsonModel result = null;
		String memberCode = null;
		MemberSearch search = null;
		try {
			if(member.getMemberId() == null){
				member.setBirthday(DateUtils.formatString(member.getBirthday()));
				member.setMobile(StringUtil.differentInternationalMobile(member.getMobile(), member.getCountryCode()));
				search = new MemberSearch();
				search.setEqualMobile(member.getMobile());
				Long count = memberService.queryCountSearchAuto(search);
				if(count > 0){
					result = new JsonModel(JsonModel.Status_Error,"手机号已存在！");
					return result;
				}
				memberCode = this.codeService.generateCode(Member.class);
				member.setMemberCode(memberCode);
				this.memberService.insertMember(member);
			}else{
				member.setMobile(StringUtil.differentInternationalMobile(member.getMobile(), member.getCountryCode()));
				this.memberService.updateByPkSelective(member);
			}
			
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			result.setObj(member);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * editMember:编辑会员基本信息 <br/>
	 * Date: 2015年8月17日 上午10:43:23 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param member
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editMember")
	@ResponseBody
	public JsonModel editMember(Long memberId, Member member) {
		JsonModel result = null;
		try {
			member.setMemberId(memberId);
			member.setMobile(StringUtil.differentInternationalMobile(member.getMobile(), member.getCountryCode()));
			this.memberService.updateMember(member);
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (MemberException ex) {
			ex.printStackTrace();
			result = new JsonModel(JsonModel.Status_Error,ex.getMessage());
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * saveMemberExtAccount:新增或修改会员联系信息 <br/>
	 * Date: 2015年8月14日 下午3:07:19 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param memberExp
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMemberExtAccount")
	@ResponseBody
	public JsonModel saveMemberExtAccount(Long memberId,MemberExp memberExp) {
		String sina = null;
		String wechat = null;
		String qq = null;
		String msn = null;
		String online = null;
		String cc = null;
		JsonModel result = null;
		List<ExtAccountBinding> bindingList = null;
		try {
			sina = memberExp.getSina();
			wechat = memberExp.getWechat();
			qq = memberExp.getQq();
			msn = memberExp.getMsn();
			online = memberExp.getOnline();
			cc = memberExp.getCc();
			bindingList = dualWithAccount(memberId, sina, wechat, qq, msn, online, cc);
			memberId = (long) this.memberService.updateMemberExtAccount(bindingList, memberId);
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	/**
	 * 
	 * saveMemberTag:会员添加标签和分组 <br/>
	 * Date: 2015年8月14日 下午3:12:59 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagIds
	 * @param memberId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMemberTagAndGroup")
	@ResponseBody
	public JsonModel saveMemberTagAndGroup(Long memberId,String memberTagIds, String memberGroupIds) {
		JsonModel result = null;
		try {
			
			this.memberService.updateMemberTagAndGroup(memberId,memberTagIds,memberGroupIds);
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * saveMemberExtInfo:新增或修改会员的其他信息 <br/>
	 * Date: 2015年8月14日 下午3:28:34 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param memberExtInfo
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMemberExtInfo")
	@ResponseBody
	public JsonModel saveMemberExtInfo(Long memberId, MemberExtInfo memberExtInfo) {
		MemberExtInfo extInfo = null;
		JsonModel result = null;
		try {
			memberExtInfo.setMemberId(memberId);
			extInfo = this.memberService.queryExtInfoByMemberId(memberId);
			if(extInfo != null){
				this.memberService.updateMemberExtInfo(memberExtInfo);
			}else{
				 this.memberService.insertMemberExtInfo(memberExtInfo);
			}
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * saveMemberAddress:新增或保存会员地址 <br/>
	 * Date: 2015年8月14日 下午3:41:04 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param memberAddress
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMemberAddress")
	@ResponseBody
	public JsonModel saveMemberAddress(Long memberId, MemberAddress memberAddress) {
		JsonModel result = null;
		try {
			memberAddress.setMemberId(memberId);
			memberAddress.setPersonPhone(memberAddress.getPersonPhoneZone()+"_"+memberAddress.getPersonPhoneNum());
			if(memberAddress.getMemberAddressId() != null && !"".equals(memberAddress.getMemberAddressId())){
				this.memberService.updateMemberAddress(memberAddress);
			}else{
				this.memberService.insertMemberAddress(memberAddress);
			}
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * removeMemberAddress:删除会员联系地址 <br/>
	 * Date: 2015年8月14日 下午3:43:06 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberAddressId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "removeMemberAddress")
	@ResponseBody
	public JsonModel removeMemberAddress(Long memberAddressId) {
		JsonModel result = null;
		try {
			this.memberService.deleteMemberAddress(memberAddressId);
			result = new JsonModel(JsonModel.Status_Success,"删除成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"删除失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 首页会员统计
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/queryMemberSummary")
	@ResponseBody
	public Map<String,Object> queryMemberSummary(){
		Map<String,Object> mapResult = new HashMap<String, Object>();
		try {
			MemberSummaryDay memberSummaryOne = memberService.selectBySummaryDate(DateUtils.addDays(-1));
			MemberSummaryDay memberSummaryTwo = memberService.selectBySummaryDate(DateUtils.addDays(-2));
			MemberSummaryDay memberSummaryThree = memberService.selectBySummaryDate(DateUtils.addDays(-7));
			MemberSummaryDay memberSummaryFour = memberService.selectBySummaryDate(DateUtils.addMonths(-1));
			
			CouponSummaryDay couponSummaryOne = couponService.selectBySummaryDate(DateUtils.addDays(-1));
			CouponSummaryDay couponSummaryTwo = couponService.selectBySummaryDate(DateUtils.addDays(-2));
			CouponSummaryDay couponSummaryThree = couponService.selectBySummaryDate(DateUtils.addMonths(-1));
			if(memberSummaryOne != null){
				mapResult.put("memberDay", memberSummaryOne.getDayNewNumber());
				mapResult.put("memberWeek", memberSummaryOne.getWeekNewNumber());
				mapResult.put("memberMonth", memberSummaryOne.getMonthNewNumber());
				mapResult.put("allNewMember", memberSummaryOne.getAllNewNumber());
			}
			if(memberSummaryTwo != null && memberSummaryOne != null){
				Integer dayChange1 = memberSummaryOne.getDayNewNumber() - memberSummaryTwo.getDayNewNumber();
				Integer newChange1 = memberSummaryOne.getAllNewNumber() - memberSummaryTwo.getAllNewNumber();
				mapResult.put("memberDayChange", dayChange1 >= 0L ? "+"+dayChange1 : dayChange1);
				mapResult.put("memberAllChange", newChange1 >= 0L ? "+"+newChange1 : newChange1);
			}
			if(memberSummaryThree != null && memberSummaryOne != null){
				Integer weekChange1 = memberSummaryOne.getWeekNewNumber() - memberSummaryThree.getWeekNewNumber();
				mapResult.put("memberWeekChange", weekChange1 >= 0L ? "+"+weekChange1 : weekChange1);
			}
			if(memberSummaryFour != null && memberSummaryOne != null){
				Integer monthchange1 = memberSummaryOne.getMonthNewNumber() - memberSummaryFour.getMonthNewNumber();
				mapResult.put("memberMonthchange", monthchange1 >= 0L ? "+"+monthchange1 : monthchange1);
			}
			
			if(couponSummaryOne != null && memberSummaryOne != null){
				mapResult.put("couponDay", couponSummaryOne.getDayNewNumber());
				mapResult.put("couponMonth", couponSummaryOne.getMonthNewNumber());
			}
			if(couponSummaryTwo !=null && memberSummaryOne != null){
				Integer dayChange2 = couponSummaryOne.getDayNewNumber() - couponSummaryTwo.getDayNewNumber();
				mapResult.put("couponDayChange", dayChange2 >= 0L ? "+"+dayChange2 : dayChange2);
			}
			if(couponSummaryThree !=null && memberSummaryOne != null){
				Integer monthchange2 = couponSummaryOne.getMonthNewNumber() - couponSummaryThree.getMonthNewNumber();
				mapResult.put("couponMonthchange", monthchange2 >= 0L ? "+"+monthchange2 : monthchange2);
			}
		} catch (Exception e) {
			e.printStackTrace();
			mapResult.put("error", "error");
		}
		return mapResult;
	}
	
	/**
	 * 
	 * dualWithAccount:将会员外部账户信息转换成外部对象集合 <br/>
	 * Date: 2015年8月14日 下午2:45:48 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberId
	 * @param sina
	 * @param wechat
	 * @param qq
	 * @param msn
	 * @param online
	 * @param cc
	 * @return
	 */
	private List<ExtAccountBinding> dualWithAccount(Long memberId, String sina, String wechat,
			String qq, String msn, String online, String cc) {
		ExtAccountBinding binding = null;
		List<ExtAccountBinding> bindingList = null;
		bindingList = new ArrayList<ExtAccountBinding>();
		if(StringUtils.isNotBlank(sina)){
			binding = new ExtAccountBinding();
			binding.setMemberId(memberId);
			binding.setExtAccountTypeId((long)MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_SINA);
			binding.setBindingAccount(sina);
			bindingList.add(binding);
		}
		if(StringUtils.isNotBlank(wechat)){
			binding = new ExtAccountBinding();
			binding.setMemberId(memberId);
			binding.setExtAccountTypeId((long)MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_WECHAT);
			binding.setBindingAccount(wechat);
			bindingList.add(binding);
		}
		if(StringUtils.isNotBlank(qq)){
			binding = new ExtAccountBinding();
			binding.setMemberId(memberId);
			binding.setExtAccountTypeId((long)MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_QQ);
			binding.setBindingAccount(qq);
			bindingList.add(binding);
		}
		if(StringUtils.isNotBlank(msn)){
			binding = new ExtAccountBinding();
			binding.setMemberId(memberId);
			binding.setExtAccountTypeId((long)MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_MSN);
			binding.setBindingAccount(msn);
			bindingList.add(binding);
		}
		if(StringUtils.isNotBlank(online)){
			binding = new ExtAccountBinding();
			binding.setMemberId(memberId);
			binding.setExtAccountTypeId((long)MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_ONLINE);
			binding.setBindingAccount(online);
			bindingList.add(binding);
		}
		if(StringUtils.isNotBlank(cc)){
			binding = new ExtAccountBinding();
			binding.setMemberId(memberId);
			binding.setExtAccountTypeId((long)MyConstants.EXT_ACCOUNT_TYPE.ACCOUNT_TYPE_CC);
			binding.setBindingAccount(cc);
			bindingList.add(binding);
		}
		return bindingList;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/bindBlackCard")
	@ResponseBody
	public JsonModel bindBlackCardManually(Member member, String cardNo) {
		JsonModel json = new JsonModel();
		try {
			memberService.bindBlackCardManually(member, cardNo);
			json.setStatusSuccess();
			json.setMessage("绑定成功");
		} catch (Exception e){
			log.error(e);
			json.setStatusError();
			json.setMessage("绑定失败");
		}
		return json;

	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/showMemberConsume")
	@ResponseBody
	public Map<String,Object> showMemberConsume(Long memberId) {
		Map<String,Object> result = new HashMap<String, Object>();
		try {
			JAXBContext context = JAXBContext.newInstance(Gexf.class);
	        Marshaller marshaller = context.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	        StringWriter writer = new StringWriter();

	        List<Node> nodeList = new ArrayList<Node>();
	        for (int i = 0; i < 8; i++) {
	        	Node node = new Node();
		        node.setId(i);
		        node.setLabel("门店"+i);
		        List<Attvalue> attrs = new ArrayList<Attvalue>();
		        attrs.add(new Attvalue("modularity_class", "0"));
		        node.setAttvalues(attrs);
		        node.setColor("236", "81", "72");
		        node.setSize("4.0");
		        node.setPosition("-418.08344", "446.8853", "0.0");
		        nodeList.add(node);
			}
	        
	        Edge edge = new Edge();
	        edge.setId(0);
	        edge.setSource(1);
	        edge.setTarget(0);
	        
	        List<Edge> edgeList = new ArrayList<Edge>();
	        edgeList.add(edge);
	        
	        Graph graph = new Graph();
	        graph.setNodes(nodeList);
	        graph.setEdges(edgeList);
	        
	        Attribute attribute = new Attribute();
	        attribute.setId("modularity_class");
	        attribute.setTitle("Modularity Class");
	        attribute.setType("integer");
	        
	        Attributes attributes = new Attributes();
	        attributes.setAclass("node");
	        attributes.setMode("static");
	        
	        graph.setAttributes(attributes);
	        
	        Gexf gexf = new Gexf();
	        gexf.setGraph(graph);
	        
	        marshaller.marshal(gexf, writer);
	        String resultxml = writer.toString();
	        
	        Member member = memberService.queryByPk(memberId);
			result.put("xml", resultxml);
			result.put("member", member);
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	public MemberService getMemberService() {
		return memberService;
	}
	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}
	public CodeService getCodeService() {
		return codeService;
	}
	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}
	@Autowired
	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}
	
	public static void main(String[] args) throws Exception {
        JAXBContext context = JAXBContext.newInstance(Graph.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        StringWriter writer = new StringWriter();

        Node node = new Node();
        node.setId(123);
        node.setLabel("Napoleon");
        List<Attvalue> attrs = new ArrayList<Attvalue>();
        attrs.add(new Attvalue("modularity_class", "0"));
        node.setAttvalues(attrs);
        node.setColor("72", "81", "236");
        node.setSize("4.0");
        node.setPosition("-418.08344", "446.8853", "0.0");
        
        List<Node> nodeList = new ArrayList<Node>();
        nodeList.add(node);
        
        Edge edge = new Edge();
        edge.setId(0);
        edge.setSource(1);
        edge.setTarget(0);
        
        List<Edge> edgeList = new ArrayList<Edge>();
        edgeList.add(edge);
        
        Graph graph = new Graph();
        graph.setNodes(nodeList);
        graph.setEdges(edgeList);
        
        marshaller.marshal(graph, writer);
        String result = writer.toString();
        System.out.println(result);
    }
	
}
