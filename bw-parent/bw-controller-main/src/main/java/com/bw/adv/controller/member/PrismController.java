package com.bw.adv.controller.member;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberPrism;
import com.bw.adv.service.member.service.MemberPrismService;

/**
 * 
 * @author cuicd
 *
 */
@Controller
@RequestMapping("/PrismController")
public class PrismController {
	
	private MemberPrismService memberPrismService;
	
	/**
	 * 查找棱镜
	 * @param page
	 * @param rows
	 * @param request
	 * @return
	 * @author cuicd
	 */
	@RequestMapping(value = "/showMemberPrismList", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showMemberPrismList(int page,int rows,HttpServletRequest request){
		Map<String, Object> map = null;
		Page<MemberPrism> templatePage = null;
		try {
			map = new HashMap<String, Object>();
			templatePage = new Page<MemberPrism>();
			templatePage.setPageNo(page);
			templatePage.setPageSize(rows);
			this.memberPrismService.queryAllMemberPrism(templatePage);
			map.put("total", templatePage.getTotalRecord());
			map.put("rows", templatePage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	@Autowired
	public void setMemberPrismService(MemberPrismService memberPrismService) {
		this.memberPrismService = memberPrismService;
	}

}
