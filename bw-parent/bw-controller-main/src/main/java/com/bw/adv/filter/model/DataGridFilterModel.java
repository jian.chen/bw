package com.bw.adv.filter.model;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.bw.adv.filter.utils.DataFilterUtils;
import com.bw.adv.module.common.model.DataGridModel;

/**
 * Created by jeoy.zhou on 1/5/16.
 */
public class DataGridFilterModel<T> extends DataGridModel<T> {

    public DataGridFilterModel() {
        super();
    }

    public DataGridFilterModel(String message) {
        super(message, false);
    }

    public DataGridFilterModel(String message, boolean isSuccess) {
        super(message, isSuccess);
    }

    public DataGridFilterModel(List<T> rows, Long total) {
        super(rows, total);
    }

    public DataGridFilterModel(List<T> rows, Long total, int flag) {
        super(rows, total, flag);
    }

    @Override
    public List<T> getRows() {
        if(CollectionUtils.isEmpty(super.getRows())) return super.getRows();
        return DataFilterUtils.dataFilter(super.getRows());
    }
}
