package com.bw.adv.filter.utils;

/**
 * Created by jeoy.zhou on 1/5/16.
 */
public class ShiroConstants {

    public static final String PATH_PREFIX = "/";
    public static final String AUTH_SESSION_KEY = "_auth";
}
