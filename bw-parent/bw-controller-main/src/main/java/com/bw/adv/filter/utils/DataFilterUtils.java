package com.bw.adv.filter.utils;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.filter.domain.DataFiledManager;
import com.bw.adv.filter.domain.JsonPropertyFilter;
import com.bw.adv.filter.domain.ScrmAuthorizationInfo;

/**
 * Created by jeoy.zhou on 1/5/16.
 */
public final class DataFilterUtils {

    private final static Logger LOGGER = Logger.getLogger(DataFilterUtils.class);


    public static <T> List<T> dataFilter(List<T> originDataList){
        if(CollectionUtils.isEmpty(originDataList)) return originDataList;
        T originData = originDataList.get(0);
        JsonPropertyFilter jsonPropertyFilter = getFilter(originData);
        return jsonPropertyFilter == null ? originDataList :
                JSONObject.parseArray(JSONObject.toJSONString(originDataList, jsonPropertyFilter), (Class<T>) originData.getClass());
    }

    public static <T>T dataFilter(T originData) {
        if(originData == null) return originData;
        JsonPropertyFilter jsonPropertyFilter = getFilter(originData);
        return jsonPropertyFilter == null ? originData :
                JSONObject.parseObject(JSONObject.toJSONString(originData, jsonPropertyFilter), (Class<T>) originData.getClass());
    }

    private static <T> JsonPropertyFilter getFilter(T originData) {
        Subject subject = SecurityUtils.getSubject();
        if(subject == null && LOGGER.isDebugEnabled()) {
            LOGGER.debug("DataFilterUtils: getFilter can not get current user by SecurityUtils.getSubject()");
            return null;
        }
        Session session = subject.getSession();
        if(session == null && LOGGER.isDebugEnabled()) {
            LOGGER.debug("DataFilterUtils: getFilter can not get current user by SecurityUtils.getSubject().getSession()");
            return null;
        }
        ScrmAuthorizationInfo authInfo = (ScrmAuthorizationInfo) session.getAttribute(ShiroConstants.AUTH_SESSION_KEY);
        Map<String, JsonPropertyFilter> filterMap = authInfo.getFilterMap();
        if(MapUtils.isEmpty(filterMap)) return null;
        String key = null;
        if(DataFiledManager.useDefault) {
            key = DataFiledManager.DEFAULT_FILTER;
        } else {
            Class<T> clazz = (Class<T>) originData.getClass();
            key = clazz.getName();
        }
        JsonPropertyFilter jsonPropertyFilter = filterMap.get(key);
        if(jsonPropertyFilter == null && LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("DataFilterUtils: getFilter no key[%s] filter", key));
            return null;
        }
        return jsonPropertyFilter;
    }

}
