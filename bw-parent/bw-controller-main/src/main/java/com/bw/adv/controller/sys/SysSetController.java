/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:SysSetController.java
 * Package Name:com.sage.scrm.controller.sys
 * Date:2016年4月6日下午5:20:37
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.sys;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.module.base.model.CodeSeqType;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.service.sys.service.CodeSeqTypeService;

/**
 * ClassName:SysSetController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年4月6日 下午5:20:37 <br/>
 * scrmVersion 1.0
 * @author   chengdi.cui
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/sysSetController")
public class SysSetController {

	private CodeSeqTypeService codeSeqTypeService;
	
	/**
	 * sysCodeSave:保存系统编号设置. <br/>
	 * @author chengdi.cui
	 * @param couponCodeLength
	 * @param cardPrefix
	 * @param cardCodeLength
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "sysCodeSave")
	@ResponseBody
	public JsonModel sysCodeSave(int couponCodeLength,String cardPrefix,int cardCodeLength){
		JsonModel result = null;
		CodeSeqType couponSeqType = new CodeSeqType();
		CodeSeqType cardSeqType = new CodeSeqType();
		try {
			result = new JsonModel();
			//memberCard
			cardSeqType.setCodeSeqTypeId(801L);
			cardSeqType.setCodeSeqTypePrefix(cardPrefix);
			cardSeqType.setCodeLength(cardCodeLength-2);
			codeSeqTypeService.updateByPkSelective(cardSeqType);
			
			//couponInstance
			couponSeqType.setCodeSeqTypeId(301L);
			couponSeqType.setCodeLength(couponCodeLength-2);
			codeSeqTypeService.updateByPkSelective(couponSeqType);
			result.setStatusSuccess();
		} catch (Exception e) {
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * showSysCode:(查询系统编号). <br/>
	 * @author chengdi.cui
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showSysCode")
	@ResponseBody
	public JsonModel showSysCode(){
		JsonModel result = null;
		CodeSeqType couponSeqType = null;
		CodeSeqType cardSeqType = null;
		Map<String,Object> map = null;
		try {
			result = new JsonModel();
			map = new HashMap<String, Object>();
			couponSeqType = codeSeqTypeService.queryByTypeCode("couponInstance");
			couponSeqType.setCodeLength(couponSeqType.getCodeLength()+2);
			cardSeqType = codeSeqTypeService.queryByTypeCode("memberCard");
			cardSeqType.setCodeLength(cardSeqType.getCodeLength()+2);
			map.put("couponSeqType", couponSeqType);
			map.put("cardSeqType", cardSeqType);
			result.setObj(map);
			result.setStatusSuccess();
		} catch (Exception e) {
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}

	@Autowired
	public void setCodeSeqTypeService(CodeSeqTypeService codeSeqTypeService) {
		this.codeSeqTypeService = codeSeqTypeService;
	}
	
}

