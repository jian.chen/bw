/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:CouponCategoryCcontroller.java
 * Package Name:com.sage.scrm.controller.points
 * Date:2015年12月28日上午11:41:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.points;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.coupon.model.CouponCategory;
import com.bw.adv.module.coupon.model.CouponCategoryItem;
import com.bw.adv.module.coupon.model.exp.CouponCategoryItemExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;
import com.bw.adv.service.coupon.service.CouponCategoryService;

/**
 * ClassName:CouponCategoryCcontroller <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月28日 上午11:41:22 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/couponCategoryController")
@SuppressWarnings("unused")
public class CouponCategoryController {
	
	private CouponCategoryService couponCategoryService;

	/**
	 * 
	 * showList:(查询未删除优惠券类别明细List). <br/>
	 * Date: 2015年12月28日 上午11:44:02 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showList")
	@ResponseBody
	public Map<String, Object> showList(int page,int rows){
		Map<String, Object> map = null;
		List<CouponCategoryItemExp> result = null;
		Page<CouponCategoryItemExp> pageObj = null;
		
		try {
			
			map = new HashMap<String, Object>();
			pageObj = new Page<CouponCategoryItemExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			result = couponCategoryService.queryList(pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * 
	 * showDetail:(根据id查询优惠券类别明细详细信息). <br/>
	 * Date: 2015年12月28日 下午1:08:21 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItem
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value = "showDetail")
	@ResponseBody
	public CouponCategoryItemExp showDetail(Long couponCategoryItem){
		CouponCategoryItemExp result = null;
		
		try {
			
			result = new CouponCategoryItemExp();
			
			result = couponCategoryService.queryDetail(couponCategoryItem);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * saveCouponCategoryItem:(新增/修改 优惠券类别明细). <br/>
	 * Date: 2015年12月28日 下午1:14:40 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItem
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveCouponCategoryItem")
	@ResponseBody
	public String saveCouponCategoryItem(CouponCategoryItem couponCategoryItem,HttpServletRequest request){
		String result = null;
		
		try {
			//新建
			if(couponCategoryItem.getCouponCategoryItem()== null){
				
				if(couponCategoryItem.getStatusId() == null){
					couponCategoryItem.setStatusId(StatusConstant.COUPON_CATEGORY_ITEM_DISENABLE.getId());
				}
				couponCategoryService.saveCouCateItem(couponCategoryItem);
				
			}else{//修改
				
				couponCategoryService.updateCouCateItemByPk(couponCategoryItem);
			}
			
			result = "{\"code\":\"1\",\"message\":\"保存成功!\"}";
			
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"保存失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * removeList:(批量删除优惠券类别明细). <br/>
	 * Date: 2015年12月28日 下午1:25:14 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItems
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value = "removeList")
	@ResponseBody
	public String removeList(String couponCategoryItems){
		String result = null;
		
		try {
			
			couponCategoryService.deletedCouCateItems(couponCategoryItems);
			
			result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"删除失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * updateStatus:(上下架). <br/>
	 * Date: 2015年12月28日 下午1:29:31 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItem
	 * @param statusId
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "updateStatus")
	@ResponseBody
	public String updateStatus(Long couponCategoryItem,Long statusId,HttpServletRequest request){
		String result = null;
		CouponCategoryItem temp = null;
		
		try {
			
			temp = new CouponCategoryItemExp();
			
			if(StatusConstant.COUPON_CATEGORY_ITEM_ENABLE.getId().equals(statusId)){
				statusId = StatusConstant.COUPON_CATEGORY_ITEM_DISENABLE.getId();
			}else{
				statusId = StatusConstant.COUPON_CATEGORY_ITEM_ENABLE.getId();
			}
			
			temp.setCouponCategoryItem(couponCategoryItem);
			temp.setStatusId(statusId);
			
			couponCategoryService.updateCouCateItemByPk(temp);
			
			result = "{\"code\":\"1\",\"message\":\"操作成功!\"}";
			
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"操作失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	/**
	 * 
	 * showGoodsShowCategoryList:(优惠券类别下拉框). <br/>
	 * Date: 2015年12月28日 下午12:04:54 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showCouponCategoryList")
	@ResponseBody
	public List<CouponCategory> showCouponCategoryList(){
		List<CouponCategory> result = null;
		
		try {
			
			result = couponCategoryService.queryCouponCategoryList(null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * showCouponCategoryList:(查询优惠券类别List). <br/>
	 * Date: 2015年12月28日 下午1:38:54 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showCouponCategoryList")
	@ResponseBody
	public Map<String, Object> showCouponCategoryList(int page,int rows){
		Map<String, Object> map = null;
		List<CouponCategory> result = null;
		Page<CouponCategory> pageObj = null;
		
		try {
			
			map = new HashMap<String, Object>();
			pageObj = new Page<CouponCategory>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			result = couponCategoryService.queryCouponCategoryList(pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * 
	 * showCouponCategoryDetail:(查询优惠券类别详情). <br/>
	 * Date: 2015年12月28日 下午1:40:25 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value = "showCouponCategoryDetail")
	@ResponseBody
	public CouponCategory showCouponCategoryDetail(Long couponCategoryId){
		CouponCategory result = null;
		
		try {
			
			result = new CouponCategory();
			
			result = couponCategoryService.queryByPk(couponCategoryId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * saveCouponCategory:(新增/保存优惠券类别). <br/>
	 * Date: 2015年12月28日 下午1:42:42 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategory
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveCouponCategory")
	@ResponseBody
	public String saveCouponCategory(CouponCategory couponCategory,HttpServletRequest request){
		String result = null;
		
		try {
			//新建
			if(couponCategory.getCouponCategoryId() == null){
				
				couponCategory.setCreateBy(SysUserUtil.getSessionUser(request).getUserId());
				couponCategory.setCreateTime(DateUtils.getCurrentTimeOfDb());

				if(couponCategory.getStatusId() == null){
					couponCategory.setStatusId(StatusConstant.COUPON_CATEGORY_DISENABLE.getId());
				}
				couponCategoryService.saveSelective(couponCategory);
				
			}else{//修改
				
				couponCategory.setUpdateBy(SysUserUtil.getSessionUser(request).getUserId());
				couponCategory.setUpdateTime(DateUtils.getCurrentTimeOfDb());
				couponCategoryService.updateByPkSelective(couponCategory);
			}
			
			result = "{\"code\":\"1\",\"message\":\"保存成功!\"}";
			
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"保存失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * removeCouponCategoryList:(批量删除优惠券类别). <br/>
	 * Date: 2015年12月28日 下午1:51:37 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value = "removeCouponCategoryList")
	@ResponseBody
	public String removeCouponCategoryList(String couponCategoryIds){
		String result = null;
		
		try {
			
			couponCategoryService.deletedCouponCategorys(couponCategoryIds);
			
			result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"删除失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * updateCouponCategoryStatus:(优惠券类别上下架). <br/>
	 * Date: 2016年1月14日 下午8:06:16 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryId
	 * @param statusId
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "updateCouponCategoryStatus")
	@ResponseBody
	public String updateCouponCategoryStatus(Long couponCategoryId,Long statusId,HttpServletRequest request){
		String result = null;
		CouponCategory temp = null;
		
		try {
			
			temp = new CouponCategory();
			
			if(StatusConstant.COUPON_CATEGORY_ENABLE.getId().equals(statusId)){
				statusId = StatusConstant.COUPON_CATEGORY_DISENABLE.getId();
			}else{
				statusId = StatusConstant.COUPON_CATEGORY_ENABLE.getId();
			}
			
			temp.setCouponCategoryId(couponCategoryId);
			temp.setStatusId(statusId);
			
			couponCategoryService.updateByPkSelective(temp);
			
			result = "{\"code\":\"1\",\"message\":\"操作成功!\"}";
			
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"操作失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	/**
	 * 
	 * uploadImage:(上传图片). <br/>
	 * Date: 2016年1月6日 下午7:38:58 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
	@ResponseBody
	public String uploadImage(HttpServletRequest request,HttpServletResponse response){
		MultipartHttpServletRequest multipartRequest = null;
		String logoPathDir = null;
		String logoRealPathDir = null;
		File logoSaveFile = null;
		MultipartFile multipartFile = null;
		String suffix = null;
		String logImageName = null;
		String fileName = null;
		String filePath = null;
		File file = null;
		
		multipartRequest = (MultipartHttpServletRequest) request;
		/** 构建文件保存的目录* */
		logoPathDir = "/business/shops/upload/couponCategory/"+ DateUtils.getCurrentTimeOfDb();
		System.out.println(request.getRequestURL());
		System.out.println(request.getRequestURI());
		System.out.println(request.getSession().getServletContext().getContextPath());
		/** 得到文件保存目录的真实路径* */
		logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
		/** 根据真实路径创建目录* */
		logoSaveFile = new File(logoRealPathDir);
		if (!logoSaveFile.exists()){
			logoSaveFile.mkdirs();
		}
		/** 页面控件的文件流* */
		multipartFile = multipartRequest.getFile("couponCateimage");
		if (FileUtil.isAllowUpForPicture(multipartFile.getOriginalFilename())) {
			/** 获取文件的后缀* */
			suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
			/** 使用UUID生成文件名称* */
			logImageName = UUID.randomUUID().toString() + suffix;// 构建文件名称
			/** 拼成完整的文件保存路径加文件* */
			fileName = logoRealPathDir + File.separator + logImageName;
			file = new File(fileName);
			filePath = logoPathDir+ "/" + logImageName;
			try {
				multipartFile.transferTo(file);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			/** 打印出上传到服务器的文件的绝对路径* */
			System.out.println("****************"+fileName+"**************");
			return filePath;
		}else{
			return "不被允许上传的文件格式!";
		}
	}
	

	@Autowired
	public void setCouponCategoryService(CouponCategoryService couponCategoryService) {
		this.couponCategoryService = couponCategoryService;
	}
	
	

}

