package com.bw.adv.controller.compalin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.module.member.complain.model.Complain;
import com.bw.adv.module.member.complain.model.ComplainType;
import com.bw.adv.module.member.complain.model.exp.ComplainExp;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;
import com.bw.adv.service.complain.service.ComplainService;
import com.bw.adv.service.complain.service.ComplainTypeService;



/**
 * ClassName: CouponController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月17日 上午11:31:09 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Controller
@RequestMapping("/complainController")
@SuppressWarnings("unused")
public class ComplainController {
	
	private ComplainService complainService;
	
	private ComplainTypeService complainTypeService;
	
	/**
	 * query:查询建议列表 <br/>
	 * Date: 2015年8月17日 上午11:31:21 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param complainExp
	 * @return
	 */
	@RequestMapping(value = "/showComplainList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridModel<ComplainExp> showComplainList(ComplainExp complainExp,int page,int rows){
		DataGridModel<ComplainExp> result = null;
		Example example = null;
		Criteria criteria = null;
		Page<ComplainExp> pageObj = null;
		List<ComplainExp> complainList = null;
		Long count = null;
		String greatTime = null;
		String lessTime = null;
		try {
			example = new Example();
			criteria = example.createCriteria();
			pageObj = new Page<ComplainExp>();  
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			if(StringUtils.isNotBlank(complainExp.getMemberCode())){
				criteria.andEqualTo("mem.MEMBER_CODE",complainExp.getMemberCode());
			}
			if(StringUtils.isNotBlank(complainExp.getPersonName())){
				String complainName = new String(complainExp.getPersonName().getBytes("ISO-8859-1"),"UTF-8");
				criteria.andLike("co.PERSON_NAME", "%"+complainName+"%");
			}
			if(StringUtils.isNotBlank(complainExp.getIsMember())){
				criteria.andEqualTo("co.IS_MEMBER",complainExp.getIsMember());
			}
			if(StringUtils.isNotBlank(complainExp.getMobile())){
				criteria.andEqualTo("co.MOBILE",complainExp.getMobile());
			}
			if(StringUtils.isNotBlank(complainExp.getGreaterEqualDate())){
				greatTime = DateUtils.dateStrToNewFormat(complainExp.getGreaterEqualDate(), "yyyy-MM-dd", "yyyyMMdd")+"000000";
				criteria.andGreaterThanOrEqualTo("co.CREATE_TIME",greatTime);
			}
			if(StringUtils.isNotBlank(complainExp.getLessEqualDate())){
				lessTime = DateUtils.dateStrToNewFormat(complainExp.getLessEqualDate(), "yyyy-MM-dd", "yyyyMMdd")+"235959";
				criteria.andLessThanOrEqualTo("co.CREATE_TIME",lessTime);
			}
			if(complainExp.getComplainTypeId()!=null){
				criteria.andEqualTo("co.COMPLAIN_TYPE_ID",complainExp.getComplainTypeId());
			}
			if(complainExp.getStatusId()!=null){
				criteria.andEqualTo("co.STATUS_ID",complainExp.getStatusId());
			}
			complainList = complainService.queryByExa(example, pageObj);
			count = pageObj.getTotalRecord();
			result = new DataGridModel<ComplainExp>(complainList, count);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "downTemp")
	public void downTemp(HttpServletResponse response,HttpServletRequest request) {
		String filePath = null;  
		String fileName = null;
		try {
			filePath = request.getParameter("filePath") ;  
			fileName = FileUtil.getPathFileName(filePath);
			FileUtils.downloadUrl(filePath, fileName, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping(value = "/showComplainType", method = RequestMethod.GET)
	@ResponseBody
	public List<ComplainType> showComplainType(){
		List<ComplainType> result = null;
		try {
			result = complainTypeService.queryAllComplainType();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@RequestMapping(value = "/queryCcomplainDetail", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> queryCcomplainDetail(Long complainId){
		ComplainExp complainExp = null;
		Map<String, Object> result = null;
		try {
			result = new HashMap<String, Object>();
			complainExp = complainService.queryDetailByPk(complainId);
			result.put("complain", complainExp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@RequestMapping(value = "/readComplain", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> readComplain(Long complainId){
		Complain complain = null;
		Map<String, Object> result = null;
		try {
			result = new HashMap<String, Object>();
			complain = complainService.queryByPk(complainId);
			complain.setStatusId(StatusConstant.COMPLAIN_READED.getId());
			complainService.updateByPkSelective(complain);
			result.put("code", 200);
		} catch (Exception e) {
			result.put("code", 100);
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "/closeComplain", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> closeComplain(HttpServletRequest request,Long complainId,String remark){
		Complain complain = null;
		Map<String, Object> result = null;
		try {
			result = new HashMap<String, Object>();
			complain = complainService.queryByPk(complainId);
			complain.setStatusId(StatusConstant.COMPLAIN_CLOSED.getId());
			complain.setRemark(remark);
			SysUser sysUser = (SysUser)request.getSession().getAttribute(SysUserUtil.SESSION_USER_KEY);
			complain.setOperatorId(sysUser.getUserId());
			complain.setOperatorName(sysUser.getCn());
			complain.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			complainService.updateByPkSelective(complain);
			complainService.sendWcMsgByCloseComplain(complain);
			result.put("code", 200);
		} catch (Exception e) {
			result.put("code", 100);
			e.printStackTrace();
		}
		return result;
	}
	
	@Autowired
	public void setComplainService(ComplainService complainService) {
		this.complainService = complainService;
	}

	@Autowired
	public void setComplainTypeService(ComplainTypeService complainTypeService) {
		this.complainTypeService = complainTypeService;
	}
	
}
