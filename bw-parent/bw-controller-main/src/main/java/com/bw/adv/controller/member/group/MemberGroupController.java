package com.bw.adv.controller.member.group;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.filter.model.DataGridFilterModel;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.module.common.utils.MyConstants;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.search.MemberGroupSearch;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelImport;
import com.bw.adv.service.member.service.MemberGroupService;
import com.bw.adv.service.member.service.MemberService;

@Controller
@RequestMapping("/memberGroupController")
public class MemberGroupController {
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	private MemberGroupService memberGroupService;
	
	private MemberService memberService;
	
	private CodeService codeService;
	
	/**
	 * 
	 * showMemberGroupList:条件查询会员分组列表 <br/>
	 * Date: 2015年8月17日 上午10:57:51 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	//@RequiresPermissions(value ="memberGroup:showMemberGroupList")
	@RequestMapping(method = RequestMethod.POST, value = "showMemberGroupList")
	@ResponseBody
	public Map<String,Object> showMemberGroupList(MemberGroupSearch search) {
		Map<String,Object> result = null;
		List<MemberGroup> list = null;
		Long total = null;
		try {
			result =  new HashMap<String, Object>();
			search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
			total = this.memberGroupService.queryCountSearchAuto(search);
			list = this.memberGroupService.queryListSearchAuto(search);
			result.put("total", total);
			result.put("rows", list);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "showMemberGroupListSelect")
	@ResponseBody
	public List<MemberGroup> showMemberGroupListSelect() {
		List<MemberGroup> result = null;
		MemberGroupSearch search = null;
		try {
			search = new MemberGroupSearch();
			search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
			result = this.memberGroupService.queryListSearchAuto(search);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberGroupDetail:查看会员分组详情 <br/>
	 * Date: 2015年8月17日 上午10:57:14 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberGroupDetail")
	@ResponseBody
	public MemberGroup showMemberGroupDetail(Long memberGroupId) {
		MemberGroup result = null;
		try {
			result = this.memberGroupService.queryEntityPkAuto(memberGroupId);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	

	@RequestMapping(method = RequestMethod.POST, value = "showCountByName")
	@ResponseBody
	public Long showCountByName(String memberGroupName) {
		Long result = null;
		MemberGroupSearch search = null;
		try {
			search = new MemberGroupSearch();
			search.setEqualMemberGroupName(memberGroupName);
			result = this.memberGroupService.queryCountSearchAuto(search);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberListOfGroup:条件查询分组下会员列表 <br/>
	 * Date: 2015年8月17日 上午11:16:44 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @param search
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showMemberListOfGroup")
	@ResponseBody
	public DataGridModel<MemberExp> showMemberListOfGroup(Long memberGroupId,MemberSearch search) {
		List<MemberExp> list = null;
		String[] memberIds = null;
		MemberGroup memberGroup = null;
		Long total = null;
		DataGridFilterModel<MemberExp> result = null;
		try {
			memberIds = new String[1];
			memberIds[0] = memberGroupId+"";
			memberGroup = this.memberGroupService.queryEntityPkAuto(memberGroupId);
			if(MyConstants.IS_ALL.YES==memberGroup.getIsAll()){
				total =  this.memberService.queryCountSearchAuto(search);
				list = this.memberService.queryListSearchAuto(search);
			}else{
				search.setInMemberGroupIds(memberIds);
				total =  this.memberService.queryCountSearchAuto(search);
				list = this.memberGroupService.queryMemberListOfGroup(search);
			}
			result = new DataGridFilterModel<MemberExp>(list, total);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * saveMemberGroup:新增会员分组 <br/>
	 * Date: 2015年8月17日 上午10:56:31 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroup
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMemberGroup")
	@ResponseBody
	public JsonModel saveMemberGroup(MemberGroup memberGroup) {
		JsonModel result = null;
		String memberGroupCode = null;
		MemberGroupSearch search = null;
		Long count = 0L;
		try {
			if(!"".equals(memberGroup.getMemberGroupName())){
				search = new MemberGroupSearch();
				search.setEqualMemberGroupName(memberGroup.getMemberGroupName());
				search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
				count = this.memberGroupService.queryCountSearchAuto(search);
				if(count>0){
					result = new JsonModel(JsonModel.Status_Error,"名称已被使用");
					return result;
				}
			}
			
			memberGroupCode = this.codeService.generateCode(MemberGroup.class);
			memberGroup.setMemberGroupCode(memberGroupCode);
			memberGroup.setCreateTime(DateUtils.getCurrentTimeOfDb());
			memberGroup.setStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_CREATED.getId());
			memberGroup.setIsAll(MyConstants.IS_ALL.NO);
			this.memberGroupService.insertMemberGroup(memberGroup);
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * editMemberGroup:编辑分组 <br/>
	 * Date: 2015年8月17日 上午10:55:46 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroup
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editMemberGroup")
	@ResponseBody
	public JsonModel editMemberGroup(Long memberGroupId,MemberGroup memberGroup) {
		JsonModel result = null;
		MemberGroupSearch search = null;
		List<MemberGroup> list = null;
		try {
			if(!"".equals(memberGroup.getMemberGroupName())){
				search = new MemberGroupSearch();
				search.setEqualMemberGroupName(memberGroup.getMemberGroupName());
				search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
				list = this.memberGroupService.queryListSearchAuto(search);
				if(list != null && list.size()>0){
					if(memberGroupId != list.get(0).getMemberGroupId()){
						result = new JsonModel(JsonModel.Status_Error,"名称已被使用");
						return result;
					}
				}
			}
			
			memberGroup.setMemberGroupId(memberGroupId);
			 this.memberGroupService.updateMemberGroup(memberGroup);
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "saveAllMemberToGroup")
	@ResponseBody
	public JsonModel saveAllMemberToGroup(Long memberGroupId) {
		JsonModel result = null;
		MemberGroup memberGroup = null;
		try {
			memberGroup = new MemberGroup();
			memberGroup.setIsAll(MyConstants.IS_ALL.YES);
			memberGroup.setMemberGroupId(memberGroupId);
			this.memberGroupService.saveAllMemberToGroup(memberGroup);
			
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * removeMemberGroup:删除分组，更改状态 <br/>
	 * Date: 2015年8月17日 上午10:54:58 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "removeMemberGroup")
	@ResponseBody
	public JsonModel removeMemberGroup(Long memberGroupId) {
		JsonModel result = null;
		MemberGroup memberGroup = null;
		try {
			memberGroup =  this.memberGroupService.queryByPk(memberGroupId);
			memberGroup.setStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
			this.memberGroupService.updateMemberGroup(memberGroup);
			result = new JsonModel(JsonModel.Status_Success,"删除成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"删除失败，请稍后重试！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "removeMemberGroupItem")
	@ResponseBody
	public JsonModel removeMemberGroupItem(Long memberGroupId,Long memberId) {
		JsonModel result = null;
		try {
			this.memberGroupService.deleteMemberGroupItem(memberGroupId,memberId);
			result = new JsonModel(JsonModel.Status_Success,"删除成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"删除失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * saveMemberGroupItem:会员分组批量添加会员 <br/>
	 * Date: 2015年8月17日 上午10:54:26 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @param memberIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMemberGroupItem")
	@ResponseBody
	public JsonModel saveMemberGroupItem(Long memberGroupId,String memberIds) {
		JsonModel result = null;
		Long count = null;
		//String[] memberIdArr = null;
		try {
			//memberIdArr = memberIds.split(",");
			count = (long) this.memberGroupService.insertBatchMemberGroupItem(memberGroupId, memberIds);
			result = new JsonModel(JsonModel.Status_Success,"成功添加"+count+"名会员");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"添加失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * excuteMemberGroupSql:手动执行分组sql <br/>
	 * Date: 2015年8月17日 上午10:53:55 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberGroupId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "excuteMemberGroupSql")
	@ResponseBody
	public JsonModel excuteMemberGroupSql(Long memberGroupId) {
		JsonModel result = null;
		Long count = null;
		String sql = null;
		MemberGroup group = null;
		try {
			group = this.memberGroupService.queryByPk(memberGroupId);
			if(group != null){
				sql = group.getConditionSql();
			}
			try{
				this.memberService.checkMemberSql(sql);
			}catch(Exception ex){
				ex.printStackTrace();
				result = new JsonModel(JsonModel.Status_Error,"分组sql验证失败！请修改！");
				return result;
			}
			count = (long) this.memberGroupService.excuteMemberGroupBySql(memberGroupId);
			result = new JsonModel(JsonModel.Status_Success,"成功添加"+count+"名会员");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"执行失败，请稍后重试！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "downTemp")
	public void downTemp(HttpServletResponse response,HttpServletRequest request) {
		String filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/memberGroupTmp.xls" ;  
		try {
			FileUtils.download("会员列表导入模板.xls", filePath, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * importData:导入会员 <br/>
	 * Date: 2015年9月2日 上午11:55:49 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "importData")
	@ResponseBody
	public DataGridModel<MemberExp> importData(MultipartHttpServletRequest request) {
		DataGridModel<MemberExp> result = null;
		List<MemberExp> list = null;
		MultipartFile file = null;
		String filePath = null;
		 File fileExcel = null;
		 ExcelImport test = null;
		 List<Member> data = null;
		 String mobile = null;
		 MemberSearch search = null;
		 MemberExp exp = null;
		try {
			file = request.getFile("fileInput");
			  // 文件保存路径  
            filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/"  
                     + file.getOriginalFilename();  
            fileExcel = new File(filePath);
            file.transferTo(fileExcel);  
			test = new ExcelImport();
			data = (ArrayList) test.importExcel(fileExcel, Member.class);
			list = new ArrayList<MemberExp>();
			if(data != null && data.size()>0){
				for(Member member:data){
					mobile = member.getMobile();
					search = new MemberSearch();
					search.setEqualMobile(mobile);
					exp = this.memberService.queryEntitySearchAuto(search);
					if(exp == null){
						exp = new MemberExp();
						exp.setMobile(mobile);
					}
					list.add(exp);
				}
			}
			fileExcel.delete();
			result = new DataGridModel<MemberExp>(list, Long.valueOf(list==null?0:list.size()));
			return result;
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new DataGridModel<MemberExp>("导入失败请检查数据！");
		return result;
	}

	public MemberGroupService getMemberGroupService() {
		return memberGroupService;
	}

	@Autowired
	public void setMemberGroupService(MemberGroupService memberGroupService) {
		this.memberGroupService = memberGroupService;
	}
	
	public CodeService getCodeService() {
		return codeService;
	}
	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}

	public MemberService getMemberService() {
		return memberService;
	}
	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}
	
}
