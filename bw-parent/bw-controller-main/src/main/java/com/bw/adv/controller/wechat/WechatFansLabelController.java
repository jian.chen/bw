/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:WechatFansLabelController.java
 * Package Name:com.sage.scrm.controller.wechat
 * Date:2015年9月4日下午4:42:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.wechat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansLabel;
import com.bw.adv.module.wechat.model.exp.WechatFanLabelExp;
import com.bw.adv.service.wechat.service.WeChatFansService;
import com.bw.adv.service.wechat.service.WechatFanLabelService;
import com.bw.adv.service.wechat.service.WechatFansLabelService;

/**
 * ClassName:WechatFansLabelController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月4日 下午4:42:18 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/wechatFansLabelController")
public class WechatFansLabelController {
	
	protected final Logger logger = Logger.getLogger(WechatFansLabelController.class);
	
	private WechatFansLabelService wechatFansLabelService;
	private WechatFanLabelService wechatFanLabelService;
	private WeChatFansService weChatFansService;
	
	/**
	 * showWechatFansLabel:(标签组)<br/>
	 * Date: 2015年9月4日 下午5:23:43 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "showWechatFansLabel",method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showWechatFansLabel(HttpServletRequest request) {
		Map<String,Object> result = null;
		List<WechatFansLabel> wechatFansLabelList =null;
		try {
			result =new HashMap<String,Object>();
			String labelName =request.getParameter("likeMemberTagName");
			wechatFansLabelList = wechatFansLabelService.queryWechatFansLabelByLabelName(labelName);
			if(wechatFansLabelList.size()>0){
				result.put("rows", wechatFansLabelList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * showWechatFansLabel:(根据主键id 查询对应的标签列表). <br/>
	 * Date: 2015年9月4日 下午6:27:52 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "showWechatFansLabelListByFansId",method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showWechatFansLabelListByFansId(HttpServletRequest request) {
		String wechatFansId =null;
		Map<String,Object> result = null;
		List<WechatFanLabelExp> wechatFansLabelExpList =null;
		try {
			result =new HashMap<String,Object>();
			wechatFansId =request.getParameter("wechatFansId");
			wechatFansLabelExpList = wechatFanLabelService.queryWechatFanLabelList(Long.valueOf(wechatFansId));
			if(wechatFansLabelExpList.size()>0){
				result.put("rows", wechatFansLabelExpList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showWechatFansLabelListByFansId:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年9月4日 下午8:51:17 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveFansLabel",method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> saveFansLabel(HttpServletRequest request) {
		String wechatFansId =null;
		String addTagIdArr =null;
		Map<String,Object> result = null;
		List<String> LabelIds =null;
		String status =null;
		try {
			LabelIds =new ArrayList<String>();
			result =new HashMap<String,Object>();
			wechatFansId = request.getParameter("wechatFansIdLabel");
			addTagIdArr = request.getParameter("addTagIdArr");
			if(!StringUtils.isEmpty(addTagIdArr)){
				LabelIds = Arrays.asList(addTagIdArr.split(","));
			}
			wechatFanLabelService.saveFanLabelsRelative(wechatFansId,LabelIds);
			status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			e.printStackTrace();
		}finally{
			result.put("status", status);
		}
		return result;
	}
	
	/**
	 * 批量添加粉丝标签
	 * saveFansLabel:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年9月5日 下午3:33:44 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "batchAddLabelToFans",method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> batchAddLabelToFans(HttpServletRequest request) {
		String wechatFansIds =null;
		String tagId =null;
		Map<String,Object> result = null;
		String status =null;
		List<String> wechatFansList =null;
		try {
			result =new HashMap<String,Object>();
			wechatFansIds = request.getParameter("wechatFansIds");
			tagId = request.getParameter("tagId");
			wechatFansList = Arrays.asList(wechatFansIds.split(","));
			wechatFanLabelService.batchAddLabelToFans(tagId,wechatFansList);
			status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			e.printStackTrace();
		}finally{
			result.put("status", status);
		}
		return result;
	}
	
	/**
	 * showWechatFansList:(查询粉丝标签). <br/>
	 * Date: 2015年9月5日 下午4:19:09 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/showWechatFansLabelList", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showWechatFansLabelList(HttpServletRequest request){
		// 第几页
		String page =null;
		// 页大小
		String rows =null;
		// 昵称
		String nickName =null;
		// 某一个标签id
		String LabelId =null;
		Map<String, Object> map = null;
		Page<WechatFans> wechatFansPage =null;
		Example example =null;
		Criteria criteria =null;
		List<WechatFans> wechatFans =null;
		List<WechatFanLabelExp> wechatFanLabelExpList =null;
		try {
			example = new Example();
			map = new HashMap<String, Object>();
			wechatFansPage = new Page<WechatFans>();
			criteria = example.createCriteria();
			nickName = request.getParameter("nickName");
			LabelId = request.getParameter("LabelId");
			page =request.getParameter("page");
			rows =request.getParameter("rows");
			if(!StringUtils.isEmpty(nickName)){
				criteria.andLike("wf.NICK_NAME", nickName);
			}
			if(!StringUtils.isEmpty(page)){
				wechatFansPage.setPageNo(new Integer(page));
			}
			if(!StringUtils.isEmpty(rows)){
				wechatFansPage.setPageSize(new Integer(rows));
			}
			weChatFansService.queryWeChatFansList(example, wechatFansPage);
			wechatFans =wechatFansPage.getResults();
			// labelExp的扩展类
			wechatFanLabelExpList = wechatFanLabelService.transToLabelExpList(wechatFans);
			// 添加粉丝标签
			wechatFanLabelExpList =wechatFanLabelService.handleData(wechatFanLabelExpList,LabelId);
			
			map.put("total", wechatFanLabelExpList.size());
			map.put("rows", wechatFanLabelExpList);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Autowired
	public void setWechatFansLabelService(
			WechatFansLabelService wechatFansLabelService) {
		this.wechatFansLabelService = wechatFansLabelService;
	}
	
	@Autowired
	public void setWechatFanLabelService(WechatFanLabelService wechatFanLabelService) {
		this.wechatFanLabelService = wechatFanLabelService;
	}
	
	@Autowired
	public void setWeChatFansService(WeChatFansService weChatFansService) {
		this.weChatFansService = weChatFansService;
	}


	
}

