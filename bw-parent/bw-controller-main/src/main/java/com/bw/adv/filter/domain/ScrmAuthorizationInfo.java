package com.bw.adv.filter.domain;

import com.bw.adv.module.sys.enums.SysRoleDataFilterType;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.exp.SysRoleDataExp;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.shiro.authz.SimpleAuthorizationInfo;

import java.util.*;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public class ScrmAuthorizationInfo extends SimpleAuthorizationInfo{

    private static final Logger LOGGER = Logger.getLogger(ScrmAuthorizationInfo.class);
    private static final String COMMA_STR = ",";

    /**
     * 会员功能菜单列表
     */
    private List<SysFunction> sysFunctions;

    /**
     * 数据过滤map
     * key className
     */
    private Map<String, JsonPropertyFilter> filterMap;

    public Map<String, JsonPropertyFilter> getFilterMap() {
        return filterMap;
    }

    public List<SysFunction> getSysFunctions() {
        return sysFunctions;
    }

    public void setSysFunctions(List<SysFunction> sysFunctions) {
        this.sysFunctions = sysFunctions;
    }

    /**
     * 初始化数据权限
     * @param sysRoleDataExps
     */
    public void initDataFilter(List<SysRoleDataExp> sysRoleDataExps) {
        if(DataFiledManager.useDefault) {
            JsonPropertyFilter filter = createJsonFilter(sysRoleDataExps);
            if(filter != null) {
                filterMap = Maps.newHashMap();
                filterMap.put(DataFiledManager.DEFAULT_FILTER, filter);
            }
            return;
        }
        Map<String, List<SysRoleDataExp>> dataMap = new HashMap<String, List<SysRoleDataExp>>();
        List<SysRoleDataExp> dataList = null;
        for(SysRoleDataExp sysRoleDataExp : sysRoleDataExps) {
            if((dataList = dataMap.get(sysRoleDataExp.getClassName())) == null) {
                dataList = new ArrayList<SysRoleDataExp>();
                dataMap.put(sysRoleDataExp.getClassName(), dataList);
            }
            dataList.add(sysRoleDataExp);
        }

        Map<String, JsonPropertyFilter> resultMap = new HashMap<String, JsonPropertyFilter>();
        JsonPropertyFilter jsonPropertyFilter = null;
        for(String className : dataMap.keySet()) {
            dataList = dataMap.get(className);
            if(dataList.isEmpty()) continue;
            if((jsonPropertyFilter = createJsonFilter(dataList)) != null) {
                resultMap.put(className, jsonPropertyFilter);
            }
        }
        filterMap = resultMap;
    }

    /**
     * 根据className来分类filter
     * @param sysRoleDataExps
     * @return
     */
    private JsonPropertyFilter createJsonFilter(List<SysRoleDataExp> sysRoleDataExps) {
        Set<String> filterField = new HashSet<String>();
        Set<SysRoleDataExp> treeSet = Sets.newTreeSet(sysRoleDataExps);
        for(SysRoleDataExp sysRoleDataExp : treeSet) {
            Set<String> params = Sets.newHashSet(sysRoleDataExp.getOperationParams().split(COMMA_STR));
            if(sysRoleDataExp.getFilterTypeId().equals(SysRoleDataFilterType.NOT_DISPLAY.getKey())) {
                filterField.addAll(params);
            } else if(sysRoleDataExp.getFilterTypeId().equals(SysRoleDataFilterType.DISPLAY.getKey())) {
                for(String fieldName : params) {
                    filterField.remove(fieldName);
                }
            }
        }
        return CollectionUtils.isEmpty(filterField) ? null : new JsonPropertyFilter(filterField);
    }

}
