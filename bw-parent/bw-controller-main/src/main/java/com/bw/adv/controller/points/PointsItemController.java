/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:PointsItemController.java
 * Package Name:com.sage.scrm.controller.points
 * Date:2015年9月1日下午2:27:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.points;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.member.exception.MemberPointsItemException;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.points.model.PointsType;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.points.service.MemberPointsItemSevice;

/**
 * ClassName:PointsItemController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月1日 下午2:27:56 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/pointsItemController")
@SuppressWarnings("unused")
public class PointsItemController {
	
	private MemberPointsItemSevice memberPointsItemSevice;
	
	private MemberService memberService;
	
	/**
	 * 
	 * showListByExample:根据条件查询会员积分列表
	 * Date: 2015年9月1日 下午2:39:22 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberPointsItemExp
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showListByExample")
	@ResponseBody
	public Map<String, Object> showListByExample(MemberPointsItemExp memberPointsItemExp,int page,int rows){
		Map<String, Object> map = null;
		List<MemberPointsItemExp> result = null;
		Page<MemberPointsItemExp> pageObj = null;
		Example example = null;
		
		try {
			map = new HashMap<String, Object>();
			example = new Example();
			Criteria temp = example.createCriteria();
			if(memberPointsItemExp!=null){
				if(StringUtils.isNotBlank(memberPointsItemExp.getMemberCode())){
					temp.andEqualTo("mem.member_code", memberPointsItemExp.getMemberCode());
				}
				if(StringUtils.isNotBlank(memberPointsItemExp.getCardNo())){
					temp.andEqualTo("mc.card_no", memberPointsItemExp.getCardNo());
				}
				if(StringUtils.isNotBlank(memberPointsItemExp.getMobile())){
					temp.andEqualTo("mem.mobile", memberPointsItemExp.getMobile());
				}
				//发生时间
				if(StringUtils.isNotBlank(memberPointsItemExp.getHappenTimeStart())){
					temp.andGreaterThanOrEqualTo("mpi.HAPPEN_TIME", DateUtils.formatString(memberPointsItemExp.getHappenTimeStart()+"000000"));
				}
				if(StringUtils.isNotBlank(memberPointsItemExp.getHappenTimeEnd())){
					temp.andLessThanOrEqualTo("mpi.HAPPEN_TIME", DateUtils.formatString(memberPointsItemExp.getHappenTimeEnd())+"235959");
				}
				//有效期
				if(StringUtils.isNotBlank(memberPointsItemExp.getInvalidTimeStart())){
					temp.andGreaterThanOrEqualTo("mpi.INVALID_DATE", DateUtils.formatString(memberPointsItemExp.getInvalidTimeStart()));
				}
				if(StringUtils.isNotBlank(memberPointsItemExp.getInvalidTimeEnd())){
					temp.andLessThanOrEqualTo("mpi.INVALID_DATE",DateUtils.formatString(memberPointsItemExp.getInvalidTimeEnd()));
				}
				//积分名称
				if(StringUtils.isNotBlank(memberPointsItemExp.getPointsRuleName())){
					temp.andLike("pr.POINTS_RULE_NAME", "%"+memberPointsItemExp.getPointsRuleName()+"%");
				}
				//积分类型
				if(memberPointsItemExp.getPointsTypeId() != null){
					temp.andEqualTo("pt.POINTS_TYPE_ID", memberPointsItemExp.getPointsTypeId());
				}
				if(memberPointsItemExp.getStatusId() != null){
					temp.andEqualTo("mpi.STATUS_ID", memberPointsItemExp.getStatusId());
				}
				
			}
			pageObj = new Page<MemberPointsItemExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = memberPointsItemSevice.queryExpByExample(example, pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return map;
	}
	
	/**
	 * 
	 * showPointsTypeList:查询所有积分类型
	 * Date: 2015年9月1日 下午7:35:33 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showPointsTypeList")
	@ResponseBody
	public List<PointsType> showPointsTypeList(){
		List<PointsType> result = null;
		
		try {
			result = memberPointsItemSevice.queryPointsTypeList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * showMemberInfo:积分调账时根据条件查询会员信息. <br/>
	 * Date: 2016年1月19日 下午6:59:36 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showMemberInfo")
	@ResponseBody
	public DataGridModel<MemberExp> showMemberInfo(MemberSearch search) {
		DataGridModel<MemberExp> result = null;
		List<MemberExp> list = null;
		try {
			list = this.memberService.queryListSearchAuto(search);
			result =  new DataGridModel<MemberExp>(list, Long.parseLong(list.size()+""));
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * savePointsRevise:保存积分调账信息. <br/>
	 * Date: 2016年1月20日 下午1:25:20 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItemExp
	 * @return
	 */
	@RequestMapping(value="/savePointsRevise", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> savePointsRevise(HttpServletRequest request,MemberPointsItemExp memberPointsItemExp){
		Map<String, Object> map = null;
		try {
			 map = new HashMap<String, Object>();
			 memberPointsItemExp.setCreateBy(SysUserUtil.getSessionUser(request).getUserId());
			 memberPointsItemExp.setStatusId(StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId());
			 memberPointsItemSevice.savePointsRevise(memberPointsItemExp);
			 map.put("message", "积分调账成功");
		}catch (MemberPointsItemException pointsException){
			 map.put("message", pointsException.getMessage());
		}catch (Exception e) {
			 map.put("message", "积分调账失败");
			 e.printStackTrace(); 
		}
		return map;
	} 
	
	/**
	 * showPointsReviseList:查询积分调账记录. <br/>
	 * Date: 2016年1月20日 下午7:35:27 <br/>
	 * scrmVersion 1.0
	 * @author tait
	 * @version jdk1.7
	 * @param memberPointsItemExp
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showPointsReviseList")
	@ResponseBody
	public Map<String, Object> showPointsReviseList(MemberPointsItemExp memberPointsItemExp,String mobile,int page,int rows){
		Map<String, Object> map = null;
		List<MemberPointsItemExp> result = null;
		Page<MemberPointsItemExp> pageObj = null;
		Example example = null;
		
		try {
			map = new HashMap<String, Object>();
			example = new Example();
			Criteria temp = example.createCriteria();
			if(memberPointsItemExp!=null){
				temp.andIsNotNull("mpi.points_type_id");
				
				if(StringUtils.isNotBlank(memberPointsItemExp.getMemberCode())){
					temp.andEqualTo("mem.member_code", memberPointsItemExp.getMemberCode());
				}
				if(StringUtils.isNotBlank(mobile)){
					temp.andEqualTo("mem.mobile", mobile);
				}
				if(StringUtils.isNotBlank(memberPointsItemExp.getCreateUserName())){
					temp.andLike("u.cn", memberPointsItemExp.getCreateUserName());
				}
				//发生时间
				if(StringUtils.isNotBlank(memberPointsItemExp.getHappenTimeStart())){
					temp.andGreaterThanOrEqualTo("mpi.HAPPEN_TIME", DateUtils.formatString(memberPointsItemExp.getHappenTimeStart()+"000000"));
				}
				if(StringUtils.isNotBlank(memberPointsItemExp.getHappenTimeEnd())){
					temp.andLessThanOrEqualTo("mpi.HAPPEN_TIME", DateUtils.formatString(memberPointsItemExp.getHappenTimeEnd())+"235959");
				}
				
			}
			pageObj = new Page<MemberPointsItemExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = memberPointsItemSevice.queryPointsReviseList(example, pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@Autowired
	public void setMemberPointsItemSevice(
			MemberPointsItemSevice memberPointsItemSevice) {
		this.memberPointsItemSevice = memberPointsItemSevice;
	}

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

}

