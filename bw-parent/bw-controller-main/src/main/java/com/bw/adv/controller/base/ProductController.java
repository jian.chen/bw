package com.bw.adv.controller.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.alibaba.fastjson.JSONArray;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.base.model.ProductCategory;
import com.bw.adv.module.base.model.ProductCategoryItem;
import com.bw.adv.module.base.model.exp.ProductExp;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelImport;
import com.bw.adv.service.base.service.ProductCategoryService;
import com.bw.adv.service.base.service.ProductService;

@Controller
@RequestMapping("/productController")
@SuppressWarnings("unused")
public class ProductController {
	@Resource
	private ProductService productService;

	private ProductCategoryService productCategoryService;

	/**
	 * showProduct:查询产品列表. <br/>
	 * Date: 2015-8-26 上午11:00:23 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/showProduct", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showProduct(int page,int rows,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Page<ProductExp> pageObj = null;
		List<ProductExp> productList = null;
		Map<String, Object> map = null;
		Map<String, Object> searchMap  = new HashMap<String, Object>();
		try {	
			//获取查询参数
			searchMap.put("productCode", request.getParameter("productCode") == null ? "" :request.getParameter("productCode").trim());
			searchMap.put("productName", request.getParameter("productName") == null ? "" :request.getParameter("productName").trim());
			searchMap.put("productCategoryId", request.getParameter("productCategoryId") == null ? "" :request.getParameter("productCategoryId").trim());
			
			map = new HashMap<String, Object>();
			pageObj = new Page<ProductExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			productList = productService.queryListForProduct(StatusConstant.PRODUCT_ACTIVE.getId(),pageObj,searchMap);

			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
		
	}
	
	@RequestMapping(value="/showProductByExample", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showProductByExample(Product product,int page,int rows) throws IOException{
		Page<Product> pageObj = null;
		List<Product> sysRoleList = null;
		Map<String, Object> map = null;
		Example example = null;
		
		try {	
	
			map = new HashMap<String, Object>();
			
			pageObj = new Page<Product>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			example = new Example();
			Criteria temp = example.createCriteria();
			temp.andEqualTo("STATUS_ID", StatusConstant.PRODUCT_ACTIVE.getId());
			if(product != null){
				if(product.getProductCode() != null && product.getProductCode() != ""){
					temp.andLike("PRODUCT_CODE", "%"+product.getProductCode()+"%");
				}
				if(product.getProductName() != null && product.getProductName() != ""){
					temp.andLike("PRODUCT_Name", "%"+product.getProductName()+"%");
				}
			}
			
			sysRoleList=productService.queryByExample(example,pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
		
	}
	
	/**
	 * saveProduct:新增产品保存. <br/>
	 * Date: 2015-8-26 上午11:00:55 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param product
	 * @return
	 */
	@RequestMapping(value = "saveProduct", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public JsonModel saveProduct(Product product,String sortList){
		JsonModel result = null;
		String createTime=null;
		try {
			result = new JsonModel();
			createTime= DateUtils.getCurrentTimeOfDb();
			product.setCreateTime(createTime);
			product.setStatusId(StatusConstant.PRODUCT_ACTIVE.getId());
			JSONArray jsonarray = JSONArray.parseArray(sortList);
			
			if(StringUtils.isBlank(product.getProductCode())){
				result.setMessage("产品编号不能为空!");
				result.setStatusError();
				return result;
			}else{
				List<Product> list=productService.queryListForProductByProductCode(StatusConstant.PRODUCT_ACTIVE.getId(), product.getProductCode());
				if(null!=list&&list.size()>0){
					result.setMessage("产品编号不能重复!");
					result.setStatusError();
					return result;
				}			
			}
			
			productService.save(product);
			
			List<ProductCategoryItem> itemList = new ArrayList<ProductCategoryItem>();
			for (int i = 0; i < jsonarray.size(); i++) {
				ProductCategoryItem item = new ProductCategoryItem();
				item.setProductId(product.getProductId());
				item.setProductCategoryId(jsonarray.getLong(i));
				itemList.add(item);
			}
			productCategoryService.saveProductCategoryItem(itemList);
			result.setMessage("新增成功!");
			result.setStatusSuccess();
		} catch (Exception e) {
			result.setMessage("新增失败!");
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}	

	/**
	 * productDetail:获取产品明细. <br/>
	 * Date: 2015-8-26 上午11:01:18 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param productId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "productDetail", method = RequestMethod.POST)
	@ResponseBody
	public Product productDetail(String productId) throws IOException{
		Product product=null;
		try {	
			product=new Product();
			product = productService.queryProductByPrimaryKey(StatusConstant.PRODUCT_ACTIVE.getId(), Long.parseLong(productId));
			List<ProductCategoryItem> itemList = productService.queryCategoryItemById(product.getProductId());
			product.setItemList(itemList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return product;
	}	
	
	/**
	 * updateProduct:编辑产品保存 <br/>
	 * Date: 2015-8-26 上午11:01:34 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param product
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "editProduct", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel editProduct(Product product,String sortList){
		JsonModel result = null;
		String updateTime=null;
		try {
			result = new JsonModel();
			updateTime= DateUtils.getCurrentTimeOfDb();
			product.setUpdateTime(updateTime);
			JSONArray jsonarray = JSONArray.parseArray(sortList);
			
			if(StringUtils.isBlank(product.getProductCode())){
				result.setMessage("产品编号不能为空!");
				result.setStatusError();
				return result;
			}else{
				List<Product> list=productService.queryListForProductByProductCode(StatusConstant.PRODUCT_ACTIVE.getId(), product.getProductCode());
				if(null!=list&&list.size()>0){
					Boolean b=false;
					for(Product p:list){
						if(!p.getProductId().equals(product.getProductId())){//id不同 code相同
							b=true;
						}
					}
					if(b){
						result.setMessage("产品编号不能重复!");
						result.setStatusError();
						return result;
					}
				}	
			}		
			
			productService.updateByPkSelective(product);
			
			List<ProductCategoryItem> itemList = new ArrayList<ProductCategoryItem>();
			for (int i = 0; i < jsonarray.size(); i++) {
				ProductCategoryItem item = new ProductCategoryItem();
				item.setProductId(product.getProductId());
				item.setProductCategoryId(jsonarray.getLong(i));
				itemList.add(item);
			}
			productCategoryService.removeProductCategoryItemByProductId(product.getProductId());
			productCategoryService.saveProductCategoryItem(itemList);
			
			result.setMessage("编辑成功!");
			result.setStatusSuccess();
		} catch (Exception e) {
			result.setMessage("编辑失败!");
			result.setStatusError();
			e.printStackTrace();
		}
		
		return result;
	}	
	
	/**
	 * deleteProduct:批量删除产品（软删除，更改产品状态） <br/>
	 * Date: 2015-8-26 上午11:01:57 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param productIds
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "removeProduct", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel removeProduct(Long productId){
		JsonModel result = null;	
		try {
			result = new JsonModel();
			productService.deleteProducts(productId);
			result.setMessage("删除成功");
			result.setStatusSuccess();
		} catch (Exception e) {
			result.setMessage("删除失败");
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * importData:(产品导入). <br/>
	 * Date: 2015-9-25 下午3:07:28 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(method = RequestMethod.POST, value = "importData")
	@ResponseBody
	public JsonModel importData(MultipartHttpServletRequest request) {
		JsonModel result = null;
		String code =null;
		String message =null;
		List<Product> list = null;
		MultipartFile file = null;
		String filePath = null;
		File fileExcel = null;
		ExcelImport test = null;
		List<Product> data = null;
		String codes=null;//导入的所有数据的code
		String eachcode=null;//导入的数据的每条code
		 
		try {
			result = new JsonModel();
			file = request.getFile("fileInput");
			  // 文件保存路径  
            filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/"  
                     + file.getOriginalFilename();  
            fileExcel = new File(filePath);
            file.transferTo(fileExcel);  
			test = new ExcelImport();
			data = (ArrayList) test.importExcel(fileExcel, Product.class);
			list = new ArrayList<Product>();
			if(data != null && data.size()>0){
				codes="";
				for(Product p:data){
					eachcode=p.getProductCode();
					List<Product> productList=productService.queryListForProductByProductCode(StatusConstant.PRODUCT_ACTIVE.getId(), eachcode);
					if(productList == null || productList.size()==0 ){
						if(StringUtils.isNotBlank(eachcode)&&codes.indexOf("'"+eachcode+"'")<0){
							codes+="'"+eachcode+"'";
							p.setCreateTime(DateUtils.getCurrentTimeOfDb());
							p.setStatusId(StatusConstant.PRODUCT_ACTIVE.getId());
							list.add(p);
						}
					}
				}
			}
			fileExcel.delete();
			productService.insertProducts(list);
			result.setMessage("导入成功");
			result.setStatusSuccess();
		}
		catch (Exception ex) {
			result.setMessage("导入失败");
			result.setStatusError();
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * downTemp:(模板导出). <br/>
	 * Date: 2015-9-25 下午3:07:41 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param response
	 * @param request
	 */
	@RequestMapping(method = RequestMethod.GET, value = "downTemp")
	public void downTemp(HttpServletResponse response,HttpServletRequest request) {
		String filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/产品导入模板.xls" ;  
		try {
			FileUtils.download("产品导入模板.xls", filePath, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 产品类别模板导出
	 * downCategoryTemp:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param response
	 * @param request
	 */
	@RequestMapping(method = RequestMethod.GET, value = "downCategoryTemp")
	public void downCategoryTemp(HttpServletResponse response,HttpServletRequest request) {
		String filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/类别导入模板.xls" ;  
		try {
			FileUtils.download("类别导入模板.xls", filePath, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 产品类别导入
	 * importCategoryData:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(method = RequestMethod.POST, value = "importCategoryData")
	@ResponseBody
	public JsonModel importCategoryData(MultipartHttpServletRequest request) {
		JsonModel result = null;
		String code =null;
		String message =null;
		List<ProductCategory> list = null;
		MultipartFile file = null;
		String filePath = null;
		File fileExcel = null;
		ExcelImport test = null;
		List<ProductCategory> data = null;
		String codes=null;//导入的所有数据的code
		String eachcode=null;//导入的数据的每条code
		 
		try {
			result = new JsonModel();
			file = request.getFile("fileInput");
			  // 文件保存路径  
            filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/"  
                     + file.getOriginalFilename();  
            fileExcel = new File(filePath);
            file.transferTo(fileExcel);  
			test = new ExcelImport();
			data = (ArrayList) test.importExcel(fileExcel, ProductCategory.class);
			list = new ArrayList<ProductCategory>();
			if(data != null && data.size()>0){
				codes = "";
				for(ProductCategory p:data){
					eachcode = p.getProductCategoryCode();
					int productList = productCategoryService.findProductCategoryIsMultiple(p);
					if( productList == 0 ){
						if(StringUtils.isNotBlank(eachcode)&&codes.indexOf("'"+eachcode+"'")<0){
							codes+="'"+eachcode+"'";
							p.setCreateTime(DateUtils.getCurrentTimeOfDb());
							list.add(p);
						}
					}
				}
			}
			fileExcel.delete();
			productCategoryService.saveProductCategory(list);
			result.setMessage("导入成功");
			result.setStatusSuccess();
		}
		catch (Exception ex) {
			result.setMessage("导入失败");
			result.setStatusError();
			ex.printStackTrace();
		}
		return result;
	}

	@Autowired
	public void setProductCategoryService(ProductCategoryService productCategoryService) {
		this.productCategoryService = productCategoryService;
	}
	
}
