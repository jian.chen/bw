/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:SceneGameController.java
 * Package Name:com.sage.scrm.controller.activity
 * Date:2016年1月15日下午1:50:14
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.activity;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.module.activity.model.SceneGame;
import com.bw.adv.module.activity.model.exp.SceneGameExp;
import com.bw.adv.module.component.rule.init.BusinessShowPageInit;
import com.bw.adv.module.tools.BeanUtils;
import com.bw.adv.service.activity.service.SceneGameService;

/**
 * ClassName:SceneGameController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午1:50:14 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/sceneGameController")
public class SceneGameController {
	
	private static String GAME_SMASHING_EGGS = "GAME_SMASHING_EGGS";
	private static String GAME_SCRATCH_CARD = "GAME_SCRATCH_CARD";
	private static String GAME_SHAKE = "GAME_SHAKE";

	@Autowired
	private SceneGameService sceneGameService;
	
	/**
	 * 
	 * showSceneList:查询所有的场景活动<br/>
	 * Date: 2016年1月15日 上午11:47:37 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/showSceneGameList")
	@ResponseBody
	public List<SceneGameExp> showSceneGameList(HttpServletRequest request,HttpServletResponse response){
		List<SceneGame> resultList = null;
		List<SceneGameExp> sceneGameExpList = null;
		SceneGameExp exp = null;
		try {
			resultList = sceneGameService.queryAllSceneGame();
			sceneGameExpList = new ArrayList<SceneGameExp>();
			for (SceneGame sceneGame : resultList) {
				exp = new SceneGameExp();
				BeanUtils.copy(sceneGame, exp);
				//System.out.println(BusinessShowPageInit.getShowPageUrlByCode(sceneGame.getSceneGameCode())+"------"+sceneGame.getSceneGameCode());
				exp.setPageUrl(BusinessShowPageInit.getShowPageUrlByCode(sceneGame.getSceneGameCode()));
				sceneGameExpList.add(exp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sceneGameExpList;
	}
}

