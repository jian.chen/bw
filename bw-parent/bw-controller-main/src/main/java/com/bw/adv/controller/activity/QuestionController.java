package com.bw.adv.controller.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.QuestionCount;
import com.bw.adv.module.activity.model.Questionnaire;
import com.bw.adv.module.activity.model.QuestionnaireTemplate;
import com.bw.adv.module.activity.model.QuestionnaireTemplateInfo;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.QuestionService;

@Controller
@RequestMapping("/QuestionController")
public class QuestionController {
	
	private QuestionService questionService;
	
	/**
	 * 新建问卷
	 * @param questionnaire
	 * @return
	 */
	@RequestMapping(value = "/addQuestion", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addQuestion(Questionnaire questionnaire){
		Map<String, Object> map = null;
		try {
			questionnaire.setStartDate(DateUtils.formatString(questionnaire.getStartDate()));
			questionnaire.setEndDate(DateUtils.formatString(questionnaire.getEndDate()));
			questionnaire.setCreateTime(DateUtils.getCurrentTimeOfDb());
			questionnaire.setStatusId(StatusConstant.ACTIVITY_DISABLE.getId());
			int id = this.questionService.insertQuestion(questionnaire);
			map = new HashMap<String, Object>();
			map.put("message", "新建问卷成功！");
			map.put("id", id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 查找所有问卷
	 * @return
	 */
	@RequestMapping(value = "/showQuestion", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showQuestion(int page,int rows,HttpServletRequest request){
		Map<String, Object> map = null;
		Page<Questionnaire> templatePage = null;
		try {
			map = new HashMap<String, Object>();
			templatePage = new Page<Questionnaire>();
			templatePage.setPageNo(page);
			templatePage.setPageSize(rows);
			this.questionService.selectAllQuestionWithTemplateCount(templatePage);
			map.put("total", templatePage.getTotalRecord());
			map.put("rows", templatePage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 删除问卷
	 * @param questionnaireId
	 * @return
	 */
	@RequestMapping(value = "/deleteQuestion", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> deleteQuestion(Long questionnaireId){
		Map<String, Object> map = null;
		try {
			this.questionService.deleteQuestion(questionnaireId);
			map = new HashMap<String, Object>();
			map.put("code", "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 查询需要修改的问卷
	 * @param editQuestionId
	 * @return
	 */
	@RequestMapping(value = "/showEditQuestion", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showEditQuestion(Long editQuestionId){
		Map<String, Object> map = null;
		try {
			Questionnaire editResult = this.questionService.queryNaire(editQuestionId);
			map = new HashMap<String, Object>();
			map.put("editResult", editResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 修改问卷
	 * @param questionnaire
	 * @return
	 */
	@RequestMapping(value = "/updateQuestion", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateQuestion(Questionnaire questionnaire){
		Map<String, Object> map = null;
		try {
			questionnaire.setStartDate(DateUtils.formatString(questionnaire.getStartDate()));
			questionnaire.setEndDate(DateUtils.formatString(questionnaire.getEndDate()));
			questionnaire.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			this.questionService.updateQuestion(questionnaire);
			map = new HashMap<String, Object>();
			map.put("message", "更新问卷成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 查询问卷的题目
	 * @param questionnaireId
	 * @return
	 */
	@RequestMapping(value = "/showTemplate", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showTemplate(Long questionnaireId,int page,int rows,HttpServletRequest request){
		Map<String, Object> map = null;
		Page<QuestionnaireTemplate> templatePage = null;
		try {
			map = new HashMap<String, Object>();
			templatePage = new Page<QuestionnaireTemplate>();
			templatePage.setPageNo(page);
			templatePage.setPageSize(rows);
			this.questionService.queryQuestionTemplate(questionnaireId,templatePage);
			map.put("total", templatePage.getTotalRecord());
			map.put("rows", templatePage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/saveTemplate", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> saveTemplate(@RequestParam("selectList[]")String[] selectList,Long type,String explains,Long selectNum,Long questionnaireId){
		Map<String, Object> map = null;
		QuestionnaireTemplate template = null;
		QuestionnaireTemplateInfo templateInfo = null;
		List<QuestionnaireTemplateInfo> infoList = null;
		try {
			template = new QuestionnaireTemplate();
			template.setTitle(explains);
			template.setType(type);
			template.setSelectNum(selectNum);
			template.setQuestionnaireId(questionnaireId);
			this.questionService.saveTemplate(template);
			
			infoList = new ArrayList<QuestionnaireTemplateInfo>();
			for(int i = 0;i < selectList.length; i++){
				templateInfo = new QuestionnaireTemplateInfo();
				templateInfo.setContent(selectList[i]);
				templateInfo.setQuestionnaireTemplateId(template.getQuestionnaireTemplateId());
				infoList.add(templateInfo);
			}
			this.questionService.insertTemplateInfo(infoList);
			
			map = new HashMap<String, Object>();
			map.put("message", "保存成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 更新选项和题目
	 * @param selectList
	 * @param type
	 * @param explains
	 * @param selectNum
	 * @param questionnaireId
	 * @param selectTemplateId
	 * @return
	 */
	@RequestMapping(value = "/updateTemplate", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> updateTemplate(@RequestParam("selectList[]")String[] selectList,Long type,String explains,Long selectNum,Long questionnaireId,Long selectTemplateId){
		Map<String, Object> map = null;
		QuestionnaireTemplate template = null;
		QuestionnaireTemplateInfo templateInfo = null;
		List<QuestionnaireTemplateInfo> infoList = null;
		try {
			template = new QuestionnaireTemplate();
			template.setTitle(explains);
			template.setType(type);
			template.setSelectNum(selectNum);
			template.setQuestionnaireId(questionnaireId);
			template.setQuestionnaireTemplateId(selectTemplateId);
			this.questionService.updateTemplate(template);
			
			this.questionService.deleteTemplateInfoByTemplateId(selectTemplateId);
			infoList = new ArrayList<QuestionnaireTemplateInfo>();
			for(int i = 0;i < selectList.length; i++){
				templateInfo = new QuestionnaireTemplateInfo();
				templateInfo.setContent(selectList[i]);
				templateInfo.setQuestionnaireTemplateId(template.getQuestionnaireTemplateId());
				infoList.add(templateInfo);
			}
			this.questionService.insertTemplateInfo(infoList);
			
			map = new HashMap<String, Object>();
			map.put("message", "更新成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/delTemplete", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> delTemplete(Long questionnaireTemplateId){
		Map<String, Object> map = null;
		try {
			this.questionService.deleteTemplate(questionnaireTemplateId);
			map = new HashMap<String, Object>();
			map.put("message", "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/showTemplateAndInfo", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showTemplateAndInfo(Long selectTemplateId){
		Map<String, Object> map = null;
		QuestionnaireTemplate template = null;
		List<QuestionnaireTemplateInfo> infoList = null;
		try {
			map = new HashMap<String, Object>();
			template = this.questionService.queryTemplateById(selectTemplateId);
			infoList = this.questionService.queryTemplateInfoByTemplateId(selectTemplateId);
			map.put("template", template);
			map.put("infoList", infoList);
			map.put("message", "ok");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/showQuestionCount", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<Long,Object>> showQuestionCount(Long questionnaireId){
		Map<Long, Object> map = null;
		List<Map<Long,Object>> result = new ArrayList<Map<Long,Object>>();
		try {
			List<QuestionnaireTemplate> templateList = questionService.queryQuestionTemplateNoPage(questionnaireId);
			for (int i = 0; i < templateList.size(); i++) {
				map = new HashMap<Long, Object>();
				if(templateList.get(i).getType() == 1L){
					List<QuestionCount> countList = questionService.selectResultCountForSelect(templateList.get(i).getQuestionnaireTemplateId());
					map.put(1L, countList);
				}
				if(templateList.get(i).getType() == 2L){
					List<QuestionCount> countList = questionService.selectResultCountForMark(templateList.get(i).getQuestionnaireTemplateId());
					map.put(2L, countList);
				}
				if(templateList.get(i).getType() == 3L){
					List<QuestionCount> countList = questionService.selectResultCountForQa(templateList.get(i).getQuestionnaireTemplateId());
					map.put(3L, countList);
				}
				result.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "/instanceAllMember", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> instanceAllMember(Long questionnaireId){
		Map<String, Object> map = null;
		try {
			map = new HashMap<String, Object>();
			Long sum = this.questionService.selectCountInfoForMember(questionnaireId);
			map.put("sum", sum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/changeQuestionStatus", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel changeQuestionStatus(Long questionnaireId,String status){
		JsonModel result = null;
		Questionnaire question = new Questionnaire();
		try {
			result = new JsonModel();
			if(status.equals("Y")){
				Long activieAmount = this.questionService.queryActiveQuestionAmount();
				if(activieAmount > 0){
					result.setMessage("同一时间只能有一份问卷启用!");
					result.setStatusError();
					return result;
				}else{
					question.setQuestionnaireId(questionnaireId);
					question.setStatusId(StatusConstant.ACTIVITY_ENABLE.getId());
					this.questionService.updateQuestion(question);
				}
			}
			if(status.equals("N")){
				question.setQuestionnaireId(questionnaireId);
				question.setStatusId(StatusConstant.ACTIVITY_DISABLE.getId());
				this.questionService.updateQuestion(question);
			}
			result.setStatusSuccess();
			result.setMessage("操作成功!");
		} catch (Exception e) {
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}

	@Autowired
	public void setQuestionService(QuestionService questionService) {
		this.questionService = questionService;
	}
	
}
