/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:WechatFansController.java
 * Package Name:com.sage.scrm.controller.wechat
 * Date:2015年8月28日下午5:12:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.wechat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.wechat.api.ApiWeChatFansGroup;
import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansGroup;
import com.bw.adv.module.wechat.model.WechatFansLabel;
import com.bw.adv.module.wechat.model.exp.WechatFansGroupItemExp;
import com.bw.adv.service.wechat.service.WeChatFansService;
import com.bw.adv.service.wechat.service.WechatFansGroupItemService;
import com.bw.adv.service.wechat.service.WechatFansGroupService;
import com.bw.adv.service.wechat.service.WechatFansLabelService;

/**
 * ClassName:WechatFansController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月28日 下午5:12:50 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/wechatFansController")
public class WechatFansController {
	protected final Logger logger = Logger.getLogger(WechatFansController.class);
	
	private WeChatFansService weChatFansService;
	private WechatFansGroupService wechatFansGroupService;
	private ApiWeChatFansGroup apiWeChatFansGroup;
	private WechatFansGroupItemService wechatFansGroupItemService;
	private WechatFansLabelService wechatFansLabelService;
	
	/**
	 * showWechatFansList:(加载粉丝信息列表). <br/>
	 * Date: 2015年8月29日 下午6:33:13 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/showWechatFansList", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showWechatFansList(HttpServletRequest request){
		// 第几页
		String page =null;
		// 页大小
		String rows =null;
		// 粉丝id
		// openid
		String openId =null;
		// 昵称
		String nickName =null;
		// 性别
		String sex =null;
		//二维码关注渠道
		String orgName =null;
		Map<String, Object> map = null;
		Page<WechatFans> wechatFansPage =null;
		Example example =null;
		
		Criteria criteria =null;
		Criteria criteria2 =null;
		
		List<WechatFans> wechatFans =null;
		Map<String,String> paramMap =null;
		String fromAttentionTime ="";
		String toAttentionTime ="";
		try {
			example = new Example();
			
			map = new HashMap<String, Object>();
			wechatFansPage = new Page<WechatFans>();
			criteria = example.createCriteria();
			criteria2 = example.createCriteria();
			
			openId = request.getParameter("openId");
			nickName = request.getParameter("nickName");
			fromAttentionTime = request.getParameter("fromAttentionTime");
			toAttentionTime = request.getParameter("toAttentionTime");
			orgName = request.getParameter("orgName");
			sex = request.getParameter("sex");
			String isMember = request.getParameter("isMember");
			page =request.getParameter("page");
			rows =request.getParameter("rows");
			paramMap =weChatFansService.transferParam(sex,fromAttentionTime,toAttentionTime);
			sex =paramMap.get("sex");
			fromAttentionTime =paramMap.get("fromAttentionTime");
			toAttentionTime = paramMap.get("toAttentionTime");
			
			if(!StringUtils.isEmpty(openId)){
				criteria.andEqualTo("wf.OPENID", openId);
				criteria2.andEqualTo("wf.OPENID", openId);
			}
			if(!StringUtils.isEmpty(nickName)){
				criteria.andLike("wf.NICK_NAME", nickName);
				criteria2.andLike("wf.NICK_NAME", nickName);
			}
			if(!StringUtils.isEmpty(fromAttentionTime)){
				criteria.andGreaterThanOrEqualTo("wf.ATTENTION_TIME", fromAttentionTime);//>=
				criteria2.andGreaterThanOrEqualTo("wf.ATTENTION_TIME", fromAttentionTime);
			}
			if(!StringUtils.isEmpty(fromAttentionTime)){
				criteria.andLessThanOrEqualTo("wf.ATTENTION_TIME", toAttentionTime);//<=
				criteria2.andLessThanOrEqualTo("wf.ATTENTION_TIME", toAttentionTime);
			}
			if(!StringUtils.isEmpty(isMember)){
				if("是".equals(isMember)){
					criteria.andIsNotNull("wf.MEMBER_ID");
				}else if("否".equals(isMember)){
					criteria.andIsNull("wf.MEMBER_ID");
				}
			}
			if(!StringUtils.isEmpty(sex)){
				if("男".equals(sex)){
					criteria.andEqualTo("wf.SEX", "0");
				}else if("女".equals(sex)){
					criteria.andEqualTo("wf.SEX", "1");
				}
				
			}
			if(!StringUtils.isEmpty(page)){
				wechatFansPage.setPageNo(new Integer(page));
			}
			if(!StringUtils.isEmpty(rows)){
				wechatFansPage.setPageSize(new Integer(rows));
			}
			if(!StringUtils.isEmpty(orgName)){
				criteria.andEqualTo("org.ORG_NAME", orgName);
				criteria2.andEqualTo("org.ORG_NAME", orgName);
			}
			
			weChatFansService.queryWeChatFansList(example, wechatFansPage);
			wechatFans =wechatFansPage.getResults();
			wechatFans = weChatFansService.transData(wechatFans);
			map.put("total", wechatFansPage.getTotalRecord());
			map.put("rows", wechatFans);
			
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * showWechatFansList:(查询微信粉丝分组信息). <br/>
	 * Date: 2015年9月1日 下午9:30:50 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/showWechatFansGroupList", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showWechatFansGroupList(HttpServletRequest request){
		String groupId =null;
		Map<String,Object> map =null;
		List<WechatFansGroupItemExp> WechatFansGroupItemExpList =null;
		List<WechatFans> wechatFansList =null;
		try {
			map =new HashMap<String,Object>();
			wechatFansList =new ArrayList<WechatFans>();
			WechatFansGroupItemExpList =new ArrayList<WechatFansGroupItemExp>();
			groupId = request.getParameter("groupId");
			WechatFansGroupItemExpList = wechatFansGroupItemService.queryWechatFansGroupItemByGroupId(Long.valueOf(groupId));
			for(WechatFansGroupItemExp wechatGroupItem:WechatFansGroupItemExpList){
				wechatFansList.add(wechatGroupItem.getWechatFansList().get(0));
			}
			wechatFansList = weChatFansService.transData(wechatFansList);
			map.put("rows", wechatFansList);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return map;
	}
	
	/**
	 * 
	 * showWechatFansList:(粉丝详情). <br/>
	 * Date: 2015年8月29日 下午6:33:34 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/showWechatFansDetail", method=RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showWechatFansDetail(HttpServletRequest request){
		String wechatFansId =null;
		WechatFans wechatFans =null;
		Map<String,Object> map =null;
		String status =null;
		List<WechatFans> wechatFansList =null;
		try {
			map =new HashMap<String,Object>();
			wechatFansList =new ArrayList<WechatFans>();
			wechatFansId = request.getParameter("wechatFansId");
			wechatFans = weChatFansService.queryWechatFansById(Long.valueOf(wechatFansId));
			wechatFansList.add(wechatFans);
			wechatFansList = weChatFansService.transData(wechatFansList);
			if(wechatFansList.size()>0){
				wechatFans = wechatFansList.get(0);
			}
			if(wechatFans !=null){
				status ="success";
				map.put("wechatInfo", wechatFans);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			status ="error";
		}finally{
			map.put("status", status);
		}
		return map;
	}
	
	/**
	 * 微信渠道--本地--
	 * showWechatFansGroup:(新建分组). <br/>
	 * Date: 2015年8月31日 下午8:22:28 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/saveWechatFansGroup", method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> saveWechatFansGroup(HttpServletRequest request){
		String groupName=null;
		Map<String,String> map =null;
		String groupNameString =null;
		String groupIdString =null;
		SysUser sysUser =null;
		String status =null;
		try {
			groupName = request.getParameter("groupName");
			System.out.println("***********groupName************:"+groupName);
			sysUser = SysUserUtil.getSessionUser(request);
			/**
			 * 两点注意：
			 *    a.通过微信分组接口建分组时，需要校验特定公众号新建分组的上限
			 *    b.微信分组名称可以相同
			 */
			System.out.println("********groupName*********:"+groupName);
			if(!StringUtils.isEmpty(groupName)){
				map = apiWeChatFansGroup.createWeChatGroup(groupName);
				// 微信渠道--创建成功
				if(map.get("code").equals(WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage())){
					System.out.println("**************微信创建分组成功!*****************");
					groupNameString =map.get("name");
					groupIdString =map.get("id");
					System.out.println("*************groupNameString:***************"+groupNameString);
					wechatFansGroupService.saveWechatFansGroup(groupNameString,groupIdString,sysUser);
					status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
				}
			}
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			logger.error(e.getMessage());
			e.printStackTrace();
		}finally{
			map.put("status", status);
		}
		return map;
	}
	
	
	/**
	 * showWechatFansGroup:(删除分组). <br/>
	 * Date: 2015年8月31日 下午8:22:28 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/deleteWechatFansGroup", method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> deleteWechatFansGroup(HttpServletRequest request){
		String wechatGroupId =null;
		Map<String,String> map =null;
		SysUser sysUser =null;
		String status =null;
		WechatFansGroup wechatFansGroup =null;
		try {
			wechatGroupId = request.getParameter("wechatGroupId");
			sysUser = SysUserUtil.getSessionUser(request);
			if(wechatGroupId!=null){
				wechatFansGroup = wechatFansGroupService.queryByPk(Long.valueOf(wechatGroupId));
			}
			map = apiWeChatFansGroup.deleteWeChatGroup(wechatFansGroup.getGroupId());
			if(map.get("code").equals(WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage())){
				/**
				 * 微信分组信息删除成功:归属会员自动到默认分组上
				 * 本地的话,首先将归属会员分组信息置为无效
				 * 		   然后将分组信息归属到默认分组中
				 * 		  最后删除分组信息	
				 */
				wechatFansGroupService.deleteWechatFansGroup(wechatGroupId,sysUser);
				wechatFansGroupItemService.deleteWechatFansGroupItems(wechatGroupId);
				status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
			}
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			logger.error(e.getMessage());
			e.printStackTrace();
		}finally{
			map.put("status", status);
		}
		return map;
	}
	
	/**
	 * updateWechatFansGroup:(删除分组成功). <br/>
	 * Date: 2015年12月27日 下午2:26:29 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/updateWechatFansGroup", method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> updateWechatFansGroup(HttpServletRequest request){
		String groupName=null;
		String wechatGroupId =null;
		Map<String,String> map =null;
		SysUser sysUser =null;
		String status =null;
		WechatFansGroup wechatFansGroup =null;
		try {
			groupName = request.getParameter("groupNameStr");
			wechatGroupId = request.getParameter("wechatGroupId");
			sysUser = SysUserUtil.getSessionUser(request);
			if(wechatGroupId!=null){
				wechatFansGroup = wechatFansGroupService.queryByPk(Long.valueOf(wechatGroupId));
			}
			if(!StringUtils.isEmpty(groupName)){
				map = apiWeChatFansGroup.updateWeChatGroup(wechatFansGroup.getGroupId(),groupName);
				// 微信渠道--创建成功
				if(map.get("code").equals(WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage())){
					//更新本地分组名称
					wechatFansGroupService.updateWechatFansGroup(wechatGroupId,groupName,sysUser);
					status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
				}
			}
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			logger.error(e.getMessage());
			e.printStackTrace();
		}finally{
			map.put("status", status);
		}
		return map;
	}
	
	
	/**
	 * batchRemoveGroup:(批量粉丝改变所属组). <br/>
	 * Date: 2015年9月2日 上午11:07:19 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/batchRemoveGroup", method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> batchRemoveGroup(HttpServletRequest request){
		// 主键
		String wechatGroupId =null;
		// 粉丝主键
		String wechatFansIds =null;
		String openIds =null;
		WechatFansGroup wechatFansGroup =null;
		String status =null;
		Map<String,String> map =null;
		try {
			map =new HashMap<String,String>();
			wechatGroupId = request.getParameter("groupId");
			wechatFansIds = request.getParameter("wechatFansIds");
			openIds = request.getParameter("openIds");
			wechatFansGroup = wechatFansGroupService.queryByPk(Long.valueOf(wechatGroupId));
			wechatFansGroupService.batchUpdateFansToGroup(openIds,wechatFansIds,wechatFansGroup);
			status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			logger.error(e.getMessage());
			e.printStackTrace();
		}finally{
			map.put("status", status);
		}
		return map;
	}
	
	/**
	 * showWechatFansLabel:(粉丝标签).
	 * Date: 2015年9月3日 下午12:06:12 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/showWechatFansLabel", method=RequestMethod.GET)
	@ResponseBody
	public List<WechatFansLabel> showWechatFansLabel(HttpServletRequest request){
		List<WechatFansLabel> wechatFansLabelList =null;
		try {
			wechatFansLabelList = wechatFansLabelService.queryWechatFansLabels();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return wechatFansLabelList;
	}
	/**
	 * saveWechatFansGroup:(新建标签名称). <br/>
	 * Date: 2015年9月4日 下午1:58:41 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/saveWechatFansLabel", method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> saveWechatFansLabel(HttpServletRequest request){
		// 标签名称
		String labelName=null;
		Map<String,String> map =null;
		String status =null;
		SysUser sysyUser =null;
		try {
			map =new HashMap<String,String>();
			sysyUser =SysUserUtil.getSessionUser(request);
			labelName = request.getParameter("labelName");
			if(!StringUtils.isEmpty(labelName)){
				wechatFansLabelService.saveFansLabel(labelName,sysyUser);
				status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
			}
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			logger.error(e.getMessage());
			e.printStackTrace();
		}finally{
			map.put("status", status);
		}
		return map;
	}
	
	
	@Autowired
	public void setWechatFansLabelService(
			WechatFansLabelService wechatFansLabelService) {
		this.wechatFansLabelService = wechatFansLabelService;
	}
	
	@Autowired
	public void setWeChatFansService(WeChatFansService weChatFansService) {
		this.weChatFansService = weChatFansService;
	}
	
	@Autowired
	public void setWechatFansGroupService(
			WechatFansGroupService wechatFansGroupService) {
		this.wechatFansGroupService = wechatFansGroupService;
	}
	
	@Autowired
	public void setApiWeChatFansGroup(ApiWeChatFansGroup apiWeChatFansGroup) {
		this.apiWeChatFansGroup = apiWeChatFansGroup;
	}
	
	@Autowired
	public void setWechatFansGroupItemService(
			WechatFansGroupItemService wechatFansGroupItemService) {
		this.wechatFansGroupItemService = wechatFansGroupItemService;
	}
	
}

