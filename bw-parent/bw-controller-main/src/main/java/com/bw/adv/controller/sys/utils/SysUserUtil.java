package com.bw.adv.controller.sys.utils;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysUser;

public class SysUserUtil {
	
	public static final String SESSION_USER_KEY = "sysUser";
	public static final String SESSION_ROLES_KEY = "roles";
	public static final String SESSION_ROLE_KEY = "role";
	public static final String SESSION_ORGS_KEY = "ORGS";
	public static final String SESSION_ORG_KEY = "ORG";
	public static final String SESSION_FUNCTION = "sysFunctions";
	
	public static SysUser getSessionUser(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (SysUser) session.getAttribute(SESSION_USER_KEY);
	}
	
	public static SysRole getSessionCurrentSysRole(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (SysRole) session.getAttribute(SESSION_ROLE_KEY);
	}
	
	public static Org getSessionCurrentOrg(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (Org) session.getAttribute(SESSION_ORG_KEY);
	}
	
	@SuppressWarnings("unchecked")
	public static List<SysRole> getSessionRoles(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return  (List<SysRole>) session.getAttribute(SESSION_ROLES_KEY);
	}
	
	@SuppressWarnings("unchecked")
	public static List<Org> getSessionOrgs(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return  (List<Org>) session.getAttribute(SESSION_ORGS_KEY);
	}
	
	/**
	 * 获取客户端的真实IP地址
	 */
	public static String getIpAddress(HttpServletRequest request) {
		final String unknown = "unknown";
		String ip = request.getHeader("x-forwarded-for");
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
			ip = request.getHeader("Proxy-Client-IP");
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
			ip = request.getHeader("WL-Proxy-Client-IP");
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
			ip = request.getRemoteAddr();
		return ip;
	}
}
