package com.bw.adv.controller.sys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.sys.enums.SysSystemEnum;
import com.bw.adv.module.sys.model.OperationItem;
import com.bw.adv.module.sys.model.SysFunctionOperation;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysRoleFuncKey;
import com.bw.adv.module.sys.model.exp.RoleDataConfigExp;
import com.bw.adv.module.sys.model.exp.RoleDataRequest;
import com.bw.adv.module.sys.model.exp.SysFunctionExp;
import com.bw.adv.module.sys.model.exp.SysRoleExp;
import com.bw.adv.service.exception.SysException;
import com.bw.adv.service.sys.service.SysFunctionOperationService;
import com.bw.adv.service.sys.service.SysFunctionService;
import com.bw.adv.service.sys.service.SysRoleDataService;
import com.bw.adv.service.sys.service.SysRoleFuncService;
import com.bw.adv.service.sys.service.SysRoleService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Hello world!
 *
 */
@Controller
@RequestMapping("/sysRoleController")
@SuppressWarnings("unused")
public class SysRoleController {

    private SysRoleService sysRoleService;
    private SysFunctionService sysFunctionService;
    private SysRoleFuncService sysRoleFuncService;
    private SysFunctionOperationService sysFunctionOperationService;

    private SysRoleDataService sysRoleDataService;

    /**
     * showSysRole:获取角色列表. <br/>
     * Date: 2015-8-24 下午5:21:55 <br/>
     * scrmVersion 1.0
     *
     * @param request
     * @param response
     * @throws IOException
     * @author lulu.wang
     * @version jdk1.7
     */
    @RequestMapping(value = "/showSysRole", method = RequestMethod.GET)
    @ResponseBody
    public DataGridModel<SysRole> showSysRole(int page, int rows, HttpServletRequest request, HttpServletResponse response) throws IOException {
        DataGridModel<SysRole> result = new DataGridModel<SysRole>();
        try {
            Page<SysRole> pageObj = new Page<SysRole>();
            result.setRows(sysRoleService.findListForSysRole(StatusConstant.SYS_ROLE_ACTIVE.getCode(), pageObj));
            result.setTotal(pageObj.getTotalRecord());
            result.setStatusSuccess();
        } catch (Exception e) {
            result.setStatusError();
            e.printStackTrace();
        }
        return result;
    }

    /**
     * showSysRoleDetail:获取角色详情. <br/>
     * Date: 2015-8-24 下午5:22:53 <br/>
     * scrmVersion 1.0
     *
     * @param request
     * @param response
     * @throws IOException
     * @author lulu.wang
     * @version jdk1.7
     */
    @RequestMapping(value = "/showSysRoleDetail", method = RequestMethod.POST)
    public void showSysRoleDetail(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<SysFunctionExp> sysFunctionList = null;
        List<SysFunctionExp> returnFunction = null;
        List<OperationItem> operationItemList = null;
        SysRoleExp sysRoleExp = null;
        Long userId = null;
        List<SysRoleFuncKey> checkedRoleList = null;
        String json = null;
        String jsonData = null;
        String operationItemIds = null;
        String roleId = null;
        String systemIdStr = null;
        Long systemId = null;
        List<SysFunctionOperation> buttonList = null;
        SysFunctionOperation button = null;
        try {
            json = "";
            jsonData = "";
            roleId = request.getParameter("roleId");
            systemIdStr = request.getParameter("systemId");
            systemId = StringUtils.isNotBlank(systemIdStr)
                    && SysSystemEnum.AIR_ASSISANT.getSystemId().toString().equals(systemIdStr) ? SysSystemEnum.AIR_ASSISANT.getSystemId() : SysSystemEnum.AIR.getSystemId();
            sysFunctionList = sysFunctionService.selectSysFunctionListAndOperate(systemId);    //所有的功能list,无二级菜单
            returnFunction = sysFunctionService.selectSecondFunction(systemId);//一二级菜单
            for (SysFunctionExp functionFAndS : returnFunction) {
                buttonList = new ArrayList<SysFunctionOperation>();
                for (SysFunctionExp operateList : sysFunctionList) {
                    if (functionFAndS.getFunctionId().equals(operateList.getFunctionId())) {
                        button = new SysFunctionOperation();
                        button.setFunctionOperationId(operateList.getFunctionOperationId());
                        buttonList.add(button);
                    }
                }
                functionFAndS.setButtonList(buttonList);
            }
            if (StringUtils.isNotEmpty(roleId)) {
                checkedRoleList = sysRoleFuncService.queryCheckedSysRoleFuncLists(Long.parseLong(roleId)); //选中的操作list
                if (!CollectionUtils.isEmpty(checkedRoleList)) {
                    for (SysRoleFuncKey sysRoleFuncKey : checkedRoleList) {
                        for (SysFunctionExp functionOne : returnFunction) {
                            if (sysRoleFuncKey.getFunctionOperationId().equals(0L) && functionOne.getFunctionId().equals(sysRoleFuncKey.getFunctionId())) {
                                functionOne.setIsCheck("Y");
                            }
                            if (!sysRoleFuncKey.getFunctionOperationId().equals(0L) && functionOne.getFunctionId().equals(sysRoleFuncKey.getFunctionId())) {
                                for (SysFunctionOperation buttonsss : functionOne.getButtonList()) {
                                    if (buttonsss.getFunctionOperationId().equals(sysRoleFuncKey.getFunctionOperationId())) {
                                        buttonsss.setIsCheck("Y");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            json = JSONArray.fromObject(returnFunction).toString();
            json = json.replace("pareFunction", "_parentId");
            json = json.replace(",\"_parentId\":0", ",\"iconCls\":\"icon-ok\"");
            json = "{\"rows\":" + json + "}";
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().print(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/showSysRoleData", method = RequestMethod.POST)
    @ResponseBody
    public DataGridModel<RoleDataConfigExp> showSysRoleData(HttpServletRequest request, HttpServletResponse response) {
        DataGridModel<RoleDataConfigExp> result = new DataGridModel<RoleDataConfigExp>();
        try {
            String roleIdStr = request.getParameter("roleId");
            Long roleId = StringUtils.isNotBlank(roleIdStr) ? Long.valueOf(roleIdStr) : null;
            result.setRows(this.sysRoleDataService.getRoleDataConfig(roleId));
            result.setStatusSuccess();
        } catch (Exception e) {
            result.setStatusError();
            e.printStackTrace();
        }
        return result;
    }


    /**
     * saveSysRoleDetail:(保存角色信息及相关权限信息). <br/>
     * TODO(新建角色及编辑角色时调用).<br/>
     * Date: 2015-8-31 下午5:41:15 <br/>
     * scrmVersion 1.0
     *
     * @return
     * @throws IOException
     * @author lulu.wang
     * @version jdk1.7
     */
    @RequestMapping(value = "/saveSysRoleDetail", method = RequestMethod.POST)
    @ResponseBody
    public String saveSysRoleDetail(String str) throws IOException {
        String params = new String(str.getBytes("ISO-8859-1"), "UTF-8");
        String result = null;
        JSONObject json = null;
        try {
            json = JSONObject.fromObject(params);
            String roleName = json.getString("roleName");
            if (StringUtils.isBlank(roleName)) {
                result = "{\"code\":\"2\",\"message\":\"角色名不能为空!\"}";
                return result;
            }
            String roleIdStr = json.getString("roleId");
            SysRole sysRole = sysRoleService.findByRoleName(roleName);
            //判断新建时候角色名是否重复
            if (StringUtils.isBlank(roleIdStr) && sysRole != null) {
                result = "{\"code\":\"4\",\"message\":\"角色名已经存在!\"}";
                return result;
            }
            //判断编辑时候角色名是否重复
            if (StringUtils.isNotBlank(roleIdStr) && sysRole != null
                    && !roleIdStr.equals(sysRole.getRoleId().toString())) {
                result = "{\"code\":\"4\",\"message\":\"角色名已经存在!\"}";
                return result;
            }
            //保存功能权限
            Long roleId = sysRoleService.saveRuleFunction(json);
            //保存数据权限
            RoleDataRequest roleDataRequest = new RoleDataRequest();
            Map<Integer, List<String>> configMap = new HashMap<Integer, List<String>>();

            JSONArray jsonArray = JSONArray.fromObject(json.get("roleDataArray"));
            roleDataRequest.setRoleId(roleId);
            if (!jsonArray.isEmpty()) {
                for (Object object : jsonArray) {
                    JSONObject jsonObject = JSONObject.fromObject(object);
                    if (configMap.containsKey((int) jsonObject.getLong("dataTypeId"))) {
                        configMap.get((int) jsonObject.getLong("dataTypeId")).add(jsonObject.getString("dataItems"));
                    } else {
                        List<String> buttonUnSelect = new ArrayList<String>();
                        buttonUnSelect.add(jsonObject.getString("dataItems"));
                        configMap.put((int) jsonObject.getLong("dataTypeId"), buttonUnSelect);
                    }
                }
            }
            roleDataRequest.setConfigMap(configMap);
            this.sysRoleDataService.saveRoleDataConfig(roleDataRequest);
            result = "{\"code\":\"1\",\"message\":\"保存成功!\"}";
        } catch (Exception e) {
            result = "{\"code\":\"0\",\"message\":\"保存失败!\"}";
            e.printStackTrace();
        }

        return result;

    }


    /**
     * removeSysRole:删除角色信息.(软删除,更改USE_FLAG) <br/>
     * Date: 2015-8-31 下午5:43:01 <br/>
     * scrmVersion 1.0
     *
     * @param roleId
     * @return
     * @throws IOException
     * @author lulu.wang
     * @version jdk1.7
     */
    @RequestMapping(value = "/removeSysRole", method = RequestMethod.POST)
    @ResponseBody
    public JsonModel removeSysRole(Long roleId) throws IOException {
        JsonModel result = new JsonModel();
        try {
            sysRoleService.deleteSysRoleByRoleId(roleId);
            result.setStatusSuccess();
        } catch (SysException e) {
            result.setStatusError();
            result.setMessage(e.getMessage());
            e.printStackTrace();
        }catch (Exception e) {
            result.setStatusError();
            result.setMessage("删除失败");
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 清理缓存
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/clearCache", method = RequestMethod.POST)
    @ResponseBody
    public JsonModel clearCache(HttpServletRequest request, HttpServletResponse response) {
        JsonModel result = new JsonModel();
        try {
            this.sysRoleDataService.getNoCacheDataTypeMap();
            result.setStatusSuccess();
        } catch (Exception e) {
            result.setStatusError();
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 判定url是否有权限
     *
     * @param url
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/isAuth")
    @ResponseBody
    public JsonModel memberDetailAuth(String url) {
        JsonModel result = new JsonModel();
        Subject subject = SecurityUtils.getSubject();
        if (subject.isPermitted(url)) {
            result.setStatusSuccess();
        } else {
            result.setStatusError();
        }
        return result;
    }

    @Autowired
    public void setSysRoleService(SysRoleService sysRoleService) {
        this.sysRoleService = sysRoleService;
    }

    @Autowired
    public void setSysFunctionService(SysFunctionService sysFunctionService) {
        this.sysFunctionService = sysFunctionService;
    }

    @Autowired
    public void setSysRoleFuncService(SysRoleFuncService sysRoleFuncService) {
        this.sysRoleFuncService = sysRoleFuncService;
    }

    @Autowired
    public void setSysFunctionOperationService(
            SysFunctionOperationService sysFunctionOperationService) {
        this.sysFunctionOperationService = sysFunctionOperationService;
    }

    @Autowired
    public void setSysRoleDataService(SysRoleDataService sysRoleDataService) {
        this.sysRoleDataService = sysRoleDataService;
    }

}
