package com.bw.adv.controller.sys;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.base.model.ProductCategory;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.sys.model.SysFunctionOperation;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.service.base.service.ProductCategoryService;
import com.bw.adv.service.channel.service.ChannelService;
import com.bw.adv.service.member.service.GradeService;
import com.bw.adv.service.sys.service.SysFunctionOperationService;
import com.bw.adv.service.sys.service.SysUserService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


@Controller
@RequestMapping("/scrm/view")
public class PageController {

	private GradeService gradeService;
	private SysFunctionOperationService sysFunctionOperationService;
	private ProductCategoryService productCategoryService;
	private SysUserService sysUserService;
	private ChannelService channelService;

	static String statusString = "";
	static {
//		Map<Long, Status> statusMap = StatusInit.getStatusMap();
//		JSONObject obj = new JSONObject();
//		Status status = null;
//		for (Long key : statusMap.keySet()) {
//			status = statusMap.get(key);
//			obj.put(key, status.getStatusName());
//		}
//		statusString = obj.toString();
	}

	@RequestMapping(value = "/getContantsForJS", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showGradeForJS(){
		List<Grade> list = null;
		JSONObject object = null;
		Map<String, Object> result = null;
		JSONArray gradeJson = new JSONArray();
		try {
			result = new HashMap<String, Object>();
			list = gradeService.queryGradeListForJS();
			for (Grade grade : list) {
				object = new JSONObject();
				object.put("value", grade.getValue());
				object.put("text", grade.getText());
				gradeJson.add(object);
			}

			result.put("gradeResult", gradeJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/showFunctionButtonForJS", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showFunctionButtonForJS(){
		List<SysFunctionOperation> list = null;
		JSONObject object = null;
		Map<String, Object> result = null;
		JSONArray buttonJson = new JSONArray();
		try {
			result = new HashMap<String, Object>();
			list = sysFunctionOperationService.queryAllFunctionOperation();
			for (SysFunctionOperation operation : list) {
				object = new JSONObject();
				object.put("value", operation.getFunctionOperationId());
				object.put("text", operation.getOperationName());
				buttonJson.add(object);
			}
			result.put("buttonResult", buttonJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@RequestMapping(value = "/showMemberLevelForJS", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showMemberLevelForJS(){
		List<Channel> list = null;
		JSONObject object = null;
		Map<String, Object> result = null;
		JSONArray levelJson = new JSONArray();
		try {
			result = new HashMap<String, Object>();
			list = channelService.findListByChannelName(null, null);
			for (Channel operation : list) {
				object = new JSONObject();
				object.put("value", operation.getChannelId());
				object.put("text", operation.getChannelName());
				levelJson.add(object);
			}
			result.put("levelResult", levelJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@RequestMapping(value = "/showProductSortForJS", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showProductSortForJS(){
		List<ProductCategory> list = null;
		JSONObject object = null;
		Map<String, Object> result = null;
		JSONArray sortList = new JSONArray();
		try {
			result = new HashMap<String, Object>();
			list = productCategoryService.queryAll();
			for (ProductCategory productCategory : list) {
				object = new JSONObject();
				object.put("value", productCategory.getProductCategoryId());
				object.put("label", productCategory.getProductCategoryName());
				sortList.add(object);
			}
			result.put("sortList", sortList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "/showRoleListForJS", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showRoleListForJS(){
		List<SysRole> roleList = null;
		JSONObject object = null;
		Map<String, Object> result = null;
		JSONArray buttonJson = new JSONArray();
		try {
			result = new HashMap<String, Object>();
			roleList = sysUserService.querySysRole();
			for (SysRole operation : roleList) {
				object = new JSONObject();
				object.put("label", operation.getRoleName());
				object.put("value", operation.getRoleId());
				buttonJson.add(object);
			}
			result.put("buttonResult", buttonJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	@RequestMapping(value = "/index.htm", method = RequestMethod.GET)
	public String showIndex(HttpServletRequest request, HttpServletResponse response) {
		return "index";
	}

	@Autowired
	public void setGradeService(GradeService gradeService) {
		this.gradeService = gradeService;
	}

	@Autowired
	public void setSysFunctionOperationService(SysFunctionOperationService sysFunctionOperationService) {
		this.sysFunctionOperationService = sysFunctionOperationService;
	}
	
	@Autowired
	public void setProductCategoryService(ProductCategoryService productCategoryService) {
		this.productCategoryService = productCategoryService;
	}

	@Autowired
	public void setSysUserService(SysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	@Autowired
	public void setChannelService(ChannelService channelService) {
		this.channelService = channelService;
	}


}
