package com.bw.adv.controller.points;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.rule.init.PointsRuleInit;
import com.bw.adv.module.points.constant.PointsTypeConstant;
import com.bw.adv.module.points.model.PointsRule;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.points.service.PointsRuleSevice;
  
 
@Controller
@RequestMapping("/pointsRuleController")
@SuppressWarnings("unused")
public class PointsRuleController {

	private PointsRuleSevice pointsRuleSevice;
	private PointsRuleInit pointsRuleInit;
 
	/**
	 * showPointsRule:查询积分规则列表. <br/>
	 * Date: 2015-9-2 上午10:12:02 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/showPointsRule", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showPointsRule(int page,int rows) throws IOException{
		List<PointsRule> pointsRuleList=null;
		Page<PointsRule> pageObj = null;
		List<PointsRule> sysRoleList = null;
		Map<String, Object> map = null;
		
		try {
			map = new HashMap<String, Object>();
			pageObj = new Page<PointsRule>();
			
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			pointsRuleList=pointsRuleSevice.queryPointsRuleList(StatusConstant.POINTS_RULE_ACTIVE.getCode(),pageObj);

			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}	

	/**
	 * pointsRuleDetail:(查看积分规则明细). <br/>
	 * Date: 2015-9-2 上午10:12:30 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param pointsRuleId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/pointsRuleDetail", method = RequestMethod.GET)
	@ResponseBody
	public PointsRule pointsRuleDetail(Long pointsRuleId) throws IOException{
		PointsRule pointsRule=null;
		try {		
			pointsRule=pointsRuleSevice.queryByPk(pointsRuleId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pointsRule;
	}	
	
	/**
	 * savePointsRule:保存积分规则(新建及编辑). <br/>
	 * Date: 2015-9-2 上午10:12:58 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/savePointsRule", method = RequestMethod.POST)
	@ResponseBody
	public String savePointsRule(HttpServletRequest request,HttpServletResponse response) throws IOException{
		String result = null;
		PointsRule pointsRule=null;
		List<PointsRule> pointsRuleList=null;
		
		try {	
			pointsRuleList=new ArrayList<PointsRule>();
			pointsRule=new PointsRule();
			
			pointsRule.setPointsTypeId(Long.parseLong(request.getParameter("pointsTypeId")));
			pointsRule.setPointsRuleCode(request.getParameter("PointsRuleCode"));
			pointsRule.setPointsRuleName(request.getParameter("pointsRuleName"));
			pointsRule.setPointsRuleDesc(request.getParameter("pointsRuleDesc"));
			pointsRule.setPointsRuleType(request.getParameter("pointsRuleType"));
			pointsRule.setPointsAmount(BigDecimal.valueOf(Double.parseDouble(request.getParameter("pointsAmount"))));
			pointsRule.setInvalidTimeType(request.getParameter("invalidTimeType"));
			pointsRule.setInvalidTimeValue(request.getParameter("invalidTimeValue"));
			pointsRule.setCleanTime(request.getParameter("cleanTime"));
			pointsRule.setIsActive(StatusConstant.POINTS_RULE_ACTIVE.getCode());
			
			pointsRuleList=pointsRuleSevice.queryByPointsRuleName(pointsRule.getIsActive(), pointsRule.getPointsRuleName());
			if(null!=pointsRuleList&&pointsRuleList.size()>0){
				Boolean b=false;
				if(StringUtils.isEmpty(request.getParameter("pointsRuleId"))){//新建时 
					b=true;
				}else{//编辑时
					for(PointsRule p:pointsRuleList){
						if(p.getPointsRuleId()!=Long.parseLong(request.getParameter("pointsRuleId"))){
							b=true;
						}
					}
				}
				if(b){
					result = "{\"code\":\"3\",\"message\":\"规则名称已存在!\"}";
					return result;
				}
			}
			
			
			if(StringUtils.isEmpty(request.getParameter("pointsRuleId"))){
				pointsRule.setCreateTime(DateUtils.getCurrentTimeOfDb());
				pointsRuleSevice.saveSelective(pointsRule);
			}else{
				pointsRule.setUpdateTime(DateUtils.getCurrentTimeOfDb());
				pointsRule.setPointsRuleId(Long.parseLong(request.getParameter("pointsRuleId")));
				pointsRuleSevice.updateByPkSelective(pointsRule);
			}
			pointsRuleInit.reLoad();
			result = "{\"code\":\"1\",\"message\":\"保存成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"保存失败!\"}";
			e.printStackTrace();
		}
		return result;
	}	
	
	/**
	 * removePointsRule:删除积分规则(软删除). <br/>
	 * Date: 2015-9-2 上午10:13:52 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param pointsRuleId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/removePointsRule", method = RequestMethod.POST)
	@ResponseBody
	public String removePointsRule(Long pointsRuleId) throws IOException{
		String result = null;
		try {	
			pointsRuleSevice.deletePointsRuleByPointsRuleId(pointsRuleId);
			result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
			pointsRuleInit.reLoad();
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"删除失败!\"}";
			e.printStackTrace();
		}
		return result;
	}		
	

	@Autowired
	public void setPointsRuleSevice(PointsRuleSevice pointsRuleSevice) {
		this.pointsRuleSevice = pointsRuleSevice;
	}

	@Autowired
	public void setPointsRuleInit(PointsRuleInit pointsRuleInit) {
		this.pointsRuleInit = pointsRuleInit;
	}	
	
}
