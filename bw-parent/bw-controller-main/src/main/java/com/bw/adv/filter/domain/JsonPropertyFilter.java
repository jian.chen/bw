package com.bw.adv.filter.domain;

import com.alibaba.fastjson.serializer.PropertyFilter;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public class JsonPropertyFilter implements PropertyFilter,Serializable{

	private static final long serialVersionUID = -9015506442735312823L;
	
	private Set<String> unDisplayField;

    public JsonPropertyFilter(Set<String> unDisplayField) {
        this.unDisplayField = unDisplayField;
    }

    @Override
    public boolean apply(Object source, String name, Object value) {
        if(unDisplayField.contains(name))
            return false;
        return true;
    }
}
