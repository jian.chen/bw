package com.bw.adv.filter;

import org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Created by jeoy.zhou on 12/30/15.
 */
public class URLPermissionsFilter extends PermissionsAuthorizationFilter {

    @Override
    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws IOException {
        return super.isAccessAllowed(request, response, getPath(request));
    }

    protected String[] getPath(ServletRequest request) {
        return new String[]{WebUtils.getPathWithinApplication(WebUtils.toHttp(request))};
    }
}
