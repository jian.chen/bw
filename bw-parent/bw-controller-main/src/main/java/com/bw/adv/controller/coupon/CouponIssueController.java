package com.bw.adv.controller.coupon;

import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.sage.scrm.module.coupon.model.exp.ExternalCoupon;












import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.core.utils.MD5Utils;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.module.coupon.model.*;
import com.bw.adv.module.coupon.model.exp.CouponExp;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.report.model.MemberAge;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelExport;
import com.bw.adv.module.tools.ExcelImport;
import com.bw.adv.service.coupon.service.CouponInstanceService;
import com.bw.adv.service.coupon.service.CouponIssueService;
import com.bw.adv.service.coupon.service.CouponService;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.sys.service.SysUserService;
import com.google.gson.JsonObject;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartRequest;

/**
 * ClassName: CouponIssueController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月25日 下午2:41:06 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Controller
@RequestMapping("/couponIssueController")
@SuppressWarnings("unused")
public class CouponIssueController {
	
	private CouponIssueService couponIssueService;
	
	private MemberService memberService;
	
	private CouponInstanceService couponInstanceService;
	
	private CouponService couponService;
	private Logger logger = Logger.getLogger(CouponIssueController.class);
	@RequestMapping(value = "/editCoupon", method = RequestMethod.POST)
	@ResponseBody
	public String editCoupon(Long couponId){
		String result = null;
		Coupon coupon = null;
		try {
			coupon = couponService.queryByPk(couponId);
			coupon.setStatusId(StatusConstant.COUPON_ISSUEDING.getId());
			couponService.updateByPkSelective(coupon);
			result = "{\"code\":\"1\",\"message\":\"修改成功!\"}";
		} catch (Exception e) {
			e.printStackTrace();
			result = "{\"code\":\"0\",\"message\":\"修改失败!\"}";
		}
		return result;//返回值待定义
	}
	
	/**
	 * saveCouponIssues:优惠券发布 <br/>
	 * Date: 2015年8月25日 下午3:09:20 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponId
	 * @param sourceId
	 * @param couponIssueNum
	 * @param memberIds
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/saveCouponIssues", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel saveCouponIssues(Long couponId,Long couponIssueNum, String memberIds,int personalAmount,String search, Integer isSendTemplate) throws Exception{
		JsonModel result = null;
		String memberGroupIds = null;
		String memberTagIds = null;
		String orgIds = null;
		try {
			result = new JsonModel();
			JSONObject object = JSONObject.parseObject(search);
			MemberSearch memberSearch = JSONObject.toJavaObject(object, MemberSearch.class);
			memberGroupIds = memberSearch.getMemberGroupIds();
			memberTagIds = memberSearch.getMemberTagIds();
			orgIds = memberSearch.getEqualOrgId();
			
			memberSearch.setGreaterEqualBirthday(DateUtils.formatString(memberSearch.getGreaterEqualBirthday()));
			if(StringUtils.isNotBlank(memberSearch.getLessEqualBirthday())){
				memberSearch.setLessEqualBirthday(DateUtils.formatString(memberSearch.getLessEqualBirthday()+"235959"));
			}
			memberSearch.setGreaterEqualRegisterTime(DateUtils.formatString(memberSearch.getGreaterEqualRegisterTime()));
			if(StringUtils.isNotBlank(memberSearch.getLessEqualRegisterTime())){
				memberSearch.setLessEqualRegisterTime(DateUtils.formatString(memberSearch.getLessEqualRegisterTime()+"235959"));
			}
			
			if(StringUtils.isNotBlank(memberGroupIds)){
				memberSearch.setInMemberGroupIds(memberGroupIds.split(","));
			}
			if(StringUtils.isNotBlank(memberTagIds)){
				memberSearch.setInMemberTagIds(memberTagIds.split(","));
			}
			if(StringUtils.isNotBlank(orgIds)){
				memberSearch.setInOrgId(orgIds.split(","));
			}
			//memberSearch.setNotEqualIsEmployee("Y");
			result = this.webIssueCoupon(couponId, couponIssueNum, memberIds,personalAmount, memberSearch, isSendTemplate == null ? 1 : isSendTemplate);
		} catch (Exception e) {
			result.setStatusError();
			result.setMessage("发放失败!");
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 后台手动下发优惠券功能整合
	 * webIssueCoupon: <br/>
	 * Date: 2016年5月11日 下午4:05:07 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param couponId
	 * @param couponIssueNum
	 * @param memberIds
	 * @param personalAmount
	 * @param search
	 * @return
	 */
	private JsonModel webIssueCoupon(Long couponId,Long couponIssueNum, String memberIds,int personalAmount,MemberSearch search, int sendTemp){
		JsonModel result = null;
		//1.修改优惠券状态
		Coupon coupon = null;
		coupon = couponService.queryByPk(couponId);
		coupon.setStatusId(StatusConstant.COUPON_ISSUEDING.getId());
		couponService.updateByPkSelective(coupon);
		try {
			result = new JsonModel();
			//2.下发优惠券
			couponIssueService.insertCouponIssue(couponId, ChannelConstant.GET_COUPON_SCRM.getId(), couponIssueNum, memberIds,personalAmount,search, sendTemp);
			result.setStatusSuccess();
			result.setMessage("发放成功!");
		} catch (Exception e) {
			coupon.setStatusId(StatusConstant.COUPON_ISSUED.getId());
			couponService.updateByPkSelective(coupon);
			result.setStatusError();
			result.setMessage("发放失败!");
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * showChannel:查询渠道列表 <br/>
	 * Date: 2015年8月25日 下午3:09:36 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(value = "/showChannel", method = RequestMethod.GET)
	@ResponseBody
	public List<Channel> showChannel(){
		List<Channel> result = null;
		
		try {
			result = couponIssueService.queryChannelList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * showMemberCount:查询会员数量 <br/>
	 * Date: 2015年8月25日 下午4:08:42 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(value = "/showMemberCount", method = RequestMethod.GET)
	@ResponseBody
	public Long showMemberCount(){
		Long memberCount = null;
		try {
			memberCount = memberService.queryActiveMemberCount();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberCount;
	}
	/**
	 * showCouponIssueList:根据优惠券ID查询优惠券发布数量 <br/>
	 * Date: 2015年8月25日 下午6:39:04 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponId
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/showCouponIssueList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridModel<CouponIssue> showCouponIssueList(Long couponId,int page,int rows){
		Page<CouponIssue> pageObj = null;
		List<CouponIssue> list = null;
		DataGridModel<CouponIssue> result = null;
		Long count = null;
		
		try {
			pageObj = new Page<CouponIssue>();  
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			list = couponIssueService.queryList(couponId,pageObj);
//			count = (long)couponIssueService.queryCountByCouponId(couponId);
			count = pageObj.getTotalRecord();
			result = new DataGridModel<CouponIssue>(list, count);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * showCouponInstance:根据条件查询优惠券实例 <br/>
	 * Date: 2015年8月25日 下午9:10:48 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param Map
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/showCouponInstance", method = RequestMethod.GET)
	@ResponseBody
	public DataGridModel<CouponInstanceExp> showCouponInstance(CouponInstanceSearch couponInstanceSearch){
		Page<CouponInstanceExp> pageObj = null;
		List<CouponInstanceExp> list = null;
		DataGridModel<CouponInstanceExp> result = null;
		Long count = null;
		Coupon coupon = null;
		
		try {
			coupon = new Coupon();
			pageObj = new Page<CouponInstanceExp>();  
			pageObj.setPageNo(couponInstanceSearch.getPage());
			pageObj.setPageSize(couponInstanceSearch.getRows());
			list = couponInstanceService.queryListByDyc(couponInstanceSearch,pageObj);
			count = pageObj.getTotalRecord();
			result = new DataGridModel<CouponInstanceExp>(list, count);
			coupon = couponService.queryByPk(couponInstanceSearch.getCouponId());
			result.setObj(coupon);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "/leadToExcelCouponInstance", method = RequestMethod.GET)
	public void leadToExcelCouponInstance(@RequestParam("memberMobile")String memberMobile,@RequestParam("couponInstanceCode")String couponInstanceCode
			,@RequestParam("issueStartTime")String issueStartTime,@RequestParam("issueEndTime")String issueEndTime
			,@RequestParam("couponId")Long couponId,@RequestParam("statusId")Long statusId,@RequestParam("issueStatus")String issueStatus
			,HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		CouponInstanceSearch couponInstanceSearch = null;
		List<CouponInstanceExp> list = null;
		try {
			couponInstanceSearch = new CouponInstanceSearch();
			couponInstanceSearch.setMemberMobile(memberMobile);
			couponInstanceSearch.setCouponInstanceCode(couponInstanceCode);
			couponInstanceSearch.setIssueStartTime(issueStartTime);
			couponInstanceSearch.setIssueEndTime(issueEndTime);
			couponInstanceSearch.setCouponId(couponId);
			couponInstanceSearch.setStatusId(statusId);
			couponInstanceSearch.setIssueStatus(issueStatus);
			String diposition="attachment;filename="+URLEncoder.encode("优惠券明细记录.xlsx", "utf-8");
			response.setContentType("application/xlsx;charset=UTF-8");
			response.setHeader("Content-disposition", diposition);
			// excel的sheetName
			String sheetName = "优惠券明细记录";
			// excel要导出的数据
			list = couponInstanceService.queryListByDyc(couponInstanceSearch);
			// 导出
			if (list == null || list.size() == 0) {
				return;
			}else {
				OutputStream out = new FileOutputStream(diposition);
				ExcelExport.exportExcel(sheetName, CouponInstanceExp.class, list, response.getOutputStream());
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/queryIsIssueIng", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel queryIsIssueIng(){
		JsonModel result = null;
		try {
			result = new JsonModel();
			Long IssueCount = this.couponIssueService.queryCouponIsIssueing();
			result.setStatusSuccess();
			result.setMessage(IssueCount.toString());
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatusError();
		}
		return result;
	}
	
	/**
	 * editCouponInstance:优惠券核销 <br/>
	 * Date: 2015年8月27日 下午12:15:31 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param instanceCode
	 * @return
	 */
	@RequestMapping(value = "/editCouponInstance", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> editCouponInstance(String instanceCode,String action){
		CouponInstance couponInstance = null;
		JsonModel result = null;
		Coupon coupon = null;
		Map<String,Object> resultMap = new HashMap<String, Object>();
		try {
			result = new JsonModel();
			if(StringUtils.isBlank(action)){
				resultMap.put("result", "异常操作!");
			}else{
				//修改优惠券实例
				couponInstance = couponInstanceService.queryCoupontByCode(instanceCode);
				coupon = couponService.queryByPk(couponInstance.getCouponId());
				if(action.equals(StatusConstant.COUPON_INSTANCE_USED.getId().toString())){
					result = couponService.verificationCoupon(couponInstance,StatusConstant.COUPON_INSTANCE_USED.getId());
				}else if(action.equals(StatusConstant.COUPON_INSTANCE_UNUSED.getId().toString())){
					result = couponService.verificationCoupon(couponInstance,StatusConstant.COUPON_INSTANCE_UNUSED.getId());
				}else{
					result.setMessage("异常操作");
				}
				
				resultMap.put("status", result.getStatus());
				resultMap.put("result", result.getMessage());
				resultMap.put("coupon", coupon);
				resultMap.put("couponInstance", couponInstance);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultMap;
	}
	/**
	 * 导入外部券码
	 * @return
	 */
	@RequestMapping(value = "/importExternalCoupon", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel importExternalCoupon(MultipartHttpServletRequest request, Long couponId){
		JsonModel result = new JsonModel();
		MultipartFile file = null;
		File fileExcel = null;
		ExcelImport ei = null;
		String filePath = null;
		List<ExternalCoupon> ecs = null;
		Coupon coupon = null;
		int ptPos = -1;
		String fl = "";
		try{
			file = request.getFile("file");
			ei = new ExcelImport();
			ptPos = file.getOriginalFilename().lastIndexOf(".");
			if (ptPos > 0){
				fl = MD5Utils.getMD5String(file.getOriginalFilename().substring(0, ptPos) + System.currentTimeMillis()) +
				    file.getOriginalFilename().substring(ptPos);
			} else {
				fl = MD5Utils.getMD5String(file.getOriginalFilename() + System.currentTimeMillis());
			}
			fileExcel = new File(request.getSession().getServletContext().getRealPath("/") + "/upload/"
                     + fl);
			file.transferTo(fileExcel);
			ecs = (ArrayList)ei.importExcel(fileExcel, ExternalCoupon.class);
			if (ecs != null && ecs.size() > 0) {
                couponIssueService.insertExternalCoupon(couponId, ecs, result);
				result.setStatusSuccess();
				result.setMessage("导入成功");
			} else {
				result.setStatusError();
				result.setMessage("请上传有效的Excel文件！");
			}


		} catch (Exception e){
			result.setMessage("内部异常！");
			result.setStatusError();

		}
		return  result;
	}
	/**
	 * 下载外部券模板
	 * @return
	 */
	@RequestMapping(value = "/downloadExternalCouponTmpl.do")
	public void downloadExternalCouponTmpl(HttpServletRequest request, HttpServletResponse response) {
		String filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/礼品券模板.xls" ;
        try{
			FileUtils.download("礼品券模板.xls", filePath, response);
        } catch (Exception e){
			logger.error("下载礼品券模板失败", e);
		}
	}
	@Autowired
	public void setCouponIssueService(CouponIssueService couponIssueService) {
		this.couponIssueService = couponIssueService;
	}

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	@Autowired
	public void setCouponInstanceService(CouponInstanceService couponInstanceService) {
		this.couponInstanceService = couponInstanceService;
	}

	@Autowired
	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}
	

}
