package com.bw.adv.controller.common;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.bw.adv.module.tools.FileUtil;

@Controller
@RequestMapping("/file")
public class FileUpAndDownController {
	
	
	/**
	 * 图片上传
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/upload.json", method  = RequestMethod.POST)
	public ModelAndView fileUpload(HttpServletRequest request) throws IOException{
		Map<String,String> result = new HashMap<String, String>();
		String fileurl = null;
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request; 
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap(); 
		String fileName = null;
		String newName = null;
		MultipartFile mf = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {   
			mf = entity.getValue();
			fileName = mf.getOriginalFilename(); 
			if(!fileName.isEmpty()){
				 newName = FileUtil.generateTmpFileName(fileName);
				 fileurl =  "upload/" + newName;
				 FileCopyUtils.copy(mf.getBytes(), new File(fileurl));
			 }
			 
		}
		result.put("src", newName);
		return new ModelAndView("", result);
	}
	
	@RequestMapping(value = "/downloadTmp.json", method  = RequestMethod.GET)
	public void fileDownloadTmp(String filePath, HttpServletRequest request, HttpServletResponse response) throws IOException{
		filePath = "upload/" + filePath;
		
		String fileName = FileUtil.getPathFileName(filePath);
		FileUtil.downloadFile(request, response, filePath, fileName);
	}
	
	
	@RequestMapping(value = "/downloadFinal.json", method  = RequestMethod.GET)
	public void fileDownloadFinal(String filePath, HttpServletRequest request, HttpServletResponse response) throws IOException{
		filePath = new String(filePath.getBytes("iso8859-1"),"UTF-8");
		//filePath = FINAL_UPLOAD_DIR + "/" + filePath;
		filePath = request.getSession().getServletContext().getRealPath(filePath);
		System.out.println("filePath:"+filePath);
		String fileName = FileUtil.getPathFileName(filePath);
		try{
			FileUtil.downloadFile(request, response, filePath, fileName);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/downloadTemplate.json" , method = RequestMethod.GET)
	public void fileDownloadTemplate(String filePath, HttpServletRequest request, HttpServletResponse response) throws IOException{
		filePath = "upload";
		String[] pathSub = filePath.split("/");
		String fileName = pathSub[pathSub.length-1];
		try{
			FileUtil.downloadFile(request, response, filePath, fileName);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

}
