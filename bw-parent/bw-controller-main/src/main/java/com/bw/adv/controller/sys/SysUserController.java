package com.bw.adv.controller.sys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.MD5Utils;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.report.model.IndexChartShowInstance;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.SysUserRoleKey;
import com.bw.adv.module.sys.model.exp.SysUserExp;
import com.bw.adv.module.sys.model.exp.SysUserView;
import com.bw.adv.service.base.service.OrgService;
import com.bw.adv.service.report.service.IndexChartShowInstanceService;
import com.bw.adv.service.sys.service.SysUserService;

/**
 * Hello world!
 * 
 */
@Controller
@RequestMapping("/sysUserController")
public class SysUserController {
	
	private SysUserService sysUserService;
	private OrgService orgService;
	
	private IndexChartShowInstanceService indexChartShowInstanceService;
	
	/**
	 * 
	 * showSysUserList:查询启用用户列表
	 * Date: 2015年8月25日 下午2:23:58 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showSysUserList.do")
	@ResponseBody
	public DataGridModel<SysUser> showSysUserList(int page,int rows){
		DataGridModel<SysUser> result = new DataGridModel<SysUser>();
		try {
			Page<SysUser> pageObj = new Page<SysUser>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result.setRows(sysUserService.queryAllByPage(pageObj));
			result.setTotal(pageObj.getTotalRecord());
			result.setStatusSuccess();
		} catch (Exception e) {
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * getUserNameList:获取所有用户名（用于比对用户名不能重复）
	 * Date: 2015年9月14日 上午10:56:00 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "getUserNameList.do")
	@ResponseBody
	public List<String> getUserNameList(){
		List<SysUser> sysUserList = null;
		List<String> nameList = null;
		try {
			nameList = new ArrayList<String>();
			sysUserList = sysUserService.queryUserNames();
			for(int i=0;i<sysUserList.size();i++){
				nameList.add(sysUserList.get(i).getUserName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nameList;
	}
	
	/**
	 * 
	 * showSysUserById:查看用户详情
	 * Date: 2015年8月25日 下午5:01:44 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUserId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showSysUserById")
	@ResponseBody
	public JsonModel showSysUserById(Long sysUserId){
		JsonModel jsonModel = new JsonModel();
		SysUserView sysUserView = new SysUserView();
		try {
			jsonModel.setObj(sysUserView);
			sysUserView.setSysUser(sysUserService.queryByPk(sysUserId));
//			SysUserRoleKey sur = sysUserService.querySysUserRoleByUserId(sysUserView.getSysUser().getUserId());
			List<SysUserRoleKey> roleKeyList = sysUserService.querySysUserRoleListByUserId(sysUserView.getSysUser().getUserId());
//			if(sur != null){
//				sysUserView.setSysRole(sysUserService.querySysRoleByPk(sur.getRoleId()));
//			}
//			sysUserView.setSysRoles(sysUserService.querySysRole());
			List<Long> roleIds = new ArrayList<Long>();
			if (CollectionUtils.isNotEmpty(roleKeyList)) {
				for (SysUserRoleKey sysUserRoleKey : roleKeyList) {
					roleIds.add(sysUserRoleKey.getRoleId());
				}
			}
			sysUserView.setRoleIds(roleIds);
			if (sysUserView.getSysUser().getOrgId() != null) {
				sysUserView.setOrg(orgService.queryOrg(sysUserView.getSysUser().getOrgId()));
			}
			jsonModel.setStatusSuccess();
		} catch (Exception e) {
			jsonModel.setStatusError();
			e.printStackTrace();
		}
		return jsonModel;
	}
	
	/**
	 * 
	 * editSysUser:修改用户
	 * Date: 2015年8月25日 下午5:03:33 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUserexp
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editSysUser")
	@ResponseBody
	public JsonModel editSysUser(SysUserExp sysUserexp){
		JsonModel jsonModel = new JsonModel();
		try {
			sysUserexp.setPassword(null);//不修改密码
			sysUserService.updateUserForRole(sysUserexp, sysUserexp.getSelectRoleIds());
            jsonModel.setStatusSuccess();
		} catch (Exception e) {
			jsonModel.setStatusError();
			e.printStackTrace();
		}
		return jsonModel;
	}
	
	/**
	 * 
	 * saveSysUser:保存新建用户
	 * Date: 2015年8月25日 下午5:05:00 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUserexp
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveSysUser")
	@ResponseBody
	public JsonModel saveSysUser(SysUserExp sysUserexp){
		JsonModel jsonModel = new JsonModel();
		try {
			sysUserexp.setPassword(MD5Utils.getMD5String(sysUserexp.getPassword()));
			sysUserService.saveUserForRole(sysUserexp, sysUserexp.getSelectRoleIds());
            jsonModel.setStatusSuccess();
		} catch (DuplicateKeyException e) {
			jsonModel.setStatusError();
			jsonModel.setMessage("用户名重复");
			e.printStackTrace();
		} catch (Exception e) {
			jsonModel.setStatusError();
			jsonModel.setMessage("新增用户失败");
			e.printStackTrace();
		}
		return jsonModel;
	}
	
	/**
	 * 
	 * showSysRoleList:查询可用角色
	 * Date: 2015年8月28日 下午5:22:18 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showSysRoleList")
	@ResponseBody
	public List<SysRole> showSysRoleList(){
		List<SysRole> result = null;
		try {
			result = sysUserService.querySysRole();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * removeSysUser:批量启用/停用用户(逻辑删除)
	 * Date: 2015年8月25日 下午5:43:43 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUserIds
	 * @param type
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "changeStatus.do")
	@ResponseBody
	public JsonModel removeSysUser(String sysUserIds, Long type) {
		JsonModel jsonModel = new JsonModel();
		if (StringUtils.isBlank(sysUserIds)) {
			jsonModel.setStatusError();
			jsonModel.setMessage("请选择系统操作用户");
			return jsonModel;
		}
		if (type == null ||
				(!StatusConstant.SYS_USER_ACTIVE.getId().equals(type) && !StatusConstant.SYS_USER_REMOVE.getId().equals(type))) {
			jsonModel.setStatusError();
			jsonModel.setMessage("处理状态不正确");
			return jsonModel;
		}
		List<Long> sysUserIdList = new ArrayList<Long>();
		String[] ids = null;
		try {
			ids = sysUserIds.split(",");
			for (String idStr : ids) {
				sysUserIdList.add(Long.valueOf(idStr));
			}
			if (sysUserService.changeSysUserStatus(sysUserIdList,type)) {
				jsonModel.setStatusSuccess();
			}
		} catch (Exception e) {
			jsonModel.setStatusError();
			e.printStackTrace();
		}
		return jsonModel;
	}
	
	@RequestMapping(value="/exportMemberCard", method = RequestMethod.GET)
	public void exportOrderMeal(HttpServletRequest request, HttpServletResponse response, String params) throws IOException{
		System.out.println("***********"+sysUserService);
		response.getWriter().write("ok");
	}

	/**
	 * updatePassword:(用户修改密码). <br/>
	 * Date: 2015-10-14 下午4:05:20 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePassword.do", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel updatePassword(HttpServletRequest request, HttpServletResponse response) throws Exception{
		JsonModel jsonModel = new JsonModel();
		try {
			String newpwd = request.getParameter("newpwd");
			String oldpwd = request.getParameter("oldpwd");
			SysUser sysUser = (SysUser)request.getSession().getAttribute(SysUserUtil.SESSION_USER_KEY);
			String sessionPwd = sysUser.getPassword();
			if(sessionPwd.equals(MD5Utils.getMD5String(oldpwd))){
				sysUser.setPassword(MD5Utils.getMD5String(newpwd));
				sysUserService.updateByPkSelective(sysUser);
                jsonModel.setStatusSuccess();
                request.getSession().setAttribute(SysUserUtil.SESSION_USER_KEY, sysUser);
			}else{
				jsonModel.setStatusError();
				jsonModel.setMessage("原密码不正确");
			}
		} catch (Exception e) {
			jsonModel.setStatusError();
			jsonModel.setMessage("操作出错");
			e.printStackTrace();
		}
		return jsonModel;
	}
	
	/**
	 * 更新用户首页仪表盘选项
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateUserChart", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> updateUserChart(HttpServletRequest request, HttpServletResponse response,String selectList){
		Map<String, Object> map = null;
		IndexChartShowInstance instance = null;
		List<IndexChartShowInstance> instanceList = null;
		map = new HashMap<String, Object>();
		JSONArray jsonArray = JSONArray.parseArray(selectList);
		Long[] arr=new Long[jsonArray.size()];  
		for(int i=0;i<jsonArray.size();i++){  
			arr[i]=jsonArray.getLong(i);  
		}  
		try {
			SysUser sysUser = (SysUser)request.getSession().getAttribute(SysUserUtil.SESSION_USER_KEY);
			instanceList = new ArrayList<IndexChartShowInstance>();
			for (Long chartId : arr) {
				instance = new IndexChartShowInstance();
				instance.setUserId(sysUser.getUserId());
				instance.setChartId(chartId);
				instanceList.add(instance);
			}
			indexChartShowInstanceService.updateIndexCharts(instanceList, sysUser.getUserId());
			map.put("result", "修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "更新失败");
		}
		return map;
	}
	
	/**
	 * 查询首页仪表盘设置
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/showIndexCharts", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showIndexCharts(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> map = null;
		List<IndexChartShowInstance> instanceList = null;
		try {
			map = new HashMap<String, Object>();
			SysUser sysUser = (SysUser)request.getSession().getAttribute(SysUserUtil.SESSION_USER_KEY);
			instanceList = indexChartShowInstanceService.findUserIndexByUserId(sysUser.getUserId());
			map.put("result", instanceList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	@Autowired
	public void setIndexChartShowInstanceService(IndexChartShowInstanceService indexChartShowInstanceService) {
		this.indexChartShowInstanceService = indexChartShowInstanceService;
	}

	@Autowired
	public void setAdminService(SysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}

	@Autowired
	public void setOrgService(OrgService orgService) {
		this.orgService = orgService;
	}
}
