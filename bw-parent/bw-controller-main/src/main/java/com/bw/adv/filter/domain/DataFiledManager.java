package com.bw.adv.filter.domain;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public final class DataFiledManager {

    private static final Logger LOGGER = Logger.getLogger(DataFiledManager.class);

    public static boolean useDefault = false;
    public static final String DEFAULT_FILTER = "default-filter";
    @Value("${datafilter.useDefault}")
    private String datafilter;
    @PostConstruct
    private void init() {
        if(StringUtils.isNotBlank(datafilter) && "true".equals(datafilter)) {
            useDefault = true;
        }
    }
}
