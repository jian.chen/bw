/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:WechatAccountController.java
 * Package Name:com.sage.scrm.controller.sys
 * Date:2015年8月22日下午2:30:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.sys;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.wechat.common.enums.WechatAccountConfigStatus;
import com.bw.adv.module.wechat.model.exp.WechatAccountExp;
import com.bw.adv.service.sys.service.WechatAccountService;
import com.bw.adv.service.wechat.msg.view.WechatConfigView;

/**
 * ClassName:WechatAccountController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月22日 下午2:30:24 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/wechatAccountController")
public class WechatAccountController {
	
	private WechatAccountService wechatAccountService;
	
	@RequestMapping(method = RequestMethod.GET, value = "showWechatAccount.do")
	@ResponseBody
	public JsonModel showWechatAccount(){
		return getWechatAccount();
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "saveWechatAccount")
	@ResponseBody
	public Integer saveWechatAccount(WechatConfigView configView){
		try {
			return wechatAccountService.updateWechatAccountAndConfig(configView);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "testWechatConnect")
	@ResponseBody
	public JsonModel testWechatConnect() {
		JsonModel jsonModel = new JsonModel();
		try {
			if (wechatAccountService.testWechatConnect()) {
				jsonModel.setStatusSuccess();
			}
		} catch (Exception e) {
            e.printStackTrace();
			jsonModel.setStatusError();
		}
		return jsonModel;
	}

    /**
     * 获取wechat账号
     * @return
     */
    private JsonModel getWechatAccount() {
        JsonModel result = new JsonModel();
        WechatAccountExp wechatAccountExp = null;
        try {
            wechatAccountExp = wechatAccountService.queryWechatAccount();
            if (StringUtils.isNotBlank(wechatAccountExp.getConnectStatus()))
                wechatAccountExp.setConnectStatusMsg(WechatAccountConfigStatus.getMsg(wechatAccountExp.getConnectStatus()));
            result.setStatusSuccess();
            result.setObj(wechatAccountExp);
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatusError();
        }
        return result;
    }
	
	
	@Autowired
	public void setWechatAccountService(WechatAccountService wechatAccountService) {
		this.wechatAccountService = wechatAccountService;
	}
	

}

