package com.bw.adv.controller.member.tag;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.filter.model.DataGridFilterModel;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.module.common.utils.MyConstants;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.model.MemberTagCondition;
import com.bw.adv.module.member.tag.search.MemberTagSearch;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelImport;
import com.bw.adv.service.member.service.MemberService;
import com.bw.adv.service.member.service.MemberTagService;

@Controller
@RequestMapping("/memberTagController")
public class MemberTagController {
	
	private static Gson gson = new GsonBuilder().serializeNulls().create();	
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	private MemberTagService memberTagService;
	
	private MemberService memberService;
	
	private CodeService codeService;
	
	/**
	 * 
	 * showMemberTagList:条件查询会员标签列表 <br/>
	 * Date: 2015年8月17日 上午10:57:51 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showMemberTagList")
	@ResponseBody
	public Map<String,Object> showMemberTagList(MemberTagSearch search) {
		Map<String,Object> result = null;
		List<MemberTag> list = null;
		Long total = null;
		try {
			result =  new HashMap<String, Object>();
			search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
			total = this.memberTagService.queryCountSearchAuto(search);
			list = this.memberTagService.queryListSearchAuto(search);
			result.put("total", total);
			result.put("rows", list);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "showMemberTagListSelect")
	@ResponseBody
	public List<MemberTag> showMemberTagListSelect() {
		List<MemberTag> result = null;
		MemberTagSearch search = null;
		try {
			search = new MemberTagSearch();
			search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
			result = this.memberTagService.queryListSearchAuto(new MemberTagSearch());
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "showCountByName")
	@ResponseBody
	public Long showCountByName(String memberTagName) {
		Long result = null;
		MemberTagSearch search = null;
		try {
			search = new MemberTagSearch();
			search.setEqualMemberTagName(memberTagName);
			result = this.memberTagService.queryCountSearchAuto(search);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberTagDetail:查看会员标签详情 <br/>
	 * Date: 2015年8月17日 上午10:57:14 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberTagDetail")
	@ResponseBody
	public MemberTag showMemberTagDetail(Long memberTagId) {
		MemberTag result = null;
		try {
			result = this.memberTagService.queryEntityPkAuto(memberTagId);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showMemberListOfTag:条件查询标签下会员列表 <br/>
	 * Date: 2015年8月17日 上午11:16:44 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @param search
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showMemberListOfTag")
	@ResponseBody
	public DataGridModel<MemberExp> showMemberListOfTag(Long memberTagId,MemberSearch search) {
		List<MemberExp> list = null;
		String[] memberIds = null;
		MemberTag memberTag = null;
		Long total = null;
		DataGridFilterModel<MemberExp> result = null;
		try {
			memberIds = new String[1];
			memberIds[0] = memberTagId+"";
			memberTag = this.memberTagService.queryEntityPkAuto(memberTagId);
			if(MyConstants.IS_ALL.YES==memberTag.getIsAll()){
				total =  this.memberService.queryCountSearchAuto(search);
				list = this.memberService.queryListSearchAuto(search);
			}else{
				search.setInMemberTagIds(memberIds);
				total =  this.memberService.queryCountSearchAuto(search);
				list = this.memberTagService.queryMemberListOfTag(search);
			}
			result = new DataGridFilterModel<MemberExp>(list, total);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * saveMemberTag:新增会员标签 <br/>
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMemberTag")
	@ResponseBody
	public JsonModel saveMemberTag(MemberTag memberTag, String selectList) {
		JsonModel result = null;
		String memberTagCode = null;
		MemberTagSearch search = null;
		Long count = 0L;
		try {
			if(!"".equals(memberTag.getMemberTagName())){
				search = new MemberTagSearch();
				search.setEqualMemberTagName(memberTag.getMemberTagName());
				search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
				count = this.memberTagService.queryCountSearchAuto(search);
				if(count>0){
					result = new JsonModel(JsonModel.Status_Error,"名称已被使用");
					return result;
				}
			}
			
			memberTagCode = this.codeService.generateCode(MemberTag.class);
			memberTag.setMemberTagCode(memberTagCode);
			memberTag.setCreateTime(DateUtils.getCurrentTimeOfDb());
			if(StringUtils.isNotBlank(memberTag.getUpdateType()) && memberTag.getUpdateType().equals("2")){
				memberTag.setStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_USE.getId());
			}else{
				memberTag.setStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_CREATED.getId());
			}
			memberTag.setIsAll(MyConstants.IS_ALL.NO);
			this.memberTagService.insertMemberTag(memberTag);
			Long memberTagId = memberTag.getMemberTagId();
			List<MemberTagCondition> list = gson.fromJson(selectList,  new TypeToken<List<MemberTagCondition>>(){}.getType());
			if( list.size() > 0){
				for (MemberTagCondition memberTagCondition : list) {
					memberTagCondition.setMemberTagId(memberTagId);
				}
				this.memberTagService.saveTagCondition(list);
			}
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * saveMemberTag:新增会员标签 <br/>
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "updateMemberTag")
	@ResponseBody
	public JsonModel updateMemberTag(MemberTag memberTag, String selectList) {
		JsonModel result = null;
		try {
			
			this.memberTagService.updateMemberTag(memberTag);
			Long memberTagId = memberTag.getMemberTagId();
			List<MemberTagCondition> list = gson.fromJson(selectList,  new TypeToken<List<MemberTagCondition>>(){}.getType());
			if( list.size() > 0){
				for (MemberTagCondition memberTagCondition : list) {
					memberTagCondition.setMemberTagId(memberTagId);
				}
				this.memberTagService.updateTagCondition(list, memberTagId);
			}else{
				this.memberTagService.deleteConditionByTagId(memberTagId);
			}
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 查询要更新的标签
	 * @param memberTagId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showEditTag")
	@ResponseBody
	public Map<String,Object> showEditTag(Long memberTagId){
		Map<String,Object> result = new HashMap<String, Object>();
		try {
			MemberTag tag = new MemberTag();
			tag = memberTagService.queryByPk(memberTagId);
			result.put("tag", tag);
			List<MemberTagCondition> tagCondition = new ArrayList<MemberTagCondition>();
			tagCondition = memberTagService.findConditionByTagId(memberTagId);
			result.put("tagCondition", tagCondition);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * editMemberTag:编辑标签 <br/>
	 * Date: 2015年8月17日 上午10:55:46 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTag
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editMemberTag")
	@ResponseBody
	public JsonModel editMemberTag(Long memberTagId,MemberTag memberTag) {
		JsonModel result = null;
		MemberTagSearch search = null;
		List<MemberTag> list = null;
		try {
			if(!"".equals(memberTag.getMemberTagName())){
				search = new MemberTagSearch();
				search.setEqualMemberTagName(memberTag.getMemberTagName());
				search.setNotEqualStatusId(StatusConstant.MEMBER_GROUP_AND_TAG_REMOVE.getId());
				list = this.memberTagService.queryListSearchAuto(search);
				if(list != null && list.size()>0){
					if(memberTagId != list.get(0).getMemberTagId()){
						result = new JsonModel(JsonModel.Status_Error,"名称已被使用");
						return result;
					}
				}
			}
			
			memberTag.setMemberTagId(memberTagId);
			 this.memberTagService.updateMemberTag(memberTag);
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "saveAllMemberToTag")
	@ResponseBody
	public JsonModel saveAllMemberToTag(Long memberTagId) {
		JsonModel result = null;
		MemberTag memberTag = null;
		try {
			memberTag = new MemberTag();
			memberTag.setIsAll(MyConstants.IS_ALL.YES);
			memberTag.setMemberTagId(memberTagId);
			this.memberTagService.saveAllMemberToTag(memberTag);
			
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * removeMemberTag:删除标签 <br/>
	 * Date: 2015年8月17日 上午10:54:58 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "removeMemberTag")
	@ResponseBody
	public JsonModel removeMemberTag(Long memberTagId) {
		JsonModel result = null;
		try {
			this.memberTagService.deleteMemberTagItemByTagId(memberTagId);
			this.memberTagService.deleteConditionByTagId(memberTagId);
			this.memberTagService.deleteteMemberTagById(memberTagId);
			result = new JsonModel(JsonModel.Status_Success,"删除成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"删除失败，请稍后重试！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "removeMemberTagItem")
	@ResponseBody
	public JsonModel removeMemberTagItem(Long memberTagId,Long memberId) {
		JsonModel result = null;
		try {
			this.memberTagService.deleteMemberTagItem(memberTagId,memberId);
			result = new JsonModel(JsonModel.Status_Success,"删除成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"删除失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * saveMemberTagItem:会员标签批量添加会员 <br/>
	 * Date: 2015年8月17日 上午10:54:26 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @param memberIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveMemberTagItem")
	@ResponseBody
	public JsonModel saveMemberTagItem(Long memberTagId,String memberIds) {
		JsonModel result = null;
		Long count = null;
		//String[] memberIdArr = null;
		try {
			//memberIdArr = memberIds.split(",");
			count = (long) this.memberTagService.insertBatchMemberTagItem(memberTagId, memberIds);
			result = new JsonModel(JsonModel.Status_Success,"成功添加"+count+"名会员");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"添加失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 
	 * excuteMemberTagSql:手动执行标签sql <br/>
	 * Date: 2015年8月17日 上午10:53:55 <br/>
	 * scrmVersion 1.0
	 * @author simon
	 * @version jdk1.7
	 * @param memberTagId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "excuteMemberTagSql")
	@ResponseBody
	public JsonModel excuteMemberTagSql(Long memberTagId) {
		JsonModel result = null;
		Long count = null;
		String sql = null;
		MemberTag tag = null;
		try {
			tag = this.memberTagService.queryByPk(memberTagId);
			if(tag != null){
				sql = tag.getConditionSql();
			}
			try{
				this.memberService.checkMemberSql(sql);
			}catch(Exception ex){
				ex.printStackTrace();
				result = new JsonModel(JsonModel.Status_Error,"标签sql验证失败！请修改！");
				return result;
			}
			count = (long) this.memberTagService.excuteMemberTagBySql(memberTagId);
			result = new JsonModel(JsonModel.Status_Success,"成功添加"+count+"名会员");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"执行失败，请稍后重试！");
		return result;
	}
	
	/**
	 * 手动sql执行
	 * @param memberTagId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "excuteMemberTagManel")
	@ResponseBody
	public JsonModel excuteMemberTagManel(Long memberTagId) {
		JsonModel result = null;
		try {
			List<MemberTag> settingList = this.memberTagService.findMemberTagByStatusId(StatusConstant.MEMBER_TAG_SETTING.getId());
			if(settingList != null && settingList.size()>0){
				result = new JsonModel(JsonModel.Status_Success,"系统中有正在执行的标签，请稍后再来执行，谢谢！");
			}else{
				this.memberTagService.setTagManel(memberTagId);
				result = new JsonModel(JsonModel.Status_Success,"正在执行，请稍后查询结果！");
			}
		}catch (Exception ex) {
			ex.printStackTrace();
			result = new JsonModel(JsonModel.Status_Error,"执行失败，请稍后重试！");
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "downTemp")
	public void downTemp(HttpServletResponse response,HttpServletRequest request) {
		String filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/会员列表导入模板.xls" ;  
		try {
			FileUtils.download("会员列表导入模板.xls", filePath, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "importData")
	@ResponseBody
	public DataGridModel<MemberExp> importData(MultipartHttpServletRequest request) {
		DataGridModel<MemberExp> result = null;
		List<MemberExp> list = null;
		MultipartFile file = null;
		String filePath = null;
		 File fileExcel = null;
		 ExcelImport test = null;
		 List<Member> data = null;
		 String mobile = null;
		 MemberSearch search = null;
		 MemberExp exp = null;
		try {
			file = request.getFile("fileInput");
			  // 文件保存路径  
            filePath = request.getSession().getServletContext().getRealPath("/") + "/upload/"  
                     + file.getOriginalFilename();  
            fileExcel = new File(filePath);
            file.transferTo(fileExcel);  
			test = new ExcelImport();
			data = (ArrayList) test.importExcel(fileExcel, Member.class);
			list = new ArrayList<MemberExp>();
			if(data != null && data.size()>0){
				for(Member member:data){
					mobile = member.getMobile();
					search = new MemberSearch();
					search.setEqualMobile(mobile);
					exp = this.memberService.queryEntitySearchAuto(search);
					if(exp == null){
						exp = new MemberExp();
						exp.setMobile(mobile);
					}
					list.add(exp);
				}
			}
			fileExcel.delete();
			result = new DataGridModel<MemberExp>(list, Long.valueOf(list==null?0:list.size()));
			return result;
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new DataGridModel<MemberExp>("导入失败请检查数据！");
		return result;
	}

	public MemberTagService getMemberTagService() {
		return memberTagService;
	}

	@Autowired
	public void setMemberTagService(MemberTagService memberTagService) {
		this.memberTagService = memberTagService;
	}
	
	public CodeService getCodeService() {
		return codeService;
	}
	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}

	public MemberService getMemberService() {
		return memberService;
	}
	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}
	
}
