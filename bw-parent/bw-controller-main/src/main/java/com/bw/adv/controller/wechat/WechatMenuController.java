/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:WechatMenuController.java
 * Package Name:com.sage.scrm.controller.wechat
 * Date:2015年9月4日下午6:53:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.wechat;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.module.base.enums.menuMsgTypeEnum;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.module.common.utils.TreeUtil;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.api.ApiWeChat;
import com.bw.adv.module.wechat.api.ApiWechatMaterialUpload;
import com.bw.adv.module.wechat.enums.WechatEventTypeEnum;
import com.bw.adv.module.wechat.model.WechatButton;
import com.bw.adv.module.wechat.model.WechatMenu;
import com.bw.adv.module.wechat.model.WechatMenuJson;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.model.WeixinMedia;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.module.wechat.util.HttpKit;
import com.bw.adv.service.wechat.service.WechatMenuService;
import com.bw.adv.service.wechat.service.WechatMessageTemplateService;

/**
 * ClassName:WechatMenuController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月4日 下午6:53:09 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/wechatMenuController")
public class WechatMenuController {
	protected final Logger logger = Logger.getLogger(WechatFansController.class);
	
	private WechatMenuService wechatMenuService;
	
	private WechatMessageTemplateService wechatMessageTemplateService;
	
	private ApiWechatMaterialUpload apiWechatMaterialUpload;
	
	private AccessTokenCacheService accessTokenCacheService;
	//查询菜单
	@RequestMapping(method = RequestMethod.GET, value = "showWechatMenu")
	@ResponseBody
	public WechatMenuJson showWechatMenu() {
		List<WechatButton> list = null;
		List<WechatButton> result = null;
		list = this.wechatMenuService.queryAllMenu();
		result = TreeUtil.dualWithWechatButton(list);
		WechatMenuJson menu =  new WechatMenuJson();
		menu.setButton(result);
		return menu;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "showWechatMessageTemplateById")
	@ResponseBody
	public WechatMessageTemplate showWechatMessageTemplateById(String tempId) {
		WechatMessageTemplate temp = null;
		temp = this.wechatMessageTemplateService.queryByPk(tempId);
		return temp;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "saveAndPub")
	@ResponseBody
	public JsonModel saveAndPub(String menuStr) {
		JsonModel result = null;
		 String newStr = null;
		try {
		   // newStr = menuStr.replaceAll("menu_name", "name");
			this.wechatMenuService.saveAndPub(newStr);
			result = new JsonModel(JsonModel.Status_Success,"发布菜单成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"发布菜单失败！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "saveAndPubMenu")
	@ResponseBody
	public JsonModel saveAndPubMenu() {
		JsonModel result = null;
		List<WechatMenu> wechatMenus = null;
		String wechatResult = "";
		JSONObject obj = new JSONObject();
		try {
			wechatMenus = wechatMenuService.findAllMenu();
			List<WechatMenu> buttonList = new ArrayList<WechatMenu>();
	        List<WechatMenu> subButtonList = new ArrayList<WechatMenu>();
	        for (WechatMenu wechatMenu : wechatMenus) {
	            if (wechatMenu.getPareWechatMenuId() == 0) {
	                buttonList.add(wechatMenu);
	            }
	            else {
	                subButtonList.add(wechatMenu);
	            }
	        }
	        // 开始封装微信的格式
	        String menuStr = "{ \"button\": [";
	        Integer flag = 0;
	        for (WechatMenu buttonMenu : buttonList) {
	            if (flag == 0) {
	                menuStr += "{";
	            }
	            else if (flag < buttonList.size()) {
	                menuStr += ",{";
	            }
	            
	            List<WechatMenu> subButtonMenu = new ArrayList<WechatMenu>();
	            subButtonMenu = getSubCountWechatMenu(buttonMenu.getWechatMenuId() + "", subButtonList);
	            if (subButtonMenu.size() > 0) {
	                menuStr += "\"name\":" + "\"" + buttonMenu.getWechatMenuName() + "\",";
	                menuStr += "\"sub_button\":[";
	                for (int i = 0; i < subButtonMenu.size(); i++) {
	                    if (i == 0) {
	                        menuStr += "{";
	                    }
	                    else if (i < subButtonMenu.size()) {
	                        menuStr += ",{";
	                    }
	                    menuStr += "\"type\":" + "\"" + WechatEventTypeEnum.getDesc(Integer.valueOf(subButtonMenu.get(i).getMsgType())) + "\",";
	                    menuStr += "\"name\":" + "\"" + subButtonMenu.get(i).getWechatMenuName() + "\",";
	                    if (WechatEventTypeEnum.VIEW.getId().toString().equals(subButtonMenu.get(i).getMsgType())) {
	                        menuStr += "\"url\":" + "\"" + subButtonMenu.get(i).getUrl() + "\"";
	                    }else {
	                        menuStr += "\"key\":" + "\"" + subButtonMenu.get(i).getMenuKey() + "\"";
	                    }
	                    menuStr += "}";
	                }
	                menuStr += "]}";
	            }
	            else {
	                menuStr += "\"type\":" + "\"" + WechatEventTypeEnum.getDesc(Integer.valueOf(buttonMenu.getMsgType())) + "\",";
	                menuStr += "\"name\":" + "\"" + buttonMenu.getWechatMenuName() + "\",";
	                if (WechatEventTypeEnum.VIEW.getId().toString().equals(buttonMenu.getMsgType())) {
	                    menuStr += "\"url\":" + "\"" + buttonMenu.getUrl() + "\"";
	                }
	                else {
	                    menuStr += "\"key\":" + "\"" + buttonMenu.getMenuKey() + "\"";
	                }
	                menuStr += "}";
	            }
	            flag++;
	        }
	        menuStr += "]}";
	        logger.info(menuStr);
	        wechatResult = this.createMenu(menuStr);
	        if(StringUtils.isNotEmpty(wechatResult)){
	        	obj = JSONObject.fromObject(wechatResult);
	        	if(obj.getString("errcode").equals(StatusConstant.WECHAT_ERRCODE_SUC.getId().toString())){
	        		result = new JsonModel(JsonModel.Status_Success,"发布菜单成功！");
	        	}else{
	        		logger.error("发布菜单失败："+obj);
	        		result = new JsonModel(JsonModel.Status_Error,"发布菜单失败！");
	        	}
	        }else{
	        	logger.error("wechatResult is null");
	        	result = new JsonModel(JsonModel.Status_Error,"发布菜单失败！");
	        }
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"发布菜单失败！");
		return result;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "saveMenu")
	@ResponseBody
	public JsonModel saveMenu(HttpServletRequest request,HttpServletResponse response) {
		JsonModel result = null;
		String menuName = request.getParameter("menuName");
		String pareWechatMenuId = request.getParameter("pareWechatMenuId");
		try {
			WechatMenu menu = new WechatMenu();
			menu.setWechatMenuName(URLDecoder.decode(menuName, "UTF-8"));
			menu.setCreateTime(DateUtils.getCurrentTimeOfDb());
			menu.setCreateBy(SysUserUtil.getSessionUser(request).getUserId());
			menu.setMsgType(menuMsgTypeEnum.CLICK.getId());
			menu.setPareWechatMenuId(Long.valueOf(0));
			if(StringUtils.isNotEmpty(pareWechatMenuId)){
				menu.setPareWechatMenuId(Long.valueOf(pareWechatMenuId));
			}
			this.wechatMenuService.addMenu(menu);
			result = new JsonModel(JsonModel.Status_Success,"发布菜单成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"发布菜单失败！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "editMenu")
	@ResponseBody
	public JsonModel editMenu(HttpServletRequest request,HttpServletResponse response) {
		JsonModel result = null;
		String menuName = request.getParameter("menuName");
		String pareWechatMenuId = request.getParameter("id");
		try {
			menuName = URLDecoder.decode(menuName, "UTF-8");
			WechatMenu menu = new WechatMenu();
			menu.setWechatMenuName(menuName);
			menu.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			menu.setWechatMenuId(Long.valueOf(pareWechatMenuId));
			menu.setUpdateBy(SysUserUtil.getSessionUser(request).getUserId());
			this.wechatMenuService.updateByPkSelective(menu);
			result = new JsonModel(JsonModel.Status_Success,"更新菜单成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"更新菜单失败！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "saveUrl")
	@ResponseBody
	public JsonModel saveUrl(HttpServletRequest request,HttpServletResponse response) {
		JsonModel result = null;
		String menuUrl = request.getParameter("menuUrl");
		String menuId = request.getParameter("id");
		try {
			if(menuUrl.contains(";")){
				menuUrl = menuUrl.replaceAll(";", "&");
			}
			WechatMenu menu = new WechatMenu();
			menu.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			menu.setUpdateBy(SysUserUtil.getSessionUser(request).getUserId());
			menu.setUrl(menuUrl);
			menu.setWechatMenuId(Long.valueOf(menuId));
			menu.setMsgType(menuMsgTypeEnum.VIEW.getId());
			this.wechatMenuService.updateByPkSelective(menu);
			result = new JsonModel(JsonModel.Status_Success,"设置菜单跳转链接成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"设置菜单跳转链接失败！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "deleteMenu")
	@ResponseBody
	public JsonModel deleteMenu(HttpServletRequest request,HttpServletResponse response) {
		JsonModel result = null;
		String wechatMenuId = request.getParameter("id");
		try {
			this.wechatMenuService.removeByPk(wechatMenuId);
			this.wechatMenuService.removeByParentId(Long.valueOf(wechatMenuId));
			result = new JsonModel(JsonModel.Status_Success,"更新菜单成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"更新菜单失败！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "uploadImage")
	@ResponseBody
	public WechatMessageTemplate uploadImage(MultipartHttpServletRequest request) {
		String filePath = "";
		WechatMessageTemplate temp = null;
		WeixinMedia media = null;
		try {
			//文件上传到本地服务器,返回映射的路径
			filePath = FileUtils.upload(request);
			String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath(); 
			//上传微信
			media = apiWechatMaterialUpload.uploadMediaForever("image", strBackUrl+filePath);
			temp = new WechatMessageTemplate();
			//图片url
			temp.setPictureUrl(filePath);
			//消息类型
			temp.setWechatMessageType("image");
			//创建时间
			temp.setCreateTime(DateUtils.getCurrentTimeOfDb());
			//媒体ID
			temp.setMediaId(media.getMediaId());
			this.wechatMessageTemplateService.saveSelective(temp);
			return temp;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
		
	}
	
	private List<WechatMenu> getSubCountWechatMenu(String parentid, List<WechatMenu> wechats) {
		List<WechatMenu> subWechatmenus = new ArrayList<WechatMenu>();
		for (WechatMenu wechatmenu : wechats) {
			if (parentid.equals(wechatmenu.getPareWechatMenuId().toString())) {
				subWechatmenus.add(wechatmenu);
			}
		}
		return subWechatmenus;
	}

	private String createMenu(String newStr){
		String menuCreateUrl =null;
		// 微信返回的结果
		String result =null;
		try {
			menuCreateUrl =ApiWeChat.MENU_CREATE.replace("ACCESS_TOKEN", accessTokenCacheService.getAccessTokenCache());
			result = HttpKit.post(menuCreateUrl, newStr);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}
	
	public WechatMenuService getWechatMenuService() {
		return wechatMenuService;
	}
	@Autowired
	public void setWechatMenuService(WechatMenuService wechatMenuService) {
		this.wechatMenuService = wechatMenuService;
	}
	
	public WechatMessageTemplateService getWechatMessageTemplateService() {
		return wechatMessageTemplateService;
	}
	@Autowired
	public void setWechatMessageTemplateService(
			WechatMessageTemplateService wechatMessageTemplateService) {
		this.wechatMessageTemplateService = wechatMessageTemplateService;
	}
	
	public ApiWechatMaterialUpload getApiWechatMaterialUpload() {
		return apiWechatMaterialUpload;
	}
	@Autowired
	public void setApiWechatMaterialUpload(
			ApiWechatMaterialUpload apiWechatMaterialUpload) {
		this.apiWechatMaterialUpload = apiWechatMaterialUpload;
	}
	
	@Autowired
	public void setAccessTokenCacheService(AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}
}

