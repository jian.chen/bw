package com.bw.adv.controller.base;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.exp.OrgExp;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.TreeMenuModel;
import com.bw.adv.module.common.utils.TreeUtil;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.OrgService;



@Controller
@RequestMapping("/orgController")
@SuppressWarnings("unused")
public class OrgController {
	@Resource
	private OrgService orgService;

	/**
	 * showOrg:查询区域列表 <br/>
	 * Date: 2015-8-26 下午7:45:14 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/showOrg", method = RequestMethod.GET)
	public void showOrg(HttpServletRequest request,HttpServletResponse response) throws IOException{
		Page<Org> page = null;
		List<Org> orgList = null;
		String json="";
		try {	
			orgList=orgService.queryListForOrg(StatusConstant.ORG_ACTIVE.getCode());
			json=JSONArray.fromObject(orgList).toString(); 
			json=json.replace("pareOrgId", "_parentId");
			json=json.replace(",\"_parentId\":0", ",\"iconCls\":\"icon-ok\"");
			json="{\"total\":10,\"rows\":"+json+"}";
			response.setContentType("text/html;charset=UTF-8"); 
			response.getWriter().print(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * saveOrg:保存子区域 <br/>
	 * Date: 2015-8-26 下午7:45:41 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param org
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "saveOrg", method = RequestMethod.POST)
	@ResponseBody
	public String saveOrg(Org org) throws IOException{
		String result = null;
		Long pareOrgId=null;
		String orgName=null;
		List<Org> orgList = null;
		try {	
			orgList=new ArrayList<Org>();
			
			pareOrgId=org.getPareOrgId();
			orgName=org.getOrgName();
			
			orgList=orgService.queryOrgByName(StatusConstant.ORG_ACTIVE.getCode(), orgName, pareOrgId);
			
			if(null!=orgList&&orgList.size()>0){
				result = "{\"code\":\"2\",\"message\":\"同级区域名称不能重复!\"}";
				return result;
			}
			
			org.setUseFlag(StatusConstant.ORG_ACTIVE.getCode());
			org.setStartDate(DateUtils.getCurrentDateOfDb());
			orgService.save(org);
			result = "{\"code\":\"1\",\"message\":\"新增成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"新增失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}	
	
	/**
	 * orgDetail:获取选中区域详情. <br/>
	 * Date: 2015-8-27 下午3:18:11 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param orgId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "orgDetail", method = RequestMethod.POST)
	@ResponseBody
	public OrgExp orgDetail(Long orgId) throws IOException{
		OrgExp org=null;
		try {	
			org=orgService.queryOrgById(orgId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return org;
	}	
	
	/**
	 * editOrg:修改区域 <br/>
	 * Date: 2015-8-26 下午7:46:02 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param org
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "editOrg", method = RequestMethod.POST)
	@ResponseBody
	public String editOrg(Org org) throws IOException{
		String result = null;
		Long pareOrgId=null;
		String orgName=null;
		List<Org> orgList = null;
		try {	
			orgList=new ArrayList<Org>();
			
			pareOrgId=org.getPareOrgId();
			orgName=org.getOrgName();
			
			orgList=orgService.queryOrgByName(StatusConstant.ORG_ACTIVE.getCode(), orgName, pareOrgId);
			if(null!=orgList&&orgList.size()>0){
				Boolean b=false;
				for(Org o:orgList){
					if(o.getOrgId()!=org.getOrgId()){//id不同 name相同
						b=true;
					}
				}
				if(b){
					result = "{\"code\":\"3\",\"message\":\"同级区域名称不能重复!\"}";
					return result;
				}
			}			
			if(null==pareOrgId){//改成第一级  pareOrgId没值
				String newOrgName = org.getOrgName();
				org=orgService.queryByPk(org.getOrgId());
				org.setOrgName(newOrgName);
				org.setPareOrgId(null);
				orgService.updateByPk(org);
				
			}else{//改成非第一级 pareOrgId有值
				orgService.updateByPkSelective(org);
			}
			result = "{\"code\":\"1\",\"message\":\"修改成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"修改失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * findParentOrgList:查询选中区域以外的所有区域（用于修改上级区域）. <br/>
	 * Date: 2015-8-27 下午3:19:13 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param orgId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "findParentOrgList", method = RequestMethod.GET)
	@ResponseBody
	public List<Org> findParentOrgList(Long orgId) throws IOException{
		List<Org> result = null;
		Org org=null;
		try {	
			result=orgService.queryListForParentOrg(orgId);
			org=new Org();
			org.setOrgId(null);
			org.setOrgName("无上级区域");
			
			result.add(org);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}	
	
	/**
	 * removeProduct:删除选中区域及其所有子区域(软删除)，并同时情况相关表中的区域ID. <br/>
	 * Date: 2015-8-27 下午5:18:37 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param orgId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "removeOrg", method = RequestMethod.POST)
	@ResponseBody
	public String removeOrg(Long orgId) throws IOException{
		String result = null;	
		List<Org> childOrglist=null;
		List<Object> longList=null;
		try {	
			childOrglist=new ArrayList<Org>();
			longList=new ArrayList<Object>();
			
			childOrglist=orgService.queryListForChildOrg(orgId);
			for(Org org:childOrglist){
				longList.add(org.getOrgId());
			}
			if(longList.size()>0){
				Example example=new Example();
				example.createCriteria().andIdIn("ORG_ID ", longList);
				
				orgService.deleteOrg(DateUtils.getCurrentDateOfDb(),example);
				orgService.deleteStoreOrgId(example);
			}
			result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
			
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"删除失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * testTree:区域树(包含门店)
	 * Date: 2015年9月3日 下午12:26:03 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "testTree")
	@ResponseBody
	public List<TreeMenuModel> testTree() {
		List<TreeMenuModel> treeList = new ArrayList<TreeMenuModel>();
		List<TreeMenuModel> result = new ArrayList<TreeMenuModel>();
		List<Org> orgList = new ArrayList<Org>();
		Org temp = null;
		try {
			orgList = orgService.queryOrgForTree();
			
			for(int i=0;i<orgList.size();i++){
				temp = orgList.get(i);
				TreeMenuModel nodes = new TreeMenuModel();
				nodes.setId(temp.getOrgId().toString());
				if(temp.getPareOrgId() != null){
					nodes.setPid(temp.getPareOrgId().toString());
				}else{
					nodes.setPid("");
				}
				nodes.setType((temp.getOrgTypeId() == null) ? StringUtils.EMPTY : temp.getOrgTypeId().toString());
				nodes.setName(temp.getOrgName());
				result.add(nodes);
			}
			
			treeList = TreeUtil.dualWithTreeModel(result);  
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return treeList;
	}
	

	/**
	 * 
	 * testTree:区域树（不包含门店）
	 * Date: 2015年9月22日 下午3:26:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "OrgTree")
	@ResponseBody
	public List<TreeMenuModel> OrgTree() {
		List<TreeMenuModel> treeList = new ArrayList<TreeMenuModel>();
		List<TreeMenuModel> result = new ArrayList<TreeMenuModel>();
		List<Org> orgList = new ArrayList<Org>();
		Org temp = null;
		try {
			orgList = orgService.queryOrgForTree();
			
			for(int i=0;i<orgList.size();i++){
				temp = orgList.get(i);
				if(temp.getOrgTypeId() == null || temp.getOrgTypeId() != 4){
					TreeMenuModel nodes = new TreeMenuModel();
					nodes.setId(temp.getOrgId().toString());
					if(temp.getPareOrgId() != null){
						nodes.setPid(temp.getPareOrgId().toString());
					}else{
						nodes.setPid("");
					}
					nodes.setName(temp.getOrgName());
					result.add(nodes);
				}
			}
			
			treeList = TreeUtil.dualWithTreeModel(result);  
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return treeList;
	}
	
}
