/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:GoodsShowController.java
 * Package Name:com.sage.scrm.controller.points
 * Date:2015年12月9日上午11:18:45
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.points;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.exp.CouponExp;
import com.bw.adv.module.goods.model.GoodsShow;
import com.bw.adv.module.goods.model.GoodsShowCategory;
import com.bw.adv.module.goods.model.GoodsShowMemberCardGrade;
import com.bw.adv.module.goods.model.exp.GoodsShowExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.coupon.service.CouponService;
import com.bw.adv.service.points.service.GoodsShowMemberCardGradeService;
import com.bw.adv.service.points.service.GoodsShowSevice;

/**
 * ClassName:GoodsShowController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月9日 上午11:18:45 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/goodsShowController")
@SuppressWarnings("unused")
public class GoodsShowController {
	
	private GoodsShowSevice goodsShowSevice;
	
	private CouponService couponService;
	
	private final Log logger = LogFactory.getLog(this.getClass());
	
	
	/**
	 * 
	 * showList:(查询有效的商品展示列表). <br/>
	 * Date: 2015年12月9日 上午11:32:12 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showList")
	@ResponseBody
	public Map<String, Object> showList(int page,int rows){
		Map<String, Object> map = null;
		List<GoodsShowExp> result = null;
		Page<GoodsShowExp> pageObj = null;
		
		try {
			
			map = new HashMap<String, Object>();
			pageObj = new Page<GoodsShowExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			result = goodsShowSevice.queryList(pageObj, StatusConstant.GOODS_SHOW_REMOVE.getId());
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * 
	 * saveGoodsShow:(保存商品展示信息(新建/修改)). <br/>
	 * Date: 2015年12月9日 下午3:33:33 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShow
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveGoodsShow")
	@ResponseBody
	public JsonModel saveGoodsShow(GoodsShowExp goodsShowExp,HttpServletRequest request){
		
		logger.info("==>saveGoodsShow-params:"+JSON.toJSONString(goodsShowExp));
		JsonModel result = null;
		boolean isSave = false;
		try {
			result = new JsonModel();
			//新建
			if(goodsShowExp.getGoodsShowId() == null){
				if(goodsShowExp.getStatusId() == null){
					goodsShowExp.setStatusId(StatusConstant.GOODS_SHOW_DISENABLE.getId());
				}
				goodsShowExp.setCreateBy(SysUserUtil.getSessionUser(request).getUserId());
				goodsShowExp.setCreateTime(DateUtils.getCurrentTimeOfDb());
				GoodsShow goodsShow = JSON.parseObject(JSON.toJSONString(goodsShowExp),GoodsShow.class);
				goodsShowSevice.saveSelective(goodsShow);
				goodsShowExp.setGoodsShowId(goodsShow.getGoodsShowId());
				isSave=true;
			}else{//修改
				goodsShowExp.setUpdateBy(SysUserUtil.getSessionUser(request).getUserId());
				goodsShowExp.setUpdateTime(DateUtils.getCurrentTimeOfDb());
				goodsShowSevice.updateByPkSelective(goodsShowExp);
			}
			
			//添加或更新会员卡等级与积分兑换关系
			this.saveGoodsShowMemberCardGrade(goodsShowExp,isSave);
			
			result.setStatusSuccess();
			result.setMessage("保存成功!");
		} catch (Exception e) {
			result.setStatusError();
			result.setMessage("保存失败!");
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	
	/**
	 * saveGoodsShowMemberCardGrade:(添加或更新会员卡等级与积分兑换关系). <br/>
	 * Date: 2016年7月19日 上午10:50:27 <br/>
	 * scrmVersion standard
	 * @author dong.d
	 * @version jdk1.7
	 * @param goodsShowExp
	 */
	private void saveGoodsShowMemberCardGrade(GoodsShowExp goodsShowExp , boolean isSave){
		
		logger.info("==>saveGoodsShowMemberCardGrade-params:"+JSON.toJSONString(goodsShowExp)+" isSave:"+isSave);
		List<GoodsShowMemberCardGrade> goodsShowMemberCardGradeList =JSON.parseArray(goodsShowExp.getGoodsShowMemberCardGradeList() , GoodsShowMemberCardGrade.class);
		if(isSave){
			if(goodsShowMemberCardGradeList != null && goodsShowMemberCardGradeList.size() >0){
				for(GoodsShowMemberCardGrade grade : goodsShowMemberCardGradeList){
					grade.setGoodsShowId(goodsShowExp.getGoodsShowId());
				}
				goodsShowMemberCardGradeService.saveGoodsShowMemberCardGrade(goodsShowMemberCardGradeList);
			}
		}else{
			if(goodsShowMemberCardGradeList != null && goodsShowMemberCardGradeList.size() >0){
				for(GoodsShowMemberCardGrade grade : goodsShowMemberCardGradeList){
					goodsShowMemberCardGradeService.updateGoodsShowMemberCardGrade(grade);
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * showDetail:(根据id查询商品展示详细信息). <br/>
	 * Date: 2015年12月9日 下午3:37:55 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value = "showDetail")
	@ResponseBody
	public GoodsShowExp showDetail(String goodsShowId){
		GoodsShowExp result = null;
		
		try {
			result = new GoodsShowExp();
			result = goodsShowSevice.queryExpByPk(goodsShowId);
			List<GoodsShowMemberCardGrade> goodsShowMemberCardGradeList = goodsShowMemberCardGradeService.queryGoodsShowMemberCardGradeListByGoodsShowId(Long.parseLong(goodsShowId.replace(",", "")));
			result.setGoodsShowMemberCardGradeList(JSON.toJSONString(goodsShowMemberCardGradeList));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * removeList:(批量删除商品展示). <br/>
	 * Date: 2015年12月9日 下午3:41:48 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value = "removeList")
	@ResponseBody
	public String removeList(String goodsShowIds){
		String result = null;
		
		try {
			
			goodsShowSevice.deleteList(goodsShowIds);
			
			result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"删除失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * updateStatus:(上架/下架). <br/>
	 * Date: 2015年12月10日 下午3:31:39 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowId
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "updateStatus")
	@ResponseBody
	public String updateStatus(Long goodsShowId,Long statusId,HttpServletRequest request){
		String result = null;
		GoodsShow goodsShow = null;
		
		try {
			
			goodsShow = new GoodsShow();
			
			if(StatusConstant.GOODS_SHOW_ENABLE.getId().equals(statusId)){
				statusId = StatusConstant.GOODS_SHOW_DISENABLE.getId();
			}else{
				statusId = StatusConstant.GOODS_SHOW_ENABLE.getId();
			}
			
			goodsShow.setGoodsShowId(goodsShowId);
			goodsShow.setStatusId(statusId);
			goodsShow.setUpdateBy(SysUserUtil.getSessionUser(request).getUserId());
			goodsShow.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			
			
			goodsShowSevice.updateByPkSelective(goodsShow);
			
			result = "{\"code\":\"1\",\"message\":\"操作成功!\"}";
			
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"操作失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * showCouponList:(查询所有已下发的券). <br/>
	 * Date: 2015年12月10日 下午2:11:16 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponName
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/queryCouponList", method = RequestMethod.POST)
	@ResponseBody
	public DataGridModel<CouponExp> showCouponList(String couponName,int page,int rows){
		Page<Coupon> pageObj = null;
		List<CouponExp> list = null;
		DataGridModel<CouponExp> result = null;
		Long count = null;
		
		try {
			pageObj = new Page<Coupon>();  
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			Example example = new Example();
			Criteria criteria = example.createCriteria();
			
			if(!StringUtils.isBlank(couponName)){
				criteria.andLike("cou.COUPON_NAME", "%"+couponName+"%");
			}
			
			list = couponService.queryCouLByName(example, pageObj);
			count = pageObj.getTotalRecord();
			result = new DataGridModel<CouponExp>(list, count);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showGoodsShowCategoryList:(积分兑换下拉框). <br/>
	 * Date: 2015年12月24日 下午5:30:42 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showGoodsShowCategoryList")
	@ResponseBody
	public List<GoodsShowCategory> showGoodsShowCategoryList(){
		List<GoodsShowCategory> result = null;
		
		try {
			
			result = goodsShowSevice.queryGoodsShowCategory(null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * showCategoryList:(商品兑换列表). <br/>
	 * Date: 2015年12月25日 上午10:55:23 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showCategoryList")
	@ResponseBody
	public Map<String, Object> showCategoryList(int page,int rows){
		Map<String, Object> map = null;
		List<GoodsShowCategory> result = null;
		Page<GoodsShowCategory> pageObj = null;
		
		try {
			
			map = new HashMap<String, Object>();
			pageObj = new Page<GoodsShowCategory>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			result = goodsShowSevice.queryGoodsShowCategory(pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * 
	 * showCategoryDetail:(查询商品展示类型详情). <br/>
	 * Date: 2015年12月25日 上午11:02:52 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowCategoryId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value = "showCategoryDetail")
	@ResponseBody
	public GoodsShowCategory showCategoryDetail(Long goodsShowCategoryId){
		GoodsShowCategory result = null;
		
		try {
			
			result = new GoodsShowCategory();
			
			result = goodsShowSevice.queryCategoryDetail(goodsShowCategoryId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * saveGoodsShowCategory:(新建/修改商品展示类型). <br/>
	 * Date: 2015年12月25日 上午11:13:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowCategory
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveGoodsShowCategory")
	@ResponseBody
	public String saveGoodsShowCategory(GoodsShowCategory goodsShowCategory,HttpServletRequest request){
		String result = null;
		
		try {
			//新建
			if(goodsShowCategory.getGoodsShowCategoryId() == null){
				
				goodsShowCategory.setCreateBy(SysUserUtil.getSessionUser(request).getUserId());
				goodsShowCategory.setCreateTime(DateUtils.getCurrentTimeOfDb());
				goodsShowCategory.setStatusId(StatusConstant.GOODS_SHOW_CATEGORY_ENABLE.getId());
				goodsShowSevice.saveCategory(goodsShowCategory);
				
			}else{//修改
				
				goodsShowCategory.setUpdateBy(SysUserUtil.getSessionUser(request).getUserId());
				goodsShowCategory.setUpdateTime(DateUtils.getCurrentTimeOfDb());
				goodsShowSevice.updateCategory(goodsShowCategory);
			}
			
			result = "{\"code\":\"1\",\"message\":\"保存成功!\"}";
			
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"保存失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * removeCategoryList:(批量删除商品展示类型). <br/>
	 * Date: 2015年12月25日 上午10:58:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param goodsShowCategoryIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value = "removeCategoryList")
	@ResponseBody
	public String removeCategoryList(String goodsShowCategoryIds){
		String result = null;
		
		try {
			
			goodsShowSevice.deleteGoodsShowCategoryList(goodsShowCategoryIds);
			
			result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"删除失败!\"}";
			e.printStackTrace();
		}
		
		return result;
	}

	@Autowired
	public void setGoodsShowSevice(GoodsShowSevice goodsShowSevice) {
		this.goodsShowSevice = goodsShowSevice;
	}

	@Autowired
	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}
	
	
	
	@Autowired
	private GoodsShowMemberCardGradeService goodsShowMemberCardGradeService;
	

}

