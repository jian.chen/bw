package com.bw.adv.controller.coupon;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.coupon.model.*;
import com.bw.adv.module.coupon.model.exp.CouponExp;
import com.bw.adv.module.coupon.model.exp.CouponForUse;
import com.bw.adv.module.coupon.model.exp.CouponInstanceRelayExp;
import com.bw.adv.module.coupon.model.exp.CouponRangeExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;
import com.bw.adv.service.coupon.service.CouponService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * ClassName: CouponController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月17日 上午11:31:09 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Controller
@RequestMapping("/couponController")
@SuppressWarnings("unused")
public class CouponController {
	
	private CouponService couponService;
	
	/**
	 * query:查询优惠券列表 <br/>
	 * Date: 2015年8月17日 上午11:31:21 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/queryCouponList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridModel<CouponExp> showCouponList(HttpServletRequest request, String couponName,String isSeachStatus,int page,int rows){
		String couponName1 = null;
		Page<Coupon> pageObj = null;
		List<CouponExp> list = null;
		DataGridModel<CouponExp> result = null;
		Long count = null;
		String isBrowse = null;
		try {
			if(couponName!=null && couponName!=""){
				couponName1 = new String(couponName.getBytes("ISO-8859-1"),"UTF-8");
			}
			Example example = new Example();
			Criteria criteria = example.createCriteria();
			if(couponName1!=null && couponName1!=""){
				criteria.andLike("cou.COUPON_NAME", "%"+couponName1+"%");
			}
			//如果isSeachStatus传值,筛选未过期的券(即已发放&已创建状态,否则查询所有未删除的券)
			if(isSeachStatus != null){
				List<Object> statusList = new ArrayList<Object>();
				statusList.add(StatusConstant.COUPON_NEW.getId());
				statusList.add(StatusConstant.COUPON_ISSUED.getId());
				criteria.andIdIn("cou.STATUS_ID", statusList);
			}else{
				criteria.andNotEqualTo("cou.STATUS_ID", 1405);
			}
			isBrowse = request.getParameter("isBrowse");
			if (isBrowse != null && isBrowse.equals("N")){
				criteria.andEqualTo("cou.ORIGIN_ID", 1);
			}
			pageObj = new Page<Coupon>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			list = couponService.queryCouLByName(example, pageObj);
//			count = (long) couponService.queryCouponNum(couponName);
			count = pageObj.getTotalRecord();
			result = new DataGridModel<CouponExp>(list, count);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * insertCoupon:新增优惠券<br/>
	 * Date: 2015年8月21日 下午6:13:06 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param coupon
	 * @return
	 */
	@RequestMapping(value = "/insertCoupon", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel saveCoupon(Coupon coupon,String equalOrgId){
		JsonModel result = null;
		try {
			result = new JsonModel();
			couponService.insertCoupon(coupon,equalOrgId);
			result.setStatusSuccess();
		} catch (Exception e) {
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * queryCouponDetail:查看优惠券详情 <br/>
	 * Date: 2015年8月21日 下午6:12:09 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponid
	 * @return
	 */
	@RequestMapping(value = "/queryCouponDetail", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showCouponDetail(Long couponid){
		Coupon coupon = null;
		CouponRangeExp couponRangeExp = null;
		Map<String, Object> couponMap = null;
		CouponType couponType = null;
		CouponBusinessType couponBusinessType = null;
		try {
			couponMap = new HashMap<String, Object>();
			coupon = couponService.queryByPk(couponid);
			couponRangeExp = couponService.queryCouponRange(couponid);
			couponType = couponService.selectTypeByPk(coupon.getCouponTypeId());
			couponBusinessType = couponService.selectCouponBusinessByPk(coupon.getCouponBusinessTypeId());
			couponMap.put("coupon", coupon);
			couponMap.put("couponRange", couponRangeExp);
			couponMap.put("couponType", couponType);
			couponMap.put("couponBusinessType", couponBusinessType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return couponMap;
	}
	
	/**
	 * editCoupon:编辑优惠券<br/>
	 * Date: 2015年8月22日 下午4:44:44 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponid
	 * @return
	 */
	@RequestMapping(value = "/editCoupon", method = RequestMethod.POST)
	@ResponseBody
	public String editCoupon(Coupon coupon,String equalOrgId){
		String result = null;
		
		try{
			couponService.updateCoupon(coupon, equalOrgId);
			result = "{\"code\":\"1\",\"message\":\"修改成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"修改失败!\"}";
			e.printStackTrace();
			
		}
		return result;
	}
	/**
	 * removeCoupons:批量删除优惠券 <br/>
	 * Date: 2015年8月24日 下午1:24:37 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponIds
	 * @return
	 */
	@RequestMapping(value = "/removeCoupons", method = RequestMethod.GET)
	@ResponseBody
	public String removeCoupons(String couponids){
		String result = null;
		
		try {
			result = couponService.removeCoupons(couponids);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * uploadCouponImage:上传优惠券大图片 <br/>
	 * Date: 2015年8月31日 下午2:50:39 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponimage
	 * @param couponimageFileName
	 * @return
	 */
	@RequestMapping(value = "/uploadCouponImage", method = RequestMethod.POST)
	@ResponseBody
	public String uploadCouponImage(HttpServletRequest request,HttpServletResponse response){
		MultipartHttpServletRequest multipartRequest = null;
		String logoPathDir = null;
		String logoRealPathDir = null;
		File logoSaveFile = null;
		MultipartFile multipartFile = null;
		String suffix = null;
		String logImageName = null;
		String fileName = null;
		String filePath = null;
		File file = null;
		
	 	multipartRequest = (MultipartHttpServletRequest) request;
	 	/** 构建文件保存的目录* */
        logoPathDir = "/business/shops/upload/coupon/"+ DateUtils.getCurrentTimeOfDb();
        /** 得到文件保存目录的真实路径* */
        logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
        /** 根据真实路径创建目录* */
        logoSaveFile = new File(logoRealPathDir);
        if (!logoSaveFile.exists()){
            logoSaveFile.mkdirs();
        }
        /** 页面控件的文件流* */
        multipartFile = multipartRequest.getFile("couponimage");
	    if (FileUtil.isAllowUpForPicture(multipartFile.getOriginalFilename())) {
	    	/** 获取文件的后缀* */
	    	suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
	    	/** 使用UUID生成文件名称* */
	    	logImageName = UUID.randomUUID().toString() + suffix;// 构建文件名称
	    	/** 拼成完整的文件保存路径加文件* */
	    	fileName = logoRealPathDir + File.separator + logImageName;
	    	file = new File(fileName);
	    	filePath = logoPathDir+ "/" + logImageName;
	        try {
	            multipartFile.transferTo(file);
	        } catch (IllegalStateException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return filePath;
	    }else{
	    	return "不被允许上传的文件格式!";
	    }
	}
	@RequestMapping(value = "/uploadCouponMinImage", method = RequestMethod.POST)
	@ResponseBody
	public String uploadCouponMinImage(HttpServletRequest request,HttpServletResponse response){
		MultipartHttpServletRequest multipartRequest = null;
		String logoPathDir = null;
		String logoRealPathDir = null;
		File logoSaveFile = null;
		MultipartFile multipartFile = null;
		String suffix = null;
		String logImageName = null;
		String fileName = null;
		String filePath = null;
		File file = null;
		
		multipartRequest = (MultipartHttpServletRequest) request;
		/** 构建文件保存的目录* */
		logoPathDir = "/business/shops/upload/coupon/"+ DateUtils.getCurrentTimeOfDb();
		/** 得到文件保存目录的真实路径* */
		logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
		/** 根据真实路径创建目录* */
		logoSaveFile = new File(logoRealPathDir);
		if (!logoSaveFile.exists()){
			logoSaveFile.mkdirs();
		}
		/** 页面控件的文件流* */
		multipartFile = multipartRequest.getFile("couponimage2");
		if (FileUtil.isAllowUpForPicture(multipartFile.getOriginalFilename())) {
			/** 获取文件的后缀* */
			suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
			/** 使用UUID生成文件名称* */
			logImageName = UUID.randomUUID().toString() + suffix;// 构建文件名称
			/** 拼成完整的文件保存路径加文件* */
			fileName = logoRealPathDir + File.separator + logImageName;
			file = new File(fileName);
			filePath = logoPathDir+ "/" + logImageName;
			try {
				multipartFile.transferTo(file);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return filePath;
		}else{
			return "不被允许上传的文件格式!";
		}
	}
	@RequestMapping(value = "/showBusinessType", method = RequestMethod.GET)
	@ResponseBody
	public List<CouponBusinessType> showBusinessType(){
		List<CouponBusinessType> result = null;
		
		try {
			result = couponService.selectCouponBusinessType();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@RequestMapping(value = "/showCouponType", method = RequestMethod.GET)
	@ResponseBody
	public List<CouponType> showCouponType(){
		List<CouponType> result = null;
		
		try {
			result = couponService.selectAllActive();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * showCouponInstanceRelayList:查询转发记录列表(条件). <br/>
	 * Date: 2016年6月1日 上午10:32:33 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param relay
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showCouponInstanceRelayList")
	@ResponseBody
	public Map<String, Object> showCouponInstanceRelayList(CouponInstanceRelayExp relay,int page,int rows) {
		Map<String, Object> result = null;
		List<CouponInstanceRelayExp> list = null;
		Page<CouponInstanceRelayExp> pageObj = null;
		Long count = null;
		
		try {
			result = new HashMap<String, Object>();
			pageObj = new Page<CouponInstanceRelayExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			count = couponService.selectCouponInstanceRelayList(relay, pageObj);			
			result.put("total", count);
			result.put("rows", pageObj.getResults());
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * couponrollback:优惠券退回. <br/>
	 * Date: 2016年6月1日 下午6:15:18 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param couponInstanceId
	 * @param srcId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "couponrollback")
	@ResponseBody
	public JsonModel couponrollback(Long couponInstanceId,Long srcId) {
		JsonModel result = null;
		int count = 0;
		
		try {			
			count = couponService.couponrollback(couponInstanceId, srcId);			
			if(count == 1){
				result = new JsonModel(JsonModel.Status_Success,"退回成功！");
			}
		}catch (Exception ex) {
			ex.printStackTrace();
			result = new JsonModel(JsonModel.Status_Error,"退回失败，请稍后重试！");
		}
		return result;
	}
	
	/**
	 * 查询会员优惠券使用情况
	 * @return
	 */
	@RequestMapping(value = "/queryCouponInstanceForUse", method = RequestMethod.POST)
	@ResponseBody
	public List<CouponForUse> queryCouponInstanceForUse(String couponInstanceCode){
		List<CouponForUse> result = null;
		try {
			result = new ArrayList<CouponForUse>();
			if(StringUtils.isBlank(couponInstanceCode)){
				return result;
			}
			result = couponService.queryCouponInstanceForUse(couponInstanceCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * 查询外部券来源种类列表
	 * @return
	 */
	@RequestMapping(value = "/queryCouponOriginList")
	@ResponseBody
	public List<CouponOrigin> queryCouponOrigin(){
		return couponService.getCouponOriginList();
	}

	/**
	 * 查询商户列表
	 * @return
	 */
	@RequestMapping(value = "/queryExternalCouponMerchants")
	@ResponseBody
	public List<ExternalCouponMerchant> queryExternalCouponMerchantList(int page, int rows){
		return couponService.getExternalCouponMerchantsPaged(page, rows);
	}
	@Autowired
	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

}
