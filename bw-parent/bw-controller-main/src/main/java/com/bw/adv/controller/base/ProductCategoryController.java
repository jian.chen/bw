/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:ProductCategoryController.java
 * Package Name:com.sage.scrm.controller.base
 * Date:2016年1月14日上午11:53:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.base;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.base.model.ProductCategory;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.service.base.service.ProductCategoryService;

/**
 * ClassName:产品类别 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 上午11:53:35 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/productCategoryController")
@SuppressWarnings("unused")
public class ProductCategoryController {

	@Resource
	private ProductCategoryService productCategoryService;
	
	/**
	 * 
	 * showProductCategory:查询所有的产品类别 <br/>
	 * Date: 2016年1月14日 上午11:57:47 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/showProductCategory", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showProductCategory(int page,int rows,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Page<ProductCategory> pageObj = null;
		List<ProductCategory> list = null;
		Map<String, Object> map = null;
		try {	
			map = new HashMap<String, Object>();
			pageObj = new Page<ProductCategory>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			list = productCategoryService.queryProductCategoryPage(pageObj);
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * showProductCategoryDetail:(查询产品类别明细). <br/>
	 * @author chengdi.cui
	 * @param productCategoryId
	 * @return
	 */
	@RequestMapping(value="/showProductCategoryDetail", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel showProductCategoryDetail(Long productCategoryId){
		JsonModel result = null;
		ProductCategory productCategory = null;
		try {
			result = new JsonModel();
			productCategory = productCategoryService.queryByPk(productCategoryId);
			result.setObj(productCategory);
			result.setStatusSuccess();
		} catch (Exception e) {
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * showProductCategoryList:查询全部的产品种类. <br/>
	 * Date: 2016年1月14日 下午12:31:57 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showProductCategoryList")
	@ResponseBody
	public List<ProductCategory> showProductCategoryList(){
		List<ProductCategory> result = null;
		try {
			result = productCategoryService.queryAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * removeCategory:(删除产品类型). <br/>
	 * Date: 2016年3月28日 下午5:14:45 <br/>
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param productCategoryId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "removeCategory")
	@ResponseBody
	public JsonModel removeCategory(Long productCategoryId){
		JsonModel result = null;
		try {
			result = new JsonModel();
			productCategoryService.removeProductCategory(productCategoryId);
			result.setStatusSuccess();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * saveProductCategory:(保存产品类别). <br/>
	 * @author chengdi.cui
	 * @param productCategory
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveProductCategory")
	@ResponseBody
	public JsonModel saveProductCategory(ProductCategory productCategory){
		JsonModel result = null;
		int multAmount = 0;
		try {
			result = new JsonModel();
			if(productCategory.getProductCategoryId() == null){
				productCategory.setProductCategoryId(0L);
			}
			multAmount = productCategoryService.findProductCategoryIsMultiple(productCategory);
			if(multAmount > 0){
				result.setMessage("类别编号或类别名称重复,请重试");
				result.setStatusError();
			}else{
				productCategoryService.saveSelective(productCategory);
				result.setStatusSuccess();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * editProductCategory:(修改产品类别). <br/>
	 * @author chengdi.cui
	 * @param productCategory
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editProductCategory")
	@ResponseBody
	public JsonModel editProductCategory(ProductCategory productCategory){
		JsonModel result = null;
		int multAmount = 0;
		try {
			result = new JsonModel();
			multAmount = productCategoryService.findProductCategoryIsMultiple(productCategory);
			if(multAmount > 0){
				result.setMessage("类别编号或类别名称重复,请重试");
				result.setStatusError();
			}else{
				productCategoryService.updateByPkSelective(productCategory);
				result.setStatusSuccess();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
}

