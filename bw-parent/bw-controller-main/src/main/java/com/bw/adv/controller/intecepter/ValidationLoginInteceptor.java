package com.bw.adv.controller.intecepter;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bw.adv.controller.sys.utils.SysUserUtil;

/**
 * 
 * @author zhouy
 * 
 */
public class ValidationLoginInteceptor implements Filter {

	private String loginPageUrl = "/view/login.jsp";
	private static final List<String> NOT_FILTER_URL = new ArrayList<String>();
	static {
		NOT_FILTER_URL.add("sysuser/sysuserLogin.json");
		NOT_FILTER_URL.add("file/upload.json");
		NOT_FILTER_URL.add("view/login.jsp");
		NOT_FILTER_URL.add("/rs/*");
		NOT_FILTER_URL.add("/js/*");
		NOT_FILTER_URL.add("/css/*");
		NOT_FILTER_URL.add("/images/*");
		NOT_FILTER_URL.add("/test/*");
		
		NOT_FILTER_URL.add("/binding.htm");
		NOT_FILTER_URL.add("/call.json");
		NOT_FILTER_URL.add("/view/binding/*");
	}

	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
			ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String uri = request.getRequestURI();
		String contextPath = request.getContextPath();
		String url = uri.replace(contextPath, "");
		boolean filter = false;
		if (SysUserUtil.getSessionUser(request) == null) {
			for (String path : NOT_FILTER_URL) {
				if ((path.endsWith("*") && url.startsWith(path.replace("*", ""))) || url.contains(path)) {
					filter = true;
				}
			}
		} else {
			filter = true;
		}

		if (filter) {
			chain.doFilter(request, response);
			return;
		}
		
		String requestedWith = request.getHeader("X-Requested-With");
		if (requestedWith != null && requestedWith.contains("XMLHttpRequest")) {
			response.setHeader("sessionstatus", "0");
		} else {
			response.sendRedirect(request.getContextPath() + loginPageUrl);
		}
		return;
	}

	@Override
	public void init(FilterConfig config) throws ServletException {

	}

}
