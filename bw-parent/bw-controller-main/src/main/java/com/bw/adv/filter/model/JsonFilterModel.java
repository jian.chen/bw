package com.bw.adv.filter.model;

import com.bw.adv.filter.utils.DataFilterUtils;
import com.bw.adv.module.common.model.JsonModel;

/**
 * Created by jeoy.zhou on 1/25/16.
 */
public class JsonFilterModel extends JsonModel{

    public JsonFilterModel() {
        super();
    }

    public JsonFilterModel(int status, String message) {
        super(status, message);
    }

    @Override
    public Object getObj() {
        if(super.getObj() == null) return null;
        return DataFilterUtils.dataFilter(super.getObj());
    }
}
