package com.bw.adv.controller.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.module.activity.model.FabuChannel;
import com.bw.adv.module.activity.model.ShakeUserInfo;
import com.bw.adv.module.activity.model.UserAndWinning;
import com.bw.adv.module.activity.model.WinningItem;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.FabuService;
import com.bw.adv.service.activity.service.LotteryService;
import com.bw.adv.service.activity.service.ShakeUserInfoService;

/**
 * 产品发布会代码先保留
 * @author cuicd
 *
 */

@Controller
@RequestMapping("/shakeController")
public class ShakeController {
	
	private ShakeUserInfoService shakeUserInfoService;
	private LotteryService lotteryService;
	private FabuService fabuService;
	
	private static String SHAKE_ACTIVITY ="SHAKE_ACTIVITY";
	private static String SUCCESS ="success";
	private static String ERROR ="fail";

	@RequestMapping(value = "/savePersonInfo", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> savePersonInfo(HttpServletRequest req,HttpServletResponse res){
			
			String phoneNum =null;
			String userName =null;
			ShakeUserInfo shakeUserInfo =null;
			String status =null;
			Map<String, Object> map =null;
		try {
			req.setCharacterEncoding("utf-8");
			map =new HashMap<String,Object>();
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			
			phoneNum = req.getParameter("teleNumber");
			userName = req.getParameter("userName");
			shakeUserInfo = shakeUserInfoService.queryShakeUserInfoByPhoneNum(phoneNum);
			if(shakeUserInfo !=null){
				status =ERROR;
			}
			
			shakeUserInfo =new ShakeUserInfo();
			shakeUserInfo.setActivityTypeName(SHAKE_ACTIVITY);
			shakeUserInfo.setCreateTime(DateUtils.getCurrentTimeOfDb());
			shakeUserInfo.setMobile(userName);
			shakeUserInfo.setUserName(phoneNum);
			shakeUserInfoService.save(shakeUserInfo);
			status =SUCCESS;
		} catch (Exception e) {
			status =ERROR;
			e.printStackTrace();
		}finally{
			map.put("status", status);
			map.put("data", shakeUserInfo.getUserId());
		}
		return map;
	}
	
	/**
	 * 抽奖
	 */
	@RequestMapping(value = "/extractMemberAwards", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> extractMemberAwards(HttpServletRequest req,HttpServletResponse res){
		WinningItem winningItem =null;
		Map<String,Object> map =null;
		String status =null;
		try {
			map =new HashMap<String,Object>();
			Long activityId =Long.valueOf(1);
			String userId = req.getParameter("userId");
			winningItem = lotteryService.extractMemberAwards(Long.valueOf(userId), activityId,null);
			status =SUCCESS;
		} catch (NumberFormatException e) {
			status =ERROR;
			e.printStackTrace();
		}finally{
			map.put("status", status);
			map.put("awardId", winningItem.getAwardsId());
		}
		
		return map;
	}
	
	/**
	 * 查询关注渠道列表
	 * @return
	 */
	@RequestMapping(value = "/queryGz", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> queryGz(){
		Map<String,Object> map =null;
		List<FabuChannel> list = new ArrayList<FabuChannel>();
		try {
			//DbContextHolder.setDbType(DataSourceInstances.dataSource2);
			map = new HashMap<String,Object>();
			list = fabuService.queryAllGroupByChannel();
 			map.put("list", list);
 			//DbContextHolder.clearDbType();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
		
	/**
	 * 查询中奖情况
	 * @return
	 */
	@RequestMapping(value = "/queryZj", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> queryZj(){
		Map<String,Object> map =null;
		List<UserAndWinning> itemList = new ArrayList<UserAndWinning>();
		try {
			map = new HashMap<String,Object>();
			itemList = shakeUserInfoService.selectAllForUserAndWinning();
			map.put("list", itemList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	
	@Autowired
	public void setShakeUserInfoService(ShakeUserInfoService shakeUserInfoService) {
		this.shakeUserInfoService = shakeUserInfoService;
	}
	
	@Autowired
	public void setLotteryService(LotteryService lotteryService) {
		this.lotteryService = lotteryService;
	}

	@Autowired
	public void setFabuService(FabuService fabuService) {
		this.fabuService = fabuService;
	}

}
