package com.bw.adv.controller.sys;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.filter.domain.ScrmAuthorizationInfo;
import com.bw.adv.filter.model.SuperUserConfig;
import com.bw.adv.filter.utils.ShiroConstants;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.SysFunctionOperation;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysRoleData;
import com.bw.adv.module.sys.model.SysRoleDataType;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.exp.SysRoleDataExp;
import com.bw.adv.service.sys.service.SysFunctionOperationService;
import com.bw.adv.service.sys.service.SysFunctionService;
import com.bw.adv.service.sys.service.SysRoleDataService;
import com.bw.adv.service.sys.service.SysRoleService;
import com.bw.adv.service.sys.service.SysUserService;
import com.google.common.collect.Lists;

public class ShiroRealm extends AuthorizingRealm {

    private SysUserService sysUserService;
    private SysRoleService sysRoleService;
    private SysFunctionService sysFunctionService;
    private SysFunctionOperationService sysFunctionOperationService;
    private SysRoleDataService sysRoleDataService;



    public SysUserService getSysUserService() {
        return sysUserService;
    }

    @Autowired
    public void setSysUserService(SysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    public SysRoleService getSysRoleService() {
        return sysRoleService;
    }

    @Autowired
    public void setSysRoleService(SysRoleService sysRoleService) {
        this.sysRoleService = sysRoleService;
    }

    public SysFunctionService getSysFunctionService() {
        return sysFunctionService;
    }

    @Autowired
    public void setSysFunctionService(SysFunctionService sysFunctionService) {
        this.sysFunctionService = sysFunctionService;
    }

    @Autowired
    public void setSysFunctionOperationService(SysFunctionOperationService sysFunctionOperationService) {
        this.sysFunctionOperationService = sysFunctionOperationService;
    }

    public SysRoleDataService getSysRoleDataService() {
        return sysRoleDataService;
    }

    @Autowired
    public void setSysRoleDataService(SysRoleDataService sysRoleDataService) {
        this.sysRoleDataService = sysRoleDataService;
    }

    /**
     * 认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)
            throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
        SysUser sysUser = null;
        String loginName = token.getUsername();
        String loginPass = new String(token.getPassword());
        sysUser = sysUserService.querySysUserByUserName(loginName);//用户信息
        // superUserConfig 超级用户判断
        if (sysUser == null && !SuperUserConfig.getUserName().equals(loginName)) {
            throw new UnknownAccountException();
        }
        if (!(SuperUserConfig.getPassword().equals(loginPass) && SuperUserConfig.getUserName().equals(loginName)) && !sysUser.getPassword().equals(loginPass)) {
            throw new IncorrectCredentialsException();
        }
        if(sysUser == null) {
            sysUser = new SysUser();
            sysUser.setUserName(loginName);
            sysUser.setCn(loginName);
            sysUser.setPassword(loginPass);
            sysUser.setUserId(0l);
        }
        return new SimpleAuthenticationInfo(sysUser, new String(token.getPassword()), getName());
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // 根据用户配置用户与权限
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        Subject currentUser = SecurityUtils.getSubject();
        Object authInfo = currentUser.getSession().getAttribute(ShiroConstants.AUTH_SESSION_KEY);
        SysUser sysUser = (SysUser) principals.getPrimaryPrincipal();
        if (authInfo != null) return (AuthorizationInfo) authInfo;
        if(SuperUserConfig.getUserName().equals(sysUser.getUserName())) {
            AuthorizationInfo info = getSuperUserAuthorizationInfo();
            currentUser.getSession().setAttribute(ShiroConstants.AUTH_SESSION_KEY, info);
            return info;
        }
        // 初始化权限
        ScrmAuthorizationInfo info = new ScrmAuthorizationInfo();
        List<SysFunction> sysFunctionList = null;
        List<SysRole> sysRoles = null;
        Org org = null;
        sysRoles = sysRoleService.findSysRoleListByUserId(sysUser.getUserId());//用户角色信息
        Set<String> roles = new HashSet<String>();
        for (SysRole roleEntity : sysRoles) {
            roles.add(roleEntity.getRoleId() + "");
        }
        info.setRoles(roles);
        sysFunctionList = sysFunctionService.findListForSysFunctionByUserId(sysUser.getUserId(), sysUser.getSystemId());//功能权限
        Set<String> stringPermissions = new HashSet<String>();
        String urlPath = null;

        info.setSysFunctions(sysFunctionList);
        // 功能权限菜单
        for (SysFunction resourceEntity : sysFunctionList) {
            if (StringUtils.isNotBlank((urlPath = resourceEntity.getUrl()))) {
                if (!urlPath.startsWith(ShiroConstants.PATH_PREFIX)) {
                    urlPath = ShiroConstants.PATH_PREFIX + urlPath;
                }
                stringPermissions.add(urlPath);
            }
        }
        // 初始化默认operation权限
        List<SysFunctionOperation> defaultFunctionOperation = sysFunctionOperationService.queryDefaultFunctionOperations();
        // 初始化operation权限
        List<SysFunctionOperation> roleFunctionOperations = sysFunctionOperationService.queryButtonFunctionOperationsByUserId(sysUser.getUserId());
        roleFunctionOperations.addAll(defaultFunctionOperation);
        for (SysFunctionOperation sysFunctionOperation : roleFunctionOperations) {
            if (StringUtils.isNotBlank((urlPath = sysFunctionOperation.getOperationUrl()))) {
                if (!urlPath.startsWith(ShiroConstants.PATH_PREFIX)) {
                    urlPath = ShiroConstants.PATH_PREFIX + urlPath;
                }
                stringPermissions.add(urlPath);
            }
        }
        info.setRoles(roles);
        info.setStringPermissions(stringPermissions);
        // 初始化数据权限配置
        info.initDataFilter(convert(sysRoleDataService.queryByUserId(sysUser.getUserId())));
        currentUser.getSession().setAttribute(ShiroConstants.AUTH_SESSION_KEY, info);
        return info;
    }

    @Override
    public boolean isPermitted(PrincipalCollection principals, String permission) {
        AuthorizationInfo authorizationInfo = getAuthorizationInfo(principals);
        SysUser sysUser = (SysUser) principals.getPrimaryPrincipal();
        return SuperUserConfig.getUserName().equals(sysUser.getUserName()) ? true : authorizationInfo.getStringPermissions().contains(permission);
    }

    private List<SysRoleDataExp> convert(List<SysRoleData> sysRoleDatas) {
        if(CollectionUtils.isEmpty(sysRoleDatas)) {
            return Lists.newArrayList();
        }
        Map<Integer, SysRoleDataType> map = sysRoleDataService.getCacheDataTypeMap();
        List<SysRoleDataExp> sysRoleDataExps = Lists.newArrayList();
        for(SysRoleData sysRoleData : sysRoleDatas) {
            SysRoleDataType sysRoleDataType = map.get(sysRoleData.getDataTypeId());
            sysRoleDataExps.add(new SysRoleDataExp(sysRoleData, sysRoleDataType));
        }
        return sysRoleDataExps;
    }

    private AuthorizationInfo getSuperUserAuthorizationInfo() {
        ScrmAuthorizationInfo info = new ScrmAuthorizationInfo();
        List<SysFunction> sysFunctions = sysFunctionService.findListForSysFunction(SuperUserConfig.getSystemId());
        info.setSysFunctions(sysFunctions);
        return info;
    }
}