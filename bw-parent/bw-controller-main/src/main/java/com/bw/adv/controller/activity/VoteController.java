package com.bw.adv.controller.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.VoteOptions;
import com.bw.adv.module.activity.model.VoteResult;
import com.bw.adv.module.activity.model.VoteTitle;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.VoteService;

@Controller
@RequestMapping("/VoteController")
public class VoteController {
	
	private VoteService voteService;

	/**
	 * 查找所有问卷
	 * @return
	 */
	@RequestMapping(value = "/showVote", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showVote(int page,int rows,HttpServletRequest request){
		Map<String, Object> map = null;
		Page<VoteTitle> templatePage = null;
		try {
			map = new HashMap<String, Object>();
			templatePage = new Page<VoteTitle>();
			this.voteService.findAllVoteAndOptionCount(templatePage);
			templatePage.setPageNo(page);
			templatePage.setPageSize(rows);
			map.put("total", templatePage.getTotalRecord());
			map.put("rows", templatePage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 添加标题
	 * @param voteTitle
	 * @return
	 */
	@RequestMapping(value = "/addVoteTitle", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addVoteTitle(VoteTitle voteTitle){
		Map<String, Object> map = null;
		try {
			voteTitle.setStartDate(DateUtils.formatString(voteTitle.getStartDate()));
			voteTitle.setEndDate(DateUtils.formatString(voteTitle.getEndDate()));
			voteTitle.setCreateDate(DateUtils.getCurrentTimeOfDb());
			voteTitle.setStatusId(StatusConstant.ACTIVITY_DISABLE.getId());
			int id = this.voteService.saveVoteTitle(voteTitle);
			map = new HashMap<String, Object>();
			map.put("message", "新建投票成功！");
			map.put("id", id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 查询需要修改的问卷信息
	 * @param editVoteId
	 * @return
	 */
	@RequestMapping(value = "/showEditVote", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showEditVote(Long editVoteId){
		Map<String, Object> map = null;
		try {
			VoteTitle editResult = this.voteService.findVoteTitleById(editVoteId);
			map = new HashMap<String, Object>();
			map.put("editResult", editResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 修改问卷
	 * @param questionnaire
	 * @return
	 */
	@RequestMapping(value = "/updateVoteTitle", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateVoteTitle(VoteTitle voteTitle){
		Map<String, Object> map = null;
		try {
			voteTitle.setStartDate(DateUtils.formatString(voteTitle.getStartDate()));
			voteTitle.setEndDate(DateUtils.formatString(voteTitle.getEndDate()));
			this.voteService.updateVoteTitle(voteTitle);
			map = new HashMap<String, Object>();
			map.put("message", "更新投票成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 删除问卷
	 * @param questionnaireId
	 * @return
	 */
	@RequestMapping(value = "/deleteVoteTitle", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> deleteVoteTitle(Long titleId){
		Map<String, Object> map = null;
		try {
			this.voteService.deleteVoteTitle(titleId);
			map = new HashMap<String, Object>();
			map.put("code", "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/showVoteOptions", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showVoteOptions(int page,int rows,HttpServletRequest request,Long titleId){
		Map<String, Object> map = null;
		Page<VoteOptions> templatePage = null;
		try {
			map = new HashMap<String, Object>();
			templatePage = new Page<VoteOptions>();
			this.voteService.findVoteOptionsByTitleId(titleId, templatePage);
			templatePage.setPageNo(page);
			templatePage.setPageSize(rows);
			map.put("total", templatePage.getTotalRecord());
			map.put("rows", templatePage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/saveOptions", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveOptions(VoteOptions voteOptions){
		Map<String, Object> map = null;
		try {
			map = new HashMap<String, Object>();
			this.voteService.saveVoteOptions(voteOptions);
			map.put("message", "新建选项成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/updateOptions", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateOptions(VoteOptions voteOptions){
		Map<String, Object> map = null;
		try {
			map = new HashMap<String, Object>();
			this.voteService.updateVoteOptions(voteOptions);
			map.put("message", "更新选项成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/findOptionsById", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> findOptionsById(Long optionsId){
		Map<String, Object> map = null;
		try {
			map = new HashMap<String, Object>();
			VoteOptions result = this.voteService.findOptionsById(optionsId);
			map.put("result", result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 删除问卷
	 * @param questionnaireId
	 * @return
	 */
	@RequestMapping(value = "/delOptions", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> delOptions(Long optionsId){
		Map<String, Object> map = null;
		try {
			this.voteService.deleteVoteOptions(optionsId);
			map = new HashMap<String, Object>();
			map.put("message", "删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/voteInstanceAllMember", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> voteInstanceAllMember(Long titleId){
		Map<String, Object> map = null;
		try {
			map = new HashMap<String, Object>();
			Long sum = this.voteService.selectCountForVoteResult(titleId);
			map.put("sum", sum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 查询问卷的结果
	 * @param titleId
	 * @return
	 */
	@RequestMapping(value = "/showVoteCount", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> showVoteCount(Long titleId){
		Map<String, Object> map = null;
		try {
			map = new HashMap<String, Object>();
			List<VoteResult> result = this.voteService.findVoteResultByTitleId(titleId);
			map.put("result", result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/changeVoteStatus", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel changeVoteStatus(Long titleId,String status){
		JsonModel result = null;
		VoteTitle voteTitle = new VoteTitle();
		try {
			result = new JsonModel();
			if(status.equals("Y")){
				Long activieAmount = this.voteService.queryActiveVoteAmount();
				if(activieAmount > 0){
					result.setMessage("同一时间只能有一份投票启用!");
					result.setStatusError();
					return result;
				}else{
					voteTitle.setTitleId(titleId);
					voteTitle.setStatusId(StatusConstant.ACTIVITY_ENABLE.getId());
					this.voteService.updateVoteTitle(voteTitle);
				}
			}
			if(status.equals("N")){
				voteTitle.setTitleId(titleId);
				voteTitle.setStatusId(StatusConstant.ACTIVITY_DISABLE.getId());
				this.voteService.updateVoteTitle(voteTitle);
			}
			result.setStatusSuccess();
			result.setMessage("操作成功!");
		} catch (Exception e) {
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}
	

	@Autowired
	public void setVoteService(VoteService voteService) {
		this.voteService = voteService;
	}
}
