package com.bw.adv.controller.sys;

import java.awt.image.BufferedImage;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.impl.DefaultKaptcha;

@Controller("ValidateCodeController")
public class ValidateCodeController {


	@Resource(name = "captchaProducer")
	private DefaultKaptcha    producer;

	@RequestMapping(method = RequestMethod.GET, value = "validatecode")
	public void execute(HttpServletResponse response, HttpSession session) throws Exception {
		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		response.setContentType("image/jpeg");

		String text = producer.createText();
		this.setValidateCode(text, session);
		BufferedImage image = producer.createImage(text);
		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(image, "jpg", out);
		try {
			out.flush();
		}
		finally {
			out.close();
		}
	}
	
	private void setValidateCode(String newValidateCode, HttpSession session) {

		session.setAttribute("KAPTCHA_SESSION_TIME", System.currentTimeMillis());
		session.setAttribute(Constants.KAPTCHA_SESSION_KEY, newValidateCode);
	}
	
	
}