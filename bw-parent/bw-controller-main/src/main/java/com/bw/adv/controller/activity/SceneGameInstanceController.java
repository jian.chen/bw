/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:SceneGameInstanceController.java
 * Package Name:com.sage.scrm.controller.activity
 * Date:2016年1月15日下午2:30:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.SceneGameInstance;
import com.bw.adv.module.activity.model.exp.AwardsExp;
import com.bw.adv.module.activity.model.exp.WinningItemExp;
import com.bw.adv.module.base.exception.ActivityException;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.repository.MemberGroupRepository;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.repository.MemberTagRepository;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.activity.service.AwardsService;
import com.bw.adv.service.activity.service.SceneGameInstanceService;
import com.bw.adv.service.activity.service.WinningItemService;


/**
 * ClassName:SceneGameInstanceController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午2:30:39 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping(value="/sceneGameInstanceController")
public class SceneGameInstanceController {

	@Autowired
	private SceneGameInstanceService sceneGameInstanceService;
	
	@Autowired
	private AwardsService awardsService;
	
	@Autowired
	private WinningItemService winningItemService;
	
	@Autowired
	private MemberTagRepository memberTagRepository;
	
	@Autowired
	private MemberGroupRepository memberGroupRepository;
	
	/**
	 * 
	 * saveSceneGameInstance:保存新建游戏场景实例 <br/>
	 * Date: 2016年1月17日 下午2:26:06 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param request
	 * @param sceneGameInstance
	 * @return
	 */
	@RequestMapping(value = "/saveSceneGameInstance")
	@ResponseBody
	public Map<String, String> saveSceneGameInstance(HttpServletRequest request,String params){
		Map<String, String> map = new HashMap<String, String>();
		JSONObject json = null;
		try {
			json = JSONObject.parseObject(params);
			if(checkSubmit(json,map)){
				sceneGameInstanceService.saveSceneGameInstance(json,SysUserUtil.getSessionUser(request));
			}else{
				return map;
			}
			map.put("code", "SUCCESS");
			map.put("message", "新建成功！");
		} catch (ActivityException e) {
			map.put("code", "FAIL");
			map.put("message", e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			map.put("code", "FAIL");
			map.put("message", "新建失败！");
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 
	 * editSceneGameInstance:编辑活动实例. <br/>
	 * Date: 2016年1月19日 下午3:37:39 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param request
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/editSceneGameInstance")
	@ResponseBody
	public Map<String, String> editSceneGameInstance(HttpServletRequest request,String params){
		Map<String, String> map = new HashMap<String, String>();
		JSONObject json = null;
		try {
			json = JSONObject.parseObject(params);
			if(checkSubmit(json,map)){
				sceneGameInstanceService.editSceneGameInstance(json,SysUserUtil.getSessionUser(request));
			}else{
				return map;
			}
			map.put("code", "SUCCESS");
			map.put("message", "编辑成功！");
		} catch (Exception e) {
			map.put("code", "FAIL");
			map.put("message", "编辑失败！");
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/editSceneGameInstanceIsUse")
	@ResponseBody
	public Map<String, String> editSceneGameInstanceIsUse(HttpServletRequest request,String params){
		Map<String, String> map = new HashMap<String, String>();
		JSONObject json = null;
		try {
			json = JSONObject.parseObject(params);
			if(checkSubmit(json,map)){
				sceneGameInstanceService.editSceneGameInstanceIsUse(json,SysUserUtil.getSessionUser(request));
			}else{
				return map;
			}
			map.put("code", "SUCCESS");
			map.put("message", "编辑成功！");
		} catch (Exception e) {
			map.put("code", "FAIL");
			map.put("message", "编辑失败！");
			e.printStackTrace();
		}
		return map;
	}
	
	
	public boolean checkSubmit(JSONObject json,Map<String, String> map){
		
		//判断活动名称
		if(StringUtils.isEmpty(json.getString("instanceName"))){
			map.put("status", "FAIL");
			map.put("message", "活动名称不能为空");
			return false;
		}
		//活动开始结束时间不能为空
		if(StringUtils.isEmpty(json.getString("startTime"))){
			map.put("status", "FAIL");
			map.put("message", "活动开始时间不能为空");
			return false;
		}
		if(StringUtils.isEmpty(json.getString("endTime"))){
			map.put("status", "FAIL");
			map.put("message", "活动结束不能为空");
			return false;			
		}
		if(StringUtils.isEmpty(json.getString("luckyDrawType"))){
			map.put("status", "FAIL");
			map.put("message", "抽奖机会类型不能为空");
			return false;
		}
		if(StringUtils.isEmpty(json.getString("luckyDrawValue"))){
			map.put("status", "FAIL");
			map.put("message", "抽奖次数不能为空");
			return false;
		}
		return true;
	}
	/**
	 * showSceneGameInstanceList:加载场景实例. <br/>
	 * Date: 2016年1月18日 下午5:25:53 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @param sceneGameId
	 * @return
	 */
	@RequestMapping(value = "/showSceneGameInstanceList")
	@ResponseBody
	public Map<String, Object> showSceneGameInstanceList(HttpServletRequest request,HttpServletResponse response,Long sceneGameId,int page,int rows){
		Map<String, Object> map = null;
		Page<SceneGameInstance> sceneGameInstancePage = null;
		try {
			map = new HashMap<String, Object>();
			sceneGameInstancePage = new Page<SceneGameInstance>();
			sceneGameInstancePage.setPageNo(page);
			sceneGameInstancePage.setPageSize(rows);
			sceneGameInstanceService.querySceneGameInstanceByGameId(sceneGameId,sceneGameInstancePage);
			map.put("total", sceneGameInstancePage.getTotalRecord());
			map.put("rows", sceneGameInstancePage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * querySceneGameInstanceDetil:查询活动实例明细. <br/>
	 * Date: 2016年1月19日 上午10:53:51 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @param sceneGameId
	 * @return
	 */
	@RequestMapping(value = "/querySceneGameInstanceDetil")
	@ResponseBody
	public Map<String, Object> querySceneGameInstanceDetil(HttpServletRequest request,HttpServletResponse response,Long sceneGameInstanceId){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			//查询活动实例
			SceneGameInstance sceneGameInstance = sceneGameInstanceService.queryByPk(sceneGameInstanceId);
			if(sceneGameInstance!=null){
				sceneGameInstance.setStartTime(DateUtils.dateStrToNewFormat(sceneGameInstance.getStartTime(), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss, DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2));
				sceneGameInstance.setEndTime(DateUtils.dateStrToNewFormat(sceneGameInstance.getEndTime(), DateUtils.DATE_PATTERN_YYYYMMDDHHmmss, DateUtils.DATE_PATTERN_YYYYMMDDHHmmss_2));
				//查询会员标签
				List<MemberTag> memberTags = null;
				if(sceneGameInstance.getSptIsusememberlabel()!=null && "1".equals(sceneGameInstance.getSptIsusememberlabel())){
					String[] arr = sceneGameInstance.getSptMlrid().split(",");
					memberTags = memberTagRepository.findTagByIdList(arr);
				}
				//查询会员标签
				MemberGroup memberGroup = null;
				if(sceneGameInstance.getSptIsuseforgroupmember()!=null && "1".equals(sceneGameInstance.getSptIsuseforgroupmember())){
					memberGroup = memberGroupRepository.findByPk(sceneGameInstance.getSptMgoid());
				}
				map.put("status", "SUCCESS");
				map.put("sceneGameInstance", sceneGameInstance);
				map.put("memberTags", memberTags);
				map.put("memberGroup", memberGroup);
				if(memberTags!=null){
					map.put("memberTagCount",memberTags.size());
				}else{
					map.put("memberTagCount",0);
				}
			}else{
				map.put("status", "FAIL");
			}
		} catch (Exception e) {
			map.put("status", "FAIL");
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 
	 * queryAwardsDetail:查询活动实例对应的奖项. <br/>
	 * Date: 2016年1月19日 下午12:07:13 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @param sceneGameInstanceId
	 * @return
	 */
	@RequestMapping(value = "/queryAwardsDetail")
	@ResponseBody
	public Map<String, Object> queryAwardsDetail(HttpServletRequest request,HttpServletResponse response,Long sceneGameInstanceId){
		Map<String, Object> map = new HashMap<String, Object>();
		List<AwardsExp> awardsExps= null;
		try{
			awardsExps = awardsService.queryAwardsBySceneGameInstanceId(sceneGameInstanceId);
			map.put("awardsExps", awardsExps);
			map.put("total", awardsExps.size());
			map.put("status", "SUCCESS");
		}catch(Exception e){
			map.put("status", "FAIL");
			e.printStackTrace();
		}
		
		return map;
	}
	
	@RequestMapping(value = "/editStatus")
	@ResponseBody
	public Map<String, Object> editStatus(HttpServletRequest request,HttpServletResponse response,Long sceneGameInstanceId,String status,Long sceneGameId){
		Map<String, Object> map = new HashMap<String, Object>();
		SceneGameInstance sceneGameInstance= new SceneGameInstance();
		try{
			if("close".equals(status)){
				sceneGameInstance.setStatusId(StatusConstant.SCENE_GAME_INSTANCE_CLOSE.getId());
			}else if("open".equals(status)){
				sceneGameInstance.setStatusId(StatusConstant.SCENE_GAME_INSTANCE_ENABLE.getId());
				//活动开启只允许有一个活动实例
				int count = sceneGameInstanceService.countOpenSceneGameInstance(sceneGameId);
				if(count>0){
					map.put("status", "FAIL");
					map.put("message", "一个场景允许有一个激活的实例！");
					return map;
				}
			}else {
				map.put("status", "FAIL");
				map.put("message", "操作失败！");
				return map;
			}
			sceneGameInstance.setSceneGameInstanceId(sceneGameInstanceId);
			int count = sceneGameInstanceService.editSceneGameInstanceStatus(sceneGameInstance);
			if(count>0){
				map.put("status", "SUCCESS");
				map.put("message", "操作成功！");
			}else{
				map.put("status", "FAIL");
				map.put("message", "操作失败！");
			}
		}catch(Exception e){
			map.put("status", "FAIL");
			map.put("message", "操作失败！");
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * showGameInstanceRecord:查询概率游戏实例中枪记录. <br/>
	 * Date: 2016年1月20日 下午3:16:20 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param activityInstanceId
	 * @param recordMobile
	 * @param page
	 * @param rows
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/showGameInstanceRecord")
	@ResponseBody
	public Map<String, Object> showGameInstanceRecord(Long sceneGameInstanceId,String memberName,String memberCode,int page,int rows,HttpServletRequest request){
		Map<String, Object> map = null;
		Page<WinningItemExp> recordPage = null;
		Example example = null;
		try {
			map = new HashMap<String, Object>();
			recordPage = new Page<WinningItemExp>();
			recordPage.setPageNo(page);
			recordPage.setPageSize(rows);
			
			example = new Example();
			Criteria temp = example.createCriteria();
			temp.andEqualTo("wi.SCENE_GAME_INSTANCE_ID", sceneGameInstanceId);
			if(StringUtils.isNotBlank(memberName)){
				temp.andLike("me.MEMBER_NAME", "%"+memberName+"%");
			}
			if(StringUtils.isNotBlank(memberCode)){
				temp.andLike("me.MEMBER_CODE", "%"+memberCode+"%");
			}
			winningItemService.findWinningItemListByMemberIdAndActivityId(example, recordPage);
			map.put("total", recordPage.getTotalRecord());
			map.put("rows", recordPage.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}

