/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:PointsItemController.java
 * Package Name:com.sage.scrm.controller.points
 * Date:2015年9月1日下午2:27:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.points;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.points.model.PointsConf;
import com.bw.adv.service.points.service.PointsConfSevice;

/**
 * ClassName: PointsConfController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016-3-18 下午4:04:41 <br/>
 * scrmVersion standard
 * @author mennan
 * @version jdk1.7
 */
@Controller
@RequestMapping("/pointsConfController")
@SuppressWarnings("unused")
public class PointsConfController {
	
	private PointsConfSevice pointsConfSevice;
	
	@RequestMapping(method = RequestMethod.POST, value = "savePointsConf")
	@ResponseBody
	public JsonModel savePointsConf(PointsConf pointsConf) {
		JsonModel result = null;
		String memberCode = null;
		MemberSearch search = null;
		PointsConf conf = null;
		try {
			conf = pointsConfSevice.queryByPk(1L);
			if(conf == null){
				pointsConf.setPointsConfId(1L);
				pointsConfSevice.save(pointsConf);
			}else{
				pointsConf.setPointsConfId(1L);
				pointsConfSevice.updateByPk(pointsConf);
			}
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "showPointsConf")
	@ResponseBody
	public JsonModel showPointsConf(PointsConf pointsConf) {
		JsonModel result = null;
		String memberCode = null;
		MemberSearch search = null;
		PointsConf conf = null;
		try {
			conf = pointsConfSevice.queryByPk(1L);
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			result.setObj(conf);
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	@Autowired
	public void setPointsConfSevice(PointsConfSevice pointsConfSevice) {
		this.pointsConfSevice = pointsConfSevice;
	}
}

