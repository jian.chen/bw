package com.bw.adv.controller.wechat;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.service.wechat.service.WechatMessageAutoReplyService;

@Controller
@RequestMapping("/WechatMessageAutoReplyController")
@SuppressWarnings("unused")
public class WechatMessageAutoReplyController {

	private WechatMessageAutoReplyService wechatMessageAutoReplyService;

	@RequestMapping(value="/sendWechatMessageAutoReply", method = RequestMethod.POST)
	@ResponseBody
	public String sendWechatMessageAutoReply(MultipartHttpServletRequest request) throws IOException{
		String result = null;
		Map<String,Object> paramsMap=null;//参数map
		String content=null;//内容
		String wechatMessageType=null;//发送模式（图文/文本/图片）
		String replytype=null;//自动回复类型（1:关注回复 2:消息自动回复 3:关键字回复 ）
		String filePath = "";
		String fileUrl = "";
		String strBackUrl=null;//图片url
		Long userid=null;
		
		try {	
			content=request.getParameter("content");
			wechatMessageType=request.getParameter("wechatMessageType");
			replytype=request.getParameter("replyType");
			userid=SysUserUtil.getSessionUser(request).getUserId();
			
			if(wechatMessageType.equals("image")){
				//文件上传到本地服务器,返回映射的路径
				filePath = FileUtils.upload(request);
				fileUrl = request.getSession().getServletContext().getRealPath("/") + filePath;
				strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+filePath;
				System.out.println(filePath);
				System.out.println(fileUrl);
				System.out.println(strBackUrl);
			}
			
			paramsMap=new HashMap<String, Object>();
			paramsMap.put("content", content);
			paramsMap.put("wechatMessageType", wechatMessageType);
			paramsMap.put("replytype", replytype);
			paramsMap.put("strBackUrl", strBackUrl);
			paramsMap.put("userid", userid);
			
			wechatMessageAutoReplyService.sendWechatMessageAutoReply(paramsMap);
			
			result = "{\"code\":\"1\",\"message\":\"发送成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"发送失败!\"}";
			e.printStackTrace();
		}
		return result;
	}

	@Autowired
	public void setWechatMessageAutoReplyService(
			WechatMessageAutoReplyService wechatMessageAutoReplyService) {
		this.wechatMessageAutoReplyService = wechatMessageAutoReplyService;
	}	

	
}
