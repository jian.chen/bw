package com.bw.adv.controller.wechat;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.wechat.model.WechatTemplateMessage;
import com.bw.adv.module.wechat.model.WechatTemplateMessageItem;
import com.bw.adv.service.wechat.service.WechatTemplateMessageItemService;
import com.bw.adv.service.wechat.service.WechatTemplateMessageService;

  
 
@Controller
@RequestMapping("/wechatTemplateMessageController")
@SuppressWarnings("unused")
public class WechatTemplateMessageController {

	private WechatTemplateMessageService wechatTemplateMessageService;
	
	private WechatTemplateMessageItemService wechatTemplateMessageItemService;

	/**
	 * showWechatMessageTemplate:查询模板消息列表. <br/>
	 * Date: 2015-9-2 下午3:52:00 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/showWechatTemplateMessage", method = RequestMethod.GET)
	@ResponseBody
	public List<WechatTemplateMessage> showWechatTemplateMessage() throws IOException{
		List<WechatTemplateMessage> wechatTemplateMessageList=null;
		Page<WechatTemplateMessage> page = null;
		try {		
			wechatTemplateMessageList=wechatTemplateMessageService.queryWechatTemplateMessageList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return wechatTemplateMessageList;
	}
	
	/**
	 * wechatTemplateMessageItemDetail:(通过模板消息ID查询模板消息item明细). <br/>
	 * Date: 2015-9-2 下午5:14:27 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param templateMessId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/wechatTemplateMessageItemDetail", method = RequestMethod.GET)
	@ResponseBody
	public WechatTemplateMessageItem wechatTemplateMessageItemDetail(Long templateMessId) throws IOException{
		WechatTemplateMessageItem wechatTemplateMessageItem=null;
		try {		
			wechatTemplateMessageItem=new WechatTemplateMessageItem();
			wechatTemplateMessageItem=wechatTemplateMessageItemService.queryWechatTemplateMessageItemByTemplateMessId(templateMessId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return wechatTemplateMessageItem;
	}
	
	@RequestMapping(value="/saveTemplateMessageItem", method = RequestMethod.POST)
	@ResponseBody
	public String saveTemplateMessageItem(WechatTemplateMessageItem wechatTemplateMessageItem) throws IOException{
		String result = null;
		try {		
			wechatTemplateMessageItemService.updateByPkSelective(wechatTemplateMessageItem);
			result = "{\"code\":\"1\",\"message\":\"保存成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"保存失败!\"}";
			e.printStackTrace();
		}
		return result;
	}	
	@Autowired
	public void setWechatTemplateMessageService(
			WechatTemplateMessageService wechatTemplateMessageService) {
		this.wechatTemplateMessageService = wechatTemplateMessageService;
	}
	@Autowired
	public void setWechatTemplateMessageItemService(
			WechatTemplateMessageItemService wechatTemplateMessageItemService) {
		this.wechatTemplateMessageItemService = wechatTemplateMessageItemService;
	}	

	
}
