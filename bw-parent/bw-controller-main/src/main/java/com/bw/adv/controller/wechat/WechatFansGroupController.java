/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:WechatFansGroupController.java
 * Package Name:com.sage.scrm.controller.wechat
 * Date:2015年9月5日下午7:18:14
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.wechat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.wechat.model.WechatFansGroup;
import com.bw.adv.module.wechat.model.exp.WechatFansExp;
import com.bw.adv.service.wechat.service.WeChatFansService;
import com.bw.adv.service.wechat.service.WechatFansGroupItemService;
import com.bw.adv.service.wechat.service.WechatFansGroupService;

/**
 * ClassName:WechatFansGroupController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月5日 下午7:18:14 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/wechatFansGroupController")
public class WechatFansGroupController {
	
	protected final Logger logger = Logger.getLogger(WechatFansGroupController.class);
	private WeChatFansService weChatFansService;
	private WechatFansGroupItemService wechatFansGroupItemService;
	private WechatFansGroupService wechatFansGroupService;
	
	/**
	 * showWechatFansGroupList:(初始化粉丝分组数据). <br/>
	 * Date: 2015年9月5日 下午7:19:15 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/showWechatFansGroupList", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showWechatFansGroupList(HttpServletRequest request){
		// 第几页
		String page =null;
		// 页大小
		String rows =null;
		// 昵称
		String nickName =null;
		// 组id
		String wechatGroupId =null;
		
		Map<String, Object> map = null;
		Page<WechatFansExp> wechatFansPage =null;
		Example example =null;
		Criteria criteria =null;
		List<WechatFansExp> wechatFans =null;
		List<WechatFansExp> wechatFansExpList =null;
		try {
			example = new Example();
			map = new HashMap<String, Object>();
			wechatFansPage = new Page<WechatFansExp>();
			criteria = example.createCriteria();
			nickName = request.getParameter("nickName");
			wechatGroupId = request.getParameter("wechatGroupId");
			page =request.getParameter("page");
			rows =request.getParameter("rows");
			// 筛选出有效的粉丝分组
			criteria.andEqualTo("wfgi.IS_DELETE ", WechatTypeConstant.UNDELETED.getCode());
			if(!StringUtils.isEmpty(nickName)){
				criteria.andLike("wf.NICK_NAME", nickName);
			}
			
			if(!StringUtils.isEmpty(wechatGroupId)){
				criteria.andEqualTo("wfg.WECHAT_FANS_GROUP_ID", wechatGroupId);
			}
			
			if(!StringUtils.isEmpty(page)){
				wechatFansPage.setPageNo(new Integer(page));
			}
			if(!StringUtils.isEmpty(rows)){
				wechatFansPage.setPageSize(new Integer(rows));
			}
			weChatFansService.queryWechatFansExpList(example, wechatFansPage);
			wechatFans =wechatFansPage.getResults();
			wechatFans = weChatFansService.modifyWechatFansExp(wechatFans);
//			wechatFansExpList = wechatFansGroupItemService.transToGroupItemExp(wechatFans,wechatGroupId);
			map.put("total", wechatFans.size());
			map.put("rows", wechatFans);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 
	 * showWechatFansDetail:(查询分组列表). <br/>
	 * Date: 2015年8月31日 下午5:14:36 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/showWechatFansGroup", method=RequestMethod.GET)
	@ResponseBody
	public List<WechatFansGroup> showWechatFansGroup(HttpServletRequest request){
		List<WechatFansGroup> wechatFansGroupList =null;
		try {
			wechatFansGroupList = wechatFansGroupService.queryAllGroups();
			//将微信默认的三个分组不显示
			// wechatFansGroupList = wechatFansGroupService.removeWechatDefaultGroup(wechatFansGroupList);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return wechatFansGroupList;
	}
	
	@Autowired
	public void setWeChatFansService(WeChatFansService weChatFansService) {
		this.weChatFansService = weChatFansService;
	}
	
	@Autowired
	public void setWechatFansGroupItemService(
			WechatFansGroupItemService wechatFansGroupItemService) {
		this.wechatFansGroupItemService = wechatFansGroupItemService;
	}
	
	@Autowired
	public void setWechatFansGroupService(
			WechatFansGroupService wechatFansGroupService) {
		this.wechatFansGroupService = wechatFansGroupService;
	}

}

