/**

 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:WechatFansImageTextController.java
 * Package Name:com.sage.scrm.controller.wechat
 * Date:2015年9月7日下午9:22:57
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.wechat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.module.common.constant.WechatTypeConstant;
import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.module.wechat.api.ApiWechatMaterialUpload;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.model.WeixinMedia;
import com.bw.adv.service.wechat.service.WechatMessageTemplateService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * ClassName:WechatFansImageTextController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月7日 下午9:22:57 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/wechatFansImageTextController")
public class WechatFansImageTextController {
	
	private ApiWechatMaterialUpload apiWechatMaterialUpload;
	private WechatMessageTemplateService wechatMessageTemplateService;

	@RequestMapping(method = RequestMethod.POST, value = "uploadTextImageInfo")
	@ResponseBody
	public List<WechatMessageTemplate> uploadTextImageInfo(MultipartHttpServletRequest request) {
		// 前台传过来的数据
		String data =null;
		List<String> filePathList = null;
		List<WechatMessageTemplate> tempList = null;
		WeixinMedia media = null;
		String strBackUrl =null;
		MultipartHttpServletRequest multipartRequest =null;
		String filePath =null;
		// 多图文微信返回的media_id列表
		List<WeixinMedia> mediaList =null;
		// json
		JSONArray dataJson =null;
		JSONObject wechatReponseJson =null;
		String firstTemplateId =null;
		// 第一条图文消息记录
		WechatMessageTemplate wechatMessageTemplate =null;
		try {
			multipartRequest =(MultipartHttpServletRequest)request;
			strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
			filePathList =new ArrayList<String>();
			mediaList =new ArrayList<WeixinMedia>();
			data =multipartRequest.getParameter("ImageTextInfoInput");
			dataJson = JSONArray.fromObject(data);
			//文件上传到本地服务器,返回映射的路径 
			filePathList = FileUtils.uploadImages(request);
			for(int i=0;i<filePathList.size();i++){
				filePath =filePathList.get(i);
				media = apiWechatMaterialUpload.uploadMedia("image", strBackUrl+filePath);
				mediaList.add(media);
			}
			// 微信多图文上传
			wechatReponseJson = apiWechatMaterialUpload.upLoadMultiImageText(mediaList,dataJson);
			// 本地保存图文信息
			firstTemplateId =wechatMessageTemplateService.saveImageTextInfo(mediaList,dataJson,wechatReponseJson,strBackUrl,filePathList);
			wechatMessageTemplate =wechatMessageTemplateService.queryByPk(Long.valueOf(firstTemplateId));
			wechatMessageTemplate.setMediaId(wechatReponseJson.getString("media_id"));
			wechatMessageTemplateService.updateByPk(wechatMessageTemplate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tempList;
	}
	
	/**
	 * 初始化图文列表
	 * showImageTextList:(). <br/>
	 * Date: 2015年9月10日 下午5:22:41 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showImageTextList")
	@ResponseBody
	public Map<String, Object> showImageTextList(HttpServletRequest request) {
		
		Map<String,Object> responseMap =null;
		Map<Long,List<WechatMessageTemplate>> map =null;
		responseMap =new HashMap<String,Object>();
		List<List<WechatMessageTemplate>> total =null;
		
		map =new HashMap<Long,List<WechatMessageTemplate>>();
		total= new ArrayList<List<WechatMessageTemplate>>();
		List<WechatMessageTemplate> list =
					wechatMessageTemplateService.queryParentImageText();
		Long index =Long.valueOf(0);
		if(list!=null){
			for(WechatMessageTemplate wechatMessageTemplate:list){
				List<WechatMessageTemplate> singleImageText =new ArrayList<WechatMessageTemplate>();
				singleImageText.add(wechatMessageTemplate);
				
				List<WechatMessageTemplate> subList =
						wechatMessageTemplateService.querySubImageTextList(wechatMessageTemplate.getWechatMessageTemplateId()+"");
				if(subList !=null){
					for(WechatMessageTemplate info :subList){
						singleImageText.add(info);
					}
				}
				total.add(singleImageText);
				/*map.put(index, singleImageText);
				index++;*/
			}
		}
		responseMap.put("data", total);
		System.out.println("responseMap:"+responseMap);
		return responseMap;
	}
	
	/**
	 * deleteImageText:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年9月14日 上午10:52:36 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "deleteImageText")
	@ResponseBody
	public Map<String, Object> deleteImageText(HttpServletRequest request) {
		String wechatMessTemplateId =null;
		WechatMessageTemplate wechatMessageTemplate =null;
		Map<String, Object> map =null;
		String status =null;
		try {
			map =new HashMap<String,Object>();
			wechatMessTemplateId = request.getParameter("id");
			wechatMessageTemplate = wechatMessageTemplateService.queryByPk(wechatMessTemplateId);
			wechatMessageTemplateService.deleteWechatMessageTemplate(wechatMessageTemplate);
			status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			e.printStackTrace();
		}finally{
			map.put("status", status);
		}
		return map;
	}
	
	/**
	 * initImageTextDetail:(初始化图文信息). <br/>
	 * Date: 2015年9月14日 下午8:18:03 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "initImageTextDetail")
	@ResponseBody
	public Map<String, Object> initImageTextDetail(HttpServletRequest request) {
		String wechatMessTemplateId =null;
		WechatMessageTemplate wechatMessageTemplate =null;
		Map<String, Object> map =null;
		String status =null;
		List<WechatMessageTemplate> subList =null;
		List<WechatMessageTemplate> imageTextInfo =null;
		try {
			imageTextInfo =new ArrayList<WechatMessageTemplate>();
			map =new HashMap<String,Object>();
			wechatMessTemplateId = request.getParameter("wechatTemplateId");
			wechatMessageTemplate = wechatMessageTemplateService.queryByPk(wechatMessTemplateId);
			subList = wechatMessageTemplateService.querySubImageTextList(wechatMessageTemplate.getWechatMessageTemplateId()+"");
			imageTextInfo.add(wechatMessageTemplate);
			for(WechatMessageTemplate wechatMesTemplate:subList){
				imageTextInfo.add(wechatMesTemplate);
			}
			status =WechatTypeConstant.WECHAT_SUCESS_CODE.getMessage();
		} catch (Exception e) {
			status =WechatTypeConstant.WECHAT_FAILE_CODE.getMessage();
			e.printStackTrace();
		}finally{
			map.put("status", status);
			map.put("data", imageTextInfo);
		}
		return map;
	}
	
	
	
	@Autowired
	public void setApiWechatMaterialUpload(
			ApiWechatMaterialUpload apiWechatMaterialUpload) {
		this.apiWechatMaterialUpload = apiWechatMaterialUpload;
	}
	
	@Autowired
	public void setWechatMessageTemplateService(
			WechatMessageTemplateService wechatMessageTemplateService) {
		this.wechatMessageTemplateService = wechatMessageTemplateService;
	}

	
}

