package com.bw.adv.controller.coupon;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.CouponBusinessType;
import com.bw.adv.module.coupon.model.CouponRange;
import com.bw.adv.module.coupon.model.CouponType;
import com.bw.adv.module.coupon.model.exp.CouponExp;
import com.bw.adv.module.coupon.model.exp.CouponRangeExp;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;
import com.bw.adv.module.tools.UploadUtil;
import com.bw.adv.service.coupon.service.CouponService;
import com.bw.adv.service.sys.service.SysUserService;
import com.google.gson.JsonObject;

/**
 * ClassName: CouponController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月17日 上午11:31:09 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Controller
@RequestMapping("/couponBusinessTypeController")
@SuppressWarnings("unused")
public class CouponBusinessTypeController {
	
	private CouponService couponService;
	
	@RequestMapping(value = "/insertCouponBusinessType", method = RequestMethod.POST)
	@ResponseBody
	public String saveCoupon(CouponBusinessType couponBusinessType){
		String result = null;
		
		try {
			couponService.insertCouponBusinessType(couponBusinessType);
			result = "{\"code\":\"1\",\"message\":\"新增成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"新增失败!\"}";
			e.printStackTrace();
			
		}
		return result;
	}
	/**
	 * editCoupon:编辑优惠券<br/>
	 * Date: 2015年8月22日 下午4:44:44 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponid
	 * @return
	 */
	@RequestMapping(value = "/editCouponBusinessType", method = RequestMethod.POST)
	@ResponseBody
	public String editCoupon(CouponBusinessType couponBusinessType){
		String result = null;
		
		try{
			couponService.updateCouponBusinessType(couponBusinessType);
			result = "{\"code\":\"1\",\"message\":\"修改成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"修改失败!\"}";
			e.printStackTrace();
			
		}
		return result;
	}
	/**
	 * removeCoupons:批量删除优惠券 <br/>
	 * Date: 2015年8月24日 下午1:24:37 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponIds
	 * @return
	 */
	@RequestMapping(value = "/removeCouponBusinessType", method = RequestMethod.GET)
	@ResponseBody
	public String removeCouponBusinessType(Long couponBusinessTypeId){
		String result = null;
		
		try {
			couponService.removeCouponBusinessType(couponBusinessTypeId);
			result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"删除失败!\"}";
			e.printStackTrace();
			
		}
		return result;
	}
	
	@RequestMapping(value = "/queryCouponBusinessTypeDetail", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showCouponDetail(Long couponBusinessTypeId){
		Map<String, Object> couponMap = null;
		CouponBusinessType couponBusinessType = null;
		try {
			couponMap = new HashMap<String, Object>();
			couponBusinessType = couponService.selectCouponBusinessByPk(couponBusinessTypeId);
			couponMap.put("couponBusinessType", couponBusinessType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return couponMap;
	}
	@RequestMapping(value = "/showBusinessTypeWithPage", method = RequestMethod.GET)
	@ResponseBody
	public List<CouponBusinessType> showBusinessType(int page,int rows){
		List<CouponBusinessType> result = null;
		Page<CouponBusinessType> pageObj = null;
		
		try {
			pageObj = new Page<CouponBusinessType>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = couponService.selectCouponBusinessType(pageObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@Autowired
	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}
}
