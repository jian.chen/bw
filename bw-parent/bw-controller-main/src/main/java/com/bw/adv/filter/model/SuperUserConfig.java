package com.bw.adv.filter.model;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

/**
 * Created by jeoy.zhou on 1/12/16.
 */
public class SuperUserConfig {

    private static String userName;
    private static String password;
    private static boolean isOpen;
    private static Long systemId;

    @Value("${superUser.isOpen}")
    private boolean superUserisOpen;
    @Value("${superUser.userName}")
    private String superUserName;
    @Value("${superUser.password}")
    private String superUserPwd;
    @Value("${superUser.systemId}")
    private String superUserSystemId;

    @PostConstruct
    private void init() {
        if(!superUserisOpen) {
            setEmptyConfig();
            return;
        }
        if(StringUtils.isBlank(superUserName) || StringUtils.isBlank(superUserPwd)) {
            setEmptyConfig();
            return;
        }
        setIsOpen(true);
        setUserName(superUserName);
        setPassword(superUserPwd);
        setSystemId(superUserSystemId);
    }

    private static void setEmptyConfig() {
        setIsOpen(false);
        setUserName(StringUtils.EMPTY);
        setPassword(StringUtils.EMPTY);
    }

    public static String getUserName() {
        if(isOpen) {
            return userName;
        }
        return StringUtils.EMPTY;
    }

    public static void setUserName(String userName) {
        SuperUserConfig.userName = userName;
    }

    public static String getPassword() {
        if(isOpen) {
            return password;
        }
        return StringUtils.EMPTY;
    }

    public static void setPassword(String password) {
        SuperUserConfig.password = password;
    }

    public static void setIsOpen(boolean isOpen) {
        SuperUserConfig.isOpen = isOpen;
    }

    public static Long getSystemId() {
        return systemId;
    }

    public static void setSystemId(String systemId) {
        if (StringUtils.isNotBlank(systemId)) {
            SuperUserConfig.systemId = Long.valueOf(systemId);
        } else {
            SuperUserConfig.systemId = null;
        }
    }
}
