/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:ApiTest.java
 * Package Name:com.sage.scrm.controller.api.test
 * Date:2015-10-22上午10:48:48
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.api.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ClassName:ApiTest <br/>
 * Function: 压力测试. <br/>
 * Date: 2015-10-22 上午10:48:48 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/pressureTest")
@SuppressWarnings("unused")
public class PressureTest {
	
	
	@RequestMapping(method = RequestMethod.POST, value = "testHandleOrder")
	@ResponseBody
	public Map<String, Object> testHandleOrder(String orderXml){
		Map<String, Object> map = new HashMap<String, Object>();
		String result = sendPost("http://localhost/scrm-web-main/rs/external/api/order/handleSyncOrder", orderXml,"xml");
		map.put("result", result);
		return map;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "testRegisterMember")
	@ResponseBody
	public Map<String, Object> testRegisterMember(String orderXml){
		Map<String, Object> map = new HashMap<String, Object>();
		JSONObject json = new JSONObject();
		json.put("extAccountTypeId", "1");
		json.put("bindingAccount", "adsfsdsdsdsd");
		json.put("mobile", "18317117311");
		String result = sendPost("http://localhost/scrm-web-main/rs/external/api/member/registerMemberNoWechat", json.toString(),"json");
		map.put("result", result);
		return map;
	}
	
	
	public static void main(String[] args) {
		JSONObject json = new JSONObject();
		json.put("extAccountTypeId", "1");
		json.put("bindingAccount", "adsfsdsdsdsd");
		json.put("mobile", "18317117311");
		String result = sendPost("http://139.196.48.250:8084/scrm-web-main/rs/external/api/member/registerMemberNoWechat", json.toString(),"json");
		System.out.println(result);
	}
	
	public static String sendPost(String url, String param,String type) {
	      PrintWriter out = null;
	      BufferedReader in = null;
	      String result = "";
	      try {
	          URL realUrl = new URL(url);
	          // 打开和URL之间的连接
	          URLConnection conn = realUrl.openConnection();
	          // 设置通用的请求属性
	          if(type.equals("xml")){
	        	  conn.setRequestProperty("Content-Type", "application/xml;charset=UTF-8");
	          }else{
	        	  conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
	          }
	          
	          
	          // 发送POST请求必须设置如下两行
	          conn.setDoOutput(true);
	          conn.setDoInput(true);
	          // 获取URLConnection对象对应的输出流
	          out = new PrintWriter(conn.getOutputStream());
	          // 发送请求参数
	          out.print(param);
	          // flush输出流的缓冲
	          out.flush();
	          // 定义BufferedReader输入流来读取URL的响应
	          in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	          String line;
	          while ((line = in.readLine()) != null) {
	              result += line;
	          }
	      } catch (Exception e) {
	          System.out.println("发送 POST 请求出现异常！"+e);
	          e.printStackTrace();
	      }
	      //使用finally块来关闭输出流、输入流
	      finally{
	          try{
	              if(out!=null){
	                  out.close();
	              }
	              if(in!=null){
	                  in.close();
	              }
	          }
	          catch(IOException ex){
	              ex.printStackTrace();
	          }
	      }
	      return result;
	  }

}

