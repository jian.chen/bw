package com.bw.adv.controller.grade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.GradeConf;
import com.bw.adv.service.member.service.GradeService;

/**
 * ClassName: GradeController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月6日 上午10:47:36 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Controller
@RequestMapping("/gradeController")
@SuppressWarnings("unused")
public class GradeController {
	
	private GradeService gradeService;
	
	/**
	 * showGrade:查询等级设置信息 <br/>
	 * Date: 2015年9月6日 上午10:47:47 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param gradeConfId
	 * @return
	 */
	@RequestMapping(value = "/showGrade", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> showGrade(Long gradeConfId){
		List<Grade> list = null;
		GradeConf gradeConf = null;
		Map<String, Object> result = null; 
		
		try {
			result = new HashMap<String, Object>();
			list = gradeService.queryGradeList();
			gradeConf = gradeService.queryGradeConfByPk(gradeConfId);
			result.put("rows", list);
			result.put("obj", gradeConf);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * enableGrade:启用/停用等级 <br/>
	 * Date: 2015年9月6日 上午11:28:00 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param gradeConf
	 * @return
	 */
	@RequestMapping(value = "/enableGrade", method = RequestMethod.GET)
	@ResponseBody
	public String enableGrade(GradeConf gradeConf){
		String result = null;
		
		try {
			gradeService.updateGradeConf(gradeConf);
			if("Y".equals(gradeConf.getIsActive())){
				result = "{\"code\":\"1\",\"message\":\"启用成功!\"}";
			}else if("N".equals(gradeConf.getIsActive())){
				result = "{\"code\":\"1\",\"message\":\"停用成功!\"}";
			}
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"操作失败!\"}";
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * saveGrade:保存等级设置 <br/>
	 * Date: 2015年9月7日 上午11:12:52 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param gradelist
	 * @param gradeConf
	 * @return
	 */
	@RequestMapping(value = "/saveGrade", method = RequestMethod.POST)
	@ResponseBody
	public String saveGrade(String gradelist,GradeConf gradeConf){
		JSONArray jsonArr = null;
		Grade[] grades = null;
		String result = null;
		
		try {
			jsonArr = JSONArray.fromObject(gradelist);
			grades = (Grade[]) JSONArray.toArray(jsonArr, Grade.class);  
			gradeService.updateGrade(grades,gradeConf);
			result = "{\"code\":\"1\",\"message\":\"保存成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"保存失败!\"}";
			e.printStackTrace();
			
		}
		return result;
	}
	
	/**
	 * showGradeList:(查询等级列表). <br/>
	 * Date: 2015年9月24日 下午4:37:22 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(value = "/showGradeList", method = RequestMethod.GET)
	@ResponseBody
	public List<Grade> showGradeList(){
		List<Grade> gradeAllList = null;
		try {
			gradeAllList = GradeInit.GRADE_LIST;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gradeAllList;
	}
	
	
	@Autowired
	public void setGradeService(GradeService gradeService) {
		this.gradeService = gradeService;
	}
	
}
