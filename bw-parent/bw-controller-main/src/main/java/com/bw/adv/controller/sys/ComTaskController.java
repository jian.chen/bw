/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:ComTaskController.java
 * Package Name:com.sage.scrm.controller.sys
 * Date:2015年9月5日下午3:26:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.component.job.model.exp.ComTaskExp;
import com.bw.adv.module.sys.model.SysLog;
import com.bw.adv.module.sys.model.exp.SysLogExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.job.service.ComTaskService;

/**
 * ClassName:ComTaskController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月5日 下午3:26:19 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/comTaskController")
@SuppressWarnings("unused")
public class ComTaskController {
	
	private ComTaskService comTaskService;

	/**
	 * 
	 * showComTaskList:根据条件查询所有定时任务
	 * Date: 2015年9月5日 下午3:34:38 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param comTaskExp
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showComTaskList")
	@ResponseBody
	public Map<String, Object> showComTaskList(ComTaskExp comTaskExp,int page,int rows){
		List<ComTaskExp> result = null;
		Page<ComTaskExp> pageObj = null;
		Example example = null;
		Map<String, Object> map = null;
		
		try {
			map = new HashMap<String, Object>();
			example = new Example();
			Criteria temp = example.createCriteria();
			if(comTaskExp != null){
				if(comTaskExp.getLastStatus() != null && comTaskExp.getLastStatus() != ""){
					temp.andEqualTo("ct.LAST_STATUS", comTaskExp.getLastStatus());
				}
				if(comTaskExp.getIsJob() != null && comTaskExp.getIsJob() != ""){
					temp.andEqualTo("ct.IS_JOB", comTaskExp.getIsJob());
				}
				if(comTaskExp.getTaskTypeId() != null){
					temp.andEqualTo("ct.TASK_TYPE_ID", comTaskExp.getTaskTypeId());
				}
			}
			pageObj = new Page<ComTaskExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = comTaskService.queryExpByExample(example, pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	@Autowired
	public void setComTaskService(ComTaskService comTaskService) {
		this.comTaskService = comTaskService;
	}
	
	

}

