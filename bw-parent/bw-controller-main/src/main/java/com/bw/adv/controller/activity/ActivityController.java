package com.bw.adv.controller.activity;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.model.Activity;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.model.PhotoScoreResult;
import com.bw.adv.module.activity.model.exp.ActivityInstanceRecordExp;
import com.bw.adv.module.activity.model.exp.ActivityRuleConditionExp;
import com.bw.adv.module.base.model.ActivityDate;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.component.msg.model.SmsTemp;
import com.bw.adv.module.component.rule.init.RuleInit;
import com.bw.adv.module.component.rule.model.Conditions;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.points.model.PointsRule;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;
import com.bw.adv.service.activity.service.ActivityService;
import com.bw.adv.service.base.service.ActivityDateService;
import com.bw.adv.service.base.service.SmsTempService;
import com.bw.adv.service.points.service.PointsRuleSevice;
/**
 * ClassName: ActivityController <br/>
 * date: 2015-8-24 下午4:54:09 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Controller
@RequestMapping("/activityController")
public class ActivityController {
	
	
	private ActivityService activityService;
	private ActivityDateService activityDateService;
	private PointsRuleSevice pointsRuleSevice;
	private SmsTempService smsTempService;

	@RequestMapping(value = "/showActivityInfo")
	@ResponseBody
	public Map<String, Object> showActivityInfo(HttpServletRequest request,String activityCode){
		Map<String, Object> map = null;
		Map<String, Conditions> conMap = null;
		JSONArray jsonArray = null;
		JSONObject jsonObject = null;
		try {
			map = new HashMap<String, Object>();
			jsonArray = new JSONArray();
			conMap = RuleInit.getActivityConditions(activityCode);
			System.out.println(activityCode+"************"+conMap);
			if(conMap != null){
				for (String code : conMap.keySet()) {
					jsonObject = new JSONObject();
					if(code.startsWith("GIVE_")){
						continue;
					}
					jsonObject.put("conditionId", conMap.get(code).getConditionsId());
					jsonObject.put("conditionCode", conMap.get(code).getConditionsCode());
					jsonObject.put("conditionName", conMap.get(code).getConditionName());
					jsonArray.add(jsonObject);
				}
			}
			map.put("total", jsonArray.size());
			map.put("rows", jsonArray);
		}catch (MemberException e) {
			map.put("msg",e.getMessage());
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	
	
	
	/**
	 * 
	 * showActivityList:查询所有活动
	 * Date: 2015年9月8日 下午3:35:44 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(value = "/showActivityList")
	@ResponseBody
	public List<Activity> showActivityList(Long typeId){
		List<Activity> result = null;
		
		try {
			result = activityService.queryByTypeId(typeId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	
	/**
	 * startActivity:发起活动<br/>
	 * Date: 2015-8-24 下午4:56:55 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/startActivity")
	@ResponseBody
	public Map<String, String> startActivity(HttpServletRequest request,String params){
		Map<String, String> map = null;
		JSONObject json = null;
		try {
			map = new HashMap<String, String>();
			json = JSONObject.fromObject(params);
			activityService.startActivity(json,SysUserUtil.getSessionUser(request));
			map.put("msg", "");
			map.put("code", "200");
		}catch (MemberException e) {
			map.put("code", "500");
			map.put("msg", "保存失败："+e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", "500");
			map.put("msg", "保存失败："+e.getMessage());
		}
		return map;
	}
	
	
	/**
	 * startActivity:修改活动<br/>
	 * Date: 2015-8-24 下午4:56:55 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/editActivity")
	public Map<String, String> editActivity(HttpServletRequest request,String params){
		Map<String, String> map = null;
		JSONObject json = null;
		try {
			map = new HashMap<String, String>();
			json = JSONObject.fromObject(params);
			activityService.updateActivity(json,SysUserUtil.getSessionUser(request));
			map.put("code", "200");
			map.put("msg", "");
		}catch (MemberException e) {
			map.put("code", "500");
			map.put("msg", "保存失败："+e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", "500");
			map.put("msg", "保存失败："+e.getMessage());
		}
		return map;
	}
	/**
	 * showActivityInstanceList:(查询活动实例列表). <br/>
	 * Date: 2015-8-26 下午6:44:59 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param params
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/showActivityInstanceList", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showActivityInstanceList(Long activityId,int page,int rows,HttpServletRequest request){
		Map<String, Object> map = null;
		Page<ActivityInstance> activityPage = null;
		try {
			map = new HashMap<String, Object>();
			activityPage = new Page<ActivityInstance>();
			
			activityPage.setPageNo(page);
			activityPage.setPageSize(rows);
			activityService.queryActivityInstanceByActivityId(activityId, activityPage);
			
			map.put("total", activityPage.getTotalRecord());
			map.put("rows", activityPage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * showActivityInstance:(查询某一个实例信息). <br/>
	 * Date: 2015-9-2 下午4:03:33 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityInstanceId
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/showActivityInstance", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showActivityInstance(Long activityInstanceId,HttpServletRequest request){
		Map<String, Object> map = null;
		ActivityInstance instance = null;
		List<ActivityRuleConditionExp> instanceList = null; 
		Map<String, Object> conditionMap = null;
		try {
			map = new HashMap<String, Object>();
			conditionMap = new HashMap<String, Object>();
			if(activityInstanceId != null){
				instance = activityService.queryActivityInstanceByInstanceId(activityInstanceId);
				instanceList = activityService.queryActivityConditionByInstanceId(activityInstanceId);
				for (ActivityRuleConditionExp conditionExp : instanceList) {
					conditionMap.put(conditionExp.getConditionsCode(), StringUtils.isBlank(conditionExp.getConditionsValues())? "" : conditionExp.getConditionsValues());
				}
			}
			map.put("instance", instance);
			map.put("conditionMap", conditionMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@RequestMapping(value = "/showActivityInstanceInfo", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showActivityInstanceInfo(Long activityInstanceId,HttpServletRequest request){
		Map<String, Object> map = null;
		ActivityInstance instance = null;
		List<ActivityRuleConditionExp> instanceList = null; 
		Map<String, Object> conditionMap = null;
		try {
			map = new HashMap<String, Object>();
			conditionMap = new HashMap<String, Object>();
			if(activityInstanceId != null){
				instance = activityService.queryActivityInstanceByInstanceId(activityInstanceId);
				instanceList = activityService.queryActivityConditionByInstanceId(activityInstanceId);
				for (ActivityRuleConditionExp conditionExp : instanceList) {
					conditionMap.put(conditionExp.getConditionsCode(), conditionExp.getConditionsValues());
				}
			}
			map.put("instance", instance);
			map.put("conditionMap", conditionMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * showActivityInstanceRecord:(查询实例记录). <br/>
	 * Date: 2015-8-27 下午3:18:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityId
	 * @param page
	 * @param rows
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/showActivityInstanceRecord", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showActivityInstanceRecord(Long activityInstanceId,String recordMobile,int page,int rows,HttpServletRequest request){
		Map<String, Object> map = null;
		Page<ActivityInstanceRecordExp> activityPage = null;
		Example example = null;
		try {
			map = new HashMap<String, Object>();
			activityPage = new Page<ActivityInstanceRecordExp>();
			activityPage.setPageNo(page);
			activityPage.setPageSize(rows);
			
			example = new Example();
			Criteria temp = example.createCriteria();
			temp.andEqualTo("r.activity_instance_id", activityInstanceId);
			if(StringUtils.isNotBlank(recordMobile)){
				temp.andLike("m.mobile", "%"+recordMobile+"%");
			}
			
			//activityService.queryActivityInstanceRecordByInstanceId(activityInstanceId, activityPage);
			activityService.queryActivityInstanceRecordByExample(example, activityPage);
			
			map.put("total", activityPage.getTotalRecord());
			map.put("rows", activityPage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * showActivityPointsRule:(查询活动的积分规则). <br/>
	 * Date: 2015-8-31 下午2:43:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @throws IOException
	 */
	@RequestMapping(value="/showActivityPointsRule", method = RequestMethod.GET)
	@ResponseBody
	public List<PointsRule> showActivityPointsRule(){
		List<PointsRule> pointsRuleList = null;
		
		try {
			pointsRuleList = pointsRuleSevice.queryPointsRuleList(StatusConstant.POINTS_RULE_ACTIVE.getCode(), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pointsRuleList;
	}
	
	
	/**
	 * ActivityDate:(查询活动日期). <br/>
	 * Date: 2015-8-31 下午2:43:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @throws IOException
	 */
	@RequestMapping(value="/showActivityDateList", method = RequestMethod.GET)
	@ResponseBody
	public List<ActivityDate> showActivityDateList(){
		List<ActivityDate> pointsRuleList = null;
		try {
			pointsRuleList = activityDateService.queryAllActiveDate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pointsRuleList;
	}
	
	
	/**
	 * ActivityDate:(查询活动日期). <br/>
	 * Date: 2015-8-31 下午2:43:52 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @throws IOException
	 */
	@RequestMapping(value="/showSmsTempList", method = RequestMethod.GET)
	@ResponseBody
	public List<SmsTemp> showSmsTempList(){
		List<SmsTemp> pointsRuleList = null;
		try {
			pointsRuleList = smsTempService.querySmsTempAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pointsRuleList;
	}
	
	/**
	 * 
	 * updateInstanceStatus:手动终止活动
	 * Date: 2015年9月18日 下午5:38:18 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param activityInstanceId
	 * @return
	 */
	@RequestMapping(value="/updateInstanceStatus", method = RequestMethod.POST)
	@ResponseBody
	public int updateInstanceStatus(HttpServletRequest request,Long activityInstanceId){
		int result = 0;
		try {
			activityService.updateInstanceStatus(SysUserUtil.getSessionUser(request),activityInstanceId);
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	@RequestMapping(value = "/uploadPhoto", method = RequestMethod.POST)
	@ResponseBody
	public String uploadCardImage(HttpServletRequest request,HttpServletResponse response){
		MultipartHttpServletRequest multipartRequest = null;
		String logoPathDir = null;
		String logoRealPathDir = null;
		File logoSaveFile = null;
		MultipartFile multipartFile = null;
		String suffix = null;
		String logImageName = null;
		String fileName = null;
		String filePath = null;
		File file = null;
		
	 	multipartRequest = (MultipartHttpServletRequest) request;
	 	/** 构建文件保存的目录* */
        logoPathDir = "/scrm/img/face";
        /** 得到文件保存目录的真实路径* */
        logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
        /** 根据真实路径创建目录* */
        logoSaveFile = new File(logoRealPathDir);
        if (!logoSaveFile.exists()){
            logoSaveFile.mkdirs();
        }
        /** 页面控件的文件流* */
        multipartFile = multipartRequest.getFile("cardimage");
	    if (FileUtil.isAllowUpForPicture(multipartFile.getOriginalFilename())) {
	    	/** 获取文件的后缀* */
	    	suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
	    	if(!suffix.equals(".jpg")){
	    		return "请使用.jpg的图片格式!";
	    	}
	    	/** 使用UUID生成文件名称* */
	    	logImageName = "face" + suffix;// 构建文件名称
	    	/** 拼成完整的文件保存路径加文件* */
	    	fileName = logoRealPathDir + File.separator + logImageName;
	    	file = new File(fileName);
	    	filePath = logoPathDir+ "/" + logImageName;
	        try {
	            multipartFile.transferTo(file);
	        } catch (IllegalStateException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        /** 打印出上传到服务器的文件的绝对路径* */
	        System.out.println("*** "+fileName+" ***");
	        return filePath;
	    }else{
	    	return "不被允许上传的文件格式!";
	    }
	}
	
	/**
	 * 保存照片发布结果
	 * @param photoScoreResult
	 * @return
	 */
	@RequestMapping(value = "/savePhotoResult", method = RequestMethod.POST)
	@ResponseBody
	public String savePhotoResult(PhotoScoreResult photoScoreResult,HttpServletRequest request,HttpServletResponse response){
		try {
			photoScoreResult.setCreateTime(DateUtils.getCurrentTimeOfDb());
			this.activityService.savePhotoScoreResult(photoScoreResult);
			
			//删除照片
			String logoPathDir = null;
			String logoRealPathDir = null;
			File logoSaveFile = null;
			String logImageName = null;
			String fileName = null;
			File file = null;
			
		 	/** 构建文件保存的目录* */
	        logoPathDir = "/scrm/img/face";
	        /** 得到文件保存目录的真实路径* */
	        logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
	        /** 根据真实路径创建目录* */
	        logoSaveFile = new File(logoRealPathDir);
	        if (!logoSaveFile.exists()){
	            logoSaveFile.mkdirs();
	        }
	        /** 页面控件的文件流* */
	    	logImageName = "face.jpg";// 构建文件名称
	    	fileName = logoRealPathDir + File.separator + logImageName;
	    	file = new File(fileName);
	    	file.delete(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
        return "";
	}
	
	
	@Autowired
	public void setActivityService(ActivityService activityService) {
		this.activityService = activityService;
	}

	@Autowired
	public void setPointsRuleSevice(PointsRuleSevice pointsRuleSevice) {
		this.pointsRuleSevice = pointsRuleSevice;
	}

	@Autowired
	public void setActivityDateService(ActivityDateService activityDateService) {
		this.activityDateService = activityDateService;
	}
	
	@Autowired
	public void setSmsTempService(SmsTempService smsTempService) {
		this.smsTempService = smsTempService;
	}

}
