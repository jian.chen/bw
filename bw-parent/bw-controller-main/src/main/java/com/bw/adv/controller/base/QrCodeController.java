package com.bw.adv.controller.base;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.controller.base.utils.QrCodeUtils;
import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.base.model.QrCode;
import com.bw.adv.module.base.model.exp.QrCodeExp;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.base.service.QrCodeService;



@Controller
@RequestMapping("/qrCodeController")
@SuppressWarnings("unused")
public class QrCodeController {
	@Resource
	private QrCodeService qrCodeService;
	private AccessTokenCacheService accessTokenCacheService;
	private CodeService codeService;

	/**
	 * showQrCode:(查询二维码列表). <br/>
	 * Date: 2015-11-3 下午5:03:33 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/showQrCode", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showQrCode(int page,int rows,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Page<QrCode> pageObj = null;
		List<QrCode> qrCodeList = null;
		Map<String, Object> map = null;
		
		try {
			map = new HashMap<String, Object>();
			pageObj = new Page<QrCode>();  
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			qrCodeList=qrCodeService.queryListForQrCode(StatusConstant.QR_CODE_ACTIVE.getCode(), pageObj);

			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
		
	}
	
	@RequestMapping(value = "saveQrCode", method = RequestMethod.POST)
	@ResponseBody
	public String saveQrCode(QrCode orCode,HttpServletRequest request) throws IOException{
		String result = null;
		String encoderContent=null;
		FileOutputStream output=null;	
		String filePath=null;
		String url=null;
		JSONObject jsonObject =null;//微信接口调用的json参数
		String weChatResult=null;//微信接口返回的结果
		try {
			 
			if(null==orCode.getQrCodeId()){//新建保存
				orCode.setQrCodeCode(codeService.generateCode(QrCode.class));
				orCode.setIsActive(StatusConstant.QR_CODE_ACTIVE.getCode());
				orCode.setCreateTime(DateUtils.getCurrentTimeOfDb());
				orCode.setResponseNumber(0l);
				qrCodeService.save(orCode);
			}else{//修改保存
				orCode.setUpdateTime(DateUtils.getCurrentTimeOfDb());
				qrCodeService.updateByPkSelective(orCode);
			}
			
//			 //二维码生成
//			QrCodeUtils handler = new QrCodeUtils();
//			handler.encoderQRCode(encoderContent, output, "png", 7);
//			
			result = "{\"code\":\"1\",\"message\":\"保存成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"保存失败!\"}";
			e.printStackTrace();
		} 
		
		return result;
	}
	
	/**
	 * qrCodeDetail:(查询二维码明细). <br/>
	 * Date: 2015-11-5 下午5:32:22 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param qrCodeId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "qrCodeDetail", method = RequestMethod.POST)
	@ResponseBody
	public QrCodeExp qrCodeDetail(String qrCodeId) throws IOException{
		QrCodeExp qrCode = null;
		try {
			qrCode = new QrCodeExp();
			qrCode = qrCodeService.queryQrCodeByPrimaryKey(Long.parseLong(qrCodeId));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return qrCode;
	}	
	
	/**
	 * removeQrCode:(删除二维码). <br/>
	 * Date: 2015-11-5 下午5:47:07 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param qrCodeId
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "removeQrCode", method = RequestMethod.POST)
	@ResponseBody
	public String removeQrCode(String qrCodeIds) throws IOException{
		String result = null;
		try {
			if(StringUtils.isNotBlank(qrCodeIds)){
				qrCodeService.removeQrCode(qrCodeIds);
				result = "{\"code\":\"1\",\"message\":\"删除成功!\"}";
				return result;
			}
			result = "{\"code\":\"1\",\"message\":\"删除失败!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"删除失败!\"}";
			e.printStackTrace();
		}
		return result;
	}	
	
	@RequestMapping(method = RequestMethod.GET, value = "showQrCodePic")
	public void showQrCodePic(String qrCodeId,HttpServletResponse response){
		try {
			QrCodeExp qrCode = null;
			InputStream fis = null;
			OutputStream output = null;
			try {
				qrCode = new QrCodeExp();
				qrCode = qrCodeService.queryQrCodeByPrimaryKey(Long.parseLong(qrCodeId));
				if(qrCode!=null&&StringUtils.isBlank(qrCode.getQrCodeDesc())){
					return;
				}
				
				String type = "image/png";
				response.setContentType(type);       
				response.setHeader("Pragma","No-cache");
				response.setHeader("Cache-Control","no-cache");
				response.setDateHeader("Expire",0);
				output = response.getOutputStream();
				
				 QrCodeUtils handler = new QrCodeUtils();
				 handler.encoderQRCode(qrCode.getQrCodeDesc(), output);
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				output.flush();
				output.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@RequestMapping(method = RequestMethod.GET, value = "downQrCodePic")
	public void downQrCodePic(String qrCodeId,HttpServletResponse response){
		try {
			QrCodeExp qrCode = null;
			InputStream fis = null;
			OutputStream output = null;
			try {
				qrCode = new QrCodeExp();
				qrCode = qrCodeService.queryQrCodeByPrimaryKey(Long.parseLong(qrCodeId));
				if(qrCode!=null&&StringUtils.isBlank(qrCode.getQrCodeDesc())){
					return;
				}
				
				String type = "application/octet-stream";
				response.setContentType(type);       
				response.setHeader("Pragma","No-cache");
				response.setHeader("Cache-Control","no-cache");
				String fileName = URLEncoder.encode(qrCode.getQrCodeName(), "UTF-8")+".png";
				response.setHeader("Content-Disposition", "attachment;fileName=" + fileName);
				output = response.getOutputStream();
				
				QrCodeUtils handler = new QrCodeUtils();
				handler.encoderQRCode(qrCode.getQrCodeDesc(), output,"png",10);
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				output.flush();
				output.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "showChannelQrCodeList")
	@ResponseBody
	public List<Channel> showChannelQrCodeList(){
		List<Channel> result = null;
		try {
			result = qrCodeService.queryChannelQrCodeList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Autowired
	public void setQrCodeService(QrCodeService qrCodeService) {
		this.qrCodeService = qrCodeService;
	}

	@Autowired
	public void setAccessTokenCacheService(AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}
	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}
	
	
}
