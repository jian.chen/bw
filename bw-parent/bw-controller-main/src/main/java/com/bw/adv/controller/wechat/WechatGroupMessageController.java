package com.bw.adv.controller.wechat;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.module.common.utils.FileUtils;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.wechat.api.ApiWechatMaterialUpload;
import com.bw.adv.module.wechat.model.WechatGroupMessage;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;
import com.bw.adv.module.wechat.model.WeixinMedia;
import com.bw.adv.module.wechat.service.AccessTokenCacheService;
import com.bw.adv.service.wechat.service.WechatGroupMessageService;

 
@Controller
@RequestMapping("/wechatGroupMessageController")
@SuppressWarnings("unused")
public class WechatGroupMessageController {

	private WechatGroupMessageService wechatGroupMessageService;
	
	private ApiWechatMaterialUpload apiWechatMaterialUpload;
	
	private AccessTokenCacheService accessTokenCacheService;
	
	
	@Autowired
	public void setAccessTokenCacheService(
			AccessTokenCacheService accessTokenCacheService) {
		this.accessTokenCacheService = accessTokenCacheService;
	}

	@Autowired
	public void setApiWechatMaterialUpload(
			ApiWechatMaterialUpload apiWechatMaterialUpload) {
		this.apiWechatMaterialUpload = apiWechatMaterialUpload;
	}

	@RequestMapping(value="/showAllGroups", method = RequestMethod.GET)
	@ResponseBody
	public List<?> showAllGroups(String type) throws IOException{
		List<?>  list=null;
		try {	
			list=wechatGroupMessageService.queryGroupList(type);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * sendWechatGroupMessage:(微信发送消息并保存记录). <br/>
	 * Date: 2015-9-6 上午11:51:21 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/sendWechatGroupMessage", method = RequestMethod.POST)
	@ResponseBody
	public String sendWechatGroupMessage(MultipartHttpServletRequest request) throws IOException{
		String content=null;//内容
		String groupSelectValue=null;//下拉框选中的值（组ID/标签ID）
		String groupTypeValue=null;//下拉框选中的类型（按分组/按标签/按粉丝）
		String wechatMessageType=null;//发送模式（图文/文本/图片）
		String result = null;
		Map<String,Object> paramsMap=null;//参数map
		String filePath = "";
		String fileUrl = "";
		String strBackUrl=null;//图片url
		
		try {	
			paramsMap=new HashMap<String, Object>();
			
			groupTypeValue=request.getParameter("groupTypeValue");
			groupSelectValue=request.getParameter("groupSelectValue");
			content=request.getParameter("content");
			wechatMessageType=request.getParameter("wechatMessageType");
	
			System.out.println(groupTypeValue+"(****)"+groupSelectValue+"(****)"+content);
			
			if(wechatMessageType.equals("image")){
				//文件上传到本地服务器,返回映射的路径
				filePath = FileUtils.upload(request);
				fileUrl = request.getSession().getServletContext().getRealPath("/") + filePath;
				strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+filePath;	
				
			}
			

			
			paramsMap.put("groupTypeValue", groupTypeValue);
			paramsMap.put("groupSelectValue", groupSelectValue);
			paramsMap.put("content", content);
			paramsMap.put("wechatMessageType", wechatMessageType);
			paramsMap.put("strBackUrl", strBackUrl);
			
			wechatGroupMessageService.sendWechatGroupMessage(paramsMap);
			
			result = "{\"code\":\"1\",\"message\":\"发送成功!\"}";
		} catch (Exception e) {
			result = "{\"code\":\"0\",\"message\":\"发送失败!\"}";
			e.printStackTrace();
		}
		return result;
	}	
	@Autowired
	public void setWechatGroupMessageService(
			WechatGroupMessageService wechatGroupMessageService) {
		this.wechatGroupMessageService = wechatGroupMessageService;
	}
	
	
	

	
}
