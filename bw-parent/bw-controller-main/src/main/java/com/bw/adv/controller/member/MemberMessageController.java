package com.bw.adv.controller.member;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.core.code.service.CodeService;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.member.message.model.MemberMessage;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;
import com.bw.adv.service.member.service.MemberMessageItemService;
import com.bw.adv.service.member.service.MemberMessageService;

/**
 * ClassName: MemberMessageController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年2月22日 下午3:39:39 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Controller
@RequestMapping("/memberMessageController")
@SuppressWarnings("unused")
public class MemberMessageController {
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	private CodeService codeService;
	
	private MemberMessageService memberMessageService;
	
	private MemberMessageItemService memberMessageItemService;
	
	@RequestMapping(value = "/showList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridModel<MemberMessage> showList(int page,int rows){
		DataGridModel<MemberMessage> result = null;
		Page<MemberMessage> pageObj = null;
		List<MemberMessage> list = null;
		Long count = null;
		
		try {
			pageObj = new Page<MemberMessage>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			list = memberMessageService.queryList(pageObj);
			count = pageObj.getTotalRecord();
			result = new DataGridModel<MemberMessage>(list, count);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return result;
	}
	
	/**
	 * showObj:根据ID查询会员消息 <br/>
	 * Date: 2016年2月25日 上午11:08:26 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/showObj", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> showObj(Long id){
		Map<String, Object> result = null;
		MemberMessage memberMessage = null;
		
		try {
			result = new HashMap<String, Object>();
			memberMessage = memberMessageService.queryByPk(id);
			result.put("message",memberMessage);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return result;
	}
	/**
	 * addMessage:保存消息<br/>
	 * Date: 2016年2月23日 下午4:01:29 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessage
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "addMessage")
	@ResponseBody
	public JsonModel addMessage(MemberMessage memberMessage){
		JsonModel result = null;
		try {
			memberMessageService.saveObj(memberMessage);
			result = new JsonModel(JsonModel.Status_Success,"保存成功！");
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		result = new JsonModel(JsonModel.Status_Error,"保存失败，请稍后重试！");
		return result;
	}
	
	/**
	 * editMessage:修改会员消息<br/>
	 * Date: 2016年2月25日 下午3:03:27 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessage
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "updateMessage")
	@ResponseBody
	public JsonModel editMessage(MemberMessage memberMessage){
		JsonModel result = null;
		try {
			memberMessageService.updateMessage(memberMessage);
			result = new JsonModel(JsonModel.Status_Success,"修改成功！");
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		result = new JsonModel(JsonModel.Status_Error,"修改失败，请稍后重试！");
		return result;
	}
	/**
	 * deleteMessage:删除会员消息<br/>
	 * Date: 2016年2月25日 下午4:48:27 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "deleteMessage")
	@ResponseBody
	public JsonModel deleteMessage(Long id){
		JsonModel result = null;
		
		try {
			memberMessageService.updateStatus(id);
			result = new JsonModel(JsonModel.Status_Success,"删除成功！");
			return result;
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		result = new JsonModel(JsonModel.Status_Error,"删除失败，请稍后重试！");
		return result;
	}
	
	/**
	 * sendMessage:发送消息 <br/>
	 * Date: 2016年2月26日 下午2:36:30 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param messageId
	 * @param memberIds
	 * @param type
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "sendMessage")
	@ResponseBody
	public JsonModel sendMessage(Long messageId,String memberIds,String type){
		JsonModel result = null;
		
		try {
			memberMessageItemService.insertBatchPer(messageId, memberIds, type);
			result = new JsonModel(JsonModel.Status_Success,"发送成功！");
			return result;
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		result = new JsonModel(JsonModel.Status_Error,"发送失败，请稍后重试！");
		return result;
	}
	/**
	 * recallMessage:撤回会员消息<br/>
	 * Date: 2016年2月26日 下午4:08:00 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param messageId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "recallMessage")
	@ResponseBody
	public JsonModel recallMessage(Long messageId){
		JsonModel result = null;
		
		try {
			memberMessageItemService.deleteByMessageId(messageId);
			result = new JsonModel(JsonModel.Status_Success,"撤回成功！");
			return result;
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		result = new JsonModel(JsonModel.Status_Error,"撤回失败，请稍后重试！");
		return result;
	}
	/**
	 * uploadMessageImage:上传封面图片 <br/>
	 * Date: 2016年2月25日 上午10:56:24 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/uploadMessageImage", method = RequestMethod.POST)
	@ResponseBody
	public String uploadMessageImage(HttpServletRequest request,HttpServletResponse response){
		MultipartHttpServletRequest multipartRequest = null;
		String logoPathDir = null;
		String logoRealPathDir = null;
		File logoSaveFile = null;
		MultipartFile multipartFile = null;
		String suffix = null;
		String logImageName = null;
		String fileName = null;
		String filePath = null;
		File file = null;
		
	 	multipartRequest = (MultipartHttpServletRequest) request;
	 	/** 构建文件保存的目录* */
        logoPathDir = "/business/shops/upload/message/"+ DateUtils.getCurrentTimeOfDb();
        /** 得到文件保存目录的真实路径* */
        logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
        /** 根据真实路径创建目录* */
        logoSaveFile = new File(logoRealPathDir);
        if (!logoSaveFile.exists()){
            logoSaveFile.mkdirs();
        }
        /** 页面控件的文件流* */
        multipartFile = multipartRequest.getFile("messageimage");
	    if (FileUtil.isAllowUpForPicture(multipartFile.getOriginalFilename())) {
	    	/** 获取文件的后缀* */
	    	suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
	    	/** 使用UUID生成文件名称* */
	    	logImageName = UUID.randomUUID().toString() + suffix;// 构建文件名称
	    	/** 拼成完整的文件保存路径加文件* */
	    	fileName = logoRealPathDir + File.separator + logImageName;
	    	file = new File(fileName);
	    	filePath = logoPathDir+ "/" + logImageName;
	        try {
	            multipartFile.transferTo(file);
	        } catch (IllegalStateException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return filePath;
	    }else{
	    	return "不被允许上传的文件格式!";
	    }
	}
	
	/**
	 * uploadMessageImages:上传富文本框图片 <br/>
	 * Date: 2016年2月27日 下午4:11:01 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "/uploadMessageImages", method = RequestMethod.POST)
	@ResponseBody
	public String uploadMessageImages(HttpServletRequest request,HttpServletResponse response,
			@RequestParam(value="upload", required=false) MultipartFile file){
		MultipartHttpServletRequest multipartRequest = null;
		String logoPathDir = null;
		String logoRealPathDir = null;
		File logoSaveFile = null;
		MultipartFile multipartFile = null;
		String suffix = null;
		String logImageName = null;
		String fileName = null;
		String filePath = null;
		File newfile = null;
		PrintWriter out = null;
		String callback = null;
		
	    String url = request.getRequestURL().toString();//获得客户端发送请求的完整url
	    url = url.substring(0,url.length()-47);
		callback =request.getParameter("CKEditorFuncNum");
	 	multipartRequest = (MultipartHttpServletRequest) request;
	 	/** 构建文件保存的目录* */
        logoPathDir = "/business/shops/upload/message/"+ DateUtils.getCurrentTimeOfDb();
        /** 得到文件保存目录的真实路径* */
        logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
        /** 根据真实路径创建目录* */
        logoSaveFile = new File(logoRealPathDir);
        if (!logoSaveFile.exists()){
            logoSaveFile.mkdirs();
        }
        /** 页面控件的文件流* */
//        multipartFile = multipartRequest.getFile("messageimage");
        multipartFile = file;
	    if (FileUtil.isAllowUpForPicture(multipartFile.getOriginalFilename())) {
	    	/** 获取文件的后缀* */
	    	suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
	    	/** 使用UUID生成文件名称* */
	    	logImageName = UUID.randomUUID().toString() + suffix;// 构建文件名称
	    	/** 拼成完整的文件保存路径加文件* */
	    	fileName = logoRealPathDir + File.separator + logImageName;
	    	newfile = new File(fileName);
	    	filePath = url+logoPathDir+ "/" + logImageName;
	        try {
	        	
	            multipartFile.transferTo(newfile);
	        } catch (IllegalStateException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
			try {
				out = response.getWriter();
			} catch (IOException e) {
				
				e.printStackTrace();
				
			}
			out.println("<script type=\"text/javascript\">");
			out.println("window.parent.CKEDITOR.tools.callFunction(" + callback
		           + ",'"+ filePath + "',''" + ")");
			out.println("</script>");
			out.flush();
			out.close();
			
			return null;
	    }else{
	    	return "不被允许上传的文件格式!";
	    }
	}
	
	@Autowired
	public void setCodeService(CodeService codeService) {
		this.codeService = codeService;
	}
	
	@Autowired
	public void setMemberMessageService(MemberMessageService memberMessageService) {
		this.memberMessageService = memberMessageService;
	}

	@Autowired
	public void setMemberMessageItemService(
			MemberMessageItemService memberMessageItemService) {
		this.memberMessageItemService = memberMessageItemService;
	}
	
}
