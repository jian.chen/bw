/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:memberCardGradeController.java
 * Package Name:com.sage.scrm.controller.base
 * Date:2015年9月6日下午4:45:45
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.exp.SmsTempExp;
import com.bw.adv.module.member.init.GradeInit;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.MemberCardGrade;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.FileUtil;
import com.bw.adv.service.base.service.MemberCardGradeService;

/**
 * ClassName:memberCardGradeController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月6日 下午4:45:45 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/memberCardGradeController")
@SuppressWarnings("unused")
public class MemberCardGradeController {
	
	private MemberCardGradeService memberCardGradeService;

	/**
	 * 
	 * showMemberCardList:查看会员卡设置list
	 * Date: 2015年9月6日 下午8:08:30 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberCardList")
	@ResponseBody
	public List<MemberCardGrade> showMemberCardList(){
		List<MemberCardGrade> result = null;
		List<MemberCardGrade> cardGradeList = null;
		List<Grade> gradeAllList = null;
		MemberCardGrade memberCardgrade = null;
		
		try {
			result = new ArrayList<MemberCardGrade>();
			cardGradeList = memberCardGradeService.queryAll();
			gradeAllList = GradeInit.GRADE_LIST;
			
			for(int i=0;i<cardGradeList.size();i++){
				memberCardgrade = new MemberCardGrade();
				memberCardgrade = cardGradeList.get(i);
				//根据会员等级id获取等级名称
				memberCardgrade.setMemberCardGradeName(GradeInit.getGradeByPk(memberCardgrade.getGradeId()).getGradeName());
				
				result.add(memberCardgrade);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * editMemberCard:保存修改的会员卡背景的url
	 * Date: 2015年9月6日 下午8:11:56 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberCardGrade
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editMemberCard")
	@ResponseBody
	public int editMemberCard(MemberCardGrade memberCardGrade){
		int result = 0;
		
		try {
			result = memberCardGradeService.updateSrcByPk(memberCardGrade);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * uploadCardImage:上传会员卡图片
	 * Date: 2015年9月6日 下午8:08:06 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/uploadCardImage", method = RequestMethod.POST)
	@ResponseBody
	public String uploadCardImage(HttpServletRequest request,HttpServletResponse response){
		MultipartHttpServletRequest multipartRequest = null;
		String logoPathDir = null;
		String logoRealPathDir = null;
		File logoSaveFile = null;
		MultipartFile multipartFile = null;
		String suffix = null;
		String logImageName = null;
		String fileName = null;
		String filePath = null;
		File file = null;
		
	 	multipartRequest = (MultipartHttpServletRequest) request;
	 	/** 构建文件保存的目录* */
        logoPathDir = "/business/shops/upload/card/"+ DateUtils.getCurrentTimeOfDb();
        System.out.println(request.getRequestURL());
        System.out.println(request.getRequestURI());
        System.out.println(request.getSession().getServletContext().getContextPath());
        /** 得到文件保存目录的真实路径* */
        logoRealPathDir = request.getSession().getServletContext().getRealPath(logoPathDir);
        /** 根据真实路径创建目录* */
        logoSaveFile = new File(logoRealPathDir);
        if (!logoSaveFile.exists()){
            logoSaveFile.mkdirs();
        }
        /** 页面控件的文件流* */
        multipartFile = multipartRequest.getFile("cardimage");
	    if (FileUtil.isAllowUpForPicture(multipartFile.getOriginalFilename())) {
	    	/** 获取文件的后缀* */
	    	suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
	    	/** 使用UUID生成文件名称* */
	    	logImageName = UUID.randomUUID().toString() + suffix;// 构建文件名称
	    	/** 拼成完整的文件保存路径加文件* */
	    	fileName = logoRealPathDir + File.separator + logImageName;
	    	file = new File(fileName);
	    	filePath = logoPathDir+ "/" + logImageName;
	        try {
	            multipartFile.transferTo(file);
	        } catch (IllegalStateException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        /** 打印出上传到服务器的文件的绝对路径* */
	        System.out.println("****************"+fileName+"**************");
	        return filePath;
	    }else{
	    	return "不被允许上传的文件格式!";
	    }
	}
	
	@Autowired
	public void setMemberCardGradeService(
			MemberCardGradeService memberCardGradeService) {
		this.memberCardGradeService = memberCardGradeService;
	}
	
	

}

