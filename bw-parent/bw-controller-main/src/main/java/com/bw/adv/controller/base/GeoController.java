/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:GeoController.java
 * Package Name:com.sage.scrm.controller.base
 * Date:2015年8月26日下午4:10:13
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.module.base.model.Geo;
import com.bw.adv.service.base.service.GeoService;

/**
 * ClassName:GeoController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月26日 下午4:10:13 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/geoController")
@SuppressWarnings("unused")
public class GeoController {
	
	private GeoService geoService;

	/**
	 * 
	 * showGeoByParent:根据区域父节点id,取它的子区域List
	 * Date: 2015年8月26日 下午4:14:12 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param geoId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showGeoByParent")
	@ResponseBody
	public List<Geo> showGeoByParent(Long geoId){
		List<Geo> result = null;
		
		try {
			result = geoService.queryByParentId(geoId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Autowired
	public void setGeoService(GeoService geoService) {
		this.geoService = geoService;
	}
	
	

}

