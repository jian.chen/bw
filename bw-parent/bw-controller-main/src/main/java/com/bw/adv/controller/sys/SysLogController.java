/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:SysLogController.java
 * Package Name:com.sage.scrm.controller.sys
 * Date:2015年8月28日下午5:54:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.component.msg.model.SmsTempParams;
import com.bw.adv.module.sys.model.SysLog;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.exp.SysLogExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.sys.log.service.SysLogService;

/**
 * ClassName:SysLogController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月28日 下午5:54:41 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/sysLogController")
@SuppressWarnings("unused")
public class SysLogController {
	
	private SysLogService sysLogService;
	
	/**
	 * 
	 * showSysLogList:查看日志列表
	 * Date: 2015年8月30日 下午4:47:32 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysLog
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showSysLogList")
	@ResponseBody
	public Map<String, Object> showSysLogList(SysLogExp sysLogExp,int page,int rows){
		List<SysLogExp> result = null;
		Page<SysLogExp> pageObj = null;
		Example example = null;
		Map<String, Object> map = null;
		
		try {
			map = new HashMap<String, Object>();
			example = new Example();
			Criteria temp = example.createCriteria();
			if(sysLogExp != null){
				/*if(sysLogExp.getLogDate() != null && sysLogExp.getLogDate() != ""){
					temp.andEqualTo("log_Date",DateUtils.formatString(sysLogExp.getLogDate()) );
				}*/
				//操作日期
				if(sysLogExp.getLogDateStart() != null && sysLogExp.getLogDateStart() != ""){
					temp.andGreaterThanOrEqualTo("log_Date", DateUtils.formatString(sysLogExp.getLogDateStart()));
				}
				if(sysLogExp.getLogDateEnd() != null && sysLogExp.getLogDateEnd() != ""){
					temp.andLessThanOrEqualTo("log_Date", DateUtils.formatString(sysLogExp.getLogDateEnd()));
				}
				if(sysLogExp.getSysPartyId() != null){
					temp.andEqualTo("sys_Party_Id", sysLogExp.getSysPartyId());
				}
				if(sysLogExp.getLogEventId() != null){
					temp.andEqualTo("log_Event_Id", sysLogExp.getLogEventId());
				}
				if(sysLogExp.getLogTypeId() != null){
					temp.andEqualTo("log_Type_Id", sysLogExp.getLogTypeId());
				}
				if(sysLogExp.getLogKeyword() != null){
					temp.andEqualTo("log_Key_word", sysLogExp.getLogKeyword());
				}
				
			}
			pageObj = new Page<SysLogExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = sysLogService.queryByExample(example, pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * 
	 * showById:根据ID查看系统日志详情
	 * Date: 2015年9月4日 下午1:39:34 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysLogId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showLogById")
	@ResponseBody
	public SysLog showLogById(Long logId) {
		SysLog result = null;
		
		try {
			result = sysLogService.queryByPk(logId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;

	}
	
	/**
	 * 
	 * showSysUserList:查询操作人list
	 * Date: 2015年8月30日 下午9:05:33 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showSysUserList")
	@ResponseBody
	public List<SysUser> showSysUserList(){
		List<SysUser> result = null;
		
		try {
			result = sysLogService.querySysUserList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	@Autowired
	public void setSysLogService(SysLogService sysLogService) {
		this.sysLogService = sysLogService;
	}
	
	

}

