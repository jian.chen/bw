/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:StoreController.java
 * Package Name:com.sage.scrm.controller.base
 * Date:2015年8月26日上午10:50:01
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.base;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.service.channel.service.ChannelService;

/**
 * ClassName:StoreController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月26日 上午10:50:01 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/channelController")
public class ChannelController {
	
	private Logger logger=Logger.getLogger(getClass());
	private ChannelService channelService;
	
	@RequestMapping("/queryChannelList")
	@ResponseBody
	public DataGridModel<Channel> queryChannelList(HttpServletRequest request, String channelName,String isSeachStatus,int page,int rows){
		String couponName1 = null;
		Page<Channel> pageObj = null;
		List<Channel> list = null;
		DataGridModel<Channel> result = null;
		Long count = null;
		try {
			if(channelName!=null && channelName!=""){
				couponName1 = new String(channelName.getBytes("ISO-8859-1"),"UTF-8");
			}
			pageObj = new Page<Channel>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			list = channelService.findListByChannelName(couponName1, pageObj);
			count = pageObj.getTotalRecord();
			result = new DataGridModel<Channel>(list, count);
		} catch (Exception e) {
			logger.equals(e);
			e.printStackTrace();
		}
		return result;
	}
	
	@Autowired
	public void setChannelService(ChannelService channelService) {
		this.channelService = channelService;
	}

}

