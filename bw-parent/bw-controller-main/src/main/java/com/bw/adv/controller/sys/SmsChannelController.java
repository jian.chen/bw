/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:SmsChannelController.java
 * Package Name:com.sage.scrm.controller.sys
 * Date:2015年8月24日下午2:02:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.sys;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.model.SmsContent;
import com.bw.adv.service.sys.service.SmsChannelService;

/**
 * ClassName:SmsChannelController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月24日 下午2:02:40 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/smsChannelController")
public class SmsChannelController {
	
	private SmsChannelService smsChannelService;
	
	/**
	 * 
	 * showSmsChannelList:查看有效的短信通道列表
	 * Date: 2015年8月24日 下午6:50:18 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showSmsChannelList.do")
	@ResponseBody
	public DataGridModel showSmsChannelList(){
		DataGridModel result = new DataGridModel();
		try {
			List<SmsChannel> smsChannels = smsChannelService.queryAllSmsChannel();
			result.setTotal(Long.valueOf(smsChannels.size()));
            result.setRows(smsChannels);
            result.setStatusSuccess();
		} catch (Exception e) {
            e.printStackTrace();
            result.setStatusError();
        }
        return result;
	}
	
	/**
	 * 
	 * getSmsNameList:查询所有通道名称(用于通道名称不能重复)
	 * Date: 2015年9月21日 下午5:29:14 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	@RequestMapping(method = RequestMethod.GET, value = "getSmsNameList")
	@ResponseBody
	public List<String> getSmsNameList(){
		List<SmsChannel> smsList = null;
		List<String> nameList = null;
		
		try {
			nameList = new ArrayList<String>();
			
			smsList = smsChannelService.querySmsNames();
			
			for(int i=0;i<smsList.size();i++){
				nameList.add(smsList.get(i).getSmsChannelName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return nameList;
	}
     */
	
	
	/**
	 * 
	 * showChannelList:查看启用的通道list
	 * Date: 2015年9月2日 上午11:02:01 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showChannelList")
	@ResponseBody
	public List<SmsChannel> showChannelList(){
		List<SmsChannel> result = null;
		Page<SmsChannel> page = null;
		
		try {
			page = new Page<SmsChannel>();
			result = smsChannelService.querySmsChannel(page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
    
	
	/**
	 * 
	 * showSmsChannelById:查看通道详情
	 * Date: 2015年8月24日 下午6:50:51 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsChannelId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showSmsChannelById.do")
	@ResponseBody
	public JsonModel showSmsChannelById(Long smsChannelId){
        JsonModel jsonModel = new JsonModel();
		try {
            SmsChannel smsChannel = smsChannelService.queryByPk(smsChannelId);
            jsonModel.setStatusSuccess();
            jsonModel.setObj(smsChannel);
		} catch (Exception e) {
			e.printStackTrace();
            jsonModel.setStatusError();
		}
		return jsonModel;
	}
	
	/**
	 * 
	 * editSmsChannel:修改通道信息
	 * Date: 2015年8月24日 下午6:51:22 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsChannel
	 * @return
     */
	@RequestMapping(method = RequestMethod.POST, value = "editSmsChannel.do")
	@ResponseBody
	public JsonModel editSmsChannel(SmsChannel smsChannel){
		JsonModel jsonModel = new JsonModel();
		try {
            if (smsChannelService.updateSmsChannelAccount(smsChannel)) {
                jsonModel.setStatusSuccess();
            } else {
                jsonModel.setStatusError();
            }
		} catch (Exception e) {
			e.printStackTrace();
            jsonModel.setStatusError();
		}
		return jsonModel;
	}

    @RequestMapping(method = RequestMethod.POST, value = "testSmsChannel.do")
    @ResponseBody
    public JsonModel testSmsChannel(Long smsChannelId, String mobile) {
        JsonModel jsonModel = new JsonModel();
        try {
            SmsContent smsContent = new SmsContent();
            smsContent.setMobile(mobile);
            if (smsChannelService.testSmsChannel(smsChannelId, smsContent)) {
                jsonModel.setStatusSuccess();
            } else {
                jsonModel.setStatusError();
            }
        } catch (Exception e) {
            e.printStackTrace();
            jsonModel.setStatusError();
        }
        return jsonModel;
    }

	/**
	 * 
	 * saveSmsChannel:保存短信通道信息
	 * Date: 2015年8月24日 下午6:52:35 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsChannel
	 * @return

	@RequestMapping(method = RequestMethod.POST, value = "saveSmsChannel")
	@ResponseBody
	public int saveSmsChannel(SmsChannel smsChannel){
		int result = 0;
		String code = null;
		try {
			//生成一个编号
			code = codeService.generateCode(SmsChannel.class);
			smsChannel.setSmsChannelCode(code);
			result = smsChannelService.saveSelective(smsChannel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
     */
	
	/**
	 * 
	 * removeSmsChannel:批量删除短信通道信息(修改状态为不启用)
	 * Date: 2015年8月24日 下午6:53:12 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsChannelIds
	 * @return

	@RequestMapping(method = RequestMethod.POST, value = "removeSmsChannel")
	@ResponseBody
	public int removeSmsChannel(String smsChannelIds){
		List<Long> smsChannelIdList = null;
		String[] ids = null;
		int result = 0;
		try {
			smsChannelIdList = new ArrayList<Long>();
			ids = smsChannelIds.split(",");
			for(int i=0;i<ids.length;i++){
				smsChannelIdList.add(Long.parseLong(ids[i]));
			}
			
			smsChannelService.deleteSmsChannelList(smsChannelIdList);
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
     */
	
	@Autowired
	public void setSmsChannelService(SmsChannelService smsChannelService) {
		this.smsChannelService = smsChannelService;
	}


}

