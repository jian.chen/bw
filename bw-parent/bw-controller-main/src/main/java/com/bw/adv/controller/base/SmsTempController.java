/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:SmsTempController.java
 * Package Name:com.sage.scrm.controller.base
 * Date:2015年8月26日下午6:22:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.component.msg.model.SmsTemp;
import com.bw.adv.module.component.msg.model.SmsTempParams;
import com.bw.adv.module.component.msg.model.exp.SmsInstanceExp;
import com.bw.adv.module.component.msg.model.exp.SmsTempExp;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.SmsTempService;

/**
 * ClassName:SmsTempController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月26日 下午6:22:11 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/smsTempController")
@SuppressWarnings("unused")
public class SmsTempController {
	
	private SmsTempService smsTempService;
	
	/**
	 * 
	 * showSmsList:查看模板列表
	 * Date: 2015年8月26日 下午7:06:31 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showSmsList")
	@ResponseBody
	public Map<String, Object> showSmsList(int page,int rows){
		
		List<SmsTempExp> result = null;
		Page<SmsTempExp> pageObj = null;
		Map<String, Object> map = null;
		try {
			map = new HashMap<String, Object>();
			pageObj = new Page<SmsTempExp>();
			
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = smsTempService.querySmsExpByStatusId(pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * 
	 * showSmsTempDetail:查看短信模板详情
	 * Date: 2015年8月26日 下午7:55:37 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTempId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showSmsTempDetail")
	@ResponseBody
	public JsonModel showSmsTempDetail(Long smsTempId){
		SmsTempExp smstemp = null;
		JsonModel result = null;
		try {
			result = new JsonModel();
			smstemp = smsTempService.queryExpById(smsTempId);
			result.setStatusSuccess();
			result.setObj(smstemp);
		} catch (Exception e) {
			result.setStatusError();
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * editSmsTemp:保存修改模板信息
	 * Date: 2015年8月27日 上午10:07:42 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTemp
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editSmsTemp")
	@ResponseBody
	public JsonModel editSmsTemp(SmsTemp smsTemp,HttpServletRequest request){
		JsonModel result = null;
		int editResult = 0;
		try {
			smsTemp.setUpdateBy(SysUserUtil.getSessionUser(request).getUserId());//获取当前系统用户
			smsTemp.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			editResult = smsTempService.updateByPkSelective(smsTemp);
			result = new JsonModel(JsonModel.Status_Success,"保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			result = new JsonModel(JsonModel.Status_Error,"保存失败!");
		}
		return result;
	}
	
	/**
	 * 更新短信模板状态
	 * @param smsTempId
	 * @param request
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "updateSmsTempStatus")
	@ResponseBody
	public JsonModel updateSmsTempStatus(Long smsTempId,String status,HttpServletRequest request){
		JsonModel result = null;
		SmsTemp smsTemp = null;
		try {
			smsTemp = new SmsTemp();
			smsTemp.setSmsTempId(smsTempId);
			if(status.equals("Y")){
				smsTemp.setStatusId(StatusConstant.SMS_TEMP_IS_ACTIVE.getId());
			}else if(status.equals("N")){
				smsTemp.setStatusId(StatusConstant.SMS_TEMP_IS_NOT_ACTIVE.getId());
			}else{
				result = new JsonModel(JsonModel.Status_Error,"操作失败!");
				return result;
			}
			smsTemp.setUpdateBy(SysUserUtil.getSessionUser(request).getUserId());//获取当前系统用户
			smsTemp.setUpdateTime(DateUtils.getCurrentTimeOfDb());
			smsTempService.updateByPkSelective(smsTemp);
			result = new JsonModel(JsonModel.Status_Success,"操作成功!");
		} catch (Exception e) {
			e.printStackTrace();
			result = new JsonModel(JsonModel.Status_Error,"操作失败!");
		}
		return result;
	}
	
	/**
	 * 
	 * saveSmsTemp:保存新建模板
	 * Date: 2015年8月27日 上午10:10:54 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTemp
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveSmsTemp")
	@ResponseBody
	public int saveSmsTemp(SmsTemp smsTemp,HttpServletRequest request){
		int result = 0;
		try {
			//获取当前系统用户
			smsTemp.setCreateBy(SysUserUtil.getSessionUser(request).getUserId());
			smsTemp.setCreateTime(DateUtils.getCurrentTimeOfDb());
			
			result = smsTempService.saveSelective(smsTemp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * saveSmsInstance:保存发送信息
	 * Date: 2015年8月28日 下午3:23:47 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberGroupId
	 * @param smsTempId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveSmsInstance")
	@ResponseBody
	public int saveSmsInstance(Long memberGroupId,Long smsTempId){
		int result = 0;
		
		try {
			result = smsTempService.addSmsInstanceList(memberGroupId, smsTempId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	/**
	 * 
	 * removeSmsTempList:批量删除短信模板（逻辑删除）
	 * Date: 2015年8月27日 上午10:28:44 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTempIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "removeSmsTempList")
	@ResponseBody
	public int removeSmsTempList(String smsTempIds){
		int result = 0 ;
		String[] ids = null;
		List<Long> smsTempIdList = null;
		
		try {
			smsTempIdList = new ArrayList<Long>();
			ids = smsTempIds.split(",");
			for(int i=0;i<ids.length;i++){
				smsTempIdList.add(Long.parseLong(ids[i]));
			}
			
			smsTempService.deleteSmsTempList(smsTempIdList);
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * showMemberGroup:查询会员分组
	 * Date: 2015年8月27日 上午10:42:09 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showMemberGroup")
	@ResponseBody
	public List<MemberGroup> showMemberGroup(){
		List<MemberGroup> result = null;
				
		try {
			result = smsTempService.queryMemberGroupList();
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		return result;
		
	}
	
	/**
	 * 
	 * showParsmsList:查询所有短信通道的参数类型
	 * Date: 2015年9月5日 下午8:22:49 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showParsmsList")
	@ResponseBody
	public JsonModel showParsmsList(){
		List<SmsTempParams> smsTempList = null;
		JsonModel result = null;
		try {
			result = new JsonModel();
			smsTempList = smsTempService.queryParamsList();
			result.setStatusSuccess();
			result.setObj(smsTempList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	
	/**
	 * 
	 * showSmsInstanceByExampleList:根据条件查询短信实例
	 * Date: 2015年8月27日 上午11:11:31 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showSmsInstanceByExampleList")
	@ResponseBody
	public Map<String, Object> showSmsInstanceByExampleList(String mobile,Long smsTempId,int page,int rows){
		Map<String, Object> map = null;
		List<SmsInstanceExp> smsInstanceList = null;
		Page<SmsInstanceExp> pageObj = null;
		Example example = null;
		JsonModel result = null;
		try {
			result = new JsonModel();
			map = new HashMap<String, Object>();
			example = new Example();
			Criteria temp = example.createCriteria();
			if(smsTempId != null){
				temp.andEqualTo("si.sms_temp_id", smsTempId);
			}else{
				return null;
			}
			
			if(StringUtils.isNotBlank(mobile)){
				mobile = "%"+mobile+"%";
				temp.andLike("mobile", mobile);
			}
			
			pageObj = new Page<SmsInstanceExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			smsInstanceList = smsTempService.querySmsInstanceExpByExample(example, pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	@Autowired
	public void setSmsTempService(SmsTempService smsTempService) {
		this.smsTempService = smsTempService;
	}
	
	

}

