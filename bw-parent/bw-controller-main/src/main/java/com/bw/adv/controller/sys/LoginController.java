package com.bw.adv.controller.sys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.controller.sys.utils.SysUserUtil;
import com.bw.adv.core.utils.MD5Utils;
import com.bw.adv.filter.domain.ScrmAuthorizationInfo;
import com.bw.adv.filter.utils.ShiroConstants;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.sys.enums.SysSystemEnum;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.SysLicense;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.OrgService;
import com.bw.adv.service.base.service.StatusService;
import com.bw.adv.service.sys.service.SysFunctionService;
import com.bw.adv.service.sys.service.SysRoleService;
import com.bw.adv.service.sys.service.SysUserService;
import com.google.code.kaptcha.Constants;


/**
 * Hello world!
 * 
 */
@Controller
@RequestMapping("/")
@SuppressWarnings("unused")
public class LoginController {
	private SysUserService sysUserService;
	private StatusService statusService;
	private OrgService orgService;
	private SysRoleService sysRoleService;
	private SysFunctionService sysFunctionService;
	
	@RequestMapping(method = RequestMethod.GET, value = "login")
	public String preLogin() {
		SysLicense sysLicense = null;
		sysLicense = sysUserService.getSysActiveInfo();
		if( sysLicense != null && sysLicense.getIsActive().equals("Y")){
			return "forward:/scrm/view/redirect.jsp";
		}else{
			return "forward:/scrm/view/license.jsp";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/unauthorized")
	@ResponseBody
	public JsonModel unauthorized() {
		return new JsonModel(JsonModel.Status_Unauthorized, "unauthorized");
	}

	@RequestMapping(method = RequestMethod.POST, value = "/onActiveSys")
	@ResponseBody
	public JsonModel onActiveSys(String code){
		JsonModel result = null;
		try {
			SysLicense sysLicense = null;
			sysLicense = sysUserService.getSysActiveInfo();
			if(MD5Utils.getMD5String(code+sysLicense.getAuthAccount()).equals(sysLicense.getLicenseCode())){
				sysLicense.setIsActive("Y");
				sysLicense.setAuthTime(DateUtils.getCurrentTimeOfDb());
				sysUserService.updateByPkSelective(sysLicense);
				result = new JsonModel(JsonModel.Status_Success,"Y");
			}else{
				result = new JsonModel(JsonModel.Status_Success,"N");
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			result = new JsonModel(JsonModel.Status_Error,"激活失败，请稍后重试！");
			return result;
		}
	}

	/**
	 * login:登陆 <br/>
	 * Date: 2015-8-24 下午5:21:09 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param request
	 * @param user
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/onlogin", method = RequestMethod.POST)
//	@ResponseBody
	public String onlogin(SysUser user,ModelMap model, HttpServletRequest request,HttpServletResponse response) throws IOException{
		String message = null;
		String menu = "";
		Map<String,Object> menuMap =null;
		UsernamePasswordToken token = null;
		Subject subject = null;
		SysLicense sysLicense = null;
		try {
			//登录时候做系统激活判定
			sysLicense = sysUserService.getSysActiveInfo();
			if(sysLicense.getIsActive().equals("N")){
				return "forward:/scrm/view/license.jsp";
			}
			
			if (!this.validateTheCode(user.getValidateCode(), request.getSession())) {
				message = "验证码错误！";
				request.getSession().setAttribute("message", message);
				return "redirect:login.htm";
			}
			
			subject = SecurityUtils.getSubject();

			token = new UsernamePasswordToken(user.getUserName(),MD5Utils.getMD5String(user.getPassword()), true,SysUserUtil.getIpAddress(request));
			subject.login(token);
			boolean result = subject.isPermitted("/scrm/view/index.htm");
			
			subject = SecurityUtils.getSubject();
			Object authInfo = subject.getSession().getAttribute(ShiroConstants.AUTH_SESSION_KEY);
			if(authInfo == null){
				return "redirect:login.htm";
			}
			
			ScrmAuthorizationInfo info = (ScrmAuthorizationInfo) authInfo;

			menuMap = sysFunctionService.findSysFunctionByUser(request,response,info.getSysFunctions());
			
			request.getSession().setAttribute(SysUserUtil.SESSION_USER_KEY, subject.getPrincipal());
			request.getSession().setAttribute("menu",menuMap.get("menu"));
			
			return "redirect:/scrm/view/index.htm";
		} catch (UnknownAccountException uae) {// 帐号未找到
			uae.printStackTrace();
			message = "帐号不存在,请检查!";
		} catch (IncorrectCredentialsException ice) {// 帐号或密码认证失败
			ice.printStackTrace();
			message = "帐号或密码错误,请检查!";
		} catch (AuthenticationException ae) {// 一般在身份验证过程中由于错误引发的异常。
			ae.printStackTrace();
			message = "登录失败,请联系系统管理员!";
		}
		token.clear();
		request.getSession().setAttribute("message", message);
		return "redirect:login.htm";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "loginout")
	public String doLoginout(HttpServletResponse response) {
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setHeader("cache-control", "no-store");
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:login.htm";
	}
	
	/**
	 * 获取客户端的真实IP地址
	 */
	protected String getIpAddress(HttpServletRequest request) {

		final String unknown = "unknown";
		String ip = request.getHeader("x-forwarded-for");
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
			ip = request.getHeader("Proxy-Client-IP");
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
			ip = request.getHeader("WL-Proxy-Client-IP");
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
			ip = request.getRemoteAddr();
		return ip;
	}
	
	/**
	 * menu:获取角色对应权限的操作菜单. <br/>
	 * Date: 2015-8-26 上午10:55:25 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param request
	 * @param session
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/menu", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> menu(HttpServletRequest request,HttpSession session,HttpServletResponse response) throws IOException{

		SysUser sysUser=null;
		List<SysFunction>  sysFunctionList=null;
		List<SysRole> sysRoles=null;
		Org org=null;
		
		String menu="";
		Map<String,Object> map=new HashMap<String,Object>();
		List<SysFunction> parentList = new ArrayList<SysFunction>();
		List<SysFunction> list = new ArrayList<SysFunction>();
		try {		

			sysUser = SysUserUtil.getSessionUser(request);//用户信息
			response.setCharacterEncoding("utf-8");
			
			if(sysUser!=null){
				sysRoles=sysRoleService.findSysRoleListByUserId(sysUser.getUserId());//用户角色信息
				sysFunctionList=sysFunctionService.findListForSysFunctionByUserId(sysUser.getUserId(), SysSystemEnum.AIR.getSystemId());//功能权限
				for (SysFunction function : sysFunctionList) {
					if(function.getPareFunction()!=null){
						list.add(function);
					}else{
						parentList.add(function);
					}
				}
				
				for(SysFunction pareSysFunction:parentList){
					menu+="<li class=\"menu_icon_jcsj\">"+pareSysFunction.getFunctionName()+"</li>";
					menu+="<ol style=\"display: none;\">";
					for(SysFunction sysFunction:list){
						if(pareSysFunction.getFunctionId()==sysFunction.getPareFunction()){
							menu+="<li data-url=\""+sysFunction.getUrl()+"\">"+sysFunction.getFunctionName()+"</li>";
						}
					}
					menu+="</ol>";
				}
				
				map.put("sysUser", sysUser);
				map.put("menu", menu);
			}else{
				System.out.println("未获取用户信息！");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	
	@Autowired
	public void setSysUserService(SysUserService sysUserService) {
		this.sysUserService = sysUserService;
	}
	@Autowired
	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}
	@Autowired
	public void setOrgService(OrgService orgService) {
		this.orgService = orgService;
	}
	@Autowired
	public void setSysRoleService(SysRoleService sysRoleService) {
		this.sysRoleService = sysRoleService;
	}
	@Autowired
	public void setSysFunctionService(SysFunctionService sysFunctionService) {
		this.sysFunctionService = sysFunctionService;
	}	
	
	private boolean validateTheCode(String pageValidateCode, HttpSession session) {
		System.out.println((String) session.getAttribute(Constants.KAPTCHA_SESSION_KEY));
		return pageValidateCode.equalsIgnoreCase((String) session.getAttribute(Constants.KAPTCHA_SESSION_KEY));
	}
	public static void main(String[] args) {
		System.out.println(MD5Utils.getMD5String("admin"));
		System.out.println(MD5Utils.getMD5String("a996-4d38-b300bkcrm").equals("10b83f7f008447a33c3867be11c10156"));
	}
	
}
