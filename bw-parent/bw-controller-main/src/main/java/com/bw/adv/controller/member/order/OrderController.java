package com.bw.adv.controller.member.order;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.constant.ChannelConstant;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.common.model.JsonModel;
import com.bw.adv.module.order.model.dto.OrderHeaderDto;
import com.bw.adv.module.order.model.dto.OrderInfo;
import com.bw.adv.module.order.model.exp.OrderHeaderExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.order.service.OrderHeaderService;

/**
 * ClassName:OrderController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月31日 下午12:21:23 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/orderController")
@SuppressWarnings("unused")
public class OrderController {
	
	private OrderHeaderService orderHeaderService;
	//private OrderParseSupport orderParseSupport;
	/**
	 * 
	 * showListByExample:查看订单列表
	 * Date: 2015年8月31日 下午3:51:22 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param orderExp
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showListByExample")
	@ResponseBody
	public Map<String, Object> showListByExample(OrderHeaderExp orderExp,int page,int rows){
		Map<String, Object> map = null;
		List<OrderHeaderExp> result = null;
		Page<OrderHeaderExp> pageObj = null;
		Example example = null;
		
		try {
			map = new HashMap<String, Object>();
			example = new Example();
			Criteria temp = example.createCriteria();
			if(orderExp != null){
				//会员编号
				if(orderExp.getMemberCode() != null && orderExp.getMemberCode() != ""){
					temp.andEqualTo("MEM.MEMBER_CODE", orderExp.getMemberCode());
				}
				//订单日期
				if(orderExp.getOrderDateStart() != null && orderExp.getOrderDateStart() != "" ){
					temp.andGreaterThanOrEqualTo("OH.ORDER_DATE", DateUtils.formatString(orderExp.getOrderDateStart()));
				}
				if(orderExp.getOrderDateEnd() != null && orderExp.getOrderDateEnd() != ""){
					temp.andLessThanOrEqualTo("OH.ORDER_DATE", DateUtils.formatString(orderExp.getOrderDateEnd()));
				}
				//订单金额
				/*if(orderExp.getOrderAmountStart() != null && orderExp.getOrderAmountStart() != ""){
					temp.andGreaterThanOrEqualTo("OH.ORDER_AMOUNT", orderExp.getOrderAmountStart());
				}
				if(orderExp.getOrderAmountEnd() != null && orderExp.getOrderAmountEnd() != ""){
					temp.andLessThanOrEqualTo("OH.ORDER_AMOUNT", orderExp.getOrderAmountEnd());
				}*/
				//实付金额
				if(orderExp.getPayAmountStart() != null && orderExp.getPayAmountStart() != ""){
					temp.andGreaterThanOrEqualTo("OH.PAY_AMOUNT", orderExp.getPayAmountStart());
				}
				if(orderExp.getPayAmountEnd() != null && orderExp.getPayAmountEnd() != ""){
					temp.andLessThanOrEqualTo("OH.PAY_AMOUNT", orderExp.getPayAmountEnd());
				}
				//订单编号
				if(orderExp.getOrderCode() != null && orderExp.getOrderCode() != ""){
					temp.andLike("OH.ORDER_CODE", "%"+orderExp.getOrderCode()+"%");
				}
				//渠道
				if(orderExp.getChannelId() != null){
					temp.andEqualTo("OH.CHANNEL_ID", orderExp.getChannelId());
				}
				//状态
				if(orderExp.getStatusId() != null){
					temp.andEqualTo("OH.STATUS_ID", orderExp.getStatusId());
				}
				if(orderExp.getStoreName() != null && orderExp.getStoreName() != ""){
					temp.andLike("st.store_name", "%"+orderExp.getStoreName()+"%");
				}
			}
			pageObj = new Page<OrderHeaderExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = orderHeaderService.queryExpByExample(example, pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return map;
	}
	
	/**
	 * 
	 * addPoints:(补订单). <br/>
	 * Date: 2016年9月20日 上午11:13:35 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param orderHeaderDto
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "addOrderHeader")
	@ResponseBody
	public JsonModel addOrderHeader(OrderHeaderDto orderHeaderDto) {
		JsonModel result = null;
		OrderInfo orderInfo = null;
		String billInfo = null;
		BigDecimal payAmount = null;
		String orderDate = null;
		double pay = 0;
		
		try {
			payAmount = orderHeaderDto.getPayAmount();			
			pay = payAmount.doubleValue();
			
			if(pay < 0){
				orderHeaderDto.setOptType("d");
			}else{
				orderHeaderDto.setOptType("i");
			}
			orderHeaderDto.setOrderAmount(payAmount);
			orderDate = DateUtils.formatString(orderHeaderDto.getHappenTime());
			if(StringUtils.isNotBlank(orderDate)){				
				orderHeaderDto.setHappenTime(DateUtils.formatString(orderHeaderDto.getHappenTime()));
				orderHeaderDto.setCreateTime(DateUtils.formatString(orderHeaderDto.getHappenTime()));
				orderDate = orderDate.substring(0, 8);
				orderHeaderDto.setOrderDate(orderDate);
				orderHeaderDto.setOrderDesc("系统补订单");
				orderHeaderDto.setChannelId(ChannelConstant.SCRM_WEB.getId());
			}
			
			//orderInfo = orderParseSupport.parseOrderInfo(orderHeaderDto);
			billInfo = orderHeaderService.handleSyncOrder(orderInfo,orderHeaderDto.getOptType());		
			result = new JsonModel(JsonModel.Status_Success,"补订单成功，请到 订单查询 页面查看！");
			return result;
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		result = new JsonModel(JsonModel.Status_Error,"信息填写错误 ，补订单失败，请稍后重试！");
		return result;
	}
	
	@Autowired
	public void setOrderHeaderService(OrderHeaderService orderHeaderService) {
		this.orderHeaderService = orderHeaderService;
	}

//	@Autowired
//	public void setOrderParseSupport(OrderParseSupport orderParseSupport) {
//		this.orderParseSupport = orderParseSupport;
//	}
}
