/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:ActivityDateController.java
 * Package Name:com.sage.scrm.controller.base
 * Date:2015年8月27日下午7:17:13
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.activity.enums.ActivityDateTypeEnum;
import com.bw.adv.module.activity.enums.ActivityValidityEnum;
import com.bw.adv.module.base.model.ActivityDate;
import com.bw.adv.module.component.msg.model.SmsTemp;
import com.bw.adv.module.component.msg.model.exp.SmsTempExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.service.base.service.ActivityDateService;

/**
 * ClassName:ActivityDateController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月27日 下午7:17:13 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/activityDateController")
@SuppressWarnings("unused")
public class ActivityDateController {
	
	private ActivityDateService activityDateService;
	
	/**
	 * 
	 * showActivityDateList:查看活动日期列表
	 * Date: 2015年8月27日 下午8:08:42 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "showActivityDateList")
	@ResponseBody
	public Map<String, Object> showActivityDateList(int page,int rows){
		Map<String, Object> map = null;
		List<ActivityDate> result = null;
		Page<ActivityDate> pageObj = null;
		
		try {
			map = new HashMap<String, Object>();
			pageObj = new Page<ActivityDate>();
			
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			result = activityDateService.queryActivityDateByIsActive(pageObj);
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * 
	 * showActivityDateDetail:查看活动日期详情
	 * Date: 2015年8月27日 下午8:08:22 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param ActivityDateId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showActivityDateDetail")
	@ResponseBody
	public ActivityDate showActivityDateDetail(Long activityDateId){
		ActivityDate result = null;
		
		try {
			
			result = activityDateService.queryByPk(activityDateId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 
	 * editActivityDate:保存修改的活动日期
	 * Date: 2015年8月27日 下午8:07:52 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param activityDate
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "editActivityDate")
	@ResponseBody
	public int editActivityDate(ActivityDate activityDate){
		int result = 0;
		try {
			//日期类型为固定日期，将开始日期转换为适合存入数据库的串
			if(ActivityDateTypeEnum.FIXED.getId().equals(activityDate.getActivityDateType())){
				activityDate.setFromDate(DateUtils.formatString(activityDate.getFromDate()));
			}
			activityDate.setUpadeTime(DateUtils.getCurrentTimeOfDb());
			
			result = activityDateService.updateByPkSelective(activityDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * saveActivityDate:保存新建活动日期
	 * Date: 2015年8月27日 下午8:07:28 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param activityDate
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "saveActivityDate")
	@ResponseBody
	public int saveActivityDate(ActivityDate activityDate){
		int result = 0;
		try {
			//日期类型为固定日期，将开始日期转换为适合存入数据库的串
			if(ActivityDateTypeEnum.FIXED.getId().equals(activityDate.getActivityDateType())){
				activityDate.setFromDate(DateUtils.formatString(activityDate.getFromDate()));
			}
			activityDate.setCreateTime(DateUtils.getCurrentTimeOfDb());
			
			result = activityDateService.saveSelective(activityDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * removeActivityDateList:批量删除活动日期
	 * Date: 2015年8月27日 下午8:06:45 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param activityDateIds
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "removeActivityDateList")
	@ResponseBody
	public int removeActivityDateList(String activityDateIds){
		int result = 0 ;
		String[] ids = null;
		List<Long> activityDateIdList = null;
		
		try {
			activityDateIdList = new ArrayList<Long>();
			ids = activityDateIds.split(",");
			for(int i=0;i<ids.length;i++){
				activityDateIdList.add(Long.parseLong(ids[i]));
			}
			
			activityDateService.deleteActivityDateList(activityDateIdList);
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Autowired
	public void setActivityDateService(ActivityDateService activityDateService) {
		this.activityDateService = activityDateService;
	}
	
	

}

