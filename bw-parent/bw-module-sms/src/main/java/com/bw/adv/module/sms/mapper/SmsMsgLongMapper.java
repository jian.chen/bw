package com.bw.adv.module.sms.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sms.SmsMsgLog;

/**
 * Created by jeoy.zhou on 3/15/16.
 */
public interface SmsMsgLongMapper extends BaseMapper<SmsMsgLog> {

}
