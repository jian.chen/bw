package com.bw.adv.module.sms.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sms.SmsMsgLog;
import com.bw.adv.module.sms.mapper.SmsMsgLongMapper;
import com.bw.adv.module.sms.repository.SmsMsgLogRepository;

import org.springframework.stereotype.Repository;

/**
 * Created by jeoy.zhou on 3/15/16.
 */
@Repository
public class SmsMsgLogRepositoryImpl extends BaseRepositoryImpl<SmsMsgLog, SmsMsgLongMapper> implements SmsMsgLogRepository {

        @Override
        protected Class<SmsMsgLongMapper> getMapperClass() {
                return SmsMsgLongMapper.class;
        }
}
