package com.bw.adv.module.sms.service.impl;

import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.enums.SmsChannelEnum;
import com.bw.adv.module.sms.model.SmsContent;
import com.bw.adv.module.sms.model.SmsResponse;
import com.bw.adv.module.sms.service.AbstractSmsService;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jeoy.zhou on 3/14/16.
 */
@Service("etonenetSmsService")
public class EtonenetSmsServiceImpl extends AbstractSmsService {

    private static final Logger LOGGER = Logger.getLogger(EtonenetSmsServiceImpl.class);
    /**
     * Hex编码字符组
     */
    private static final char[] DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    private static final String URL = "%s?command=%s&spid=%s&sppassword=%s&spsc=%s&da=%s&sm=%s&dc=%s";
    private static final String MT_URL = "http://esms4.etonenet.com/sms/mt";
    // 参数 command 为单条下行请求指令 MT_REQUEST
    private static final String COMMON = "MT_REQUEST";
    // 参数 spsc 为 SP 服务代码,可选,不填则默认为缺省服务代码“00”;
    private static final String SPSC = "00";
    // 参数 dc 为消息内容编码格式,必须填写;
    private static final int dc = 15;
    private static final String ECODE = "GBK";
    private static final String SUCCESS_CODE = "000";


    @Override
    protected SmsResponse sendTest(SmsChannel channel, SmsContent smsContent) throws Exception {
        return send(channel, smsContent);
    }

    @Override
    protected SmsResponse send(SmsChannel channel, SmsContent smsContent) throws Exception {
        if (LOGGER.isDebugEnabled())
            LOGGER.debug(String.format("sendMsg to mobile[%s], content[%s], by channelCode[%s]",
                    smsContent.getMobile(), smsContent.getContent(), getSmsCode()));
        String smsUrl = String.format(URL, channel.getUrl(), COMMON, channel.getAppId(), channel.getAppSecret(), SPSC,
                smsContent.getMobile(), new String(Hex.encodeHex(smsContent.getContent().getBytes(ECODE))), dc);
        String response = doGetRequest(smsUrl);
        if (LOGGER.isDebugEnabled())
            LOGGER.debug(String.format("receivedMsg[%s], by channelCode[%s] and mobile[%s]", response, getSmsCode(), smsContent.getMobile()));
        return parseResponse(response);
    }

    private SmsResponse parseResponse(String response) {
        SmsResponse smsResponse = new SmsResponse();
        Map<String, String> map = parseResStr(response);
        smsResponse.setResponseCode(map.get("mterrcode"));
        smsResponse.setIsPass(SUCCESS_CODE.equals(smsResponse.getResponseCode()));
        smsResponse.setMsgId(map.get("mtmsgid"));
        return smsResponse;
    }

    /**
     * 将 短信下行 请求响应字符串解析到一个HashMap中
     * @param resStr
     * @return
     */
    public static Map<String, String> parseResStr(String resStr) {
        Map<String, String> result = new HashMap<String, String>();
        try {
            String[] ps = resStr.split("&");
            for (int i = 0; i < ps.length; i++) {
                int ix = ps[i].indexOf("=");
                if (ix != -1) {
                    result.put(ps[i].substring(0, ix), ps[i].substring(ix + 1));
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    /**
     * 单条下行示例
     * @throws Exception

    public static HashMap testSingleMt(String phone,String content) throws Exception {
        //操作命令、SP编号、SP密码，必填参数
        String command = "MT_REQUEST";
        //sp服务代码，可选参数，默认为 00
        String spsc = "00";
        //源号码，可选参数
        //String sa = "10657109053657";
        //目标号码，必填参数
        //String da = phone;
        //下行内容以及编码格式，必填参数
        int dc = 15;
        String ecodeform = "GBK";
        String sm = new String(Hex.encodeHex(content.getBytes(ecodeform)));

        //组成url字符串
        String smsUrl = mtUrl + "?command=" + command + "&spid=" + spid + "&sppassword=" + sppassword + "&spsc=" + spsc
                + "&da=" + phone + "&sm=" + sm + "&dc=" + dc;

        //发送http请求，并接收http响应
        String resStr = doGetRequest(smsUrl.toString());
        System.out.println(resStr);

        //解析响应字符串
        HashMap pp = parseResStr(resStr);
        return pp;
    }
*/


    /**
     * 将普通字符串转换成Hex编码字符串
     *
     * @param dataCoding 编码格式，15表示GBK编码，8表示UnicodeBigUnmarked编码，0表示ISO8859-1编码
     * @param realStr 普通字符串
     * @return Hex编码字符串

    public static String encodeHexStr(int dataCoding, String realStr) {
        String hexStr = null;

        if (realStr != null) {
            byte[] data = null;
            try {
                if (dataCoding == 15) {
                    data = realStr.getBytes("GBK");
                } else if ((dataCoding & 0x0C) == 0x08) {
                    data = realStr.getBytes("UnicodeBigUnmarked");
                } else {
                    data = realStr.getBytes("ISO8859-1");
                }
            } catch (UnsupportedEncodingException e) {
                System.out.println(e.toString());
            }

            if (data != null) {
                int len = data.length;
                char[] out = new char[len << 1];
                // two characters form the hex value.
                for (int i = 0, j = 0; i < len; i++) {
                    out[j++] = DIGITS[(0xF0 & data[i]) >>> 4];
                    out[j++] = DIGITS[0x0F & data[i]];
                }
                hexStr = new String(out);
            }
        }
        return hexStr;
    }
     */

    /**
     * 发送http GET请求，并返回http响应字符串
     *
     * @param urlstr 完整的请求url字符串
     * @return
     */
    public static String doGetRequest(String urlstr) {
        String res = null;
        try {
            URL url = new URL(urlstr);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setRequestMethod("GET");
            httpConn.setRequestProperty("Content-Type", "text/html; charset=GB2312");
            //System.setProperty("sun.net.client.defaultConnectTimeout", "5000");//jdk1.4换成这个,连接超时
            //System.setProperty("sun.net.client.defaultReadTimeout", "10000"); //jdk1.4换成这个,读操作超时
            httpConn.setConnectTimeout(5000);//jdk 1.5换成这个,连接超时
            httpConn.setReadTimeout(10000);//jdk 1.5换成这个,读操作超时
            httpConn.setDoInput(true);
            int rescode = httpConn.getResponseCode();
            if (rescode == 200) {
                BufferedReader bfw = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
                res = bfw.readLine();
            } else {
                res = "Http request error code :" + rescode;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return res;
    }


    @Override
    public String getSmsCode() {
        return SmsChannelEnum.ETONENET.getSmsCode();
    }
}
