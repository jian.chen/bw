/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:ComVerificationRepositoryImpl.java
 * Package Name:com.sage.scrm.module.sys.repository.impl
 * Date:2015年11月10日下午8:25:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sms.mapper.ComVerificationMapper;
import com.bw.adv.module.sms.repository.ComVerificationRepository;
import com.bw.adv.module.sys.model.ComVerification;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:ComVerificationRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月10日 下午8:25:18 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ComVerificationRepositoryImpl extends BaseRepositoryImpl<ComVerification, ComVerificationMapper>
	implements ComVerificationRepository {

	@Override
	protected Class<ComVerificationMapper> getMapperClass() {
		
		return ComVerificationMapper.class;
	}

	@Override
	public List<ComVerification> findByExample(Example example) {
		
		return this.getMapper().selectComvByExample(example);
	}

}

