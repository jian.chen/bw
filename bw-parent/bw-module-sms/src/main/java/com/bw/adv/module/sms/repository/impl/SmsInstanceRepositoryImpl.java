/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsInstanceRepositoryImpl.java
 * Package Name:com.sage.scrm.module.component.msg.repository.impl
 * Date:2015年8月19日下午1:25:20
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.component.msg.model.exp.SmsInstanceExp;
import com.bw.adv.module.sms.mapper.SmsInstanceMapper;
import com.bw.adv.module.sms.repository.SmsInstanceRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:SmsInstanceRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午1:25:20 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SmsInstanceRepositoryImpl extends BaseRepositoryImpl<SmsInstance, SmsInstanceMapper> implements SmsInstanceRepository {

	@Override
	protected Class<SmsInstanceMapper> getMapperClass() {
		
		return SmsInstanceMapper.class;
	}

	@Override
	public List<SmsInstance> findByMobile(String mobile) {
		
		return this.getMapper().selectByMobile(mobile);
	}
	
	@Override
	public int saveList(List<SmsInstance> recordList) {
		
		return this.getMapper().insertBatch(recordList);
	}

	@Override
	public List<SmsInstance> findByExample(Example example,Page<SmsInstance> page) {
		return this.getMapper().selectByExample(example,page);
	}

	@Override
	public List<SmsInstanceExp> findExpByExample(Example example,
			Page<SmsInstanceExp> page) {
		
		return this.getMapper().selectExpByExample(example, page);
	}

}

