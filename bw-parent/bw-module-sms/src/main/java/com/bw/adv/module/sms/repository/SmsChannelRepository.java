/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsChannelRepository.java
 * Package Name:com.sage.scrm.module.component.msg.repository
 * Date:2015年8月17日下午2:25:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.mapper.SmsChannelMapper;

import java.util.List;

/**
 * ClassName:SmsChannelRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午2:25:33 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SmsChannelRepository extends BaseRepository<SmsChannel, SmsChannelMapper> {


	/**
	 * 查询所有的短信渠道
	 * @return
	 */
	public List<SmsChannel> queryAllSmsChannel();

	/**
	 * 更新短信账号配置
	 * @param smsChannel
	 * @return
	 */
	public boolean updateSmsChannelAccount(SmsChannel smsChannel);

	/**
	 * 关闭所有启用的短信通道
	 */
	public void updateSmsChannelUnActive();

	/**
	 * 获取启用的短信通道
	 * @return
	 */
	public SmsChannel getActiveSmsChannel();

	/**
	 * 
	 * findByStatus:根据状态查询通道List
	 * Date: 2015年9月2日 下午2:00:54 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param isActive
	 * @param page
	 * @return
	 */
	public List<SmsChannel> findByStatus(String isActive,Page<SmsChannel> page);
	
	/**
	 * 
	 * findByStatusInit:根据状态查询通道List
	 * Date: 2015年9月2日 下午2:01:39 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param isActive
	 * @return
	 */
	public List<SmsChannel> findByStatusInit(String isActive);
	
	/**
	 * 
	 * findCountByStatus:根据状态查询通道个数
	 * Date: 2015年9月2日 下午2:04:44 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param isActive
	 * @return
	 */
	public Long findCountByStatus(String isActive);
	
	/**
	 * 
	 * updateIsActiveList:根据id批量修改通道的状态
	 * Date: 2015年9月2日 下午2:05:04 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsChannelIds
	 */
	public void updateIsActiveList(List<Long> smsChannelIds);
	
	/**
	 * 
	 * findAllName:查询所有通道名称
	 * Date: 2015年9月21日 下午5:34:03 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<SmsChannel> findAllName();
}

