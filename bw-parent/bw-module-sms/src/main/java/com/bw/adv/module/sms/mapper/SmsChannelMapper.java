package com.bw.adv.module.sms.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsChannel;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SmsChannelMapper extends BaseMapper<SmsChannel> {
	
	List<SmsChannel> selectByStatus(@Param("isActive")String isActive,Page<SmsChannel> page);

	List<SmsChannel> selectByStatusInit(String isActive);

	Long selectCountByStatus(String isActive);
	
	int updataIsActiveList(List<Long> smsChannelIds);
	
	List<SmsChannel> selectAllName();


    /**
     * 查询所有的短信渠道
     * @return
     */
    List<SmsChannel> selectAllSmsChannel();

    /**
     * 更新渠道账号信息
     * @param smsChannel
     * @return
     */
    int updateSmsChannelAccount(SmsChannel smsChannel);

    /**
     * 关闭所有的启用的短信通道
     */
    void updateSmsChannelUnActive();
}