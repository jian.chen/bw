/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsTempTypeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.component.msg.repository.impl
 * Date:2015年8月19日下午1:11:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.component.msg.model.SmsTempType;
import com.bw.adv.module.sms.mapper.SmsTempTypeMapper;
import com.bw.adv.module.sms.repository.SmsTempTypeRepository;

import org.springframework.stereotype.Repository;

/**
 * ClassName:SmsTempTypeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午1:11:11 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SmsTempTypeRepositoryImpl extends BaseRepositoryImpl<SmsTempType, SmsTempTypeMapper> implements SmsTempTypeRepository {

	@Override
	protected Class<SmsTempTypeMapper> getMapperClass() {
		
		return SmsTempTypeMapper.class;
	}

}

