package com.bw.adv.module.sms.enums;

/**
 * Created by jeoy.zhou on 3/14/16.
 */
public enum SmsChannelEnum {
    ETONENET("S0000000001", "移通");

    private String smsCode;
    private String desc;

    private SmsChannelEnum(String smsCode, String desc) {
        this.smsCode = smsCode;
        this.desc = desc;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public String getDesc() {
        return desc;
    }
}
