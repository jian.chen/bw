/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsTempRepository.java
 * Package Name:com.sage.scrm.module.component.msg.repository
 * Date:2015年8月19日上午11:23:27
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsTemp;
import com.bw.adv.module.component.msg.model.exp.SmsTempExp;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.sms.mapper.SmsTempMapper;

import java.util.List;

/**
 * ClassName:SmsTempRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 上午11:23:27 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SmsTempRepository extends BaseRepository<SmsTemp, SmsTempMapper> {
	
	/**
	 * 
	 * findByStatusId:根据状态查询模板List
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 * Date: 2015年9月2日 下午3:28:03 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @return
	 */
	public List<SmsTemp> findByStatusId(Long statusId,Page<SmsTemp> page);
	
	/**
	 * 
	 * findExpByStatusId:根据状态查询模板List(扩展类)
	 * Date: 2015年9月2日 下午3:28:46 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @return
	 */
	public List<SmsTempExp> findExpByStatusId(Page<SmsTempExp> page);
	
	/**
	 * 
	 * findByGroupId:根据分组id查询会员
	 * Date: 2015年8月20日 下午3:12:16 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param groupId
	 * @return
	 */
	public List<Member> findByGroupId(Long groupId);
	
	/**
	 * 
	 * findAllMember:查询所有会员
	 * Date: 2015年9月2日 下午3:29:17 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<Member> findAllMember();
	
	/**
	 * 
	 * findByGroupStatusId:根据状态查询分组列表
	 * Date: 2015年8月20日 下午3:12:53 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param groupStatusId
	 * @return
	 */
	public List<MemberGroup> findGroupByStatusId(Long groupStatusId);
	
	/**
	 * 
	 * removeSmsTempList:批量修改短信模板状态
	 * Date: 2015年8月27日 上午10:18:22 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTempIds
	 * @return
	 */
	public int removeSmsTempList(Long statusId,List<Long> smsTempIds);
	
	/**
	 * 
	 * findExpById:根据id查询模板详情(扩展类)
	 * Date: 2015年9月2日 下午3:29:41 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param smsTempId
	 * @return
	 */
	public SmsTempExp findExpById(Long smsTempId);

	/**
	 * findSmsTempByTypeId:(根据模板类型查询模板列表). <br/>
	 * Date: 2015-9-3 下午3:54:18 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param smsTempTypeId
	 * @return
	 */
	public List<SmsTemp> findSmsTempByTypeId(Long smsTempTypeId);

	
	public List<SmsTemp> querySmsTempAll();
}

