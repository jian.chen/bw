/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsTempParamsRepositoryImpl.java
 * Package Name:com.sage.scrm.module.component.msg.repository.impl
 * Date:2015年8月19日下午2:37:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.component.msg.model.SmsTempParams;
import com.bw.adv.module.sms.mapper.SmsTempParamsMapper;
import com.bw.adv.module.sms.repository.SmsTempParamsRepository;

import org.springframework.stereotype.Repository;

/**
 * ClassName:SmsTempParamsRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午2:37:05 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SmsTempParamsRepositoryImpl extends BaseRepositoryImpl<SmsTempParams, SmsTempParamsMapper> implements SmsTempParamsRepository {

	@Override
	protected Class<SmsTempParamsMapper> getMapperClass() {
		
		return SmsTempParamsMapper.class;
	}

}

