package com.bw.adv.module.sms.service;

import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.SmsMsgLog;
import com.bw.adv.module.sms.model.SmsContent;
import com.bw.adv.module.sms.model.SmsResponse;
import com.bw.adv.module.sms.repository.SmsMsgLogRepository;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * Created by jeoy.zhou on 3/14/16.
 */
public abstract class AbstractSmsService implements SmsService {

    private static final Logger LOGGER = Logger.getLogger(AbstractSmsService.class);
    private static final String MOBILE_PREFIX = "86";
    private SmsMsgLogRepository smsMsgLogRepository;

    @Override
    public boolean sendTestMsg(SmsChannel channel, SmsContent smsContent) {
        SmsResponse smsResponse = null;
        try {
            smsResponse = sendTest(channel, smsContent);
        } catch (Exception e) {
            LOGGER.error(String.format("sendTestMsg error, by Channel[%s]", getSmsCode()));
        }
        return smsResponse != null && smsResponse.isPass();
    }

    @Override
    public boolean sendMsg(SmsChannel channel, SmsContent smsContent) {
        SmsResponse response = null;
        try {
            if (!smsContent.getMobile().startsWith(MOBILE_PREFIX)) {
                smsContent.setMobile(MOBILE_PREFIX + smsContent.getMobile());
            }
            response = send(channel, smsContent);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            response = new SmsResponse();
            response.setIsPass(false);
            response.setErrorMsg(e.getMessage().length() > 150 ? e.getMessage().substring(0, 150) : e.getMessage());
            return false;
        } finally {
            try {
                logConVerification(channel, smsContent, response);
            } catch (Exception e) {
                LOGGER.error("logConVerification error: " + e.getMessage());
            }
        }
        return true;
    }

    @Override
    public void batchSendMsg(SmsChannel channel, List<SmsContent> smsContents) {
        for (SmsContent smsContent : smsContents) {
            sendMsg(channel, smsContent);
        }
    }

    /**
     * 短信日志记录
     * @param channel
     * @param smsContent
     * @param smsResponse
     * @throws Exception
     */
    private void logConVerification(SmsChannel channel, SmsContent smsContent, SmsResponse smsResponse) throws Exception {
        SmsMsgLog smsMsgLog = new SmsMsgLog();
        smsMsgLog.setMobile(smsContent.getMobile());
        smsMsgLog.setComments(smsContent.getContent());
        smsMsgLog.setIsPass((smsResponse != null && smsResponse.isPass()) ? "Y" : "N");
        smsMsgLog.setRefId(smsResponse != null && StringUtils.isNotBlank(smsResponse.getMsgId()) ? smsResponse.getMsgId() : StringUtils.EMPTY);
        smsMsgLog.setRemark(StringUtils.isBlank(smsResponse.getErrorMsg()) ? StringUtils.EMPTY : smsResponse.getErrorMsg());
        smsMsgLogRepository.save(smsMsgLog);
    }

    protected abstract SmsResponse sendTest(SmsChannel channel, SmsContent smsContent) throws Exception;

    protected abstract SmsResponse send(SmsChannel channel, SmsContent smsContent) throws Exception;

    @Autowired
    public void setSmsMsgLogRepository(SmsMsgLogRepository smsMsgLogRepository) {
        this.smsMsgLogRepository = smsMsgLogRepository;
    }
}
