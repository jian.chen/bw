package com.bw.adv.module.sms.model;

import java.io.Serializable;

/**
 * Created by jeoy.zhou on 3/15/16.
 */
public class SmsResponse implements Serializable {

    private static final long serialVersionUID = -911787425817976185L;

    private String responseCode;
    private String responseMsg;
    private String msgId;
    private boolean isPass;
    private String errorMsg;

    public SmsResponse(){}

    public SmsResponse(String responseCode, String responseMsg, boolean isPass, String msgId) {
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
        this.isPass = isPass;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public boolean isPass() {
        return isPass;
    }

    public void setIsPass(boolean isPass) {
        this.isPass = isPass;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
