package com.bw.adv.module.sms.exception;

/**
 * Created by jeoy.zhou on 3/15/16.
 */
public class SmsException extends RuntimeException{

    private static final long serialVersionUID = -4642951228532066496L;

    public SmsException(String msg) {
        super(msg);
    }

    public SmsException(Throwable e) {
        super(e);
    }

    public SmsException(String msg, Throwable e) {
        super(msg, e);
    }
}
