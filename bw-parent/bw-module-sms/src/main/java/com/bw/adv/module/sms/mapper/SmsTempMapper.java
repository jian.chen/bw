package com.bw.adv.module.sms.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsTemp;
import com.bw.adv.module.component.msg.model.exp.SmsTempExp;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.model.Member;

public interface SmsTempMapper extends BaseMapper<SmsTemp> {
	
	public List<SmsTemp> selectByStatusId(@Param("statusId")Long statusId,@Param("page")Page<SmsTemp> page);
	
	public List<SmsTempExp> selectExpByStatusId(Page<SmsTempExp> page);
	
	public List<Member> selectMemberByGroupId(Long memberGroupId);
	
	public List<Member> selectAllMember();
	
	public List<MemberGroup> selectMemberGroupByStatusId(Long groupStatusId);
	
	public int updateStatusList(@Param("statusId")Long statusId, @Param("smsTempIds")List<Long> smsTempIds);
	
	public SmsTempExp selectExpById(Long smsTempId);

	/**
	 * selectBySmsTempByTypeId:(根据模板类型查询模板列表). <br/>
	 * Date: 2015-9-3 下午3:55:13 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param smsTempTypeId
	 * @return
	 */
	public List<SmsTemp> selectSmsTempByTypeId(Long smsTempTypeId);
	
	
	public List<SmsTemp> querySmsTempAll();

}