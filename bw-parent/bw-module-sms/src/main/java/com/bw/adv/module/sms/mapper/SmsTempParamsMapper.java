package com.bw.adv.module.sms.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.component.msg.model.SmsTempParams;

public interface SmsTempParamsMapper extends BaseMapper<SmsTempParams>{
	
}