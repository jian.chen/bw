package com.bw.adv.module.sms.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sys.model.ComVerification;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ComVerificationMapper extends BaseMapper<ComVerification>{

	List<ComVerification> selectComvByExample(@Param("example")Example example);
	

}