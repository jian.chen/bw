/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsTempRepositoryImpl.java
 * Package Name:com.sage.scrm.module.component.msg.repository.impl
 * Date:2015年8月19日上午11:23:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsTemp;
import com.bw.adv.module.component.msg.model.exp.SmsTempExp;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.sms.mapper.SmsTempMapper;
import com.bw.adv.module.sms.repository.SmsTempRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:SmsTempRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 上午11:23:36 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SmsTempRepositoryImpl extends BaseRepositoryImpl<SmsTemp, SmsTempMapper> implements SmsTempRepository {

	@Override
	protected Class<SmsTempMapper> getMapperClass() {
		return SmsTempMapper.class;
	}

	@Override
	public List<SmsTemp> findByStatusId(Long statusId,Page<SmsTemp> page) {
		
		return this.getMapper().selectByStatusId(statusId,page);
	}

	@Override
	public List<Member> findByGroupId(Long groupId) {
		
		return this.getMapper().selectMemberByGroupId(groupId);
	}

	@Override
	public List<MemberGroup> findGroupByStatusId(Long groupStatusId) {
		
		return this.getMapper().selectMemberGroupByStatusId(groupStatusId);
	}

	@Override
	public List<SmsTempExp> findExpByStatusId(Page<SmsTempExp> page) {
		return this.getMapper().selectExpByStatusId(page);
	}

	@Override
	public int removeSmsTempList(Long statusId, List<Long> smsTempIds) {
		
		return this.getMapper().updateStatusList(statusId,smsTempIds);
	}

	@Override
	public SmsTempExp findExpById(Long smsTempId) {
		
		return this.getMapper().selectExpById(smsTempId);
	}

	@Override
	public List<Member> findAllMember() {
		return this.getMapper().selectAllMember();
	}

	@Override
	public List<SmsTemp> findSmsTempByTypeId(Long smsTempTypeId) {
		return this.getMapper().selectSmsTempByTypeId(smsTempTypeId);
	}
	
	@Override
	public List<SmsTemp> querySmsTempAll(){
		return this.getMapper().querySmsTempAll();
	}

}

