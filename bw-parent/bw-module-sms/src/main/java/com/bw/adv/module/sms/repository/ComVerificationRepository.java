/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:ComVerificationRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015年11月10日下午8:23:20
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sms.mapper.ComVerificationMapper;
import com.bw.adv.module.sys.model.ComVerification;

import java.util.List;

/**
 * ClassName:ComVerificationRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月10日 下午8:23:20 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface ComVerificationRepository extends BaseRepository<ComVerification, ComVerificationMapper> {

	/**
	 * 
	 * findByExample:(根据查询条件查询验证码信息). <br/>
	 * Date: 2015年11月11日 上午11:38:09 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	public List<ComVerification> findByExample(Example example);
}

