package com.bw.adv.module.sms.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.component.msg.model.exp.SmsInstanceExp;

public interface SmsInstanceMapper extends BaseMapper<SmsInstance> {
	
	public List<SmsInstance> selectByMobile(String mobile);
	
	@Override
	public int insertBatch(List<SmsInstance> list);

	List<SmsInstance> selectByExample(@Param("example")Example example,Page<SmsInstance> page);
	
	List<SmsInstanceExp> selectExpByExample(@Param("example")Example example,Page<SmsInstanceExp> page);
	
}