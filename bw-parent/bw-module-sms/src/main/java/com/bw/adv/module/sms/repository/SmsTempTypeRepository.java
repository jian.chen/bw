/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsTempTypeRepository.java
 * Package Name:com.sage.scrm.module.component.msg.repository
 * Date:2015年8月19日下午1:11:03
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.component.msg.model.SmsTempType;
import com.bw.adv.module.sms.mapper.SmsTempTypeMapper;

/**
 * ClassName:SmsTempTypeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午1:11:03 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SmsTempTypeRepository extends BaseRepository<SmsTempType, SmsTempTypeMapper> {

}

