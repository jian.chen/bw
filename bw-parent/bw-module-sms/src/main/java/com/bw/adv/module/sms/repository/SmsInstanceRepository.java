/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsInstanceRepository.java
 * Package Name:com.sage.scrm.module.component.msg.repository
 * Date:2015年8月19日下午1:25:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.component.msg.model.exp.SmsInstanceExp;
import com.bw.adv.module.sms.mapper.SmsInstanceMapper;

import java.util.List;

/**
 * ClassName:SmsInstanceRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午1:25:11 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SmsInstanceRepository extends BaseRepository<SmsInstance, SmsInstanceMapper> {
	
	/**
	 * 
	 * findByMobile:根据手机号查询短信实例List
	 * Date: 2015年9月2日 下午3:30:35 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param mobile
	 * @return
	 */
	public List<SmsInstance> findByMobile(String mobile);
	
	/**
	 * 
	 * 批量保存短信实例
	 * @see com.bw.adv.core.repository.BaseRepository#saveList(java.util.List)
	 */
	@Override
	public int saveList(List<SmsInstance> arg0);

	/**
	 * 
	 * findByExample:根据example查询短信实例List
	 * Date: 2015年9月2日 下午3:32:41 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<SmsInstance> findByExample(Example example,Page<SmsInstance> page);
	
	/**
	 * 
	 * findExpByExample:根据example查询短信实例List(扩展类)
	 * Date: 2015年9月2日 下午3:33:04 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<SmsInstanceExp> findExpByExample(Example example,Page<SmsInstanceExp> page);
}

