/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsChannelRepositoryImpl.java
 * Package Name:com.sage.scrm.moudule.component.msg.repository.impl
 * Date:2015年8月17日下午2:28:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.mapper.SmsChannelMapper;
import com.bw.adv.module.sms.repository.SmsChannelRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:SmsChannelRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午2:28:33 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SmsChannelRepositoryImpl extends BaseRepositoryImpl<SmsChannel, SmsChannelMapper> implements SmsChannelRepository {

	@Override
	protected Class<SmsChannelMapper> getMapperClass() {
		return SmsChannelMapper.class;
	}

    /**
     * 查询所有的短信渠道
     * @return
     */
    @Override
    public List<SmsChannel> queryAllSmsChannel() {
        return this.getMapper().selectAllSmsChannel();
    }

    @Override
    public boolean updateSmsChannelAccount(SmsChannel smsChannel) {
        return this.getMapper().updateSmsChannelAccount(smsChannel) > 0;
    }

    @Override
    public void updateSmsChannelUnActive() {
        this.getMapper().updateSmsChannelUnActive();
    }

    @Override
    public SmsChannel getActiveSmsChannel() {
        List<SmsChannel> smsChannels = findByStatusInit("Y");
        if (smsChannels.size() > 0)
            return smsChannels.get(0);
        return null;
    }

	@Override
	public List<SmsChannel> findByStatus(String isActive,Page<SmsChannel> page) {
		return this.getMapper().selectByStatus(isActive,page);
	}

	@Override
	public List<SmsChannel> findByStatusInit(String isActive) {
		
		return this.getMapper().selectByStatusInit(isActive);
	}

	@Override
	public Long findCountByStatus(String isActive) {
		
		return this.getMapper().selectCountByStatus(isActive);
	}

	@Override
	public void updateIsActiveList(List<Long> smsChannelIds) {
		
		this.getMapper().updataIsActiveList(smsChannelIds);
	}

	@Override
	public List<SmsChannel> findAllName() {
		
		return this.getMapper().selectAllName();
	}

}

