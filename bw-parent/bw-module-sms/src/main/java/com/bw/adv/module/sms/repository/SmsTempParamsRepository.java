/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:SmsTempParamsRepository.java
 * Package Name:com.sage.scrm.module.component.msg.repository
 * Date:2015年8月19日下午2:36:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sms.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.component.msg.model.SmsTempParams;
import com.bw.adv.module.sms.mapper.SmsTempParamsMapper;

/**
 * ClassName:SmsTempParamsRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午2:36:55 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SmsTempParamsRepository extends BaseRepository<SmsTempParams, SmsTempParamsMapper> {

}

