package com.bw.adv.module.sms.service;

import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.exception.SmsException;
import com.bw.adv.module.sms.model.SmsContent;
import com.bw.adv.module.sms.repository.SmsChannelRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jeoy.zhou on 3/14/16.
 */
@Component("smsServiceManager")
public class SmsServiceManager {

    private List<SmsService> smsServices;
    private Map<String, SmsService> smsMaps;
    private SmsChannelRepository smsChannelRepository;

    /**
     * 发送测试短信
     * @param smsContent
     * @return
     */
    public boolean sendTestMsg(SmsChannel smsChannel, SmsContent smsContent) throws SmsException {
        return getSmsService(smsChannel.getSmsChannelCode()).sendTestMsg(smsChannel, smsContent);
    }

    /**
     * 发送短信
     * @param smsContent
     * @return
     */
    public boolean sendMsg(SmsContent smsContent) throws SmsException {
        SmsChannel smsChannel = smsChannelRepository.getActiveSmsChannel();
        return getSmsService(smsChannel.getSmsChannelCode()).sendMsg(smsChannel, smsContent);
    }

    /**
     * 批量发送短信
     * @param channel
     * @param smsContents
     */
    public void batchSendMsg(SmsChannel channel, List<SmsContent> smsContents) throws SmsException {
        SmsChannel smsChannel = smsChannelRepository.getActiveSmsChannel();
        getSmsService(smsChannel.getSmsChannelCode()).batchSendMsg(smsChannel, smsContents);
    }

    /**
     * 获取启用的短信渠道
     * @return
     */
    private SmsChannel getActiveSmsChannel() throws SmsException{
        SmsChannel smsChannel = this.smsChannelRepository.getActiveSmsChannel();
        if (smsChannel == null)
            throw new SmsException("no find active smsChannel");
        return  smsChannel;
    }

    /**
     * 获取对应的短信供应商处理类
     * @param key
     * @return
     */
    private SmsService getSmsService(String key) throws SmsException{
        SmsService smsService = smsMaps.get(key);
        if (smsService == null)
            throw new SmsException("no find smsChannel by channelCode:" + key);
        return smsService;
    }

    @PostConstruct
    private void init() {
        smsMaps = new HashMap<String, SmsService>();
        for (SmsService smsService : smsServices) {
            smsMaps.put(smsService.getSmsCode(), smsService);
        }
    }

    @Autowired
    public void setSmsServices(List<SmsService> smsServices) {
        this.smsServices = smsServices;
    }

    @Autowired
    public void setSmsChannelRepository(SmsChannelRepository smsChannelRepository) {
        this.smsChannelRepository = smsChannelRepository;
    }
}
