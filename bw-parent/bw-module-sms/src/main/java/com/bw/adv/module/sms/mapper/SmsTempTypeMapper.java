package com.bw.adv.module.sms.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.component.msg.model.SmsTempType;

public interface SmsTempTypeMapper extends BaseMapper<SmsTempType> {

}