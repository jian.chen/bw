package com.bw.adv.module.sms.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sms.SmsMsgLog;
import com.bw.adv.module.sms.mapper.SmsMsgLongMapper;

/**
 * Created by jeoy.zhou on 3/15/16.
 */
public interface SmsMsgLogRepository extends BaseRepository<SmsMsgLog, SmsMsgLongMapper> {

}
