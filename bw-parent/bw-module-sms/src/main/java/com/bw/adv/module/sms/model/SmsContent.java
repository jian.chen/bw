package com.bw.adv.module.sms.model;

import java.io.Serializable;

/**
 * Created by jeoy.zhou on 3/14/16.
 */
public class SmsContent implements Serializable{

    private static final long serialVersionUID = -2206264277882321810L;

    private String mobile;
    private String content;

    public SmsContent(){}

    public SmsContent(String mobile, String content) {
        this.mobile = mobile;
        this.content = content;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
