package com.bw.adv.module.sms.service;

import com.bw.adv.module.component.msg.model.SmsChannel;
import com.bw.adv.module.sms.model.SmsContent;

import java.util.List;

/**
 * Created by jeoy.zhou on 3/14/16.
 */
public interface SmsService {

    String getSmsCode();

    /**
     * 发送测试短信
     * @param channel
     * @param smsContent
     * @return
     */
    public boolean sendTestMsg(SmsChannel channel, SmsContent smsContent);

    /**
     * 发送短信
     * @param channel
     * @param smsContent
     * @return
     */
    public boolean sendMsg(SmsChannel channel, SmsContent smsContent);

    /**
     * 批量发送短信
     * @param channel
     * @param smsContents
     */
    public void batchSendMsg(SmsChannel channel, List<SmsContent> smsContents);
}
