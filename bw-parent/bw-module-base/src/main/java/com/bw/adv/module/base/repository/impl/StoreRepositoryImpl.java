/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:StoreRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年8月18日下午7:12:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.StoreMapper;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.model.exp.StoreExp;
import com.bw.adv.module.base.repository.StoreRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:StoreRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午7:12:26 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class StoreRepositoryImpl extends BaseRepositoryImpl<Store, StoreMapper> implements StoreRepository {

	@Override
	protected Class<StoreMapper> getMapperClass() {
		return StoreMapper.class;
	}

	@Override
	public List<Store> findByStatusId(Long statusId,Page<Store> page) {
		return this.getMapper().selectByStatusId(statusId,page);
	}

	@Override
	public int deleteStatusIdList(Long statusId, List<Long> storeIds) {
		return this.getMapper().updateStatusIdList(statusId,storeIds);
	}

	@Override
	public StoreExp findByPk(Long storeId) {
		return this.getMapper().selectByPrimaryKey(storeId);
	}

	@Override
	public Store findByStoreCode(String storeCode) {
		return this.getMapper().selectByStoreCode(storeCode);
	}

	@Override
	public Store queryByOrgId(Long orgId) {
		return this.getMapper().queryByOrgId(orgId);
	}

	@Override
	public List<Store> findStoreList() {
		return this.getMapper().findStoreList();
	}

}

