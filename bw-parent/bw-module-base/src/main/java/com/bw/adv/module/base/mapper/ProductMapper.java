package com.bw.adv.module.base.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.base.model.exp.ProductExp;
import com.bw.adv.module.common.model.Example;

public interface ProductMapper extends BaseMapper<Product>{
    int deleteByPrimaryKey(Long productId);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(@Param("statusId") Long statusId,@Param("productId") Long productId);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);
    
    List<ProductExp> selectProductList(@Param("statusId") Long statusId,Page<ProductExp> pag, @Param("map") Map<String,Object> map);
    
    List<Product> selectProductListByProductCode(@Param("statusId") Long statusId,@Param("productCode") String productCode);
    
    List<Product> selectByExample(@Param("example") Example example,Page<Product> pag);
    
    int deleteByExample(@Param("productIds")String ids,Long statusId);
    /**
     * 
     * selectByCode:(根据多个code查询产品(用于活动)). <br/>
     * Date: 2015年10月8日 下午8:28:12 <br/>
     * scrmVersion 1.0
     * @author chuanxue.wei
     * @version jdk1.7
     * @param statusId
     * @param productCodes
     * @return
     */
    List<Product> selectByCode(@Param("statusId") Long statusId,@Param("productCodes") String productCodes);
    
    /**
     * selectAllActive:(查询所有有效的产品). <br/>
     * Date: 2015-12-15 下午9:54:39 <br/>
     * scrmVersion 1.0
     * @author mennan
     * @version jdk1.7
     * @return
     */
    List<Product> selectAllActive();
    
    /**
     * selectByCodeList:(根据编号查询产品). <br/>
     * Date: 2016-1-8 下午2:33:42 <br/>
     * scrmVersion 1.0
     * @author mennan
     * @version jdk1.7
     * @return
     */
    List<Product> selectByCodeList(List<String> codeList);
    
}









