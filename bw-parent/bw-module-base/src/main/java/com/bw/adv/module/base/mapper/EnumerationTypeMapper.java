package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.EnumerationType;

public interface EnumerationTypeMapper {
    int deleteByPrimaryKey(Long enumTypeId);

    int insert(EnumerationType record);

    int insertSelective(EnumerationType record);

    EnumerationType selectByPrimaryKey(Long enumTypeId);

    int updateByPrimaryKeySelective(EnumerationType record);

    int updateByPrimaryKey(EnumerationType record);
}