/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:MealPeriodRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2016年5月23日下午5:42:23
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.MealPeriodMapper;
import com.bw.adv.module.base.model.MealPeriod;
/**
 * ClassName:MealPeriodRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月23日 下午5:42:23 <br/>
 * scrmVersion 1.0
 * @author   zhangqi
 * @version  jdk1.7
 * @see 	 
 */
public interface MealPeriodRepository extends BaseRepository<MealPeriod, MealPeriodMapper> {
	
	/**
	 * 
	 * findByStatusId:根据状态查询时段list. <br/>
	 * Date: 2016年5月23日 下午5:45:34 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @return
	 */
	public List<MealPeriod> findByStatusId(Long statusId,Page<MealPeriod> page);
	
	/**
	 * 
	 * selectByPrimaryKey:按时段id查询时段信息. <br/>
	 * Date: 2016年5月24日 下午1:50:15 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodId
	 * @return
	 */
	public MealPeriod selectByPrimaryKey(Long mealPeriodId);
	
	/**
	 * 
	 * deleteStatusIdList:批量删除时段. <br/>
	 * Date: 2016年5月24日 下午3:18:09 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param statusId
	 * @param mealPeriodIdList
	 * @return
	 */
	public int deleteStatusIdList(List<Long> mealPeriodIdList);
	
	/**
	 * 
	 * selectByMealPeriodNameById:查询时段名称是否相同(排除自身的时段名称). <br/>
	 * Date: 2016年5月24日 下午4:01:48 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodName
	 * @param mealPeriodId
	 * @return
	 */
	public Long selectByMealPeriodNameById(String mealPeriodName,Long mealPeriodId);
}

