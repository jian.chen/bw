package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.OrgRelType;

public interface OrgRelTypeMapper {
    int deleteByPrimaryKey(Long orgRelTypeId);

    int insert(OrgRelType record);

    int insertSelective(OrgRelType record);

    OrgRelType selectByPrimaryKey(Long orgRelTypeId);

    int updateByPrimaryKeySelective(OrgRelType record);

    int updateByPrimaryKey(OrgRelType record);
}