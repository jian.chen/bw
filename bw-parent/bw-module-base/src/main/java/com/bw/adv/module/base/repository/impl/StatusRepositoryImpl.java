/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:OrgRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年8月18日下午8:37:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;


import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.mapper.StatusMapper;
import com.bw.adv.module.base.model.Status;
import com.bw.adv.module.base.repository.StatusRepository;

/**
 * ClassName:OrgRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午8:37:30 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class StatusRepositoryImpl extends BaseRepositoryImpl<Status, StatusMapper> implements StatusRepository {

	@Override
	protected Class<StatusMapper> getMapperClass() {
		
		return StatusMapper.class;
	}

	
}

