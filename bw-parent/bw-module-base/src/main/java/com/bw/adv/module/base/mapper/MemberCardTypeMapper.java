package com.bw.adv.module.base.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.MemberCardType;

public interface MemberCardTypeMapper extends BaseMapper<MemberCardType>{
    int deleteByPrimaryKey(Long memberCardTypeId);

    int insert(MemberCardType record);

    int insertSelective(MemberCardType record);

    MemberCardType selectByPrimaryKey(Long memberCardTypeId);

    int updateByPrimaryKeySelective(MemberCardType record);

    int updateByPrimaryKey(MemberCardType record);
    
    List<MemberCardType> selectMemberCardTypeList();
}