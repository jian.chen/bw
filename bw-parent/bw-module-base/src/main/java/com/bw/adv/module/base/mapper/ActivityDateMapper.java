package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.ActivityDate;

public interface ActivityDateMapper extends BaseMapper<ActivityDate> {
	
	public List<ActivityDate> selectByIsActive(@Param("isActive")String isActive,Page<ActivityDate> page);
	
	/**
	 * selectByIsActive:(根据查询日期列表). <br/>
	 * Date: 2015-9-3 下午3:02:05 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param isActive：是否启用：N，Y
	 * @return
	 */
	public List<ActivityDate> selectByIsActive(@Param("isActive")String isActive);
	
	public int updateIsActiveList(@Param("isActive")String isActive,@Param("activityDateIds")List<Long> activityDateIds);

}