package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ProductCategoryMapper;
import com.bw.adv.module.base.model.ProductCategory;
import com.bw.adv.module.base.repository.ProductCategoryRepository;


/**
 * ClassName: ProductCategoryRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:16:05 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Repository
public class ProductCategoryRepositoryImpl extends BaseRepositoryImpl<ProductCategory, ProductCategoryMapper> implements ProductCategoryRepository{

	@Override
	protected Class<ProductCategoryMapper> getMapperClass() {
		return ProductCategoryMapper.class;
	}
	
	@Override
	public List<ProductCategory> findProductCategoryPage(Page<ProductCategory> page) {
		return this.getMapper().selectProductCategoryPage(page);
	}

	@Override
	public List<ProductCategory> findProductCategoryIsMultiple(ProductCategory productCategory) {
		return this.getMapper().findProductCategoryIsMultiple(productCategory);
	}
	
}