package com.bw.adv.module.member.card.mapper;

import com.bw.adv.module.member.model.MemberCardType;

public interface MemberCardTypeMapper {
    int deleteByPrimaryKey(Long memberCardTypeId);

    int insert(MemberCardType record);

    int insertSelective(MemberCardType record);

    MemberCardType selectByPrimaryKey(Long memberCardTypeId);

    int updateByPrimaryKeySelective(MemberCardType record);

    int updateByPrimaryKey(MemberCardType record);
}