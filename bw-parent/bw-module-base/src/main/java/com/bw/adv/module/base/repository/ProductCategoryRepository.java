package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ProductCategoryMapper;
import com.bw.adv.module.base.model.ProductCategory;


/**
 * ClassName: ProductCategoryRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:16:05 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface ProductCategoryRepository extends BaseRepository<ProductCategory, ProductCategoryMapper> {
	
	List<ProductCategory> findProductCategoryPage(Page<ProductCategory> page);
	
	/**
	 * findProductCategoryIsMultiple:(查询产品分类和名称是否重复). <br/>
	 * @author chengdi.cui
	 * @param productCategory
	 * @return
	 */
	public List<ProductCategory> findProductCategoryIsMultiple(ProductCategory productCategory);
	
}