/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:OrgRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年8月18日下午8:36:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;







import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.base.mapper.OrgMapper;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.exp.OrgExp;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName:OrgRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午8:36:18 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface OrgRepository extends BaseRepository<Org, OrgMapper> {
	
	public List<Org> findOrgList(String useFlag);

	public List<Org> findParentOrgList(Long upOrgId);
	
	public List<Org> findChildOrgList(Long orgId);

	public void updateStoreOrgIdByExample(Example example);
	
	public void deleteOrg(String stopDate,Example example);
	
	public OrgExp findOrgById(Long orgId);
	
	public List<Org> findOrgByName(String useFlag,String orgName,Long pareOrgId);
	
	/**
	 * 
	 * findListForTree:查询所有启用状态的区域
	 * Date: 2015年9月25日 下午2:50:20 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param userFlag
	 * @return
	 */
	public List<Org> findListForTree(String userFlag);
	
	public Org queryOrg(Long orgId);
}

