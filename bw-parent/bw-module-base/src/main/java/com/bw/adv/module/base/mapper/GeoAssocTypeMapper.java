package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.GeoAssocType;

public interface GeoAssocTypeMapper {
    int deleteByPrimaryKey(Long geoAssocTypeId);

    int insert(GeoAssocType record);

    int insertSelective(GeoAssocType record);

    GeoAssocType selectByPrimaryKey(Long geoAssocTypeId);

    int updateByPrimaryKeySelective(GeoAssocType record);

    int updateByPrimaryKey(GeoAssocType record);
}