/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:MealPeriodMapper.java
 * Package Name:com.sage.scrm.module.base.mapper
 * Date:2016年5月23日下午5:14:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.MealPeriod;

/**
 * ClassName:MealPeriodMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月23日 下午5:14:04 <br/>
 * scrmVersion 1.0
 * @author   zhangqi
 * @version  jdk1.7
 * @see 	 
 */
public interface MealPeriodMapper extends BaseMapper<MealPeriod>{
	
	/**
	 * 
	 * selectByStatusId:按状态Id查询所有的时段. <br/>
	 * Date: 2016年5月23日 下午5:15:24 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @return
	 */
	public List<MealPeriod> selectByStatusId(@Param("statusId")Long statusId,Page<MealPeriod> page);
	
	/**
	 * 
	 * updateStatusIdList:批量删除时段. <br/>
	 * Date: 2016年5月24日 下午3:20:49 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param statusId
	 * @param mealPeriodIds
	 * @return
	 */
	public int deleteStatusIdList(@Param("mealPeriodIds")List<Long> mealPeriodIds);
	
	/**
	 * 
	 * selectByMealPeriodNameById:查询时段名称是否相同(排除自身的时段名称). <br/>
	 * Date: 2016年5月24日 下午4:03:09 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param mealPeriodName
	 * @param mealPeriodId
	 * @return
	 */
	public Long selectByMealPeriodNameById(@Param("mealPeriodName")String mealPeriodName,@Param("mealPeriodId")Long mealPeriodId);
}

