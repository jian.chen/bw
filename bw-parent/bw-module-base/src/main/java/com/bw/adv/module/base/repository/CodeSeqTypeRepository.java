/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:CodeSeqTypeRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2016年4月6日下午5:27:39
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.base.mapper.CodeSeqTypeMapper;
import com.bw.adv.module.base.model.CodeSeqType;

/**
 * ClassName:CodeSeqTypeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年4月6日 下午5:27:39 <br/>
 * scrmVersion 1.0
 * @author   chengdi.cui
 * @version  jdk1.7
 * @see 	 
 */
public interface CodeSeqTypeRepository extends BaseRepository<CodeSeqType, CodeSeqTypeMapper>{

	/**
     * queryByTypeCode:(按编码查询). <br/>
     * @author chengdi.cui
     * @return
     */
    public CodeSeqType queryByTypeCode(@Param("typeCode")String typeCode);
    
}

