/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:OrgRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年8月18日下午8:37:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.QrCodeMapper;
import com.bw.adv.module.base.model.QrCode;
import com.bw.adv.module.base.model.exp.QrCodeExp;
import com.bw.adv.module.base.repository.QrCodeRepository;


/**
 * ClassName: QrCodeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-11-3 下午3:28:00 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Repository
public class QrCodeRepositoryImpl extends BaseRepositoryImpl<QrCode, QrCodeMapper> implements QrCodeRepository {

	
	@Override
	protected Class<QrCodeMapper> getMapperClass() {
		return QrCodeMapper.class;
	}
	/**
	 * TODO 简单描述该方法的实现功能（可选）.
	 * @see com.bw.adv.module.base.repository.QrCodeRepository#findQrCodeList(java.lang.String, com.bw.adv.core.utils.Page)
	 */
	@Override
	public List<QrCode> findQrCodeList(String isActive,Page<QrCode> page) {
		return this.getMapper().selectQrCodeList(isActive, page);
	}
	@Override
	public QrCodeExp findQrCodeByPrimaryKey(Long qrCodeId) {
		return this.getMapper().selectByPrimaryKey(qrCodeId);
	}
	@Override
	public int removeQrCode(String qrCodeIds) {
		return this.getMapper().deleteQrCode(qrCodeIds);
	}
	@Override
	public QrCode findQrCodeByQrCode(String qrCodeCode) {
		return this.getMapper().selectQrCodeByQrCode(qrCodeCode);
	}

}

