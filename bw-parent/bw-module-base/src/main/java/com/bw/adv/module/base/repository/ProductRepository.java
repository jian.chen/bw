package com.bw.adv.module.base.repository;

import java.util.List;
import java.util.Map;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ProductMapper;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.base.model.exp.ProductExp;
import com.bw.adv.module.common.model.Example;


/**
 * ClassName: ProductRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:16:05 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface ProductRepository extends BaseRepository<Product, ProductMapper> {
	
	public List<ProductExp> findProductList(Long statusId,Page<ProductExp> page,Map<String,Object> map);
	
	/**
	 * 
	 * findByExample:根据条件查询产品
	 * Date: 2015年9月9日 下午4:29:13 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<Product> findByExample(Example example,Page<Product> page);
	
	/**
	 * findProductByPrimaryKey:指定产品明细. <br/>
	 * Date: 2015-9-19 下午2:45:42 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param statusId
	 * @param productCode
	 * @return
	 */
	public Product findProductByPrimaryKey(Long statusId,Long productId);
	
	public List<Product> findProductListByProductCode(Long statusId,String productCode);
	
	/**
	 * 
	 * findByCode:(根据多个code查询产品(用于活动)). <br/>
	 * Date: 2015年10月8日 下午8:29:34 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param statusId
	 * @param productCodes
	 * @return
	 */
	public List<Product> findByCode(Long statusId,String productCodes);
	
	/**
	 * findByCodeList:(根据编号查询产品). <br/>
	 * Date: 2016-1-8 下午2:34:54 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param codeList
	 * @return
	 */
	public List<Product> findByCodeList(List<String> codeList);
	
	public void removeProductList(String ids,Long statusId);
	
	/**
	 * findAllActive:(查询所有有效的产品). <br/>
	 * Date: 2015-12-15 下午9:56:13 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	public List<Product> findAllActive();
	
}