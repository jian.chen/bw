/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:GeoRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年8月18日下午3:46:08
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.mapper.GeoMapper;
import com.bw.adv.module.base.model.Geo;
import com.bw.adv.module.base.repository.GeoRepository;

/**
 * ClassName:GeoRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午3:46:08 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class GeoRepositoryImpl extends BaseRepositoryImpl<Geo, GeoMapper> implements GeoRepository {

	@Override
	protected Class<GeoMapper> getMapperClass() {
		
		return GeoMapper.class;
	}

	@Override
	public List<Geo> findByTypeId(Long geoTypeId) {
		
		return this.getMapper().selectByGeoTypeId(geoTypeId);
	}

	@Override
	public List<Geo> findByParentId(Long geoId) {
		
		return this.getMapper().selectByParentId(geoId);
	}

}

