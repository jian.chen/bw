/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:GeoAssocRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年8月18日下午3:56:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.mapper.GeoAssocMapper;
import com.bw.adv.module.base.model.GeoAssoc;
import com.bw.adv.module.base.repository.GeoAssocRepository;

/**
 * ClassName:GeoAssocRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午3:56:50 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class GeoAssocRepositoryImpl extends BaseRepositoryImpl<GeoAssoc, GeoAssocMapper> implements GeoAssocRepository {

	@Override
	protected Class<GeoAssocMapper> getMapperClass() {
		return GeoAssocMapper.class;
	}

	@Override
	public List<GeoAssoc> findByParentId(Long geoId) {
		
		return this.getMapper().selectByParentId(geoId);
	}

}

