package com.bw.adv.module.base.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.model.exp.StoreExp;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StoreMapper extends BaseMapper<Store>{
	
	public List<Store> selectByStatusId(@Param("statusId")Long statusId,Page<Store> page);
	
	public StoreExp selectByPrimaryKey(Long storeId);
	
	int updateStatusIdList(@Param("statusId")Long statusId, @Param("storeIds")List<Long> storeIds);

	
	/**
	 * selectByStoreCode:(根据门店编号查询门店信息). <br/>
	 * Date: 2015-9-27 下午3:49:53 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param storeCode
	 * @return
	 */
	public Store selectByStoreCode(@Param("storeCode") String storeCode);

	/**
	 * queryByOrgId:(根据orgid查询门店). <br/>
	 * @author chengdi.cui
	 * @param orgId
	 * @return
	 */
	public Store queryByOrgId(@Param("orgId")Long orgId);
	
	/**
	 * 查询门店列表
	 * @return
	 */
	public List<Store> findStoreList();
	
}