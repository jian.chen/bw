package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.StatusType;

public interface StatusTypeMapper {
    int deleteByPrimaryKey(Long statusTypeId);

    int insert(StatusType record);

    int insertSelective(StatusType record);

    StatusType selectByPrimaryKey(Long statusTypeId);

    int updateByPrimaryKeySelective(StatusType record);

    int updateByPrimaryKey(StatusType record);
}