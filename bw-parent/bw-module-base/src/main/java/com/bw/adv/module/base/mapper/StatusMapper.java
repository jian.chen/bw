package com.bw.adv.module.base.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.Status;

public interface StatusMapper extends BaseMapper<Status> {
    int deleteByPrimaryKey(Long statusId);

    int insert(Status record);

    int insertSelective(Status record);

    Status selectByPrimaryKey(Long statusId);

    int updateByPrimaryKeySelective(Status record);

    int updateByPrimaryKey(Status record);
}