/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:ChannelRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年11月27日下午2:11:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ChannelMapper;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.base.repository.ChannelRepository;

/**
 * ClassName:ChannelRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月27日 下午2:11:29 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ChannelRepositoryImpl extends BaseRepositoryImpl<Channel, ChannelMapper>
	implements ChannelRepository{
	
	@Override
	protected Class<ChannelMapper> getMapperClass() {
		
		return ChannelMapper.class;
	}

	@Override
	public List<Channel> findList() {
		
		return this.getMapper().selectList();
	}

	@Override
	public List<Channel> findListByChannelName(String channelName, Page<Channel> page) {
		return this.getMapper().selectListByChannelName(channelName, page);
	}

	@Override
	public Long findCountChannelByChannelName(String channelName,Long channelId) {
		return this.getMapper().selectCountChannelByChannelName(channelName,channelId);
	}

	@Override
	public Long findCountChannelByChannelCode(String channelCode,Long channelId) {
		return this.getMapper().selectCountChannelByChannelCode(channelCode,channelId);
	}

}

