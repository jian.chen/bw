
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ChannelCfgMapper;
import com.bw.adv.module.base.repository.ChannelCfgRepository;
import com.bw.adv.module.cfg.model.ChannelCfg;
import com.bw.adv.module.cfg.model.ChannelCfgExp;
import com.bw.adv.module.sys.model.SysUser;


/**
 * ClassName: ChannelCfgRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月14日 下午6:05:41 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
@Repository
public class ChannelCfgRepositoryImpl extends BaseRepositoryImpl<ChannelCfg, ChannelCfgMapper> implements ChannelCfgRepository{

	@Override
	protected Class<ChannelCfgMapper> getMapperClass() {
		return ChannelCfgMapper.class;
	}


	@Override
	public ChannelCfg queryChannelCfgByChannelId(Long channelId) {
		return this.getMapper().selectChannelCfgByChannelId(channelId);
	}

	@Override
	public List<ChannelCfgExp> queryChannelInstanceByChannelId(Long channelId, Page<ChannelCfgExp> activityPage) {
		return this.getMapper().selectChannelInstanceByChannelId(channelId, activityPage);
	}

	@Override
	public List<ChannelCfgExp> queryChannelCfgExpByChannelId(Long channelId) {
		return this.getMapper().selectChannelCfgExpByChannelId(channelId);
	}
	
}

