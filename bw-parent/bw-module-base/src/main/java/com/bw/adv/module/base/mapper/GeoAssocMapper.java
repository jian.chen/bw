package com.bw.adv.module.base.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.GeoAssoc;
import com.bw.adv.module.base.model.GeoAssocKey;

public interface GeoAssocMapper extends BaseMapper<GeoAssoc> {
	
	List<GeoAssoc> selectAll();
	
	List<GeoAssoc> selectByParentId(Long geoId);

}