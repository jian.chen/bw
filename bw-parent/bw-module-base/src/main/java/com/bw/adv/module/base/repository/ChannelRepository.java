/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:ChannelRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年11月27日下午2:10:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ChannelMapper;
import com.bw.adv.module.base.model.Channel;

/**
 * ClassName:ChannelRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月27日 下午2:10:04 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface ChannelRepository extends BaseRepository<Channel, ChannelMapper>{

	public List<Channel> findList();
	
	public List<Channel> findListByChannelName(String ChannelName,Page<Channel> page);
	
	public Long findCountChannelByChannelName(String channelName,Long channelId);
	
	public Long findCountChannelByChannelCode(String channelCode,Long channelId);
}

