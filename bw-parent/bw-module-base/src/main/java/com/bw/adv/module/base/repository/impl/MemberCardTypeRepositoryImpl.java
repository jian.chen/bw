/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:OrgRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年8月18日下午8:37:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.mapper.MemberCardTypeMapper;
import com.bw.adv.module.base.repository.MemberCardTypeRepository;
import com.bw.adv.module.member.model.MemberCardType;

/**
 * ClassName:OrgRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午8:37:30 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberCardTypeRepositoryImpl extends BaseRepositoryImpl<MemberCardType, MemberCardTypeMapper> implements MemberCardTypeRepository {

	@Override
	protected Class<MemberCardTypeMapper> getMapperClass() {
		return MemberCardTypeMapper.class;
	}

	@Override
	public List<MemberCardType> findMemberCardTypeList() {
		return this.getMapper().selectMemberCardTypeList();
	}


	
}

