
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.base.mapper.ChannelCfgValueMapper;
import com.bw.adv.module.cfg.model.ChannelCfgValue;
import com.bw.adv.module.cfg.model.ChannelCfgValueExp;


/**
 * ClassName: ChannelCfgRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月14日 下午6:05:41 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
public interface ChannelCfgValueRepository extends BaseRepository<ChannelCfgValue, ChannelCfgValueMapper>{
	
	public int removeCoditionByChannelCfgId(Long channelCfgId);
	
	public List<ChannelCfgValueExp> queryConditionByChannelCfgId(Long channelCfgId);
}

