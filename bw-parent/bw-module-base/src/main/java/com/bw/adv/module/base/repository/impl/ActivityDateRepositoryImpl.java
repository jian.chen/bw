/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:ActivityDateRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年8月27日下午7:10:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.service.impl.BaseServiceImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ActivityDateMapper;
import com.bw.adv.module.base.model.ActivityDate;
import com.bw.adv.module.base.repository.ActivityDateRepository;

/**
 * ClassName:ActivityDateRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月27日 下午7:10:19 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ActivityDateRepositoryImpl extends BaseRepositoryImpl<ActivityDate, ActivityDateMapper> implements ActivityDateRepository {

	@Override
	protected Class<ActivityDateMapper> getMapperClass() {
		
		return ActivityDateMapper.class;
	}

	@Override
	public List<ActivityDate> findByIsActive(String isActive,
			Page<ActivityDate> page) {
		return this.getMapper().selectByIsActive(isActive,page);
	}
	@Override
	
	public List<ActivityDate> findByIsActive(String isActive) {
		return this.getMapper().selectByIsActive(isActive);
	}

	@Override
	public int updateIsActiveList(String isActive,List<Long> activityDateIds) {
		
		return this.getMapper().updateIsActiveList(isActive,activityDateIds);
	}
	
	

}

