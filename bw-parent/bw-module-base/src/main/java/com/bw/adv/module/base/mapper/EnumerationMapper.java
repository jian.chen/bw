package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.Enumeration;

public interface EnumerationMapper {
    int deleteByPrimaryKey(Long enumId);

    int insert(Enumeration record);

    int insertSelective(Enumeration record);

    Enumeration selectByPrimaryKey(Long enumId);

    int updateByPrimaryKeySelective(Enumeration record);

    int updateByPrimaryKey(Enumeration record);
}