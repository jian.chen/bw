
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ChannelInstanceRecordMapper;
import com.bw.adv.module.cfg.model.ChannelInstanceRecord;
import com.bw.adv.module.cfg.model.ChannelInstanceRecordDto;


/**
 * ClassName: 配置记录 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月28日 下午4:27:33 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
public interface ChannelInstanceRecordRepository extends BaseRepository<ChannelInstanceRecord, ChannelInstanceRecordMapper>{
	public List<ChannelInstanceRecordDto> findListByPages(Long channelCfgId,String recordMobile,Page<ChannelInstanceRecordDto> page);
}

