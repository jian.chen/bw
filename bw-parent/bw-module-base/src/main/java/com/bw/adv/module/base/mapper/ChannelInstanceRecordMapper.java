package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.cfg.model.ChannelInstanceRecord;
import com.bw.adv.module.cfg.model.ChannelInstanceRecordDto;

public interface ChannelInstanceRecordMapper extends BaseMapper<ChannelInstanceRecord> {
	public List<ChannelInstanceRecordDto> selectListByPages(@Param("channelCfgId")Long channelCfgId,@Param("recordMobile")String recordMobile,Page<ChannelInstanceRecordDto> page);
}