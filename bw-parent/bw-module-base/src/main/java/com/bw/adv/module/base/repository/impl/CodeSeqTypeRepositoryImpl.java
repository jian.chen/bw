/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:CodeSeqTypeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2016年4月6日下午5:27:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.mapper.CodeSeqTypeMapper;
import com.bw.adv.module.base.model.CodeSeqType;
import com.bw.adv.module.base.repository.CodeSeqTypeRepository;

/**
 * ClassName:CodeSeqTypeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年4月6日 下午5:27:56 <br/>
 * scrmVersion 1.0
 * @author   chengdi.cui
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class CodeSeqTypeRepositoryImpl extends BaseRepositoryImpl<CodeSeqType, CodeSeqTypeMapper> implements CodeSeqTypeRepository{

	@Override
	protected Class<CodeSeqTypeMapper> getMapperClass() {
		return CodeSeqTypeMapper.class;
	}

	@Override
	public CodeSeqType queryByTypeCode(String typeCode) {
		return this.getMapper().queryByTypeCode(typeCode);
	}

}

