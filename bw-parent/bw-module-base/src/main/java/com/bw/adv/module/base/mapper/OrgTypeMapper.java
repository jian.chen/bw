package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.OrgType;

public interface OrgTypeMapper {
    int deleteByPrimaryKey(Long orgTypeId);

    int insert(OrgType record);

    int insertSelective(OrgType record);

    OrgType selectByPrimaryKey(Long orgTypeId);

    int updateByPrimaryKeySelective(OrgType record);

    int updateByPrimaryKey(OrgType record);
}