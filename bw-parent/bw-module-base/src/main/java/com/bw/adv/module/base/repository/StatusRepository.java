/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:OrgRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年8月18日下午8:36:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.base.mapper.StatusMapper;
import com.bw.adv.module.base.model.Status;

/**
 * ClassName: StatusRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-20 下午4:34:03 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface StatusRepository extends BaseRepository<Status,  StatusMapper> {

}

