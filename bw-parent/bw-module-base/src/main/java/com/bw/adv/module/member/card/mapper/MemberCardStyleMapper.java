package com.bw.adv.module.member.card.mapper;

import com.bw.adv.module.member.model.MemberCardStyle;

public interface MemberCardStyleMapper {
    int deleteByPrimaryKey(Long memberCardStyleId);

    int insert(MemberCardStyle record);

    int insertSelective(MemberCardStyle record);

    MemberCardStyle selectByPrimaryKey(Long memberCardStyleId);

    int updateByPrimaryKeySelective(MemberCardStyle record);

    int updateByPrimaryKey(MemberCardStyle record);
}