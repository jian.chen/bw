package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.mapper.ProductCategoryItemMapper;
import com.bw.adv.module.base.model.ProductCategoryItem;
import com.bw.adv.module.base.repository.ProductCategoryItemRepository;


/**
 * ClassName: ProductCategoryItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:16:05 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Repository
public class ProductCategoryItemRepositoryImpl extends BaseRepositoryImpl<ProductCategoryItem, ProductCategoryItemMapper> implements ProductCategoryItemRepository{

	@Override
	protected Class<ProductCategoryItemMapper> getMapperClass() {
		return ProductCategoryItemMapper.class;
	}

	@Override
	public int removeAll() {
		return this.getMapper().deleteAll();
	}

	@Override
	public void removeByCategoryId(Long productCategoryId) {
		this.getMapper().removeByCategoryId(productCategoryId);
	}

	@Override
	public void removeByProductId(Long productId) {
		this.getMapper().removeByProductId(productId);
	}

	@Override
	public List<ProductCategoryItem> queryCategoryItemById(Long productId) {
		return this.getMapper().queryCategoryItemById(productId);
	}
	
}