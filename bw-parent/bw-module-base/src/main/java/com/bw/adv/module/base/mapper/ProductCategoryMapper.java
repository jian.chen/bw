package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.ProductCategory;

public interface ProductCategoryMapper extends BaseMapper<ProductCategory>{

	/**
	 * 
	 * selectProductCategoryPage:分页查询产品类别. <br/>
	 * Date: 2016年1月14日 下午12:17:54 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	List<ProductCategory> selectProductCategoryPage(Page<ProductCategory> page);
	
	/**
	 * findProductCategoryIsMultiple:(查询产品分类和名称是否重复). <br/>
	 * @author chengdi.cui
	 * @param productCategory
	 * @return
	 */
	List<ProductCategory> findProductCategoryIsMultiple(@Param("productCategory")ProductCategory productCategory);
	
}