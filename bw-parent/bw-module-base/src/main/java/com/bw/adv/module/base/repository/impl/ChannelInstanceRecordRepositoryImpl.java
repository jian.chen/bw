
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ChannelInstanceRecordMapper;
import com.bw.adv.module.base.repository.ChannelInstanceRecordRepository;
import com.bw.adv.module.cfg.model.ChannelInstanceRecord;
import com.bw.adv.module.cfg.model.ChannelInstanceRecordDto;


/**
 * ClassName: ChannelInstanceRecordRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月28日 下午4:28:17 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
@Repository
public class ChannelInstanceRecordRepositoryImpl extends BaseRepositoryImpl<ChannelInstanceRecord, ChannelInstanceRecordMapper> implements ChannelInstanceRecordRepository{

	@Override
	protected Class<ChannelInstanceRecordMapper> getMapperClass() {
		return ChannelInstanceRecordMapper.class;
	}

	@Override
	public List<ChannelInstanceRecordDto> findListByPages(Long channelCfgId,String recordMobile,Page<ChannelInstanceRecordDto> page) {
		return this.getMapper().selectListByPages(channelCfgId,recordMobile,page);
	}

	
}

