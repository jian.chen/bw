/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:OrgRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年8月18日下午8:36:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;


import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.QrCodeMapper;
import com.bw.adv.module.base.model.QrCode;
import com.bw.adv.module.base.model.exp.QrCodeExp;


/**
 * ClassName: QrCodeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-11-3 下午3:28:10 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface QrCodeRepository extends BaseRepository<QrCode, QrCodeMapper> {
	
	/**
	 * findQrCodeList:(查询二维码列表). <br/>
	 * Date: 2015-11-3 下午5:02:08 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param isActive
	 * @param page
	 * @return
	 */
	public List<QrCode> findQrCodeList(String isActive,Page<QrCode> page);
	
	/**
	 * findQrCodeByPrimaryKey:(查询二维码明细). <br/>
	 * Date: 2015-11-5 下午3:02:23 <br/>
	 * scrmVersion 1.0
	 * @author lulu.wang
	 * @version jdk1.7
	 * @param orCodeId
	 * @return
	 */
	public QrCodeExp findQrCodeByPrimaryKey(Long qrCodeId);
	
	public int removeQrCode(String qrCodeIds);

	/**
	 * 根据二维码编号，查询二维码信息
	 * @param qrCodeCode
	 * @return
	 */
	public QrCode findQrCodeByQrCode(String qrCodeCode);

}

