package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.Scene;

public interface SceneMapper {
    int deleteByPrimaryKey(Long sceneId);

    int insert(Scene record);

    int insertSelective(Scene record);

    Scene selectByPrimaryKey(Long sceneId);

    int updateByPrimaryKeySelective(Scene record);

    int updateByPrimaryKey(Scene record);
}