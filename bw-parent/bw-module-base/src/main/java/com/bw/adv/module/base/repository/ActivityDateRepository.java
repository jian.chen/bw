/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:ActivityDateRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年8月27日下午7:10:09
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ActivityDateMapper;
import com.bw.adv.module.base.model.ActivityDate;

/**
 * ClassName:ActivityDateRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月27日 下午7:10:09 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface ActivityDateRepository extends BaseRepository<ActivityDate, ActivityDateMapper> {
	
	/**
	 * 
	 * findByIsActive:根据日期状态查询日期list(启用Y/禁用N)
	 * Date: 2015年8月27日 下午8:05:15 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param isActive
	 * @param page
	 * @return
	 */
	public List<ActivityDate> findByIsActive(String isActive,Page<ActivityDate> page);
	
	/**
	 * findByIsActive:(查询日期列表). <br/>
	 * Date: 2015-9-3 下午3:00:42 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param isActive：是否有效:N,Y
	 * @return
	 */
	public List<ActivityDate> findByIsActive(String isActive);
	
	/**
	 * 
	 * updateIsActiveList:批量修改日期状态(启用Y/禁用N)
	 * Date: 2015年8月27日 下午8:04:54 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param isActive
	 * @param activityDateIds
	 * @return
	 */
	public int updateIsActiveList(String isActive,List<Long> activityDateIds);

}

