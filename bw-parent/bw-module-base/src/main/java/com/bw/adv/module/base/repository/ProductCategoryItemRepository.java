package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.base.mapper.ProductCategoryItemMapper;
import com.bw.adv.module.base.model.ProductCategoryItem;


/**
 * ClassName: ProductCategoryItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:16:05 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
public interface ProductCategoryItemRepository extends BaseRepository<ProductCategoryItem, ProductCategoryItemMapper> {
	
	/**
	 * removeAll:(删除所有类别明细). <br/>
	 * Date: 2015-12-30 下午4:16:12 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	int removeAll();
	
	/**
	 * removeByCategoryId:(根据产品类别id删除产品类别关系). <br/>
	 * @author chengdi.cui
	 * @param productCategoryId
	 */
	public void removeByCategoryId(Long productCategoryId);
	
	/**
	 * removeByProductId:(根据产品id删除产品类别关系). <br/>
	 * @author chengdi.cui
	 * @param productId
	 */
	public void removeByProductId(Long productId);
	
	/**
	 * 根据产品id查询产品所属种类明细
	 * @param productId
	 * @return
	 */
	public List<ProductCategoryItem> queryCategoryItemById(Long productId);
	
}