/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:MealPeriodRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2016年5月23日下午5:46:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.MealPeriodMapper;
import com.bw.adv.module.base.model.MealPeriod;
import com.bw.adv.module.base.repository.MealPeriodRepository;

/**
 * ClassName:MealPeriodRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月23日 下午5:46:51 <br/>
 * scrmVersion 1.0
 * @author   zhangqi
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MealPeriodRepositoryImpl extends BaseRepositoryImpl<MealPeriod, MealPeriodMapper> implements MealPeriodRepository {
	
	@Override
	protected Class<MealPeriodMapper> getMapperClass() {
		return MealPeriodMapper.class;
	}
	
	@Override
	public List<MealPeriod> findByStatusId(Long statusId, Page<MealPeriod> page) {
		return this.getMapper().selectByStatusId(statusId,page);
	}

	@Override
	public MealPeriod selectByPrimaryKey(Long mealPeriodId) {
		return this.getMapper().selectByPrimaryKey(mealPeriodId);
	}

	@Override
	public int deleteStatusIdList(List<Long> mealPeriodIdList) {
		return this.getMapper().deleteStatusIdList(mealPeriodIdList);
	}

	@Override
	public Long selectByMealPeriodNameById(String mealPeriodName, Long mealPeriodId) {
		return this.getMapper().selectByMealPeriodNameById(mealPeriodName, mealPeriodId);
	}
}

