package com.bw.adv.module.base.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.CodeSeq;

public interface CodeSeqMapper extends BaseMapper<CodeSeq> {
	
    int deleteByPrimaryKey(Long codeSeqId);

    int insert(CodeSeq record);

    int insertSelective(CodeSeq record);

    CodeSeq selectByPrimaryKey(Long codeSeqId);

    int updateByPrimaryKeySelective(CodeSeq record);

    int updateByPrimaryKey(CodeSeq record);
}