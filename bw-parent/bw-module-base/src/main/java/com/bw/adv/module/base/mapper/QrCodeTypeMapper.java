package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.QrCodeType;

public interface QrCodeTypeMapper {
    int deleteByPrimaryKey(Long qrCodeTypeId);

    int insert(QrCodeType record);

    int insertSelective(QrCodeType record);

    QrCodeType selectByPrimaryKey(Long qrCodeTypeId);

    int updateByPrimaryKeySelective(QrCodeType record);

    int updateByPrimaryKey(QrCodeType record);
}