package com.bw.adv.module.base.mapper;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.CodeSeqType;

public interface CodeSeqTypeMapper extends BaseMapper<CodeSeqType>{
	
    int deleteByPrimaryKey(Long codeSeqTypeId);

    int insert(CodeSeqType record);

    int insertSelective(CodeSeqType record);

    CodeSeqType selectByPrimaryKey(Long codeSeqTypeId);

    int updateByPrimaryKeySelective(CodeSeqType record);

    int updateByPrimaryKey(CodeSeqType record);
    
    /**
     * queryByTypeCode:(按编码查询). <br/>
     * @author chengdi.cui
     * @return
     */
    CodeSeqType queryByTypeCode(@Param("typeCode")String typeCode);
    
}