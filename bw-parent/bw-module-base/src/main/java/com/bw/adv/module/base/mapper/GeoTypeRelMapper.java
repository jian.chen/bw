package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.GeoTypeRel;
import com.bw.adv.module.base.model.GeoTypeRelKey;

public interface GeoTypeRelMapper {
    int deleteByPrimaryKey(GeoTypeRelKey key);

    int insert(GeoTypeRel record);

    int insertSelective(GeoTypeRel record);

    GeoTypeRel selectByPrimaryKey(GeoTypeRelKey key);

    int updateByPrimaryKeySelective(GeoTypeRel record);

    int updateByPrimaryKey(GeoTypeRel record);
}