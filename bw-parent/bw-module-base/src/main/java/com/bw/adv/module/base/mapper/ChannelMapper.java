package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.Channel;

public interface ChannelMapper extends BaseMapper<Channel> {
	
	public List<Channel> selectList();
	
	public List<Channel> selectListByChannelName(@Param("channelName") String channelName, Page<Channel> page);
	
	public Long selectCountChannelByChannelName(@Param("channelName") String channelName,@Param("channelId") Long channelId);
	
	public Long selectCountChannelByChannelCode(@Param("channelCode") String channelCode,@Param("channelId") Long channelId);

}