package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.cfg.model.ChannelCfg;
import com.bw.adv.module.cfg.model.ChannelCfgExp;

public interface ChannelCfgMapper extends BaseMapper<ChannelCfg> {
	
	public ChannelCfg selectChannelCfgByChannelId(@Param("channelId") Long channelId);
	
	/**
	 * 根据渠道加载所有配置信息:(说明). <br/>
	 * Date: 2016年11月25日 <br/>
	 * scrmVersion 11.2
	 * @author cjian
	 * @version jdk1.7
	 * @param channelId
	 * @param page
	 * @return
	 */
	public List<ChannelCfgExp> selectChannelInstanceByChannelId(@Param("channelId")Long channelId, Page<ChannelCfgExp> page);
	
	/**
	 * 注册之后查询渠道配置. <br/>
	 * Date: 2016年11月25日 <br/>
	 * scrmVersion 11.2
	 * @author cjian
	 * @version jdk1.7
	 * @param channelId
	 * @return
	 */
	public List<ChannelCfgExp> selectChannelCfgExpByChannelId(@Param("channelId") Long channelId);
}