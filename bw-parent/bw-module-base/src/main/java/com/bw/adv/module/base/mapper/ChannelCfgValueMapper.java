package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.cfg.model.ChannelCfgValue;
import com.bw.adv.module.cfg.model.ChannelCfgValueExp;

public interface ChannelCfgValueMapper extends BaseMapper<ChannelCfgValue> {
	
	public int deleteCoditionByChannelCfgId(@Param("channelCfgId") Long channelCfgId);
	
	public List<ChannelCfgValueExp> selectConditionByChannelCfgId(@Param("channelCfgId") Long channelCfgId);
}