/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:OrgRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年8月18日下午8:36:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.base.mapper.MemberCardTypeMapper;
import com.bw.adv.module.member.model.MemberCardType;

/**
 * ClassName:OrgRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午8:36:18 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberCardTypeRepository extends BaseRepository<MemberCardType, MemberCardTypeMapper> {
	
	public List<MemberCardType> findMemberCardTypeList();

}

