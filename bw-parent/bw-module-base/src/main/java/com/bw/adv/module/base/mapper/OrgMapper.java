package com.bw.adv.module.base.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.exp.OrgExp;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sys.model.SysOrg;

public interface OrgMapper  extends BaseMapper<Org>{
	
    int deleteByPrimaryKey(Long orgId);

    int insert(SysOrg record);

	int insertSelective(Org record);

	OrgExp selectByPrimaryKey(Long orgId);
    
    List<Org> selectOrgByName(@Param("useFlag") String useFlag,@Param("orgName") String orgName,@Param("pareOrgId") Long pareOrgId);

    int updateByPrimaryKeySelective(SysOrg record);

    int updateByPrimaryKey(SysOrg record);
    
    List<Org> selectOrgList(@Param("useFlag") String useFlag);
    
    List<Org> selectParentOrgList(Long upOrgId);

    void updateStoreOrgIdByExample(@Param("example") Example example);
    
    List<Org> selectChildOrgList(Long OrgId);
    
    void deleteByExample(@Param("stopDate") String stopDate,@Param("example") Example example);
    
    List<Org> selectForTree(String userFlag);
    
    Org queryOrg(Long orgId);
}