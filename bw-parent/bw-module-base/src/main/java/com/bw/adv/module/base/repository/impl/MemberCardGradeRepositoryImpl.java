/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:MemberCardGradeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年9月6日下午4:34:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;


import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.repository.MemberCardGradeRepository;
import com.bw.adv.module.member.card.mapper.MemberCardGradeMapper;
import com.bw.adv.module.member.model.MemberCardGrade;

/**
 * ClassName:MemberCardGradeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月6日 下午4:34:55 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberCardGradeRepositoryImpl extends BaseRepositoryImpl<MemberCardGrade, MemberCardGradeMapper> implements MemberCardGradeRepository {

	@Override
	protected Class<MemberCardGradeMapper> getMapperClass() {
		
		return MemberCardGradeMapper.class;
	}

	@Override
	public int updateSrcByPk(MemberCardGrade memberCardGrade) {
		
		return this.getMapper().updateSrcByPk(memberCardGrade);
	}


}

