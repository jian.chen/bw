package com.bw.adv.module.member.card.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.MemberCardGrade;

public interface MemberCardGradeMapper extends BaseMapper<MemberCardGrade> {
	

	int updateSrcByPk(MemberCardGrade memberCardGrade);

}