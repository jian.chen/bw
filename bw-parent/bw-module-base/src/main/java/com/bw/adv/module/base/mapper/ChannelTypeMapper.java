package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.ChannelType;

public interface ChannelTypeMapper {
    int deleteByPrimaryKey(Long channelTypeId);

    int insert(ChannelType record);

    int insertSelective(ChannelType record);

    ChannelType selectByPrimaryKey(Long channelTypeId);

    int updateByPrimaryKeySelective(ChannelType record);

    int updateByPrimaryKey(ChannelType record);
}