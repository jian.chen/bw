/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:OrgRepositoryImpl.java
 * Package Name:com.sage.scrm.module.base.repository.impl
 * Date:2015年8月18日下午8:37:30
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.mapper.OrgMapper;
import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.base.model.exp.OrgExp;
import com.bw.adv.module.base.repository.OrgRepository;
import com.bw.adv.module.common.model.Example;

/**
 * ClassName:OrgRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午8:37:30 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class OrgRepositoryImpl extends BaseRepositoryImpl<Org, OrgMapper> implements OrgRepository {

	@Override
	protected Class<OrgMapper> getMapperClass() {
		return OrgMapper.class;
	}

	@Override
	public List<Org> findOrgList(String useFlag) {
		return this.getMapper().selectOrgList(useFlag);
	}

	@Override
	public List<Org> findParentOrgList(Long upOrgId) {
		return this.getMapper().selectParentOrgList(upOrgId);
	}
	
	@Override
	public List<Org> findChildOrgList(Long orgId) {
		return this.getMapper().selectChildOrgList(orgId);
	}
	@Override
	public void updateStoreOrgIdByExample(Example example) {
		this.getMapper().updateStoreOrgIdByExample(example);
	}

	@Override
	public void deleteOrg(String stopDate,Example example) {
		this.getMapper().deleteByExample(stopDate,example);
	}

	@Override
	public List<Org> findOrgByName(String useFlag, String orgName,
			Long pareOrgId) {
		return this.getMapper().selectOrgByName(useFlag, orgName, pareOrgId);
	}

	@Override
	public List<Org> findListForTree(String userFlag) {
		
		return this.getMapper().selectForTree(userFlag);
	}

	@Override
	public OrgExp findOrgById(Long orgId) {
		return this.getMapper().selectByPrimaryKey(orgId);
	}

	@Override
	public Org queryOrg(Long orgId) {
		return this.getMapper().queryOrg(orgId);
	}
	
}

