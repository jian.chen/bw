package com.bw.adv.module.base.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.ProductMapper;
import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.base.model.exp.ProductExp;
import com.bw.adv.module.base.repository.ProductRepository;
import com.bw.adv.module.common.model.Example;



/**
 * ClassName: ProductRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午7:15:57 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Repository
public class ProductRepositoryImpl extends BaseRepositoryImpl<Product,ProductMapper> implements ProductRepository{

	@Override
	protected Class<ProductMapper> getMapperClass() {
		return ProductMapper.class;
	}

	@Override
	public List<ProductExp> findProductList(Long statusId,Page<ProductExp> page,Map<String,Object> map) {
		return this.getMapper().selectProductList(statusId,page,map);
	}

	@Override
	public void removeProductList(String ids,Long statusId) {
		this.getMapper().deleteByExample(ids,statusId);
		
	}

	@Override
	public List<Product> findByExample(Example example, Page<Product> page) {
		return this.getMapper().selectByExample(example, page);
	}

	@Override
	public List<Product> findProductListByProductCode(Long statusId,String productCode) {
		return this.getMapper().selectProductListByProductCode(statusId, productCode);
	}
	
	@Override
	public List<Product> findByCode(Long statusId,String productCodes) {
		return this.getMapper().selectByCode(statusId, productCodes);
	}

	@Override
	public Product findProductByPrimaryKey(Long statusId, Long productId) {
		return this.getMapper().selectByPrimaryKey(statusId, productId);
	}

	@Override
	public List<Product> findAllActive() {
		return this.getMapper().selectAllActive();
	}

	/**
	 * TODO 根据编号查询产品（可选）.
	 * @see com.bw.adv.module.base.repository.ProductRepository#findByCodeList(java.util.List)
	 */
	@Override
	public List<Product> findByCodeList(List<String> codeList) {
		if(codeList==null || codeList.size() == 0){
			return new ArrayList<Product>();
		}
		return this.getMapper().selectByCodeList(codeList);
	}


}