package com.bw.adv.module.base.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.Geo;

public interface GeoMapper extends BaseMapper<Geo> {
	
	List<Geo> selectByGeoTypeId(Long geoTypeId);
	
	List<Geo> selectByParentId(Long geoId);

}