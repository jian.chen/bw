package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.ProductCategoryItem;

public interface ProductCategoryItemMapper extends BaseMapper<ProductCategoryItem>{
	
	/**
	 * deleteAll:(删除所有类别明细). <br/>
	 * Date: 2015-12-30 下午4:14:39 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	int deleteAll();
	
	/**
	 * removeByCategoryId:(根据产品类别id删除产品类别关系). <br/>
	 * @author chengdi.cui
	 * @param productCategoryId
	 */
	public void removeByCategoryId(@Param("productCategoryId")Long productCategoryId);
	
	/**
	 * removeByProductId:(根据产品id删除产品类别关系). <br/>
	 * @author chengdi.cui
	 * @param productId
	 */
	public void removeByProductId(@Param("productId")Long productId);
	
	/**
	 * 根据产品id查询产品所属种类明细
	 * @param productId
	 * @return
	 */
	List<ProductCategoryItem> queryCategoryItemById(@Param("productId")Long productId);
	
}