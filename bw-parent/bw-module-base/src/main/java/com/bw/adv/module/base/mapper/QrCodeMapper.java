package com.bw.adv.module.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.model.QrCode;
import com.bw.adv.module.base.model.exp.QrCodeExp;

public interface QrCodeMapper extends BaseMapper<QrCode>{

    QrCodeExp selectByPrimaryKey(Long qrCodeId);

    /**
     * selectOrgList:(查询二维码列表). <br/>
     * Date: 2015-11-3 下午5:02:41 <br/>
     * scrmVersion 1.0
     * @author lulu.wang
     * @version jdk1.7
     * @param isActive
     * @param pag
     * @return
     */
    List<QrCode> selectQrCodeList(@Param("isActive") String isActive,Page<QrCode> pag);
    
    /**
     * deleteQrCode:(删除二维码). <br/>
     * Date: 2015-11-5 下午5:42:57 <br/>
     * scrmVersion 1.0
     * @author lulu.wang
     * @version jdk1.7
     * @param qrCodeId
     * @return
     */
    int deleteQrCode(@Param("qrCodeIds") String qrCodeIds);

	QrCode selectQrCodeByQrCode(String qrCodeCode);
    
}