/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:GeoAssocRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年8月18日下午3:56:10
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.base.mapper.GeoAssocMapper;
import com.bw.adv.module.base.model.GeoAssoc;

/**
 * ClassName:GeoAssocRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午3:56:10 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface GeoAssocRepository extends BaseRepository<GeoAssoc, GeoAssocMapper> {
	
	//public List<GeoAssoc> findAll();
	
	public List<GeoAssoc> findByParentId(Long geoId);

}

