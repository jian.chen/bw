
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.mapper.ChannelCfgValueMapper;
import com.bw.adv.module.base.repository.ChannelCfgValueRepository;
import com.bw.adv.module.cfg.model.ChannelCfgValue;
import com.bw.adv.module.cfg.model.ChannelCfgValueExp;


/**
 * ClassName: ChannelCfgRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月14日 下午6:05:41 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
@Repository
public class ChannelCfgValueRepositoryImpl extends BaseRepositoryImpl<ChannelCfgValue, ChannelCfgValueMapper> implements ChannelCfgValueRepository{

	@Override
	protected Class<ChannelCfgValueMapper> getMapperClass() {
		return ChannelCfgValueMapper.class;
	}

	@Override
	public int removeCoditionByChannelCfgId(Long channelCfgId) {
		return this.getMapper().deleteCoditionByChannelCfgId(channelCfgId);
	}

	@Override
	public List<ChannelCfgValueExp> queryConditionByChannelCfgId(Long channelCfgId) {
		return this.getMapper().selectConditionByChannelCfgId(channelCfgId);
	}
	
	
	
}

