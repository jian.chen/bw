package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.DataType;

public interface DataTypeMapper {
    int deleteByPrimaryKey(Long dataTypeId);

    int insert(DataType record);

    int insertSelective(DataType record);

    DataType selectByPrimaryKey(Long dataTypeId);

    int updateByPrimaryKeySelective(DataType record);

    int updateByPrimaryKey(DataType record);
}