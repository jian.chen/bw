/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:StoreRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年8月18日下午7:11:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.base.mapper.StoreMapper;
import com.bw.adv.module.base.model.Store;
import com.bw.adv.module.base.model.exp.StoreExp;

import java.util.List;

/**
 * ClassName:StoreRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午7:11:18 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface StoreRepository extends BaseRepository<Store, StoreMapper> {
	
	/**
	 * 
	 * findByStatusId:根据状态查询门店list
	 * Date: 2015年9月2日 下午3:18:45 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @return
	 */
	public List<Store> findByStatusId(Long statusId,Page<Store> page);
	
	/**
	 * 
	 * deleteStatusIdList:批量修改门店状态
	 * Date: 2015年9月2日 下午3:19:10 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param statusId
	 * @param storeIds
	 * @return
	 */
	public int deleteStatusIdList(Long statusId,List<Long> storeIds);
	
	/**
	 * 
	 * findByPk:根据id查询门店(扩展类)详情
	 * Date: 2015年9月3日 下午3:53:15 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param storeId
	 * @return
	 */
	public StoreExp findByPk(Long storeId);
	
	
	/**
	 * findByStoreCode:(根据门店编号查询门店信息). <br/>
	 * Date: 2015-9-27 下午3:48:47 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param storeCode
	 * @return
	 */
	public Store findByStoreCode(String storeCode);


	/**
	 * queryByOrgId:(根据orgid查询门店). <br/>
	 * @author chengdi.cui
	 * @param orgId
	 * @return
	 */
	public Store queryByOrgId(Long orgId);
	
	/**
	 * 查询门店列表
	 * @return
	 */
	public List<Store> findStoreList();
}

