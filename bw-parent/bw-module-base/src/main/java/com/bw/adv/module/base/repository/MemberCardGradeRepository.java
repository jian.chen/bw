/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-base
 * File Name:MemberCardGradeRepository.java
 * Package Name:com.sage.scrm.module.base.repository
 * Date:2015年9月6日下午4:32:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.card.mapper.MemberCardGradeMapper;
import com.bw.adv.module.member.model.MemberCardGrade;

/**
 * ClassName:MemberCardGradeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月6日 下午4:32:26 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberCardGradeRepository extends BaseRepository<MemberCardGrade, MemberCardGradeMapper> {

	/**
	 * 
	 * updateSrcByPk:根据ID修改src和styleId两个字段
	 * Date: 2015年9月7日 上午11:14:07 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberCardGrade
	 * @return
	 */
	public int updateSrcByPk(MemberCardGrade memberCardGrade);
	
}

