package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.GeoType;

public interface GeoTypeMapper {
    int deleteByPrimaryKey(Long geoTypeId);

    int insert(GeoType record);

    int insertSelective(GeoType record);

    GeoType selectByPrimaryKey(Long geoTypeId);

    int updateByPrimaryKeySelective(GeoType record);

    int updateByPrimaryKey(GeoType record);
}