package com.bw.adv.module.base.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.ProductSeries;

public interface ProductSeriesMapper extends BaseMapper<ProductSeries>{
	
}