package com.bw.adv.module.base.mapper;

import com.bw.adv.module.base.model.OrgRel;

public interface OrgRelMapper {
    int deleteByPrimaryKey(Long orgRelId);

    int insert(OrgRel record);

    int insertSelective(OrgRel record);

    OrgRel selectByPrimaryKey(Long orgRelId);

    int updateByPrimaryKeySelective(OrgRel record);

    int updateByPrimaryKey(OrgRel record);
}