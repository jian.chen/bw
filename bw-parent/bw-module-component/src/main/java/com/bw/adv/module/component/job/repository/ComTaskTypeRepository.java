package com.bw.adv.module.component.job.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.component.job.mapper.ComTaskTypeMapper;
import com.bw.adv.module.component.job.model.ComTaskType;


/**
 * ClassName: ComTaskTypeRepositoryService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-3 下午8:21:56 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface ComTaskTypeRepository extends BaseRepository<ComTaskType, ComTaskTypeMapper> {

}
