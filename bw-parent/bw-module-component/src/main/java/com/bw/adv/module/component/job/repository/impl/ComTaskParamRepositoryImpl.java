package com.bw.adv.module.component.job.repository.impl;


import java.util.List;

import org.springframework.stereotype.Service;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.component.job.model.ComTaskParam;
import com.bw.adv.module.component.job.repository.ComTaskParamRepository;

@Service
public class ComTaskParamRepositoryImpl extends BaseRepositoryImpl<ComTaskParam, Long> implements ComTaskParamRepository{

	@Override
	public List<ComTaskParam> findByTask(Long taskId) {
		return null;
	}

	@Override
	public List<ComTaskParam> findComTaskParamByTaskIdAndTaskParamType(Long taskId, String taskParamType) {
		return null;
	}

	@Override
	protected Class<Long> getMapperClass() {
		return null;
	}

}
