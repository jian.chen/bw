package com.bw.adv.module.component.rule.mapper;

import com.bw.adv.module.component.rule.model.BusinessRule;

public interface BusinessRuleMapper {
    int deleteByPrimaryKey(Long businessRuleId);

    int insert(BusinessRule record);

    int insertSelective(BusinessRule record);

    BusinessRule selectByPrimaryKey(Long businessRuleId);

    int updateByPrimaryKeySelective(BusinessRule record);

    int updateByPrimaryKey(BusinessRule record);
}