package com.bw.adv.module.component.rule.result;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bw.adv.module.coupon.model.exp.CouponResult;
import com.bw.adv.module.member.tag.model.MemberTagItem;
import com.bw.adv.module.points.model.MemberPointsItem;

/**
 * ClassName:活动规则结果 <br/>
 * Date:     2014-3-25 下午4:34:31 <br/>
 * @author   menn
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
/**
 * ClassName: ActivityRuleResult <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-23 上午11:38:05 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class ActivityRuleResult extends BaseRuleResult{
	
	private List<MemberPointsItem> memberPointsItemList = new ArrayList<MemberPointsItem>();//积分结果
	private Map<Long, Integer> couponQuantityMap = new HashMap<Long, Integer>();//优惠券结果
	private List<CouponResult> couponList = new ArrayList<CouponResult>();//优惠券结果
	private List<Long> activityInstainceIdList = new ArrayList<Long>();//活动实例结果
	private MemberTagItem memberTagItemResult = new MemberTagItem();//会员标签结果
	private List<Long> tmpInstanceList = new ArrayList<Long>();
	private Long orderId;
	private String externalId;

	private Object result;
	
	private String activityCode;
	
	private String activityInstanceId;
	
	private String isPerfectMember;//是否完善会员
	
	/**
	 * addPointsItem:新增积分记录<br/>
	 * @author menn
	 * Date: 2014-5-22 下午3:28:40
	 * @param memberPointsItem
	 * @since JDK 1.6
	 */
//	public void addPointsItem(MemberPointsItem  memberPointsItem){
//		memberPointsItemList.add(memberPointsItem);
//	}
	
	/**
	 * addPointsItem:(添加积分记录). <br/>
	 * Date: 2015-9-8 下午8:56:26 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param activityInstanceId
	 * @param pointsRuleId
	 * @param message
	 */
	public void addPointsItem(Long memberId, Long activityInstanceId, Long pointsRuleId,String happenTime,String message){
		MemberPointsItem item = new MemberPointsItem();
		item.setMemberId(memberId);
		item.setActivityInstanceId(activityInstanceId);
		item.setPointsRuleId(pointsRuleId);
		item.setPointsReason(message);
		item.setHappenTime(happenTime);
		memberPointsItemList.add(item);
	}
	
	/**
	 * addPointsItem:(添加积分记录). <br/>
	 * Date: 2015-9-18 下午8:31:56 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param activityInstanceId
	 * @param pointsRuleId
	 * @param happenTime
	 * @param message
	 * @param orderAmount
	 */
	public void addPointsItem(Long memberId, Long activityInstanceId, Long pointsTypeId,String happenTime,String invalidDate,String message,BigDecimal orderAmount,Long orderId,String externalId){
		MemberPointsItem item = new MemberPointsItem();
		item.setMemberId(memberId);
		item.setActivityInstanceId(activityInstanceId);
//		item.setPointsRuleId(pointsRuleId);
		item.setPointsReason(message);
		item.setHappenTime(happenTime);
		item.setItemPointsNumber(orderAmount);
		item.setInvalidDate(invalidDate);
		item.setOrderId(orderId);
		item.setExternalId(externalId);
		item.setPointsTypeId(pointsTypeId);
		memberPointsItemList.add(item);
	}
	
	/**
	 * 添加优惠券记录
	 * @param couponId
	 * @param quantity
	 * @param activityInstanceId
	 */
	public void addCoupon(Long couponId,Integer quantity,Long activityInstanceId,Long orderId,String externalId){
		CouponResult result = new CouponResult();
		result.setCouponId(couponId);
		result.setQuantity(quantity);
		result.setActivityInstanceId(activityInstanceId);
		result.setOrderId(orderId);
		result.setExternalId(externalId);
		couponList.add(result);
	}
	
	public void addTagItem(MemberTagItem memberTagItem){
		this.memberTagItemResult = memberTagItem;
	}
	
	
	/**
	 * addActivityInstainceId:(添加活动实例ID). <br/>
	 * Date: 2015-9-23 上午11:38:07 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param instanceId
	 */
	public void addActivityInstainceId(Long instanceId){
		activityInstainceIdList.add(instanceId);
	}
	
	
	
	/**
	 * getActivityInstainceIdList:(得到活动实例列表). <br/>
	 * Date: 2015-9-23 上午11:38:24 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	public List<Long> getActivityInstainceIdList() {
		return activityInstainceIdList;
	}
	
	/**
	 * putCouponQuantity:新增优惠券记录<br/>
	 * @author menn
	 * Date: 2014-5-22 下午3:28:57
	 * @param couponId
	 * @param Quantity
	 * @since JDK 1.6
	 */
	public void putCoupon(Long couponId, Integer Quantity){
		couponQuantityMap.put(couponId,Quantity);
	}
	
	/**
	 * getPointsResults:(得到积分结果). <br/>
	 * Date: 2015-9-8 下午8:54:47 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	public List<MemberPointsItem> getPointsResults(){
		return memberPointsItemList;
	}
	
	/**
	 * getCouponResults:(得到发送优惠券信息). <br/>
	 * Date: 2015-9-8 下午8:55:45 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	public Map<Long, Integer> getCouponResults() {
		return couponQuantityMap;
	}

	public void addTempInstanceId(Long instanceId) {
		tmpInstanceList.add(instanceId);
	}
	public Object getResult() {
		return result;
	}
	
	public void setResult(Object result) {
		this.result = result;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}

	public String getActivityInstanceId() {
		return activityInstanceId;
	}

	public void setActivityInstanceId(String activityInstanceId) {
		this.activityInstanceId = activityInstanceId;
	}

	public List<CouponResult> getCouponList() {
		return couponList;
	}

	public MemberTagItem getMemberTagItemResult() {
		return memberTagItemResult;
	}

	public String getIsPerfectMember() {
		return isPerfectMember;
	}

	public void setIsPerfectMember(String isPerfectMember) {
		this.isPerfectMember = isPerfectMember;
	}

	public List<Long> getTmpInstanceList() {
		return tmpInstanceList;
	}

	public void setTmpInstanceList(List<Long> tmpInstanceList) {
		this.tmpInstanceList = tmpInstanceList;
	}
}

