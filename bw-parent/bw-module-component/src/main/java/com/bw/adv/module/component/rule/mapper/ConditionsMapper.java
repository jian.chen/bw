package com.bw.adv.module.component.rule.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.component.rule.model.Conditions;

public interface ConditionsMapper extends BaseMapper<Conditions>{
	
}