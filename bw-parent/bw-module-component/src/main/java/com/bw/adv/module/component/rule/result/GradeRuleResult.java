package com.bw.adv.module.component.rule.result;



import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: 等级规则执行结果 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-29 上午11:28:26 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class GradeRuleResult extends BaseRuleResult{
	
	private Map<Long, Integer> couponQuantityMap = new HashMap<Long, Integer>();
	
	public void putCouponQuantity(Long couponId, Integer Quantity){
		couponQuantityMap.put(couponId,Quantity);
	}
	
	public Map<Long, Integer> getCouponQuantityMap() {
		return couponQuantityMap;
	}

}

