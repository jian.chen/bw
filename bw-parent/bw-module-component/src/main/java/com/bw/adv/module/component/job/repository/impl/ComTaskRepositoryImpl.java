package com.bw.adv.module.component.job.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.job.mapper.ComTaskMapper;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.model.exp.ComTaskExp;
import com.bw.adv.module.component.job.repository.ComTaskRepository;

@Repository
public class ComTaskRepositoryImpl extends BaseRepositoryImpl<ComTask, ComTaskMapper> implements ComTaskRepository {

	@Override
	protected Class<ComTaskMapper> getMapperClass() {
		return ComTaskMapper.class;
	}
	
	@Override
	public List<ComTask> findByStatusId(String statusIds) {
		return this.getMapper().selectByStatusId(statusIds);
	}

	@Override
	public List<ComTask> findJobTask() {
		return this.getMapper().selectJobTask("Y");
	}
	
	@Override
	public List<ComTask> findJobTaskByTypeId(String className){
		return this.getMapper().selectJobTaskByTypeId(className);
	}

	@Override
	public void createNoJobComTask(List<String> contentList, String taskName,Long taskTypeId) {
		
	}

	@Override
	public List<ComTaskExp> findExpByExample(Example example,Page<ComTaskExp> page) {
		
		return this.getMapper().selectExpByExample(example,page);
	}

	
	

}
