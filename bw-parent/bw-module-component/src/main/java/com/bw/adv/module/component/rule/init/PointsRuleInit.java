/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:PointsRuleInit.java
 * Package Name:com.sage.scrm.module.component.rule.init
 * Date:2015-9-22下午4:21:23
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.component.rule.init;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.points.model.PointsRule;
import com.bw.adv.module.points.repository.PointsRuleRepository;

/**
 * ClassName:PointsRuleInit <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-9-22 下午4:21:23 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public class PointsRuleInit {
	
	private static final Map<Long, PointsRule> POINTS_RULE_MAP = new HashMap<Long, PointsRule>();//积分规则map
	private static final Logger logger = LoggerFactory.getLogger(PointsRuleInit.class);
	private PointsRuleRepository pointsRuleRepository;
	
	@PostConstruct
	public void init() {
		initPointsRule();
		logger.info("积分规则初始化完成......");
	}

	public void reLoad(){
		POINTS_RULE_MAP.clear();
		init();
	}
	
	/**
	 * initPointsRule:(初始化积分规则). <br/>
	 * Date: 2015-9-8 下午6:19:55 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	private void initPointsRule(){
		List<PointsRule> list = null;
		try {
			list = pointsRuleRepository.findByIsActive("Y");
			for (PointsRule pointsRule : list) {
				POINTS_RULE_MAP.put(pointsRule.getPointsRuleId(), pointsRule);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * getPointsRule:(根据积分规则ID，得到积分规则). <br/>
	 * Date: 2015-9-8 下午9:01:31 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param pointsRuleId
	 * @return
	 */
	public static PointsRule getPointsRule(Long pointsRuleId){
		return POINTS_RULE_MAP.get(pointsRuleId);
	}

	
	@Autowired
	public void setPointsRuleRepository(PointsRuleRepository pointsRuleRepository) {
		this.pointsRuleRepository = pointsRuleRepository;
	}
	
}

