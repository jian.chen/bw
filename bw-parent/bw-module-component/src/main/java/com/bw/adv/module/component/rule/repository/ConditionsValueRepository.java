/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:RuleRepository.java
 * Package Name:com.sage.scrm.module.component.rule.repository.impl
 * Date:2015-8-21下午6:03:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.component.rule.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.component.rule.mapper.ConditionsValueMapper;
import com.bw.adv.module.component.rule.model.ConditionsValue;

/**
 * ClassName:ConditionsValueRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-21 下午6:03:24 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface ConditionsValueRepository extends BaseRepository<ConditionsValue, ConditionsValueMapper>{

	/**
	 * removeByActivityInstanceId:(删除活动实例所对应的条件值). <br/>
	 * Date: 2015-9-3 下午6:18:35 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityInstanceId
	 */
	int removeByActivityInstanceId(Long activityInstanceId);

}

