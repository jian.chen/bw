package com.bw.adv.module.component.rule.mapper;

import com.bw.adv.module.component.rule.model.BusinessType;

public interface BusinessTypeMapper {
    int deleteByPrimaryKey(Long businessTypeId);

    int insert(BusinessType record);

    int insertSelective(BusinessType record);

    BusinessType selectByPrimaryKey(Long businessTypeId);

    int updateByPrimaryKeySelective(BusinessType record);

    int updateByPrimaryKey(BusinessType record);
}