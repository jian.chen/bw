package com.bw.adv.module.component.rule.result;


import java.util.ArrayList;
import java.util.List;

public class BaseRuleResult{
	
	private List<Message> messages = new ArrayList<Message>();
	private Object result;//规则执行结果
	private Object conditionRel;//条件关系
	

	public void addMessage(Object value, String message){
		messages.add(new Message(value, message));
	}
	
	public void addMessage(Long id, Object value, String message, String enMessage){
		messages.add(new Message(id, value, message, enMessage));
	}
	
	public void setResult(Object result){
		this.result = result;
	}
	
	public Object getResult(){
		return this.result;
	}
	
	public Object getConditionRel() {
		return conditionRel;
	}
	
	public void setConditionRel(Object conditionRel) {
		this.conditionRel = conditionRel;
	}

	public String getMessage(){
		StringBuffer messageBuffer = new StringBuffer();
		for(Message message : messages){
			if(message.getValue() instanceof Boolean){
				if(!(Boolean)message.getValue()){
					messageBuffer.append(message.getMesssage() + " 不符合条件<br/>");
				}
			}
		}
		return messageBuffer.toString();
	}
	
	public List<Message> getConditionResults(){
		return messages;
	}
	
	@Override
	public String toString() {
		return "RuleResult [messages=" + messages + ", result=" + result + "]";
	}

	public class Message{
		
		private Long id;
		private Object value;
		private String messsage;
		private String enMessage;
		
		public Message(Object value, String messsage) {
			this.value = value;
			this.messsage = messsage;
		}
		
		public Message(Long id, Object value, String messsage, String enMessage) {
			this.id = id;
			this.value = value;
			this.messsage = messsage;
			this.enMessage = enMessage;
		}
		
		public Long getId() {
			return id;
		}
		
		public void setId(Long id) {
			this.id = id;
		}
		
		public Object getValue() {
			return value;
		}
		
		public String getMesssage() {
			return messsage;
		}
		
		public void setValue(Object value) {
			this.value = value;
		}
		
		public void setMesssage(String messsage) {
			this.messsage = messsage;
		}
		
		public String getEnMessage() {
			return enMessage;
		}
		
		public void setEnMessage(String enMessage) {
			this.enMessage = enMessage;
		}
		
		@Override
		public String toString() {
			return "Message [id=" + id + ", value=" + value + ", messsage=" + messsage + ", enMessage=" + enMessage + "]";
		}
	}
	
}
