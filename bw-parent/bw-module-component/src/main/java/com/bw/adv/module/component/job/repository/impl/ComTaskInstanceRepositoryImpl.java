package com.bw.adv.module.component.job.repository.impl;


import java.util.List;

import org.springframework.stereotype.Service;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.job.mapper.ComTaskInstanceMapper;
import com.bw.adv.module.component.job.model.ComTaskInstance;
import com.bw.adv.module.component.job.repository.ComTaskInstanceRepository;

@Service
public class ComTaskInstanceRepositoryImpl extends BaseRepositoryImpl<ComTaskInstance, ComTaskInstanceMapper> implements ComTaskInstanceRepository{

	@Override
	protected Class<ComTaskInstanceMapper> getMapperClass() {
		return ComTaskInstanceMapper.class;
	}
	
	@Override
	public List<ComTaskInstance> findNotRunInstance() {
		return this.getMapper().selectByStatusId(StatusConstant.JOB_SUCCEED.getId());
	}

	@Override
	public List<ComTaskInstance> findByRefId(Long refId) {
		return this.getMapper().selectByRefId(refId);
	}

	



}
