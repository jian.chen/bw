package com.bw.adv.module.component.job.base;


import java.util.Date;
import java.util.concurrent.Callable;

import org.springframework.context.ApplicationContext;

import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.job.model.ComTaskInstance;
import com.bw.adv.module.component.job.model.TaskResult;
import com.bw.adv.module.tools.DateUtils;

public abstract class BaseTask implements Callable<TaskResult> {

	private ApplicationContext springContext;

	private ComTaskInstance comTaskInstance;

	public abstract void init(ApplicationContext context);

	public abstract void exectue(ComTaskInstance comTaskInstance, TaskResult result);

	@Override
	public TaskResult call() throws Exception {
		setRun();
		init(springContext);
		TaskResult result = new TaskResult();
		result.setStartTime(System.currentTimeMillis());
		try{
			exectue(comTaskInstance, result);
			comTaskInstance.setTaskInstanceStatus(StatusConstant.JOB_SUCCEED.getId().toString());
			result.setSucceed("任务执行成功");
		}catch(Exception e){
			result.setFaied(e.getMessage());
			comTaskInstance.setTaskInstanceStatus(StatusConstant.JOB_FAILED.getId().toString());
			e.printStackTrace();
		}
		result.setEndTime(System.currentTimeMillis());
		setCompleted(result);
		return result;
	}
	
	private void setRun(){
		if(this.comTaskInstance != null){
			comTaskInstance.setTaskInstanceStatus(StatusConstant.JOB_RUN.getId().toString());
//			springContext.getBean(ComTaskInstanceRepositoryService.class).update(comTaskInstance);
		}else{
//			throw new ObjectIsNullException("ComTaskInstance 实例为空，无法执行");
		}
	}
	
	private void setCompleted(TaskResult result){
		comTaskInstance.setStartTime(DateUtils.formatDate(new Date(result.getStartTime()), "yyyyMMddHHmmss"));
		comTaskInstance.setEndTime(DateUtils.formatDate(new Date(result.getEndTime()), "yyyyMMddHHmmss"));
//		springContext.getBean(ComTaskInstanceRepositoryService.class).update(comTaskInstance);
	}
	
	
	public void setContext(ApplicationContext context) {
		this.springContext = context;
	}

	public void setComTaskInstance(ComTaskInstance comTaskInstance){
		this.comTaskInstance = comTaskInstance;
	}
}
