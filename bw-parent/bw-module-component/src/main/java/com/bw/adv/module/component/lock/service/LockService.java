package com.bw.adv.module.component.lock.service;

public interface LockService {

	boolean lock(String lockCode);
	
	boolean unlock(String lockCode);
	
}
