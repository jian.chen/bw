package com.bw.adv.module.component.rule.mapper;

import com.bw.adv.module.component.rule.model.ConditionParameterValue;

public interface ConditionParameterValueMapper {
    int deleteByPrimaryKey(Long conditionParameterValueId);

    int insert(ConditionParameterValue record);

    int insertSelective(ConditionParameterValue record);

    ConditionParameterValue selectByPrimaryKey(Long conditionParameterValueId);

    int updateByPrimaryKeySelective(ConditionParameterValue record);

    int updateByPrimaryKey(ConditionParameterValue record);
}