package com.bw.adv.module.component.rule.mapper;

import com.bw.adv.module.component.rule.model.ConditionParameter;

public interface ConditionParameterMapper {
    int deleteByPrimaryKey(Long conditionParameterId);

    int insert(ConditionParameter record);

    int insertSelective(ConditionParameter record);

    ConditionParameter selectByPrimaryKey(Long conditionParameterId);

    int updateByPrimaryKeySelective(ConditionParameter record);

    int updateByPrimaryKey(ConditionParameter record);
}