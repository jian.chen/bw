package com.bw.adv.module.component.job.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.component.job.model.ComTaskParam;

/**
 * ClassName: ComTaskParamRepositoryService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-3 下午8:22:42 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface ComTaskParamRepository extends BaseRepository<ComTaskParam, Long> {

	public List<ComTaskParam> findByTask(Long taskId);
	
	public List<ComTaskParam> findComTaskParamByTaskIdAndTaskParamType(Long taskId, String taskParamType);
}
