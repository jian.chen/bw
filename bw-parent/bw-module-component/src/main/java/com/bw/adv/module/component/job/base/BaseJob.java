package com.bw.adv.module.component.job.base;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.model.ComTaskInstance;
import com.bw.adv.module.component.job.model.ComTaskParam;
import com.bw.adv.module.component.job.repository.ComTaskInstanceRepository;
import com.bw.adv.module.component.job.repository.ComTaskParamRepository;
import com.bw.adv.module.component.job.repository.ComTaskRepository;
import com.bw.adv.module.component.job.repository.ComTaskTypeRepository;
import com.bw.adv.module.component.lock.service.LockService;
import com.bw.adv.module.tools.DateUtils;

public abstract class BaseJob extends QuartzJobBean {
	
	protected static final Logger logger = Logger.getLogger(BaseJob.class);

	public static final String JOB_PARAM_TASK = "TASK";
	public static final String JOB_PARAM_INSTANCE = "INSTANCE";
	public static final String JOB_PARAM_SCHEDULE = "JOB_PARAM_SCHEDULE";
	public static final String JOB_PARAM_ONCE = "ONCE";
	public static final String JOB_PARAM_NAME_KEY = "JOB_PARAM_NAME_KEY";
	public static final String JOB_PARAM_CONTEXT = "APPLICATION_CONTEXT";
	public static final String PARAM_SPLIT = ",";
	private ComTask comTask;

	private ComTaskInstance comTaskInstance;

	private List<ComTaskParam> list;

	private Long taskId;
	
	protected ComTaskTypeRepository comTaskTypeRepositoryService;

	protected ComTaskRepository comTaskRepositoryService;

	protected ComTaskInstanceRepository comTaskInstanceRepositoryService;

	protected ComTaskParamRepository comTaskParamRepositoryService;

	private ApplicationContext applicationContext;

	private Scheduler schedulerFactory;
	
	private LockService lockService;

	protected abstract void excute() throws Exception;
	
	protected abstract String getLockCode();
	
	protected abstract boolean isNeedLock();

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = null;
		try {
			JobDetail jd = context.getJobDetail();
			jobDataMap = jd.getJobDataMap();
			this.applicationContext = (ApplicationContext) jobDataMap.get(JOB_PARAM_CONTEXT);
			this.initService();
			this.comTask = (ComTask) jobDataMap.get(JOB_PARAM_TASK);
			this.comTaskInstance = (ComTaskInstance) jobDataMap.get(JOB_PARAM_INSTANCE);
			this.schedulerFactory = (Scheduler) jobDataMap.get(JOB_PARAM_SCHEDULE);
			this.taskId = comTask != null ? this.comTask.getTaskId() : this.comTaskInstance.getRefId();
			
			this.setStart();//任务开始执行
			if(!this.isNeedLock()){
				this.excute();
			}else{
				boolean isLock = false;
				try{
					isLock = lockService.lock(this.getLockCode());
					if(!isLock){ 
						return; 
					}
					this.excute();
				}catch(Exception e){
					e.printStackTrace();
					logger.error(e.getMessage());
				}finally{
					if(isLock){
						lockService.unlock(this.getLockCode());
					}
				}
			}
			
			this.setSuccess();
		} catch (Exception e) {
			this.setError(e);
			e.printStackTrace();
		} finally {
			if (jobDataMap != null && jobDataMap.containsKey(JOB_PARAM_ONCE)
					&& Boolean.getBoolean(String.valueOf(jobDataMap.get(JOB_PARAM_ONCE)))) {
				String key = jobDataMap.getString(JOB_PARAM_NAME_KEY);
				try {
					this.schedulerFactory.deleteJob(key, key);
				} catch (SchedulerException e) {
					this.setError(e);
					e.printStackTrace();
				}
			}
		}
	}

	public ComTask getComTask() {
		return comTask;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 得到该任务的参数MAP
	 * @author Peter
	 * @return (key为TASK_PARAM_TYPE,value为TASK_PARAM_VALUE)
	 */
	public Map<String, String> getParamMap() {
		this.list = this.comTaskParamRepositoryService.findByTask(this.getTaskId());
		Map<String, String> paramMap = new HashMap<String, String>();
		if (list == null || list.isEmpty())
			return paramMap;
		String key = null;
		String value = null;
		for (ComTaskParam comTaskParam : list) {
			key = comTaskParam.getTaskParamType();
			if (paramMap.containsKey(key)) {
				value = paramMap.get(key) + PARAM_SPLIT + comTaskParam.getTaskParamValue();
			} else {
				value = comTaskParam.getTaskParamValue();
			}
			paramMap.put(key, value);
		}
		return paramMap;
	}

	public void deleteParam() {
	//先不删了,万一怕以后有重新计算的功能.
//		if (this.list == null || this.list.isEmpty()) {
//			return;
//		}
//		System.out.println("删除生成优惠券实例参数!" + this.taskId);
//		this.comTaskParamRepositoryService.deletes(this.list);
	}

	public Long getTaskId() {
		return this.taskId;
	}

	private void UpdateTask() {
		if (this.comTask != null) {
			this.comTaskRepositoryService.updateByPkSelective(this.comTask);
		}
		if (this.comTaskInstance != null) {
			this.comTaskInstanceRepositoryService.updateByPk(this.comTaskInstance);
		}
	}

	private void initService() {
		this.comTaskTypeRepositoryService = this.applicationContext.getBean(ComTaskTypeRepository.class);
		this.comTaskRepositoryService = this.applicationContext.getBean(ComTaskRepository.class);
		this.comTaskParamRepositoryService = this.applicationContext.getBean(ComTaskParamRepository.class);
		this.comTaskInstanceRepositoryService = this.applicationContext.getBean(ComTaskInstanceRepository.class);
		this.lockService = this.applicationContext.getBean(LockService.class);
	}

	
	private void setStart() {
		String startTime = DateUtils.formatCurrentDate(DateUtils.DATE_PATTERN_YYYYMMDDHHmmss);
		String status = StatusConstant.JOB_RUN.getId().toString();
		if (this.comTask != null) {
			comTask.setTotalCount(comTask.getTotalCount() == null ? 1: comTask.getTotalCount()+1);
			this.comTask.setLastRuntime(null);//开始执行任务，不更新最后执行时间,放到任务结束是更新时间
			this.comTask.setLastStatus(status);
		}
		if (this.comTaskInstance != null) {
			this.comTaskInstance.setStartTime(startTime);
			this.comTaskInstance.setTaskInstanceStatus(status);
		}
		this.UpdateTask();
	}

	private void setSuccess() {
		String status = StatusConstant.JOB_SUCCEED.getId().toString();
		this.setEnd(status, "SUCCESS");
	}

	private void setError(Exception e) {
		String status = StatusConstant.JOB_FAILED.getId().toString();
		String message = e.getMessage() == null ? "" : (e.getMessage().length() > 492 ? e.getMessage().substring(0, 493) : e.getMessage());
		this.setEnd(status, "ERROR: " + message);
	}

	private void setEnd(String status, String notes) {
		String endTime = DateUtils.formatCurrentDate(DateUtils.DATE_PATTERN_YYYYMMDDHHmmss);
		
		if (this.comTask != null) {
			if(status.equals(StatusConstant.JOB_FAILED.getId().toString())){
				comTask.setFaiedCount(comTask.getFaiedCount() == null ? 1: comTask.getFaiedCount()+1);
			}
			this.comTask.setLastRuntime(endTime);
			this.comTask.setLastStatus(status);
		}
		
		if (this.comTaskInstance != null) {
			this.comTaskInstance.setNotes(notes);
			this.comTaskInstance.setTaskInstanceStatus(status);
			this.comTaskInstance.setEndTime(endTime);
			Long execTime = Long.valueOf(endTime) - Long.valueOf(this.comTaskInstance.getStartTime());
			this.comTaskInstance.setExecTime(execTime.toString());
		}
		this.UpdateTask();
	}
}
