package com.bw.adv.module.component.lock.service.impl;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Service;

import com.bw.adv.module.component.lock.service.LockService;
import com.bw.adv.module.tools.DateUtils;

@Service
public class LockServiceImpl implements LockService {
	
	private static final ConcurrentMap<String, Long> LOCK_TIME_MAP = new ConcurrentHashMap<String, Long>();
	private static final Long MAX_LOCK_TIME = 300000L;//最大锁定时间为5分钟
	private DataSource dataSource;
	
	@Override
	public boolean lock(String lockCode) {
		int row = executeSql("insert into COM_LOCKS(LOCKS_CODE, LOCKS_SERVER, LOCKS_TIME) values('" + lockCode + "','" + getLocalIp() + "','" + DateUtils.getCurrentTimeOfDb() + "');");
		if(row == 1){
			setLockTime(lockCode);
		}else{
			checkLockTime(lockCode);
		}
		return row == 1;
	}

	@Override
	public boolean unlock(String lockCode) {
		int row = executeSql("delete from COM_LOCKS where LOCKS_CODE = '" + lockCode + "';");
		return row == 1;
	}
	
	public void setLockTime(String lockCode){
		LOCK_TIME_MAP.put(lockCode, System.currentTimeMillis());
	}
	
	public void checkLockTime(String lockCode){
		Long lockTime = LOCK_TIME_MAP.get(lockCode);
		if(lockTime == null){
			unlock(lockCode);
		}else{
			Long currentTime = System.currentTimeMillis();
			if(currentTime - lockTime > MAX_LOCK_TIME){
				unlock(lockCode);
			}
		}
	}
	
	public int executeSql(String sql ){
		Connection conn = null;
		int row = 0;
		try {
			conn = this.dataSource.getConnection();
			Statement st = conn.createStatement();
			row = st.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
			row = 0;
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return row;
	}

	
	public String getLocalIp(){
        String ip = "";  
        try{  
//            ip =  InetAddress.getLocalHost().getHostAddress().toString();  
        }catch(Exception e){  
            e.printStackTrace();  
        }
        return ip;
       
	}

	@Resource
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	
	
	
}
