package com.bw.adv.module.component.job.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.component.job.model.ComTaskParam;

public interface ComTaskParamMapper extends BaseMapper<ComTaskParam>{
	
}