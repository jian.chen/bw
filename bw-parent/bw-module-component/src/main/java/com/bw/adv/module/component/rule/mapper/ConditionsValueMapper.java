package com.bw.adv.module.component.rule.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.component.rule.model.ConditionsValue;

public interface ConditionsValueMapper extends BaseMapper<ConditionsValue>{

	/**
	 * removeByActivityInstanceId:(删除活动实例所对应的条件值). <br/>
	 * Date: 2015-9-3 下午6:26:44 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityInstanceId
	 * @return
	 */
	int removeByActivityInstanceId(Long activityInstanceId);
	
}