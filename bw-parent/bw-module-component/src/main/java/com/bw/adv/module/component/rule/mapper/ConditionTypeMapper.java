package com.bw.adv.module.component.rule.mapper;

import com.bw.adv.module.component.rule.model.ConditionType;

public interface ConditionTypeMapper {
    int deleteByPrimaryKey(Long conditionTypeId);

    int insert(ConditionType record);

    int insertSelective(ConditionType record);

    ConditionType selectByPrimaryKey(Long conditionTypeId);

    int updateByPrimaryKeySelective(ConditionType record);

    int updateByPrimaryKey(ConditionType record);
}