package com.bw.adv.module.component.job.service;

import java.sql.SQLException;
import java.text.ParseException;

import org.quartz.SchedulerException;

import com.bw.adv.module.component.job.model.ComTaskInstance;


public interface JobService {

	public void initJob() throws ParseException, ClassNotFoundException, SchedulerException, SQLException;


}
