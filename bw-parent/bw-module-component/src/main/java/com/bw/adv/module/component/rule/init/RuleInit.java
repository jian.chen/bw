package com.bw.adv.module.component.rule.init;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.activity.constant.ActivityConstant;
import com.bw.adv.module.activity.model.Activity;
import com.bw.adv.module.activity.model.ActivityConditions;
import com.bw.adv.module.activity.model.ActivityInstance;
import com.bw.adv.module.activity.model.exp.ActivityRuleConditionExp;
import com.bw.adv.module.activity.model.exp.BusinessActivityExp;
import com.bw.adv.module.activity.repository.ActivityConditionsRepository;
import com.bw.adv.module.activity.repository.ActivityInstanceRepository;
import com.bw.adv.module.activity.repository.ActivityRepository;
import com.bw.adv.module.activity.repository.BusinessActivityRepository;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.rule.model.Conditions;
import com.bw.adv.module.component.rule.repository.ConditionsRepository;
import com.bw.adv.module.tools.DateUtils;

/**
 * ClassName: RuleInit 规则初始化类<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-17 上午11:17:58 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class RuleInit {

	private static final Logger logger = LoggerFactory.getLogger(RuleInit.class);
	
	private static final Map<String,Activity> ACTIVITY_MAP = new HashMap<String, Activity>();	//所有的活动
	private static final Map<String,List<Conditions>> ACTIVITY_CONDITIONS_MAP_LIST = new HashMap<String, List<Conditions>>();	//活动对应条件
	private static final Map<String,Map<String,Conditions>> ACTIVITY_CONDITIONS_MAP_MAP = new HashMap<String, Map<String,Conditions>>();//活动对应条件
	
	private static final Map<String, List<ActivityInstance>> ACTIVITY_INSTANCE_MAP = new HashMap<String, List<ActivityInstance>>();;//每个活动对应的实例列表map
	private static final Map<Long, List<ActivityRuleConditionExp>> INSTANCE_CONDITION_MAP = new HashMap<Long, List<ActivityRuleConditionExp>>();;//每个实例对应的条件值列表map
	private static final Map<String, List<BusinessActivityExp>> BUSINESS_ACTIVITY_MAP = new HashMap<String, List<BusinessActivityExp>>();;//业务对应的活动
	
	private static final Map<String, ActivityInstance> INSTANCE_MAP = new HashMap<String, ActivityInstance>();//活动实例id对应活动map
	
	private static final String STATUS_IDS = StatusConstant.ACTIVITY_INSTANCE_DRAFT.getId()+","+StatusConstant.ACTIVITY_INSTANCE_ENABLE.getId();
	
	
	private ConditionsRepository conditionsRepository;
	private ActivityRepository activityRepository;
	private ActivityConditionsRepository activityConditionsRepository;
	private ActivityInstanceRepository activityInstanceRepository;
	private BusinessActivityRepository businessActivityRepository;
	
	@PostConstruct
	public void init() {
		initActivityCondition();
		initActivityConditionValue();
		initBusinessActivity();
		logger.info("活动初始化完成......");
	}

	public void reLoad(){
		synchronized (RuleInit.class) {
			ACTIVITY_MAP.clear();
			ACTIVITY_CONDITIONS_MAP_LIST.clear();
			ACTIVITY_CONDITIONS_MAP_MAP.clear();
			ACTIVITY_INSTANCE_MAP.clear();
			INSTANCE_CONDITION_MAP.clear();
			BUSINESS_ACTIVITY_MAP.clear();
			INSTANCE_MAP.clear();
			init();
		}
	}
	
	
	/**
	 * getActivityConditions:得到活动对应的条件<br/>
	 * Date: 2015-8-24 下午3:50:16 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityCode
	 * @return
	 */
	public static Map<String, Conditions> getActivityConditions(String activityCode){
		return ACTIVITY_CONDITIONS_MAP_MAP.get(activityCode);
	}
	
	/**
	 * getActivityByActivityCode:(根据活动编号得到活动详情). <br/>
	 * Date: 2015-9-5 下午7:09:19 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityCode
	 * @return
	 */
	public static Activity getActivityByActivityCode(String activityCode){
		return ACTIVITY_MAP.get(activityCode);
	}
	
	/**
	 * getInstanceConditionJson:(根据实例ID得到实例对应的条件信息). <br/>
	 * Date: 2015-9-5 下午7:09:42 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param instanceId
	 * @return
	 */
	public static JSONObject getInstanceConditionJson(Long instanceId){
		List<ActivityRuleConditionExp> list = null;
		JSONObject json = null;
		
		list = INSTANCE_CONDITION_MAP.get(instanceId);
		json = new JSONObject();
		
		for (ActivityRuleConditionExp conditionExp : list) {
			json.put(conditionExp.getConditionsCode(), StringUtils.isBlank(conditionExp.getConditionsValues()) ? "" : conditionExp.getConditionsValues());
			json.put("activityCode", conditionExp.getActivityCode());
		}
		
		return json;
	}
	
	/**
	 * getActivityInstanceList:(根据活动编号得到实例列表).<br/>
	 * Date: 2015-9-5 下午7:10:27 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param activityCode
	 * @return
	 */
	public static List<ActivityInstance> getActivityInstanceList(String activityCode){
		List<ActivityInstance> list = null;
		String currentDate = DateUtils.getCurrentDateOfDb();
		
		if(ACTIVITY_INSTANCE_MAP.get(activityCode) == null){
			list = new ArrayList<ActivityInstance > ();
		}else{
			list = ACTIVITY_INSTANCE_MAP.get(activityCode);
		}
		for (int i = 0; i < list.size(); i++) {
			boolean hasRemoved = false;
			ActivityInstance instance = list.get(i);

			//如果开始时间还没到
			if(instance.getFromDate().compareTo(currentDate) > 0 ){
				list.remove(i);
				hasRemoved = true;
			}
			//如果到期时间已过
			if(StringUtils.isNotBlank(instance.getThruDate()) && instance.getThruDate().compareTo(currentDate) < 0 && !hasRemoved){
				list.remove(i);
				hasRemoved = true;
			}
		}
		return list;
	}
	
	/**
	 * 根据活动实例id查询活动实例
	 * @param activityInstanceId
	 * @return
	 */
	public static ActivityInstance getActivityInstanceById(String activityInstanceId){
		ActivityInstance instance = null;
		instance = INSTANCE_MAP.get(activityInstanceId);
		return instance;
	}
	
	/**
	 * getBusinessActivityList:(查询业务对应的活动). <br/>
	 * Date: 2015-9-5 下午9:49:57 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param businessCode
	 * @return
	 */
	public static List<BusinessActivityExp> getBusinessActivityList(String businessCode){
		if(BUSINESS_ACTIVITY_MAP.get(businessCode) == null){
			return new ArrayList<BusinessActivityExp>(); 
		}
		return BUSINESS_ACTIVITY_MAP.get(businessCode);
	}
	
	
	
	
	
	/**
	 * initActivityConditionValue:(初始化活动和活动对应的条件,条件值). <br/>
	 * Date: 2015-9-5 下午5:11:57 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	private void initActivityConditionValue() {
		List<ActivityRuleConditionExp> allConditionExps = null;
		List<ActivityRuleConditionExp> instanceConditionExps = null;//实例对应的条件值
		
		Long instancId = null;
		try {
			allConditionExps = activityInstanceRepository.findActivityConditionValue(STATUS_IDS);
			instanceConditionExps = new ArrayList<ActivityRuleConditionExp>();
			for (ActivityRuleConditionExp activityRuleConditionExp : allConditionExps) {
				
				instancId = activityRuleConditionExp.getActivityInstanceId();
				
				if(INSTANCE_CONDITION_MAP.get(instancId) == null){
					instanceConditionExps = new ArrayList<ActivityRuleConditionExp>();
					instanceConditionExps.add(activityRuleConditionExp);
					INSTANCE_CONDITION_MAP.put(instancId, instanceConditionExps);
				}else{
					INSTANCE_CONDITION_MAP.get(instancId).add(activityRuleConditionExp);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * initActivityCondition:(初始化活动和活动对应的条件). <br/>
	 * Date: 2015-9-5 下午5:10:50 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	private void initActivityCondition() {
		List<Conditions> allList = null;
		Map<Long,Conditions> allConditionsMap = null;
		Map<String,Conditions> conditionsMap = null;
		Map<Long,Activity> allActivityMap = null;
		
		List<Activity> activitieList = null;
		List<Conditions> activityConditionList = null;
		List<ActivityConditions> allActivityConditionList = null;
		List<ActivityInstance> instanceList = null;
		List<ActivityInstance> allInstanceList = null;
		
		Long activittyId = null;
		String activittyCode = null;
		String conditionsCode = null;
		try {
			
			allList = conditionsRepository.findAll();
			activitieList = activityRepository.findAllActive();
			allActivityConditionList =  activityConditionsRepository.findAll();
			allList = conditionsRepository.findAll();
			allInstanceList = activityInstanceRepository.findByStatusIds(STATUS_IDS);
			
			
			allConditionsMap = new HashMap<Long, Conditions>();
			allActivityMap = new HashMap<Long, Activity>();
			
			//保存所有的条件值到map里
			for (Conditions conditions : allList) {
				allConditionsMap.put(conditions.getConditionsId(), conditions);
			}
			
			//保存所有的活动值到map里
			for (Activity activity: activitieList) {
				allActivityMap.put(activity.getActivityId(), activity);
				ACTIVITY_MAP.put(activity.getActivityCode(), activity);
			}
			
			//活动的实例列表放入map中
			for (ActivityInstance instance : allInstanceList) {
				activittyCode = allActivityMap.get(instance.getActivityId()).getActivityCode();
				
				INSTANCE_MAP.put(instance.getActivityInstanceId().toString(), instance);
				
				if(ACTIVITY_INSTANCE_MAP.get(activittyCode) == null){
					instanceList = new ArrayList<ActivityInstance>();
					instanceList.add(instance);
					ACTIVITY_INSTANCE_MAP.put(activittyCode, instanceList);
				}else{
					ACTIVITY_INSTANCE_MAP.get(activittyCode).add(instance);
				}
			}
			
			activityConditionList = new ArrayList<Conditions>();
			conditionsMap = new HashMap<String, Conditions>();
			for (ActivityConditions activityConditions : allActivityConditionList) {
				activittyId = activityConditions.getActivityId();
				activittyCode = allActivityMap.get(activittyId).getActivityCode();
				conditionsCode = allConditionsMap.get(activityConditions.getConditionsId()).getConditionsCode();
				
				if(ACTIVITY_CONDITIONS_MAP_MAP.get(activittyCode) == null){
					conditionsMap = new HashMap<String, Conditions>();
					conditionsMap.put(conditionsCode, allConditionsMap.get(activityConditions.getConditionsId()));
					ACTIVITY_CONDITIONS_MAP_MAP.put(activittyCode, conditionsMap);
				}else{
					ACTIVITY_CONDITIONS_MAP_MAP.get(activittyCode).put(conditionsCode, allConditionsMap.get(activityConditions.getConditionsId()));
				}
				
				if(ACTIVITY_CONDITIONS_MAP_LIST.get(activittyCode) == null){
					activityConditionList = new ArrayList<Conditions>();
					activityConditionList.add(allConditionsMap.get(activityConditions.getConditionsId()));
					ACTIVITY_CONDITIONS_MAP_LIST.put(activittyCode, activityConditionList);
				}else{
					ACTIVITY_CONDITIONS_MAP_LIST.get(activittyCode).add(allConditionsMap.get(activityConditions.getConditionsId()));
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * initBusinessActivity:(初始化加载业务活动). <br/>
	 * Date: 2015-9-8 下午6:18:33 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 */
	private void initBusinessActivity(){
		List<BusinessActivityExp> listAll = null;
		List<BusinessActivityExp> activityList = null;
		String businessCode = null;
		try {
			listAll = businessActivityRepository.findAllExp();
			activityList = new ArrayList<BusinessActivityExp>();
			
			for (BusinessActivityExp businessActivityExp : listAll) {
				businessCode = businessActivityExp.getBusinessCode();
				if(BUSINESS_ACTIVITY_MAP.get(businessCode) == null){
					activityList = new ArrayList<BusinessActivityExp>();
					activityList.add(businessActivityExp);
					BUSINESS_ACTIVITY_MAP.put(businessCode, activityList);
				}else{
					BUSINESS_ACTIVITY_MAP.get(businessCode).add(businessActivityExp);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@Autowired
	public void setConditionsRepository(ConditionsRepository conditionsRepository) {
		this.conditionsRepository = conditionsRepository;
	}

	@Autowired
	public void setActivityConditionsRepository(ActivityConditionsRepository activityConditionsRepository) {
		this.activityConditionsRepository = activityConditionsRepository;
	}
	
	@Autowired
	public void setActivityRepository(ActivityRepository activityRepository) {
		this.activityRepository = activityRepository;
	}

	@Autowired
	public void setActivityInstanceRepository(ActivityInstanceRepository activityInstanceRepository) {
		this.activityInstanceRepository = activityInstanceRepository;
	}

	@Autowired
	public void setBusinessActivityRepository(BusinessActivityRepository businessActivityRepository) {
		this.businessActivityRepository = businessActivityRepository;
	}

	
}
