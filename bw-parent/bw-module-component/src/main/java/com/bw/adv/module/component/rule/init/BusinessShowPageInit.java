package com.bw.adv.module.component.rule.init;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.activity.model.BusinessShowPage;
import com.bw.adv.module.activity.repository.BusinessShowPageRepository;

/**
 * ClassName: BusinessShowPageInit 业务页面<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-17 上午11:17:58 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class BusinessShowPageInit {

	private static final Logger logger = LoggerFactory.getLogger(BusinessShowPageInit.class);
	
	private static final Map<String,String> PAGE_URL_MAP = new HashMap<String, String>();	//所有的活动
	private static final Map<String, BusinessShowPage> PAGE_MAP = new HashMap<String, BusinessShowPage>();//活动实例id对应活动map
	
	private BusinessShowPageRepository businessShowPageRepository;
	
	@PostConstruct
	public void init() {
		List<BusinessShowPage> list = null;
		
		list = businessShowPageRepository.findAll();
		for (BusinessShowPage page : list) {
			PAGE_MAP.put(page.getBusinessShowPageCode(), page);
			PAGE_URL_MAP.put(page.getBusinessShowPageCode(), page.getPageUrl());
		}
		logger.info("活动初始化完成......");
	}

	public static BusinessShowPage getShowPageByCode(String code){
		return PAGE_MAP.get(code);
	}
	
	public static String getShowPageUrlByCode(String code){
		return PAGE_URL_MAP.get(code);
	}

	@Autowired
	public void setBusinessShowPageRepository(BusinessShowPageRepository businessShowPageRepository) {
		this.businessShowPageRepository = businessShowPageRepository;
	}
	
	
	
}
