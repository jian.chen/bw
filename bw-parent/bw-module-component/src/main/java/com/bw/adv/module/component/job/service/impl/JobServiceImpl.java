package com.bw.adv.module.component.job.service.impl;


import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.quartz.CronExpression;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.CronTriggerBean;
import org.springframework.scheduling.quartz.JobDetailBean;

import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.component.job.base.BaseJob;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.model.ComTaskType;
import com.bw.adv.module.component.job.repository.ComTaskRepository;
import com.bw.adv.module.component.job.repository.ComTaskTypeRepository;
import com.bw.adv.module.component.job.service.JobService;
import com.bw.adv.module.tools.DateUtils;

/**
 * ClassName: JobServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-4 下午1:50:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class JobServiceImpl implements JobService {

	private ComTaskRepository comTaskRepository;
	private ComTaskTypeRepository comTaskTypeRepository;
	private Scheduler schedulerFactory;
	private ApplicationContext context;
	@Value("${start_job}")
	private String START_JOB;

	@PostConstruct
	public void initJob() throws ParseException, ClassNotFoundException, SchedulerException, SQLException {
		System.out.println(START_JOB+"**********是否开启任务***********");
		if (!"Y".equals(START_JOB)) {
			return;
		}
		//只加在任务的job
		List<ComTask> allTasks = this.comTaskRepository.findJobTask();
		Map<Long, ComTaskType> taskTypeMap = this.getTaskTypeMap();
		for (ComTask comTask : allTasks) {
			if (comTask.getEndDate() != null) {
				// 过期的就不要注册了，改变任务状态
				Long endDate = DateUtils.StringToLong(comTask.getEndDate(), DateUtils.DATE_PATTERN_YYYYMMDD);
				if (endDate <= new Date().getTime() && !StatusConstant.JOB_RUN.getId().toString().equals(comTask.getLastStatus())) {
					comTask.setLastStatus(StatusConstant.JOB_EXPIRED.getId().toString());
					comTaskRepository.updateByPk(comTask);
					continue;
				}
			}
			try {
				this.registOperation(comTask, taskTypeMap);
			} catch (Exception e) {
				System.out.println("TASK: " + comTask.getTaskName() + " 注册失败");
				e.printStackTrace();
			}
		}
	}

	/**
	 * 根据任务调度器，作业名称，作业类，启动时间间隔，及作业信息 注册作业任务到任务调度器上
	 * @param comTask 任务记录
	 * @param taskTypeMap 以TASK_TYPE_ID为KEY的map
	 * @throws SchedulerException
	 * @throws ClassNotFoundException
	 * @throws java.text.ParseException
	 */
	public void registOperation(ComTask comTask, Map<Long, ComTaskType> taskTypeMap) throws SchedulerException, ClassNotFoundException,
			java.text.ParseException {

		ComTaskType ctt = taskTypeMap.get(comTask.getTaskTypeId());
		// 创建任务的作业信息
		// 1）作业明细，作业数据集合，触发器
		// 2）根据作业明细，触发器来实现任务的注册
		JobDetailBean jobDetail = new JobDetailBean();
		jobDetail.setName(comTask.getTaskName());
		jobDetail.setBeanName(comTask.getTaskName());
		jobDetail.setGroup(comTask.getTaskId().toString());
		if (ctt.getTaskTypeClass() == null)
			return;
		jobDetail.setJobClass(Class.forName(ctt.getTaskTypeClass()));

		jobDetail.getJobDataMap().put(BaseJob.JOB_PARAM_TASK, comTask);
		jobDetail.getJobDataMap().put(BaseJob.JOB_PARAM_CONTEXT, this.context);
		jobDetail.setApplicationContextJobDataKey(BaseJob.JOB_PARAM_CONTEXT);

		/**
		 * 根据taskTimer 中的规则生成 trigger的触发规则
		 */
		CronTriggerBean cronTrigger = new CronTriggerBean();

		cronTrigger.setName(comTask.getTaskName());// 任务触发器的名字
		cronTrigger.setGroup(comTask.getTaskId().toString());

		String cronExpressionStr = comTask.getScheduleTime();
		// cronExpressionStr="0 49 9 12 11 ? *";
		System.out.println(cronExpressionStr);

		CronExpression cronExpression = new CronExpression(cronExpressionStr);

		cronTrigger.setCronExpression(cronExpression);// 根据基本设置中的任务触发设置规则，生成任务执行规则
		if (StringUtils.isNotBlank(comTask.getEndDate()) && DateUtils.StringToLong(comTask.getEndDate(), DateUtils.DATE_PATTERN_YYYYMMDD) > new Date().getTime()) {
			cronTrigger.setEndTime(DateUtils.dateStrToDate(comTask.getEndDate(), DateUtils.DATE_PATTERN_YYYYMMDD));
		}
		if (StringUtils.isNotBlank(comTask.getStartDate())) {
			cronTrigger.setStartTime(DateUtils.dateStrToDate(comTask.getStartDate(), DateUtils.DATE_PATTERN_YYYYMMDD));
		}
		cronTrigger.setJobDetail(jobDetail);
		// 注册作业

		this.schedulerFactory.scheduleJob(jobDetail, cronTrigger);
		System.out.println("TASK: " + comTask.getTaskName() + " 已注册");
		if (StatusConstant.JOB_RUN.getId().toString().equals(comTask.getLastStatus())) {
			this.schedulerFactory.triggerJob(jobDetail.getName(), jobDetail.getGroup());
		}
	}



	/**
	 * 根据调度器工厂获得一个调度器
	 * 
	 * @author Peter
	 * @return Scheduler
	 */
	public Scheduler getScheduler() {
		Scheduler scheduler = null;
		try {
			// 根据调度器工厂获得一个任务调度器的实例
			scheduler = StdSchedulerFactory.getDefaultScheduler();
		} catch (SchedulerException ex) {
			ex.printStackTrace();
		}
		return scheduler;
	}


	/**
	 * getTaskTypeMap:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015-9-19 下午7:27:12 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	private Map<Long, ComTaskType> getTaskTypeMap() {
		Map<Long, ComTaskType> resultMap = null;
		List<ComTaskType> comTaskTypeList = null;
		
		resultMap = new HashMap<Long, ComTaskType>();
		comTaskTypeList = this.comTaskTypeRepository.findAll();
		if (comTaskTypeList == null || comTaskTypeList.isEmpty()){
			return resultMap;
		}
		
		for (ComTaskType comTaskType : comTaskTypeList) {
			resultMap.put(comTaskType.getTaskTypeId(), comTaskType);
		}
		return resultMap;
	}

	
	@Resource
	public void setSchedulerFactory(Scheduler schedulerFactory) {
		this.schedulerFactory = schedulerFactory;
	}

	@Resource
	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	@Autowired
	public void setComTaskRepository(ComTaskRepository comTaskRepository) {
		this.comTaskRepository = comTaskRepository;
	}

	@Autowired
	public void setComTaskTypeRepository(ComTaskTypeRepository comTaskTypeRepository) {
		this.comTaskTypeRepository = comTaskTypeRepository;
	}

}
