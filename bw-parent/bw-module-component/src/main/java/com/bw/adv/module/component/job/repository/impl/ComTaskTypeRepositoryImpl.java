package com.bw.adv.module.component.job.repository.impl;


import org.springframework.stereotype.Service;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.component.job.mapper.ComTaskTypeMapper;
import com.bw.adv.module.component.job.model.ComTaskType;
import com.bw.adv.module.component.job.repository.ComTaskTypeRepository;

@Service
public class ComTaskTypeRepositoryImpl extends BaseRepositoryImpl<ComTaskType, ComTaskTypeMapper> implements ComTaskTypeRepository{
	

	@Override
	protected Class<ComTaskTypeMapper> getMapperClass() {
		return ComTaskTypeMapper.class;
	}

}
