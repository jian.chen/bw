package com.bw.adv.module.component.job.base;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.bw.adv.module.component.job.model.TaskResult;

public class ManagedThreadPoolTaskExecutor {

	private static final ThreadPoolTaskExecutor threadPoolTaskExecutor;
	private ApplicationContext context;

	static{
		threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.initialize();
	}
	
	public Future<TaskResult> submit(BaseTask task) {
		task.setContext(context);
		return threadPoolTaskExecutor.submit(task);
	}
	
	/**
	 * 当前活动线程数
	 * @return int
	 */
	public int getActiveCount(){
		return threadPoolTaskExecutor.getActiveCount();
	}
	
	/**
	 * 返回此执行程序使用的任务队列
	 * @return 任务队列
	 */
	public BlockingQueue<Runnable> getQueue(){
		return threadPoolTaskExecutor.getThreadPoolExecutor().getQueue();
	}
	

	public void setCorePoolSize(int corePoolSize) {
		threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
	}

	public void setMaxPoolSize(int maxPoolSize) {
		threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
	}

	public void setKeepAliveSeconds(int keepAliveSeconds) {
		threadPoolTaskExecutor.setKeepAliveSeconds(keepAliveSeconds);
	}

	public void setAllowCoreThreadTimeOut(boolean allowCoreThreadTimeOut) {
		threadPoolTaskExecutor.setAllowCoreThreadTimeOut(allowCoreThreadTimeOut);
	}

	public void setQueueCapacity(int queueCapacity) {
		threadPoolTaskExecutor.setQueueCapacity(queueCapacity);
	}

	@Resource
	public void setContext(ApplicationContext context) {
		this.context = context;
	}

}
