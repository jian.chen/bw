package com.bw.adv.module.component.job.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.model.exp.ComTaskExp;

public interface ComTaskMapper extends BaseMapper<ComTask>{

	List<ComTask> selectByStatusId(@Param("statusIds") String statusIds);

	List<ComTask> selectJobTask(String isJob);
	
	List<ComTask> selectJobTaskByTypeId(String className);
	
	List<ComTaskExp> selectExpByExample(@Param("example")Example example,Page<ComTaskExp> page);
	
}