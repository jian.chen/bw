package com.bw.adv.module.component.job.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.component.job.model.ComTaskInstance;

public interface ComTaskInstanceMapper extends BaseMapper<ComTaskInstance>{

	List<ComTaskInstance> selectByStatusId(Long statusId);

	List<ComTaskInstance> selectByRefId(Long refId);
	
}