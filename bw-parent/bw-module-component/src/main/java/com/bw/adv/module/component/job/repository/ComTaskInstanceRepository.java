package com.bw.adv.module.component.job.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.component.job.mapper.ComTaskInstanceMapper;
import com.bw.adv.module.component.job.model.ComTaskInstance;

public interface ComTaskInstanceRepository extends BaseRepository<ComTaskInstance, ComTaskInstanceMapper> {

	public List<ComTaskInstance> findNotRunInstance();

	public List<ComTaskInstance> findByRefId(Long refId);
	
}
