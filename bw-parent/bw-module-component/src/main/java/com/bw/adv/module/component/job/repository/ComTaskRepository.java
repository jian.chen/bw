/**
 * @author Peter
 *
 */
package com.bw.adv.module.component.job.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.job.mapper.ComTaskMapper;
import com.bw.adv.module.component.job.model.ComTask;
import com.bw.adv.module.component.job.model.exp.ComTaskExp;

/**
 * @author Peter
 * 
 */
public interface ComTaskRepository extends BaseRepository<ComTask, ComTaskMapper> {
	/**
	 * 按状态查询非job的任务列表
	 * @param status
	 * @return
	 */
	public List<ComTask> findByStatusId(String statusId);
	
	/**
	 * 查询定时任务列表
	 * @return
	 */
	public List<ComTask> findJobTask();
	
	/**
	 * 根据类名查询任务
	 * @return
	 */
	public List<ComTask> findJobTaskByTypeId(String className);
	
	public void createNoJobComTask(List<String> contentList, String taskName, Long taskTypeId);
	
	/**
	 * 
	 * findAllExp:根据查询条件查询所有定时任务(扩展类，关联com_task_type表)
	 * Date: 2015年9月5日 下午3:22:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	public List<ComTaskExp> findExpByExample(Example example,Page<ComTaskExp> page);
}
