/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-component
 * File Name:RuleRepository.java
 * Package Name:com.sage.scrm.module.component.rule.repository.impl
 * Date:2015-8-21下午6:03:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.component.rule.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.component.rule.mapper.ConditionsMapper;
import com.bw.adv.module.component.rule.model.Conditions;

/**
 * ClassName:RuleRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-21 下午6:03:24 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public interface ConditionsRepository extends BaseRepository<Conditions, ConditionsMapper>{

}

