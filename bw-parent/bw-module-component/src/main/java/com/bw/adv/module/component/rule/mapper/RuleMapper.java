package com.bw.adv.module.component.rule.mapper;

import com.bw.adv.module.component.rule.model.Rule;

public interface RuleMapper {
    int deleteByPrimaryKey(Long ruleId);

    int insert(Rule record);

    int insertSelective(Rule record);

    Rule selectByPrimaryKey(Long ruleId);

    int updateByPrimaryKeySelective(Rule record);

    int updateByPrimaryKey(Rule record);
}