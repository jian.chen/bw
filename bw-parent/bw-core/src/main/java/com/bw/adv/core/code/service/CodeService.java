package com.bw.adv.core.code.service;


import java.util.List;

/**
 * ClassName: CodeService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-26 下午9:04:19 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface CodeService {
	
	/**
	 * generateCode:按实体类获取记录的编码支持一次生成多个<br/>
	 * Date: 2015-8-14 上午10:41:54 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param domainClass 需要生成编码的实体类class
	 * @param count 需要生成的编码数量
	 * @return
	 * @throws Exception
	 */
	public List<String> generateCode(Class<?> domainClass, int count) throws Exception;
	
	/**
	 * generateCode:(按实体类获取记录的编码仅生成一个). <br/>
	 * Date: 2015-8-14 上午10:42:39 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param domainClass 需要生成编码的实体类class
	 * @return
	 * @throws Exception
	 */
	public String generateCode(Class<?> domainClass) throws Exception;
	
	/**
	 * generateCode:(按实体类获取记录的编码支持一次生成多个). <br/>
	 * Date: 2015-8-14 上午10:43:05 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param domainClass 需要生成编码的实体类class
	 * @param refId 业务实体类的类型区分字段，一般是业务类型的typeId
	 * @param count 需要生成的编码数量
	 * @return
	 * @throws Exception
	 */
	public List<String> generateCode(Class<?> domainClass, Long refId, int count) throws Exception;
	
	/**
	 * generateCode:(按实体类获取记录的编码仅生成一个). <br/>
	 * Date: 2015-8-14 上午10:43:30 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param domainClass 需要生成编码的实体类class
	 * @param refId 业务实体类的类型区分字段，一般是业务类型的typeId
	 * @return
	 * @throws Exception
	 */
	public String generateCode(Class<?> domainClass, Long refId) throws Exception;
	
	

}
