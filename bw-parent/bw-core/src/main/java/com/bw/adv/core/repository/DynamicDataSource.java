package com.bw.adv.core.repository;


import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDataSource extends AbstractRoutingDataSource {
 
    @Override
    public Object determineCurrentLookupKey() {
        return  DbContextHolder.getDbType();
    }
 
}