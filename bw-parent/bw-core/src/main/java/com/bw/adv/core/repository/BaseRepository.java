package com.bw.adv.core.repository;

import java.io.Serializable;
import java.util.List;

public interface BaseRepository<T, M> {

	public abstract M getMapper();

	public abstract int removeByPk(Serializable pk);

	public abstract int save(T t);

	public abstract int saveSelective(T t);
	
	public abstract int saveList(List<T> list);

	public abstract T findByPk(Serializable pk);

	public abstract int updateByPkSelective(T t);

	public abstract int updateByPk(T t);
	
	public abstract int updateList(List<T> list);
	
	public abstract List<T> findAll();
	
}