/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-core
 * File Name:BeanFactory.java
 * Package Name:com.sage.scrm.core.repository
 * Date:2015-12-17下午4:45:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.core.repository;

import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.core.NestedIOException;

import java.io.IOException;

/**
 * ClassName:BeanFactory <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-12-17 下午4:45:41 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public class BeanFactory extends SqlSessionFactoryBean {

	/**
	 * TODO 重写buildSqlSessionFactory方法，抛出mybatis mapper.xml编译的错误.
	 * @see org.mybatis.spring.SqlSessionFactoryBean#buildSqlSessionFactory()
	 */
	@Override
	protected SqlSessionFactory buildSqlSessionFactory() throws IOException {
		try {
			return super.buildSqlSessionFactory();
		} catch (NestedIOException e) {
			e.printStackTrace(); // XML 有错误时打印异常。
			System.out.println(e.getMessage());
			throw new NestedIOException("---------------Failed to parse mapping resource:", e.getCause());
		} finally {
			ErrorContext.instance().reset();
		}
	}
}

