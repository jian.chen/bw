package com.bw.adv.core.code.service.impl;



import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bw.adv.core.code.service.CodeService;

/**
 * ClassName: DomainClassCodeServiceImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-14 上午10:40:38 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Service("codeService")
public class DomainClassCodeServiceImpl implements CodeService {
	
	private DataSource dataSource;

	private static final int CHECK_CODE_LENGTH = 2;
	
	private static final char PAD_CHAR = '0';
	
	private static final String SEQ_KEY = "SEQ";
	
	private static final String PREFIX_KEY = "PREFIX";
	
	private static final String CODE_LENGTH = "CODE_LENGTH";
	
	private static final boolean IS_ADD_CHECK_CODE = false; 


	@Override
	public List<String> generateCode(Class<?> domainClass, int count) throws Exception {
		return generateCode(domainClass, null, count);
	}

	@Override
	public String generateCode(Class<?> domainClass) throws Exception {
		return generateCode(domainClass, 1).get(0);
	}

	@Override
	public List<String> generateCode(Class<?> domainClass, Long refId, int count) throws Exception {
		if (domainClass == null || count <= 0) {
			return null;
		}
		String codeSeqTypeCode = getCodeSeqTypeCode(domainClass);
		
		List<String> resultList = new ArrayList<String>();
		StringBuffer code = null;
		Map<String, Object> resultMap = this.getSeqAndPrefixByTypeCode(codeSeqTypeCode, refId, count);
		long seq = ((Long) resultMap.get(SEQ_KEY)).intValue();
		String prefix = (String) resultMap.get(PREFIX_KEY);
		int codeLength = (Integer) resultMap.get(CODE_LENGTH);
		if(codeSeqTypeCode.equals("CouponInstance")){
			for (int i = 0; i < count; i++) {
				code = new StringBuffer();
				code.append(prefix).append(StringUtils.reverse(StringUtils.leftPad(String.valueOf(seq + i), codeLength, PAD_CHAR)));
				String randemCode = this.getRandemCode();
				code.append(randemCode);
				resultList.add(code.toString());
			}
		}if(codeSeqTypeCode.equals("MemberCard")){
			for (int i = 0; i < count; i++) {
				code = new StringBuffer();
				code.append(prefix).append(StringUtils.reverse(StringUtils.leftPad(String.valueOf(seq + i), codeLength, PAD_CHAR)));
				String randemCode = this.getRandemCode();
				code.append(randemCode);
				resultList.add(code.toString());
			}
		}else{
			for (int i = 0; i < count; i++) {
				code = new StringBuffer();
				code.append(prefix).append(StringUtils.leftPad(String.valueOf(seq + i), codeLength, PAD_CHAR));
				if(IS_ADD_CHECK_CODE){
					String checkCode = this.getCheckCode(code.toString());
					code.append(checkCode);
				}
				resultList.add(code.toString());
			}
		}
		return resultList;
	}

	@Override
	public String generateCode(Class<?> domainClass, Long refId) throws Exception {
		return generateCode(domainClass, refId, 1).get(0);
	}
	

	private String getCodeSeqTypeCode(Class<?> domainClass){
		return domainClass.getSimpleName();
	}
	
	private Map<String, Object> getSeqAndPrefixByTypeCode(String codeSeqTypeCode, Long refId, int count) throws Exception {
		Connection conn = null;
		Long newSeqNo = null;
		Integer codeLength = null;
		String prefix = null;
		Map<String, Object> result = null;
		CallableStatement stmt = null;
		try {
			conn = this.dataSource.getConnection();
			//数据库存储过程定义
			//proc_generate_code_seq(IN i_code_seq_type_code CHAR(50), IN i_ref_id BIGINT, IN i_count BIGINT, OUT o_ret INT, OUT o_seq BIGINT, OUT o_prefix CHAR(50))
			stmt = conn.prepareCall("{call proc_generate_code_seq(?, ?, ?, ?, ?, ?, ?)}");
			stmt.setString(1, codeSeqTypeCode);
			if(refId == null){
				stmt.setLong(2, -1);
			}else{
				stmt.setLong(2, refId);
			}
			stmt.setInt(3, count);
	        // out 注册的index 和取值时要对应  
	        stmt.registerOutParameter(4, Types.INTEGER);  
	        stmt.registerOutParameter(5, Types.BIGINT);
	        stmt.registerOutParameter(6, Types.CHAR);
	        stmt.registerOutParameter(7, Types.INTEGER);
			conn.setAutoCommit(true);
			synchronized (codeSeqTypeCode) {
				stmt.execute();  
				// out parameter should be indexed as the same in the begin
				int ret = stmt.getInt(4);
				//如果执行结果为1，表示自增处理过程中出现错误，表示在表中没有找到对应的codeSeqTypeCode 
				if(ret == 1){
					throw new Exception("请确认CODE_SEQ_TYPE表中已经配置" + codeSeqTypeCode + "的类型记录的数据!");
				}else{
					result = new HashMap<String, Object>();
					newSeqNo = stmt.getLong(5);
					prefix = stmt.getString(6);
					codeLength = stmt.getInt(7);
					
					result.put(SEQ_KEY, newSeqNo);
					result.put(PREFIX_KEY, prefix);
					result.put(CODE_LENGTH, codeLength);
					
			        return result;
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * getCheckCode:校验码规则<br/>
	 * Date: 2015-8-14 上午10:41:29 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param code
	 * @return
	 */
	private String getCheckCode(String code) {
		Double checkCode =  Math.pow(10, CHECK_CODE_LENGTH) - Integer.valueOf(StringUtils.reverse(code.substring(code.length() - CHECK_CODE_LENGTH))) - 1;
		return String.valueOf(checkCode.intValue());
	}
	
	/**
	 * 生成2位随机数
	 * @return
	 */
	private String getRandemCode(){
		return String.valueOf((int)(Math.random() * 90 + 10));
	}
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
}

