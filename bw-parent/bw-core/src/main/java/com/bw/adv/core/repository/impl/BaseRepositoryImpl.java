package com.bw.adv.core.repository.impl;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.SeqService;
import com.bw.adv.core.utils.Id;

public abstract class BaseRepositoryImpl<T,M> extends SqlSessionDaoSupport implements BaseRepository<T, M>{

	private SeqService seqService;
	@Autowired
	protected SqlSessionFactory sqlSessionFactory;
	
	protected abstract Class<M> getMapperClass();
	
	@Override
	public M getMapper(){
//		 TransactionFactory transactionFactory = new JdbcTransactionFactory();
//		 Transaction newTransaction = transactionFactory.newTransaction(getSqlSession().getConnection());
//		 System.out.println(newTransaction.hashCode());
//		 System.out.println(getSqlSession().getConnection());
//		return getSqlSession().getMapper(getMapperClass());
		return sqlSessionFactory.getConfiguration().getMapper(getMapperClass(), getSqlSession());
	}
	
	@SuppressWarnings("unchecked")
	private BaseMapper<T> getBaseMapper(){
		return (BaseMapper<T>) this.getMapper();
	}
	
	@Override
	public int saveSelective(T t) {
//		checkEntityId(t);
		return this.getBaseMapper().insertSelective(t);
	}
	
	@Override
	public int save(T t){
//		checkEntityId(t);
    	return this.getBaseMapper().insert(t);
    }
	
	public int saveList(List<T> recordList){
//		checkEntityId(t);
		return this.getBaseMapper().insertBatch(recordList);
	}
	
	@Override
	public int removeByPk(Serializable pk) {
		return this.getBaseMapper().deleteByPrimaryKey(pk);
	}


	@Override
	public int updateByPkSelective(T t) {
		return this.getBaseMapper().updateByPrimaryKeySelective(t);
	}

	@Override
	public int updateByPk(T t) {
		return this.getBaseMapper().updateByPrimaryKey(t);
	}
	
	public int updateList(List<T> list){
		return this.getBaseMapper().updateBatch(list);
	}

	@Override
	public T findByPk(Serializable pk) {
		return this.getBaseMapper().selectByPrimaryKey(pk);
	}
	
	public List<T> findAll(){
		return this.getBaseMapper().selectAll();
	}
	
	@SuppressWarnings("unused")
	private void checkEntityIds(Iterable<T> entities) {
		int i = 0;
		List<Long> pks = null;
		for (T entity : entities) {
			i++;
		}
		try {
			pks = seqService.generateIdsJdbc(i);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		i = 0;
		for (T entity : entities) {
			Class<?> cls = entity.getClass();
			Field[] fields = cls.getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				try {
					// 判断是否有@ID注解 并且是否为NULL并且是Long类型
					if (null != field.getAnnotation(Id.class) && field.get(entity) == null && Long.class.isAssignableFrom(field.getType())) {
						field.set(entity, pks.get(i));
						i++;
						break;
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void checkEntityId(T entity) {
		Class<?> cls = entity.getClass();
		Field[] fields = cls.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			try {
				// 判断是否有@ID注解 并且是否为NULL并且是Long类型
				if (null != field.getAnnotation(Id.class) && field.get(entity) == null
						&& Long.class.isAssignableFrom(field.getType())) {
					field.set(entity, seqService.generateIdJdbc());
					break;
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Autowired
	public void setSeqService(SeqService seqService) {
		this.seqService = seqService;
	}
	
}
