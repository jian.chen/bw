package com.bw.adv.core.service;

import java.sql.SQLException;
import java.util.List;

/**
 * ClassName: SeqService <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-26 下午9:06:44 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface SeqService {

	public Long generateId();
	
	public Long generateIdJdbc() throws SQLException;

	public List<Long> generateIdsJdbc(int count) throws SQLException;
}
