package com.bw.adv.core.repository;


public class DbContextHolder {

    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();
     
    public static void setDbType(String dbType) {
        contextHolder.set(dbType);
    }
 
    public static String getDbType() {
    	String nowThreadDataSource = contextHolder.get();
    	clearDbType();
        return nowThreadDataSource;
    }
    
    
    private static void clearDbType() {
        contextHolder.remove();
    }
    
}
