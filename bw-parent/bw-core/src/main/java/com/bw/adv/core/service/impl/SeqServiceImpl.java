package com.bw.adv.core.service.impl;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.bw.adv.core.service.SeqService;


@Service
public class SeqServiceImpl implements SeqService {

	private static final String SEQ_1_TABLE = "SEQ_1";

	private static final String SEQ_2_TABLE = "SEQ_2";

	private JdbcTemplate jdbcTemplate;
	
	private DataSource dataSource;

	@Override
	public Long generateId() {
		String tableName = this.getTableName();
		int seq = 0;
		synchronized (tableName) {
			this.jdbcTemplate.update("REPLACE INTO " + tableName + " (stub) VALUES ('a');");
			seq = this.jdbcTemplate.queryForInt("SELECT LAST_INSERT_ID();");
		}
		if(SEQ_1_TABLE.equals(tableName)) {
			return new Long(seq * 2 - 1);
		} else {
			return new Long(seq * 2);
		}
	}
	
	@Override
	public List<Long> generateIdsJdbc(int count) throws SQLException {
		List<Long> resultList = new ArrayList<Long>();
		if (count <= 0) {
			return resultList;
		}
		String tableName = this.getTableName();
		Connection conn = null;
		ResultSet rs = null;
		Long id = null;
		try {
			conn = this.dataSource.getConnection();
			CallableStatement stmt = conn.prepareCall("{call proc_generate_ids(?, ?, ?)}");
			stmt.setString(1, tableName);
			stmt.setInt(2, count);
	        stmt.registerOutParameter(3, Types.BIGINT);  
			conn.setAutoCommit(true);
			synchronized (tableName) {
				stmt.execute();
				id = stmt.getLong(3); //这个拿到的是目前数据库中的值,还需要加1才能用.
			}
			for(int i = 0; i < count; i++) {
				if (SEQ_1_TABLE.equals(tableName)) {
					resultList.add(++id * 2 - 1);
				} else {
					resultList.add(++id * 2);
				}
			}
			return resultList;
		} catch (SQLException e) {
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
	
	@Override
	public Long generateIdJdbc() throws SQLException {
		String tableName = this.getTableName();
		Connection conn = null;
		try {
			conn = this.dataSource.getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = null;
			synchronized (tableName) {
				st.executeUpdate("REPLACE INTO " + tableName + " (stub) VALUES ('a');");
				rs = st.executeQuery("SELECT LAST_INSERT_ID();");
			}
			if (rs == null || !rs.next())
				return null;
			if (SEQ_1_TABLE.equals(tableName)) {
				return new Long(rs.getInt(1) * 2 - 1);
			} else {
				return new Long(rs.getInt(1) * 2);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if(conn != null) {
				conn.close();
			}
		}
	} 

	@Resource
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Resource
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	private String getTableName() {
		if (Math.random() > 0.5) {
			return SEQ_1_TABLE;
		} else {
			return SEQ_2_TABLE;
		}
	}
	
}
