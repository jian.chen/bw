package com.bw.adv.core.service.impl;

import java.io.Serializable;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.service.BaseService;

/**
 * 业务公共类Service类接口
 */
public abstract class BaseServiceImpl<T> implements BaseService<T> {
	
	public abstract BaseRepository<T, ? extends BaseMapper<T>> getBaseRepository();

	@Override
	public T queryByPk(Serializable pk) {
		return getBaseRepository().findByPk(pk);
	}

	@Override
	public int removeByPk(Serializable pk) {
		return getBaseRepository().removeByPk(pk);
	}

	@Override
	public int save(T t) {
		return getBaseRepository().save(t);
	}

	@Override
	public int saveSelective(T t) {
		return getBaseRepository().saveSelective(t);
	}

	@Override
	public int updateByPkSelective(T t) {
		return getBaseRepository().updateByPkSelective(t);
	}

	@Override
	public int updateByPk(T t) {
		return getBaseRepository().updateByPk(t);
	}

	
//	public List<T> findByPage(Page<T> page) {
//		return getBaseDao().findByPage(page);
//	}
	
}
