package com.bw.adv.core.utils;


import java.util.HashMap;
import java.util.Map;
/**
 * 线程局部变量工具类
 *
 */
public class ThreadLocalUtil {
	
	public static final String LANGX = "langx";
	public static final String SYSTEM_ID = "systemId";
	public static final String CHANNEL_ID = "channelId";
	public static final String PRODUCT_STORE_ID = "productStoreId";
	public static final String MSG_ID = "msgId";
	public static final String CXF_REQUEST_IP = "cxfRequestIp";
	
	private static final ThreadLocal<Map<String, Object>> currentThread = new ThreadLocal<Map<String, Object>>(){
		@Override
		protected Map<String, Object> initialValue() {
			return new HashMap<String, Object>();
		}
	};

	public static void set(String key, Object value){
		currentThread.get().put(key, value);
	}
	
	public static Object get(String key){
		return currentThread.get().get(key);
	}
	
	public static void remove(String key){
		currentThread.get().remove(key);
	}
	
}
