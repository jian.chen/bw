package com.bw.adv.core.service;

import java.io.Serializable;

public interface BaseService<T> {

	T queryByPk(Serializable pk);

	int removeByPk(Serializable pk);

	int save(T t);

	int saveSelective(T t);

	int updateByPkSelective(T t);

	int updateByPk(T t);
	
//	List<T> findByPage(Page<T> page) throws DataAccessException;


}
