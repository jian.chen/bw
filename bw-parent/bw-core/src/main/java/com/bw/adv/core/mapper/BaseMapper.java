package com.bw.adv.core.mapper;

import java.io.Serializable;
import java.util.List;




public interface BaseMapper<T> {
	
	int deleteByPrimaryKey(Serializable pk);

    int insert(T record);

    int insertSelective(T record);
    
    int insertBatch(List<T> recordList);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);
    
    int updateBatch(List<T> recordList);
    
    T selectByPrimaryKey(Serializable pk);
    
    List<T> selectAll();
    
//    List<T> selectByPage(Page<T> page,Object... args);
    
}


