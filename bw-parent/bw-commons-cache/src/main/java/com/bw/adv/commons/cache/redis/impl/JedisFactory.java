package com.bw.adv.commons.cache.redis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import redis.clients.jedis.JedisPool;

@Component
public class JedisFactory{
	private JedisPool  jedisPool  = null;
	
	/**
	 * 获取redisPool
	 * 
	 * @param key
	 * @return
	 */
	@Autowired
	public void  setJedisPool(JedisPool jedisPool) {
		this.jedisPool=jedisPool;
	}

	public JedisPool getJedisPool(){
		return jedisPool;
	}
}
