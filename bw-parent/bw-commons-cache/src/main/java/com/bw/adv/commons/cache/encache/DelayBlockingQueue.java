package com.bw.adv.commons.cache.encache;

import java.util.Collection;
import java.util.Random;
import java.util.concurrent.DelayQueue;
/**
 * 延迟阻塞队列
 * @author klslgy
 *
 */
public class DelayBlockingQueue {

	private DelayQueue<DelayedElement> delayQueue = new DelayQueue<DelayedElement>();

	private static DelayBlockingQueue instance = new DelayBlockingQueue();

	private DelayBlockingQueue() {

	}

	public static DelayBlockingQueue getInstance() {
		return instance;
	}
	
	/**
	 * @param obj
	 */
	public void put(DelayedElement obj) {
		delayQueue.put(obj);
	}
	
	 /**
     * Inserts the specified element into this delay queue.
     *
     * @param e the element to add
     * @return <tt>true</tt> (as specified by {@link Collection#add})
     * @throws NullPointerException if the specified element is null
     */
    public boolean add(DelayedElement obj) {
        return delayQueue.add(obj);
    }
    
    /**
     * Retrieves and removes the head of this queue, waiting if necessary
     * until an element with an expired delay is available on this queue.
     *
     * @return the head of this queue
     * @throws InterruptedException {@inheritDoc}
     * @return
     * @throws Exception
     */
    public DelayedElement take() throws Exception {
        return delayQueue.take();
    }
    
    public int getSize(){
    	return delayQueue.size();
    }
    
    /**
     * Removes a single instance of the specified element from this
     * queue, if it is present, whether or not it has expired.
     */
    public boolean remove(DelayedElement obj) {
    	return delayQueue.remove(obj);
    }
    
    public static void main(String[] args) throws InterruptedException {

        System.out.println("test delay queue.......");
        DelayBlockingQueue delayQueue = DelayBlockingQueue.getInstance();
        
        DelayedElement el4 = new DelayedElement(9000,"111","222","PC");
        DelayedElement el7 = new DelayedElement(9000,"111","223","PC");
        DelayedElement el5 = new DelayedElement(11000,"112","223","PC");
        DelayedElement el6 = new DelayedElement(13000,"113","224","PC");
        DelayedElement el2 = new DelayedElement(5000,"114","225","PC");
        DelayedElement el3 = new DelayedElement(7000,"115","226","PC");
        DelayedElement el1 = new DelayedElement(3000,"116","227","PC");   
        
        DelayedElement elremove = new DelayedElement(9000,"111","225","PC"); 
        
        
        delayQueue.put(el1); 
        delayQueue.put(el2);
        delayQueue.put(el3);
        delayQueue.put(el4);
        delayQueue.put(el5);
        delayQueue.put(el6);
        delayQueue.put(el7);
        DelayedElement poll = null;
        boolean flag = delayQueue.remove(elremove);
        while (true) {
            try {
				poll = delayQueue.take();					
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            System.out.println("poll result \n" + poll);           
        }
//    	 Random r = new Random();
//         int num = r.nextInt(3);
//         System.out.println("num--" + num);
//         int num1 = r.nextInt(3);
//         System.out.println("num--" + num1);
//         int num2 = r.nextInt(3);
//         System.out.println("num--" + num2);
//         int num3 = r.nextInt(3);
//         System.out.println("num--" + num3);
//         int num4 = r.nextInt(3);
//         System.out.println("num--" + num4);
    }
}
