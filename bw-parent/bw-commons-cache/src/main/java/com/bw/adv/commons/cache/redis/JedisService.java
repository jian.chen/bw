package com.bw.adv.commons.cache.redis;

import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.Tuple;
import redis.clients.jedis.ZParams.Aggregate;

/**
 * redis操作
 * @author klslgy
 *
 */
public interface JedisService {
	/**
	 * key不存在返回的结果
	 */
	public static final String nil = "nil";

	/**
	 * OK
	 */
	public static final String OK = "OK";

	/**
	 * ERROR
	 */
	public static final String ERROR = "ERROR";

	/**
	 * Long类型的错误标示
	 */
	public static final Long ERROR_LONG = Long.valueOf(-1);

	/**
	 * Double类型的错误标示
	 */
	public static final Double ERROR_DOUBLE = Double.valueOf(-1);

	/**
	 * llen
	 * 
	 * @param key
	 * @return 0 or -1 or long value
	 */
	Long llen(String key);

	/**
	 * ltrim
	 * 
	 * @param key
	 * @return 成功时返回 OK, 错误时返回 ERROR
	 */
	String ltrim(String key, long start, long end);

	/**
	 * lrem
	 * 
	 * @param key
	 * @param count
	 * @param value
	 * @return
	 */
	Long lrem(String key, long count, String value);

	/**
	 * lrange
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	List<String> lrange(String key, int start, int end);

	/**
	 * lpush
	 * 
	 * @param key
	 * @param value
	 */
	Long lpush(String key, String value);

	/**
	 * lpush list
	 * 
	 * @param key
	 * @param value
	 */
	Long lpush(String key, List<String> value);

	/**
	 * 一致性写入，多服务器间数据保证同步
	 * 
	 * @param key
	 * @param value
	 * @param needCompare
	 */
	Long lpush(String key, String[] value);

	/**
	 * rpop
	 * 
	 * @param key
	 * @param value
	 */
	String rpop(String key);

	/**
	 * set 赋值, 添加到key所在的分片服务器中
	 * 
	 * @param key
	 * @param value
	 * @return 成功返回“OK”, 失败返回“ERROR”
	 */
	String set(String key, String value);
	
	/**
	 * getSet 赋值, 添加到key所在的分片服务器中
	 * 
	 * @param key
	 * @param value
	 * @return 成功返回“OK”, 失败返回“ERROR”
	 */
	String getSet(String key, String value);

	/**
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	String set(byte[] key, byte[] value);

	/**
	 * 查询缓存 当 key 不存在时，返回 nil ，否则，返回 key 的值
	 * 
	 * @param key
	 * @return 如果发生错误，返回"ERROR"
	 */
	String get(String key);
	
	byte[] get(byte[] key );

	/**
	 * key对应的value 自增
	 * 
	 * @param key
	 * @return 成功
	 */
	public Long incr(String key);

	/**
	 * 删除缓存 删除给定的一个或多个 key 。不存在的 key 会被忽略。
	 * 
	 * @param key
	 * @return 被删除 key 的数量。错误返回 -1
	 */
	Long del(String key);

	/**
	 * expire
	 * 
	 * @param key
	 * @param seconds
	 * @return
	 */
	Long expire(String key, int seconds);

	/**
	 * setnx
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	long setnx(String key, String value);

	/**
	 * ZADD
	 * 
	 * @param key
	 * @param score
	 * @param member
	 */
	Long zadd(String key, double score, String member);

	/**
	 * ZADD
	 * 
	 * @param key
	 * @param scoreMembers
	 */
	Long zadd(String key,  Map<String, Double> scoreMembers);

	/**
	 * zrem
	 * 
	 * @param key
	 * @param members
	 * @return
	 */
	Long zrem(String key, String... members);

	/**
	 * zcard
	 * 
	 * @param key
	 * @param members
	 * @return
	 */
	Long zcard(String key);

	/**
	 * zcount
	 * 
	 * @param key
	 * @return
	 */
	Long zcount(String key, double min, double max);

	/**
	 * zscore
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	Double zscore(String key, String member);

	/**
	 * zincrby
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	Double zincrby(String key, Double inc, String member);

	/**
	 * zrange
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Set<String> zrange(String key, long start, long end);

	/**
	 * zrevrange
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Set<String> zrevrange(String key, long start, long end);

	/**
	 * zrangebyscore
	 * 
	 * @param key
	 * @param min
	 * @param max
	 * @param offset
	 * @param count
	 * @return
	 */
	Set<String> zrangeByScore(String key, double min, double max, int offset,
			int count);

	/**
	 * zrangebyscore
	 * 
	 * @param key
	 * @param min
	 * @param max
	 * @return
	 */
	Set<String> zrangeByScore(String key, double min, double max);

	/**
	 * zrangeWithScores
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Set<Tuple> zrangeWithScores(String key, long start, long end);

	/**
	 * zrangeWithScoresByConfirmKey
	 * 
	 * @param key
	 * @param confirmKey
	 * @param start
	 * @param end
	 * @return
	 */
	Set<Tuple> zrangeWithScoresByConfirmKey(String key, String confirmKey,
			long start, long end);

	/**
	 * zrevrangeByScore
	 * 
	 * @param key
	 * @param min
	 * @param max
	 * @param offset
	 * @param count
	 * @return
	 */
	Set<String> zrevrangeByScore(String key, double max, double min,
			int offset, int count);

	/**
	 * zrevrangeByScoreWithScores
	 * 
	 * @param key
	 * @param min
	 * @param max
	 * @param offset
	 * @param count
	 * @return
	 */
	Set<Tuple> zrevrangeByScoreWithScores(String key, double max, double min,
			int offset, int count);

	/**
	 * zrevrangeWithScores
	 * 
	 * @param key
	 * @param start
	 * @param emd
	 * @return
	 */
	Set<Tuple> zrevrangeWithScores(String key, long start, long end);

	/**
	 * zrevrangeWithScoresByConfirmKey
	 * 
	 * @param key
	 * @param confirmKey
	 * @param start
	 * @param emd
	 * @return
	 */
	Set<Tuple> zrevrangeWithScoresByConfirmKey(String key, String confirmKey,
			long start, long end);

	/**
	 * zrank
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	Long zrank(String key, String member);

	/**
	 * zrevrank
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	Long zrevrank(String key, String member);

	/**
	 * zremrangeByRank
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Long zremrangeByRank(String key, long start, long end);

	/**
	 * zremrangeByScore
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Long zremrangeByScore(String key, double start, double end);

	/**
	 * to check if one key is existing in redis
	 * 
	 * @param key
	 * @return
	 */
	boolean exists(String key);

	/**
	 * zunionstore
	 * 
	 * @param dstkey
	 * @param sets
	 * @return
	 */
	Long zunionstore(String dstkey, String... sets);

	/**
	 * zinterstore
	 * 
	 * @param dstkey
	 * @param confirmKey
	 * @param sets
	 * @return
	 */
	Long zinterstore(String dstkey, String confirmKey, String... sets);

	/**
	 * zinterstoreByAggregate
	 * 
	 * @param dstkey
	 * @param confirmKey
	 * @param aggregate
	 * @param sets
	 * @return
	 */
	Long zinterstoreByAggregate(String dstkey, String confirmKey,
			Aggregate aggregate, String... sets);

	/**
	 * hset
	 * 
	 * @param key
	 * @param filed
	 * @param value
	 * @return
	 */
	Long hset(String key, String field, String value);

	boolean hexists(String key, String field);

	/**
	 * hmset
	 * 
	 * @param key
	 * @param fields
	 * @return
	 */
	String hmset(String key, Map<String, String> fields);

	/**
	 * hget
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	String hget(String key, String field);

	/**
	 * 
	 * @author V
	 * @param key
	 * @param field
	 * @return
	 * @since JDK 1.6
	 */
	byte[] hget(byte[] key, byte[] field);

	/**
	 * hget
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	List<String> hmget(String key, String... fields);

	/**
	 * hgetAll
	 * 
	 * @param key
	 * @param fields
	 * @return
	 */
	Map<String, String> hgetAll(String key);

	/**
	 * 
	 * @author V
	 * @param key
	 * @return
	 * @since JDK 1.6
	 */
	Map<byte[], byte[]> hgetAll(byte[] key);

	/**
	 * hdel
	 * 
	 * @param key
	 * @param fields
	 * @return
	 */
	Long hdel(String key, String... fields);

	/**
	 * hlen
	 * 
	 * @param key
	 * @return
	 */
	Long hlen(String key);

	/**
	 * hincrBy
	 * 
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	Long hincrBy(String key, String field, long value);

	/**
	 * hkeys
	 * 
	 * @param key
	 * @return
	 */
	Set<String> hkeys(String key);

	/**
	 * hvals
	 * 
	 * @param key
	 * @return
	 */
	List<String> hvals(String key);

	Long sadd(String key, String value);

	Long sadd(String key, String... value);

	Long srem(String key, String value);

	Set<String> smembers(String key);
	
    public String lpop(String key);	
    
    public Long sort(String key,String order);

}
