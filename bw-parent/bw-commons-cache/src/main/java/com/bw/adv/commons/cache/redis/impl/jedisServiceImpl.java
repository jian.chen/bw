package com.bw.adv.commons.cache.redis.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.bw.adv.commons.cache.redis.JedisService;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Tuple;
import redis.clients.jedis.ZParams.Aggregate;

/**
 * redis实现类
 * @author klslgy
 *
 */
@Component
public class jedisServiceImpl implements JedisService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private JedisFactory jedisFactory;

	public Long del(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.del(key);
		} catch (Exception e) {
			logger.error("redis del error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR_LONG;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}
	public String set(String key, String value) {
		JedisPool pool = jedisFactory.getJedisPool();
		String result = OK;
		Jedis je = pool.getResource();
		try {
			result = je.set(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("redis set error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}
	
	@Override
	public String set(byte[] key, byte[] value) {
		JedisPool pool = jedisFactory.getJedisPool();
		String result = OK;
		Jedis je = pool.getResource();
		try {
			result = je.set(key, value);
		} catch (Exception e) {
			logger.error("redis set error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public String get(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		Jedis je = pool.getResource();
		try {
			String t = je.get(key);
			return t;
		} catch (Exception e) {
			logger.error("redis get error::",this.getStackTrace(e), je.getClient().getHost());
			return null;
		} finally {
			if (je != null) {
				je.close();
			}
		}
	}
	
	public Long expire(String key, int seconds) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.expire(key, seconds);
		} catch (Exception e) {
			logger.error("redis expire error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR_LONG;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	/**
	 * 分布式锁使用
	 */
	public long setnx(String key, String value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.setnx(key, value);
		} catch (Exception e) {
			logger.error("redis setnx error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR_LONG;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zadd(String key, double score, String member) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zadd(key, score, member);
		} catch (Exception e) {
			logger.error("redis zadd error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR_LONG;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zadd(String key, Map<String, Double> scoreMembers) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zadd(key,scoreMembers);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zrem(String key, String... members) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrem(key,members);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zcard(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zcard(key);
			return result;
		} catch (Exception e) {
			logger.error("redis zcard error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR_LONG;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}
	public Long zremrangeByRank(String key, long start, long end) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zremrangeByRank(key, start, end);
		} catch (Exception e) {
			logger.error("redis zremrangeByRank error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR_LONG;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}
	public Long hset(String key, String field, String value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hset(key, field, value);
		} catch (Exception e) {
			logger.error("redis hset error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR_LONG;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}


	public String hmset(String key, Map<String, String> fields) {
		JedisPool pool = jedisFactory.getJedisPool();
		String result = OK;
		Jedis je = pool.getResource();
		try {
			result = je.hmset(key, fields);
		} catch (Exception e) {
			logger.error("redis hmget error::",this.getStackTrace(e), je.getClient().getHost());
			result = ERROR;
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public String hget(String key, String field) {
		JedisPool pool = jedisFactory.getJedisPool();
		String result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hget(key, field);
			return result;
		} catch (Exception e) {
			logger.error(
					"redis hget error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public List<String> hmget(String key, String... fields) {
		JedisPool pool = jedisFactory.getJedisPool();
		List<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hmget(key, fields);
			return result;
		} catch (Exception e) {
			logger.error("redis hmget error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Map<String, String> hgetAll(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		Map<String, String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hgetAll(key);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}
	

	public Long incr(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.incr(key);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long llen(String key) {
		throw new UnsupportedOperationException();
	}

	public String ltrim(String key, long start, long end) {
		JedisPool pool = jedisFactory.getJedisPool();
		String result = null;
		Jedis je = pool.getResource();
		try {
			result = je.ltrim(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long lrem(String key, long count, String value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.lrem(key,count,value);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public List<String> lrange(String key, int start, int end) {
		JedisPool pool = jedisFactory.getJedisPool();
		List<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.lrange(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long lpush(String key, String value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.lpush(key, value);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long lpush(String key, List<String> value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.lpush(key,value.toArray(new String[]{}));
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long lpush(String key, String[] value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.lpush(key,value);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public String rpop(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		String result = null;
		Jedis je = pool.getResource();
		try {
			result = je.rpop(key);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	
	public Long zcount(String key, double min, double max) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zcount(key,min,max);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Double zscore(String key, String member) {
		JedisPool pool = jedisFactory.getJedisPool();
		Double result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zscore(key,member);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Double zincrby(String key, Double inc, String member) {
		JedisPool pool = jedisFactory.getJedisPool();
		Double result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zincrby(key,inc,member);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<String> zrange(String key, long start, long end) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrange(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<String> zrevrange(String key, long start, long end) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrevrange(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<String> zrangeByScore(String key, double min, double max,
			int offset, int count) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrangeByScore(key,min,max,offset,count);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<String> zrangeByScore(String key, double min, double max) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrangeByScore(key,min,max);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<Tuple> zrangeWithScores(String key, long start, long end) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<Tuple> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrangeWithScores(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<Tuple> zrangeWithScoresByConfirmKey(String key,
			String confirmKey, long start, long end) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<Tuple> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrangeWithScores(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<String> zrevrangeByScore(String key, double max, double min,
			int offset, int count) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrevrangeByScore(key,max,min,offset,count);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<Tuple> zrevrangeByScoreWithScores(String key, double max,
			double min, int offset, int count) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<Tuple> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrevrangeByScoreWithScores(key,max,min,offset,count);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<Tuple> zrevrangeWithScores(String key, long start, long end) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<Tuple> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrevrangeWithScores(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<Tuple> zrevrangeWithScoresByConfirmKey(String key,
			String confirmKey, long start, long end) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<Tuple> result = null;
		Jedis je = pool.getResource();
		try {
			//result = je.zrevrangeWithScoresByConfirmKey(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zrank(String key, String member) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrank(key,member);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zrevrank(String key, String member) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zrevrank(key,member);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	
	public Long zremrangeByScore(String key, double start, double end) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zremrangeByScore(key,start,end);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public boolean exists(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		boolean result = false;
		Jedis je = pool.getResource();
		try {
			result = je.exists(key);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zunionstore(String dstkey, String... sets) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zunionstore(dstkey,sets);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zinterstore(String dstkey, String confirmKey, String... sets) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.zinterstore(dstkey,sets);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long zinterstoreByAggregate(String dstkey, String confirmKey,
			Aggregate aggregate, String... sets) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	
	public boolean hexists(String key, String field) {
		JedisPool pool = jedisFactory.getJedisPool();
		boolean result = false;
		Jedis je = pool.getResource();
		try {
			result = je.hexists(key,field);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}


	public Long hdel(String key, String... fields) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hdel(key,fields);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long hlen(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hlen(key);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long hincrBy(String key, String field, long value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hincrBy(key,field,value);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<String> hkeys(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hkeys(key);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public List<String> hvals(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		List<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.hvals(key);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long sadd(String key, String value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.sadd(key,value);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long sadd(String key, String... value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.sadd(key,value);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Long srem(String key, String value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Long result = null;
		Jedis je = pool.getResource();
		try {
			result = je.srem(key,value);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}

	public Set<String> smembers(String key) {
		JedisPool pool = jedisFactory.getJedisPool();
		Set<String> result = null;
		Jedis je = pool.getResource();
		try {
			result = je.smembers(key);
			return result;
		} catch (Exception e) {
			logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
		} finally {
			if (je != null) {
				je.close();
			}
		}
		return result;
	}
	
    @Override
    public byte[] hget(byte[] key, byte[] field) {
        JedisPool pool = jedisFactory.getJedisPool();
        byte[] result = null;
        Jedis je = pool.getResource();
        try {
            result = je.hget(key, field);
            return result;
        } catch (Exception e) {
            logger.error(
                    "redis hget error::",this.getStackTrace(e), je.getClient().getHost());
        } finally {
            if (je != null) {
                je.close();
            }
        }
        return result;
    }
    
    @Override
    public Map<byte[], byte[]> hgetAll(byte[] key) {
        JedisPool pool = jedisFactory.getJedisPool();
        Map<byte[], byte[]> result = null;
        Jedis je = pool.getResource();
        try {
            result = je.hgetAll(key);
            return result;
        } catch (Exception e) {
            logger.error("redis hgetAll error::",this.getStackTrace(e), je.getClient().getHost());
        } finally {
            if (je != null) {
                je.close();
            }
        }
        return result;
    }
	
	@Override
	public String lpop(String key) {
        JedisPool pool = jedisFactory.getJedisPool();
        String result = null;
        Jedis je = pool.getResource();
        try {
            result = je.lpop(key);
            return result;
        } catch (Exception e) {
            logger.error("redis blpop error::",this.getStackTrace(e), je.getClient().getHost());
        } finally {
            if (je != null) {
                je.close();
            }
        }
        return result;
    }
	
    @Override
    public byte[] get(byte[] key) {
        JedisPool pool = jedisFactory.getJedisPool();
        Jedis je = pool.getResource();
        try {
            byte[] t = je.get(key);
            return t;
        } catch (Exception e) {
            logger.error("redis get error::",this.getStackTrace(e), je.getClient().getHost());
            return null;
        } finally {
            if (je != null) {
                je.close();
            }
        }
    }
	
    
    /**
	 * 取出exception中的信息
	 * 
	 * @param exception
	 * @return
	 */
	private static String getStackTrace(Throwable exception) {
		StringWriter sw = null;
		PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			exception.printStackTrace(pw);
			return sw.toString();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}
	
	public static void main(String aggs[]) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml"); // 实例化
		jedisServiceImpl service = (jedisServiceImpl) applicationContext.getBean(jedisServiceImpl.class);
		// service.getUserFollowedZhubo("dzhrebouser2", false);
		service.set("liuwen", "hello gis!");
		System.out.println(service.get("liuwen"));
	}
	
	@Override
	public String getSet(String key, String value) {
		JedisPool pool = jedisFactory.getJedisPool();
		Jedis je = pool.getResource();
		try {
			String t = je.getSet(key,value);
			return t;
		} catch (Exception e) {
			logger.error("redis getSet error::",this.getStackTrace(e), je.getClient().getHost());
			return null;
		} finally {
			if (je != null) {
				je.close();
			}
		}
	}
	@Override
	public Long sort(String key, String dstkey) {
		JedisPool pool = jedisFactory.getJedisPool();
		Jedis je = pool.getResource();
		try {
			Long sort = je.sort(key, dstkey);
			return sort;
		} catch (Exception e) {
			logger.error("redis getSet error::",this.getStackTrace(e), je.getClient().getHost());
			return null;
		} finally {
			if (je != null) {
				je.close();
			}
		}	
	}
}
