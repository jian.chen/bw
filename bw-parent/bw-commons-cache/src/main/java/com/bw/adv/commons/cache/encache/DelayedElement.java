package com.bw.adv.commons.cache.encache;

import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * LiuWen <liuwen@gujinsuo.com.cn>
 * 延期元素
 * @author klslgy
 *
 */
public class DelayedElement implements Delayed {
    
    private final long delay;

    private final long expire;
    
    //原交易流水号
    private String squence;
    
    //交易码
    private String transCode;   
    
   
    //应用标识
    private String appId;

    /**
     * 延期时间，毫秒
     * @param delay
     */
    public DelayedElement(long delay) {
    	super();
        this.delay = delay;
        expire = new Date().getTime() + delay;
    }
    
    /**
     * 延期时间，毫秒
     * @param delay
     */
    public DelayedElement(long delay,String squence,String transCode,String appId) {
    	super();
    	this.squence=squence;
    	this.transCode=transCode;
        this.delay = delay;
        this.appId = appId;
        expire = new Date().getTime() + delay;
    }

    @Override
    public long getDelay(TimeUnit unit) {
    	Date now = new Date();
    	long diff = expire - now.getTime();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
    	long result = this.getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
        if (result < 0) {
              return -1;
           } else if (result > 0) {
                   return 1;
           } else {
         return 0;
       }
   }

    @Override
	public String toString() {
		return "DelayedElement [delay=" + delay
				+ ", squence=" + squence + ", transCode=" + transCode
				+ ", appId=" + appId + "]";
	}

	public String getSquence() {
		return squence;
	}

	public void setSquence(String squence) {
		this.squence = squence;
	}

	public String getTransCode() {
		return transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}
	
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((squence == null) ? 0 : squence.hashCode());
		result = prime * result + ((transCode == null) ? 0 : transCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DelayedElement other = (DelayedElement) obj;
		if (squence == null) {
			if (other.squence != null)
				return false;
		} else if (!squence.equals(other.squence))
			return false;
		if (transCode == null) {
			if (other.transCode != null)
				return false;
		} else if (!transCode.equals(other.transCode))
			return false;
		return true;
	}    
    
	
}


