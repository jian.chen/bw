package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.RoleType;

public interface RoleTypeMapper {
    int deleteByPrimaryKey(Long roleTypeId);

    int insert(RoleType record);

    int insertSelective(RoleType record);

    RoleType selectByPrimaryKey(Long roleTypeId);

    int updateByPrimaryKeySelective(RoleType record);

    int updateByPrimaryKey(RoleType record);
}