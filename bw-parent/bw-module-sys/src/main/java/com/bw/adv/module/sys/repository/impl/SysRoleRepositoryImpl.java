/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysRoleRepositoryImpl.java
 * Package Name:com.sage.scrm.module.sys.repository.impl
 * Date:2015年8月17日上午11:01:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.sys.mapper.SysRoleMapper;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.exp.SysRoleExp;
import com.bw.adv.module.sys.repository.SysRoleRepository;

/**
 * ClassName:SysRoleRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 上午11:01:21 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SysRoleRepositoryImpl extends BaseRepositoryImpl<SysRole, SysRoleMapper> implements SysRoleRepository {

	@Override
	protected Class<SysRoleMapper> getMapperClass() {
		
		return SysRoleMapper.class;
	}

	@Override
	public List<SysRole> findByFlag(String useFlag) {
		
		return this.getMapper().selectByFlag(useFlag);
	}
	
	@Override
	public List<SysRole> findSysRoleList(String useFlag,Page<SysRole> page) {
		return this.getMapper().selectSysRoleList(useFlag,page);
	}
	
	@Override
	public List<SysRole> findSysRoleListByUserId(Long sysUserId) {
		return this.getMapper().selectSysRoleListByUserId(sysUserId);
	}
	
	@Override
	public SysRoleExp findSysFunctionOperationListByPrimaryKey(Long sysRoleId) {
		return this.getMapper().selectSysFunctionOperationListByPrimaryKey(sysRoleId);
	}

	@Override
	public void removeByRoleId(Long roleId,String stopDate) {
		this.getMapper().deleteByRoleId(roleId,stopDate);
	}

	@Override
	public List<SysRole> findSysRoleListBySysRoleName(String useFlag,Long roleId) {
		return this.getMapper().selectSysRoleListBySysRoleName(useFlag,roleId);
	}

	@Override
	public SysRole findByRoleName(String roleName) {
		return this.getMapper().selectByRoleName(roleName);
	}


}

