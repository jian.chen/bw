package com.bw.adv.module.sys.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.SysRoleDataMapper;
import com.bw.adv.module.sys.model.SysRoleData;

import java.util.List;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public interface SysRoleDataRepository extends BaseRepository<SysRoleData, SysRoleDataMapper>{

    public List<SysRoleData> queryByRoleId(Long roleId);

    public List<SysRoleData> queryByUserId(Long userId);

    public boolean deleteByRoleId(Long roleId);

    public boolean batchInsert(List<SysRoleData> sysRoleDatas);

}
