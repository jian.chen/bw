package com.bw.adv.module.sys.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.WechatAccountMapper;
import com.bw.adv.module.sys.repository.WechatAccountRepository;
import com.bw.adv.module.wechat.model.WechatAccount;
import com.bw.adv.module.wechat.model.WechatAccountConfig;
import com.bw.adv.module.wechat.model.exp.WechatAccountExp;

/**
 * 
* @ClassName: WechatAccountRepositoryImpl
* @Description: 微信账号信息RepositoryImpl
* @author A18ccms a18ccms_gmail_com
* @date 2015年8月13日 下午5:11:25
*
 */
@Repository
public class WechatAccountRepositoryImpl extends BaseRepositoryImpl<WechatAccount, WechatAccountMapper> implements WechatAccountRepository{

	@Override
	protected Class<WechatAccountMapper> getMapperClass() {
		return WechatAccountMapper.class;
	}

	@Override
	public WechatAccountExp findExpByPk(Long wechatAccountId) {
		
		return this.getMapper().selectExpByPk(wechatAccountId);
	}

	@Override
	public List<WechatAccountExp> findExp() {
		
		return this.getMapper().selectExp();
	}


}
