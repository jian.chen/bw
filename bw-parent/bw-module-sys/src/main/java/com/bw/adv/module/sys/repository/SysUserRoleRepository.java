/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysUserRoleRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015年8月17日下午6:09:49
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.repository;


import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.SysUserRoleMapper;
import com.bw.adv.module.sys.model.SysUserRoleKey;

/**
 * ClassName:SysUserRoleRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午6:09:49 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SysUserRoleRepository extends BaseRepository<SysUserRoleKey, SysUserRoleMapper> {
	
	/**
	 * 
	 * findByUserId:根据用户id查询用户的角色
	 * Date: 2015年9月2日 上午10:52:20 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param userId
	 * @return
	 */
	public List<SysUserRoleKey> findByUserId(Long userId);
	
	/**
	 * 
	 * removeByUserId:根据用户Id删除用户角色信息
	 * Date: 2015年9月2日 上午10:52:27 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param userId
	 * @return
	 */
	public int removeByUserId(Long userId);


	/**
	 * 根据角色id查询
	 * @param roleIde
	 * @return
	 */
	public List<SysUserRoleKey> findByRoleId(long roleIde);
}

