package com.bw.adv.module.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sys.model.SysLog;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.exp.SysLogExp;

public interface SysLogMapper extends BaseMapper<SysLog>{
	
	List<SysLogExp> selectByExample(@Param("example")Example example,Page<SysLogExp> page);
	
	List<SysUser> selectAllUser();
	
}