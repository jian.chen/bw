/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysRoleRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015年8月17日上午10:54:10
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sys.mapper.SysLogMapper;
import com.bw.adv.module.sys.model.SysLog;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.exp.SysLogExp;


/**
 * ClassName: SysLogRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-18 下午4:22:22 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface SysLogRepository extends BaseRepository<SysLog, SysLogMapper> {

	/**
	 * 
	 * findByExample:根据条件查询日志List
	 * Date: 2015年9月2日 下午3:06:40 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	public List<SysLogExp> findByExample(Example example,Page<SysLogExp> page);
	
	/**
	 * 
	 * findAllUser:查询数据库中所有系统用户(包括已逻辑删除的)
	 * Date: 2015年9月4日 下午7:31:04 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<SysUser> findAllUser();
	
}


