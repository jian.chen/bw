package com.bw.adv.module.sys.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sys.model.SysLicense;

public interface SysLicenseMapper extends BaseMapper<SysLicense>{
	
	/**
	 * 获取当前系统的激活信息
	 * @return
	 */
	SysLicense getActiveInfo();
	
}