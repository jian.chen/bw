package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.SysRoleDimLevelValuesKey;

public interface SysRoleDimLevelValuesMapper {
    int deleteByPrimaryKey(SysRoleDimLevelValuesKey key);

    int insert(SysRoleDimLevelValuesKey record);

    int insertSelective(SysRoleDimLevelValuesKey record);
}