package com.bw.adv.module.sys.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.SysRoleDataTypeMapper;
import com.bw.adv.module.sys.model.SysRoleDataType;
import com.bw.adv.module.sys.repository.SysRoleDataTypeRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
@Repository
public class SysRoleDataTypeRepositoryImpl extends BaseRepositoryImpl<SysRoleDataType, SysRoleDataTypeMapper> implements SysRoleDataTypeRepository {

    @Override
    protected Class<SysRoleDataTypeMapper> getMapperClass() {
        return SysRoleDataTypeMapper.class;
    }

    @Override
    public List<SysRoleDataType> findAll() {
        return this.getMapper().findAll();
    }

}
