package com.bw.adv.module.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.exp.SysRoleExp;


public interface SysRoleMapper extends BaseMapper<SysRole> {
	
	List<SysRole> selectByFlag(String useFlag);
	
	List<SysRole> selectSysRoleList(@Param("useFlag")String useFlag,Page<SysRole> page);
	
	List<SysRole> selectSysRoleListByUserId(@Param("sysUserId") Long sysUserId);
	
	List<SysRole> selectSysRoleListBySysRoleName(@Param("useFlag")String useFlag,@Param("roleId")Long roleId);
	
	SysRoleExp selectSysFunctionOperationListByPrimaryKey(Long sysRoleId);
	
	void deleteByRoleId(@Param("roleId")Long roleId,@Param("stopDate")String stopDate);

	SysRole selectByRoleName(@Param("roleName") String roleName);
}