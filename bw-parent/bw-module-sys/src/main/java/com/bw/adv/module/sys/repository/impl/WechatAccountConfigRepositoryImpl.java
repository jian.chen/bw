package com.bw.adv.module.sys.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.WechatAccountConfigMapper;
import com.bw.adv.module.sys.repository.WechatAccountConfigRepository;
import com.bw.adv.module.wechat.model.WechatAccountConfig;

@Repository
public class WechatAccountConfigRepositoryImpl extends BaseRepositoryImpl<WechatAccountConfig, WechatAccountConfigMapper> implements WechatAccountConfigRepository {

	@Override
	protected Class<WechatAccountConfigMapper> getMapperClass() {
		return WechatAccountConfigMapper.class;
	}
	
	@Override
    public int updateWechatConnectStatus(WechatAccountConfig wechatAccountConfig) {
        return getMapper().updateWechatConnectStatus(wechatAccountConfig);
    }

	@Override
	public WechatAccountConfig findByWechatAccountId(Long wechatAccountId) {
		
		return this.getMapper().selectByWechatAccountId(wechatAccountId);
	}

}
