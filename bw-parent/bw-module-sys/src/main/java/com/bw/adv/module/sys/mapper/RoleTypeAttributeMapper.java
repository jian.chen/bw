package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.RoleTypeAttribute;

public interface RoleTypeAttributeMapper {
    int deleteByPrimaryKey(Long roleTypeAttributeId);

    int insert(RoleTypeAttribute record);

    int insertSelective(RoleTypeAttribute record);

    RoleTypeAttribute selectByPrimaryKey(Long roleTypeAttributeId);

    int updateByPrimaryKeySelective(RoleTypeAttribute record);

    int updateByPrimaryKey(RoleTypeAttribute record);
}