package com.bw.adv.module.sys.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.SysLicenseMapper;
import com.bw.adv.module.sys.model.SysLicense;

public interface SysLicenseRepository extends BaseRepository<SysLicense, SysLicenseMapper>{
	
	/**
	 * 获取系统当前激活信息
	 * @return
	 */
	public SysLicense getActiveInfo();
	
}
