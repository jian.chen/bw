package com.bw.adv.module.sys.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatAccountConfig;

/**
 * 
 * ClassName: WechatAccountConfigMapper <br/>
 * Function: 微信账号配置信息
 * date: 2015年8月14日 上午10:01:37 <br/>
 * scrmVersion 1.0
 * @author chuanxue.wei
 * @version jdk1.7
 */
public interface WechatAccountConfigMapper extends BaseMapper<WechatAccountConfig> {
	/**
	 * 
	 * selectByWechatAccountId:查询微信账号是否存在配置信息
	 * Date: 2015年8月14日 上午11:27:15 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param wechatAccountId
	 * @return
	 */
	WechatAccountConfig selectByWechatAccountId(Long wechatAccountId);
	
	/**
	 * 更新微信连接状态
	 * @param wechatAccountConfig
	 * @return
	 */
	public int updateWechatConnectStatus(WechatAccountConfig wechatAccountConfig);
	
}