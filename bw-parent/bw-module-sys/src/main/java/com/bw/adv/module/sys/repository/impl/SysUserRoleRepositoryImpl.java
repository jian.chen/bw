/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysUserRoleRepositoryImpl.java
 * Package Name:com.sage.scrm.module.sys.repository.impl
 * Date:2015年8月17日下午6:11:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.SysUserRoleMapper;
import com.bw.adv.module.sys.model.SysUserRoleKey;
import com.bw.adv.module.sys.repository.SysUserRoleRepository;

/**
 * ClassName:SysUserRoleRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午6:11:19 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class SysUserRoleRepositoryImpl extends BaseRepositoryImpl<SysUserRoleKey, SysUserRoleMapper> implements SysUserRoleRepository {

	@Override
	protected Class<SysUserRoleMapper> getMapperClass() {
		
		return SysUserRoleMapper.class;
	}

	@Override
	public List<SysUserRoleKey> findByUserId(Long userId) {
		
		return this.getMapper().selectByUserId(userId);
	}

	@Override
	public int removeByUserId(Long userId) {
		
		return this.getMapper().deleteByUserId(userId);
	}

	@Override
	public List<SysUserRoleKey> findByRoleId(long roleId) {
		return this.getMapper().selectByRoleId(roleId);
	}
}

