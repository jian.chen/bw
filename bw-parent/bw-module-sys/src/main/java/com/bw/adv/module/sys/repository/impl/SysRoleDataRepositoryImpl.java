package com.bw.adv.module.sys.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.SysRoleDataMapper;
import com.bw.adv.module.sys.model.SysRoleData;
import com.bw.adv.module.sys.repository.SysRoleDataRepository;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
@Repository
public class SysRoleDataRepositoryImpl extends BaseRepositoryImpl<SysRoleData, SysRoleDataMapper> implements SysRoleDataRepository {

    @Override
    protected Class<SysRoleDataMapper> getMapperClass() {
        return SysRoleDataMapper.class;
    }

    @Override
    public List<SysRoleData> queryByRoleId(Long roleId) {
        if(roleId == null || roleId <= 0)
            throw new IllegalArgumentException("SysOrgDataRepositoryImpl: queryByOrgId argument[roleId] is null");
        return getMapper().selectByRoleId(roleId);
    }

    @Override
    public List<SysRoleData> queryByUserId(Long userId) {
        if(userId == null || userId <= 0)
            throw new IllegalArgumentException("SysOrgDataRepositoryImpl: queryByUserId argument[userId] is null");
        return getMapper().selectByUserId(userId);
    }

    @Override
    public boolean deleteByRoleId(Long roleId) {
        if(roleId == null || roleId <= 0)
            throw new IllegalArgumentException("SysOrgDataRepositoryImpl: deleteByRoleId argument[roleId] is null");
        return getMapper().deleteByRoleId(roleId)>0;
    }

    @Override
    public boolean batchInsert(List<SysRoleData> sysRoleDatas) {
        if(CollectionUtils.isEmpty(sysRoleDatas))
            throw new IllegalArgumentException("SysOrgDataRepositoryImpl: batchInsert argument[sysRoleDatas] is null");
        return getMapper().batchInsert(sysRoleDatas)>0;
    }
}
