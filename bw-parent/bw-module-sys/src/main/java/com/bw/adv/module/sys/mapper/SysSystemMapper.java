package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.SysSystem;

public interface SysSystemMapper {
    int deleteByPrimaryKey(Long systemId);

    int insert(SysSystem record);

    int insertSelective(SysSystem record);

    SysSystem selectByPrimaryKey(Long systemId);

    int updateByPrimaryKeySelective(SysSystem record);

    int updateByPrimaryKey(SysSystem record);
}