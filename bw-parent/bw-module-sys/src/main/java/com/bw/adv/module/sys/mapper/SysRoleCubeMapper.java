package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.SysRoleCubeKey;

public interface SysRoleCubeMapper {
    int deleteByPrimaryKey(SysRoleCubeKey key);

    int insert(SysRoleCubeKey record);

    int insertSelective(SysRoleCubeKey record);
}