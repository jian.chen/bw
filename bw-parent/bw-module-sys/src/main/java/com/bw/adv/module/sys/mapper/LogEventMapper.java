package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.LogEvent;

public interface LogEventMapper {
    int deleteByPrimaryKey(Long logEventId);

    int insert(LogEvent record);

    int insertSelective(LogEvent record);

    LogEvent selectByPrimaryKey(Long logEventId);

    int updateByPrimaryKeySelective(LogEvent record);

    int updateByPrimaryKey(LogEvent record);
}