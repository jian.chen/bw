package com.bw.adv.module.sys.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.wechat.model.WechatAccount;
import com.bw.adv.module.wechat.model.exp.WechatAccountExp;


public interface WechatAccountMapper extends BaseMapper<WechatAccount> {
	
	public WechatAccountExp selectExpByPk(Long wechatAccountId);
	
	public List<WechatAccountExp> selectExp();
	
}