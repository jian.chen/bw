/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysRoleRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015年8月17日上午10:54:10
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.repository;

import java.util.List;






import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.sys.mapper.SysRoleMapper;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.exp.SysRoleExp;


/**
 * ClassName:SysRoleRepository <br/>
 * Function: 角色权限
 * Date: 2015年8月17日 上午10:54:10 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface SysRoleRepository extends BaseRepository<SysRole, SysRoleMapper> {

	/**
	 * 
	 * findByFlag:根据角色状态查询角色list
	 * Date: 2015年9月2日 下午2:43:57 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param useFlag
	 * @return
	 */
	public List<SysRole> findByFlag(String useFlag);
	
	public List<SysRole> findSysRoleList(String useFlag,Page<SysRole> page);
	
	public List<SysRole> findSysRoleListByUserId(Long sysUserId);
	
	public SysRoleExp findSysFunctionOperationListByPrimaryKey(Long sysRoleId);
	
	public void removeByRoleId(Long roleId,String stopDate);
	
	public List<SysRole> findSysRoleListBySysRoleName(String useFlag,Long roleId);

	public SysRole findByRoleName(String roleName);
}


