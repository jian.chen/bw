package com.bw.adv.module.sys.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sys.model.OperationItem;

public interface OperationItemMapper extends BaseMapper<OperationItem>  {
    int deleteByPrimaryKey(Long operationItemId);

    int insert(OperationItem record);

    int insertSelective(OperationItem record);

    OperationItem selectByPrimaryKey(Long operationItemId);

    int updateByPrimaryKeySelective(OperationItem record);

    int updateByPrimaryKey(OperationItem record);
    
    List<OperationItem> selectOperationItemList();
}