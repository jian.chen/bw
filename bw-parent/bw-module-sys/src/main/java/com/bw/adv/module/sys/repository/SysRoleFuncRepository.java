/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysFunctionRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015-8-17下午4:35:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.sys.repository;



import java.util.List;
import java.util.Map;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.SysRoleFuncMapper;
import com.bw.adv.module.sys.model.SysRoleFuncKey;

/**
 * ClassName:SysFunctionRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-17 下午4:35:19 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
public interface SysRoleFuncRepository extends BaseRepository<SysRoleFuncKey, SysRoleFuncMapper> {

	public List<Map<String,Object>> findCheckedSysRoleFuncList(Long roleId);

	/**
	 * 查询角色选中的权限
	 * @param roleId
	 * @return
	 */
	public List<SysRoleFuncKey> selectCheckedSysRoleFuncLists(Long roleId);

	public void removeByRoleId(Long roleId);

	public List<SysRoleFuncKey> findByUserId(long userId);
}

