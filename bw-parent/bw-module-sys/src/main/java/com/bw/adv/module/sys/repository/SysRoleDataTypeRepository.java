package com.bw.adv.module.sys.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.SysRoleDataTypeMapper;
import com.bw.adv.module.sys.model.SysRoleDataType;

import java.util.List;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public interface SysRoleDataTypeRepository extends BaseRepository<SysRoleDataType, SysRoleDataTypeMapper>{

    public List<SysRoleDataType> findAll();
}
