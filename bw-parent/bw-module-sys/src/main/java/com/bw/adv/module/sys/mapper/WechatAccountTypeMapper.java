package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.wechat.model.WechatAccountType;

public interface WechatAccountTypeMapper {
    int deleteByPrimaryKey(Long wechatAccountTypeId);

    int insert(WechatAccountType record);

    int insertSelective(WechatAccountType record);

    WechatAccountType selectByPrimaryKey(Long wechatAccountTypeId);

    int updateByPrimaryKeySelective(WechatAccountType record);

    int updateByPrimaryKey(WechatAccountType record);
}