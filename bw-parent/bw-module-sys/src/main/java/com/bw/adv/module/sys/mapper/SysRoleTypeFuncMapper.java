package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.SysRoleTypeFuncKey;

public interface SysRoleTypeFuncMapper {
    int deleteByPrimaryKey(SysRoleTypeFuncKey key);

    int insert(SysRoleTypeFuncKey record);

    int insertSelective(SysRoleTypeFuncKey record);
}