package com.bw.adv.module.sys.repository.impl;


import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.enums.SysFunctionOperationTypeEnum;
import com.bw.adv.module.sys.mapper.SysFunctionOperationMapper;
import com.bw.adv.module.sys.model.SysFunctionOperation;
import com.bw.adv.module.sys.repository.SysFunctionOperationRepository;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: SysRoleRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-17 下午4:03:22 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Repository
public class SysFunctionOperationRepositoryImpl extends BaseRepositoryImpl<SysFunctionOperation, SysFunctionOperationMapper> implements  SysFunctionOperationRepository {

	@Override
	protected Class<SysFunctionOperationMapper> getMapperClass() {
		return SysFunctionOperationMapper.class;
	}

	@Override
	public List<SysFunctionOperation> selectButtonOperationByUserId(Long userId) {
		if(userId == null || userId < 1)
			throw new IllegalArgumentException("SysFunctionOperationRepositoryImpl: selectByUserId argument[userId] is null");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		params.put("type", SysFunctionOperationTypeEnum.BUTTON.getKey());
		return getMapper().selectByUserIdAndType(params);
	}

	@Override
	public SysFunctionOperation selectByPrimaryKey(Long functionOperationId) {
		if(functionOperationId == null || functionOperationId < 1)
			throw new IllegalArgumentException("SysFunctionOperationRepositoryImpl:selectByPrimaryKey param[functionOperationId] is invalid");
		return getMapper().selectByPrimaryKey(functionOperationId);
	}

	@Override
	public List<SysFunctionOperation> queryFunctionOperationsByType(Integer type) {
		return getMapper().selectByTypeFunctionOperations(type);
	}

	@Override
	public List<SysFunctionOperation> queryDefaultFunctionOperations() {
		return getMapper().selectByTypeFunctionOperations(SysFunctionOperationTypeEnum.DEFAULT.getKey());
	}

	@Override
	public List<SysFunctionOperation> selectByFunctionIdWithStatusAndType(Long functionId, Integer status, Integer type) {
		if(functionId == null || functionId < 1)
			throw new IllegalArgumentException("SysFunctionOperationRepositoryImpl:selectByFunctionIdWithStatusAndType param[functionId] is invalid");
		SysFunctionOperation sysFunctionOperation = new SysFunctionOperation();
		sysFunctionOperation.setFunctionId(functionId);
		sysFunctionOperation.setStatus(status);
		sysFunctionOperation.setFunctionOperationType(type);
		return getMapper().selectByFunctionIdWithStatusAndType(sysFunctionOperation);
	}

	@Override
	public int deleteByPrimaryKey(Long functionOperationId) {
		if(functionOperationId == null || functionOperationId < 1)
			throw new IllegalArgumentException("SysFunctionOperationRepositoryImpl:deleteByPrimaryKey param[functionOperationId] is invalid");
		return getMapper().deleteByPrimaryKey(functionOperationId);
	}

	@Override
	public int insert(SysFunctionOperation functionOperation) {
		return getMapper().insert(functionOperation);
	}

	@Override
	public int updateByPrimaryKeySelective(SysFunctionOperation functionOperation) {
		if(functionOperation == null || functionOperation.getFunctionOperationId() == null || functionOperation.getFunctionOperationId() < 1)
			throw new IllegalArgumentException("SysFunctionOperationRepositoryImpl:updateByPrimaryKeySelective param[functionOperation] is invalid");
		return getMapper().updateByPrimaryKeySelective(functionOperation);
	}
}
