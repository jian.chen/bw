package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.SysRoleCubeMeasureKey;

public interface SysRoleCubeMeasureMapper {
    int deleteByPrimaryKey(SysRoleCubeMeasureKey key);

    int insert(SysRoleCubeMeasureKey record);

    int insertSelective(SysRoleCubeMeasureKey record);
}