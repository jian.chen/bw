package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.SysRoleDimLevel;
import com.bw.adv.module.sys.model.SysRoleDimLevelKey;

public interface SysRoleDimLevelMapper {
    int deleteByPrimaryKey(SysRoleDimLevelKey key);

    int insert(SysRoleDimLevel record);

    int insertSelective(SysRoleDimLevel record);

    SysRoleDimLevel selectByPrimaryKey(SysRoleDimLevelKey key);

    int updateByPrimaryKeySelective(SysRoleDimLevel record);

    int updateByPrimaryKey(SysRoleDimLevel record);
}