package com.bw.adv.module.sys.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sys.model.SysRoleFuncKey;

import java.util.List;
import java.util.Map;

public interface SysRoleFuncMapper extends BaseMapper<SysRoleFuncKey> {
    int deleteByPrimaryKey(SysRoleFuncKey key);

    int insert(SysRoleFuncKey record);

    int insertSelective(SysRoleFuncKey record);

    int insertBatch(List<SysRoleFuncKey> sysRoleFuncKeyList);

    List<Map<String,Object>> selectCheckedSysRoleFuncList(Long roleId);

    /**
     * 查询角色选中的权限
     * @param roleId
     * @return
     */
    List<SysRoleFuncKey> selectCheckedSysRoleFuncLists(Long roleId);

    int deleteByRoleId(Long roleId);

    public List<SysRoleFuncKey> selectByUserId(Long userId);
}