/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysRoleRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015年8月17日上午10:54:10
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.sys.mapper.SysLogMapper;
import com.bw.adv.module.sys.model.SysLog;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.model.exp.SysLogExp;
import com.bw.adv.module.sys.repository.SysLogRepository;


/**
 * ClassName: SysLogRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-18 下午4:22:50 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class SysLogRepositoryImpl extends BaseRepositoryImpl<SysLog, SysLogMapper> implements SysLogRepository{

	@Override
	protected Class<SysLogMapper> getMapperClass() {
		return SysLogMapper.class;
	}

	@Override
	public List<SysLogExp> findByExample(Example example,Page<SysLogExp> page) {
		
		return this.getMapper().selectByExample(example,page);
	}

	@Override
	public List<SysUser> findAllUser() {
		
		return this.getMapper().selectAllUser();
	}
	
}


