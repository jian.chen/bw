package com.bw.adv.module.sys.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sys.model.SysUserRoleKey;

public interface SysUserRoleMapper extends BaseMapper<SysUserRoleKey> {
	List<SysUserRoleKey> selectByUserId(Long userId);

	int deleteByUserId(Long userId);

	List<SysUserRoleKey> selectByRoleId(long roleId);
}