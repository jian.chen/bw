package com.bw.adv.module.sys.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.sys.mapper.SysUserMapper;
import com.bw.adv.module.sys.model.SysUser;

public interface SysUserRepository extends BaseRepository<SysUser, SysUserMapper>{

	/**
	 * 
	 * findByStatusId:根据状态查询用户列表
	 * Date: 2015年9月2日 下午2:33:13 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @return
	 */
	public List<SysUser> findByStatusId(Long statusId, Page<SysUser> page);
	
	/**
	 * 
	 * updateStatusIdList:批量修改用户状态
	 * Date: 2015年9月2日 下午2:35:46 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param sysUserIds
	 * @return
	 */
	public int updateStatusIdList(Long statusId, List<Long> sysUserIds);
	
	/**
	 * 
	 * findByUserName:根据用户名查询用户信息
	 * Date: 2015年9月2日 下午2:36:12 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param name
	 * @return
	 */
	public SysUser findByUserName(String name);
	
}
