package com.bw.adv.module.sys.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sys.model.SysFunctionOperation;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysFunctionOperationMapper extends BaseMapper<SysFunctionOperation>  {

    /**
     * keys:userId(必填）, type(选填）
     * @param params
     * @return
     */
    public List<SysFunctionOperation> selectByUserIdAndType(Map<String, Object> params);

    public List<SysFunctionOperation> selectByTypeFunctionOperations(@Param("type") Integer type);

    public SysFunctionOperation selectByPrimaryKey(@Param("functionOperationId") Long functionOperationId);

    public List<SysFunctionOperation> selectByFunctionIdWithStatusAndType(SysFunctionOperation functionOperation);

    public int deleteByPrimaryKey(Long functionOperationId);

    public int insert(SysFunctionOperation functionOperation);

    public int updateByPrimaryKeySelective(SysFunctionOperation functionOperation);
//
//    int updateByPrimaryKey(SysFunctionOperation record);

//    List<Map<String,Object>> selectSysFunctionOperationList();
//    int insertSelective(SysFunctionOperation record);

}