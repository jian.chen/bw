/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysFunctionRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015-8-17下午4:35:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.sys.repository;


import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.SysFunctionOperationMapper;
import com.bw.adv.module.sys.model.SysFunctionOperation;

import java.util.List;

/**
 * ClassName:SysFunctionRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-17 下午4:35:19 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
public interface SysFunctionOperationRepository extends BaseRepository<SysFunctionOperation, SysFunctionOperationMapper> {


    public List<SysFunctionOperation> selectButtonOperationByUserId(Long userId);

    public SysFunctionOperation selectByPrimaryKey(Long functionOperationId);

    /**
     * 根据操作类型查询系统操作记录
     * 如果查询所有的话，type为null
     * @param type
     * @return
     */
    public List<SysFunctionOperation> queryFunctionOperationsByType(Integer type);

    public List<SysFunctionOperation> queryDefaultFunctionOperations();

    public List<SysFunctionOperation> selectByFunctionIdWithStatusAndType(Long functionId, Integer status, Integer type);

    public int deleteByPrimaryKey(Long functionOperationId);

    public int insert(SysFunctionOperation functionOperation);

    public int updateByPrimaryKeySelective(SysFunctionOperation functionOperation);

}

