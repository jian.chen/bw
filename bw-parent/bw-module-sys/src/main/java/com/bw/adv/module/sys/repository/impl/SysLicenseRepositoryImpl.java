package com.bw.adv.module.sys.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.SysLicenseMapper;
import com.bw.adv.module.sys.model.SysLicense;
import com.bw.adv.module.sys.repository.SysLicenseRepository;

@Repository
public class SysLicenseRepositoryImpl extends BaseRepositoryImpl<SysLicense, SysLicenseMapper> implements SysLicenseRepository{

	@Override
	protected Class<SysLicenseMapper> getMapperClass() {
		return SysLicenseMapper.class;
	}

	@Override
	public SysLicense getActiveInfo() {
		return this.getMapper().getActiveInfo();
	}

}
