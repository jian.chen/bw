package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.ResourcesType;

public interface ResourcesTypeMapper {
    int deleteByPrimaryKey(Long resourcesTypeId);

    int insert(ResourcesType record);

    int insertSelective(ResourcesType record);

    ResourcesType selectByPrimaryKey(Long resourcesTypeId);

    int updateByPrimaryKeySelective(ResourcesType record);

    int updateByPrimaryKey(ResourcesType record);
}