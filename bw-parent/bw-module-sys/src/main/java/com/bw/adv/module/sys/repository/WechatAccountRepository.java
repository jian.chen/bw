package com.bw.adv.module.sys.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.WechatAccountMapper;
import com.bw.adv.module.wechat.model.WechatAccount;
import com.bw.adv.module.wechat.model.WechatAccountConfig;
import com.bw.adv.module.wechat.model.exp.WechatAccountExp;

/**
 * 
* @ClassName: WechatAccountRepository
* @Description: 微信账号信息repository
* @author A18ccms a18ccms_gmail_com
* @date 2015年8月13日 下午4:59:24
*
 */
public interface WechatAccountRepository extends BaseRepository<WechatAccount, WechatAccountMapper> {

	/**
	 * 
	 * findExpByPk:根据Id查询微信账户信息
	 * Date: 2015年9月2日 下午1:42:03 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param wechatAccountId
	 * @return
	 */
	public WechatAccountExp findExpByPk(Long wechatAccountId);
	
	/**
	 * 
	 * findExp:查询列表中所有账户信息
	 * Date: 2015年9月2日 下午1:41:29 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @return
	 */
	public List<WechatAccountExp> findExp();
	
}
