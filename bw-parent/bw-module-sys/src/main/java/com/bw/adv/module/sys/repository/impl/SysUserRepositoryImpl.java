package com.bw.adv.module.sys.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.sys.mapper.SysUserMapper;
import com.bw.adv.module.sys.model.SysUser;
import com.bw.adv.module.sys.repository.SysUserRepository;

@Repository
public class SysUserRepositoryImpl extends BaseRepositoryImpl<SysUser, SysUserMapper> implements SysUserRepository {
	
	@Override
	protected Class<SysUserMapper> getMapperClass() {
		return SysUserMapper.class;
	}



	@Override
	public List<SysUser> findByStatusId(Long statusId, Page<SysUser> page) {
		return this.getMapper().selectByStatusId(statusId, page);
	}

	@Override
	public int updateStatusIdList(Long statusId, List<Long> sysUserIds) {
		return this.getMapper().updateStatusIdList(statusId, sysUserIds);
	}


	@Override
	public SysUser findByUserName(String name) {
		return this.getMapper().selectByUserName(name);
	}

}
