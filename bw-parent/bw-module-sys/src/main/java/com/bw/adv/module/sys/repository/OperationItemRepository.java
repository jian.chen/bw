/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysRoleRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015年8月17日上午10:54:10
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.repository;

import java.util.List;





import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.OperationItemMapper;
import com.bw.adv.module.sys.model.OperationItem;


/**
 * ClassName:SysRoleRepository <br/>
 * Function: 角色权限
 * Date: 2015年8月17日 上午10:54:10 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface OperationItemRepository extends BaseRepository<OperationItem, OperationItemMapper> {

	
	public List<OperationItem> findOperationItemList();
	
	
}


