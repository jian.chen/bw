package com.bw.adv.module.sys.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sys.model.SysRoleData;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public interface SysRoleDataMapper extends BaseMapper<SysRoleData> {

    public List<SysRoleData> selectByRoleId(@Param("roleId") Long roleId);

    public List<SysRoleData> selectByUserId(@Param("userId") Long userId);

    public Integer deleteByRoleId(@Param("roleId") Long roleId);

    public Integer batchInsert(@Param("sysRoleDatas") List<SysRoleData> sysRoleDatas);
}
