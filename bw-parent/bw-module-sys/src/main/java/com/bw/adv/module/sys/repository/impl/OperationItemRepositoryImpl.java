/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysRoleRepositoryImpl.java
 * Package Name:com.sage.scrm.module.sys.repository.impl
 * Date:2015年8月17日上午11:01:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.OperationItemMapper;
import com.bw.adv.module.sys.model.OperationItem;
import com.bw.adv.module.sys.repository.OperationItemRepository;

/**
 * ClassName:SysRoleRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 上午11:01:21 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class OperationItemRepositoryImpl extends BaseRepositoryImpl<OperationItem, OperationItemMapper> implements OperationItemRepository {

	@Override
	protected Class<OperationItemMapper> getMapperClass() {
		
		return OperationItemMapper.class;
	}

	@Override
	public List<OperationItem> findOperationItemList() {
		return this.getMapper().selectOperationItemList();
	}

}

