package com.bw.adv.module.sys.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sys.model.SysRoleDataType;

import java.util.List;

/**
 * Created by jeoy.zhou on 1/12/16.
 */
public interface SysRoleDataTypeMapper extends BaseMapper<SysRoleDataType> {

    public List<SysRoleDataType> findAll();
}
