/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-sys
 * File Name:SysFunctionRepository.java
 * Package Name:com.sage.scrm.module.sys.repository
 * Date:2015-8-17下午4:35:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.sys.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.SysFunctionMapper;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.exp.SysFunctionExp;

import java.util.List;

/**
 * ClassName:SysFunctionRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-17 下午4:35:19 <br/>
 * scrmVersion 1.0
 * @author   lulu.wang
 * @version  jdk1.7
 * @see
 */
public interface SysFunctionRepository extends BaseRepository<SysFunction, SysFunctionMapper> {

	public List<SysFunction> findSysFunctionList(Long systemId);

	public List<SysFunction> findSysFunctionListByParentid(Long parentid);

	public List<SysFunction> findSysFunctionListByUserId(Long userId, Long systemId);

	/**
	 * 查询所有权限和按钮
	 * @return
	 */
	public List<SysFunctionExp> selectSysFunctionListAndOperate(Long systemId);

	public List<SysFunctionExp> selectSecondFunction(Long systemId);

}

