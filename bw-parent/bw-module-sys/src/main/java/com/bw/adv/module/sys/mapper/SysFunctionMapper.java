package com.bw.adv.module.sys.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.exp.SysFunctionExp;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysFunctionMapper extends BaseMapper<SysFunction> {
    int deleteByPrimaryKey(Long functionId);

    int insert(SysFunction record);

    int insertSelective(SysFunction record);

    SysFunction selectByPrimaryKey(Long functionId);

    int updateByPrimaryKeySelective(SysFunction record);

    int updateByPrimaryKey(SysFunction record);

    List<SysFunction> selectSysFunctionList(@Param("systemId") Long systemId);

    /**
     * 查询所有权限和按钮
     * @return
     */
    List<SysFunctionExp> selectSysFunctionListAndOperate(@Param("systemId") Long systemId);

    List<SysFunction> selectByParentid(Long parentid);

    List<SysFunction> selectSysFunctionByUserid(@Param("userId")Long userId, @Param("systemId") Long systemId);

    /**
     * 查询1，2级菜单
     * @return
     */
    List<SysFunctionExp> selectSecondFunction(@Param("systemId") Long systemId);
}