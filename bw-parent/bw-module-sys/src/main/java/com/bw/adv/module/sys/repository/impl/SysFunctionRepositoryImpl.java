package com.bw.adv.module.sys.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.SysFunctionMapper;
import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.exp.SysFunctionExp;
import com.bw.adv.module.sys.repository.SysFunctionRepository;

/**
 * ClassName: SysRoleRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-17 下午4:03:22 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Repository
public class SysFunctionRepositoryImpl extends BaseRepositoryImpl<SysFunction, SysFunctionMapper> implements  SysFunctionRepository {

	@Override
	protected Class<SysFunctionMapper> getMapperClass() {
		return SysFunctionMapper.class;
	}

	@Override
	public List<SysFunction> findSysFunctionList(Long systemId) {
		return this.getMapper().selectSysFunctionList(systemId);
	}

	@Override
	public List<SysFunction> findSysFunctionListByParentid(Long parentid) {
		return this.getMapper().selectByParentid(parentid);
	}

	@Override
	public List<SysFunction> findSysFunctionListByUserId(Long userId, Long systemId) {
		return this.getMapper().selectSysFunctionByUserid(userId, systemId);
	}

	@Override
	public List<SysFunctionExp> selectSysFunctionListAndOperate(Long systemId) {
		return this.getMapper().selectSysFunctionListAndOperate(systemId);
	}

	@Override
	public List<SysFunctionExp> selectSecondFunction(Long systemId) {
		return this.getMapper().selectSecondFunction(systemId);
	}
}
