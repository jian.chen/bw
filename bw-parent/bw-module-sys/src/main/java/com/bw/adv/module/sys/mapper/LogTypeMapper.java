package com.bw.adv.module.sys.mapper;

import com.bw.adv.module.sys.model.LogType;

public interface LogTypeMapper {
    int deleteByPrimaryKey(Long logTypeId);

    int insert(LogType record);

    int insertSelective(LogType record);

    LogType selectByPrimaryKey(Long logTypeId);

    int updateByPrimaryKeySelective(LogType record);

    int updateByPrimaryKey(LogType record);
}