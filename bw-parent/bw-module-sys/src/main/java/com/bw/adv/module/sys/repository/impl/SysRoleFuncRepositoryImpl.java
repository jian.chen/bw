package com.bw.adv.module.sys.repository.impl;



import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.sys.mapper.SysRoleFuncMapper;
import com.bw.adv.module.sys.model.SysRoleFuncKey;
import com.bw.adv.module.sys.repository.SysRoleFuncRepository;

/**
 * ClassName: SysRoleRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-17 下午4:03:22 <br/>
 * scrmVersion 1.0
 * @author lulu.wang
 * @version jdk1.7
 */
@Repository
public class SysRoleFuncRepositoryImpl extends BaseRepositoryImpl<SysRoleFuncKey, SysRoleFuncMapper> implements  SysRoleFuncRepository {

	@Override
	protected Class<SysRoleFuncMapper> getMapperClass() {
		return SysRoleFuncMapper.class;
	}

	@Override
	public List<Map<String, Object>> findCheckedSysRoleFuncList(Long roleId) {
		return this.getMapper().selectCheckedSysRoleFuncList(roleId);
	}

	@Override
	public void removeByRoleId(Long roleId) {
		this.getMapper().deleteByRoleId(roleId);
	}

	@Override
	public List<SysRoleFuncKey> findByUserId(long userId) {
		return this.getMapper().selectByUserId(userId);
	}

	@Override
	public List<SysRoleFuncKey> selectCheckedSysRoleFuncLists(Long roleId) {
		return this.getMapper().selectCheckedSysRoleFuncLists(roleId);
	}

}
