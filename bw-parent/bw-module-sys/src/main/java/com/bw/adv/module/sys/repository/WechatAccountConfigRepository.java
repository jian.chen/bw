package com.bw.adv.module.sys.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.sys.mapper.WechatAccountConfigMapper;
import com.bw.adv.module.wechat.model.WechatAccountConfig;

/**
 * 
* @ClassName: WechatAccountConfigRepository
* @Description: 微信账号配置信息
* @author chuanxue.wei   
* @date 2015年8月13日 下午8:21:47
*
 */
public interface WechatAccountConfigRepository extends BaseRepository<WechatAccountConfig, WechatAccountConfigMapper> {
	
	/**
	 * 
	 * findByWechatAccountId:根据微信账号id查询其配置信息
	 * Date: 2015年8月14日 上午11:10:55 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param wechatAccountId
	 * @return
	 */
	public WechatAccountConfig findByWechatAccountId(Long wechatAccountId);
	
	/**
	 * 更新微信连接状态
	 * @param wechatAccountConfig
	 * @return
	 */
	public int updateWechatConnectStatus(WechatAccountConfig wechatAccountConfig);


}
