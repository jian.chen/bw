package com.bw.adv.module.sys.mapper;

import java.io.Serializable;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Condition;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.sys.model.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser>{

	public List<SysUser> getByName(@Param("name")String username, Page<SysUser> page);

	SysUser findByPk(@Param("id")Serializable id,Condition condition);
	
	List<SysUser> selectByStatusId(@Param("status") Long statusId, Page<SysUser> page);
	
	int updateStatusIdList(@Param("statusId")Long statusId, @Param("sysUserIds")List<Long> sysUserIds);
	
	SysUser selectByUserName(String userName);

}
