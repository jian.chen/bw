package com.bw.adv.module.report.model;

import com.bw.adv.module.tools.Excel;

public class MemberRegion {

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "城市", importConvertSign = 0)
	private String geoName;
	
	private Long memberCount;
	
	private Long allMemberAccount;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "人数", importConvertSign = 0)
	private Long memberId;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "占比", importConvertSign = 0)
	private String rate;
	
	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getGeoName() {
		return geoName;
	}

	public void setGeoName(String geoName) {
		this.geoName = geoName;
	}


	public Long getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(Long memberCount) {
		this.memberCount = memberCount;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public Long getAllMemberAccount() {
		return allMemberAccount;
	}

	public void setAllMemberAccount(Long allMemberAccount) {
		this.allMemberAccount = allMemberAccount;
	}
	
	
}
