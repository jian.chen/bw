package com.bw.adv.module.base.model;

public class GeoAssocKey {
    private Long geoId;

    private Long toGeoId;

    public Long getGeoId() {
        return geoId;
    }

    public void setGeoId(Long geoId) {
        this.geoId = geoId;
    }

    public Long getToGeoId() {
        return toGeoId;
    }

    public void setToGeoId(Long toGeoId) {
        this.toGeoId = toGeoId;
    }
}