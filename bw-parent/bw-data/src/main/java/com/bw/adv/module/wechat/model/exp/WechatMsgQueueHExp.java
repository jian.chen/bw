/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatMsgQueueHExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年12月16日上午10:56:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;

import com.bw.adv.module.wechat.model.WechatMsgQueueH;

/**
 * ClassName:WechatMsgQueueHExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月16日 上午10:56:19 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class WechatMsgQueueHExp extends WechatMsgQueueH{
	private String openId;
	private String memberName;
	private String memberCode;
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
}

