package com.bw.adv.module.activity.enums;

import org.apache.commons.lang.StringUtils;



public enum IsCountEnum {
	
	IS_COUNT_N("N", "未计算"),
	IS_COUNT_Y("Y", "已计算"),
	IS_COUNT_E("E", "计算异常");
	
	private String id;
	private String desc;
	
	private IsCountEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static IsCountEnum[] getArray() {
		return IsCountEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			IsCountEnum[] values = IsCountEnum.values();
			for(IsCountEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}
