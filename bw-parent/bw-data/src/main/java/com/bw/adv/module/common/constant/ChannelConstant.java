package com.bw.adv.module.common.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.base.model.Channel;

public class ChannelConstant extends AbstractConstant<Channel> {
	//内部系统
	public static final ChannelConstant SCRM_WEB = new ChannelConstant(100L,"SCRM_WEB","scrm后台");
	public static final ChannelConstant WEBSITE = new ChannelConstant(101L,"WEBSITE","官网");
	public static final ChannelConstant STORE = new ChannelConstant(104L,"STORE","门店");
	
	//第三方账号
	public static final ChannelConstant WEIXIN = new ChannelConstant(202L,"WEIXIN","微信");
	public static final ChannelConstant ALIPAY = new ChannelConstant(206L,"ALIPAY","支付宝");
	public static final ChannelConstant PC = new ChannelConstant(207L,"PC","PC端");
	
	//特殊注册渠道(300-400有注册奖励请注意)
	public static final ChannelConstant LENOVO = new ChannelConstant(301L,"LENOVO","联想");
	public static final ChannelConstant HONGYI = new ChannelConstant(302L,"HONGYI","宏伊");
	public static final ChannelConstant STOREVIP = new ChannelConstant(303L,"STOREVIP","门店VIP");
	public static final ChannelConstant FAMILY = new ChannelConstant(304L,"FAMILY","内测亲友");
	
	public static final ChannelConstant BLACK = new ChannelConstant(401L,"BLACK","黑卡渠道");
	
	
	//活动渠道
	public static final ChannelConstant GET_COUPON_SYS = new ChannelConstant(501L,"GET_COUPON_SYS","系统发放");
	public static final ChannelConstant GET_COUPON_HANDLE = new ChannelConstant(502L,"GET_COUPON_HANDLE","手动领取");
	public static final ChannelConstant GET_COUPON_ACTIVITY = new ChannelConstant(503L,"GET_COUPON_ACTIVITY","活动发放");
	public static final ChannelConstant GET_COUPON_EXCHANGE = new ChannelConstant(504L,"GET_COUPON_EXCHANGE","徽章兑换");
	public static final ChannelConstant GET_COUPON_LOTTERY = new ChannelConstant(505L,"GET_COUPON_LOTTERY","转盘抽奖");
	public static final ChannelConstant GET_COUPON_ALIPAY_HANDLE = new ChannelConstant(506L,"GET_COUPON_ALIPAY_HANDLE","支付宝手动领取");
	public static final ChannelConstant GET_COUPON_ALIPAY_EXCHANGE = new ChannelConstant(507L,"GET_COUPON_ALIPAY_EXCHANGE","支付宝积分兑换");
	public static final ChannelConstant GET_COUPON_SCRM = new ChannelConstant(508L,"GET_COUPON_SCRM","系统手动发放");

	public static final ChannelConstant TABLE = new ChannelConstant(600L,"TABLE","桌贴");
	public static final ChannelConstant POSTER = new ChannelConstant(601L,"POSTER","海报");
	public static final ChannelConstant DM = new ChannelConstant(602L,"DM","DM");
	
	
	public ChannelConstant(Long id, String code, String name) {
		super(id, code, name);
	}

	@Override
	public Channel getInstance() {
		Channel channel = new Channel();
		channel.setChannelId(id);
		channel.setChannelCode(code);
		channel.setChannelName(name);
		return channel;
	}

}
