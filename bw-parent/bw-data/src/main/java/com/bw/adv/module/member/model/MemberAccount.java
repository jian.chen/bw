package com.bw.adv.module.member.model;

import java.math.BigDecimal;

public class MemberAccount {
    private Long memberAccountId;

    private Long memberId;

    private BigDecimal accountBalance;

    private BigDecimal pointsBalance;

    private BigDecimal totalConsumption;

    private Integer totalConsumptionCount;

    private BigDecimal totalAccount;

    private BigDecimal totalPoints;

    private String lastAccountTime;

    private String lastPointsTime;

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public void setMemberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    public BigDecimal getPointsBalance() {
        return pointsBalance;
    }

    public void setPointsBalance(BigDecimal pointsBalance) {
        this.pointsBalance = pointsBalance;
    }

    public BigDecimal getTotalConsumption() {
        return totalConsumption;
    }

    public void setTotalConsumption(BigDecimal totalConsumption) {
        this.totalConsumption = totalConsumption;
    }

    public Integer getTotalConsumptionCount() {
        return totalConsumptionCount;
    }

    public void setTotalConsumptionCount(Integer totalConsumptionCount) {
        this.totalConsumptionCount = totalConsumptionCount;
    }

    public BigDecimal getTotalAccount() {
        return totalAccount;
    }

    public void setTotalAccount(BigDecimal totalAccount) {
        this.totalAccount = totalAccount;
    }

    public BigDecimal getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(BigDecimal totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getLastAccountTime() {
        return lastAccountTime;
    }

    public void setLastAccountTime(String lastAccountTime) {
        this.lastAccountTime = lastAccountTime;
    }

    public String getLastPointsTime() {
        return lastPointsTime;
    }

    public void setLastPointsTime(String lastPointsTime) {
        this.lastPointsTime = lastPointsTime;
    }
}