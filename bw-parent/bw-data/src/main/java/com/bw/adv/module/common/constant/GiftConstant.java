package com.bw.adv.module.common.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.base.model.Channel;

public class GiftConstant extends AbstractConstant<Channel> {
	//内部系统
	public static final GiftConstant GIFT_STATUS_NOT_RECEIVE = new GiftConstant(1001L,"GIFT_STATUS_NOT_RECEIVE","领取成功");
	public static final GiftConstant GIFT_STATUS_YES_RECEIVE = new GiftConstant(1002L,"GIFT_STATUS_NOT_RECEIVE","领取失败");
	
	
	
	
	public GiftConstant(Long id, String code, String name) {
		super(id, code, name);
	}

	@Override
	public Channel getInstance() {
		Channel channel = new Channel();
		channel.setChannelId(id);
		channel.setChannelCode(code);
		channel.setChannelName(name);
		return channel;
	}

}
