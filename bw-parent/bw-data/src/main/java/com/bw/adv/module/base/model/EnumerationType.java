package com.bw.adv.module.base.model;

public class EnumerationType {
    private Long enumTypeId;

    private String enumTypeCode;

    private String enumTypeName;

    private String comments;

    public Long getEnumTypeId() {
        return enumTypeId;
    }

    public void setEnumTypeId(Long enumTypeId) {
        this.enumTypeId = enumTypeId;
    }

    public String getEnumTypeCode() {
        return enumTypeCode;
    }

    public void setEnumTypeCode(String enumTypeCode) {
        this.enumTypeCode = enumTypeCode;
    }

    public String getEnumTypeName() {
        return enumTypeName;
    }

    public void setEnumTypeName(String enumTypeName) {
        this.enumTypeName = enumTypeName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}