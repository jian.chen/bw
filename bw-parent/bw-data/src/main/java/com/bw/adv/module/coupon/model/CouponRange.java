package com.bw.adv.module.coupon.model;

public class CouponRange {
    private Long couponRangeId;

    private Long couponId;

    private Long orgId;

    public Long getCouponRangeId() {
        return couponRangeId;
    }

    public void setCouponRangeId(Long couponRangeId) {
        this.couponRangeId = couponRangeId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
}