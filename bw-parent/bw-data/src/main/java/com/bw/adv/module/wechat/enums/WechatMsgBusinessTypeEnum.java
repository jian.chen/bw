package com.bw.adv.module.wechat.enums;




/**
 * ClassName: 微信业务消息类型枚举<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-12-16 下午8:15:14 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public enum WechatMsgBusinessTypeEnum {
	
	GET_COUPON(1, "获得分享优惠券"),
	ORDER_SALE(2, "订单消费"),
	COUPON_EXPIRE(3,"优惠券过期"),
	GET_COUPON_EXCHANGE(4, "兑换,获得优惠券"),
	GET_COUPON_RECEIVE(5, "领取,获得优惠券"),
	GET_COUPON_ACTIVITY(6, "活动,获得优惠券"),
	GET_COUPON_HANDLE(7, "系统发放,获得优惠券"),
	CLOSE_COMPLAIN(8, "意见，关闭意见"),
	GRADE_UPDATE(9, "会员等级变更"),
	POINTS_UPDATE(10, "会员积分变更");
	
	private Integer id;
	private String desc;
	
	private WechatMsgBusinessTypeEnum(Integer id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public Integer getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static WechatMsgBusinessTypeEnum[] getArray() {
		return WechatMsgBusinessTypeEnum.values();
	}
	
	public static String getDesc(Integer id){
		if(id != null){
			WechatMsgBusinessTypeEnum[] values = WechatMsgBusinessTypeEnum.values();
			for(WechatMsgBusinessTypeEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}
