package com.bw.adv.module.report.member.model;

public class MemberChannelSummaryDay {
    private Long memberChannelSummaryDayId;

    private Long channelId;

    private Integer summaryYear;

    private Integer summaryMonth;

    private Integer summaryDay;

    private String summaryDate;

    private Integer dailyAddNotActivaeNumber;

    private Integer dailyAddActivaeNumber;

    private Integer dailyAddNumber;

    private Integer totalActivaeNumber;

    private Integer totalNotActivaeNumber;

    private Integer totalNumber;

    private String createTime;

    public Long getMemberChannelSummaryDayId() {
        return memberChannelSummaryDayId;
    }

    public void setMemberChannelSummaryDayId(Long memberChannelSummaryDayId) {
        this.memberChannelSummaryDayId = memberChannelSummaryDayId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Integer getSummaryYear() {
        return summaryYear;
    }

    public void setSummaryYear(Integer summaryYear) {
        this.summaryYear = summaryYear;
    }

    public Integer getSummaryMonth() {
        return summaryMonth;
    }

    public void setSummaryMonth(Integer summaryMonth) {
        this.summaryMonth = summaryMonth;
    }

    public Integer getSummaryDay() {
        return summaryDay;
    }

    public void setSummaryDay(Integer summaryDay) {
        this.summaryDay = summaryDay;
    }

    public String getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = summaryDate;
    }

    public Integer getDailyAddNotActivaeNumber() {
        return dailyAddNotActivaeNumber;
    }

    public void setDailyAddNotActivaeNumber(Integer dailyAddNotActivaeNumber) {
        this.dailyAddNotActivaeNumber = dailyAddNotActivaeNumber;
    }

    public Integer getDailyAddActivaeNumber() {
        return dailyAddActivaeNumber;
    }

    public void setDailyAddActivaeNumber(Integer dailyAddActivaeNumber) {
        this.dailyAddActivaeNumber = dailyAddActivaeNumber;
    }

    public Integer getDailyAddNumber() {
        return dailyAddNumber;
    }

    public void setDailyAddNumber(Integer dailyAddNumber) {
        this.dailyAddNumber = dailyAddNumber;
    }

    public Integer getTotalActivaeNumber() {
        return totalActivaeNumber;
    }

    public void setTotalActivaeNumber(Integer totalActivaeNumber) {
        this.totalActivaeNumber = totalActivaeNumber;
    }

    public Integer getTotalNotActivaeNumber() {
        return totalNotActivaeNumber;
    }

    public void setTotalNotActivaeNumber(Integer totalNotActivaeNumber) {
        this.totalNotActivaeNumber = totalNotActivaeNumber;
    }

    public Integer getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Integer totalNumber) {
        this.totalNumber = totalNumber;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}