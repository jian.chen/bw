package com.bw.adv.module.activity.model.exp;

import com.bw.adv.module.activity.model.ActivityInstance;

public class ActivityInstanceExp extends ActivityInstance{
	
    private Long activityCode;
    private Long activityName;
    private String userName;

    
	public Long getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(Long activityCode) {
		this.activityCode = activityCode;
	}
	
	public Long getActivityName() {
		return activityName;
	}
	
	public void setActivityName(Long activityName) {
		this.activityName = activityName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    
	
	
}