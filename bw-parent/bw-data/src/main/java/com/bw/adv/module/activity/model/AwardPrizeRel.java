package com.bw.adv.module.activity.model;

public class AwardPrizeRel {
    private Long awardsPrizeRelId;

    private Long awardsId;

    private Long prizeId;

    public Long getAwardsPrizeRelId() {
        return awardsPrizeRelId;
    }

    public void setAwardsPrizeRelId(Long awardsPrizeRelId) {
        this.awardsPrizeRelId = awardsPrizeRelId;
    }

    public Long getAwardsId() {
        return awardsId;
    }

    public void setAwardsId(Long awardsId) {
        this.awardsId = awardsId;
    }

    public Long getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(Long prizeId) {
        this.prizeId = prizeId;
    }
}