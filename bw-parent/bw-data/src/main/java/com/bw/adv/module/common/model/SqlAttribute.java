package com.bw.adv.module.common.model;

public class SqlAttribute {
    private Long sqlAttributeId;

    private Long sqlAttributeTypeId;

    private String attributeName;

    private String attributeCondition;

    private String columnName;

    private Integer pageType;

    private Integer valueType;

    private String attributeValue;

    private String createTime;

    private String updateTime;

    private Integer seq;

    private Long statusId;

    public Long getSqlAttributeId() {
        return sqlAttributeId;
    }

    public void setSqlAttributeId(Long sqlAttributeId) {
        this.sqlAttributeId = sqlAttributeId;
    }

    public Long getSqlAttributeTypeId() {
        return sqlAttributeTypeId;
    }

    public void setSqlAttributeTypeId(Long sqlAttributeTypeId) {
        this.sqlAttributeTypeId = sqlAttributeTypeId;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }


    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Integer getPageType() {
        return pageType;
    }

    public void setPageType(Integer pageType) {
        this.pageType = pageType;
    }

    public Integer getValueType() {
        return valueType;
    }

    public void setValueType(Integer valueType) {
        this.valueType = valueType;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public String getAttributeCondition() {
		return attributeCondition;
	}

	public void setAttributeCondition(String attributeCondition) {
		this.attributeCondition = attributeCondition;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}