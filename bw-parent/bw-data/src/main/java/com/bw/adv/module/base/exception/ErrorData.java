package com.bw.adv.module.base.exception;


/**
 * ClassName: ErrorData <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午5:43:56 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class ErrorData {
	
	private Long errorId;
	private String errorCode;
	private String errorMessage;
	
	public ErrorData(Long errorId, String errorCode, String errorMessage) {
		super();
		this.errorId = errorId;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	
	public Long getErrorId() {
		return errorId;
	}
	public void setErrorId(Long errorId) {
		this.errorId = errorId;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}

