package com.bw.adv.module.activity.model;

public class Prize {
    private Long prizeId;

    private Long awardsId;

    private String prizeName;

    private String prizeEn;

    private Integer totalQuantity;

    private Integer surplusQuantity;
    
    private Integer prizeType;
    
    private String prizeCode;	//奖品编码
    
    private String equalScore; // 积分
    
    public String getPrizeCode() {
		return prizeCode;
	}

	public void setPrizeCode(String prizeCode) {
		this.prizeCode = prizeCode;
	}

	public Integer getPrizeType() {
		return prizeType;
	}

	public void setPrizeType(Integer prizeType) {
		this.prizeType = prizeType;
	}

	public Long getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(Long prizeId) {
        this.prizeId = prizeId;
    }

    public Long getAwardsId() {
        return awardsId;
    }

    public void setAwardsId(Long awardsId) {
        this.awardsId = awardsId;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public String getPrizeEn() {
        return prizeEn;
    }

    public void setPrizeEn(String prizeEn) {
        this.prizeEn = prizeEn;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Integer getSurplusQuantity() {
        return surplusQuantity;
    }

    public void setSurplusQuantity(Integer surplusQuantity) {
        this.surplusQuantity = surplusQuantity;
    }

	public String getEqualScore() {
		return equalScore;
	}

	public void setEqualScore(String equalScore) {
		this.equalScore = equalScore;
	}
    
}