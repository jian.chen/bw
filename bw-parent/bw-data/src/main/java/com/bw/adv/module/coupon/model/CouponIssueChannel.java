package com.bw.adv.module.coupon.model;

public class CouponIssueChannel {
    private Long couponIssueChannelId;

    private Long couponIssueId;

    private Long channelId;

    public Long getCouponIssueChannelId() {
        return couponIssueChannelId;
    }

    public void setCouponIssueChannelId(Long couponIssueChannelId) {
        this.couponIssueChannelId = couponIssueChannelId;
    }

    public Long getCouponIssueId() {
        return couponIssueId;
    }

    public void setCouponIssueId(Long couponIssueId) {
        this.couponIssueId = couponIssueId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }
}