package com.bw.adv.module.sys.model;

public class OperationItem {
    private Long operationItemId;

    private Long resourcesTypeId;

    private String operationItemCode;

    private String operationItemName;

    private String description;

    private String isActive;

    public Long getOperationItemId() {
        return operationItemId;
    }

    public void setOperationItemId(Long operationItemId) {
        this.operationItemId = operationItemId;
    }

    public Long getResourcesTypeId() {
        return resourcesTypeId;
    }

    public void setResourcesTypeId(Long resourcesTypeId) {
        this.resourcesTypeId = resourcesTypeId;
    }

    public String getOperationItemCode() {
        return operationItemCode;
    }

    public void setOperationItemCode(String operationItemCode) {
        this.operationItemCode = operationItemCode;
    }

    public String getOperationItemName() {
        return operationItemName;
    }

    public void setOperationItemName(String operationItemName) {
        this.operationItemName = operationItemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}