package com.bw.adv.module.member.tag.model;

public class MemberTag {
    private Long memberTagId;

    private String memberTagCode;
    
	private String memberTagName;

    private String executeType;

    private String conditionSql;

    private String conditionRelation;

    private String remark;

    private String createTime;

    private String updateTime;

    private Long statusId;
    
    private Long isAll;
    
    private String updateType;
    
    private String updateNode;

    public Long getIsAll() {
		return isAll;
	}

	public void setIsAll(Long isAll) {
		this.isAll = isAll;
	}

	public Long getMemberTagId() {
        return memberTagId;
    }

    public void setMemberTagId(Long memberTagId) {
        this.memberTagId = memberTagId;
    }

    public String getMemberTagName() {
        return memberTagName;
    }

    public void setMemberTagName(String memberTagName) {
        this.memberTagName = memberTagName;
    }

    public String getExecuteType() {
        return executeType;
    }

    public void setExecuteType(String executeType) {
        this.executeType = executeType;
    }

    public String getConditionSql() {
        return conditionSql;
    }

    public void setConditionSql(String conditionSql) {
        this.conditionSql = conditionSql;
    }

    public String getConditionRelation() {
        return conditionRelation;
    }

    public void setConditionRelation(String conditionRelation) {
        this.conditionRelation = conditionRelation;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
    
    public String getMemberTagCode() {
		return memberTagCode;
	}

	public void setMemberTagCode(String memberTagCode) {
		this.memberTagCode = memberTagCode;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	public String getUpdateNode() {
		return updateNode;
	}

	public void setUpdateNode(String updateNode) {
		this.updateNode = updateNode;
	}
}