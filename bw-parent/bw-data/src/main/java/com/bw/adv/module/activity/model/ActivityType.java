package com.bw.adv.module.activity.model;

public class ActivityType {
    private Long activityTypeId;

    private Long upActivityTypeId;

    private String activityTypeCode;

    private String activityTypeName;

    public Long getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(Long activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public Long getUpActivityTypeId() {
        return upActivityTypeId;
    }

    public void setUpActivityTypeId(Long upActivityTypeId) {
        this.upActivityTypeId = upActivityTypeId;
    }

    public String getActivityTypeCode() {
        return activityTypeCode;
    }

    public void setActivityTypeCode(String activityTypeCode) {
        this.activityTypeCode = activityTypeCode;
    }

    public String getActivityTypeName() {
        return activityTypeName;
    }

    public void setActivityTypeName(String activityTypeName) {
        this.activityTypeName = activityTypeName;
    }
}