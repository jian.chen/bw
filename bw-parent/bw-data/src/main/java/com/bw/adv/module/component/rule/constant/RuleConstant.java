package com.bw.adv.module.component.rule.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.base.model.OrgType;

public class RuleConstant extends AbstractConstant<OrgType> {

	public static final RuleConstant RULE_ACTIVITY = new RuleConstant(1L, "RULE_ACTIVITY", "活动规则");
	public static final RuleConstant RULE_GRADE = new RuleConstant(2L, "RULE_GRADE", "等级规则");
	
	
	public RuleConstant(Long id, String code, String name) {
		super(id, code, name);
	}

	@Override
	public OrgType getInstance() {
		OrgType orgType = new OrgType();
		orgType.setOrgTypeId(id);
		orgType.setOrgTypeCode(code);
		orgType.setOrgTypeName(name);
		return orgType;
	}

}
