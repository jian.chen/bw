package com.bw.adv.module.activity.model;

public class ActivityBirthdayRecord {
    private Long activityBirthdayRecordId;

    private Long activityInstanceId;

    private Long memberId;

    private String createYear;

    private String createTime;

    public Long getActivityBirthdayRecordId() {
        return activityBirthdayRecordId;
    }

    public void setActivityBirthdayRecordId(Long activityBirthdayRecordId) {
        this.activityBirthdayRecordId = activityBirthdayRecordId;
    }

    public Long getActivityInstanceId() {
        return activityInstanceId;
    }

    public void setActivityInstanceId(Long activityInstanceId) {
        this.activityInstanceId = activityInstanceId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getCreateYear() {
        return createYear;
    }

    public void setCreateYear(String createYear) {
        this.createYear = createYear;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}