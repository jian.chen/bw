package com.bw.adv.module.goods.model;

public class GoodsShowCategory {
    private Long goodsShowCategoryId;

    private String goodsShowCategoryName;

    private String goodsShowCategoryDesc;

    private Long createBy;

    private Long updateBy;

    private String createTime;

    private String updateTime;

    private Long statusId;

    private String remark;

    public Long getGoodsShowCategoryId() {
        return goodsShowCategoryId;
    }

    public void setGoodsShowCategoryId(Long goodsShowCategoryId) {
        this.goodsShowCategoryId = goodsShowCategoryId;
    }

    public String getGoodsShowCategoryName() {
        return goodsShowCategoryName;
    }

    public void setGoodsShowCategoryName(String goodsShowCategoryName) {
        this.goodsShowCategoryName = goodsShowCategoryName;
    }

    public String getGoodsShowCategoryDesc() {
        return goodsShowCategoryDesc;
    }

    public void setGoodsShowCategoryDesc(String goodsShowCategoryDesc) {
        this.goodsShowCategoryDesc = goodsShowCategoryDesc;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}