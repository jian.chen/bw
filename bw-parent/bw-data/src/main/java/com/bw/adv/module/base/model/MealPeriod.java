/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:MealPeriod.java
 * Package Name:com.sage.scrm.module.base.model
 * Date:2016年5月23日下午5:04:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.model;
/**
 * ClassName:MealPeriod <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月23日 下午5:04:19 <br/>
 * scrmVersion 1.0
 * @author   zhangqi
 * @version  jdk1.7
 * @see 	 
 */
public class MealPeriod {
	private Long mealPeriodId;
	
	private String mealPeriodName;
	
	private String startTime;
	
	private String endTime;
	
	private String remark;
	
	private String createTime;
	
	private String updateTime;
	
	private Long statusId;
	
	private String weeks;
	
	public Long getMealPeriodId() {
		return mealPeriodId;
	}
	public void setMealPeriodId(Long mealPeriodId) {
		this.mealPeriodId = mealPeriodId;
	}
	public String getMealPeriodName() {
		return mealPeriodName;
	}
	public void setMealPeriodName(String mealPeriodName) {
		this.mealPeriodName = mealPeriodName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getWeeks() {
		return weeks;
	}
	public void setWeeks(String weeks) {
		this.weeks = weeks;
	}
	
}

