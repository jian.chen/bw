/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:ActivityException.java
 * Package Name:com.sage.scrm.module.base.exception
 * Date:2015年11月24日下午3:55:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.exception;

/**
 * ClassName:ActivityException <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月24日 下午3:55:25 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class ActivityException extends BaseRuntimeException {

	private static final long serialVersionUID = 4547219381262880466L;
	public static final ErrorData NO_QUALIFICATION = new ErrorData(1L, "NO_QUALIFICATION", "没有资格"); 
	public static final ErrorData NO_PRIZE_INSTANCE = new ErrorData(2L, "NO_PRIZE_INSTANCE", "没有获取到奖品实例");
	public static final ErrorData NO_COUPON_INSTANCE = new ErrorData(3L, "NO_COUPON_INSTANCE", "没有对应优惠券实例");
	public static final ErrorData ACTIVITY_END = new ErrorData(4L, "ACTIVITY_END", "活动已结束");
	public static final ErrorData ACTIVITY_START = new ErrorData(5L, "ACTIVITY_START", "活动未开始");
	public static final ErrorData UNKONWN_ERROR = new ErrorData(-1L, "UNKONWN_ERROR", "未知的异常");

	public ActivityException(String errorMsg, Throwable e) {
		super(errorMsg, e);
	}

	public ActivityException(String errorMsg) {
		super(errorMsg);
	}

	public ActivityException(Throwable e) {
		super(e);
	}

	public ActivityException(ErrorData errorData) {
		super(errorData);
	}
	

}

