package com.bw.adv.module.wechat.model;

public class AccessTicketCache {
    private Long accessTicketCacheId;

    private String accessToken;

    private String accessTicket;

    private String timeOut;

    public Long getAccessTicketCacheId() {
        return accessTicketCacheId;
    }

    public void setAccessTicketCacheId(Long accessTicketCacheId) {
        this.accessTicketCacheId = accessTicketCacheId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessTicket() {
        return accessTicket;
    }

    public void setAccessTicket(String accessTicket) {
        this.accessTicket = accessTicket;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }
}