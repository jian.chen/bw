package com.bw.adv.module.sys.model;

public class SysFunctionOperation_bak {
    private Long sysFunctionOperationId;

    private Long functionId;

    private Long operationItemId;

    public Long getSysFunctionOperationId() {
        return sysFunctionOperationId;
    }

    public void setSysFunctionOperationId(Long sysFunctionOperationId) {
        this.sysFunctionOperationId = sysFunctionOperationId;
    }

    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public Long getOperationItemId() {
        return operationItemId;
    }

    public void setOperationItemId(Long operationItemId) {
        this.operationItemId = operationItemId;
    }
}