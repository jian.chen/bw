package com.bw.adv.module.sys.model.exp;

import java.util.List;

import com.bw.adv.module.sys.model.SysRole;

public class SysRoleExp extends SysRole{
	
	private List<SysRoleFuncKeyExp> sysRoleFuncKeyExpList;

	public List<SysRoleFuncKeyExp> getSysRoleFuncKeyExpList() {
		return sysRoleFuncKeyExpList;
	}

	public void setSysRoleFuncKeyExpList(
			List<SysRoleFuncKeyExp> sysRoleFuncKeyExpList) {
		this.sysRoleFuncKeyExpList = sysRoleFuncKeyExpList;
	}


	

}