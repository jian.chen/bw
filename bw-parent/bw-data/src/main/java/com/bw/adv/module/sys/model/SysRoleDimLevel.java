package com.bw.adv.module.sys.model;

public class SysRoleDimLevel extends SysRoleDimLevelKey {
    private String allDimValues;

    public String getAllDimValues() {
        return allDimValues;
    }

    public void setAllDimValues(String allDimValues) {
        this.allDimValues = allDimValues;
    }
}