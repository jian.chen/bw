package com.bw.adv.module.activity.model;

public class QuestionnaireTemplateInfo {
    private Long infoId;

    private Long questionnaireTemplateId;

    private String content;

    public Long getInfoId() {
        return infoId;
    }

    public void setInfoId(Long infoId) {
        this.infoId = infoId;
    }

    public Long getQuestionnaireTemplateId() {
        return questionnaireTemplateId;
    }

    public void setQuestionnaireTemplateId(Long questionnaireTemplateId) {
        this.questionnaireTemplateId = questionnaireTemplateId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}