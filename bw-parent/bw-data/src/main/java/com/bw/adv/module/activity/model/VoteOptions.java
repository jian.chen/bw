package com.bw.adv.module.activity.model;

public class VoteOptions {
    private Long optionsId;

    private Long titleId;

    private String optionName;

    private String optionImg;

    private String optionDetail;
    
    private Long optionsCount;

    public Long getOptionsId() {
        return optionsId;
    }

    public void setOptionsId(Long optionsId) {
        this.optionsId = optionsId;
    }

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionImg() {
        return optionImg;
    }

    public void setOptionImg(String optionImg) {
        this.optionImg = optionImg;
    }

    public String getOptionDetail() {
        return optionDetail;
    }

    public void setOptionDetail(String optionDetail) {
        this.optionDetail = optionDetail;
    }

	public Long getOptionsCount() {
		return optionsCount;
	}

	public void setOptionsCount(Long optionsCount) {
		this.optionsCount = optionsCount;
	}
}