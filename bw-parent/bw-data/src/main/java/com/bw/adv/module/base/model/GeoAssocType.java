package com.bw.adv.module.base.model;

public class GeoAssocType {
    private Long geoAssocTypeId;

    private String geoAssocTypeName;

    private String comments;

    private Long seq;

    public Long getGeoAssocTypeId() {
        return geoAssocTypeId;
    }

    public void setGeoAssocTypeId(Long geoAssocTypeId) {
        this.geoAssocTypeId = geoAssocTypeId;
    }

    public String getGeoAssocTypeName() {
        return geoAssocTypeName;
    }

    public void setGeoAssocTypeName(String geoAssocTypeName) {
        this.geoAssocTypeName = geoAssocTypeName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }
}