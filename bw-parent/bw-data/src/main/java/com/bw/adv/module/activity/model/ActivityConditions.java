package com.bw.adv.module.activity.model;

public class ActivityConditions {
    private Long activityConditionsId;

    private Long activityId;

    private Long conditionsId;

    public Long getActivityConditionsId() {
        return activityConditionsId;
    }

    public void setActivityConditionsId(Long activityConditionsId) {
        this.activityConditionsId = activityConditionsId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getConditionsId() {
        return conditionsId;
    }

    public void setConditionsId(Long conditionsId) {
        this.conditionsId = conditionsId;
    }
}