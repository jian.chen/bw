package com.bw.adv.module.base.model;

public class Enumeration {
    private Long enumId;

    private Long enumTypeId;

    private String enumCode;

    private String enumName;

    private String comments;

    private String isActive;

    public Long getEnumId() {
        return enumId;
    }

    public void setEnumId(Long enumId) {
        this.enumId = enumId;
    }

    public Long getEnumTypeId() {
        return enumTypeId;
    }

    public void setEnumTypeId(Long enumTypeId) {
        this.enumTypeId = enumTypeId;
    }

    public String getEnumCode() {
        return enumCode;
    }

    public void setEnumCode(String enumCode) {
        this.enumCode = enumCode;
    }

    public String getEnumName() {
        return enumName;
    }

    public void setEnumName(String enumName) {
        this.enumName = enumName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}