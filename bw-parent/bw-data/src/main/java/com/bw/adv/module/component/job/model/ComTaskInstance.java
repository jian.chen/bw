package com.bw.adv.module.component.job.model;

public class ComTaskInstance {
    private Long taskInstanceId;

    private Long refId;

    private String taskInstanceName;

    private String notes;

    private String scheduleTime;

    private String isPredict;

    private String taskInstanceStatus;

    private String startTime;

    private String endTime;

    private String execTime;

    private String taskClass;

    public Long getTaskInstanceId() {
        return taskInstanceId;
    }

    public void setTaskInstanceId(Long taskInstanceId) {
        this.taskInstanceId = taskInstanceId;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public String getTaskInstanceName() {
        return taskInstanceName;
    }

    public void setTaskInstanceName(String taskInstanceName) {
        this.taskInstanceName = taskInstanceName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getIsPredict() {
        return isPredict;
    }

    public void setIsPredict(String isPredict) {
        this.isPredict = isPredict;
    }

    public String getTaskInstanceStatus() {
        return taskInstanceStatus;
    }

    public void setTaskInstanceStatus(String taskInstanceStatus) {
        this.taskInstanceStatus = taskInstanceStatus;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getExecTime() {
        return execTime;
    }

    public void setExecTime(String execTime) {
        this.execTime = execTime;
    }

    public String getTaskClass() {
        return taskClass;
    }

    public void setTaskClass(String taskClass) {
        this.taskClass = taskClass;
    }
}