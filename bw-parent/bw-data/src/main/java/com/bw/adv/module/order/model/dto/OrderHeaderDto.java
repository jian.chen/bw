package com.bw.adv.module.order.model.dto;


import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bw.adv.module.order.model.OrderHeader;

/**
 * ClassName: OrderHeaderDto <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-28 下午5:58:18 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@XmlRootElement(name = "orderHeaderDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderHeaderDto extends OrderHeader implements Serializable {
	
	private static final long serialVersionUID = 8492850831211390146L;

	private String memberCode;
	private String cardNo;
	private String isUsePoints;
	private String langx;
	private String optType;
	private String mobile;
	private Long createBy;
	private Long lastModifiedBy;
	
	@XmlElement(name = "orderItemDto")
	private List<OrderItemDto> orderItemDtoList;//订单明细
	@XmlElement(name = "orderCouponDto")
	private List<OrderCouponDto> orderCouponDtoList;//订单使用优惠券信息
	@XmlElement(name = "orderPayDto")
	private List<OrderPaymentMethodDto> orderPaymentMethodDtoList;//订单付款方式信息

	
	
	public Long getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(Long lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOptType() {
		return optType;
	}

	public void setOptType(String optType) {
		this.optType = optType;
	}

	public String getLangx() {
		return langx;
	}

	public void setLangx(String langx) {
		this.langx = langx;
	}

	public String getIsUsePoints() {
		return isUsePoints;
	}

	public void setIsUsePoints(String isUsePoints) {
		this.isUsePoints = isUsePoints;
	}

	public String getMemberCode() {
		return memberCode;
	}
	
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public List<OrderItemDto> getOrderItemDtoList() {
		return orderItemDtoList;
	}

	public void setOrderItemDtoList(List<OrderItemDto> orderItemDtoList) {
		this.orderItemDtoList = orderItemDtoList;
	}

	public List<OrderCouponDto> getOrderCouponDtoList() {
		return orderCouponDtoList;
	}

	public void setOrderCouponDtoList(List<OrderCouponDto> orderCouponDtoList) {
		this.orderCouponDtoList = orderCouponDtoList;
	}

	public List<OrderPaymentMethodDto> getOrderPaymentMethodDtoList() {
		return orderPaymentMethodDtoList;
	}

	public void setOrderPaymentMethodDtoList(
			List<OrderPaymentMethodDto> orderPaymentMethodDtoList) {
		this.orderPaymentMethodDtoList = orderPaymentMethodDtoList;
	}
	
	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	@Override
	public String toString() {
		return this.getExternalId() + "," + this.getMemberCode() + "," + this.getOrderDate() + "," + this.getPayAmount().doubleValue();
	}
	
}