package com.bw.adv.module.coupon.model;

public class CouponBusinessType {
    private Long couponBusinessTypeId;

    private Long systemId;

    private String couponBusinessTypeCode;

    private String couponBusinessTypeName;

    private String isActive;
    
    private String createTime;
    
    private String updateTime;

    public Long getCouponBusinessTypeId() {
        return couponBusinessTypeId;
    }

    public void setCouponBusinessTypeId(Long couponBusinessTypeId) {
        this.couponBusinessTypeId = couponBusinessTypeId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getCouponBusinessTypeCode() {
        return couponBusinessTypeCode;
    }

    public void setCouponBusinessTypeCode(String couponBusinessTypeCode) {
        this.couponBusinessTypeCode = couponBusinessTypeCode;
    }

    public String getCouponBusinessTypeName() {
        return couponBusinessTypeName;
    }

    public void setCouponBusinessTypeName(String couponBusinessTypeName) {
        this.couponBusinessTypeName = couponBusinessTypeName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
    
}