package com.bw.adv.module.sys.model;

import java.io.Serializable;

/**
 * Created by jeoy.zhou on 12/31/15.
 */
public class SysFunctionOperation implements Serializable{

    private Long functionOperationId;
    private Long functionId;
    private Integer functionOperationType;
    private String operationUrl;
    private String operationName;
    private Integer operationSeq;
    private Integer status;

    private String isCheck;

    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }

    public SysFunctionOperation(){}

    public Long getFunctionOperationId() {
        return functionOperationId;
    }

    public void setFunctionOperationId(Long functionOperationId) {
        this.functionOperationId = functionOperationId;
    }

    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public Integer getFunctionOperationType() {
        return functionOperationType;
    }

    public void setFunctionOperationType(Integer functionOperationType) {
        this.functionOperationType = functionOperationType;
    }

    public String getOperationUrl() {
        return operationUrl;
    }

    public void setOperationUrl(String operationUrl) {
        this.operationUrl = operationUrl;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public Integer getOperationSeq() {
        return operationSeq;
    }

    public void setOperationSeq(Integer operationSeq) {
        this.operationSeq = operationSeq;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
