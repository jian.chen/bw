package com.bw.adv.module.order.model.dto;

import java.util.List;

import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.order.model.OrderHeaderExt;
import com.bw.adv.module.order.model.OrderItem;
import com.bw.adv.module.order.model.OrderPaymentMethod;

/**
 * ClassName: 保存订单所有信息 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-27 下午8:20:11 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class OrderInfo {
	
	//传入的新订单
	private OrderHeader orderHeader;
	//数据库的查询订单
	private OrderHeader oldOrderHeader;
	//传入的新订单
	private OrderHeaderDto orderHeaderDto;
	//订单的传入订单明细
	private List<OrderItem> orderItems;
	//订单的传入优惠券
	private List<OrderCouponDto> orderCoupons;
	//订单付款方式
	private List<OrderPaymentMethod> orderPaymentMethods;
	//订单扩展信息
	private OrderHeaderExt orderHeaderExt;
	//订单次数
	private Integer saleCount;
	
	
	public Integer getSaleCount() {
		return saleCount;
	}

	public void setSaleCount(Integer saleCount) {
		this.saleCount = saleCount;
	}

	public OrderHeader getOrderHeader() {
		return orderHeader;
	}
	
	public void setOrderHeader(OrderHeader orderHeader) {
		this.orderHeader = orderHeader;
	}

	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	public OrderHeaderDto getOrderHeaderDto() {
		return orderHeaderDto;
	}

	public void setOrderHeaderDto(OrderHeaderDto orderHeaderDto) {
		this.orderHeaderDto = orderHeaderDto;
	}

	public OrderHeader getOldOrderHeader() {
		return oldOrderHeader;
	}

	public void setOldOrderHeader(OrderHeader oldOrderHeader) {
		this.oldOrderHeader = oldOrderHeader;
	}

	public List<OrderCouponDto> getOrderCoupons() {
		return orderCoupons;
	}

	public void setOrderCoupons(List<OrderCouponDto> orderCoupons) {
		this.orderCoupons = orderCoupons;
	}


	public List<OrderPaymentMethod> getOrderPaymentMethods() {
		return orderPaymentMethods;
	}


	public void setOrderPaymentMethods(List<OrderPaymentMethod> orderPaymentMethods) {
		this.orderPaymentMethods = orderPaymentMethods;
	}

	public OrderHeaderExt getOrderHeaderExt() {
		return orderHeaderExt;
	}

	public void setOrderHeaderExt(OrderHeaderExt orderHeaderExt) {
		this.orderHeaderExt = orderHeaderExt;
	}

}

