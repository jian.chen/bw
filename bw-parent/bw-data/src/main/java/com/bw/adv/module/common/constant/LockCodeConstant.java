package com.bw.adv.module.common.constant;


public class LockCodeConstant {

	public static final String MEMBER_POINTS_JOB_CODE = "MEMBER_POINTS_JOB_CODE";
	public static final String MEMBER_POINTS_JOB_TASK_CODE = "MEMBER_POINTS_JOB_TASK_CODE";
	public static final String MEMBER_BIRHTDAY_JOB_CODE = "MEMBER_BIRHTDAY_JOB_CODE";// 会员生日奖励
	public static final String MEMBER_NEW_JOB_CODE = "MEMBER_NEW_JOB_CODE";// 新会员奖励
	public static final String MEMBER_SPECIAL_DATE_JOB_CODE = "MEMBER_SPECIAL_DATE_JOB_CODE";//指定日期奖励
	public static final String MEMBER_WAKE_JOB_CODE = "MEMBER_WAKE_JOB_CODE";//会员唤醒奖励
	public static final String BALANCE_SUMMARY_JOB_CODE = "BALANCE_SUMMARY_JOB_CODE";
	public static final String COUPON_EXPIRE_JOB_CODE = "COUPON_EXPIRE_JOB_CODE";
	public static final String UPLOADFILE_CLEAR_JOB_CODE = "UPLOADFILE_CLEAR_JOB_CODE";
	public static final String CREATE_COUPON_INSTANCE = "UPLOADFILE_CLEAR_JOB_CODE";
	public static final String COM_TASK_JOB_CODE = "COM_TASK_JOB_CODE";
	public static final String POINTS_WARN_JOB_CODE = "POINTS_WARN_JOB_CODE";
	public static final String BUSINESSRULE_EXPIRE_JOB_CODE = "BUSINESSRULE_EXPIRE_JOB_CODE";
	public static final String CALCULATE_REPORT = "CALCULATE_REPORT";
	public static final String MEMBER_POINTS_ITEM_CLEAN_JOB_CODE = "MEMBER_POINTS_ITEM_CLEAN_JOB_CODE";
	public static final String MEMBER_POINTS_ITEM_CLEAN_WARN_JOB_CODE = "MEMBER_POINTS_ITEM_CLEAN_WARN_JOB_CODE";
	public static final String MEMBER_GRADE_JOB_CODE = "MEMBER_GRADE_JOB_CODE";//会员等级
	public static final String CONDITION_JOB_CODE = "CONDITION_JOB_CODE";
	public static final String MEMBER_GROUP_JOB_CODE = "MEMBER_GROUP_JOB_CODE";//会员分组
	public static final String MEMBER_TAG_JOB_CODE = "MEMBER_TAG_JOB_CODE";//会员标签
	public static final String ACTIVITY_IS_LATE_JOB_CODE = "ACTIVITY_IS_LATE_JOB_CODE";//活动到期验证
	public static final String MEMBER_SUMMARY_JOB_CODE = "MEMBER_SUMMARY_JOB_CODE";
	public static final String COUPON_SUMMARY_JOB_CODE = "COUPON_SUMMARY_JOB_CODE";
	public static final String AGE_SUMMARY_JOB_CODE = "AGE_SUMMARY_JOB_CODE";
	public static final String MEMBER_CHANNEL_SUMMARY_JOB_CODE = "MEMBER_CHANNEL_SUMMARY_JOB_CODE";//会员渠道报表任务
	public static final String FANS_CHANNEL_SUMMARY_JOB_CODE = "FANS_CHANNEL_SUMMARY_JOB_CODE";//粉丝渠道报表任务
	public static final String COUPON_SINGLE_SUMMARY_JOB_CODE = "COUPON_SINGLE_SUMMARY_JOB_CODE";//优惠券日统计报表任务
	public static final String ORDER_REGION_SUMMARY_DAY_JOB_CODE = "ORDER_REGION_SUMMARY_DAY_JOB_CODE";//订单报表任务
	public static final String POINTS_SUMMARY_DAY_JOB_CODE = "POINTS_SUMMARY_DAY_JOB_CODE";//积分报表任务
	
	public static final String WECHAT_SEND_MESAGE_JOB_CODE = "WECHAT_SEND_MESAGE_JOB_CODE";//给会员发送消息任务
	public static final String WECHAT_SEND_MESAGE_H_JOB_CODE = "WECHAT_SEND_MESAGE_H_JOB_CODE";//给会员发送消息任务
	public static final String WECHAT_SEND_MESAGE_H_EXPIRE_JOB_CODE = "WECHAT_SEND_MESAGE_H_EXPIRE_JOB_CODE";//给会员发过期优惠券消息任务
	public static final String INVITE_MEMBER_JOB_CODE = "INVITE_MEMBER_JOB_CODE";
	public static final String MASTER_DATA_COUPON = "MASTER_DATA_COUPON";//优惠券主数据
	public static final String MASTER_DATA_PRODUCT = "MASTER_DATA_PRODUCT";//产品主数据
	public static final String MASTER_DATA_STORE = "MASTER_DATA_STORE";//门店主数据
	

	public static final String COUPON_EXPIRE_JOB = "COUPON_EXPIRE_JOB";//优惠券过期
	public static final String POINTS_EXPIRE_JOB = "POINTS_EXPIRE_JOB";//积分过期
	
	
	public static final String MEMBER_TAG_JOB_BEAN = "MEMBER_TAG_JOB_BEAN";
}
