package com.bw.adv.module.member.message.model;

public class NotReadMessage {
	
    private Long memberId;

    private Integer num;

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}


}