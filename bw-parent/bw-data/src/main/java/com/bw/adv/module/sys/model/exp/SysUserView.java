package com.bw.adv.module.sys.model.exp;

import com.bw.adv.module.base.model.Org;
import com.bw.adv.module.sys.model.SysRole;
import com.bw.adv.module.sys.model.SysUser;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jeoy.zhou on 3/16/16.
 */
public class SysUserView implements Serializable {

    private static final long serialVersionUID = 475874742927314596L;

    private SysUser sysUser;
    private List<Long> roleIds;
    private SysRole sysRole;
    private Org org;
    private List<SysRole> sysRoles;

    public SysUserView() {}

    public SysUserView(SysUser sysUser, SysRole sysRole, List<SysRole> sysRoles) {
        this.sysUser = sysUser;
        this.sysRole = sysRole;
        this.sysRoles = sysRoles;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public SysRole getSysRole() {
        return sysRole;
    }

    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }

    public List<SysRole> getSysRoles() {
        return sysRoles;
    }

    public void setSysRoles(List<SysRole> sysRoles) {
        this.sysRoles = sysRoles;
    }

    public Org getOrg() {
        return org;
    }

    public void setOrg(Org org) {
        this.org = org;
    }

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }
}
