package com.bw.adv.module.wechat.model;

public class WechatTemplateMessageItem {
    private Long templateMessItemId;

    private Long templateMessId;

    private String templateUrl;

    private String templateReplaceParam;

    private Long statusId;

    private Long createId;

    private String createTime;

    private Long updateBy;

    private String updateTime;

    public Long getTemplateMessItemId() {
        return templateMessItemId;
    }

    public void setTemplateMessItemId(Long templateMessItemId) {
        this.templateMessItemId = templateMessItemId;
    }

    public Long getTemplateMessId() {
        return templateMessId;
    }

    public void setTemplateMessId(Long templateMessId) {
        this.templateMessId = templateMessId;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public String getTemplateReplaceParam() {
        return templateReplaceParam;
    }

    public void setTemplateReplaceParam(String templateReplaceParam) {
        this.templateReplaceParam = templateReplaceParam;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}