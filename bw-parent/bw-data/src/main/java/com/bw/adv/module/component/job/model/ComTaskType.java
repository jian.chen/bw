package com.bw.adv.module.component.job.model;

public class ComTaskType {
    private Long taskTypeId;

    private String taskTypeName;

    private String taskTypeClass;

    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public String getTaskTypeName() {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }

    public String getTaskTypeClass() {
        return taskTypeClass;
    }

    public void setTaskTypeClass(String taskTypeClass) {
        this.taskTypeClass = taskTypeClass;
    }
}