package com.bw.adv.module.member.model;

import java.math.BigDecimal;

public class Grade {
	
	private String gradeNameEn;
	
    private Long gradeId;

    private Long orgId;

    private String gradeCode;

    private String gradeName;

    private String gradeTitle;

    private Integer gtValue;

    private Integer ltValue;

    private BigDecimal discountRate;

    private String remark;

    private Integer seq;

    private String isActive;
    
    private Integer retainValue;
    
    private Integer value;
    
    private String text;

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getGradeCode() {
        return gradeCode;
    }

    public void setGradeCode(String gradeCode) {
        this.gradeCode = gradeCode;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getGradeTitle() {
        return gradeTitle;
    }

    public void setGradeTitle(String gradeTitle) {
        this.gradeTitle = gradeTitle;
    }

    public Integer getGtValue() {
        return gtValue;
    }

    public void setGtValue(Integer gtValue) {
        this.gtValue = gtValue;
    }

    public Integer getLtValue() {
        return ltValue;
    }

    public void setLtValue(Integer ltValue) {
        this.ltValue = ltValue;
    }

    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

	public Integer getRetainValue() {
		return retainValue;
	}

	public void setRetainValue(Integer retainValue) {
		this.retainValue = retainValue;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getGradeNameEn() {
		return gradeNameEn;
	}

	public void setGradeNameEn(String gradeNameEn) {
		this.gradeNameEn = gradeNameEn;
	}
    
}