package com.bw.adv.module.activity.model;

public class VoteTitle {
    private Long titleId;

    private String voteName;

    private String voteDetail;

    private Long voteResultDisplay;

    private String startDate;

    private String endDate;

    private String createDate;
    
    private Long optionCount;
    
    private Long statusId;

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public String getVoteName() {
        return voteName;
    }

    public void setVoteName(String voteName) {
        this.voteName = voteName;
    }

    public String getVoteDetail() {
        return voteDetail;
    }

    public void setVoteDetail(String voteDetail) {
        this.voteDetail = voteDetail;
    }

    public Long getVoteResultDisplay() {
        return voteResultDisplay;
    }

    public void setVoteResultDisplay(Long voteResultDisplay) {
        this.voteResultDisplay = voteResultDisplay;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

	public Long getOptionCount() {
		return optionCount;
	}

	public void setOptionCount(Long optionCount) {
		this.optionCount = optionCount;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
}