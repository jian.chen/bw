package com.bw.adv.module.sys.enums;

/**
 * Created by jeoy.zhou on 3/18/16.
 */
public enum SysSystemEnum {

    AIR(1l, "air管理后台"),
    AIR_ASSISANT(2l, "air助手");

    private Long systemId;
    private String desc;

    SysSystemEnum(Long systemId, String desc) {
        this.systemId = systemId;
        this.desc = desc;
    }

    public Long getSystemId() {
        return systemId;
    }

    public String getDesc() {
        return desc;
    }
}
