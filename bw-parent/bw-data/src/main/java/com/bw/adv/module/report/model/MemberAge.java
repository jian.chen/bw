package com.bw.adv.module.report.model;

import java.text.DecimalFormat;

import com.bw.adv.module.tools.Excel;

public class MemberAge {
	
	@Excel(exportConvertSign = 1, exportFieldWidth = 10, exportName = "年龄段", importConvertSign = 0)
	private Long ageType;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "男性人数", importConvertSign = 0)
	private Long maleDayCount;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "女性人数", importConvertSign = 0)
	private Long femaleDayCount;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "其他", importConvertSign = 0)
	private Long otherDayCount;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "总人数", importConvertSign = 0)
	private Long dayNewCount;
	
	@Excel(exportConvertSign = 1, exportFieldWidth = 10, exportName = "占比", importConvertSign = 0)
	private String proportion;
	
	private String summaryDate;
	
	private Long all;

	public Long getAgeType() {
		return ageType;
	}

	public void setAgeType(Long ageType) {
		this.ageType = ageType;
	}

	public Long getMaleDayCount() {
		return maleDayCount;
	}

	public void setMaleDayCount(Long maleDayCount) {
		this.maleDayCount = maleDayCount;
	}

	public Long getFemaleDayCount() {
		return femaleDayCount;
	}

	public void setFemaleDayCount(Long femaleDayCount) {
		this.femaleDayCount = femaleDayCount;
	}

	public Long getOtherDayCount() {
		return otherDayCount;
	}

	public void setOtherDayCount(Long otherDayCount) {
		this.otherDayCount = otherDayCount;
	}

	public Long getDayNewCount() {
		return dayNewCount;
	}

	public void setDayNewCount(Long dayNewCount) {
		this.dayNewCount = dayNewCount;
	}

	public String getSummaryDate() {
		return summaryDate;
	}

	public void setSummaryDate(String summaryDate) {
		this.summaryDate = summaryDate;
	}

	public Long getAll() {
		return all;
	}

	public void setAll(Long all) {
		this.all = all;
	}
	
	public String getAgeTypeConvert(){
		if(ageType == 1L){
			return "0-19";
		}else if(ageType == 2L){
			return "20-30";
		}else if(ageType == 3L){
			return "30-40";
		}else if(ageType == 4L){
			return "40+";
		}else if(ageType == 5L){
			return "未填写";
		}else{
			return "未知";
		}
	}
	
	public String getProportionConvert(){
		double x = ((double)dayNewCount * 100.00 / (double)all);
		DecimalFormat df = new DecimalFormat("#.00");
		return df.format(x) + "%";
	}

	public String getProportion() {
		return proportion;
	}

	public void setProportion(String proportion) {
		this.proportion = proportion;
	}
	
}
