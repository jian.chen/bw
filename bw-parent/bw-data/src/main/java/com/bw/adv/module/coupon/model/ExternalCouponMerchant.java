package com.bw.adv.module.coupon.model;

import java.util.Date;

public class ExternalCouponMerchant {
    private Integer externalMerchantId;

    private String merchantName;

    private String merchantCode;

    private Date createTime;

    public Integer getExternalMerchantId() {
        return externalMerchantId;
    }

    public void setExternalMerchantId(Integer externalMerchantId) {
        this.externalMerchantId = externalMerchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}