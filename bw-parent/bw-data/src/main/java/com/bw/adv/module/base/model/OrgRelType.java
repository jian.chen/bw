package com.bw.adv.module.base.model;

public class OrgRelType {
    private Long orgRelTypeId;

    private String orgRelTypeCode;

    private String orgRelTypeName;

    public Long getOrgRelTypeId() {
        return orgRelTypeId;
    }

    public void setOrgRelTypeId(Long orgRelTypeId) {
        this.orgRelTypeId = orgRelTypeId;
    }

    public String getOrgRelTypeCode() {
        return orgRelTypeCode;
    }

    public void setOrgRelTypeCode(String orgRelTypeCode) {
        this.orgRelTypeCode = orgRelTypeCode;
    }

    public String getOrgRelTypeName() {
        return orgRelTypeName;
    }

    public void setOrgRelTypeName(String orgRelTypeName) {
        this.orgRelTypeName = orgRelTypeName;
    }
}