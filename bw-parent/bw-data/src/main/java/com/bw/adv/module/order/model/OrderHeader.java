package com.bw.adv.module.order.model;

import java.math.BigDecimal;

public class OrderHeader {
    private Long orderId;

    private String orderCode;

    private String externalId;

    private Long orderTypeId;

    private Long memberId;

    private Long channelId;

    private Long storeId;

    private String storeCode;

    private String orderDate;

    private Long firstAttemptOrderId;

    private Long lastAttemptOrderId;

    private Long tableFor;

    private Long eatingStyleEnumId;

    private BigDecimal orderAmount;

    private BigDecimal couponAmount;

    private BigDecimal payAmount;

    private BigDecimal deliveryAmount;

    private BigDecimal usedPoints;

    private Long staffId;

    private String staffName;

    private String happenTime;

    private String createTime;

    private Long lastModifiedStaffId;

    private String lastModifiedStaffName;

    private String lastModifiedTime;

    private Long statusId;

    private String orderDesc;

    private String useInfo;
    
    private String getInfo;

    private String remark;

    private String field1;

    private String field2;

    private String field3;

    private String field4;

    private String field5;

    private String field6;

    private String field7;

    private String field8;

    private String field9;

    private String field10;
    
    private String storeName;

    public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Long getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(Long orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Long getFirstAttemptOrderId() {
        return firstAttemptOrderId;
    }

    public void setFirstAttemptOrderId(Long firstAttemptOrderId) {
        this.firstAttemptOrderId = firstAttemptOrderId;
    }

    public Long getLastAttemptOrderId() {
        return lastAttemptOrderId;
    }

    public void setLastAttemptOrderId(Long lastAttemptOrderId) {
        this.lastAttemptOrderId = lastAttemptOrderId;
    }

    public Long getTableFor() {
        return tableFor;
    }

    public void setTableFor(Long tableFor) {
        this.tableFor = tableFor;
    }

    public Long getEatingStyleEnumId() {
        return eatingStyleEnumId;
    }

    public void setEatingStyleEnumId(Long eatingStyleEnumId) {
        this.eatingStyleEnumId = eatingStyleEnumId;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(BigDecimal couponAmount) {
        this.couponAmount = couponAmount;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public BigDecimal getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(BigDecimal deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    public BigDecimal getUsedPoints() {
        return usedPoints;
    }

    public void setUsedPoints(BigDecimal usedPoints) {
        this.usedPoints = usedPoints;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getHappenTime() {
        return happenTime;
    }

    public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getLastModifiedStaffId() {
        return lastModifiedStaffId;
    }

    public void setLastModifiedStaffId(Long lastModifiedStaffId) {
        this.lastModifiedStaffId = lastModifiedStaffId;
    }

    public String getLastModifiedStaffName() {
        return lastModifiedStaffName;
    }

    public void setLastModifiedStaffName(String lastModifiedStaffName) {
        this.lastModifiedStaffName = lastModifiedStaffName;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(String lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }
    
    public String getUseInfo() {
		return useInfo;
	}

	public void setUseInfo(String useInfo) {
		this.useInfo = useInfo;
	}

	public String getGetInfo() {
        return getInfo;
    }

    public void setGetInfo(String getInfo) {
        this.getInfo = getInfo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public String getField4() {
        return field4;
    }

    public void setField4(String field4) {
        this.field4 = field4;
    }

    public String getField5() {
        return field5;
    }

    public void setField5(String field5) {
        this.field5 = field5;
    }

    public String getField6() {
        return field6;
    }

    public void setField6(String field6) {
        this.field6 = field6;
    }

    public String getField7() {
        return field7;
    }

    public void setField7(String field7) {
        this.field7 = field7;
    }

    public String getField8() {
        return field8;
    }

    public void setField8(String field8) {
        this.field8 = field8;
    }

    public String getField9() {
        return field9;
    }

    public void setField9(String field9) {
        this.field9 = field9;
    }

    public String getField10() {
        return field10;
    }

    public void setField10(String field10) {
        this.field10 = field10;
    }
}