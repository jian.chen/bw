/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:CouponSingleSummaryDayExp.java
 * Package Name:com.sage.scrm.module.report.coupon.model.exp
 * Date:2015年12月17日下午3:17:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.coupon.model.exp;

import com.bw.adv.module.report.coupon.model.CouponSingleSummaryDay;
import com.bw.adv.module.tools.Excel;

/**
 * ClassName:CouponSingleSummaryDayExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 下午3:17:41 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class CouponSingleSummaryDayExp extends CouponSingleSummaryDay {

	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "优惠券名称", importConvertSign = 0)
	private String couponName;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "发放数量", importConvertSign = 0)
	private Integer issueCount;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "核销数量", importConvertSign = 0)
	private Integer useCount;
	
	private String seachTime;

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getSeachTime() {
		return seachTime;
	}

	public void setSeachTime(String seachTime) {
		this.seachTime = seachTime;
	}
	
	
	
}

