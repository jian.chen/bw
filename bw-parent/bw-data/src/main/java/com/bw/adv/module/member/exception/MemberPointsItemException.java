package com.bw.adv.module.member.exception;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.module.base.exception.BaseRuntimeException;
import com.bw.adv.module.base.exception.ErrorData;


public class MemberPointsItemException extends BaseRuntimeException{

	private static final long serialVersionUID = 5787477967460509904L;

	public MemberPointsItemException(Throwable e) {
		super(e);
	}

	public MemberPointsItemException(ErrorData errorData) {
		super(errorData);
	}

	public MemberPointsItemException(ResultStatus resultStatus) {
		super(resultStatus);
	}

	public MemberPointsItemException(String errorMsg, ErrorData errorData) {
		super(errorMsg, errorData);
	}

	public MemberPointsItemException(String errorMsg, Throwable e) {
		super(errorMsg, e);
	}

	public MemberPointsItemException(String errorMsg) {
		super(errorMsg);
	}
	
	
}
