package com.bw.adv.module.wechat.model;

public class WechatFansLabel {
	
	/**
	 * 粉丝标签id
	 */
    private Long wechatFansLabelId;
    
    /*
     * 粉丝标签名称 
     */
    private String labelName;

    /**
     * 粉丝标签描述
     */
    private String description;
    
    /**
     * 粉丝标签代码
     */
    private String labelCode;
    
    /**
     * 执行方式
     */
    private String executeType;
    
    /**
     * 编辑器配置信息
     */
    private String sqlConfig;
    
    /**
     * 待执行的sql语句
     */
    private String sqlContent;
    
    /**
     * 创建人 
     */
    private Long createBy;
    
    /**
     * 创建时间 
     */
    private String createDate;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateDate;
    
    /**
     * 删除标识
     */
    private String isDelete;
    
    /**
     * 状态
     */
    private Long statusId;

    public Long getWechatFansLabelId() {
        return wechatFansLabelId;
    }

    public void setWechatFansLabelId(Long wechatFansLabelId) {
        this.wechatFansLabelId = wechatFansLabelId;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabelCode() {
        return labelCode;
    }

    public void setLabelCode(String labelCode) {
        this.labelCode = labelCode;
    }

    public String getExecuteType() {
        return executeType;
    }

    public void setExecuteType(String executeType) {
        this.executeType = executeType;
    }

    public String getSqlConfig() {
        return sqlConfig;
    }

    public void setSqlConfig(String sqlConfig) {
        this.sqlConfig = sqlConfig;
    }

    public String getSqlContent() {
        return sqlContent;
    }

    public void setSqlContent(String sqlContent) {
        this.sqlContent = sqlContent;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}