/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:Snippet.java
 * Package Name:com.sage.scrm.api.coupon.dto
 * Date:2015-8-17下午8:57:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.model;

import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;


/**
 * ClassName: CouponInstanceSearch <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月26日 上午10:53:12 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public class CouponInstanceSearch extends CouponInstanceExp{
	private String issueStartTime;
	
	private String issueEndTime;
	
	private Integer page;
	
	private Integer rows;

	public String getIssueStartTime() {
		return issueStartTime;
	}

	public void setIssueStartTime(String issueStartTime) {
		this.issueStartTime = issueStartTime;
	}

	public String getIssueEndTime() {
		return issueEndTime;
	}

	public void setIssueEndTime(String issueEndTime) {
		this.issueEndTime = issueEndTime;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}
	
}

