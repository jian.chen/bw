package com.bw.adv.module.member.model;

public class MemberCardStyle {
    private Long memberCardStyleId;

    private String memberCardStyleCode;

    private String memberCardStyleName;

    private String imgUrl;

    private String backgroundColor;

    public Long getMemberCardStyleId() {
        return memberCardStyleId;
    }

    public void setMemberCardStyleId(Long memberCardStyleId) {
        this.memberCardStyleId = memberCardStyleId;
    }

    public String getMemberCardStyleCode() {
        return memberCardStyleCode;
    }

    public void setMemberCardStyleCode(String memberCardStyleCode) {
        this.memberCardStyleCode = memberCardStyleCode;
    }

    public String getMemberCardStyleName() {
        return memberCardStyleName;
    }

    public void setMemberCardStyleName(String memberCardStyleName) {
        this.memberCardStyleName = memberCardStyleName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}