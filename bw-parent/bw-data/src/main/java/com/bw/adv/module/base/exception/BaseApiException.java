package com.bw.adv.module.base.exception;

import com.bw.adv.api.constant.ResultStatus;


public class BaseApiException extends RuntimeException {
	
	private static final long serialVersionUID = -6352263454707017097L;
	
	private ResultStatus error;
	
	public BaseApiException() {
		super();
	}
	
	public BaseApiException(ResultStatus error) {
		super();
		this.error = error;
	}

	public BaseApiException(String msg) {
		super(msg);
	}
	
	public BaseApiException(String msg, ResultStatus error) {
		super(msg);
		this.error = error;
	}
	
	public BaseApiException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public BaseApiException(Throwable cause) {
		super(cause);
	}
	
	public BaseApiException(ResultStatus error, Throwable cause) {
		super(cause);
		this.error = error;
	}

	public ResultStatus getError() {
		return error;
	}

	public void setError(ResultStatus error) {
		this.error = error;
	}
}
