package com.bw.adv.module.member.complain.model.exp;

import com.bw.adv.module.member.complain.model.Complain;

public class ComplainExp extends Complain{
	
	private String complainTypeName;
	
	private String memberCode;
	
	private String greaterEqualDate;
	
	private String lessEqualDate;
	
	private String geoName;
	
	private String isHandled;
	
	public String getComplainTypeName() {
		return complainTypeName;
	}

	public void setComplainTypeName(String complainTypeName) {
		this.complainTypeName = complainTypeName;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getGreaterEqualDate() {
		return greaterEqualDate;
	}

	public void setGreaterEqualDate(String greaterEqualDate) {
		this.greaterEqualDate = greaterEqualDate;
	}

	public String getLessEqualDate() {
		return lessEqualDate;
	}

	public void setLessEqualDate(String lessEqualDate) {
		this.lessEqualDate = lessEqualDate;
	}

	public String getGeoName() {
		return geoName;
	}

	public void setGeoName(String geoName) {
		this.geoName = geoName;
	}

	public String getIsHandled() {
		return isHandled;
	}

	public void setIsHandled(String isHandled) {
		this.isHandled = isHandled;
	}

}