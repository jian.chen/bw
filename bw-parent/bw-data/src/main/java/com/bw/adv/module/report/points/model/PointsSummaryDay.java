package com.bw.adv.module.report.points.model;

import java.math.BigDecimal;

import com.bw.adv.module.tools.Excel;

public class PointsSummaryDay {
    private Long pointsSummaryDayId;

    private Long pointsRuleId;

    private Integer summaryYear;

    private Integer summaryMonth;

    private Integer summaryDay;

    private String summaryDate;

    private Integer pointsInCount;

    private Integer pointsOutCount;

    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "发放数量", importConvertSign = 0)
    private BigDecimal pointsInNumber;

    private BigDecimal pointsOutNumber;

    private String createTime;

    public Long getPointsSummaryDayId() {
        return pointsSummaryDayId;
    }

    public void setPointsSummaryDayId(Long pointsSummaryDayId) {
        this.pointsSummaryDayId = pointsSummaryDayId;
    }

    public Long getPointsRuleId() {
        return pointsRuleId;
    }

    public void setPointsRuleId(Long pointsRuleId) {
        this.pointsRuleId = pointsRuleId;
    }

    public Integer getSummaryYear() {
        return summaryYear;
    }

    public void setSummaryYear(Integer summaryYear) {
        this.summaryYear = summaryYear;
    }

    public Integer getSummaryMonth() {
        return summaryMonth;
    }

    public void setSummaryMonth(Integer summaryMonth) {
        this.summaryMonth = summaryMonth;
    }

    public Integer getSummaryDay() {
        return summaryDay;
    }

    public void setSummaryDay(Integer summaryDay) {
        this.summaryDay = summaryDay;
    }

    public String getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = summaryDate;
    }

    public Integer getPointsInCount() {
        return pointsInCount;
    }

    public void setPointsInCount(Integer pointsInCount) {
        this.pointsInCount = pointsInCount;
    }

    public Integer getPointsOutCount() {
        return pointsOutCount;
    }

    public void setPointsOutCount(Integer pointsOutCount) {
        this.pointsOutCount = pointsOutCount;
    }

    public BigDecimal getPointsInNumber() {
        return pointsInNumber;
    }

    public void setPointsInNumber(BigDecimal pointsInNumber) {
        this.pointsInNumber = pointsInNumber;
    }

    public BigDecimal getPointsOutNumber() {
        return pointsOutNumber;
    }

    public void setPointsOutNumber(BigDecimal pointsOutNumber) {
        this.pointsOutNumber = pointsOutNumber;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}