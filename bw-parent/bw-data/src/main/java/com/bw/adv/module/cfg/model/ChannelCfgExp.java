package com.bw.adv.module.cfg.model;

import java.util.List;

public class ChannelCfgExp extends ChannelCfg {
	private String userName;

	private List<ChannelCfgValue> channelCfgValues;
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<ChannelCfgValue> getChannelCfgValues() {
		return channelCfgValues;
	}

	public void setChannelCfgValues(List<ChannelCfgValue> channelCfgValues) {
		this.channelCfgValues = channelCfgValues;
	}
}