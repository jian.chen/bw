package com.bw.adv.module.activity.model;

public class BusinessShowPage {
    private Long businessShowPageId;

    private String businessShowPageCode;

    private String pageUrl;

    private String pageParams;

    private String pageDeac;

    public Long getBusinessShowPageId() {
        return businessShowPageId;
    }

    public void setBusinessShowPageId(Long businessShowPageId) {
        this.businessShowPageId = businessShowPageId;
    }

    public String getBusinessShowPageCode() {
        return businessShowPageCode;
    }

    public void setBusinessShowPageCode(String businessShowPageCode) {
        this.businessShowPageCode = businessShowPageCode;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getPageParams() {
        return pageParams;
    }

    public void setPageParams(String pageParams) {
        this.pageParams = pageParams;
    }

    public String getPageDeac() {
        return pageDeac;
    }

    public void setPageDeac(String pageDeac) {
        this.pageDeac = pageDeac;
    }
}