/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:PointsRuleTypeEnum.java
 * Package Name:com.sage.scrm.module.sys.enums
 * Date:2015年8月20日下午4:43:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.enums;

import org.apache.commons.lang.StringUtils;

/**
 * ClassName:PointsRuleTypeEnum <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午4:43:22 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public enum PointsRuleTypeEnum {
	
	ONCE_SALE("1","每次消费"),
	SALE_AMOUNT("2","消费金额"),
	EVERY_ACCUMULATE("3","每次累积");
	
	private String id;
	private String desc;
	
	
	private PointsRuleTypeEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}


	public String getId() {
		return id;
	}


	public String getDesc() {
		return desc;
	}

	public static PointsRuleTypeEnum[] getArray() {
		return PointsRuleTypeEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			PointsRuleTypeEnum[] values = PointsRuleTypeEnum.values();
			for(PointsRuleTypeEnum pointsRuleType : values){
				if(pointsRuleType.getId().equals(id)){
					return pointsRuleType.getDesc();
				}
			}
		}
		return "";
	}
	

}

