package com.bw.adv.module.member.model;

public class MemberEntityCard {
    private Long memberEntityCardId;

    private String cardNo;

    private String createTime;

    private Long statusId;

    private String createBy;
    
    private Long issueId;

    public Long getMemberEntityCardId() {
        return memberEntityCardId;
    }

    public void setMemberEntityCardId(Long memberEntityCardId) {
        this.memberEntityCardId = memberEntityCardId;
    }
    
    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}
}