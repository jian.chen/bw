package com.bw.adv.module.member.group.model;

public class MemberGroupItem {
    private Long memberGroupItemId;

    private Long memberGroupId;

    private Long memberId;

    public Long getMemberGroupItemId() {
        return memberGroupItemId;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((memberGroupId == null) ? 0 : memberGroupId.hashCode());
		result = prime * result
				+ ((memberId == null) ? 0 : memberId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberGroupItem other = (MemberGroupItem) obj;
		if (memberGroupId == null) {
			if (other.memberGroupId != null)
				return false;
		} else if (!memberGroupId.equals(other.memberGroupId))
			return false;
		if (memberId == null) {
			if (other.memberId != null)
				return false;
		} else if (!memberId.equals(other.memberId))
			return false;
		return true;
	}



	public void setMemberGroupItemId(Long memberGroupItemId) {
        this.memberGroupItemId = memberGroupItemId;
    }

    public Long getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(Long memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }
}