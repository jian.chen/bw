/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:Snippet.java
 * Package Name:com.sage.scrm.module.member.model.exp
 * Date:2015-12-17上午10:18:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.model.exp;

import com.bw.adv.module.base.model.Status;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.member.model.MemberEntityCard;
import com.bw.adv.module.tools.Excel;

public class MemberEntityCardExp extends MemberEntityCard {
	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "卡号", importConvertSign = 0)
	private String cardNo;

	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "状态", importConvertSign = 0)
	private String statusName;

	private String createTime;

	private String createBy;

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public void copyProperty(MemberEntityCard entityCard) {
		Status status = StatusConstant.getConstantById(StatusConstant.class, entityCard.getStatusId());
		this.cardNo = entityCard.getCardNo();
		this.statusName = status.getStatusName();
		this.createTime = entityCard.getCreateTime();
		this.createBy = entityCard.getCreateBy();
	}
}
