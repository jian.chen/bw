package com.bw.adv.module.activity.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.activity.model.Activity;

public class ActivityConstant extends AbstractConstant<Activity>{
	
	public static final ActivityConstant ACTIVIY_PERFECT_MEMBER = new ActivityConstant(0L,"ACTIVIY_PERFECT_MEMBER", "完善会员奖励");
	public static final ActivityConstant ACTIVIY_NEW_MEMBER = new ActivityConstant(1L,"ACTIVIY_NEW_MEMBER", "新会员奖励");
	public static final ActivityConstant ACTIVIY_SPECIAL_DATE = new ActivityConstant(2L,"ACTIVIY_SPECIAL_DATE", "指定日期奖励");
	public static final ActivityConstant ACTIVIY_MEMBER_GRADE_UP = new ActivityConstant(3L,"ACTIVIY_MEMBER_GRADE_UP", "等级会员奖励");
	public static final ActivityConstant ACTIVIY_MEMBER_WAKE = new ActivityConstant(4L,"ACTIVIY_MEMBER_WAKE", "会员唤醒奖励");
	public static final ActivityConstant ACTIVIY_MEMBER_BIRTHDAY = new ActivityConstant(5L,"ACTIVIY_MEMBER_BIRTHDAY", "会员生日奖励");
	public static final ActivityConstant ACTIVIY_SPECIAL_SALE = new ActivityConstant(6L,"ACTIVIY_SPECIAL_SALE", "指定消费奖励");
	public static final ActivityConstant ACTIVIY_FIRST_SALE = new ActivityConstant(7L,"ACTIVIY_FIRST_SALE", "首笔消费奖励");
	public static final ActivityConstant ACTIVITY_EVERY_SALE = new ActivityConstant(8L,"ACTIVITY_EVERY_SALE", "消费即送奖励");
	
	
	public static final ActivityConstant TAG_NEW_MEMBER = new ActivityConstant(11L,"TAG_NEW_MEMBER", "注册打标签");
	public static final ActivityConstant TAG_UPDATE_MEMBER = new ActivityConstant(12L,"TAG_UPDATE_MEMBER", "更新打标签");
	public static final ActivityConstant TAG_ORDER = new ActivityConstant(13L,"TAG_ORDER", "订单打标签");
	
	public static final ActivityConstant ACTIVIY_INVITE_NEW_MEMBER = new ActivityConstant(22L,"ACTIVIY_INVITE_NEW_MEMBER", "新会员邀请奖励");
	public static final ActivityConstant ACTIVIY_ORDER_BIRTHDAY_MONTH = new ActivityConstant(22L,"ACTIVIY_ORDER_BIRTHDAY_MONTH", "生日月消费活动");
	public static final ActivityConstant ACTIVIY_ORDER_WEEK = new ActivityConstant(23L,"ACTIVIY_ORDER_WEEK", "周消费活动");
	public static final ActivityConstant ACTIVIY_ORDER_OFTEN = new ActivityConstant(24L,"ACTIVIY_ORDER_OFTEN", "常客优惠");
	
	
	public ActivityConstant(Long id,String code, String name) {
		super(id, code,name);
	}

	@Override
	public Activity getInstance() {
		Activity activity = new Activity();
		activity.setActivityCode(code);
		activity.setActivityId(id);
		activity.setActivityName(name);
		return activity;
	}

	
}