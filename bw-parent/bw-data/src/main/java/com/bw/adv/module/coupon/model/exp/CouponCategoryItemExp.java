/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:CouponCategoryItemExp.java
 * Package Name:com.sage.scrm.module.coupon.model.exp
 * Date:2015年12月28日上午10:09:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.model.exp;

import com.bw.adv.module.coupon.model.CouponCategoryItem;

/**
 * ClassName:CouponCategoryItemExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月28日 上午10:09:15 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class CouponCategoryItemExp extends CouponCategoryItem {

	private String couponName;
	
	private String couponCategoryName;

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getCouponCategoryName() {
		return couponCategoryName;
	}

	public void setCouponCategoryName(String couponCategoryName) {
		this.couponCategoryName = couponCategoryName;
	}
	
	
	
}

