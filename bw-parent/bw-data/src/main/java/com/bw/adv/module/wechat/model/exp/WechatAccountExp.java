/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatAccountExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年8月22日下午4:55:57
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;

import com.bw.adv.module.wechat.model.WechatAccount;

/**
 * ClassName:WechatAccountExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月22日 下午4:55:57 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class WechatAccountExp extends WechatAccount {
	
	private String appId;
	
	private String appSecret;
	
	private String URL;
	
	private String accessToken;

	private String connectStatus;

	private String connectStatusMsg;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getConnectStatus() {
		return connectStatus;
	}

    public String getConnectStatusMsg() {
        return connectStatusMsg;
    }

    public void setConnectStatusMsg(String connectStatusMsg) {
        this.connectStatusMsg = connectStatusMsg;
    }

    public void setConnectStatus(String connectStatus) {
		this.connectStatus = connectStatus;
	}
}

