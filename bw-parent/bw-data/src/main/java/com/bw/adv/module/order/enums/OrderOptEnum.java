package com.bw.adv.module.order.enums;


/**
 * ClassName: OrderOptEnum <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-16 下午12:18:47 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public enum OrderOptEnum {
	
	//订单操作方式
	ORDER_OPERATE_WAY_INSERT("i", "INSERT", "同步订单，新增操作"),
	ORDER_OPERATE_WAY_UPDATE("u", "UPDATE", "同步订单，更新操作"),
	ORDER_OPERATE_WAY_DELETE("d", "DELETE", "同步订单，删除操作");
	
	private String key;
	private String code;
	private String remark;
	
	private OrderOptEnum(String key, String code, String remark) {
		this.key = key;
		this.code = code;
		this.remark = remark;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getRemark() {
		return remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
}

