/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:Snippet.java
 * Package Name:com.sage.scrm.api.member.dto
 * Date:2015-8-17下午8:57:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.grade.dto;

import com.bw.adv.module.member.model.Grade;

/**
 * ClassName: GradeDto <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月14日 下午5:06:53 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public class GradeDto extends Grade{
	
	private String extAccountTypeId;
	private String bindingAccount;
	
	public String getExtAccountTypeId() {
		return extAccountTypeId;
	}
	public void setExtAccountTypeId(String extAccountTypeId) {
		this.extAccountTypeId = extAccountTypeId;
	}
	public String getBindingAccount() {
		return bindingAccount;
	}
	public void setBindingAccount(String bindingAccount) {
		this.bindingAccount = bindingAccount;
	}
	
}

