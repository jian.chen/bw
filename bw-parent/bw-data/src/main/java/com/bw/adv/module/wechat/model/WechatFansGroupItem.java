package com.bw.adv.module.wechat.model;

public class WechatFansGroupItem {
	/**
	 * 记录编号
	 */
    private Long wechatFansGroupItemId;
    
    /**
     * 微信粉丝ID
     */
    private Long wechatFansId;
    
    /**
     * 微信粉丝组ID
     */
    private Long wechatFansGroupId;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
    private String createTime;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updatedTime;
    
    /**
     * 删除标识
     */
    private String isDelete;

    public Long getWechatFansGroupItemId() {
        return wechatFansGroupItemId;
    }

    public void setWechatFansGroupItemId(Long wechatFansGroupItemId) {
        this.wechatFansGroupItemId = wechatFansGroupItemId;
    }

    public Long getWechatFansId() {
        return wechatFansId;
    }

    public void setWechatFansId(Long wechatFansId) {
        this.wechatFansId = wechatFansId;
    }

    public Long getWechatFansGroupId() {
        return wechatFansGroupId;
    }

    public void setWechatFansGroupId(Long wechatFansGroupId) {
        this.wechatFansGroupId = wechatFansGroupId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }
}