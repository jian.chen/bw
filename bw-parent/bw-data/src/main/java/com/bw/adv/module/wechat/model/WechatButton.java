/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-main
 * File Name:Button.java
 * Package Name:com.sage.scrm.controller.wechat
 * Date:2015年9月5日下午1:26:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model;

import java.util.List;

/**
 * ClassName:Button <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月5日 下午1:26:33 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class WechatButton {
	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 */
	private static final long serialVersionUID = 1L;
	
	private String  id;
	private String  pid;
	private String  tempId;
	private String  menu_name;
	private String type;
	private String key;
	private String menu_key;
	private String url;
	private String media_id;
	private List<WechatButton> sub_button;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMedia_id() {
		return media_id;
	}
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	public List<WechatButton> getSub_button() {
		return sub_button;
	}
	public void setSub_button(List<WechatButton> sub_button) {
		this.sub_button = sub_button;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getMenu_name() {
		return menu_name;
	}
	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}
	public String getMenu_key() {
		return menu_key;
	}
	public void setMenu_key(String menu_key) {
		this.menu_key = menu_key;
	}
	public String getTempId() {
		return tempId;
	}
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
}

