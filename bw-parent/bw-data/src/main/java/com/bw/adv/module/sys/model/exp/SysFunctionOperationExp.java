package com.bw.adv.module.sys.model.exp;

import com.bw.adv.module.sys.model.OperationItem;
import com.bw.adv.module.sys.model.SysFunctionOperation_bak;

import java.util.List;

public class SysFunctionOperationExp extends SysFunctionOperation_bak {

	private List<OperationItem> operationItemList;

	public List<OperationItem> getOperationItemList() {
		return operationItemList;
	}

	public void setOperationItemList(List<OperationItem> operationItemList) {
		this.operationItemList = operationItemList;
	}



}