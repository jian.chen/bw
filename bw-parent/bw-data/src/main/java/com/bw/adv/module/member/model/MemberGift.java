package com.bw.adv.module.member.model;

public class MemberGift {
	/**
     * 
     */
    private Long giftId;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 礼品编码
     */
    private String giftCode;

    /**
     * 奖品名称
     */
    private String giftName;

    /**
     * 
     */
    private Long couponId;

    /**
     * 优惠券实例编码
     */
    private Long couponInstanceId;

    /**
     * 奖品类型，1-实物奖品 2，优惠券
     */
    private String giftType;

    /**
     * 礼品领取状态
     */
    private Long giftStatus;

    /**
     * 奖项id
     */
    private Long awardsId;

    /**
     * 获得途径，兑换，抽奖
     */
    private Long channelId;

    /**
     * 单位
     */
    private String giftUnit;

    /**
     * 备注
     */
    private String reason;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 
     * @return gift_id 
     */
    public Long getGiftId() {
        return giftId;
    }

    /**
     * 
     * @param giftId 
     */
    public void setGiftId(Long giftId) {
        this.giftId = giftId;
    }

    /**
     * 会员id
     * @return member_id 会员id
     */
    public Long getMemberId() {
        return memberId;
    }

    /**
     * 会员id
     * @param memberId 会员id
     */
    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    /**
     * 礼品编码
     * @return gift_code 礼品编码
     */
    public String getGiftCode() {
        return giftCode;
    }

    /**
     * 礼品编码
     * @param giftCode 礼品编码
     */
    public void setGiftCode(String giftCode) {
        this.giftCode = giftCode == null ? null : giftCode.trim();
    }

    /**
     * 奖品名称
     * @return gift_name 奖品名称
     */
    public String getGiftName() {
        return giftName;
    }

    /**
     * 奖品名称
     * @param giftName 奖品名称
     */
    public void setGiftName(String giftName) {
        this.giftName = giftName == null ? null : giftName.trim();
    }

    /**
     * 
     * @return coupon_id 
     */
    public Long getCouponId() {
        return couponId;
    }

    /**
     * 
     * @param couponId 
     */
    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    /**
     * 优惠券实例编码
     * @return coupon_instance_id 优惠券实例编码
     */
    public Long getCouponInstanceId() {
        return couponInstanceId;
    }

    /**
     * 优惠券实例编码
     * @param couponInstanceId 优惠券实例编码
     */
    public void setCouponInstanceId(Long couponInstanceId) {
        this.couponInstanceId = couponInstanceId;
    }

    /**
     * 奖品类型，1-实物奖品 2，优惠券
     * @return gift_type 奖品类型，1-实物奖品 2，优惠券
     */
    public String getGiftType() {
        return giftType;
    }

    /**
     * 奖品类型，1-实物奖品 2，优惠券
     * @param giftType 奖品类型，1-实物奖品 2，优惠券
     */
    public void setGiftType(String giftType) {
        this.giftType = giftType == null ? null : giftType.trim();
    }

    /**
     * 礼品领取状态
     * @return gift_status 礼品领取状态
     */
    public Long getGiftStatus() {
        return giftStatus;
    }

    /**
     * 礼品领取状态
     * @param giftStatus 礼品领取状态
     */
    public void setGiftStatus(Long giftStatus) {
        this.giftStatus = giftStatus;
    }

    /**
     * 奖项id
     * @return awards_id 奖项id
     */
    public Long getAwardsId() {
        return awardsId;
    }

    /**
     * 奖项id
     * @param awardsId 奖项id
     */
    public void setAwardsId(Long awardsId) {
        this.awardsId = awardsId;
    }

    /**
     * 获得途径，兑换，抽奖
     * @return channel_id 获得途径，兑换，抽奖
     */
    public Long getChannelId() {
        return channelId;
    }

    /**
     * 获得途径，兑换，抽奖
     * @param channelId 获得途径，兑换，抽奖
     */
    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    /**
     * 单位
     * @return gift_unit 单位
     */
    public String getGiftUnit() {
        return giftUnit;
    }

    /**
     * 单位
     * @param giftUnit 单位
     */
    public void setGiftUnit(String giftUnit) {
        this.giftUnit = giftUnit == null ? null : giftUnit.trim();
    }

    /**
     * 备注
     * @return reason 备注
     */
    public String getReason() {
        return reason;
    }

    /**
     * 备注
     * @param reason 备注
     */
    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }
}