package com.bw.adv.module.member.enums;

import org.apache.commons.lang.StringUtils;



public enum GradeChangeTypeEnums{
	
	AUTO("1", "自动"),
	HANDLE("2", "手动");
	
	private String id;
	private String desc;
	
	private GradeChangeTypeEnums(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static GradeChangeTypeEnums[] getArray() {
		return GradeChangeTypeEnums.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			GradeChangeTypeEnums[] values = GradeChangeTypeEnums.values();
			for(GradeChangeTypeEnums logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}
