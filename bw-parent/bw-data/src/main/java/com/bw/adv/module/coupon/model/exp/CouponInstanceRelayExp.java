/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:CouponInstanceRelayExp.java
 * Package Name:com.sage.scrm.module.coupon.model.exp
 * Date:2016年5月31日下午4:39:01
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.model.exp;

import com.bw.adv.module.coupon.model.CouponInstanceRelay;

/**
 * ClassName:CouponInstanceRelayExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年5月31日 下午4:39:01 <br/>
 * scrmVersion 1.0
 * @author   zhangqi
 * @version  jdk1.7
 * @see 	 
 */
public class CouponInstanceRelayExp extends CouponInstanceRelay{    
    //优惠券名称
    private String couponName;
    //优惠券编号
    private String couponCode;
    //转出人编号
    private String srcCode;
    //接收人编号
    private String getCode;
    //接收时间
    private String getTime;
    //回退时间
    private String rollbackDate;
    //过期时间
    private String expireDate;
    //状态
    private Long statusId;    
    //转出人id
    private Long srcId;    
    //创建时间(转出时间)开始
    private String createTimeStart;
    //创建时间(转出时间)结束
    private String createTimeEnd;
    //接收时间  开始
    private String startGetTime;
    //接收时间  结束
    private String endGetTime;
    
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public String getGetTime() {
		return getTime;
	}
	public void setGetTime(String getTime) {
		this.getTime = getTime;
	}
	public String getRollbackDate() {
		return rollbackDate;
	}
	public void setRollbackDate(String rollbackDate) {
		this.rollbackDate = rollbackDate;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getCreateTimeStart() {
		return createTimeStart;
	}
	public void setCreateTimeStart(String createTimeStart) {
		this.createTimeStart = createTimeStart;
	}
	public String getCreateTimeEnd() {
		return createTimeEnd;
	}
	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}
	public String getStartGetTime() {
		return startGetTime;
	}
	public void setStartGetTime(String startGetTime) {
		this.startGetTime = startGetTime;
	}
	public String getEndGetTime() {
		return endGetTime;
	}
	public void setEndGetTime(String endGetTime) {
		this.endGetTime = endGetTime;
	}
	public Long getSrcId() {
		return srcId;
	}
	public void setSrcId(Long srcId) {
		this.srcId = srcId;
	}
	public String getSrcCode() {
		return srcCode;
	}
	public void setSrcCode(String srcCode) {
		this.srcCode = srcCode;
	}
	public String getGetCode() {
		return getCode;
	}
	public void setGetCode(String getCode) {
		this.getCode = getCode;
	}
}

