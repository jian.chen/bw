package com.bw.adv.module.component.rule.model;

public class ConditionParameterValue {
    private Long conditionParameterValueId;

    private Long conditionParameterId;

    private Long conditionsValueId;

    private String parameterValue;

    public Long getConditionParameterValueId() {
        return conditionParameterValueId;
    }

    public void setConditionParameterValueId(Long conditionParameterValueId) {
        this.conditionParameterValueId = conditionParameterValueId;
    }

    public Long getConditionParameterId() {
        return conditionParameterId;
    }

    public void setConditionParameterId(Long conditionParameterId) {
        this.conditionParameterId = conditionParameterId;
    }

    public Long getConditionsValueId() {
        return conditionsValueId;
    }

    public void setConditionsValueId(Long conditionsValueId) {
        this.conditionsValueId = conditionsValueId;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }
}