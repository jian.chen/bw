package com.bw.adv.module.coupon.model.exp;

import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.tools.Excel;

/**
 * ClassName: 优惠券扩展类<br/>
 * date: 2015-8-11 上午11:09:53 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class CouponInstanceExp extends CouponInstance {

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "用户名", importConvertSign = 0)
    private String memberName;
    
    private String memberCode;
    
    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "手机号", importConvertSign = 0)
    private String memberMobile;
    
    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "优惠券码", importConvertSign = 0)
    private String couponCode;
    
    private String issueDate;
    
    @Excel(exportConvertSign = 1, exportFieldWidth = 10, exportName = "发放状态", importConvertSign = 0)
    private String issueStatus;
    
    private String comments;
    
    private String couponImg3;
    private String couponImg2;
    private String remark;
    
    private String webchatRemark;
    
    private String bindingAccount;
    
    private String extField1;
    
    private String couponTypeCode;

    //BK特有套餐
//	private String promotionId;
	//BK特有折扣
//	private String compId;
    
    @Excel(exportConvertSign = 1, exportFieldWidth = 10, exportName = "状态", importConvertSign = 0)
    private Long statusId;

    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "核销时间", importConvertSign = 0)
    private String usedTime;
    
    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "发放时间", importConvertSign = 0)
    private String startDate;
    
    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "来源", importConvertSign = 0)
    private String activityInstanceName;
    
    private String isOwnerUsed;
    
    private String isReceive;
    
    private String isRemind;
    
    private String storeName;
    
    private String couponNameEn;
	
	private String commentsEn;
	
	private String remarkEn;
    
    private String extField1En;
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "兑换码", importConvertSign = 0)
	private String exchangeCode;

    private String externalCode;

	private Integer originId;
	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getIssueStatus() {
		return issueStatus;
	}

	public void setIssueStatus(String issueStatus) {
		this.issueStatus = issueStatus;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public String getCouponImg2() {
		return couponImg2;
	}

	public void setCouponImg2(String couponImg2) {
		this.couponImg2 = couponImg2;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCouponImg3() {
		return couponImg3;
	}

	public void setCouponImg3(String couponImg3) {
		this.couponImg3 = couponImg3;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBindingAccount() {
		return bindingAccount;
	}

	public void setBindingAccount(String bindingAccount) {
		this.bindingAccount = bindingAccount;
	}

	public String getExtField1() {
		return extField1;
	}

	public void setExtField1(String extField1) {
		this.extField1 = extField1;
	}

	public String getCouponTypeCode() {
		return couponTypeCode;
	}

	public void setCouponTypeCode(String couponTypeCode) {
		this.couponTypeCode = couponTypeCode;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(String usedTime) {
		this.usedTime = usedTime;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getActivityInstanceName() {
		return activityInstanceName;
	}

	public void setActivityInstanceName(String activityInstanceName) {
		this.activityInstanceName = activityInstanceName;
	}
	
	public String getStatusIdConvert() {
		if(statusId == null){
			return "";
		}else if(statusId.equals(1601L)){
			return "已创建";
		}else if(statusId.equals(1602L)){
			return "未使用";
		}else if(statusId.equals(1603L)){
			return "已使用";
		}else if(statusId.equals(1604L)){
			return "已过期";
		}else if(statusId.equals(1605L)){
			return "已作废";
		}else if(statusId.equals(1606L)){
			return "使用中";
		}else if(statusId.equals(1607L)){
			return "分享中";
		}else if(statusId.equals(1608L)){
			return "已冻结";
		}else{
			return "未知";
		}
	}
	
	public String getIssueStatusConvert() {
		if(issueStatus == null){
			return "";
		}else if(issueStatus.equals("1501")){
			return "待推送";
		}else if(issueStatus.equals("1502")){
			return "已推送";
		}else if(issueStatus.equals("1503")){
			return "推送中";
		}else if(issueStatus.equals("1504")){
			return "未发放";
		}else{
			return "未知";
		}
	}

	public String getIsOwnerUsed() {
		return isOwnerUsed;
	}

	public void setIsOwnerUsed(String isOwnerUsed) {
		this.isOwnerUsed = isOwnerUsed;
	}

	public String getIsReceive() {
		return isReceive;
	}

	public void setIsReceive(String isReceive) {
		this.isReceive = isReceive;
	}

	public String getIsRemind() {
		return isRemind;
	}

	public void setIsRemind(String isRemind) {
		this.isRemind = isRemind;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getCouponNameEn() {
		return couponNameEn;
	}

	public void setCouponNameEn(String couponNameEn) {
		this.couponNameEn = couponNameEn;
	}

	public String getCommentsEn() {
		return commentsEn;
	}

	public void setCommentsEn(String commentsEn) {
		this.commentsEn = commentsEn;
	}

	public String getRemarkEn() {
		return remarkEn;
	}

	public void setRemarkEn(String remarkEn) {
		this.remarkEn = remarkEn;
	}

	public String getWebchatRemark() {
		return webchatRemark;
	}

	public void setWebchatRemark(String webchatRemark) {
		this.webchatRemark = webchatRemark;
	}

	public String getExtField1En() {
		return extField1En;
	}

	public void setExtField1En(String extField1En) {
		this.extField1En = extField1En;
	}

	public String getExchangeCode() {
		return exchangeCode;
	}

	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

	public Integer getOriginId() {
		return originId;
	}

	public void setOriginId(Integer originId) {
		this.originId = originId;
	}

	/*public String getGetChannelIdConvert() {
		if(getChannelId == null){
			return "";
		}else if(getChannelId.equals(401L)){
			return "会员系统";
		}else if(getChannelId.equals(501L)){
			return "系统发放";
		}else if(getChannelId.equals(502L)){
			return "手动领取";
		}else if(getChannelId.equals(503L)){
			return "活动发放";
		}else if(getChannelId.equals(504L)){
			return "积分兑换";
		}else{
			return "未知";
		}
	}*/

//	public String getPromotionId() {
//		return promotionId;
//	}
//
//	public void setPromotionId(String promotionId) {
//		this.promotionId = promotionId;
//	}
//
//	public String getCompId() {
//		return compId;
//	}
//
//	public void setCompId(String compId) {
//		this.compId = compId;
//	}
//	
}