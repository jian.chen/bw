/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:PointsRuleTimeTypeEnum.java
 * Package Name:com.sage.scrm.module.sys.enums
 * Date:2015年8月20日下午4:29:42
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.enums;

import org.apache.commons.lang.StringUtils;

/**
 * ClassName:PointsRuleTimeTypeEnum <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月20日 下午4:29:42 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public enum PointsRuleTimeTypeEnum {
	
	FIXED("1","固定日期"),
	PERIOD("2","时间段"),
	AFTER_EFFECT("3","多久之后生效"),
	FOREVER("4","永久");
	
	private String id;
	private String desc;
	
	private PointsRuleTimeTypeEnum(String id,String desc){
		this.id = id;
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

	public static PointsRuleTimeTypeEnum[] getArray(){
		return PointsRuleTimeTypeEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			PointsRuleTimeTypeEnum[] values = PointsRuleTimeTypeEnum.values();
			for(PointsRuleTimeTypeEnum pointsRuleType : values){
				if(pointsRuleType.getId().equals(id)){
					return pointsRuleType.getDesc();
				}
			}
		}
		return "";
	}
	

}

