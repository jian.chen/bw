package com.bw.adv.module.member.model;

public class MemberCardType {
    private Long memberCardTypeId;

    private String memberCardTypeCode;

    private String memberCardTypeName;

    public Long getMemberCardTypeId() {
        return memberCardTypeId;
    }

    public void setMemberCardTypeId(Long memberCardTypeId) {
        this.memberCardTypeId = memberCardTypeId;
    }

    public String getMemberCardTypeCode() {
        return memberCardTypeCode;
    }

    public void setMemberCardTypeCode(String memberCardTypeCode) {
        this.memberCardTypeCode = memberCardTypeCode;
    }

    public String getMemberCardTypeName() {
        return memberCardTypeName;
    }

    public void setMemberCardTypeName(String memberCardTypeName) {
        this.memberCardTypeName = memberCardTypeName;
    }
}