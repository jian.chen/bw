/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatMessageAutoReplyExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年8月22日下午3:39:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;

import java.util.List;

import com.bw.adv.module.wechat.model.WechatMessageAutoReply;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;

/**
 * ClassName:WechatMessageAutoReplyExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月22日 下午3:39:25 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WechatMessageAutoReplyExp extends WechatMessageAutoReply {
	
	private List<WechatMessageTemplate> wechatMessageTemplateList;

	public List<WechatMessageTemplate> getWechatMessageTemplateList() {
		return wechatMessageTemplateList;
	}

	public void setWechatMessageTemplateList(
			List<WechatMessageTemplate> wechatMessageTemplateList) {
		this.wechatMessageTemplateList = wechatMessageTemplateList;
	}
	
	
	
}

