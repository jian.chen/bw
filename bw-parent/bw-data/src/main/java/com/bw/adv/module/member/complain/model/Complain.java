package com.bw.adv.module.member.complain.model;

public class Complain {
    private Long complainId;
    
    private String complainCode;

    private Long complainTypeId;

    private Long memberId;

    private Long geoId;

    private Long storeId;
    
    private String storeName;

    private String openId;

    private String personName;

    private String mobile;

    private String email;

    private String complainDesc;

    private String createTime;
    
    private String updateTime;

    private String complainImg1;

    private String complainImg2;

    private String complainImg3;

    private String complainImg4;

    private String complainImg5;

    private String remark;

    private Long statusId;

    private String isMember; 
    
    private Integer satisfaction;
    
    private String satisfactionDesc;
    
    private Long operatorId;
    
    private String operatorName;

    public Long getComplainId() {
        return complainId;
    }

    public void setComplainId(Long complainId) {
        this.complainId = complainId;
    }

    public String getComplainCode() {
		return complainCode;
	}

	public void setComplainCode(String complainCode) {
		this.complainCode = complainCode;
	}

	public Long getComplainTypeId() {
        return complainTypeId;
    }

    public void setComplainTypeId(Long complainTypeId) {
        this.complainTypeId = complainTypeId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getGeoId() {
        return geoId;
    }

    public void setGeoId(Long geoId) {
        this.geoId = geoId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComplainDesc() {
        return complainDesc;
    }

    public void setComplainDesc(String complainDesc) {
        this.complainDesc = complainDesc;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getComplainImg1() {
        return complainImg1;
    }

    public void setComplainImg1(String complainImg1) {
        this.complainImg1 = complainImg1;
    }

    public String getComplainImg2() {
        return complainImg2;
    }

    public void setComplainImg2(String complainImg2) {
        this.complainImg2 = complainImg2;
    }

    public String getComplainImg3() {
        return complainImg3;
    }

    public void setComplainImg3(String complainImg3) {
        this.complainImg3 = complainImg3;
    }

    public String getComplainImg4() {
        return complainImg4;
    }

    public void setComplainImg4(String complainImg4) {
        this.complainImg4 = complainImg4;
    }

    public String getComplainImg5() {
        return complainImg5;
    }

    public void setComplainImg5(String complainImg5) {
        this.complainImg5 = complainImg5;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

	public String getIsMember() {
		return isMember;
	}

	public void setIsMember(String isMember) {
		this.isMember = isMember;
	}

	public Integer getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(Integer satisfaction) {
		this.satisfaction = satisfaction;
	}

	public String getSatisfactionDesc() {
		return satisfactionDesc;
	}

	public void setSatisfactionDesc(String satisfactionDesc) {
		this.satisfactionDesc = satisfactionDesc;
	}

	public Long getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
    
	
}