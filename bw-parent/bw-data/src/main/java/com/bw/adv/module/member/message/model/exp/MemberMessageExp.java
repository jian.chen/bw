/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberExp.java
 * Package Name:com.sage.scrm.module.member.model.exp
 * Date:2015年8月11日下午3:44:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.member.message.model.exp;

import com.bw.adv.module.member.message.model.MemberMessage;

/**
 * ClassName: MemberMessageExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年2月28日 下午11:24:20 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public class MemberMessageExp extends MemberMessage {
	private String isRead;

	public String getIsRead() {
		return isRead;
	}

	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}
	
}
