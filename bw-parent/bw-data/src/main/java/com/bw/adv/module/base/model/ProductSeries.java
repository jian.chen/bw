package com.bw.adv.module.base.model;

public class ProductSeries {
    private Long productSeriesId;

    private String productSeriesCode;

    private String productSeriesName;

    private String productSeriesDesc;

    private String createTime;

    private String updateTime;

    public Long getProductSeriesId() {
        return productSeriesId;
    }

    public void setProductSeriesId(Long productSeriesId) {
        this.productSeriesId = productSeriesId;
    }

    public String getProductSeriesCode() {
        return productSeriesCode;
    }

    public void setProductSeriesCode(String productSeriesCode) {
        this.productSeriesCode = productSeriesCode;
    }

    public String getProductSeriesName() {
        return productSeriesName;
    }

    public void setProductSeriesName(String productSeriesName) {
        this.productSeriesName = productSeriesName;
    }

    public String getProductSeriesDesc() {
        return productSeriesDesc;
    }

    public void setProductSeriesDesc(String productSeriesDesc) {
        this.productSeriesDesc = productSeriesDesc;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}