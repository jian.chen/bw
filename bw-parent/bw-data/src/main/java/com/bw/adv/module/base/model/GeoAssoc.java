package com.bw.adv.module.base.model;

public class GeoAssoc extends GeoAssocKey {
    private Long geoAssocTypeId;

    public Long getGeoAssocTypeId() {
        return geoAssocTypeId;
    }

    public void setGeoAssocTypeId(Long geoAssocTypeId) {
        this.geoAssocTypeId = geoAssocTypeId;
    }
}