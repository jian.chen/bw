package com.bw.adv.module.badge.model.exp;

import com.bw.adv.module.badge.model.MemberBadge;

public class MemberBadgeExt  extends MemberBadge{
	private String awardsCode;

	public String getAwardsCode() {
		return awardsCode;
	}

	public void setAwardsCode(String awardsCode) {
		this.awardsCode = awardsCode;
	}
	
}