package com.bw.adv.module.base.model;

public class ProductCategoryItem {
    private Long productCategoryItemId;

    private Long productCategoryId;

    private Long productId;

    public Long getProductCategoryItemId() {
        return productCategoryItemId;
    }

    public void setProductCategoryItemId(Long productCategoryItemId) {
        this.productCategoryItemId = productCategoryItemId;
    }

    public Long getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}