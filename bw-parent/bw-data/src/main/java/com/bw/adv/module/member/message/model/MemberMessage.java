package com.bw.adv.module.member.message.model;

public class MemberMessage {
    private Long memberMessageId;

    private String title;

    private String messageDesc;

    private String messageImg1;

    private String messageImg2;

    private String messageImg3;

    private String createTime;

    private String updateTime;

    private String sendTime;

    private Long createBy;

    private Long updateBy;

    private String isAll;

    private Long statusId;

    private String messageContent;

    public Long getMemberMessageId() {
        return memberMessageId;
    }

    public void setMemberMessageId(Long memberMessageId) {
        this.memberMessageId = memberMessageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessageDesc() {
        return messageDesc;
    }

    public void setMessageDesc(String messageDesc) {
        this.messageDesc = messageDesc;
    }

    public String getMessageImg1() {
        return messageImg1;
    }

    public void setMessageImg1(String messageImg1) {
        this.messageImg1 = messageImg1;
    }

    public String getMessageImg2() {
        return messageImg2;
    }

    public void setMessageImg2(String messageImg2) {
        this.messageImg2 = messageImg2;
    }

    public String getMessageImg3() {
        return messageImg3;
    }

    public void setMessageImg3(String messageImg3) {
        this.messageImg3 = messageImg3;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getIsAll() {
        return isAll;
    }

    public void setIsAll(String isAll) {
        this.isAll = isAll;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }
}