package com.bw.adv.module.goods.model;

import java.math.BigDecimal;

public class GoodsExchangeItem {
    private Long goodsExchangeItemId;

    private Long goodsShowId;

    private Long memberId;

    private BigDecimal pointsNumber;

    private String createTime;

    private Long statusId;

    private String remark;

    public Long getGoodsExchangeItemId() {
        return goodsExchangeItemId;
    }

    public void setGoodsExchangeItemId(Long goodsExchangeItemId) {
        this.goodsExchangeItemId = goodsExchangeItemId;
    }

    public Long getGoodsShowId() {
        return goodsShowId;
    }

    public void setGoodsShowId(Long goodsShowId) {
        this.goodsShowId = goodsShowId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getPointsNumber() {
        return pointsNumber;
    }

    public void setPointsNumber(BigDecimal pointsNumber) {
        this.pointsNumber = pointsNumber;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}