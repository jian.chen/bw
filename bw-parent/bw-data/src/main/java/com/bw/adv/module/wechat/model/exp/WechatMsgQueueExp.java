/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatMsgQueueExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年12月10日下午8:15:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;

import com.bw.adv.module.wechat.model.WechatMsgQueue;

/**
 * ClassName:WechatMsgQueueExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月10日 下午8:15:41 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class WechatMsgQueueExp extends WechatMsgQueue{
	private String openId;
	private String memberName;
	private String memberCode;
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	
}

