package com.bw.adv.module.wechat.model;

public class WechatMessageTemplate {
	
	/**
	 * 消息模板ID
	 */
    private Long wechatMessageTemplateId;
    
    /**
     * 微信账号ID
     */
    private Long wechatAccountId;
    
    /**
     * 状态ID
     */
    private Long statusId;
    
    /**
     * 标题
     */
    private String title;
    
    /**
     * 描述
     */
    private String description;
    
    /**
     * 图片Url
     */
    private String pictureUrl;
    
    /**
     * 跳转url
     */
    private String linkUrl;	
    
    /**
     * 内容
     */
    private String content;
    
    /**
     * 是否封面
     */
    private String isCover;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
    private String createTime;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateTime;
    
    /**
     * 删除标识
     */
    private String isDeleted;
    
    /**
     * 消息类型
     */
    private String wechatMessageType;
    
    /**
     * 媒体ID
     */
    private String mediaId;
    
    /**
     * 父级ID
     */
    private Long parentId;
    
    /**
     * 作者
     */
    private String author;
    
    /**
     * 规则分组id
     */
    private Long keysGroupId;
    
    /**
     * 缩略图mediaId
     */
    private String thumbId;

    public String getThumbId() {
		return thumbId;
	}

	public void setThumbId(String thumbId) {
		this.thumbId = thumbId;
	}

	public Long getKeysGroupId() {
		return keysGroupId;
	}

	public void setKeysGroupId(Long keysGroupId) {
		this.keysGroupId = keysGroupId;
	}

	public Long getWechatMessageTemplateId() {
        return wechatMessageTemplateId;
    }

    public void setWechatMessageTemplateId(Long wechatMessageTemplateId) {
        this.wechatMessageTemplateId = wechatMessageTemplateId;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIsCover() {
        return isCover;
    }

    public void setIsCover(String isCover) {
        this.isCover = isCover;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getWechatMessageType() {
        return wechatMessageType;
    }

    public void setWechatMessageType(String wechatMessageType) {
        this.wechatMessageType = wechatMessageType;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}