/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:GoodsShowExp.java
 * Package Name:com.sage.scrm.module.goods.model.exp
 * Date:2015年12月10日下午2:48:48
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.model.exp;

import java.math.BigDecimal;

import com.bw.adv.module.goods.model.GoodsShow;

/**
 * ClassName:GoodsShowExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月10日 下午2:48:48 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class GoodsShowExp extends GoodsShow {
	
	private String couponNameEn;
	
	private String couponRemarkEn;
	
	private String extField1En;
	
	private String commentsEn;
	
	private String couponName;
	
	private String goodsShowCategoryName;
	
	private String fromDate;
	
	private String thruDate;
	
	private String afterDate;
	
	private String expireValue;
	
	private String couponRemark;
	
	private String couponImg2;
	
	private Long couponId;
	
	private String comments;
	
	private String couponImg3;
	
	private String extField1;

	private String goodsShowMemberCardGradeList;
	
	
	/**
	 * 兑换所需等级文案
	 */
	private String requireLevelRemark;
	
	/**
	 * 所需积分
	 */
	private BigDecimal requirePoints;

	/**
	 * 显示文案
	 */
	private String showArchives;
	
	/**
	 * 显示文案英文
	 */
	private String showArchivesEn;
	

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	
	public String getGoodsShowCategoryName() {
		return goodsShowCategoryName;
	}

	public void setGoodsShowCategoryName(String goodsShowCategoryName) {
		this.goodsShowCategoryName = goodsShowCategoryName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getThruDate() {
		return thruDate;
	}

	public void setThruDate(String thruDate) {
		this.thruDate = thruDate;
	}

	public String getAfterDate() {
		return afterDate;
	}

	public void setAfterDate(String afterDate) {
		this.afterDate = afterDate;
	}

	public String getExpireValue() {
		return expireValue;
	}

	public void setExpireValue(String expireValue) {
		this.expireValue = expireValue;
	}

	public String getCouponRemark() {
		return couponRemark;
	}

	public void setCouponRemark(String couponRemark) {
		this.couponRemark = couponRemark;
	}

	public String getCouponImg2() {
		return couponImg2;
	}

	public void setCouponImg2(String couponImg2) {
		this.couponImg2 = couponImg2;
	}

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCouponImg3() {
		return couponImg3;
	}

	public void setCouponImg3(String couponImg3) {
		this.couponImg3 = couponImg3;
	}

	public String getExtField1() {
		return extField1;
	}

	public void setExtField1(String extField1) {
		this.extField1 = extField1;
	}

	public String getRequireLevelRemark() {
		return requireLevelRemark;
	}

	public void setRequireLevelRemark(String requireLevelRemark) {
		this.requireLevelRemark = requireLevelRemark;
	}
	
	public String getGoodsShowMemberCardGradeList() {
		return goodsShowMemberCardGradeList;
	}

	public void setGoodsShowMemberCardGradeList(
			String goodsShowMemberCardGradeList) {
		this.goodsShowMemberCardGradeList = goodsShowMemberCardGradeList;
	}
	public BigDecimal getRequirePoints() {
		return requirePoints;
	}

	public void setRequirePoints(BigDecimal requirePoints) {
		this.requirePoints = requirePoints;
	}
	public String getShowArchives() {
		return showArchives;
	}

	public void setShowArchives(String showArchives) {
		this.showArchives = showArchives;
	}

	public String getCouponNameEn() {
		return couponNameEn;
	}

	public void setCouponNameEn(String couponNameEn) {
		this.couponNameEn = couponNameEn;
	}

	public String getCouponRemarkEn() {
		return couponRemarkEn;
	}

	public void setCouponRemarkEn(String couponRemarkEn) {
		this.couponRemarkEn = couponRemarkEn;
	}

	public String getCommentsEn() {
		return commentsEn;
	}

	public void setCommentsEn(String commentsEn) {
		this.commentsEn = commentsEn;
	}

	public String getExtField1En() {
		return extField1En;
	}

	public void setExtField1En(String extField1En) {
		this.extField1En = extField1En;
	}

	public String getShowArchivesEn() {
		return showArchivesEn;
	}

	public void setShowArchivesEn(String showArchivesEn) {
		this.showArchivesEn = showArchivesEn;
	}

}

