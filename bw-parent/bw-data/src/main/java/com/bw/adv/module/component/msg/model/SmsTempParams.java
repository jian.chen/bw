package com.bw.adv.module.component.msg.model;

public class SmsTempParams {
    private Long smsTempParamsId;

    private String smsTempParamsCode;

    private String smsTempParamsName;

    public Long getSmsTempParamsId() {
        return smsTempParamsId;
    }

    public void setSmsTempParamsId(Long smsTempParamsId) {
        this.smsTempParamsId = smsTempParamsId;
    }

    public String getSmsTempParamsCode() {
        return smsTempParamsCode;
    }

    public void setSmsTempParamsCode(String smsTempParamsCode) {
        this.smsTempParamsCode = smsTempParamsCode;
    }

    public String getSmsTempParamsName() {
        return smsTempParamsName;
    }

    public void setSmsTempParamsName(String smsTempParamsName) {
        this.smsTempParamsName = smsTempParamsName;
    }
}