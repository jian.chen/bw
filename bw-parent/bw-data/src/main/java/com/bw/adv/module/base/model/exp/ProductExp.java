/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:ProductExp.java
 * Package Name:com.sage.scrm.module.base.model.exp
 * Date:2016年1月14日上午10:40:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.model.exp;

import com.bw.adv.module.base.model.Product;

/**
 * ClassName:产品扩展类 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 上午10:40:50 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
public class ProductExp extends Product{

	private String productCategory;//产品类别整合（将哟个产品的多个类别综合在一起）

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	
	
}

