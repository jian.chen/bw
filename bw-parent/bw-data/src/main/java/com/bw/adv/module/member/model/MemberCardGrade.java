package com.bw.adv.module.member.model;

public class MemberCardGrade {
    private Long memberCardGradeId;

    private Long memberCardTypeId;

    private Long gradeId;

    private Long memberCardStyleId;

    private String memberCardGradeCode;

    private String memberCardGradeName;

    private String backgroundColor;

    private String imgUrl;

    private String memberCardDesc;

    private String remark;

    public Long getMemberCardGradeId() {
        return memberCardGradeId;
    }

    public void setMemberCardGradeId(Long memberCardGradeId) {
        this.memberCardGradeId = memberCardGradeId;
    }

    public Long getMemberCardTypeId() {
        return memberCardTypeId;
    }

    public void setMemberCardTypeId(Long memberCardTypeId) {
        this.memberCardTypeId = memberCardTypeId;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getMemberCardStyleId() {
        return memberCardStyleId;
    }

    public void setMemberCardStyleId(Long memberCardStyleId) {
        this.memberCardStyleId = memberCardStyleId;
    }

    public String getMemberCardGradeCode() {
        return memberCardGradeCode;
    }

    public void setMemberCardGradeCode(String memberCardGradeCode) {
        this.memberCardGradeCode = memberCardGradeCode;
    }

    public String getMemberCardGradeName() {
        return memberCardGradeName;
    }

    public void setMemberCardGradeName(String memberCardGradeName) {
        this.memberCardGradeName = memberCardGradeName;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getMemberCardDesc() {
        return memberCardDesc;
    }

    public void setMemberCardDesc(String memberCardDesc) {
        this.memberCardDesc = memberCardDesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}