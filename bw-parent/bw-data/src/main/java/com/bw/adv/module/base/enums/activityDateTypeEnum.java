package com.bw.adv.module.base.enums;

import org.apache.commons.lang.StringUtils;



public enum activityDateTypeEnum {
	
	FIXED("1", "固定日期"),
	PERIOD("2", "固定时间段"),
	CYCLE("3", "滚动日期");

	
	private String id;
	private String desc;
	
	private activityDateTypeEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static activityDateTypeEnum[] getArray() {
		return activityDateTypeEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			activityDateTypeEnum[] values = activityDateTypeEnum.values();
			for(activityDateTypeEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}
