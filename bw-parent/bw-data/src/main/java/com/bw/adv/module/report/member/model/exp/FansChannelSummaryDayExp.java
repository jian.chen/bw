/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:FansChannelSummaryDayExp.java
 * Package Name:com.sage.scrm.module.report.member.model.exp
 * Date:2015年11月30日上午9:54:48
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.member.model.exp;

import com.bw.adv.module.report.member.model.FansChannelSummaryDay;
import com.bw.adv.module.tools.Excel;

/**
 * ClassName:FansChannelSummaryDayExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月30日 上午9:54:48 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class FansChannelSummaryDayExp extends FansChannelSummaryDay{
	
	String channelName;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "渠道名称", importConvertSign = 0)
	String qrCodeName;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "新增粉丝数", importConvertSign = 0)
	private Integer dailyAddNumber;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "总粉丝数", importConvertSign = 0)
	private Integer totalNumber;
	

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "注册会员数", importConvertSign = 0)
	private Integer memberNumber;
	
	int fansNum;
	
	String startDate; //统计时间筛选字段
	
	String endDate;

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	
	public String getQrCodeName() {
		return qrCodeName;
	}

	public void setQrCodeName(String qrCodeName) {
		this.qrCodeName = qrCodeName;
	}

	public int getFansNum() {
		return fansNum;
	}

	public void setFansNum(int fansNum) {
		this.fansNum = fansNum;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public Integer getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(Integer memberNumber) {
		this.memberNumber = memberNumber;
	}

}

