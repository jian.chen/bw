package com.bw.adv.module.sys.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.sys.model.LogType;

public class LogTypeConstant extends AbstractConstant<LogType>{

	
	public static final LogTypeConstant OPT_LOG = new LogTypeConstant(1L, "操作日志");
    public static final LogTypeConstant SYS_LOG = new LogTypeConstant(2L, "系统日志");
    public static final LogTypeConstant API_LOG = new LogTypeConstant(3L, "接口日志");
    public static final LogTypeConstant TASK_LOG = new LogTypeConstant(4L, "任务日志");
	
    
	public LogTypeConstant(Long id, String name) {
		super(id, name);
	}
	
	
	@Override
	public LogType getInstance() {
		LogType logType = new LogType();
		logType.setLogTypeId(id);
		logType.setLogTypeName(name);
		return logType;
	}
	
}