package com.bw.adv.module.member.model;

public class GradeConf {
    private Long gradeConfId;

    private Long gradeFactorsId;

    private String changeType;

    private String upRule;

    private String downRule;

    private String upPeriod;

    private String downPeriod;

    private String remark;

    private String isActive;

    public Long getGradeConfId() {
        return gradeConfId;
    }

    public void setGradeConfId(Long gradeConfId) {
        this.gradeConfId = gradeConfId;
    }

    public Long getGradeFactorsId() {
        return gradeFactorsId;
    }

    public void setGradeFactorsId(Long gradeFactorsId) {
        this.gradeFactorsId = gradeFactorsId;
    }

    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    public String getUpRule() {
        return upRule;
    }

    public void setUpRule(String upRule) {
        this.upRule = upRule;
    }

    public String getDownRule() {
        return downRule;
    }

    public void setDownRule(String downRule) {
        this.downRule = downRule;
    }

    public String getUpPeriod() {
        return upPeriod;
    }

    public void setUpPeriod(String upPeriod) {
        this.upPeriod = upPeriod;
    }

    public String getDownPeriod() {
        return downPeriod;
    }

    public void setDownPeriod(String downPeriod) {
        this.downPeriod = downPeriod;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}