/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:PrizeInstanceExp.java
 * Package Name:com.sage.scrm.module.activity.model.exp
 * Date:2015年11月26日上午11:00:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.model.exp;

import com.bw.adv.module.activity.model.PrizeInstance;

/**
 * ClassName:PrizeInstanceExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 上午11:00:19 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class PrizeInstanceExp extends PrizeInstance {
	
	private String prizeName;
	private String prizeEn;
	
	
	
	public String getPrizeName() {
		return prizeName;
	}
	public void setPrizeName(String prizeName) {
		this.prizeName = prizeName;
	}
	public String getPrizeEn() {
		return prizeEn;
	}
	public void setPrizeEn(String prizeEn) {
		this.prizeEn = prizeEn;
	}
	
	
	
}

