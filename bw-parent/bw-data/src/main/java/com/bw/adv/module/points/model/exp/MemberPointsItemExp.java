/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:MemberPointsItemExp.java
 * Package Name:com.sage.scrm.module.points.model.exp
 * Date:2015年8月12日上午11:09:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.points.model.exp;

import java.math.BigDecimal;

import com.bw.adv.module.points.model.MemberPointsItem;

/**
 * ClassName:MemberPointsItemExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月12日 上午11:09:33 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class MemberPointsItemExp extends MemberPointsItem {
	
	private Long memberAccountId;
	
	private String pointsRuleName;
	
	private String pointsTypeName;
	
	private String memberCode;
	
	//发生时间筛选
	private String happenTimeStart;
	
	private String happenTimeEnd;
	//过期时间筛选
	private String invalidTimeStart;
	
	private String invalidTimeEnd;
	
	private BigDecimal accountPointsBalance;
	
	private String storeName;
	
	private BigDecimal orderAmount;
	
	private String activityInstanceName;
	
	private String createUserName;
	
	private String memberName;
	
	private String cardNo;
	
	private String mobile;
	
	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getUpdateUserName() {
		return updateUserName;
	}

	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

	private String updateUserName;

	public String getPointsRuleName() {
		return pointsRuleName;
	}

	public void setPointsRuleName(String pointsRuleName) {
		this.pointsRuleName = pointsRuleName;
	}

	public String getPointsTypeName() {
		return pointsTypeName;
	}

	public void setPointsTypeName(String pointsTypeName) {
		this.pointsTypeName = pointsTypeName;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getHappenTimeStart() {
		return happenTimeStart;
	}

	public void setHappenTimeStart(String happenTimeStart) {
		this.happenTimeStart = happenTimeStart;
	}

	public String getHappenTimeEnd() {
		return happenTimeEnd;
	}

	public void setHappenTimeEnd(String happenTimeEnd) {
		this.happenTimeEnd = happenTimeEnd;
	}

	public String getInvalidTimeStart() {
		return invalidTimeStart;
	}

	public void setInvalidTimeStart(String invalidTimeStart) {
		this.invalidTimeStart = invalidTimeStart;
	}

	public String getInvalidTimeEnd() {
		return invalidTimeEnd;
	}

	public void setInvalidTimeEnd(String invalidTimeEnd) {
		this.invalidTimeEnd = invalidTimeEnd;
	}

	public Long getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(Long memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	public BigDecimal getAccountPointsBalance() {
		return accountPointsBalance;
	}

	public void setAccountPointsBalance(BigDecimal accountPointsBalance) {
		this.accountPointsBalance = accountPointsBalance;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public String getActivityInstanceName() {
		return activityInstanceName;
	}

	public void setActivityInstanceName(String activityInstanceName) {
		this.activityInstanceName = activityInstanceName;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
}

