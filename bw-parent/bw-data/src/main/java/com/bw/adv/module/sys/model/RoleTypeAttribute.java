package com.bw.adv.module.sys.model;

public class RoleTypeAttribute {
    private Long roleTypeAttributeId;

    private Long roleTypeId;

    private String attrName;

    private String attrValue;

    private String createBy;

    private String createTime;

    private String lastModifiedBy;

    private String lastModifiedTime;

    public Long getRoleTypeAttributeId() {
        return roleTypeAttributeId;
    }

    public void setRoleTypeAttributeId(Long roleTypeAttributeId) {
        this.roleTypeAttributeId = roleTypeAttributeId;
    }

    public Long getRoleTypeId() {
        return roleTypeId;
    }

    public void setRoleTypeId(Long roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(String lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }
}