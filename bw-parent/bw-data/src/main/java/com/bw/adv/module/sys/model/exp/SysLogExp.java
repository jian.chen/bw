/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:SysLogExp.java
 * Package Name:com.sage.scrm.module.sys.model.exp
 * Date:2015年9月1日下午8:45:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.model.exp;

import com.bw.adv.module.sys.model.SysLog;

/**
 * ClassName:SysLogExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月1日 下午8:45:06 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class SysLogExp extends SysLog {
	
	private String createTimeStart;
	
	private String createTimeEnd;
	
	private String logDateStart;
	
	private String logDateEnd;
	
	private String functionName;

	public String getCreateTimeStart() {
		return createTimeStart;
	}

	public void setCreateTimeStart(String createTimeStart) {
		this.createTimeStart = createTimeStart;
	}

	public String getCreateTimeEnd() {
		return createTimeEnd;
	}

	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}

	public String getLogDateStart() {
		return logDateStart;
	}

	public void setLogDateStart(String logDateStart) {
		this.logDateStart = logDateStart;
	}

	public String getLogDateEnd() {
		return logDateEnd;
	}

	public void setLogDateEnd(String logDateEnd) {
		this.logDateEnd = logDateEnd;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	
	

}

