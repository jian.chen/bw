/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:MenuMsgType.java
 * Package Name:com.sage.scrm.module.base.enums
 * Date:2015年12月14日下午9:00:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.enums;

import org.apache.commons.lang.StringUtils;

/**
 * ClassName:MenuMsgType <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月14日 下午9:00:24 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public enum menuMsgTypeEnum {
	CLICK("1", "固定日期"),
	VIEW("2", "固定时间段");

	
	private String id;
	private String desc;
	
	private menuMsgTypeEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static menuMsgTypeEnum[] getArray() {
		return menuMsgTypeEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			menuMsgTypeEnum[] values = menuMsgTypeEnum.values();
			for(menuMsgTypeEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}

