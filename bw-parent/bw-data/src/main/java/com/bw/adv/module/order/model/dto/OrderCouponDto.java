package com.bw.adv.module.order.model.dto;

import java.io.Serializable;

import com.bw.adv.module.order.model.OrderCoupon;

/**
 * ClassName: OrderCouponDto <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-28 下午5:58:14 <br/>
 * scrmVersion 1.0
 * 
 * @author mennan
 * @version jdk1.7
 */
public class OrderCouponDto extends OrderCoupon implements Serializable {

	private static final long serialVersionUID = -2468166794550884557L;

	private String externalItemId;
	private String couponInstanceCode;

	public String getExternalItemId() {
		return externalItemId;
	}

	public void setExternalItemId(String externalItemId) {
		this.externalItemId = externalItemId;
	}

	public String getCouponInstanceCode() {
		return couponInstanceCode;
	}

	public void setCouponInstanceCode(String couponInstanceCode) {
		this.couponInstanceCode = couponInstanceCode;
	}

}
