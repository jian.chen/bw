package com.bw.adv.module.sys.model;

public class SysLog {
    private Long logId;

    private Long logTypeId;

    private Long logEventId;

    private Long functionId;

    private Integer logLevel;

    private String logNode;
    
    private String createTime;

    private String logDate;

    private String logTime;

    private Long sysPartyId;

    private String sysPartyCn;

    private String logSource;

    private String logUpdate;

    private String logKeyword;

    private String logOptIp;

    private String remark;

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Long getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(Long logTypeId) {
        this.logTypeId = logTypeId;
    }

    public Long getLogEventId() {
        return logEventId;
    }

    public void setLogEventId(Long logEventId) {
        this.logEventId = logEventId;
    }

    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public Integer getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(Integer logLevel) {
        this.logLevel = logLevel;
    }

    public String getLogNode() {
        return logNode;
    }

    public void setLogNode(String logNode) {
        this.logNode = logNode;
    }

    public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public Long getSysPartyId() {
        return sysPartyId;
    }

    public void setSysPartyId(Long sysPartyId) {
        this.sysPartyId = sysPartyId;
    }

    public String getSysPartyCn() {
        return sysPartyCn;
    }

    public void setSysPartyCn(String sysPartyCn) {
        this.sysPartyCn = sysPartyCn;
    }

    public String getLogSource() {
        return logSource;
    }

    public void setLogSource(String logSource) {
        this.logSource = logSource;
    }

    public String getLogUpdate() {
        return logUpdate;
    }

    public void setLogUpdate(String logUpdate) {
        this.logUpdate = logUpdate;
    }

    public String getLogKeyword() {
        return logKeyword;
    }

    public void setLogKeyword(String logKeyword) {
        this.logKeyword = logKeyword;
    }

    public String getLogOptIp() {
        return logOptIp;
    }

    public void setLogOptIp(String logOptIp) {
        this.logOptIp = logOptIp;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}