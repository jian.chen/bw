package com.bw.adv.module.common.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.base.model.DataType;

public class DataTypeConstant extends AbstractConstant<DataType> {

	public static final DataTypeConstant TYPE_INT = new DataTypeConstant(1L, "TYPE_INT", "整型");
	public static final DataTypeConstant TYPE_FLOAT = new DataTypeConstant(2L, "TYPE_FLOAT", "浮点型");
	public static final DataTypeConstant TYPE_DOUBLE = new DataTypeConstant(3L, "TYPE_DOUBLE", "双精度");
	public static final DataTypeConstant TYPE_LONG = new DataTypeConstant(4L, "TYPE_LONG", "长整型");
	public static final DataTypeConstant TYPE_STRING = new DataTypeConstant(5L, "TYPE_STRING", "字符型");
	
	public DataTypeConstant(Long id, String code, String name) {
		super(id, code, name);
	}

	@Override
	public DataType getInstance() {
		DataType dataType = new DataType();
		dataType.setDataTypeId(id);
		dataType.setDataTypeCode(code);
		dataType.setDataTypeName(name);
		return dataType;
	}
	
	

}
