package com.bw.adv.module.order.model;

import java.math.BigDecimal;

/**
 * ClassName: OrderPaymentMethod <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-15 下午3:58:00 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class OrderPaymentMethod {
	
    private Long orderPaymentMethodId;
    private Long orderId;
    private Long paymentMethodId;
    private String paymentMethodCode;
    private String paymentMethodName;
    private String payCode;
    private BigDecimal payAmount;
    private String createTime;
    private String payTime;
    private Long statusId;

    public Long getOrderPaymentMethodId() {
        return orderPaymentMethodId;
    }

    public void setOrderPaymentMethodId(Long orderPaymentMethodId) {
        this.orderPaymentMethodId = orderPaymentMethodId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodCode() {
        return paymentMethodCode;
    }

    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}