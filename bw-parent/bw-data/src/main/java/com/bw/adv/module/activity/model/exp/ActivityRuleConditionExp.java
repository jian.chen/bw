/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:Snippet.java
 * Package Name:com.sage.scrm.module.activity.model.exp
 * Date:2015-9-1下午5:31:25
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.model.exp;


/**
 * ClassName:ActivityInstanceExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-9-1 下午5:31:25 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */

public class ActivityRuleConditionExp {
	
	private Long activityId;
	private String activityName;
	private String activityCode;
	
	private Long activityInstanceId;
	private String activityInstanceName;
	
	private Long conditionsId;
	private String conditionName;
	private String conditionsCode;
	
	private Long conditionsValueId;
	private String conditionsValues;
	
	
	public Long getActivityId() {
		return activityId;
	}
	
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	
	public String getActivityName() {
		return activityName;
	}
	
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
	public String getActivityCode() {
		return activityCode;
	}
	
	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}
	
	public Long getActivityInstanceId() {
		return activityInstanceId;
	}
	
	public void setActivityInstanceId(Long activityInstanceId) {
		this.activityInstanceId = activityInstanceId;
	}
	
	public String getActivityInstanceName() {
		return activityInstanceName;
	}

	public void setActivityInstanceName(String activityInstanceName) {
		this.activityInstanceName = activityInstanceName;
	}
	
	public Long getConditionsId() {
		return conditionsId;
	}
	
	public void setConditionsId(Long conditionsId) {
		this.conditionsId = conditionsId;
	}

	public String getConditionName() {
		return conditionName;
	}
	
	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}
	
	public String getConditionsCode() {
		return conditionsCode;
	}

	public void setConditionsCode(String conditionsCode) {
		this.conditionsCode = conditionsCode;
	}
	
	public Long getConditionsValueId() {
		return conditionsValueId;
	}

	public void setConditionsValueId(Long conditionsValueId) {
		this.conditionsValueId = conditionsValueId;
	}
	
	public String getConditionsValues() {
		return conditionsValues;
	}
	
	public void setConditionsValues(String conditionsValues) {
		this.conditionsValues = conditionsValues;
	}
	
}






