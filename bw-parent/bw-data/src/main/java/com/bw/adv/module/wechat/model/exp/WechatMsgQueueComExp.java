/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatMsgQueueComExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年12月17日上午10:34:19
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;
/**
 * ClassName:WechatMsgQueueComExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 上午10:34:19 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public class WechatMsgQueueComExp {
	private Long wechatMsgQueueId;

    private Long memberId;

    private Integer businessType;

    private String msgKey;

    private String createTime;

    private String sendTime;

    private String msgContent;

    private String isSend;
    
    private String openId;
    
	private String memberName;
	
	private String memberCode;

	public Long getWechatMsgQueueId() {
		return wechatMsgQueueId;
	}

	public void setWechatMsgQueueId(Long wechatMsgQueueId) {
		this.wechatMsgQueueId = wechatMsgQueueId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Integer getBusinessType() {
		return businessType;
	}

	public void setBusinessType(Integer businessType) {
		this.businessType = businessType;
	}

	public String getMsgKey() {
		return msgKey;
	}

	public void setMsgKey(String msgKey) {
		this.msgKey = msgKey;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public String getIsSend() {
		return isSend;
	}

	public void setIsSend(String isSend) {
		this.isSend = isSend;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	
	
}

