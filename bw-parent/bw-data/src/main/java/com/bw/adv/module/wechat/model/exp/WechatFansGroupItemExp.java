/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatFansGroupItemExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年9月1日下午2:31:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;

import java.util.List;

import com.bw.adv.module.wechat.model.WechatFans;
import com.bw.adv.module.wechat.model.WechatFansGroupItem;

/**
 * ClassName:WechatFansGroupItemExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月1日 下午2:31:46 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WechatFansGroupItemExp extends WechatFansGroupItem {
	
	private List<WechatFans> WechatFansList;

	public List<WechatFans> getWechatFansList() {
		return WechatFansList;
	}

	public void setWechatFansList(List<WechatFans> wechatFansList) {
		WechatFansList = wechatFansList;
	}
	
}

