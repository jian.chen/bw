package com.bw.adv.module.badge.model;

public class MemberBadge {
    /**
     * 主键
     */
    private Long memberBadgeId;

    /**
     * 徽章id
     */
    private Long badageId;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 徽章类型
     */
    private Integer badgeType;

    /**
     * 徽章总数量
     */
    private Integer badgeTotalNum;

    /**
     * 徽章可用数量
     */
    private Integer badgeAvailableNum;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 修改时间
     */
    private String updateTime;

    /**
     * 主键
     * @return MEMBER_BADGE_ID 主键
     */
    public Long getMemberBadgeId() {
        return memberBadgeId;
    }

    /**
     * 主键
     * @param memberBadgeId 主键
     */
    public void setMemberBadgeId(Long memberBadgeId) {
        this.memberBadgeId = memberBadgeId;
    }

    /**
     * 徽章id
     * @return BADAGE_ID 徽章id
     */
    public Long getBadageId() {
        return badageId;
    }

    /**
     * 徽章id
     * @param badageId 徽章id
     */
    public void setBadageId(Long badageId) {
        this.badageId = badageId;
    }

    /**
     * 会员id
     * @return MEMBER_ID 会员id
     */
    public Long getMemberId() {
        return memberId;
    }

    /**
     * 会员id
     * @param memberId 会员id
     */
    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    /**
     * 徽章类型
     * @return BADGE_TYPE 徽章类型
     */
    public Integer getBadgeType() {
        return badgeType;
    }

    /**
     * 徽章类型
     * @param badgeType 徽章类型
     */
    public void setBadgeType(Integer badgeType) {
        this.badgeType = badgeType;
    }

    /**
     * 徽章总数量
     * @return BADGE_TOTAL_NUM 徽章总数量
     */
    public Integer getBadgeTotalNum() {
        return badgeTotalNum;
    }

    /**
     * 徽章总数量
     * @param badgeTotalNum 徽章总数量
     */
    public void setBadgeTotalNum(Integer badgeTotalNum) {
        this.badgeTotalNum = badgeTotalNum;
    }

    /**
     * 徽章可用数量
     * @return BADGE_AVAILABLE_NUM 徽章可用数量
     */
    public Integer getBadgeAvailableNum() {
        return badgeAvailableNum;
    }

    /**
     * 徽章可用数量
     * @param badgeAvailableNum 徽章可用数量
     */
    public void setBadgeAvailableNum(Integer badgeAvailableNum) {
        this.badgeAvailableNum = badgeAvailableNum;
    }

    /**
     * 创建时间
     * @return CREATE_TIME 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 修改时间
     * @return UPDATE_TIME 修改时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 修改时间
     * @param updateTime 修改时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }
}