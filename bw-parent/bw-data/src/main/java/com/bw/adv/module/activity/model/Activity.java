package com.bw.adv.module.activity.model;

public class Activity {
    private Long activityId;

    private String activityCode;

    private Long activityVersion;

    private Long activityTypeId;

    private Long channelId;

    private String activityName;

    private String activityTitle;

    private String activitySubTitle;

    private String activityDesc;

    private String ruleDesc;

    private String activityImg1;

    private String activityImg2;

    private String activityImg3;

    private String fromDate;

    private String thruDate;

    private String createTime;

    private Integer activityAmount;

    private String isActive;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public Long getActivityVersion() {
        return activityVersion;
    }

    public void setActivityVersion(Long activityVersion) {
        this.activityVersion = activityVersion;
    }

    public Long getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(Long activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public String getActivitySubTitle() {
        return activitySubTitle;
    }

    public void setActivitySubTitle(String activitySubTitle) {
        this.activitySubTitle = activitySubTitle;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public String getRuleDesc() {
        return ruleDesc;
    }

    public void setRuleDesc(String ruleDesc) {
        this.ruleDesc = ruleDesc;
    }

    public String getActivityImg1() {
        return activityImg1;
    }

    public void setActivityImg1(String activityImg1) {
        this.activityImg1 = activityImg1;
    }

    public String getActivityImg2() {
        return activityImg2;
    }

    public void setActivityImg2(String activityImg2) {
        this.activityImg2 = activityImg2;
    }

    public String getActivityImg3() {
        return activityImg3;
    }

    public void setActivityImg3(String activityImg3) {
        this.activityImg3 = activityImg3;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getThruDate() {
        return thruDate;
    }

    public void setThruDate(String thruDate) {
        this.thruDate = thruDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getActivityAmount() {
        return activityAmount;
    }

    public void setActivityAmount(Integer activityAmount) {
        this.activityAmount = activityAmount;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}