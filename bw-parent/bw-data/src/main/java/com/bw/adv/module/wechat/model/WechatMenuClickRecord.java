package com.bw.adv.module.wechat.model;

public class WechatMenuClickRecord {
    private Long wechatMenuClickRecordId;

    private Long wechatMenuId;

    private Integer msgType;

    private String menuSign;

    private String openId;

    private String clickIp;

    private String createTime;

    private String remark;

    public Long getWechatMenuClickRecordId() {
        return wechatMenuClickRecordId;
    }

    public void setWechatMenuClickRecordId(Long wechatMenuClickRecordId) {
        this.wechatMenuClickRecordId = wechatMenuClickRecordId;
    }

    public Long getWechatMenuId() {
        return wechatMenuId;
    }

    public void setWechatMenuId(Long wechatMenuId) {
        this.wechatMenuId = wechatMenuId;
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public String getMenuSign() {
        return menuSign;
    }

    public void setMenuSign(String menuSign) {
        this.menuSign = menuSign;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getClickIp() {
        return clickIp;
    }

    public void setClickIp(String clickIp) {
        this.clickIp = clickIp;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}