package com.bw.adv.module.activity.model.exp;

import com.bw.adv.module.activity.model.SceneGame;

public class SceneGameExp extends SceneGame {
	
	private String pageUrl;

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	
}