package com.bw.adv.module.coupon.model.exp;

public class CouponResult {

	private Long couponId;
	
	private Integer quantity;
	
	private Long activityInstanceId;
	
	private Long orderId;//订单ID
	
	private String externalId;//订单外部编号
	

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getActivityInstanceId() {
		return activityInstanceId;
	}

	public void setActivityInstanceId(Long activityInstanceId) {
		this.activityInstanceId = activityInstanceId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	
}
