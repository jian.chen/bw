package com.bw.adv.module.activity.model;

public class VoteResult {
    private Long resultId;

    private Long memberId;

    private Long titleId;

    private Long optionsId;

    private String createTime;
    
    private String optionName;
    
    private Long memberSum;

    public Long getResultId() {
        return resultId;
    }

    public void setResultId(Long resultId) {
        this.resultId = resultId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public Long getOptionsId() {
        return optionsId;
    }

    public void setOptionsId(Long optionsId) {
        this.optionsId = optionsId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public Long getMemberSum() {
		return memberSum;
	}

	public void setMemberSum(Long memberSum) {
		this.memberSum = memberSum;
	}
}