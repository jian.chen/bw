package com.bw.adv.module.coupon.model;

public class CouponCategoryItem {
    private Long couponCategoryItem;

    private Long couponCategoryId;

    private Long couponId;

    private Long statusId;
    
    private Integer seq;

    public Long getCouponCategoryItem() {
        return couponCategoryItem;
    }

    public void setCouponCategoryItem(Long couponCategoryItem) {
        this.couponCategoryItem = couponCategoryItem;
    }

    public Long getCouponCategoryId() {
        return couponCategoryId;
    }

    public void setCouponCategoryId(Long couponCategoryId) {
        this.couponCategoryId = couponCategoryId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
    
    
}