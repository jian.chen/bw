/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:AwardsSettingExp.java
 * Package Name:com.sage.scrm.module.activity.model.exp
 * Date:2015年11月26日下午9:03:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.model.exp;

import com.bw.adv.module.activity.model.AwardsSetting;

/**
 * ClassName:AwardsSettingExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 下午9:03:28 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class AwardsSettingExp extends AwardsSetting {
	
	private String awardsName;
	private String awardsEn;
	private String awardsCode;
	private String couponCodeInstance;
	
	
	public String getAwardsName() {
		return awardsName;
	}
	public void setAwardsName(String awardsName) {
		this.awardsName = awardsName;
	}
	public String getAwardsEn() {
		return awardsEn;
	}
	public void setAwardsEn(String awardsEn) {
		this.awardsEn = awardsEn;
	}
	public String getAwardsCode() {
		return awardsCode;
	}
	public void setAwardsCode(String awardsCode) {
		this.awardsCode = awardsCode;
	}
	public String getCouponCodeInstance() {
		return couponCodeInstance;
	}
	public void setCouponCodeInstance(String couponCodeInstance) {
		this.couponCodeInstance = couponCodeInstance;
	}
	
	
	
}

