package com.bw.adv.module.member.message.model;

public class MemberMessageItemBrowse {
    private Long memberMessageItemBrowseId;

    private Long memberMessageItemId;

    private Long memberMessageId;

    private String browseTime;

    private String openId;

    private String browseIp;

    private String closeTime;

    private Integer timeLength;

    public Long getMemberMessageItemBrowseId() {
        return memberMessageItemBrowseId;
    }

    public void setMemberMessageItemBrowseId(Long memberMessageItemBrowseId) {
        this.memberMessageItemBrowseId = memberMessageItemBrowseId;
    }

    public Long getMemberMessageItemId() {
        return memberMessageItemId;
    }

    public void setMemberMessageItemId(Long memberMessageItemId) {
        this.memberMessageItemId = memberMessageItemId;
    }

    public Long getMemberMessageId() {
        return memberMessageId;
    }

    public void setMemberMessageId(Long memberMessageId) {
        this.memberMessageId = memberMessageId;
    }

    public String getBrowseTime() {
        return browseTime;
    }

    public void setBrowseTime(String browseTime) {
        this.browseTime = browseTime;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getBrowseIp() {
        return browseIp;
    }

    public void setBrowseIp(String browseIp) {
        this.browseIp = browseIp;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public Integer getTimeLength() {
        return timeLength;
    }

    public void setTimeLength(Integer timeLength) {
        this.timeLength = timeLength;
    }
}