package com.bw.adv.module.alipay.model;

public class AlipayFans {
    private Long alipayFansId;

    private String avatar;

    private String nickName;

    private String city;

    private String province;

    private String gender;

    private String userTypeValue;

    private String isLicenceAuth;

    private String isCertified;

    private String isCertifyGradeA;

    private String isStudentCertified;

    private String isBankAuth;

    private String isMobileAuth;

    private String alipayUserId;

    private String userStatus;

    private String isIdAuth;

    private Long createBy;

    private String createTime;

    private Long updateBy;

    private String updateTime;

    private String isDelete;

    private Long memberId;
    
    private String mobile;
    public Long getAlipayFansId() {
        return alipayFansId;
    }

    public void setAlipayFansId(Long alipayFansId) {
        this.alipayFansId = alipayFansId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserTypeValue() {
        return userTypeValue;
    }

    public void setUserTypeValue(String userTypeValue) {
        this.userTypeValue = userTypeValue;
    }

    public String getIsLicenceAuth() {
        return isLicenceAuth;
    }

    public void setIsLicenceAuth(String isLicenceAuth) {
        this.isLicenceAuth = isLicenceAuth;
    }

    public String getIsCertified() {
        return isCertified;
    }

    public void setIsCertified(String isCertified) {
        this.isCertified = isCertified;
    }

    public String getIsCertifyGradeA() {
        return isCertifyGradeA;
    }

    public void setIsCertifyGradeA(String isCertifyGradeA) {
        this.isCertifyGradeA = isCertifyGradeA;
    }

    public String getIsStudentCertified() {
        return isStudentCertified;
    }

    public void setIsStudentCertified(String isStudentCertified) {
        this.isStudentCertified = isStudentCertified;
    }

    public String getIsBankAuth() {
        return isBankAuth;
    }

    public void setIsBankAuth(String isBankAuth) {
        this.isBankAuth = isBankAuth;
    }

    public String getIsMobileAuth() {
        return isMobileAuth;
    }

    public void setIsMobileAuth(String isMobileAuth) {
        this.isMobileAuth = isMobileAuth;
    }

    public String getAlipayUserId() {
        return alipayUserId;
    }

    public void setAlipayUserId(String alipayUserId) {
        this.alipayUserId = alipayUserId;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getIsIdAuth() {
        return isIdAuth;
    }

    public void setIsIdAuth(String isIdAuth) {
        this.isIdAuth = isIdAuth;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
    
}