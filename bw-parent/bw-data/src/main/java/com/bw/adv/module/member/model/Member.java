package com.bw.adv.module.member.model;

import java.math.BigDecimal;

import com.bw.adv.module.tools.Excel;

public class Member {
	
	private Long memberId;
	
	private Long memberOriginId;
	
	private Long channelId;

	private Long orgId;
	
	private String orgName;

	private Long geoId;
	
	private String geoName;

	private Long gradeId;

	private String memberCode;
	
	private String memberGroupIds;//冗余会员组
	
	private String memberTagIds;//冗余会员标签

	private String email;

	private String emailCheck;

	@Excel(exportName = "手机号码", exportFieldWidth = 30, exportConvertSign = 0, importConvertSign = 0)
	private String mobile;

	private String mobileCheck;

	private String perfectCheck;

	private BigDecimal perfectPercentage;

	private String loginPassword;

	private String memberName;
	
	private String userName;
	
	private String memberPhoto;

	private Long gender;

	private String birthday;

	private String registerTime;
	
	private String registerDate;//注册日期MMDD

	private String registerIp;

	private String lastLoginTime;

	private String lastLoginIp;

	private Long lastLoginCity;

	private String firstConsumptionTime;
	
	private String firstConsumptionDate;
	
	private String lastConsumptionTime;

	private Long lastConsumptionCity;

	private Long statusId;

	private Long activeStatusId;

	private Long consumptionStatusId;
	
	private String language;
	
	private String isHasChild;
	
	private String gradeName;
	
	private String isEmployee;
	
	private Long regStore;
	
	private Long memberSrcId;
	
	private String extField1;//孩子生日1

    private String extField2;//孩子生日2

    private String extField3;//新品试吃

    private String extField4;//儿童披萨制作派对

    private String extField5;//交友派对
    
    private String extField6;//注册来源门店
    
    private String hasEntityCard;//是否有实体卡
	private String countryCode;

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	
	public Long getMemberOriginId() {
		return memberOriginId;
	}

	public void setMemberOriginId(Long memberOriginId) {
		this.memberOriginId = memberOriginId;
	}

	public Long getChannelId() {
		return channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getGeoId() {
		return geoId;
	}

	public void setGeoId(Long geoId) {
		this.geoId = geoId;
	}

	public Long getGradeId() {
		return gradeId;
	}

	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}
	
	public String getMemberGroupIds() {
		return memberGroupIds;
	}

	public void setMemberGroupIds(String memberGroupIds) {
		this.memberGroupIds = memberGroupIds;
	}

	public String getMemberTagIds() {
		return memberTagIds;
	}

	public void setMemberTagIds(String memberTagIds) {
		this.memberTagIds = memberTagIds;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailCheck() {
		return emailCheck;
	}

	public void setEmailCheck(String emailCheck) {
		this.emailCheck = emailCheck;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobileCheck() {
		return mobileCheck;
	}

	public void setMobileCheck(String mobileCheck) {
		this.mobileCheck = mobileCheck;
	}

	public String getPerfectCheck() {
		return perfectCheck;
	}

	public void setPerfectCheck(String perfectCheck) {
		this.perfectCheck = perfectCheck;
	}

	public BigDecimal getPerfectPercentage() {
		return perfectPercentage;
	}

	public void setPerfectPercentage(BigDecimal perfectPercentage) {
		this.perfectPercentage = perfectPercentage;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getMemberPhoto() {
		return memberPhoto;
	}

	public void setMemberPhoto(String memberPhoto) {
		this.memberPhoto = memberPhoto;
	}

	public Long getGender() {
		return gender;
	}

	public void setGender(Long gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}
	
	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public String getFirstConsumptionTime() {
		return firstConsumptionTime;
	}

	public void setFirstConsumptionTime(String firstConsumptionTime) {
		this.firstConsumptionTime = firstConsumptionTime;
	}
	
	public String getFirstConsumptionDate() {
		return firstConsumptionDate;
	}

	public void setFirstConsumptionDate(String firstConsumptionDate) {
		this.firstConsumptionDate = firstConsumptionDate;
	}

	public Long getLastLoginCity() {
		return lastLoginCity;
	}

	public void setLastLoginCity(Long lastLoginCity) {
		this.lastLoginCity = lastLoginCity;
	}

	public String getLastConsumptionTime() {
		return lastConsumptionTime;
	}

	public void setLastConsumptionTime(String lastConsumptionTime) {
		this.lastConsumptionTime = lastConsumptionTime;
	}

	public Long getLastConsumptionCity() {
		return lastConsumptionCity;
	}

	public void setLastConsumptionCity(Long lastConsumptionCity) {
		this.lastConsumptionCity = lastConsumptionCity;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getActiveStatusId() {
		return activeStatusId;
	}

	public void setActiveStatusId(Long activeStatusId) {
		this.activeStatusId = activeStatusId;
	}

	public Long getConsumptionStatusId() {
		return consumptionStatusId;
	}

	public void setConsumptionStatusId(Long consumptionStatusId) {
		this.consumptionStatusId = consumptionStatusId;
	}
	
	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getGeoName() {
		return geoName;
	}

	public void setGeoName(String geoName) {
		this.geoName = geoName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getIsHasChild() {
		return isHasChild;
	}

	public void setIsHasChild(String isHasChild) {
		this.isHasChild = isHasChild;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public String getIsEmployee() {
		return isEmployee;
	}

	public void setIsEmployee(String isEmployee) {
		this.isEmployee = isEmployee;
	}

	public String getExtField1() {
		return extField1;
	}

	public void setExtField1(String extField1) {
		this.extField1 = extField1;
	}

	public String getExtField2() {
		return extField2;
	}

	public void setExtField2(String extField2) {
		this.extField2 = extField2;
	}

	public String getExtField3() {
		return extField3;
	}

	public void setExtField3(String extField3) {
		this.extField3 = extField3;
	}

	public String getExtField4() {
		return extField4;
	}

	public void setExtField4(String extField4) {
		this.extField4 = extField4;
	}

	public String getExtField5() {
		return extField5;
	}

	public void setExtField5(String extField5) {
		this.extField5 = extField5;
	}

	public String getExtField6() {
		return extField6;
	}

	public void setExtField6(String extField6) {
		this.extField6 = extField6;
	}

	public Long getRegStore() {
		return regStore;
	}

	public void setRegStore(Long regStore) {
		this.regStore = regStore;
	}

	public Long getMemberSrcId() {
		return memberSrcId;
	}

	public void setMemberSrcId(Long memberSrcId) {
		this.memberSrcId = memberSrcId;
	}

	public String getHasEntityCard() {
		return hasEntityCard;
	}

	public void setHasEntityCard(String hasEntityCard) {
		this.hasEntityCard = hasEntityCard;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}