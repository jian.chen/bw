package com.bw.adv.module.member.exception;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.module.base.exception.BaseRuntimeException;
import com.bw.adv.module.base.exception.ErrorData;


public class MemberCardException extends BaseRuntimeException{
	private static final long serialVersionUID = 4203482434232111182L;

	public MemberCardException(ErrorData errorData) {
		
		super(errorData);
		
	}

	public MemberCardException(ResultStatus resultStatus) {
		
		super(resultStatus);
		// TODO Auto-generated constructor stub
		
	}

	public MemberCardException(String errorMsg, ErrorData errorData) {
		
		super(errorMsg, errorData);
		// TODO Auto-generated constructor stub
		
	}

	public MemberCardException(String errorMsg, Throwable e) {
		
		super(errorMsg, e);
		// TODO Auto-generated constructor stub
		
	}

	public MemberCardException(String errorMsg) {
		
		super(errorMsg);
		// TODO Auto-generated constructor stub
		
	}

	public MemberCardException(Throwable e) {
		
		super(e);
		// TODO Auto-generated constructor stub
		
	}
	

}
