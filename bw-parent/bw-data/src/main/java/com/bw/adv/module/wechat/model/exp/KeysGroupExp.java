/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:KeysGroupExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年8月26日下午6:12:12
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;

import java.util.List;

import com.bw.adv.module.wechat.model.KeysGroup;
import com.bw.adv.module.wechat.model.WechatMessageTemplate;

/**
 * ClassName:KeysGroupExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月26日 下午6:12:12 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class KeysGroupExp extends KeysGroup {
	
	private List<WechatMessageTemplate> wechatMessageTemplateList;

	public List<WechatMessageTemplate> getWechatMessageTemplateList() {
		return wechatMessageTemplateList;
	}

	public void setWechatMessageTemplateList(
			List<WechatMessageTemplate> wechatMessageTemplateList) {
		this.wechatMessageTemplateList = wechatMessageTemplateList;
	}
	
}

