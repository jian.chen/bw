package com.bw.adv.module.activity.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.activity.model.Business;

public class BusinessConstant extends AbstractConstant<Business>{
	
	public static final BusinessConstant BUSINESS_ORDER = new BusinessConstant(1L,"BUSINESS_ORDER", "订单业务");
	public static final BusinessConstant BUSINESS_ORDER_CANCEL = new BusinessConstant(1L,"BUSINESS_ORDER_CANCEL", "订单业务-取消");
	public static final BusinessConstant BUSINESS_ACTIVITY = new BusinessConstant(1L,"BUSINESS_ACTIVITY", "活动业务");
	
	
	public BusinessConstant(Long id,String code, String name) {
		super(id, code,name);
	}

	@Override
	public Business getInstance() {
		Business activity = new Business();
		activity.setBusinessId(id);
		activity.setBusinessCode(code);
		activity.setBusinessName(name);
		return activity;
	}

	
}