/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:ExtAccountTypeConstant.java
 * Package Name:com.sage.scrm.module.member.constant
 * Date:2015年11月5日下午3:23:05
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.member.model.ExtAccountType;

/**
 * ClassName:ExtAccountTypeConstant <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月5日 下午3:23:05 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class ExtAccountTypeConstant extends AbstractConstant<ExtAccountType> {

	public static final ExtAccountTypeConstant ACCOUNT_TYPE_WECHAT = new ExtAccountTypeConstant(1L,"ACCOUNT_TYPE_WECHAT", "微信");
	public static final ExtAccountTypeConstant ACCOUNT_TYPE_ALIPAY = new ExtAccountTypeConstant(7L,"ACCOUNT_TYPE_ALIPAY", "支付宝");
	public static final ExtAccountTypeConstant ACCOUNT_TYPE_PC = new ExtAccountTypeConstant(8L,"ACCOUNT_TYPE_PC", "PC");
	
	public ExtAccountTypeConstant(Long id,String code, String name) {
		super(id, code,name);
	}

	@Override
	public ExtAccountType getInstance() {
		ExtAccountType extAccountType = new ExtAccountType();
		extAccountType.setExtAccountTypeId(id);
		extAccountType.setExtAccountTypeCode(code);
		extAccountType.setExtAccountTypeName(name);
		return extAccountType;
	}

	
	

}

