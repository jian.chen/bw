package com.bw.adv.module.sys.model;

public class LogEvent {
    private Long logEventId;

    private String logEventName;

    private String description;

    private String isActive;

    public Long getLogEventId() {
        return logEventId;
    }

    public void setLogEventId(Long logEventId) {
        this.logEventId = logEventId;
    }

    public String getLogEventName() {
        return logEventName;
    }

    public void setLogEventName(String logEventName) {
        this.logEventName = logEventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}