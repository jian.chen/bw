package com.bw.adv.module.member.model;

public class GradeFactors {
    private Long gradeFactorsId;

    private String gradeFactorsName;

    private String gradeFactorsCode;

    private String isActive;

    public Long getGradeFactorsId() {
        return gradeFactorsId;
    }

    public void setGradeFactorsId(Long gradeFactorsId) {
        this.gradeFactorsId = gradeFactorsId;
    }

    public String getGradeFactorsName() {
        return gradeFactorsName;
    }

    public void setGradeFactorsName(String gradeFactorsName) {
        this.gradeFactorsName = gradeFactorsName;
    }

    public String getGradeFactorsCode() {
        return gradeFactorsCode;
    }

    public void setGradeFactorsCode(String gradeFactorsCode) {
        this.gradeFactorsCode = gradeFactorsCode;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}