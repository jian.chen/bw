package com.bw.adv.module.points.model;

import java.math.BigDecimal;

public class PointsRule {
	
    private Long pointsRuleId;

    private Long pointsTypeId;

    private String pointsRuleCode;

    private String pointsRuleName;

    private String pointsRuleDesc;

    private String pointsRuleType;

    private BigDecimal pointsAmount;

    private String invalidTimeType;

    private String invalidTimeValue;

    private String cleanTime;

    private String isActive;
    
    private String createTime;
    
    private String updateTime;
    

    public Long getPointsRuleId() {
        return pointsRuleId;
    }

    public void setPointsRuleId(Long pointsRuleId) {
        this.pointsRuleId = pointsRuleId;
    }

    public Long getPointsTypeId() {
        return pointsTypeId;
    }

    public void setPointsTypeId(Long pointsTypeId) {
        this.pointsTypeId = pointsTypeId;
    }

    public String getPointsRuleCode() {
        return pointsRuleCode;
    }

    public void setPointsRuleCode(String pointsRuleCode) {
        this.pointsRuleCode = pointsRuleCode;
    }

    public String getPointsRuleName() {
        return pointsRuleName;
    }

    public void setPointsRuleName(String pointsRuleName) {
        this.pointsRuleName = pointsRuleName;
    }

    public String getPointsRuleDesc() {
        return pointsRuleDesc;
    }

    public void setPointsRuleDesc(String pointsRuleDesc) {
        this.pointsRuleDesc = pointsRuleDesc;
    }

    public String getPointsRuleType() {
        return pointsRuleType;
    }

    public void setPointsRuleType(String pointsRuleType) {
        this.pointsRuleType = pointsRuleType;
    }

    public BigDecimal getPointsAmount() {
        return pointsAmount;
    }

    public void setPointsAmount(BigDecimal pointsAmount) {
        this.pointsAmount = pointsAmount;
    }

    public String getInvalidTimeType() {
        return invalidTimeType;
    }

    public void setInvalidTimeType(String invalidTimeType) {
        this.invalidTimeType = invalidTimeType;
    }

    public String getInvalidTimeValue() {
        return invalidTimeValue;
    }

    public void setInvalidTimeValue(String invalidTimeValue) {
        this.invalidTimeValue = invalidTimeValue;
    }

    public String getCleanTime() {
        return cleanTime;
    }

    public void setCleanTime(String cleanTime) {
        this.cleanTime = cleanTime;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
    
}