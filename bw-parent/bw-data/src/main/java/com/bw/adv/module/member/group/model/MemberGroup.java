package com.bw.adv.module.member.group.model;

public class MemberGroup {
    private Long memberGroupId;

    private String memberGroupCode;
    
	private String memberGroupName;

    private String executeType;

    private String conditionSql;

    private String conditionRelation;

    private String remark;

    private String createTime;

    private String updateTime;

    private Long statusId;
    
    private Long isAll;

    public Long getIsAll() {
		return isAll;
	}

	public void setIsAll(Long isAll) {
		this.isAll = isAll;
	}

	public Long getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(Long memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public String getMemberGroupName() {
        return memberGroupName;
    }

    public void setMemberGroupName(String memberGroupName) {
        this.memberGroupName = memberGroupName;
    }

    public String getExecuteType() {
        return executeType;
    }

    public void setExecuteType(String executeType) {
        this.executeType = executeType;
    }

    public String getConditionSql() {
        return conditionSql;
    }

    public void setConditionSql(String conditionSql) {
        this.conditionSql = conditionSql;
    }

    public String getConditionRelation() {
        return conditionRelation;
    }

    public void setConditionRelation(String conditionRelation) {
        this.conditionRelation = conditionRelation;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
    
    public String getMemberGroupCode() {
		return memberGroupCode;
	}

	public void setMemberGroupCode(String memberGroupCode) {
		this.memberGroupCode = memberGroupCode;
	}
}