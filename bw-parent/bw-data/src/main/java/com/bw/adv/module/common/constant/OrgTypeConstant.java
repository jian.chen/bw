package com.bw.adv.module.common.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.base.model.OrgType;

public class OrgTypeConstant extends AbstractConstant<OrgType> {

	public static final OrgTypeConstant ORG_TYPE_BRAND = new OrgTypeConstant(1L, "ORG_TYPE_BRAND", "品牌");
	public static final OrgTypeConstant ORG_TYPE_AREA = new OrgTypeConstant(2L, "ORG_TYPE_AREA", "大区");
	public static final OrgTypeConstant ORG_TYPE_CITY = new OrgTypeConstant(3L, "ORG_TYPE_CITY", "城市");
	public static final OrgTypeConstant ORG_TYPE_STORE = new OrgTypeConstant(4L, "ORG_TYPE_STORE", "门店");
	public OrgTypeConstant(Long id, String code, String name) {
		super(id, code, name);
	}

	@Override
	public OrgType getInstance() {
		OrgType orgType = new OrgType();
		orgType.setOrgTypeId(id);
		orgType.setOrgTypeCode(code);
		orgType.setOrgTypeName(name);
		return orgType;
	}

}
