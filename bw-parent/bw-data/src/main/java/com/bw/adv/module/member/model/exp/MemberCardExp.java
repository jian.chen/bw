/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:Snippet.java
 * Package Name:com.sage.scrm.module.member.model.exp
 * Date:2015-12-17上午10:18:21
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.model.exp;

import com.bw.adv.module.member.model.MemberCard;

/**
 * ClassName:Snippet <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-12-17 上午10:18:21 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */

public class MemberCardExp extends MemberCard{
	
	private String memberCode;

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	
}

