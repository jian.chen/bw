package com.bw.adv.module.base.model;

public class QrCode {
	 /**
     * 
     */
    private Long qrCodeId;

    /**
     * 
     */
    private String qrCodeCode;

    /**
     * 
     */
    private String qrCodeName;

    /**
     * 
     */
    private Long qrTypeId;

    /**
     * 
     */
    private Long channelId;

    /**
     * 
     */
    private Long orgId;

    /**
     * 
     */
    private Long createUserId;

    /**
     * 
     */
    private String activityCode;

    /**
     * 
     */
    private Long updateUserId;

    /**
     * 
     */
    private String responseContent;

    /**
     * 
     */
    private Long responseNumber;

    /**
     * 
     */
    private String createTime;

    /**
     * 
     */
    private String updateTime;

    /**
     * 二维码链接
     */
    private String urlLink;

    /**
     * 
     */
    private String qrCodeDesc;

    /**
     * 
     */
    private String remark;

    /**
     * 
     */
    private String isActive;

    /**
     * 
     */
    private String field1;

    /**
     * 
     */
    private String field2;

    /**
     * 
     */
    private String field3;

    /**
     * 
     */
    private String field4;

    /**
     * 
     */
    private String field5;

    /**
     * 
     * @return QR_CODE_ID 
     */
    public Long getQrCodeId() {
        return qrCodeId;
    }

    /**
     * 
     * @param qrCodeId 
     */
    public void setQrCodeId(Long qrCodeId) {
        this.qrCodeId = qrCodeId;
    }

    /**
     * 
     * @return QR_CODE_CODE 
     */
    public String getQrCodeCode() {
        return qrCodeCode;
    }

    /**
     * 
     * @param qrCodeCode 
     */
    public void setQrCodeCode(String qrCodeCode) {
        this.qrCodeCode = qrCodeCode == null ? null : qrCodeCode.trim();
    }

    /**
     * 
     * @return QR_CODE_NAME 
     */
    public String getQrCodeName() {
        return qrCodeName;
    }

    /**
     * 
     * @param qrCodeName 
     */
    public void setQrCodeName(String qrCodeName) {
        this.qrCodeName = qrCodeName == null ? null : qrCodeName.trim();
    }

    /**
     * 
     * @return QR_TYPE_ID 
     */
    public Long getQrTypeId() {
        return qrTypeId;
    }

    /**
     * 
     * @param qrTypeId 
     */
    public void setQrTypeId(Long qrTypeId) {
        this.qrTypeId = qrTypeId;
    }

    /**
     * 
     * @return CHANNEL_ID 
     */
    public Long getChannelId() {
        return channelId;
    }

    /**
     * 
     * @param channelId 
     */
    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    /**
     * 
     * @return ORG_ID 
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 
     * @param orgId 
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 
     * @return CREATE_USER_ID 
     */
    public Long getCreateUserId() {
        return createUserId;
    }

    /**
     * 
     * @param createUserId 
     */
    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * 
     * @return ACTIVITY_CODE 
     */
    public String getActivityCode() {
        return activityCode;
    }

    /**
     * 
     * @param activityCode 
     */
    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode == null ? null : activityCode.trim();
    }

    /**
     * 
     * @return UPDATE_USER_ID 
     */
    public Long getUpdateUserId() {
        return updateUserId;
    }

    /**
     * 
     * @param updateUserId 
     */
    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    /**
     * 
     * @return RESPONSE_CONTENT 
     */
    public String getResponseContent() {
        return responseContent;
    }

    /**
     * 
     * @param responseContent 
     */
    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent == null ? null : responseContent.trim();
    }

    /**
     * 
     * @return RESPONSE_NUMBER 
     */
    public Long getResponseNumber() {
        return responseNumber;
    }

    /**
     * 
     * @param responseNumber 
     */
    public void setResponseNumber(Long responseNumber) {
        this.responseNumber = responseNumber;
    }

    /**
     * 
     * @return CREATE_TIME 
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 
     * @param createTime 
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 
     * @return UPDATE_TIME 
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 
     * @param updateTime 
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    /**
     * 二维码链接
     * @return URL_LINK 二维码链接
     */
    public String getUrlLink() {
        return urlLink;
    }

    /**
     * 二维码链接
     * @param urlLink 二维码链接
     */
    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink == null ? null : urlLink.trim();
    }

    /**
     * 
     * @return QR_CODE_DESC 
     */
    public String getQrCodeDesc() {
        return qrCodeDesc;
    }

    /**
     * 
     * @param qrCodeDesc 
     */
    public void setQrCodeDesc(String qrCodeDesc) {
        this.qrCodeDesc = qrCodeDesc == null ? null : qrCodeDesc.trim();
    }

    /**
     * 
     * @return REMARK 
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 
     * @param remark 
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 
     * @return IS_ACTIVE 
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * 
     * @param isActive 
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive == null ? null : isActive.trim();
    }

    /**
     * 
     * @return FIELD1 
     */
    public String getField1() {
        return field1;
    }

    /**
     * 
     * @param field1 
     */
    public void setField1(String field1) {
        this.field1 = field1 == null ? null : field1.trim();
    }

    /**
     * 
     * @return FIELD2 
     */
    public String getField2() {
        return field2;
    }

    /**
     * 
     * @param field2 
     */
    public void setField2(String field2) {
        this.field2 = field2 == null ? null : field2.trim();
    }

    /**
     * 
     * @return FIELD3 
     */
    public String getField3() {
        return field3;
    }

    /**
     * 
     * @param field3 
     */
    public void setField3(String field3) {
        this.field3 = field3 == null ? null : field3.trim();
    }

    /**
     * 
     * @return FIELD4 
     */
    public String getField4() {
        return field4;
    }

    /**
     * 
     * @param field4 
     */
    public void setField4(String field4) {
        this.field4 = field4 == null ? null : field4.trim();
    }

    /**
     * 
     * @return FIELD5 
     */
    public String getField5() {
        return field5;
    }

    /**
     * 
     * @param field5 
     */
    public void setField5(String field5) {
        this.field5 = field5 == null ? null : field5.trim();
    }
}