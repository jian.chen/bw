package com.bw.adv.module.coupon.model.exp;

public class CouponForUse {
	
	private String userName;
	
	private String memberName;
	
	private String couponName;
	
	private String mobile;
	
	private String couponInstanceCode;
	
	private String getTime;
	
	private String usedTime;
	
	private String expireDate;
	
	private String storeName;
	
	private Long statusId;
	
	private Long getChannelId;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCouponInstanceCode() {
		return couponInstanceCode;
	}

	public void setCouponInstanceCode(String couponInstanceCode) {
		this.couponInstanceCode = couponInstanceCode;
	}

	public String getGetTime() {
		return getTime;
	}

	public void setGetTime(String getTime) {
		this.getTime = getTime;
	}

	public String getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(String usedTime) {
		this.usedTime = usedTime;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getGetChannelId() {
		return getChannelId;
	}

	public void setGetChannelId(Long getChannelId) {
		this.getChannelId = getChannelId;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

}
