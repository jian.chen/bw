/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WinningItemExp.java
 * Package Name:com.sage.scrm.module.activity.model.exp
 * Date:2016年3月23日下午4:52:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.model.exp;

import com.bw.adv.module.activity.model.WinningItem;

/**
 * ClassName:WinningItemExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年3月23日 下午4:52:40 <br/>
 * scrmVersion 1.0
 * @author   sherry.wei
 * @version  jdk1.7
 * @see 	 
 */
public class WinningItemExp extends WinningItem {

	private String memberCode;	//会员编码
	private String memberName;	//会员姓名
	
	private String awardsName; //奖项名称
	
	
	public String getAwardsName() {
		return awardsName;
	}
	public void setAwardsName(String awardsName) {
		this.awardsName = awardsName;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	
	
}

