package com.bw.adv.module.base.model;

public class GeoTypeRel extends GeoTypeRelKey {
    private Long geoAssocTypeId;

    private Long seq;

    public Long getGeoAssocTypeId() {
        return geoAssocTypeId;
    }

    public void setGeoAssocTypeId(Long geoAssocTypeId) {
        this.geoAssocTypeId = geoAssocTypeId;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }
}