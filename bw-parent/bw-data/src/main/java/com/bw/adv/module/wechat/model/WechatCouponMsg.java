package com.bw.adv.module.wechat.model;

public class WechatCouponMsg {
	
    private String couponName;

    private String startDate;

    private String expireDate;

    private String comments;

    private String url;
    
    private String webchatRemark;

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getWebchatRemark() {
		return webchatRemark;
	}

	public void setWebchatRemark(String webchatRemark) {
		this.webchatRemark = webchatRemark;
	}
    
}