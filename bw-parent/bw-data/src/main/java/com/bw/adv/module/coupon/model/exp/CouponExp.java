package com.bw.adv.module.coupon.model.exp;

import com.bw.adv.module.coupon.model.Coupon;

/**
 * ClassName: 优惠券扩展类<br/>
 * date: 2015-8-11 上午11:09:53 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class CouponExp extends Coupon {
	//会员姓名
    private String memberName;
    //优惠券名称
    private String couponTypeName;
    //发放情况
    private String issueProportion;

	private String originName;
    
	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getCouponTypeName() {
		return couponTypeName;
	}

	public void setCouponTypeName(String couponTypeName) {
		this.couponTypeName = couponTypeName;
	}

	public String getIssueProportion() {
		return issueProportion;
	}

	public void setIssueProportion(String issueProportion) {
		this.issueProportion = issueProportion;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}
}