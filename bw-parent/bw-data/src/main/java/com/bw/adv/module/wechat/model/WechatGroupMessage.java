package com.bw.adv.module.wechat.model;

public class WechatGroupMessage {
	
	/**
	 * 消息ID
	 */
    private Long wechatGroupMessageId;
    
    /**
     * 消息模板ID
     */
    private Long wechatMessageTemplateId;
    
    /**
     * 微信粉丝组ID
     */
    private Long wechatFansGroupId;
    
    /**
     * 状态ID
     */
    private Long statusId;
    
    /**
     * 发送成功粉丝数
     */
    private Integer sentCount;
    
    /**
     * 发送失败的粉丝数
     */
    private Integer errorCount;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
    private String createTime;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateTime;
    
    /**
     * 删除标识
     */
    private String isDelete;
    
    /**
     * 错误码
     */
    private String errorCode;
    
    /**
     * 错误信息
     */
    private String errorMsg;
    
    /**
     * 返回结果
     */
    private String msgStatus;
    
    /**
     * 消息记录ID
     */
    private String messageId;

    public Long getWechatGroupMessageId() {
        return wechatGroupMessageId;
    }

    public void setWechatGroupMessageId(Long wechatGroupMessageId) {
        this.wechatGroupMessageId = wechatGroupMessageId;
    }

    public Long getWechatMessageTemplateId() {
        return wechatMessageTemplateId;
    }

    public void setWechatMessageTemplateId(Long wechatMessageTemplateId) {
        this.wechatMessageTemplateId = wechatMessageTemplateId;
    }

    public Long getWechatFansGroupId() {
        return wechatFansGroupId;
    }

    public void setWechatFansGroupId(Long wechatFansGroupId) {
        this.wechatFansGroupId = wechatFansGroupId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Integer getSentCount() {
        return sentCount;
    }

    public void setSentCount(Integer sentCount) {
        this.sentCount = sentCount;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(String msgStatus) {
        this.msgStatus = msgStatus;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}