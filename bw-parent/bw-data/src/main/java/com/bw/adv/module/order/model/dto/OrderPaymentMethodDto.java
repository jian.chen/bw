package com.bw.adv.module.order.model.dto;



import java.io.Serializable;

import com.bw.adv.module.order.model.OrderPaymentMethod;

/**
 * ClassName: OrderPaymentMethodDto <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-28 下午5:58:34 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class OrderPaymentMethodDto extends OrderPaymentMethod implements Serializable{
	private static final long serialVersionUID = -7960966692112899342L;
	
	private String externalId;
	private String payName;

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}
	
}

