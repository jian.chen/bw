package com.bw.adv.module.badge.model;

public class MemberBadgeItem {
    /**
     * 自定义增长
     */
    private Long memberBadageItemId;

    /**
     * 徽章主表主键
     */
    private Long memberBadageId;

    /**
     * 活动实例id
     */
    private Long sceneGameInstanceId;

    /**
     * 奖品id
     */
    private Long awardsId;

    /**
     * 徽章数量
     */
    private Integer itemBadgeNumber;

    /**
     * 可用数量
     */
    private Integer availableBadgeNumber;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 自定义增长
     * @return MEMBER_BADAGE_ITEM_ID 自定义增长
     */
    public Long getMemberBadageItemId() {
        return memberBadageItemId;
    }

    /**
     * 自定义增长
     * @param memberBadageItemId 自定义增长
     */
    public void setMemberBadageItemId(Long memberBadageItemId) {
        this.memberBadageItemId = memberBadageItemId;
    }

    /**
     * 徽章主表主键
     * @return MEMBER_BADAGE_ID 徽章主表主键
     */
    public Long getMemberBadageId() {
        return memberBadageId;
    }

    /**
     * 徽章主表主键
     * @param memberBadageId 徽章主表主键
     */
    public void setMemberBadageId(Long memberBadageId) {
        this.memberBadageId = memberBadageId;
    }

    /**
     * 活动实例id
     * @return SCENE_GAME_INSTANCE_ID 活动实例id
     */
    public Long getSceneGameInstanceId() {
        return sceneGameInstanceId;
    }

    /**
     * 活动实例id
     * @param sceneGameInstanceId 活动实例id
     */
    public void setSceneGameInstanceId(Long sceneGameInstanceId) {
        this.sceneGameInstanceId = sceneGameInstanceId;
    }

    /**
     * 奖品id
     * @return AWARDS_ID 奖品id
     */
    public Long getAwardsId() {
        return awardsId;
    }

    /**
     * 奖品id
     * @param awardsId 奖品id
     */
    public void setAwardsId(Long awardsId) {
        this.awardsId = awardsId;
    }

    /**
     * 徽章数量
     * @return ITEM_BADGE_NUMBER 徽章数量
     */
    public Integer getItemBadgeNumber() {
        return itemBadgeNumber;
    }

    /**
     * 徽章数量
     * @param itemBadgeNumber 徽章数量
     */
    public void setItemBadgeNumber(Integer itemBadgeNumber) {
        this.itemBadgeNumber = itemBadgeNumber;
    }

    /**
     * 可用数量
     * @return AVAILABLE_BADGE_NUMBER 可用数量
     */
    public Integer getAvailableBadgeNumber() {
        return availableBadgeNumber;
    }

    /**
     * 可用数量
     * @param availableBadgeNumber 可用数量
     */
    public void setAvailableBadgeNumber(Integer availableBadgeNumber) {
        this.availableBadgeNumber = availableBadgeNumber;
    }

    /**
     * 创建时间
     * @return CREATE_TIME 创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 更新时间
     * @return UPDATE_TIME 更新时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }
}