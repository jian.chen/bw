/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:SysUserExp.java
 * Package Name:com.sage.scrm.module.sys.model.exp
 * Date:2015年8月25日下午7:07:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.sys.model.exp;

import java.io.Serializable;
import java.util.List;

import com.bw.adv.module.sys.model.SysFunction;
import com.bw.adv.module.sys.model.SysFunctionOperation;

/**
 * ClassName:SysUserExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午7:07:06 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see
 */
public class SysFunctionExp extends SysFunction implements Serializable{

	private Long functionOperationId;

	private String operationName;

	private Long functionOperationType;

	private String isCheck;

	private List<SysFunctionOperation> buttonList;

	public List<SysFunctionOperation> getButtonList() {
		return buttonList;
	}

	public void setButtonList(List<SysFunctionOperation> buttonList) {
		this.buttonList = buttonList;
	}

	public String getIsCheck() {
		return isCheck;
	}

	public void setIsCheck(String isCheck) {
		this.isCheck = isCheck;
	}

	public Long getFunctionOperationId() {
		return functionOperationId;
	}

	public void setFunctionOperationId(Long functionOperationId) {
		this.functionOperationId = functionOperationId;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public Long getFunctionOperationType() {
		return functionOperationType;
	}

	public void setFunctionOperationType(Long functionOperationType) {
		this.functionOperationType = functionOperationType;
	}





}

