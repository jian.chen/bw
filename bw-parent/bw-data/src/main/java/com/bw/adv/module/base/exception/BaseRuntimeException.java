package com.bw.adv.module.base.exception;

import com.bw.adv.api.constant.ResultStatus;



/**
 * ClassName: BaseRuntimeException <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-13 下午5:43:40 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class BaseRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -7965384167623844879L;
	protected ErrorData errorData;	
	protected ResultStatus resultStatus;	
	
	/**
	 * 返回异常类自定义消息实现
	 * @param errorData
	 */
	public BaseRuntimeException(ErrorData errorData) {
		this.errorData = errorData;
	}
	
	public BaseRuntimeException(ResultStatus resultStatus) {
		this.resultStatus = resultStatus;
	}
	
	/**
	 * 返回异常类自定义消息实现
	 * @param errorMsg
	 * @param errorData
	 */
	public BaseRuntimeException(String errorMsg, ErrorData errorData) {
		super(errorMsg);
		this.errorData = errorData;
	}
	
	public BaseRuntimeException(String errorMsg, Throwable e) {
		super(errorMsg, e);
	}

	public BaseRuntimeException(String errorMsg) {
		super(errorMsg);
	}

	public BaseRuntimeException(Throwable e) {
		super(e);
	}
	
	
	public ErrorData getErrorData() {
		return errorData;
	}

	public void setErrorData(ErrorData errorData) {
		this.errorData = errorData;
	}

	public ResultStatus getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(ResultStatus resultStatus) {
		this.resultStatus = resultStatus;
	}

}

