package com.bw.adv.module.activity.model.exp;

import com.bw.adv.module.activity.model.ActivityInstanceRecord;

public class ActivityInstanceRecordExp extends ActivityInstanceRecord{
	
	private String memberName;
	private String activityInstanceName;
	private String mobile;
	private String userName;
	private String memberCode;
	
	
	public String getMemberName() {
		return memberName;
	}
	
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	public String getActivityInstanceName() {
		return activityInstanceName;
	}
	
	public void setActivityInstanceName(String activityInstanceName) {
		this.activityInstanceName = activityInstanceName;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	
	
	
}