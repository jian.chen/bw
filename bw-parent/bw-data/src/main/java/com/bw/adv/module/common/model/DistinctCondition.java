package com.bw.adv.module.common.model;

public class DistinctCondition {
    private Long distinctConditionId;

    private Long distinctTypeId;

    private String memberGroupName;

    private String remark;

    private String createTime;

    private String updateTime;

    private Long statusId;

    public Long getDistinctConditionId() {
        return distinctConditionId;
    }

    public void setDistinctConditionId(Long distinctConditionId) {
        this.distinctConditionId = distinctConditionId;
    }

    public Long getDistinctTypeId() {
        return distinctTypeId;
    }

    public void setDistinctTypeId(Long distinctTypeId) {
        this.distinctTypeId = distinctTypeId;
    }

    public String getMemberGroupName() {
        return memberGroupName;
    }

    public void setMemberGroupName(String memberGroupName) {
        this.memberGroupName = memberGroupName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}