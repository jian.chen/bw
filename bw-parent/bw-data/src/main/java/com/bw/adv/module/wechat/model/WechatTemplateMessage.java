package com.bw.adv.module.wechat.model;

public class WechatTemplateMessage {
    private Long templateMessId;

    private Long templateMessType;

    private String templateId;//模板消息Id

    private String templateMessCode;

    private Long firstClassId; //一级行业id

    private String firstClassName;

    private Long secondClassId;

    private String secondClsssName;

    private String templateTitle;

    private Long statusId;

    private Long createId;

    private String createTime;

    private Long updateBy;

    private String updateTime;

    public Long getTemplateMessId() {
        return templateMessId;
    }

    public void setTemplateMessId(Long templateMessId) {
        this.templateMessId = templateMessId;
    }

    public Long getTemplateMessType() {
        return templateMessType;
    }

    public void setTemplateMessType(Long templateMessType) {
        this.templateMessType = templateMessType;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateMessCode() {
        return templateMessCode;
    }

    public void setTemplateMessCode(String templateMessCode) {
        this.templateMessCode = templateMessCode;
    }

    public Long getFirstClassId() {
        return firstClassId;
    }

    public void setFirstClassId(Long firstClassId) {
        this.firstClassId = firstClassId;
    }

    public String getFirstClassName() {
        return firstClassName;
    }

    public void setFirstClassName(String firstClassName) {
        this.firstClassName = firstClassName;
    }

    public Long getSecondClassId() {
        return secondClassId;
    }

    public void setSecondClassId(Long secondClassId) {
        this.secondClassId = secondClassId;
    }

    public String getSecondClsssName() {
        return secondClsssName;
    }

    public void setSecondClsssName(String secondClsssName) {
        this.secondClsssName = secondClsssName;
    }

    public String getTemplateTitle() {
        return templateTitle;
    }

    public void setTemplateTitle(String templateTitle) {
        this.templateTitle = templateTitle;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getCreateId() {
        return createId;
    }

    public void setCreateId(Long createId) {
        this.createId = createId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}