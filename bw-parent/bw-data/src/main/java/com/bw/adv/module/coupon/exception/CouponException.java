package com.bw.adv.module.coupon.exception;


import org.apache.commons.lang.StringUtils;

import com.bw.adv.api.constant.ResultStatus;

public class CouponException extends RuntimeException {
	
	private ResultStatus resultStatus;

	private static final long serialVersionUID = 4203482434232111182L;

	public CouponException() {
		super();
	}

	public CouponException(String errorMsg, Throwable e) {
		super(errorMsg, e);
	}

	public CouponException(String errorMsg) {
		super(errorMsg);
	}
	
	public CouponException(ResultStatus resultStatus) {
		super();
		this.resultStatus = resultStatus;
	}
	
	public CouponException(String errorMsg, ResultStatus resultStatus) {
		super(errorMsg);
		this.resultStatus = resultStatus;
	}

	public ResultStatus getResultStatus() {
		return resultStatus;
	}

	@Override
	public String getMessage() {
		if(StringUtils.isNotBlank(super.getMessage())){
			return super.getMessage();
		}else if(resultStatus != null){
			return resultStatus.getMsg();
		}else{
			return "coupon error";
		}
	}

	

}
