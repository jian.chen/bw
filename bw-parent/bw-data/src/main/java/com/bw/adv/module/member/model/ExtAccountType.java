package com.bw.adv.module.member.model;

public class ExtAccountType {
	
    private Long extAccountTypeId;

    private String extAccountTypeCode;

    private String extAccountTypeName;

    public Long getExtAccountTypeId() {
        return extAccountTypeId;
    }

    public void setExtAccountTypeId(Long extAccountTypeId) {
        this.extAccountTypeId = extAccountTypeId;
    }

    public String getExtAccountTypeCode() {
        return extAccountTypeCode;
    }

    public void setExtAccountTypeCode(String extAccountTypeCode) {
        this.extAccountTypeCode = extAccountTypeCode;
    }

    public String getExtAccountTypeName() {
        return extAccountTypeName;
    }

    public void setExtAccountTypeName(String extAccountTypeName) {
        this.extAccountTypeName = extAccountTypeName;
    }
}