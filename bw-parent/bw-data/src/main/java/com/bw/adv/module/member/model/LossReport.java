package com.bw.adv.module.member.model;

public class LossReport {
    private Long lossReportId;

    private Long memberId;

    private Long lostCardId;

    private Long newCardId;

    private String lossDate;

    private Long lossReportReasonId;

    private String lossReportReason;

    private String lossReportDate;

    private String lossReportTime;

    private String processResult;

    private Long processBy;

    private String processDate;

    public Long getLossReportId() {
        return lossReportId;
    }

    public void setLossReportId(Long lossReportId) {
        this.lossReportId = lossReportId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getLostCardId() {
        return lostCardId;
    }

    public void setLostCardId(Long lostCardId) {
        this.lostCardId = lostCardId;
    }

    public Long getNewCardId() {
        return newCardId;
    }

    public void setNewCardId(Long newCardId) {
        this.newCardId = newCardId;
    }

    public String getLossDate() {
        return lossDate;
    }

    public void setLossDate(String lossDate) {
        this.lossDate = lossDate;
    }

    public Long getLossReportReasonId() {
        return lossReportReasonId;
    }

    public void setLossReportReasonId(Long lossReportReasonId) {
        this.lossReportReasonId = lossReportReasonId;
    }

    public String getLossReportReason() {
        return lossReportReason;
    }

    public void setLossReportReason(String lossReportReason) {
        this.lossReportReason = lossReportReason;
    }

    public String getLossReportDate() {
        return lossReportDate;
    }

    public void setLossReportDate(String lossReportDate) {
        this.lossReportDate = lossReportDate;
    }

    public String getLossReportTime() {
        return lossReportTime;
    }

    public void setLossReportTime(String lossReportTime) {
        this.lossReportTime = lossReportTime;
    }

    public String getProcessResult() {
        return processResult;
    }

    public void setProcessResult(String processResult) {
        this.processResult = processResult;
    }

    public Long getProcessBy() {
        return processBy;
    }

    public void setProcessBy(Long processBy) {
        this.processBy = processBy;
    }

    public String getProcessDate() {
        return processDate;
    }

    public void setProcessDate(String processDate) {
        this.processDate = processDate;
    }
}