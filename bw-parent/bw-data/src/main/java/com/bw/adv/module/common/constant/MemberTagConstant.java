/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:MemberChannelConstant.java
 * Package Name:com.sage.scrm.module.common.constant
 * Date:2016年2月22日下午2:10:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.common.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.member.tag.model.MemberTag;

/**
 * ClassName:MemberChannelConstant <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年2月22日 下午2:10:06 <br/>
 * scrmVersion 1.0
 * @author   sherry.wei
 * @version  jdk1.7
 * @see 	 
 */
public class MemberTagConstant extends AbstractConstant<MemberTag> {
	
	//员工标签
	public static final MemberTagConstant EMPLOYEES = new MemberTagConstant(1L,"MT0000000001","员工");

	public MemberTagConstant(Long id, String code, String name) {
		super(id, code, name);
	}
	
	@Override
	public MemberTag getInstance() {
		
		MemberTag memberTag = new MemberTag();
		memberTag.setMemberTagId(id);
		memberTag.setMemberTagCode(code);
		memberTag.setMemberTagName(name);
		return memberTag;
	}

}

