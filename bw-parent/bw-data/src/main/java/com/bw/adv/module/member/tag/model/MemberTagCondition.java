package com.bw.adv.module.member.tag.model;

public class MemberTagCondition {
    private Long memberTagConditionId;

    private Long memberTagId;

    private Long distinctConditionId;

    private String distinctConditionRule;

    private String distinctConditionValue;

    public Long getMemberTagConditionId() {
        return memberTagConditionId;
    }

    public void setMemberTagConditionId(Long memberTagConditionId) {
        this.memberTagConditionId = memberTagConditionId;
    }

    public Long getMemberTagId() {
        return memberTagId;
    }

    public void setMemberTagId(Long memberTagId) {
        this.memberTagId = memberTagId;
    }

    public Long getDistinctConditionId() {
        return distinctConditionId;
    }

    public void setDistinctConditionId(Long distinctConditionId) {
        this.distinctConditionId = distinctConditionId;
    }

    public String getDistinctConditionRule() {
        return distinctConditionRule;
    }

    public void setDistinctConditionRule(String distinctConditionRule) {
        this.distinctConditionRule = distinctConditionRule;
    }

    public String getDistinctConditionValue() {
        return distinctConditionValue;
    }

    public void setDistinctConditionValue(String distinctConditionValue) {
        this.distinctConditionValue = distinctConditionValue;
    }
}