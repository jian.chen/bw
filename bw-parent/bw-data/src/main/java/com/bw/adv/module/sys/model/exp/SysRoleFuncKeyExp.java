package com.bw.adv.module.sys.model.exp;

import java.util.List;

import com.bw.adv.module.sys.model.SysRoleFuncKey;

public class SysRoleFuncKeyExp extends  SysRoleFuncKey{
	
	private List<SysFunctionOperationExp> sysFunctionOperationExpList;

	public List<SysFunctionOperationExp> getSysFunctionOperationExpList() {
		return sysFunctionOperationExpList;
	}

	public void setSysFunctionOperationExpList(
			List<SysFunctionOperationExp> sysFunctionOperationExpList) {
		this.sysFunctionOperationExpList = sysFunctionOperationExpList;
	}

 
}