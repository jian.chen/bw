package com.bw.adv.module.member.model;

import java.math.BigDecimal;

public class GradeConvertRecord {
    private Long gcRecordId;

    private Long memberId;

    private Long srcGradeId;

    private Long targetGradeId;

    private Long gradeFactorsId;

    private String upDownFlag;

    private BigDecimal gradeFactorsValue;
    
    private String gradeFactorsDesc;
    
    private String createTime;
    
    private String gcDate;

    public Long getGcRecordId() {
        return gcRecordId;
    }

    public void setGcRecordId(Long gcRecordId) {
        this.gcRecordId = gcRecordId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getSrcGradeId() {
        return srcGradeId;
    }

    public void setSrcGradeId(Long srcGradeId) {
        this.srcGradeId = srcGradeId;
    }

    public Long getTargetGradeId() {
        return targetGradeId;
    }

    public void setTargetGradeId(Long targetGradeId) {
        this.targetGradeId = targetGradeId;
    }

    public Long getGradeFactorsId() {
        return gradeFactorsId;
    }

    public void setGradeFactorsId(Long gradeFactorsId) {
        this.gradeFactorsId = gradeFactorsId;
    }

    public String getUpDownFlag() {
        return upDownFlag;
    }

    public void setUpDownFlag(String upDownFlag) {
        this.upDownFlag = upDownFlag;
    }

    public BigDecimal getGradeFactorsValue() {
        return gradeFactorsValue;
    }

    public void setGradeFactorsValue(BigDecimal gradeFactorsValue) {
        this.gradeFactorsValue = gradeFactorsValue;
    }
    
    public String getGradeFactorsDesc() {
		return gradeFactorsDesc;
	}

	public void setGradeFactorsDesc(String gradeFactorsDesc) {
		this.gradeFactorsDesc = gradeFactorsDesc;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getGcDate() {
        return gcDate;
    }

    public void setGcDate(String gcDate) {
        this.gcDate = gcDate;
    }
}