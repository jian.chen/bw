package com.bw.adv.module.member.tag.model;

public class MemberTagItem {
    private Long memberTagItemId;

    private Long memberTagId;

    private Long memberId;
    
    private String createTime;

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((memberId == null) ? 0 : memberId.hashCode());
		result = prime * result
				+ ((memberTagId == null) ? 0 : memberTagId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberTagItem other = (MemberTagItem) obj;
		if (memberId == null) {
			if (other.memberId != null)
				return false;
		} else if (!memberId.equals(other.memberId))
			return false;
		if (memberTagId == null) {
			if (other.memberTagId != null)
				return false;
		} else if (!memberTagId.equals(other.memberTagId))
			return false;
		return true;
	}

	public Long getMemberTagItemId() {
        return memberTagItemId;
    }

    public void setMemberTagItemId(Long memberTagItemId) {
        this.memberTagItemId = memberTagItemId;
    }

    public Long getMemberTagId() {
        return memberTagId;
    }

    public void setMemberTagId(Long memberTagId) {
        this.memberTagId = memberTagId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}