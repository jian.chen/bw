package com.bw.adv.module.cfg.model;
/**
 * ClassName: ChannelCfgValue <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月14日 下午5:52:05 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
public class ChannelCfgValue {
    private Long cfgValueId;

    private Long channelCfgId;

    private String cfgValue;
    
    private String cfgCode;

    private String comments;

    public Long getCfgValueId() {
        return cfgValueId;
    }

    public void setCfgValueId(Long cfgValueId) {
        this.cfgValueId = cfgValueId;
    }

    public Long getChannelCfgId() {
        return channelCfgId;
    }

    public void setChannelCfgId(Long channelCfgId) {
        this.channelCfgId = channelCfgId;
    }

    public String getCfgValue() {
        return cfgValue;
    }

    public void setCfgValue(String cfgValue) {
        this.cfgValue = cfgValue;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

	public String getCfgCode() {
		return cfgCode;
	}

	public void setCfgCode(String cfgCode) {
		this.cfgCode = cfgCode;
	}
}