/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:MemberEquityExp.java
 * Package Name:com.sage.scrm.module.member.model.exp
 * Date:2016年1月15日下午1:54:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.model.exp;

import com.bw.adv.module.member.model.MemberEquity;

/**
 * ClassName:MemberEquityExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月15日 下午1:54:15 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
public class MemberEquityExp extends MemberEquity{
      
	 private String gradeName;

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	 
}

