package com.bw.adv.module.sys.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.sys.model.LogType;

public class LogEventConstant extends AbstractConstant<LogType>{

    public static final LogEventConstant LOG_EVENT_LOGIN = new LogEventConstant(1L, "登录");
    public static final LogEventConstant LOG_EVENT_LOGOUT = new LogEventConstant(2L, "注销");
    public static final LogEventConstant LOG_EVENT_INSERT = new LogEventConstant(3L, "新增");
    public static final LogEventConstant LOG_EVENT_UPDATE = new LogEventConstant(4L, "修改");
    public static final LogEventConstant LOG_EVENT_DELETE = new LogEventConstant(5L, "删除");
    public static final LogEventConstant LOG_EVENT_ENABLED = new LogEventConstant(6L, "启用");
    public static final LogEventConstant LOG_EVENT_DISABLED = new LogEventConstant(7L, "停用");
    public static final LogEventConstant LOG_EVENT_ISSUED = new LogEventConstant(8L, "下发");
    public static final LogEventConstant LOG_EVENT_EXPORT = new LogEventConstant(9L, "导入");
    
    
	public LogEventConstant(Long id, String name) {
		super(id, name);
	}
	
	@Override
	public LogType getInstance() {
		LogType logType = new LogType();
		logType.setLogTypeId(id);
		logType.setLogTypeName(name);
		return logType;
	}
	
}