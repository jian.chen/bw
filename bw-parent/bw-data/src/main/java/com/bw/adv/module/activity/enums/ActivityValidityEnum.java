package com.bw.adv.module.activity.enums;

import org.apache.commons.lang.StringUtils;



public enum ActivityValidityEnum {
	
	FIXED("1", "固定日期"),
	PERIOD_DATE("2", "固定时间段，活动开始后XX天"),
	FOREVER("3", "无截止日期");
	
	private String id;
	private String desc;
	
	private ActivityValidityEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static ActivityValidityEnum[] getArray() {
		return ActivityValidityEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			ActivityValidityEnum[] values = ActivityValidityEnum.values();
			for(ActivityValidityEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}
