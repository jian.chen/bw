package com.bw.adv.module.sys.model;

import java.io.Serializable;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public class SysRoleData implements Serializable{

    private Long roleId;
    private Integer dataTypeId;
    private Integer filterTypeId;
    private String operationParams;
    private Integer status;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFilterTypeId() {
        return filterTypeId;
    }

    public void setFilterTypeId(Integer filterTypeId) {
        this.filterTypeId = filterTypeId;
    }

    public String getOperationParams() {
        return operationParams;
    }

    public void setOperationParams(String operationParams) {
        this.operationParams = operationParams;
    }

    public Integer getDataTypeId() {
        return dataTypeId;
    }

    public void setDataTypeId(Integer dataTypeId) {
        this.dataTypeId = dataTypeId;
    }
}
