package com.bw.adv.module.order.model.dto;


import java.io.Serializable;

import com.bw.adv.module.order.model.OrderItem;

/**
 * ClassName: OrderItemDto <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-28 下午5:58:29 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class OrderItemDto extends OrderItem implements Serializable {
	
	private static final long serialVersionUID = 7718293147940929066L;
	
	private String externalItemId;

	public String getExternalItemId() {
		return externalItemId;
	}

	public void setExternalItemId(String externalItemId) {
		this.externalItemId = externalItemId;
	}
	
	
}