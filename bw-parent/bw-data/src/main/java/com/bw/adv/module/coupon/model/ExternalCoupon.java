package com.bw.adv.module.coupon.model;

import com.bw.adv.module.tools.Excel;

import java.util.Date;

public class ExternalCoupon {

    private Long couponId = 0L;


    @Excel(exportName = "礼品券码", exportFieldWidth = 30, exportConvertSign = 0, importConvertSign = 0)
    private String couponInstanceCode;


    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }


    public String getCouponInstanceCode() {
		return couponInstanceCode;
	}

	public void setCouponInstanceCode(String couponInstanceCode) {
		this.couponInstanceCode = couponInstanceCode;
	}

}