package com.bw.adv.module.cfg.model;

public class ChannelInstanceRecordDto extends ChannelInstanceRecord {
	private String memberCode;
	
	private String memberName;
	
	private String mobile;

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
}