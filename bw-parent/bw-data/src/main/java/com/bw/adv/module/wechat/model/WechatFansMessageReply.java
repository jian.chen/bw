package com.bw.adv.module.wechat.model;

public class WechatFansMessageReply {
    private Long wechatFansMessageReplyId;

    private Long wechatFansMessageId;

    private String replyContent;

    private Long createBy;

    private String createTime;

    private String isSysUserReply;

    private Long updateBy;

    private String updateTime;

    public Long getWechatFansMessageReplyId() {
        return wechatFansMessageReplyId;
    }

    public void setWechatFansMessageReplyId(Long wechatFansMessageReplyId) {
        this.wechatFansMessageReplyId = wechatFansMessageReplyId;
    }

    public Long getWechatFansMessageId() {
        return wechatFansMessageId;
    }

    public void setWechatFansMessageId(Long wechatFansMessageId) {
        this.wechatFansMessageId = wechatFansMessageId;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getIsSysUserReply() {
        return isSysUserReply;
    }

    public void setIsSysUserReply(String isSysUserReply) {
        this.isSysUserReply = isSysUserReply;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}