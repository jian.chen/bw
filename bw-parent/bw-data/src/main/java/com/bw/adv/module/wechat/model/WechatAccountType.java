package com.bw.adv.module.wechat.model;

public class WechatAccountType {
    private Long wechatAccountTypeId;

    private String wechatAccountTypeName;

    private String wechatAccountTypeCode;

    private String remark;

    public Long getWechatAccountTypeId() {
        return wechatAccountTypeId;
    }

    public void setWechatAccountTypeId(Long wechatAccountTypeId) {
        this.wechatAccountTypeId = wechatAccountTypeId;
    }

    public String getWechatAccountTypeName() {
        return wechatAccountTypeName;
    }

    public void setWechatAccountTypeName(String wechatAccountTypeName) {
        this.wechatAccountTypeName = wechatAccountTypeName;
    }

    public String getWechatAccountTypeCode() {
        return wechatAccountTypeCode;
    }

    public void setWechatAccountTypeCode(String wechatAccountTypeCode) {
        this.wechatAccountTypeCode = wechatAccountTypeCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}