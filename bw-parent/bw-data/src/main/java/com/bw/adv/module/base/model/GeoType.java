package com.bw.adv.module.base.model;

public class GeoType {
    private Long geoTypeId;

    private Long parentTypeId;

    private String geoTypeCode;

    private String geoTypeName;

    private String comments;

    private String treeCode;

    public Long getGeoTypeId() {
        return geoTypeId;
    }

    public void setGeoTypeId(Long geoTypeId) {
        this.geoTypeId = geoTypeId;
    }

    public Long getParentTypeId() {
        return parentTypeId;
    }

    public void setParentTypeId(Long parentTypeId) {
        this.parentTypeId = parentTypeId;
    }

    public String getGeoTypeCode() {
        return geoTypeCode;
    }

    public void setGeoTypeCode(String geoTypeCode) {
        this.geoTypeCode = geoTypeCode;
    }

    public String getGeoTypeName() {
        return geoTypeName;
    }

    public void setGeoTypeName(String geoTypeName) {
        this.geoTypeName = geoTypeName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTreeCode() {
        return treeCode;
    }

    public void setTreeCode(String treeCode) {
        this.treeCode = treeCode;
    }
}