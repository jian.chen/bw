package com.bw.adv.module.activity.model;

public class BusinessActivity {
    private Long businessActivityId;

    private Long businessId;

    private Long activityId;

    public Long getBusinessActivityId() {
        return businessActivityId;
    }

    public void setBusinessActivityId(Long businessActivityId) {
        this.businessActivityId = businessActivityId;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }
}