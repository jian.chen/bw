package com.bw.adv.module.sys.model;

public class SysRoleDimLevelKey {
    private Long roleId;

    private String dimCode;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getDimCode() {
        return dimCode;
    }

    public void setDimCode(String dimCode) {
        this.dimCode = dimCode;
    }
}