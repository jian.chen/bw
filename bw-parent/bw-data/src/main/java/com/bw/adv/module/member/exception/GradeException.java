package com.bw.adv.module.member.exception;


public class GradeException extends RuntimeException {

	private static final long serialVersionUID = 4203482434232111182L;

	public GradeException() {
		super();
	}

	public GradeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public GradeException(String arg0) {
		super(arg0);
	}
}
