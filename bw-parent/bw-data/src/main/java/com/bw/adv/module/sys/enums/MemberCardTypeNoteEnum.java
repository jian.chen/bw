package com.bw.adv.module.sys.enums;

import org.apache.commons.lang.StringUtils;



public enum MemberCardTypeNoteEnum {
	
	ORDINARY("1", "实体卡"),
	SENIOR("2", "电子卡");
	
	private String id;
	private String desc;
	
	private MemberCardTypeNoteEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static MemberCardTypeNoteEnum[] getArray() {
		return MemberCardTypeNoteEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			MemberCardTypeNoteEnum[] values = MemberCardTypeNoteEnum.values();
			for(MemberCardTypeNoteEnum memberCardTypeNoteEnum : values){
				if(memberCardTypeNoteEnum.getId().equals(id)){
					return memberCardTypeNoteEnum.getDesc();
				}
			}
		}
		return "";
	}
}
