package com.bw.adv.module.common.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.base.model.Channel;

public class UnitConstant extends AbstractConstant<Channel> {
	//内部系统
	public static final UnitConstant ZHANG = new UnitConstant(1L,"ZHANG","张");
	public static final UnitConstant BU = new UnitConstant(2L,"BU","部");
	
	
	
	
	public UnitConstant(Long id, String code, String name) {
		super(id, code, name);
	}

	@Override
	public Channel getInstance() {
		Channel channel = new Channel();
		channel.setChannelId(id);
		channel.setChannelCode(code);
		channel.setChannelName(name);
		return channel;
	}

}
