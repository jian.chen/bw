package com.bw.adv.module.report.points.model.exp;

import java.math.BigDecimal;

import com.bw.adv.module.report.points.model.PointsSummaryDay;
import com.bw.adv.module.tools.Excel;

public class PointsSummaryDayExp extends PointsSummaryDay {
	
	private String pointsRuleName;

	private String pointsTypeName;

	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "积分名称", importConvertSign = 0)
	private String activityInstanceName;

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "发放数量", importConvertSign = 0)
	private BigDecimal withPointsInNumber;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "占比", importConvertSign = 0)
	private String rate;

	public String getPointsRuleName() {
		return pointsRuleName;
	}

	public void setPointsRuleName(String pointsRuleName) {
		this.pointsRuleName = pointsRuleName;
	}

	public String getPointsTypeName() {
		return pointsTypeName;
	}

	public void setPointsTypeName(String pointsTypeName) {
		this.pointsTypeName = pointsTypeName;
	}

	public BigDecimal getWithPointsInNumber() {
		return withPointsInNumber;
	}

	public void setWithPointsInNumber(BigDecimal withPointsInNumber) {
		this.withPointsInNumber = withPointsInNumber;
	}

	public String getActivityInstanceName() {
		return activityInstanceName;
	}

	public void setActivityInstanceName(String activityInstanceName) {
		this.activityInstanceName = activityInstanceName;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

}