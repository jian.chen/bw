/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WeixinMedia.java
 * Package Name:com.sage.scrm.module.wechat.model
 * Date:2015年8月17日下午3:10:53
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.wechat.model;

/**
 * 多媒体类型
 * ClassName:WeixinMedia <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月17日 下午3:10:53 <br/>
 * scrmVersion 1.0
 * @author june
 * @version jdk1.7
 * @see
 */
public class WeixinMedia {
	
	/**
	 * 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图）
	 */
	private String type;
	
	/**
	 * 媒体文件上传后，获取时的唯一标识
	 */
	private String mediaId;
	
	/**
	 * 媒体文件上传时间戳
	 */
	private int createdAt;

	public int getCreatedAt() {
		return createdAt;
	}

	public String getMediaId() {
		return mediaId;
	}

	public String getType() {
		return type;
	}

	public void setCreatedAt(int createdAt) {
		this.createdAt = createdAt;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public void setType(String type) {
		this.type = type;
	}
}
