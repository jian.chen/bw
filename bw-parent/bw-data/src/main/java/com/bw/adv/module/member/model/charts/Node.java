package com.bw.adv.module.member.model.charts;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by jeoy.zhou on 2/29/16.
 */
@XmlRootElement(name = "node")
@XmlAccessorOrder()
public class Node {

    private Integer id;
    private String label;
    private List<Attvalue> attvalues;
    private Size size;
    private Color color;
    private Position position;

    @XmlAttribute(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlAttribute(name = "label")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @XmlElementWrapper(name = "attvalues")
    @XmlElement(name = "attvalue")
    public List<Attvalue> getAttvalues() {
        return attvalues;
    }

    public void setAttvalues(List<Attvalue> attvalues) {
        this.attvalues = attvalues;
    }

    @XmlElement(name = "viz:size")
    public Size getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = new Size(size);
    }

    @XmlElement(name = "viz:color")
    public Color getColor() {
        return color;
    }

    public void setColor(String r, String g, String b) {
        this.color = new Color(r, g, b);
    }

    @XmlElement(name = "viz:position")
    public Position getPosition() {
        return position;
    }

    public void setPosition(String x, String y, String z) {
        this.position = new Position(x, y ,z);
    }

    @XmlRootElement(name = "viz:size")
    public static class Size {
        private String value;

        public Size() {}

        public Size(String size) {
            this.value = size;
        }

        @XmlAttribute(name = "value")
        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    @XmlRootElement(name = "viz:color")
    public static class Color {
        private String r;
        private String g;
        private String b;


        public Color() {}

        public Color(String r, String g, String b) {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        @XmlAttribute(name = "r")
        public String getR() {
            return r;
        }

        public void setR(String r) {
            this.r = r;
        }

        @XmlAttribute(name = "g")
        public String getG() {
            return g;
        }

        public void setG(String g) {
            this.g = g;
        }

        @XmlAttribute(name = "b")
        public String getB() {
            return b;
        }

        public void setB(String b) {
            this.b = b;
        }
    }

    @XmlRootElement(name = "viz:position")
    public static class Position {
        private String x;
        private String y;
        private String z;


        public Position() {}

        public Position(String x, String y, String z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        @XmlAttribute(name = "x")
        public String getX() {
            return x;
        }

        public void setX(String x) {
            this.x = x;
        }

        @XmlAttribute(name = "y")
        public String getY() {
            return y;
        }

        public void setY(String y) {
            this.y = y;
        }

        @XmlAttribute(name = "y")
        public String getZ() {
            return z;
        }

        public void setZ(String z) {
            this.z = z;
        }
    }
}


