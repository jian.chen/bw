package com.bw.adv.module.activity.model;

public class SceneGame {
    private Long sceneGameId;

    private Long statusId;

    private Long sceneId;

    private String sceneGameCode;

    private String sceneGameName;

    private String sceneGameDesc;

    private String sceneGameContent;

    private String createTime;

    private String imgUrl;

    private String remark;

    public Long getSceneGameId() {
        return sceneGameId;
    }

    public void setSceneGameId(Long sceneGameId) {
        this.sceneGameId = sceneGameId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getSceneId() {
        return sceneId;
    }

    public void setSceneId(Long sceneId) {
        this.sceneId = sceneId;
    }

    public String getSceneGameCode() {
        return sceneGameCode;
    }

    public void setSceneGameCode(String sceneGameCode) {
        this.sceneGameCode = sceneGameCode;
    }

    public String getSceneGameName() {
        return sceneGameName;
    }

    public void setSceneGameName(String sceneGameName) {
        this.sceneGameName = sceneGameName;
    }

    public String getSceneGameDesc() {
        return sceneGameDesc;
    }

    public void setSceneGameDesc(String sceneGameDesc) {
        this.sceneGameDesc = sceneGameDesc;
    }

    public String getSceneGameContent() {
        return sceneGameContent;
    }

    public void setSceneGameContent(String sceneGameContent) {
        this.sceneGameContent = sceneGameContent;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}