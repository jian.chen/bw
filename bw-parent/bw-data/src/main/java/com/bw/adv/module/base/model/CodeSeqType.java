package com.bw.adv.module.base.model;


public class CodeSeqType {
    private Long codeSeqTypeId;

    private String codeSeqTypeCode;

    private String codeSeqTypeName;

    private String codeSeqTypePrefix;

    private String isActive;
    
    private int codeLength;

    public int getCodeLength() {
		return codeLength;
	}

	public void setCodeLength(int codeLength) {
		this.codeLength = codeLength;
	}

	public Long getCodeSeqTypeId() {
        return codeSeqTypeId;
    }

    public void setCodeSeqTypeId(Long codeSeqTypeId) {
        this.codeSeqTypeId = codeSeqTypeId;
    }

    public String getCodeSeqTypeCode() {
        return codeSeqTypeCode;
    }

    public void setCodeSeqTypeCode(String codeSeqTypeCode) {
        this.codeSeqTypeCode = codeSeqTypeCode;
    }

    public String getCodeSeqTypeName() {
        return codeSeqTypeName;
    }

    public void setCodeSeqTypeName(String codeSeqTypeName) {
        this.codeSeqTypeName = codeSeqTypeName;
    }

    public String getCodeSeqTypePrefix() {
        return codeSeqTypePrefix;
    }

    public void setCodeSeqTypePrefix(String codeSeqTypePrefix) {
        this.codeSeqTypePrefix = codeSeqTypePrefix;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}