/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberExp.java
 * Package Name:com.sage.scrm.module.member.model.exp
 * Date:2015年8月11日下午3:44:15
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.member.model.exp;

import com.bw.adv.module.member.model.Member;

import java.math.BigDecimal;

/**
 * ClassName:MemberExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午3:44:15 <br/>
 * scrmVersion 1.0
 * 
 * @author simon
 * @version jdk1.7
 * @see
 */
public class MemberExp extends Member {
	// 会员卡号
	private String cardNo;

	private String cardName;

	// 账户余额
	private BigDecimal accountBalance;
	// 积分余额
	private BigDecimal pointsBalance;
	private BigDecimal totalPoints;
	//总消费额
	private BigDecimal totalConsumption;
	//总消费次数
	private Integer totalConsumptionCount;
	
	
	
	private BigDecimal pointsPeriod;//积分时间周期的累计量
	private BigDecimal orderAmountPeriod;//订单时间周期的累计量

	private Long gradeId;

	// 新浪微博
	private String sina;
	// 微信号
	private String wechat;
	// QQ
	private String qq;

	private String msn;

	private String online;

	private String cc;
	
	private String cityName;
	
	private String[] memberTag;
	
	private String cardImg;
	
	private String comvCode; // 验证码
	
	private String originalMobile;
	
	private BigDecimal nextPoints; 
	
	private Long couponAmount;
	
	private String gradeNameEn;

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getSina() {
		return sina;
	}

	public void setSina(String sina) {
		this.sina = sina;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public BigDecimal getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public BigDecimal getPointsBalance() {
		return pointsBalance;
	}

	public void setPointsBalance(BigDecimal pointsBalance) {
		this.pointsBalance = pointsBalance;
	}

	public BigDecimal getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(BigDecimal totalPoints) {
		this.totalPoints = totalPoints;
	}

	public BigDecimal getTotalConsumption() {
		return totalConsumption;
	}

	public void setTotalConsumption(BigDecimal totalConsumption) {
		this.totalConsumption = totalConsumption;
	}

	public Integer getTotalConsumptionCount() {
		return totalConsumptionCount;
	}

	public void setTotalConsumptionCount(Integer totalConsumptionCount) {
		this.totalConsumptionCount = totalConsumptionCount;
	}

	public BigDecimal getPointsPeriod() {
		return pointsPeriod;
	}

	public void setPointsPeriod(BigDecimal pointsPeriod) {
		this.pointsPeriod = pointsPeriod;
	}

	public BigDecimal getOrderAmountPeriod() {
		return orderAmountPeriod;
	}

	public void setOrderAmountPeriod(BigDecimal orderAmountPeriod) {
		this.orderAmountPeriod = orderAmountPeriod;
	}

	public Long getGradeId() {
		return gradeId;
	}

	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String[] getMemberTag() {
		return memberTag;
	}

	public void setMemberTag(String[] memberTag) {
		this.memberTag = memberTag;
	}

	public String getCardImg() {
		return cardImg;
	}

	public void setCardImg(String cardImg) {
		this.cardImg = cardImg;
	}

	public String getComvCode() {
		return comvCode;
	}

	public void setComvCode(String comvCode) {
		this.comvCode = comvCode;
	}

	public String getOriginalMobile() {
		return originalMobile;
	}

	public void setOriginalMobile(String originalMobile) {
		this.originalMobile = originalMobile;
	}

	public BigDecimal getNextPoints() {
		return nextPoints;
	}

	public void setNextPoints(BigDecimal nextPoints) {
		this.nextPoints = nextPoints;
	}

	public Long getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(Long couponAmount) {
		this.couponAmount = couponAmount;
	}

	public String getGradeNameEn() {
		return gradeNameEn;
	}

	public void setGradeNameEn(String gradeNameEn) {
		this.gradeNameEn = gradeNameEn;
	}
	
}
