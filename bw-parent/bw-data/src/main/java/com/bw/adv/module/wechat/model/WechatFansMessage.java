package com.bw.adv.module.wechat.model;

public class WechatFansMessage {
	
	/**
	 * 粉丝互动消息ID
	 */
    private Long wechatFansMessageId;
    
    /**
     * 微信账号ID
     */
    private Long wechatAccountId;
    
    /**
     * 消息内容
     */
    private String content;
    
    /**
     * 粉丝编号
     */
    private Long fssFansid;
    
    /**
     * OPENID
     */
    private String fssOpenid;
    
    /**
     * 消息类型
     */
    private String msgType;
    
    /**
     * 媒体ID
     */
    private String mediaId;
    
    /**
     * 图片URL
     */
    private String pictureUrl;
    
    /**
     * 语音格式
     */
    private String voiceType;
    
    /**
     * 视频消息缩略图ID
     */
    private String thumbmediaId;
    
    /**
     * 地理位置维度
     */
    private String latitude;
    
    /**
     * 地理位置经度
     */
    private String longitude;
    
    /**
     * 地图缩放大小
     */
    private String scale;
    
    /**
     * 是否回复
     */
    private String isReply;
    
    /**
     * 是否关键字消息
     */
    private String isKeyword;
    
    /**
     * 创建时间
     */
    private String fssCreateddate;
    
    /**
     * 微信公众号
     */
    private String fssPublicaccount;
    
    /**
     * 消息链接
     */
    private String messageUrl;
    
    /**
     * 消息描述
     */
    private String description;
    
    /**
     * 消息标题
     */
    private String title;
    
    /**
     * 地理位置信息
     */
    private String fssLabel;
    
    /**
     * 是否已读
     */
    private String isRead;

    public Long getWechatFansMessageId() {
        return wechatFansMessageId;
    }

    public void setWechatFansMessageId(Long wechatFansMessageId) {
        this.wechatFansMessageId = wechatFansMessageId;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getFssFansid() {
        return fssFansid;
    }

    public void setFssFansid(Long fssFansid) {
        this.fssFansid = fssFansid;
    }

    public String getFssOpenid() {
        return fssOpenid;
    }

    public void setFssOpenid(String fssOpenid) {
        this.fssOpenid = fssOpenid;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getVoiceType() {
        return voiceType;
    }

    public void setVoiceType(String voiceType) {
        this.voiceType = voiceType;
    }

    public String getThumbmediaId() {
        return thumbmediaId;
    }

    public void setThumbmediaId(String thumbmediaId) {
        this.thumbmediaId = thumbmediaId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public String getIsReply() {
        return isReply;
    }

    public void setIsReply(String isReply) {
        this.isReply = isReply;
    }

    public String getIsKeyword() {
        return isKeyword;
    }

    public void setIsKeyword(String isKeyword) {
        this.isKeyword = isKeyword;
    }

    public String getFssCreateddate() {
        return fssCreateddate;
    }

    public void setFssCreateddate(String fssCreateddate) {
        this.fssCreateddate = fssCreateddate;
    }

    public String getFssPublicaccount() {
        return fssPublicaccount;
    }

    public void setFssPublicaccount(String fssPublicaccount) {
        this.fssPublicaccount = fssPublicaccount;
    }

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFssLabel() {
        return fssLabel;
    }

    public void setFssLabel(String fssLabel) {
        this.fssLabel = fssLabel;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }
}