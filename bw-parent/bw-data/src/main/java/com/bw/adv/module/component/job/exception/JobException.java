package com.bw.adv.module.component.job.exception;


public class JobException extends RuntimeException {

	private static final long serialVersionUID = 4203482434232111182L;

	public JobException() {
		super();
	}

	public JobException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public JobException(String arg0) {
		super(arg0);
	}
}
