/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:GoodsShowMemberCardGrade.java
 * Package Name:com.sage.scrm.module.goods.model
 * Date:2016年7月18日下午6:53:59
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.goods.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ClassName:GoodsShowMemberCardGrade <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年7月18日 下午6:53:59 <br/>
 * scrmVersion standard
 * @author   dong.d
 * @version  jdk1.7
 * @see 	 
 */
public class GoodsShowMemberCardGrade implements Serializable{

	/**
	 * UID
	 */
	private static final long serialVersionUID = -7746816277153959987L;

	/**
	 * 关联ID
	 */
	private Long goodsShowMemberCardGradeId;
	
	/**
	 * 用户会员卡等级ID
	 */
	private Long memberCardGradeId;

	/**
	 * 兑换单ID
	 */
	private Long goodsShowId;
	
	/**
	 * 所需积分
	 */
	private BigDecimal pointsNumber;
	
	
	
	
	
	public Long getGoodsShowMemberCardGradeId() {
		return goodsShowMemberCardGradeId;
	}

	public void setGoodsShowMemberCardGradeId(Long goodsShowMemberCardGradeId) {
		this.goodsShowMemberCardGradeId = goodsShowMemberCardGradeId;
	}

	public Long getMemberCardGradeId() {
		return memberCardGradeId;
	}

	public void setMemberCardGradeId(Long memberCardGradeId) {
		this.memberCardGradeId = memberCardGradeId;
	}

	public Long getGoodsShowId() {
		return goodsShowId;
	}

	public void setGoodsShowId(Long goodsShowId) {
		this.goodsShowId = goodsShowId;
	}

	public BigDecimal getPointsNumber() {
		return pointsNumber;
	}

	public void setPointsNumber(BigDecimal pointsNumber) {
		this.pointsNumber = pointsNumber;
	}
	
}


