package com.bw.adv.module.activity.model;

public class Awards {
	private Long awardsId;

	private Long sceneId;

	private Long sceneGameInstanceId;

	private String awardsName;

	private String awardsEn;

	private String awardsCode;

	public Long getAwardsId() {
		return awardsId;
	}

	public void setAwardsId(Long awardsId) {
		this.awardsId = awardsId;
	}

	public Long getSceneId() {
		return sceneId;
	}

	public void setSceneId(Long sceneId) {
		this.sceneId = sceneId;
	}

	public String getAwardsName() {
		return awardsName;
	}

	public void setAwardsName(String awardsName) {
		this.awardsName = awardsName;
	}

	public String getAwardsEn() {
		return awardsEn;
	}

	public void setAwardsEn(String awardsEn) {
		this.awardsEn = awardsEn;
	}

	public String getAwardsCode() {
		return awardsCode;
	}

	public void setAwardsCode(String awardsCode) {
		this.awardsCode = awardsCode;
	}

	public Long getSceneGameInstanceId() {
		return sceneGameInstanceId;
	}

	public void setSceneGameInstanceId(Long sceneGameInstanceId) {
		this.sceneGameInstanceId = sceneGameInstanceId;
	}
}