package com.bw.adv.module.member.exception;

import com.bw.adv.api.constant.ResultStatus;
import com.bw.adv.module.base.exception.BaseRuntimeException;
import com.bw.adv.module.base.exception.ErrorData;


public class MemberException extends BaseRuntimeException{
	private static final long serialVersionUID = 4203482434232111182L;

	public MemberException(ErrorData errorData) {
		
		super(errorData);
		
	}

	public MemberException(ResultStatus resultStatus) {
		
		super(resultStatus);
		// TODO Auto-generated constructor stub
		
	}

	public MemberException(String errorMsg, ErrorData errorData) {
		
		super(errorMsg, errorData);
		// TODO Auto-generated constructor stub
		
	}

	public MemberException(String errorMsg, Throwable e) {
		
		super(errorMsg, e);
		// TODO Auto-generated constructor stub
		
	}

	public MemberException(String errorMsg) {
		
		super(errorMsg);
		// TODO Auto-generated constructor stub
		
	}

	public MemberException(Throwable e) {
		
		super(e);
		// TODO Auto-generated constructor stub
		
	}
	

}
