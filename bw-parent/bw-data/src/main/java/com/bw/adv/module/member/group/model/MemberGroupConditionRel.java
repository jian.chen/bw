package com.bw.adv.module.member.group.model;

public class MemberGroupConditionRel {
    private Long memberGroupConditionRelId;

    private Long memberGroupId;

    private Long conditionId;

    private Long nextConditionId;

    private String relationValue;

    public Long getMemberGroupConditionRelId() {
        return memberGroupConditionRelId;
    }

    public void setMemberGroupConditionRelId(Long memberGroupConditionRelId) {
        this.memberGroupConditionRelId = memberGroupConditionRelId;
    }

    public Long getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(Long memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public Long getConditionId() {
        return conditionId;
    }

    public void setConditionId(Long conditionId) {
        this.conditionId = conditionId;
    }

    public Long getNextConditionId() {
        return nextConditionId;
    }

    public void setNextConditionId(Long nextConditionId) {
        this.nextConditionId = nextConditionId;
    }

    public String getRelationValue() {
        return relationValue;
    }

    public void setRelationValue(String relationValue) {
        this.relationValue = relationValue;
    }
}