/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:Snippet.java
 * Package Name:com.sage.scrm.api.member.dto
 * Date:2015-8-17下午8:57:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.api.member.dto;

import com.bw.adv.module.member.model.Member;

/**
 * ClassName:MemberDto <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-8-17 下午8:57:35 <br/>
 * scrmVersion 1.0
 * 
 * @author mennan
 * @version jdk1.7
 * @see
 */

public class MemberDto extends Member {

	private String extAccountTypeId;
	private String bindingAccount;
	private String channelCode;
	private String comvCode; // 验证码
	private String originalMobile; // 原始手机号
	private String addressDetail; // 会员默认地址

	private String couponCode;
	private Long provinceId;
	
	private String entityCardNo;//实体卡号

	// 微信粉丝字段
	private String unionid = null;
	private String nickName = null;
	private String sex = null;
	private String cty = null;
	private String province = null;
	private String country = null;
	private String language = null;
	
	private String gradeName;
	private String cardImg;
	
	private String isBinding;//是否是绑定操作
	
	public String getExtAccountTypeId() {
		return extAccountTypeId;
	}

	public void setExtAccountTypeId(String extAccountTypeId) {
		this.extAccountTypeId = extAccountTypeId;
	}

	public String getBindingAccount() {
		return bindingAccount;
	}

	public void setBindingAccount(String bindingAccount) {
		this.bindingAccount = bindingAccount;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getOriginalMobile() {
		return originalMobile;
	}

	public void setOriginalMobile(String originalMobile) {
		this.originalMobile = originalMobile;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCty() {
		return cty;
	}

	public void setCty(String cty) {
		this.cty = cty;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getComvCode() {
		return comvCode;
	}

	public void setComvCode(String comvCode) {
		this.comvCode = comvCode;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public String getCardImg() {
		return cardImg;
	}

	public void setCardImg(String cardImg) {
		this.cardImg = cardImg;
	}

	public String getEntityCardNo() {
		return entityCardNo;
	}

	public void setEntityCardNo(String entityCardNo) {
		this.entityCardNo = entityCardNo;
	}

	public String getIsBinding() {
		return isBinding;
	}

	public void setIsBinding(String isBinding) {
		this.isBinding = isBinding;
	}

}
