package com.bw.adv.module.base.model;

public class QrCodeType {
    private Long qrCodeTypeId;

    private String qrCodeTypeCode;

    private String qrCodeTypeName;

    public Long getQrCodeTypeId() {
        return qrCodeTypeId;
    }

    public void setQrCodeTypeId(Long qrCodeTypeId) {
        this.qrCodeTypeId = qrCodeTypeId;
    }

    public String getQrCodeTypeCode() {
        return qrCodeTypeCode;
    }

    public void setQrCodeTypeCode(String qrCodeTypeCode) {
        this.qrCodeTypeCode = qrCodeTypeCode;
    }

    public String getQrCodeTypeName() {
        return qrCodeTypeName;
    }

    public void setQrCodeTypeName(String qrCodeTypeName) {
        this.qrCodeTypeName = qrCodeTypeName;
    }
}