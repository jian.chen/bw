package com.bw.adv.module.coupon.model;

public class CouponInstanceUseRecord {
    private Long couponInstanceUseRecordId;

    private Long couponInstanceId;

    private String couponInstanceCode;

    private Long useBy;

    private String cardNo;

    private Long orderId;

    private String createTime;

    private String useTime;

    private Long useChannel;

    private Long useStoreId;

    private String useStoreCode;

    private Long seq;

    public Long getCouponInstanceUseRecordId() {
        return couponInstanceUseRecordId;
    }

    public void setCouponInstanceUseRecordId(Long couponInstanceUseRecordId) {
        this.couponInstanceUseRecordId = couponInstanceUseRecordId;
    }

    public Long getCouponInstanceId() {
        return couponInstanceId;
    }

    public void setCouponInstanceId(Long couponInstanceId) {
        this.couponInstanceId = couponInstanceId;
    }

    public String getCouponInstanceCode() {
        return couponInstanceCode;
    }

    public void setCouponInstanceCode(String couponInstanceCode) {
        this.couponInstanceCode = couponInstanceCode;
    }

    public Long getUseBy() {
        return useBy;
    }

    public void setUseBy(Long useBy) {
        this.useBy = useBy;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public Long getUseChannel() {
        return useChannel;
    }

    public void setUseChannel(Long useChannel) {
        this.useChannel = useChannel;
    }

    public Long getUseStoreId() {
        return useStoreId;
    }

    public void setUseStoreId(Long useStoreId) {
        this.useStoreId = useStoreId;
    }

    public String getUseStoreCode() {
        return useStoreCode;
    }

    public void setUseStoreCode(String useStoreCode) {
        this.useStoreCode = useStoreCode;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }
}