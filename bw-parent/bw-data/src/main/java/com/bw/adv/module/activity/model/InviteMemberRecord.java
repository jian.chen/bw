package com.bw.adv.module.activity.model;

public class InviteMemberRecord {
    private Long inviteMemberRecordId;

    private Long memberId;

    private Long activityInstanceId;

    private String externalId;

    private String happenTime;

    private String releaseTime;

    private Boolean isReleased;
    private String releaseAt;

    public Long getInviteMemberRecordId() {
        return inviteMemberRecordId;
    }

    public void setInviteMemberRecordId(Long inviteMemberRecordId) {
        this.inviteMemberRecordId = inviteMemberRecordId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getActivityInstanceId() {
        return activityInstanceId;
    }

    public void setActivityInstanceId(Long activityInstanceId) {
        this.activityInstanceId = activityInstanceId;
    }

    public String getHappenTime() {
        return happenTime;
    }

    public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Boolean getIsReleased() {
        return isReleased;
    }

    public void setIsReleased(Boolean isReleased) {
        this.isReleased = isReleased;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getReleaseAt() {
        return releaseAt;
    }

    public void setReleaseAt(String releaseAt) {
        this.releaseAt = releaseAt;
    }
}