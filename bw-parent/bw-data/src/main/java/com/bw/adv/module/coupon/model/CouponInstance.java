package com.bw.adv.module.coupon.model;

import com.bw.adv.module.tools.Excel;

public class CouponInstance {
    private Long couponInstanceId;
    
    private Long couponTypeId;

	private Long couponId;
	
	private String couponName;
	
	private Long couponIssueId;

	@Excel(exportConvertSign = 1, exportFieldWidth = 10, exportName = "状态", importConvertSign = 0)
    private Long statusId;
	
    private Long orgId;

    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "发放渠道", importConvertSign = 0)
    private Long getChannelId;

    private Long useChannelId;
    
    private String externalId;

    private String couponInstanceCode;

    private String couponInstancePwd;

    private Long couponInstanceOwner;
    
    private Long couponInstanceUser;

    private String getTime;

    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "核销时间", importConvertSign = 0)
    private String usedTime;
    
    private String expireDate;
    
    private String createTime;
    
    private String updateTime;
    
    private Long storeId;
    
    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "发放时间", importConvertSign = 0)
    private String startDate;
    
    private Long activityInstanceId;
    
    @Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "来源", importConvertSign = 0)
    private String activityInstanceName;

    private String exchangeCode;
    private Long orderId;

    
    public Long getCouponInstanceId() {
        return couponInstanceId;
    }

    public void setCouponInstanceId(Long couponInstanceId) {
        this.couponInstanceId = couponInstanceId;
    }

    public Long getCouponTypeId() {
		return couponTypeId;
	}

	public void setCouponTypeId(Long couponTypeId) {
		this.couponTypeId = couponTypeId;
	}
	
    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getCouponIssueId() {
        return couponIssueId;
    }

    public void setCouponIssueId(Long couponIssueId) {
        this.couponIssueId = couponIssueId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getGetChannelId() {
        return getChannelId;
    }

    public void setGetChannelId(Long getChannelId) {
        this.getChannelId = getChannelId;
    }

    public Long getUseChannelId() {
        return useChannelId;
    }

    public void setUseChannelId(Long useChannelId) {
        this.useChannelId = useChannelId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getCouponInstanceCode() {
        return couponInstanceCode;
    }

    public void setCouponInstanceCode(String couponInstanceCode) {
        this.couponInstanceCode = couponInstanceCode;
    }

    public String getCouponInstancePwd() {
        return couponInstancePwd;
    }

    public void setCouponInstancePwd(String couponInstancePwd) {
        this.couponInstancePwd = couponInstancePwd;
    }

    public Long getCouponInstanceOwner() {
        return couponInstanceOwner;
    }

    public void setCouponInstanceOwner(Long couponInstanceOwner) {
        this.couponInstanceOwner = couponInstanceOwner;
    }
    
	public Long getCouponInstanceUser() {
		return couponInstanceUser;
	}

	public void setCouponInstanceUser(Long couponInstanceUser) {
		this.couponInstanceUser = couponInstanceUser;
	}

	public String getGetTime() {
        return getTime;
    }

    public void setGetTime(String getTime) {
        this.getTime = getTime;
    }

    public String getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(String usedTime) {
        this.usedTime = usedTime;
    }

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	 public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public Long getActivityInstanceId() {
		return activityInstanceId;
	}

	public void setActivityInstanceId(Long activityInstanceId) {
		this.activityInstanceId = activityInstanceId;
	}

	public String getActivityInstanceName() {
		return activityInstanceName;
	}

	public void setActivityInstanceName(String activityInstanceName) {
		this.activityInstanceName = activityInstanceName;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

    public String getExchangeCode() {
        return exchangeCode;
    }

    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

}