/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:SceneGameInstanceExp.java
 * Package Name:com.sage.scrm.module.activity.model.exp
 * Date:2016年2月17日下午3:36:22
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.model.exp;

import com.bw.adv.module.activity.model.SceneGameInstance;


/**
 * ClassName:SceneGameInstanceExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年2月17日 下午3:36:22 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class SceneGameInstanceExp extends SceneGameInstance {
	//参与人数
	private int totalCount;
	//中奖人数
	private int winCount;
	//中奖率
	private double winRate;
	//奖项名称
	private String awardsName;
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getWinCount() {
		return winCount;
	}
	public void setWinCount(int winCount) {
		this.winCount = winCount;
	}
	public double getWinRate() {
		return winRate;
	}
	public void setWinRate(double winRate) {
		this.winRate = winRate;
	}
	public String getAwardsName() {
		return awardsName;
	}
	public void setAwardsName(String awardsName) {
		this.awardsName = awardsName;
	}
	

}

