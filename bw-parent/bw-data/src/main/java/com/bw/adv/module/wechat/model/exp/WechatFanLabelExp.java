/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatFansLabelExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年9月3日下午3:18:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;

import java.util.List;

import com.bw.adv.module.wechat.model.WechatFanLabel;
import com.bw.adv.module.wechat.model.WechatFansLabel;

/**
 * ClassName:WechatFansLabelExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月3日 下午3:18:36 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WechatFanLabelExp extends WechatFanLabel {
	
	private List<WechatFansLabel> wechatFansLabelList;
	
	private String nickName;

	private String headImgUrl;

	private String openid;
	
	private String wechatFansLabel;

	public String getWechatFansLabel() {
		return wechatFansLabel;
	}

	public void setWechatFansLabel(String wechatFansLabel) {
		this.wechatFansLabel = wechatFansLabel;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public String getNickName() {
		return nickName;
	}
	
	public String getOpenid() {
		return openid;
	}

	public List<WechatFansLabel> getWechatFansLabelList() {
		return wechatFansLabelList;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public void setWechatFansLabelList(List<WechatFansLabel> wechatFansLabelList) {
		this.wechatFansLabelList = wechatFansLabelList;
	}

	
	
}

