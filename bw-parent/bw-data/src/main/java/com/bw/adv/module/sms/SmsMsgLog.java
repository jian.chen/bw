package com.bw.adv.module.sms;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jeoy.zhou on 3/15/16.
 */
public class SmsMsgLog implements Serializable {

    private static final long serialVersionUID = -3589277472915182712L;

    private Long smsMsgId;
    private String mobile;
    private String comments;
    private Date createTime;
    private String isPass;
    private String refId;
    private String remark;


    public SmsMsgLog() {
        this.createTime = new Date();
    }

    public SmsMsgLog(String mobile, String comments, String isPass, String refId, String remark) {
        this();
        this.mobile = mobile;
        this.comments = comments;
        this.isPass = isPass;
        this.refId = refId;
        this.remark = remark;
    }

    public Long getSmsMsgId() {
        return smsMsgId;
    }

    public void setSmsMsgId(Long smsMsgId) {
        this.smsMsgId = smsMsgId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getIsPass() {
        return isPass;
    }

    public void setIsPass(String isPass) {
        this.isPass = isPass;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
