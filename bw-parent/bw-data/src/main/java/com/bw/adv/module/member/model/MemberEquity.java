package com.bw.adv.module.member.model;

public class MemberEquity {
    private Long memberEquityId;

    private String equityName;

    private String equityContent;

    private String equityStarttime;

    private String equityEndtime;

    private String equityImg;
    
    private String statusId;

    public Long getMemberEquityId() {
        return memberEquityId;
    }

    public void setMemberEquityId(Long memberEquityId) {
        this.memberEquityId = memberEquityId;
    }

    public String getEquityName() {
        return equityName;
    }

    public void setEquityName(String equityName) {
        this.equityName = equityName;
    }

    public String getEquityContent() {
        return equityContent;
    }

    public void setEquityContent(String equityContent) {
        this.equityContent = equityContent;
    }

    public String getEquityStarttime() {
        return equityStarttime;
    }

    public void setEquityStarttime(String equityStarttime) {
        this.equityStarttime = equityStarttime;
    }

    public String getEquityEndtime() {
        return equityEndtime;
    }

    public void setEquityEndtime(String equityEndtime) {
        this.equityEndtime = equityEndtime;
    }

	public String getEquityImg() {
		return equityImg;
	}

	public void setEquityImg(String equityImg) {
		this.equityImg = equityImg;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

    
}