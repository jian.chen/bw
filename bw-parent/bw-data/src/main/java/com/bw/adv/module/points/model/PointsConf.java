package com.bw.adv.module.points.model;

import java.util.Date;

public class PointsConf {
    private Long pointsConfId;

    private String invalidTimeType;

    private String invalidTimeValue;

    private Long pointsRate;

    private String isActive;

    private String discountIsActive;

    private Date createTime;

    private Date updateTime;

    public Long getPointsConfId() {
        return pointsConfId;
    }

    public void setPointsConfId(Long pointsConfId) {
        this.pointsConfId = pointsConfId;
    }

    public String getInvalidTimeType() {
        return invalidTimeType;
    }

    public void setInvalidTimeType(String invalidTimeType) {
        this.invalidTimeType = invalidTimeType;
    }

    public String getInvalidTimeValue() {
        return invalidTimeValue;
    }

    public void setInvalidTimeValue(String invalidTimeValue) {
        this.invalidTimeValue = invalidTimeValue;
    }

    public Long getPointsRate() {
        return pointsRate;
    }

    public void setPointsRate(Long pointsRate) {
        this.pointsRate = pointsRate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getDiscountIsActive() {
        return discountIsActive;
    }

    public void setDiscountIsActive(String discountIsActive) {
        this.discountIsActive = discountIsActive;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}