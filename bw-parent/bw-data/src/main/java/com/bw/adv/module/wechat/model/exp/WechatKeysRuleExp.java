package com.bw.adv.module.wechat.model.exp;

import java.util.List;

import com.bw.adv.module.wechat.model.KeysGroup;
import com.bw.adv.module.wechat.model.WechatKeysRule;

public class WechatKeysRuleExp extends WechatKeysRule{
	
	private List<KeysGroup> keysGroupList;
	

	public List<KeysGroup> getKeysGroupList() {
		return keysGroupList;
	}

	public void setKeysGroupList(List<KeysGroup> keysGroupList) {
		this.keysGroupList = keysGroupList;
	}
	
}