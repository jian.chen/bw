package com.bw.adv.module.component.msg.model;

public class SmsInstance {
    private Long smsInstanceId;

    private Long smsTempId;
    
    private String smsTempName;
    
    private Long smsTempTypeId;

	private Long memberId;

    private Long smsChannelId;

    private String mobile;

    private String params;

    private String content;

    private String createTime;

    private String sendTime;

    private String isSend;

    public Long getSmsInstanceId() {
        return smsInstanceId;
    }

    public void setSmsInstanceId(Long smsInstanceId) {
        this.smsInstanceId = smsInstanceId;
    }

    public Long getSmsTempId() {
        return smsTempId;
    }

    public void setSmsTempId(Long smsTempId) {
        this.smsTempId = smsTempId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getSmsChannelId() {
        return smsChannelId;
    }

    public void setSmsChannelId(Long smsChannelId) {
        this.smsChannelId = smsChannelId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getIsSend() {
        return isSend;
    }

    public void setIsSend(String isSend) {
        this.isSend = isSend;
    }
    
    public String getSmsTempName() {
		return smsTempName;
	}

	public void setSmsTempName(String smsTempName) {
		this.smsTempName = smsTempName;
	}

	public Long getSmsTempTypeId() {
		return smsTempTypeId;
	}

	public void setSmsTempTypeId(Long smsTempTypeId) {
		this.smsTempTypeId = smsTempTypeId;
	}
}