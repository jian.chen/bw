package com.bw.adv.module.order.model;

import java.math.BigDecimal;

public class OrderCoupon {
    private Long orderCouponId;

    private Long orderId;

    private Long orderItemId;

    private Long couponInstanceId;

    private BigDecimal couponPercentage;

    private BigDecimal couponAmount;
    
    private String externalItemId;

    public Long getOrderCouponId() {
        return orderCouponId;
    }

    public void setOrderCouponId(Long orderCouponId) {
        this.orderCouponId = orderCouponId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Long getCouponInstanceId() {
        return couponInstanceId;
    }

    public void setCouponInstanceId(Long couponInstanceId) {
        this.couponInstanceId = couponInstanceId;
    }

    public BigDecimal getCouponPercentage() {
        return couponPercentage;
    }

    public void setCouponPercentage(BigDecimal couponPercentage) {
        this.couponPercentage = couponPercentage;
    }

    public BigDecimal getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(BigDecimal couponAmount) {
        this.couponAmount = couponAmount;
    }

	public String getExternalItemId() {
		return externalItemId;
	}

	public void setExternalItemId(String externalItemId) {
		this.externalItemId = externalItemId;
	}
    
}