/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:StoreExp.java
 * Package Name:com.sage.scrm.module.base.model.exp
 * Date:2015年9月3日下午3:37:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.model.exp;

import com.bw.adv.module.base.model.Store;

/**
 * ClassName:StoreExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月3日 下午3:37:28 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class StoreExp extends Store {
	
	private String orgName;
	
	private Long pareOrgId;
	
	private String pareOrgName;
	
	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Long getPareOrgId() {
		return pareOrgId;
	}

	public void setPareOrgId(Long pareOrgId) {
		this.pareOrgId = pareOrgId;
	}

	public String getPareOrgName() {
		return pareOrgName;
	}

	public void setPareOrgName(String pareOrgName) {
		this.pareOrgName = pareOrgName;
	}
	
	

}

