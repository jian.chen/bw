package com.bw.adv.module.member.message.model;

public class MemberMessageItem {
    private Long memberMessageItemId;

    private Long memberMessageId;

    private Long memberId;

    private String createTime;

    private String readTime;

    private String isRead;

    public Long getMemberMessageItemId() {
        return memberMessageItemId;
    }

    public void setMemberMessageItemId(Long memberMessageItemId) {
        this.memberMessageItemId = memberMessageItemId;
    }

    public Long getMemberMessageId() {
        return memberMessageId;
    }

    public void setMemberMessageId(Long memberMessageId) {
        this.memberMessageId = memberMessageId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getReadTime() {
        return readTime;
    }

    public void setReadTime(String readTime) {
        this.readTime = readTime;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }
}