/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatEventTypeEnum.java
 * Package Name:com.sage.scrm.module.wechat.enums
 * Date:2015年12月21日下午4:11:56
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.enums;
/**
 * ClassName:WechatEventTypeEnum <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月21日 下午4:11:56 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public enum WechatEventTypeEnum {
	CLICK(1,"click"),
	VIEW(2,"view");
	private Integer id;
	private String desc;
	
	private WechatEventTypeEnum(Integer id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public Integer getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static WechatEventTypeEnum[] getArray() {
		return WechatEventTypeEnum.values();
	}
	
	public static String getDesc(Integer id){
		if(id != null){
			WechatEventTypeEnum[] values = WechatEventTypeEnum.values();
			for(WechatEventTypeEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}

