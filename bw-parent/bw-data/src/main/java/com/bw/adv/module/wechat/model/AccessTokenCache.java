package com.bw.adv.module.wechat.model;

import com.bw.adv.core.utils.Id;

public class AccessTokenCache {
	
	/**
	 * 编号
	 */
    private Long accessTokenCacheId;
    
    /**
     * 微信账号ID
     */
    private Long wechatAccountId;
    
    /**
     * 访问令牌
     */
    private String accessToken;
    
    /**
     * 过期时间
     */
    private String timeOut;
    
    @Id
    public Long getAccessTokenCacheId() {
        return accessTokenCacheId;
    }

    public void setAccessTokenCacheId(Long accessTokenCacheId) {
        this.accessTokenCacheId = accessTokenCacheId;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }
}