package com.bw.adv.module.order.enums;


/**
 * ClassName:OrderSourceWayEnum <br/>
 * Date:     Mar 25, 2014 3:23:25 PM <br/>
 * @author   zhouy
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public enum OrderSourceWayEnum {
	
	MEMBER_CODE("1", "会员编码"),
	MEMBER_MOBILE("2", "手机号"),
	MEMBER_CARD("3,", "会员卡");
	
	private String code;
	private String commons;
	
	private OrderSourceWayEnum(String code, String commons) {
		this.code = code;
		this.commons = commons;
	}

	public String getCode() {
		return code;
	}

	public String getCommons() {
		return commons;
	}
	
}

