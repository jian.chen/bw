package com.bw.adv.module.report.model;

public class MemberSummaryAge {
    private Long summaryId;

    private Integer ageType;

    private Integer maleDayCount;

    private Integer femaleDayCount;

    private Integer otherDayCount;

    private Integer dayNewCount;

    private String summaryDate;

    public Long getSummaryId() {
        return summaryId;
    }

    public void setSummaryId(Long summaryId) {
        this.summaryId = summaryId;
    }

    public Integer getAgeType() {
        return ageType;
    }

    public void setAgeType(Integer ageType) {
        this.ageType = ageType;
    }

    public Integer getMaleDayCount() {
        return maleDayCount;
    }

    public void setMaleDayCount(Integer maleDayCount) {
        this.maleDayCount = maleDayCount;
    }

    public Integer getFemaleDayCount() {
        return femaleDayCount;
    }

    public void setFemaleDayCount(Integer femaleDayCount) {
        this.femaleDayCount = femaleDayCount;
    }

    public Integer getOtherDayCount() {
        return otherDayCount;
    }

    public void setOtherDayCount(Integer otherDayCount) {
        this.otherDayCount = otherDayCount;
    }

    public Integer getDayNewCount() {
        return dayNewCount;
    }

    public void setDayNewCount(Integer dayNewCount) {
        this.dayNewCount = dayNewCount;
    }

    public String getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = summaryDate;
    }
}