package com.bw.adv.module.sys.model.exp;

import com.bw.adv.module.sys.model.SysRoleData;
import com.bw.adv.module.sys.model.SysRoleDataType;

/**
 * Created by jeoy.zhou on 1/12/16.
 */
public class SysRoleDataExp extends SysRoleData implements Comparable<SysRoleDataExp>{

    private String dataTypeName;
    private String className;
    private String fieldName;

    public SysRoleDataExp(){}

    public SysRoleDataExp(SysRoleData sysRoleData, SysRoleDataType dataType) {
        super.setStatus(sysRoleData.getStatus());
        super.setDataTypeId(sysRoleData.getDataTypeId());
        super.setFilterTypeId(sysRoleData.getFilterTypeId());
        super.setOperationParams(sysRoleData.getOperationParams());
        super.setRoleId(sysRoleData.getRoleId());
        setDataTypeName(dataType.getDataTypeName());
        setClassName(dataType.getClassName());
        setFieldName(dataType.getFieldName());
    }


    public String getDataTypeName() {
        return dataTypeName;
    }

    public void setDataTypeName(String dataTypeName) {
        this.dataTypeName = dataTypeName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public int compareTo(SysRoleDataExp o) {
        Integer filterType = this.getFilterTypeId() - o.getFilterTypeId();
        Integer dataType = this.getDataTypeId() - o.getDataTypeId();
        return filterType != 0 ? filterType :
                dataType != 0 ? dataType :
                        (int) (this.getRoleId() - o.getRoleId());
    }

}
