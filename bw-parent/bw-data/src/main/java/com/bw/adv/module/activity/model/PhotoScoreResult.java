package com.bw.adv.module.activity.model;

public class PhotoScoreResult {
    private Long id;

    private String photoResult;

    private String error;

    private String createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhotoResult() {
        return photoResult;
    }

    public void setPhotoResult(String photoResult) {
        this.photoResult = photoResult;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}