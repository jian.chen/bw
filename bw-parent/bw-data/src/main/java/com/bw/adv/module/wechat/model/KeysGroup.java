package com.bw.adv.module.wechat.model;

public class KeysGroup {
	
	/**
	 * 关键字分组ID
	 */
    private Long keysGroupId;
    
    /**
     * 微信账号ID
     */
    private Long wechatAccountId;
    
    /**
     * 状态ID
     */
    private Long statusId;
    
    /**
     * 分组名称
     */
	private String keysGroupName;
	
	/**
	 * 创建人
	 */
	private Long createBy;
	
	/**
	 * 创建时间
	 */
    private String createDate;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateDate;
    
    /**
     * 删除标识
     */
    private Long isDeleted;
    
    /**
     * 是否全部回复
     */
    private String isAllReply;
    
    public String getIsAllReply() {
		return isAllReply;
	}

	public void setIsAllReply(String isAllReply) {
		this.isAllReply = isAllReply;
	}

	public Long getCreateBy() {
        return createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public Long getIsDeleted() {
        return isDeleted;
    }

    public Long getKeysGroupId() {
        return keysGroupId;
    }

    public String getKeysGroupName() {
        return keysGroupName;
    }

    public Long getStatusId() {
        return statusId;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public void setIsDeleted(Long isDeleted) {
        this.isDeleted = isDeleted;
    }

    public void setKeysGroupId(Long keysGroupId) {
        this.keysGroupId = keysGroupId;
    }

    public void setKeysGroupName(String keysGroupName) {
        this.keysGroupName = keysGroupName;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }
}