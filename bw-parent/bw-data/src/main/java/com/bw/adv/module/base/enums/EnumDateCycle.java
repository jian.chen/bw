/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:EnumDateCycle.java
 * Package Name:com.sage.scrm.module.base.enums
 * Date:2015年11月24日下午4:16:20
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.enums;
/**
 * ClassName:EnumDateCycle <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月24日 下午4:16:20 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public enum EnumDateCycle {
	
	EVERY(1L, "日"), ALL(5L, "所有");
	
	private long dateCycleId;
	private String dateCycleName;
	
	private EnumDateCycle(long dateCycleId, String dateCycleName){
		this.dateCycleId = dateCycleId;
		this.dateCycleName = dateCycleName;
	}
	
	public long getDateCycleId() {
		return dateCycleId;
	}
	public void setDateCycleId(long dateCycleId) {
		this.dateCycleId = dateCycleId;
	}
	public String getDateCycleName() {
		return dateCycleName;
	}
	public void setDateCycleName(String dateCycleName) {
		this.dateCycleName = dateCycleName;
	}
	
	
	
}

