/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:SmsInstanceExp.java
 * Package Name:com.sage.scrm.module.component.msg.model.exp
 * Date:2015年8月27日下午4:59:52
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.component.msg.model.exp;

import com.bw.adv.module.component.msg.model.SmsInstance;

/**
 * ClassName:SmsInstanceExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月27日 下午4:59:52 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class SmsInstanceExp extends SmsInstance{

	private String smsTempName;

	public String getSmsTempName() {
		return smsTempName;
	}

	public void setSmsTempName(String smsTempName) {
		this.smsTempName = smsTempName;
	}
	
	
}

