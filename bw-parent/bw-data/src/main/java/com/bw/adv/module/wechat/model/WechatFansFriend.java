package com.bw.adv.module.wechat.model;

public class WechatFansFriend {
    private Long wechatFansFriendId;

    private Long friMainrid;

    private Long friOtherid;

    private Long createBy;

    private String createTime;

    private Long updateBy;

    private String updateTime;

    private String isDelete;

    public Long getWechatFansFriendId() {
        return wechatFansFriendId;
    }

    public void setWechatFansFriendId(Long wechatFansFriendId) {
        this.wechatFansFriendId = wechatFansFriendId;
    }

    public Long getFriMainrid() {
        return friMainrid;
    }

    public void setFriMainrid(Long friMainrid) {
        this.friMainrid = friMainrid;
    }

    public Long getFriOtherid() {
        return friOtherid;
    }

    public void setFriOtherid(Long friOtherid) {
        this.friOtherid = friOtherid;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }
}