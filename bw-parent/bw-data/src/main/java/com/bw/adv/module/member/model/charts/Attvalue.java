package com.bw.adv.module.member.model.charts;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by jeoy.zhou on 2/29/16.
 */
@XmlRootElement(name = "attvalue")
public class Attvalue implements Serializable {

    private String clazz;
    private String value;

    public Attvalue() {}

    public Attvalue(String clazz, String value) {
        this.clazz = clazz;
        this.value = value;
    }

    @XmlAttribute(name = "for")
    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    @XmlAttribute(name = "value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
