package com.bw.adv.module.report.coupon.model;

public class CouponSingleSummaryDay {
    private Long couponSingleSummaryDayId;

    private Long couponId;

    private Integer summaryYear;

    private Integer summaryMonth;

    private Integer summaryDay;

    private String summaryDate;

    private Integer issueCount;

    private Integer useCount;

    private String createTime;

    public Long getCouponSingleSummaryDayId() {
        return couponSingleSummaryDayId;
    }

    public void setCouponSingleSummaryDayId(Long couponSingleSummaryDayId) {
        this.couponSingleSummaryDayId = couponSingleSummaryDayId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Integer getSummaryYear() {
        return summaryYear;
    }

    public void setSummaryYear(Integer summaryYear) {
        this.summaryYear = summaryYear;
    }

    public Integer getSummaryMonth() {
        return summaryMonth;
    }

    public void setSummaryMonth(Integer summaryMonth) {
        this.summaryMonth = summaryMonth;
    }

    public Integer getSummaryDay() {
        return summaryDay;
    }

    public void setSummaryDay(Integer summaryDay) {
        this.summaryDay = summaryDay;
    }

    public String getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = summaryDate;
    }

    public Integer getIssueCount() {
        return issueCount;
    }

    public void setIssueCount(Integer issueCount) {
        this.issueCount = issueCount;
    }

    public Integer getUseCount() {
        return useCount;
    }

    public void setUseCount(Integer useCount) {
        this.useCount = useCount;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}