package com.bw.adv.module.activity.model;

import java.math.BigDecimal;

public class SceneGameInstance {
	private Long sceneGameInstanceId;

	private Long sceneGameId;

	private String sceneGameInstanceCode;

	private Long statusId;

	private String instanceName;

	private Long createBy;

	private Long updateBy;

	private String createTime;

	private String updateTime;

	private String startTime;

	private String endTime;

	private String isAccord;

	private String luckyDrawType;

	private Integer luckyDrawValue;

	private String instanceDetail;

	private String instanceDesc;

	private String remark;

	private String luckyStartTime;

	private String luckyEndTime;

	private String sptIsuseforgroupmember;

	private Long sptMgoid;

	private String sptClues;

	private String sptIsusememberlabel;

	private String sptMlrid;

	private String sptIssharefamily;

	private String sptProducer;
	
	private String sptGrades;
	
	private String isNeedPoints;
	
	private BigDecimal needPointsAmount;

	public Long getSceneGameInstanceId() {
		return sceneGameInstanceId;
	}

	public void setSceneGameInstanceId(Long sceneGameInstanceId) {
		this.sceneGameInstanceId = sceneGameInstanceId;
	}

	public Long getSceneGameId() {
		return sceneGameId;
	}

	public void setSceneGameId(Long sceneGameId) {
		this.sceneGameId = sceneGameId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getIsAccord() {
		return isAccord;
	}

	public void setIsAccord(String isAccord) {
		this.isAccord = isAccord;
	}

	public String getLuckyDrawType() {
		return luckyDrawType;
	}

	public void setLuckyDrawType(String luckyDrawType) {
		this.luckyDrawType = luckyDrawType;
	}

	public Integer getLuckyDrawValue() {
		return luckyDrawValue;
	}

	public void setLuckyDrawValue(Integer luckyDrawValue) {
		this.luckyDrawValue = luckyDrawValue;
	}

	public String getInstanceDetail() {
		return instanceDetail;
	}

	public void setInstanceDetail(String instanceDetail) {
		this.instanceDetail = instanceDetail;
	}

	public String getInstanceDesc() {
		return instanceDesc;
	}

	public void setInstanceDesc(String instanceDesc) {
		this.instanceDesc = instanceDesc;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getLuckyStartTime() {
		return luckyStartTime;
	}

	public void setLuckyStartTime(String luckyStartTime) {
		this.luckyStartTime = luckyStartTime;
	}

	public String getLuckyEndTime() {
		return luckyEndTime;
	}

	public void setLuckyEndTime(String luckyEndTime) {
		this.luckyEndTime = luckyEndTime;
	}

	public String getSptIsuseforgroupmember() {
		return sptIsuseforgroupmember;
	}

	public void setSptIsuseforgroupmember(String sptIsuseforgroupmember) {
		this.sptIsuseforgroupmember = sptIsuseforgroupmember;
	}

	public Long getSptMgoid() {
		return sptMgoid;
	}

	public void setSptMgoid(Long sptMgoid) {
		this.sptMgoid = sptMgoid;
	}

	public String getSptClues() {
		return sptClues;
	}

	public void setSptClues(String sptClues) {
		this.sptClues = sptClues;
	}

	public String getSptIsusememberlabel() {
		return sptIsusememberlabel;
	}

	public void setSptIsusememberlabel(String sptIsusememberlabel) {
		this.sptIsusememberlabel = sptIsusememberlabel;
	}

	public String getSptMlrid() {
		return sptMlrid;
	}

	public void setSptMlrid(String sptMlrid) {
		this.sptMlrid = sptMlrid;
	}

	public String getSptIssharefamily() {
		return sptIssharefamily;
	}

	public void setSptIssharefamily(String sptIssharefamily) {
		this.sptIssharefamily = sptIssharefamily;
	}

	public String getSptProducer() {
		return sptProducer;
	}

	public void setSptProducer(String sptProducer) {
		this.sptProducer = sptProducer;
	}

	public String getSceneGameInstanceCode() {
		return sceneGameInstanceCode;
	}

	public void setSceneGameInstanceCode(String sceneGameInstanceCode) {
		this.sceneGameInstanceCode = sceneGameInstanceCode;
	}

	public String getSptGrades() {
		return sptGrades;
	}

	public void setSptGrades(String sptGrades) {
		this.sptGrades = sptGrades;
	}

	public String getIsNeedPoints() {
		return isNeedPoints;
	}

	public void setIsNeedPoints(String isNeedPoints) {
		this.isNeedPoints = isNeedPoints;
	}

	public BigDecimal getNeedPointsAmount() {
		return needPointsAmount;
	}

	public void setNeedPointsAmount(BigDecimal needPointsAmount) {
		this.needPointsAmount = needPointsAmount;
	}
	
}