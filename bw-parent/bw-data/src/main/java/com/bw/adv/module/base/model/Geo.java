package com.bw.adv.module.base.model;

public class Geo {
    private Long geoId;

    private Long geoTypeId;

    private String geoCode;

    private String geoName;

    private String abbreviation;

    private String geoName1;

    private String geoName2;

    private String comments;

    private Long seq;

    private String externalId;

    public Long getGeoId() {
        return geoId;
    }

    public void setGeoId(Long geoId) {
        this.geoId = geoId;
    }

    public Long getGeoTypeId() {
        return geoTypeId;
    }

    public void setGeoTypeId(Long geoTypeId) {
        this.geoTypeId = geoTypeId;
    }

    public String getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(String geoCode) {
        this.geoCode = geoCode;
    }

    public String getGeoName() {
        return geoName;
    }

    public void setGeoName(String geoName) {
        this.geoName = geoName;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getGeoName1() {
        return geoName1;
    }

    public void setGeoName1(String geoName1) {
        this.geoName1 = geoName1;
    }

    public String getGeoName2() {
        return geoName2;
    }

    public void setGeoName2(String geoName2) {
        this.geoName2 = geoName2;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}