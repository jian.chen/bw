package com.bw.adv.module.wechat.model;

public class WechatFanLabel {
	
	/**
	 * 粉丝标签关系ID
	 */
    private Long wechatFanLabelId;
    
    /**
     * 粉丝标签ID
     */
    private Long wechatFansLabelId;

    /**
     * 微信粉丝ID
     */
    private Long wechatFansId;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
    private String createdTime;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateTime;
    
    /**
     * 删除标识
     */
    private String isDelete;

    public Long getWechatFanLabelId() {
        return wechatFanLabelId;
    }

    public void setWechatFanLabelId(Long wechatFanLabelId) {
        this.wechatFanLabelId = wechatFanLabelId;
    }

    public Long getWechatFansLabelId() {
        return wechatFansLabelId;
    }

    public void setWechatFansLabelId(Long wechatFansLabelId) {
        this.wechatFansLabelId = wechatFansLabelId;
    }

    public Long getWechatFansId() {
        return wechatFansId;
    }

    public void setWechatFansId(Long wechatFansId) {
        this.wechatFansId = wechatFansId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }
}