package com.bw.adv.module.activity.model;

public class PrizeInstance {
    private Long prizeInstanceId;

    private String prizeInstanceCode;

    private Long prizeId;

    private Long statusId;

    private String exchangeCode1;

    private String exchangeCode2;

    private String exchangeCode3;

    private String exchangeCode4;

    public Long getPrizeInstanceId() {
        return prizeInstanceId;
    }

    public void setPrizeInstanceId(Long prizeInstanceId) {
        this.prizeInstanceId = prizeInstanceId;
    }

    public String getPrizeInstanceCode() {
        return prizeInstanceCode;
    }

    public void setPrizeInstanceCode(String prizeInstanceCode) {
        this.prizeInstanceCode = prizeInstanceCode;
    }

    public Long getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(Long prizeId) {
        this.prizeId = prizeId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getExchangeCode1() {
        return exchangeCode1;
    }

    public void setExchangeCode1(String exchangeCode1) {
        this.exchangeCode1 = exchangeCode1;
    }

    public String getExchangeCode2() {
        return exchangeCode2;
    }

    public void setExchangeCode2(String exchangeCode2) {
        this.exchangeCode2 = exchangeCode2;
    }

    public String getExchangeCode3() {
        return exchangeCode3;
    }

    public void setExchangeCode3(String exchangeCode3) {
        this.exchangeCode3 = exchangeCode3;
    }

    public String getExchangeCode4() {
        return exchangeCode4;
    }

    public void setExchangeCode4(String exchangeCode4) {
        this.exchangeCode4 = exchangeCode4;
    }
}