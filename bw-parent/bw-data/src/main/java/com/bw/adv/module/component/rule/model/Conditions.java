package com.bw.adv.module.component.rule.model;

public class Conditions {
    private Long conditionsId;

    private Long ruleId;

    private String conditionsCode;

    private String conditionName;

    private Integer groupNo;

    private Integer seq;

    private String cnComments;

    private String enComments;

    public Long getConditionsId() {
        return conditionsId;
    }

    public void setConditionsId(Long conditionsId) {
        this.conditionsId = conditionsId;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getConditionsCode() {
        return conditionsCode;
    }

    public void setConditionsCode(String conditionsCode) {
        this.conditionsCode = conditionsCode;
    }

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public Integer getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(Integer groupNo) {
        this.groupNo = groupNo;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getCnComments() {
        return cnComments;
    }

    public void setCnComments(String cnComments) {
        this.cnComments = cnComments;
    }

    public String getEnComments() {
        return enComments;
    }

    public void setEnComments(String enComments) {
        this.enComments = enComments;
    }
}