package com.bw.adv.module.component.job.model;

public class ComTaskParam {
    private Long taskParamId;

    private Long taskId;

    private String taskParamType;

    private String taskParamValue;

    private String taskParamName;

    public Long getTaskParamId() {
        return taskParamId;
    }

    public void setTaskParamId(Long taskParamId) {
        this.taskParamId = taskParamId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskParamType() {
        return taskParamType;
    }

    public void setTaskParamType(String taskParamType) {
        this.taskParamType = taskParamType;
    }

    public String getTaskParamValue() {
        return taskParamValue;
    }

    public void setTaskParamValue(String taskParamValue) {
        this.taskParamValue = taskParamValue;
    }

    public String getTaskParamName() {
        return taskParamName;
    }

    public void setTaskParamName(String taskParamName) {
        this.taskParamName = taskParamName;
    }
}