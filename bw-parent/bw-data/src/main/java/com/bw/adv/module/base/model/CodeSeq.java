package com.bw.adv.module.base.model;

public class CodeSeq {
    private Long codeSeqId;

    private Long codeSeqTypeId;

    private Long seq;

    public Long getCodeSeqId() {
        return codeSeqId;
    }

    public void setCodeSeqId(Long codeSeqId) {
        this.codeSeqId = codeSeqId;
    }

    public Long getCodeSeqTypeId() {
        return codeSeqTypeId;
    }

    public void setCodeSeqTypeId(Long codeSeqTypeId) {
        this.codeSeqTypeId = codeSeqTypeId;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }
}