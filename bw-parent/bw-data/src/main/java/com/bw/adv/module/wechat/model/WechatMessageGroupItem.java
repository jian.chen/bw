package com.bw.adv.module.wechat.model;

public class WechatMessageGroupItem {
	
	/**
	 * 记录编号
	 */
    private Long wechatMessageGroupItemId;
    
    /**
     * 微信粉丝ID
     */
    private Long wechatFansId;
    
    /**
     * 消息ID
     */
    private Long wechatGroupMessageId;
    
    /**
     * 状态ID
     */
    private Long statusId;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
    private String createTime;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateTime;
    
    /**
     * 删除标识
     */
    private String isDelete;

    public Long getWechatMessageGroupItemId() {
        return wechatMessageGroupItemId;
    }

    public void setWechatMessageGroupItemId(Long wechatMessageGroupItemId) {
        this.wechatMessageGroupItemId = wechatMessageGroupItemId;
    }

    public Long getWechatFansId() {
        return wechatFansId;
    }

    public void setWechatFansId(Long wechatFansId) {
        this.wechatFansId = wechatFansId;
    }

    public Long getWechatGroupMessageId() {
        return wechatGroupMessageId;
    }

    public void setWechatGroupMessageId(Long wechatGroupMessageId) {
        this.wechatGroupMessageId = wechatGroupMessageId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }
}