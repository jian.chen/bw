/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatMenuJson.java
 * Package Name:com.sage.scrm.module.wechat.model
 * Date:2015年9月5日下午3:01:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model;

import java.util.List;

/**
 * ClassName:WechatMenuJson <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月5日 下午3:01:36 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class WechatMenuJson {
	List<WechatButton> button;

	public List<WechatButton> getButton() {
		return button;
	}

	public void setButton(List<WechatButton> button) {
		this.button = button;
	}

}

