package com.bw.adv.module.common.model;

public class SqlAttributeType {
    private String typeName;

    private Long sqlAttributeTypeId;

    private String typeCode;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getSqlAttributeTypeId() {
        return sqlAttributeTypeId;
    }

    public void setSqlAttributeTypeId(Long sqlAttributeTypeId) {
        this.sqlAttributeTypeId = sqlAttributeTypeId;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }
}