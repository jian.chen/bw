package com.bw.adv.module.wechat.model;

public class WechatKeysRule {
	
	/**
	 * 关键字信息ID
	 */
    private Long wechatKeysRuleId;
    
    /***
     * 关键字分组ID
     */
    private Long keysGroupId;
    
    /**
     * 微信账号ID
     */
    private Long wechatAccountId;
    
    /**
     * 关键字
     */
    private String keyword;
    
    /**
     * 匹配模式
     */
    private String matchType;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
    private String createDate;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间		
     */
    private String updateDate;
    
    /**
     * 删除标识
     */
    private String isDeleted;
    
    /**
     * 回复类型
     */
    private String replyType;
    
    /**
     * 状态ID
     */
    private Long statusId;

    public Long getWechatKeysRuleId() {
        return wechatKeysRuleId;
    }

    public void setWechatKeysRuleId(Long wechatKeysRuleId) {
        this.wechatKeysRuleId = wechatKeysRuleId;
    }

    public Long getKeysGroupId() {
        return keysGroupId;
    }

    public void setKeysGroupId(Long keysGroupId) {
        this.keysGroupId = keysGroupId;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getReplyType() {
        return replyType;
    }

    public void setReplyType(String replyType) {
        this.replyType = replyType;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}