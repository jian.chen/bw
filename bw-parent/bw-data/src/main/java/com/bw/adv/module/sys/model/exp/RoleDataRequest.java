package com.bw.adv.module.sys.model.exp;

import java.util.List;
import java.util.Map;

/**
 * Created by jeoy.zhou on 1/13/16.
 */
public class RoleDataRequest {

    private Long roleId;
    private Integer filterTypeId;
    /**
     * key dataTypeId;
     * value operaterParmas
     */
    private Map<Integer, List<String>> configMap;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Integer getFilterTypeId() {
        return filterTypeId;
    }

    public void setFilterTypeId(Integer filterTypeId) {
        this.filterTypeId = filterTypeId;
    }

    public Map<Integer, List<String>> getConfigMap() {
        return configMap;
    }

    public void setConfigMap(Map<Integer, List<String>> configMap) {
        this.configMap = configMap;
    }
}
