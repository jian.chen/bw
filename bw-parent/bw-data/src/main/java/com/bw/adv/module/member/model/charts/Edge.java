package com.bw.adv.module.member.model.charts;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "edge")
public class Edge {

	private Integer id;
	
	private Integer source;
	
	private Integer target;
	
	private Double weight;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public Integer getTarget() {
		return target;
	}

	public void setTarget(Integer target) {
		this.target = target;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
	
}
