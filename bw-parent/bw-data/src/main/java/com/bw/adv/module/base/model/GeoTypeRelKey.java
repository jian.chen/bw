package com.bw.adv.module.base.model;

public class GeoTypeRelKey {
    private Long geoTypeId;

    private Long subGeoTypeId;

    public Long getGeoTypeId() {
        return geoTypeId;
    }

    public void setGeoTypeId(Long geoTypeId) {
        this.geoTypeId = geoTypeId;
    }

    public Long getSubGeoTypeId() {
        return subGeoTypeId;
    }

    public void setSubGeoTypeId(Long subGeoTypeId) {
        this.subGeoTypeId = subGeoTypeId;
    }
}