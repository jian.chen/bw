/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:GradeConvertRecordExp.java
 * Package Name:com.sage.scrm.module.member.model.exp
 * Date:2015年8月21日上午11:56:55
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.model.exp;

import com.bw.adv.module.member.model.GradeConvertRecord;

/**
 * ClassName:GradeConvertRecordExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月21日 上午11:56:55 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class GradeConvertRecordExp extends GradeConvertRecord {
	
    private String srcGradeName;

    private String targetGradeName;

	public String getSrcGradeName() {
		return srcGradeName;
	}

	public void setSrcGradeName(String srcGradeName) {
		this.srcGradeName = srcGradeName;
	}

	public String getTargetGradeName() {
		return targetGradeName;
	}

	public void setTargetGradeName(String targetGradeName) {
		this.targetGradeName = targetGradeName;
	}
	

}

