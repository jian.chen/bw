package com.bw.adv.module.member.model;

public class MemberExtInfo {
    private Long memberId;

    private Long profession;

    private Long income;

    private String hobby;

    private String isMarried;

    private Long zodiac;

    private Long chineseZodiac;

    private Long blood;

    private Long ages;

    private String idCardNo;

    private String contactPhone;

    private String contactAddress;

    private String remark;

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getProfession() {
        return profession;
    }

    public void setProfession(Long profession) {
        this.profession = profession;
    }

    public Long getIncome() {
        return income;
    }

    public void setIncome(Long income) {
        this.income = income;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getIsMarried() {
        return isMarried;
    }

    public void setIsMarried(String isMarried) {
        this.isMarried = isMarried;
    }

    public Long getZodiac() {
        return zodiac;
    }

    public void setZodiac(Long zodiac) {
        this.zodiac = zodiac;
    }

    public Long getChineseZodiac() {
        return chineseZodiac;
    }

    public void setChineseZodiac(Long chineseZodiac) {
        this.chineseZodiac = chineseZodiac;
    }

    public Long getBlood() {
        return blood;
    }

    public void setBlood(Long blood) {
        this.blood = blood;
    }

    public Long getAges() {
        return ages;
    }

    public void setAges(Long ages) {
        this.ages = ages;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}