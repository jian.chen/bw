package com.bw.adv.module.activity.model;

public class MemberWinningAccord {
	  /**
     * 
     */
    private Long memberWinningAccordId;

    /**
     * 
     */
    private Long sceneId;

    /**
     * 
     */
    private Long qrCodeId;

    /**
     * 
     */
    private String mapsQrCode;

    /**
     * 
     */
    private Long memberId;

    /**
     * 
     */
    private String createDate;

    /**
     * 
     */
    private String createTime;

    /**
     * 
     */
    private String useTime;

    /**
     * 
     */
    private String isUsed;

    /**
     * 
     */
    private Long version;

    /**
     * 
     */
    private Integer accordType;

    /**
     * 
     * @return MEMBER_WINNING_ACCORD_ID 
     */
    public Long getMemberWinningAccordId() {
        return memberWinningAccordId;
    }

    /**
     * 
     * @param memberWinningAccordId 
     */
    public void setMemberWinningAccordId(Long memberWinningAccordId) {
        this.memberWinningAccordId = memberWinningAccordId;
    }

    /**
     * 
     * @return SCENE_ID 
     */
    public Long getSceneId() {
        return sceneId;
    }

    /**
     * 
     * @param sceneId 
     */
    public void setSceneId(Long sceneId) {
        this.sceneId = sceneId;
    }

    /**
     * 
     * @return QR_CODE_ID 
     */
    public Long getQrCodeId() {
        return qrCodeId;
    }

    /**
     * 
     * @param qrCodeId 
     */
    public void setQrCodeId(Long qrCodeId) {
        this.qrCodeId = qrCodeId;
    }

    /**
     * 
     * @return MAPS_QR_CODE 
     */
    public String getMapsQrCode() {
        return mapsQrCode;
    }

    /**
     * 
     * @param mapsQrCode 
     */
    public void setMapsQrCode(String mapsQrCode) {
        this.mapsQrCode = mapsQrCode == null ? null : mapsQrCode.trim();
    }

    /**
     * 
     * @return MEMBER_ID 
     */
    public Long getMemberId() {
        return memberId;
    }

    /**
     * 
     * @param memberId 
     */
    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    /**
     * 
     * @return CREATE_DATE 
     */
    public String getCreateDate() {
        return createDate;
    }

    /**
     * 
     * @param createDate 
     */
    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    /**
     * 
     * @return CREATE_TIME 
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 
     * @param createTime 
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * 
     * @return USE_TIME 
     */
    public String getUseTime() {
        return useTime;
    }

    /**
     * 
     * @param useTime 
     */
    public void setUseTime(String useTime) {
        this.useTime = useTime == null ? null : useTime.trim();
    }

    /**
     * 
     * @return IS_USED 
     */
    public String getIsUsed() {
        return isUsed;
    }

    /**
     * 
     * @param isUsed 
     */
    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed == null ? null : isUsed.trim();
    }

    /**
     * 
     * @return VERSION 
     */
    public Long getVersion() {
        return version;
    }

    /**
     * 
     * @param version 
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * 
     * @return ACCORD_TYPE 
     */
    public Integer getAccordType() {
        return accordType;
    }

    /**
     * 
     * @param accordType 
     */
    public void setAccordType(Integer accordType) {
        this.accordType = accordType;
    }
}