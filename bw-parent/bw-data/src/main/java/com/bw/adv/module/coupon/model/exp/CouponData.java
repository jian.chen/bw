/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-web
 * File Name:CouponData.java
 * Package Name:com.sage.scrm.bk.masterdate.domain
 * Date:2015-12-15上午10:48:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.model.exp;

import java.util.ArrayList;
import java.util.List;

import com.bw.adv.module.coupon.model.CouponType;

/**
 * ClassName:优惠券主数据 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-12-15 上午10:48:04 <br/>
 * scrmVersion 1.0
 * @author   mennan
 * @version  jdk1.7
 * @see 	 
 */
public class CouponData {
	
	private List<CouponType> couponTypeList = new ArrayList<CouponType>();
	
	public List<CouponType> getCouponTypeList() {
		return couponTypeList;
	}
	
	public void addCouponData(CouponType couponType){
		couponTypeList.add(couponType);
	}
	
}

