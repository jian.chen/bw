package com.bw.adv.module.member.complain.model;

public class ComplainType {
    private Long complainTypeId;

    private String complainTypeCode;

    private String complainTypeName;

    private String complainTypeDesc;

    public Long getComplainTypeId() {
        return complainTypeId;
    }

    public void setComplainTypeId(Long complainTypeId) {
        this.complainTypeId = complainTypeId;
    }

    public String getComplainTypeCode() {
        return complainTypeCode;
    }

    public void setComplainTypeCode(String complainTypeCode) {
        this.complainTypeCode = complainTypeCode;
    }

    public String getComplainTypeName() {
        return complainTypeName;
    }

    public void setComplainTypeName(String complainTypeName) {
        this.complainTypeName = complainTypeName;
    }

    public String getComplainTypeDesc() {
        return complainTypeDesc;
    }

    public void setComplainTypeDesc(String complainTypeDesc) {
        this.complainTypeDesc = complainTypeDesc;
    }
}