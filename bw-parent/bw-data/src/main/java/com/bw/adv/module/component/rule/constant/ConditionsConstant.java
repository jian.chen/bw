package com.bw.adv.module.component.rule.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.component.rule.model.Conditions;

public class ConditionsConstant extends AbstractConstant<Conditions> {

	public static final ConditionsConstant MEMBER_TAG = new ConditionsConstant(1L, "MEMBER_TAG", "会员标签");
	public static final ConditionsConstant MEMBER_GROUP = new ConditionsConstant(2L, "MEMBER_GROUP", "会员组");
	public static final ConditionsConstant MEMBER_GRADE = new ConditionsConstant(3L, "MEMBER_GRADE", "会员等级");
	public static final ConditionsConstant GIVE_COUPONS = new ConditionsConstant(4L, "GIVE_COUPONS", "送优惠券");
	public static final ConditionsConstant GIVE_POINTS = new ConditionsConstant(5L, "GIVE_POINTS", "送积分");
	public static final ConditionsConstant APPOINT_AMOUNT = new ConditionsConstant(6L, "APPOINT_AMOUNT", "指定消费额");
	public static final ConditionsConstant APPOINT_PRODUCT = new ConditionsConstant(7L, "APPOINT_PRODUCT", "指定产品");
	public static final ConditionsConstant APPOINT_PRODUCT_AMOUNT = new ConditionsConstant(11L, "APPOINT_PRODUCT_AMOUNT", "指定产品数量");
	public static final ConditionsConstant APPOINT_DATE = new ConditionsConstant(8L, "APPOINT_DATE", "指定日期");
	public static final ConditionsConstant WAKE_DATE = new ConditionsConstant(9L, "WAKE_DATE", "唤醒设置");
	public static final ConditionsConstant SEND_DATE = new ConditionsConstant(10L, "SEND_DATE", "发放时间");
	public static final ConditionsConstant APPOINT_PRODUCT_NUM = new ConditionsConstant(11L, "APPOINT_PRODUCT_NUM", "产品总数");
	public static final ConditionsConstant MEMBER_BIRTHDAY = new ConditionsConstant(12L, "MEMBER_BIRTHDAY", "会员生日");
	public static final ConditionsConstant CONSUMPTION_COUNT = new ConditionsConstant(13L, "CONSUMPTION_COUNT", "消费次数");
	public static final ConditionsConstant ORDER_STORE_CODE = new ConditionsConstant(14L, "ORDER_STORE_CODE", "订单门店编号");
	public static final ConditionsConstant ACTIVITY_INSTANCE_ID = new ConditionsConstant(15L, "ACTIVITY_INSTANCE_ID", "活动实例编号");
	public static final ConditionsConstant TOTAL_CONSUMPTION = new ConditionsConstant(16L, "TOTAL_CONSUMPTION", "累积消费金额");
	public static final ConditionsConstant MEMBER_PERFECT_INFO = new ConditionsConstant(17L, "MEMBER_PERFECT_INFO", "完善会员信息");
	public static final ConditionsConstant SEND_DATE_PARAM = new ConditionsConstant(18L, "SEND_DATE_PARAM", "时段名称");
	
	public static final ConditionsConstant ORDER_DATE = new ConditionsConstant(21L, "ORDER_DATE", "订单日期");
	public static final ConditionsConstant MEMBER_BRITHDAY = new ConditionsConstant(22L, "MEMBER_BRITHDAY", "会员生日");
	
	public static final ConditionsConstant GIVE_POINTS_MULTIPLE = new ConditionsConstant(30L, "GIVE_POINTS_MULTIPLE", "赠送积分的倍数");
	public static final ConditionsConstant GIVE_POINTS_MODE = new ConditionsConstant(31L, "GIVE_POINTS_MODE", "赠送积分方式");
	public static final ConditionsConstant COND_ORGS = new ConditionsConstant(32L, "COND_ORGS", "区域条件");
	public static final ConditionsConstant COND_WEEKS = new ConditionsConstant(33L, "COND_WEEKS", "周条件");
	
	public static final ConditionsConstant ORDER_DAY_COUNT = new ConditionsConstant(34L, "ORDER_DAY_COUNT", "每月消费天数");
	public static final ConditionsConstant ORDER_DAY_AMOUNT = new ConditionsConstant(35L, "ORDER_DAY_AMOUNT", "每天累积消费金额");
	
	
	public ConditionsConstant(Long id, String code, String name) {
		super(id, code, name);
	}

	@Override
	public Conditions getInstance() {
		Conditions conditions = new Conditions();
		conditions.setConditionsId(id);
		conditions.setConditionsCode(code);
		conditions.setConditionName(code);
		return conditions;
	}

}
