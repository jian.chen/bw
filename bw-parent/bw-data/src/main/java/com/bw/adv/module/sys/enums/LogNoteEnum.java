package com.bw.adv.module.sys.enums;

import org.apache.commons.lang.StringUtils;



public enum LogNoteEnum {
	
	BASE("1", "基础数据"),
	SYS("2", "系统管理"),
	MEMBER("3", "会员管理"),
	ORDER("4", "订单管理"),
	POINTS("5", "积分管理"),
	COUPON("6", "优惠券管理"),
	WECHAT("7", "微信管理"),
	ACTIVITY("8","活动管理"),
	COMPONENT("9","组件管理"),
	COMMON("10","公共模块");
	
	private String id;
	private String desc;
	
	private LogNoteEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static LogNoteEnum[] getArray() {
		return LogNoteEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			LogNoteEnum[] values = LogNoteEnum.values();
			for(LogNoteEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}
