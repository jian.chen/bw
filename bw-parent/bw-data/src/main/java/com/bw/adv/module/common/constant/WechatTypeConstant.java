/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatTypeConstant.java
 * Package Name:com.sage.scrm.module.common.constant
 * Date:2015年8月19日下午2:51:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.common.constant;
/**
 * ClassName:WechatTypeConstant <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月19日 下午2:51:04 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WechatTypeConstant{
	
	// 自动回复消息类型 
	public final static WechatTypeConstant AUTOREPLAY_TYPE_SUBSCRIBEREPLAY = new WechatTypeConstant(1,"1","subscribeAutoReplay");
	public final static WechatTypeConstant AUTOREPLAY_TYPE_AUTOREPLAY = new WechatTypeConstant(2,"2","AutoReplay");
	public final static WechatTypeConstant AUTOREPLAY_TYPE_KEYAUTOREPLAY = new WechatTypeConstant(3,"3","KeyReplay");
	
	// 是粉丝/不是粉丝 
	public final static WechatTypeConstant IS_WECHATFAN = new WechatTypeConstant(0,"Y","是");
	public final static WechatTypeConstant IS_NOT_WECHATFAN = new WechatTypeConstant(1,"N","否");
	
	// 关注/取消关注 与pd一致
	public final static WechatTypeConstant SUBSCRIBE = new WechatTypeConstant(0,"1","subscribe");
	public final static WechatTypeConstant UNSUBSCRIBE = new WechatTypeConstant(1,"2","unsubscribe");
	
	// 是否回复
	public final static WechatTypeConstant IS_REPLY = new WechatTypeConstant(0,"1","isReply");
	public final static WechatTypeConstant IS_NOT_REPLY = new WechatTypeConstant(1,"0","isNotReply");
	
	// 关键字匹配模式
	public final static WechatTypeConstant KEYWORD_MODE_CONTAIN = new WechatTypeConstant(0,"0","contain");
	public final static WechatTypeConstant KEYWORD_MODE_EQUAL = new WechatTypeConstant(1,"1","equal");
	
	// 是否全部回复模式
	public final static WechatTypeConstant IS_ALL_REPLY = new WechatTypeConstant(0,"0","all");
	public final static WechatTypeConstant IS_NOT_ALL_REPLY = new WechatTypeConstant(1,"1","single");
	
	// 是否是关键字
	public final static WechatTypeConstant IS_KEY_WORD = new WechatTypeConstant(0,"1","isKeyWord");
	public final static WechatTypeConstant IS_NOT_KEY_WORD = new WechatTypeConstant(1,"0","isNotKeyWord");
	
	// 是否已读 
	public final static WechatTypeConstant IS_READ = new WechatTypeConstant(0,"1","isKeyWord");
	public final static WechatTypeConstant IS_NOT_READ = new WechatTypeConstant(1,"0","isNotKeyWord");
	
	// 微信发送借口返回的状态码
	// 返回成功
	public final static WechatTypeConstant WECHAT_SUCESS_CODE = new WechatTypeConstant(0,"ok","success");
	public final static WechatTypeConstant WECHAT_FAILE_CODE = new WechatTypeConstant(1,"error","failure");
	
	// 是否删除 
	public final static WechatTypeConstant DELETED = new WechatTypeConstant(0,"0","delete");
	public final static WechatTypeConstant UNDELETED = new WechatTypeConstant(1,"1","unDelete");
	
	// 性别
	public final static WechatTypeConstant SEX_UNKNOW = new WechatTypeConstant(0,"0","未知");
	public final static WechatTypeConstant SEX_MALE = new WechatTypeConstant(1,"1","男");
	public final static WechatTypeConstant SEX_FEMALE = new WechatTypeConstant(2,"2","女");
	
	//消息类型 
	public final static WechatTypeConstant WECHAT_MESSAGE_TYPE_TEXT=new WechatTypeConstant(0,"1","text");
	public final static WechatTypeConstant WECHAT_MESSAGE_TYPE_IMAGE =new WechatTypeConstant(1,"2","image");
	public final static WechatTypeConstant WECHAT_MESSAGE_TYPE_VOICE =new WechatTypeConstant(2,"3","voice");
	public final static WechatTypeConstant WECHAT_MESSAGE_TYPE_VIDEO =new WechatTypeConstant(3,"4","video");
	public final static WechatTypeConstant WECHAT_MESSAGE_TYPE_MUSIC =new WechatTypeConstant(4,"5","music");
	public final static WechatTypeConstant WECHAT_MESSAGE_TYPE_NEWS =new WechatTypeConstant(5,"6","mpnews");
	
	private int id;
	private String code;
	private String message;
	
	
	public WechatTypeConstant() {
		super();
	}
	public WechatTypeConstant(int id, String code, String message) {
		super();
		this.id = id;
		this.code = code;
		this.message = message;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	/*public static void main(String[] args) {
		System.out.println(AUTOREPLAY_TYPE_SUBSCRIBEREPLAY.getMessage());
	}*/
}

