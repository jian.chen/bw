package com.bw.adv.module.component.msg.model;

public class SmsTemp {
    private Long smsTempId;

    private Long smsTempTypeId;
     
    private Long smsChannelId;
    
	private String smsTempName;

    private String content;

    private Long createBy;
    
    private Long updateBy;
    
    private String createTime;
    
    private String updateTime;

    private Long statusId;

    public Long getSmsTempId() {
        return smsTempId;
    }

    public void setSmsTempId(Long smsTempId) {
        this.smsTempId = smsTempId;
    }

    public Long getSmsTempTypeId() {
        return smsTempTypeId;
    }

    public void setSmsTempTypeId(Long smsTempTypeId) {
        this.smsTempTypeId = smsTempTypeId;
    }
    
    public Long getSmsChannelId() {
		return smsChannelId;
	}

	public void setSmsChannelId(Long smsChannelId) {
		this.smsChannelId = smsChannelId;
	}


    public String getSmsTempName() {
        return smsTempName;
    }

    public void setSmsTempName(String smsTempName) {
        this.smsTempName = smsTempName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}