package com.bw.adv.module.coupon.model;

public class CouponCategory {
    private Long couponCategoryId;

    private String couponCategoryName;

    private String couponCategoryDesc;

    private String couponCategoryImg1;

    private String couponCategoryImg2;

    private String couponCategoryImg3;

    private Long createBy;

    private Long updateBy;

    private String createTime;

    private String updateTime;

    private Long statusId;

    private Long sceneId;

    private String remark;

    private Integer seq;

    public Long getCouponCategoryId() {
        return couponCategoryId;
    }

    public void setCouponCategoryId(Long couponCategoryId) {
        this.couponCategoryId = couponCategoryId;
    }

    public String getCouponCategoryName() {
        return couponCategoryName;
    }

    public void setCouponCategoryName(String couponCategoryName) {
        this.couponCategoryName = couponCategoryName;
    }

    public String getCouponCategoryDesc() {
        return couponCategoryDesc;
    }

    public void setCouponCategoryDesc(String couponCategoryDesc) {
        this.couponCategoryDesc = couponCategoryDesc;
    }

    public String getCouponCategoryImg1() {
        return couponCategoryImg1;
    }

    public void setCouponCategoryImg1(String couponCategoryImg1) {
        this.couponCategoryImg1 = couponCategoryImg1;
    }

    public String getCouponCategoryImg2() {
        return couponCategoryImg2;
    }

    public void setCouponCategoryImg2(String couponCategoryImg2) {
        this.couponCategoryImg2 = couponCategoryImg2;
    }

    public String getCouponCategoryImg3() {
        return couponCategoryImg3;
    }

    public void setCouponCategoryImg3(String couponCategoryImg3) {
        this.couponCategoryImg3 = couponCategoryImg3;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getSceneId() {
        return sceneId;
    }

    public void setSceneId(Long sceneId) {
        this.sceneId = sceneId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

}