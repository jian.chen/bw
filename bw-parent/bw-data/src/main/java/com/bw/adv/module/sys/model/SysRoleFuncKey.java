package com.bw.adv.module.sys.model;

public class SysRoleFuncKey {

    private Long roleId;

    private Long functionId;

    private Long functionOperationId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public Long getFunctionOperationId() {
        return functionOperationId;
    }

    public void setFunctionOperationId(Long functionOperationId) {
        this.functionOperationId = functionOperationId;
    }

}