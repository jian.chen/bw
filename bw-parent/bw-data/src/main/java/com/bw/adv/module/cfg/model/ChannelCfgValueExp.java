package com.bw.adv.module.cfg.model;
/**
 * ClassName: ChannelCfgValue <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年11月14日 下午5:52:05 <br/>
 * scrmVersion 1.0
 * @author chenjian
 * @version jdk1.7
 */
public class ChannelCfgValueExp extends ChannelCfgValue {
	 private Long channelId;

	 public Long getChannelId() {
	        return channelId;
	    }

	    public void setChannelId(Long channelId) {
	        this.channelId = channelId;
	    }

}