package com.bw.adv.module.component.msg.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.component.msg.model.SmsTempType;

public class SmsTempTypeConstant extends AbstractConstant<SmsTempType> {

	public static final SmsTempTypeConstant TYPE_SYS = new SmsTempTypeConstant(1L, "系统功能");
	public static final SmsTempTypeConstant TYPE_ACTIVITY = new SmsTempTypeConstant(2L, "活动");
	
	
	public SmsTempTypeConstant(Long id, String name) {
		super(id, name);
	}

	@Override
	public SmsTempType getInstance() {
		SmsTempType smsTempType = new SmsTempType();
		smsTempType.setSmsTempTypeId(id);
		smsTempType.setSmsTempTypeName(code);
		return smsTempType;
	}

}
