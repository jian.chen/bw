package com.bw.adv.module.coupon.model;

public class CouponIssue {
    private Long couponIssueId;

    private Long couponId;

    private Long statusId;

    private Long orgId;

    private Long quantity;

    private String createDate;

    private String createBy;

    private String issueDate;

    private String isPredict;

    private String issueBy;
    
    private String createTime;

    public Long getCouponIssueId() {
        return couponIssueId;
    }

    public void setCouponIssueId(Long couponIssueId) {
        this.couponIssueId = couponIssueId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIsPredict() {
        return isPredict;
    }

    public void setIsPredict(String isPredict) {
        this.isPredict = isPredict;
    }

    public String getIssueBy() {
        return issueBy;
    }

    public void setIssueBy(String issueBy) {
        this.issueBy = issueBy;
    }

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}