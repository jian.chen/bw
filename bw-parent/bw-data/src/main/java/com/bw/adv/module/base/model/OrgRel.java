package com.bw.adv.module.base.model;

public class OrgRel {
    private Long orgRelId;

    private Long orgRelTypeId;

    private Long orgId;

    private Long toOrgId;

    public Long getOrgRelId() {
        return orgRelId;
    }

    public void setOrgRelId(Long orgRelId) {
        this.orgRelId = orgRelId;
    }

    public Long getOrgRelTypeId() {
        return orgRelTypeId;
    }

    public void setOrgRelTypeId(Long orgRelTypeId) {
        this.orgRelTypeId = orgRelTypeId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getToOrgId() {
        return toOrgId;
    }

    public void setToOrgId(Long toOrgId) {
        this.toOrgId = toOrgId;
    }
}