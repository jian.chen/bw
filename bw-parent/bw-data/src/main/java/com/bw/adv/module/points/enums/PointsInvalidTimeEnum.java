package com.bw.adv.module.points.enums;

import org.apache.commons.lang.StringUtils;



public enum PointsInvalidTimeEnum {
	
	FIXED_PERIOD("1", "滚动日期+固定日期"),
	FIXED("2", "固定日期"),
	FOREVER("3", "永久"),
	PERIOD("4", "滚动日期");
	
	private String id;
	private String desc;
	
	private PointsInvalidTimeEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static PointsInvalidTimeEnum[] getArray() {
		return PointsInvalidTimeEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			PointsInvalidTimeEnum[] values = PointsInvalidTimeEnum.values();
			for(PointsInvalidTimeEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}
}
