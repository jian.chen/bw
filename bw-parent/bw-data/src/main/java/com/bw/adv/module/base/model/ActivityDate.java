package com.bw.adv.module.base.model;

public class ActivityDate {
    private Long activityDateId;

    private String activityDateName;

    private String activityDateType;

    private String fromDate;

    private String thruDate;

    private String dateValue;

    private String createTime;

    private String upadeTime;

    private String isActive;

    private String activityDateDesc;

    private String remark;

    public Long getActivityDateId() {
        return activityDateId;
    }

    public void setActivityDateId(Long activityDateId) {
        this.activityDateId = activityDateId;
    }

    public String getActivityDateName() {
        return activityDateName;
    }

    public void setActivityDateName(String activityDateName) {
        this.activityDateName = activityDateName;
    }

    public String getActivityDateType() {
        return activityDateType;
    }

    public void setActivityDateType(String activityDateType) {
        this.activityDateType = activityDateType;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getThruDate() {
        return thruDate;
    }

    public void setThruDate(String thruDate) {
        this.thruDate = thruDate;
    }

    public String getDateValue() {
        return dateValue;
    }

    public void setDateValue(String dateValue) {
        this.dateValue = dateValue;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpadeTime() {
        return upadeTime;
    }

    public void setUpadeTime(String upadeTime) {
        this.upadeTime = upadeTime;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getActivityDateDesc() {
        return activityDateDesc;
    }

    public void setActivityDateDesc(String activityDateDesc) {
        this.activityDateDesc = activityDateDesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}