package com.bw.adv.module.sys.model;

public class SysRoleCubeMeasureKey {
    private Long roleId;

    private String cubeCode;

    private String measureCode;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getCubeCode() {
        return cubeCode;
    }

    public void setCubeCode(String cubeCode) {
        this.cubeCode = cubeCode;
    }

    public String getMeasureCode() {
        return measureCode;
    }

    public void setMeasureCode(String measureCode) {
        this.measureCode = measureCode;
    }
}