package com.bw.adv.module.member.model;

public class MemberPrism {
    private Long prismId;

    private String prismName;

    private String prismExplain;

    private Long selectType;

    private Long count;

    private String countTime;

    private String createTime;

    private String updateTime;

    private String ext1;

    private String ext2;

    public Long getPrismId() {
        return prismId;
    }

    public void setPrismId(Long prismId) {
        this.prismId = prismId;
    }

    public String getPrismName() {
        return prismName;
    }

    public void setPrismName(String prismName) {
        this.prismName = prismName;
    }

    public String getPrismExplain() {
        return prismExplain;
    }

    public void setPrismExplain(String prismExplain) {
        this.prismExplain = prismExplain;
    }

    public Long getSelectType() {
        return selectType;
    }

    public void setSelectType(Long selectType) {
        this.selectType = selectType;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getCountTime() {
        return countTime;
    }

    public void setCountTime(String countTime) {
        this.countTime = countTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2;
    }
}