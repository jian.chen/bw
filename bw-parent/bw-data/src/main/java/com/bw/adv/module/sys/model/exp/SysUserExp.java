/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:SysUserExp.java
 * Package Name:com.sage.scrm.module.sys.model.exp
 * Date:2015年8月25日下午7:07:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.sys.model.exp;

import com.bw.adv.module.sys.model.SysUser;

/**
 * ClassName:SysUserExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午7:07:06 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class SysUserExp extends SysUser {
	
	private static final long serialVersionUID = 3544605544851801834L;

	private Long roleId;
	
	private String selectRoleIds;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getSelectRoleIds() {
		return selectRoleIds;
	}

	public void setSelectRoleIds(String selectRoleIds) {
		this.selectRoleIds = selectRoleIds;
	}
	
}

