package com.bw.adv.module.sys.enums;

/**
 * Created by jeoy.zhou on 12/31/15.
 */
public enum SysFunctionOperationTypeEnum {

    DEFAULT(1, "默认功能"),
    BUTTON(2, "按钮功能");


    private Integer key;
    private String desc;

    private SysFunctionOperationTypeEnum(Integer key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public Integer getKey() {
        return this.key;
    }

    public String getDesc() {
        return this.desc;
    }
}
