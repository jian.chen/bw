package com.bw.adv.module.coupon.model;

public class CouponInstanceRelay {
    private Long couponInstanceRelayId;

    private Long couponInstanceId;

    private String pareOpenId;

    private String openId;

    private String createTime;

    private Integer seq;

    private String relayDesc;

    private Integer number;

    public Long getCouponInstanceRelayId() {
        return couponInstanceRelayId;
    }

    public void setCouponInstanceRelayId(Long couponInstanceRelayId) {
        this.couponInstanceRelayId = couponInstanceRelayId;
    }

    public Long getCouponInstanceId() {
        return couponInstanceId;
    }

    public void setCouponInstanceId(Long couponInstanceId) {
        this.couponInstanceId = couponInstanceId;
    }

    public String getPareOpenId() {
        return pareOpenId;
    }

    public void setPareOpenId(String pareOpenId) {
        this.pareOpenId = pareOpenId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getRelayDesc() {
        return relayDesc;
    }

    public void setRelayDesc(String relayDesc) {
        this.relayDesc = relayDesc;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}