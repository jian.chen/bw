package com.bw.adv.module.member.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.component.msg.model.SmsTempType;
import com.bw.adv.module.member.model.GradeFactors;

public class GradeFactorsConstant extends AbstractConstant<GradeFactors> {

	public static final GradeFactorsConstant POINTS_PERIOD = new GradeFactorsConstant(1L, "POINTS_PERIOD", "周期积分累计量");
	public static final GradeFactorsConstant ORDER_AMOUNT_PERIOD = new GradeFactorsConstant(2L, "POINTS_PERIOD", "周期订单金额累计量");
	
	
	public GradeFactorsConstant(Long id, String code,String name) {
		super(id, code,name);
	}

	@Override
	public GradeFactors getInstance() {
		GradeFactors factors = new GradeFactors();
		factors.setGradeFactorsId(id);
		factors.setGradeFactorsCode(code);
		factors.setGradeFactorsName(name);
		return factors;
	}
}