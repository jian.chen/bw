package com.bw.adv.module.component.msg.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

public class SmsChannel implements Serializable{

    private static final long serialVersionUID = -8058092657047991218L;

    private Long smsChannelId;

    private String smsChannelName;
    
    private String smsChannelCode;

    private String appId;

    private String appSecret;

    private String url;

    private String imgUrl;

    private String smsChannelDesc;

    private BigDecimal price;

    private String remark;

    private String isActive;

    public Long getSmsChannelId() {
        return smsChannelId;
    }

    public void setSmsChannelId(Long smsChannelId) {
        this.smsChannelId = smsChannelId;
    }

    public String getSmsChannelName() {
        return smsChannelName;
    }

    public void setSmsChannelName(String smsChannelName) {
        this.smsChannelName = smsChannelName;
    }
    
	public String getSmsChannelCode() {
		return smsChannelCode;
	}

	public void setSmsChannelCode(String smsChannelCode) {
		this.smsChannelCode = smsChannelCode;
	}

	public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSmsChannelDesc() {
        return smsChannelDesc;
    }

    public void setSmsChannelDesc(String smsChannelDesc) {
        this.smsChannelDesc = smsChannelDesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        if (StringUtils.isNotBlank(this.isActive) && "Y".equals(this.isActive)) {
            return "已启用";
        }
        return "未启用";
    }
}