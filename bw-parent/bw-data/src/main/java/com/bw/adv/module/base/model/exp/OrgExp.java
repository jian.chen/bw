/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:StoreExp.java
 * Package Name:com.sage.scrm.module.base.model.exp
 * Date:2015年9月3日下午3:37:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.model.exp;

import com.bw.adv.module.base.model.Org;


public class OrgExp extends Org {
	
	private String pareOrgName;

	public String getPareOrgName() {
		return pareOrgName;
	}

	public void setPareOrgName(String pareOrgName) {
		this.pareOrgName = pareOrgName;
	}
	


}

