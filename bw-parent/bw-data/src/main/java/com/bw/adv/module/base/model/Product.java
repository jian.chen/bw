package com.bw.adv.module.base.model;

import java.util.List;

import com.bw.adv.module.tools.Excel;

public class Product {
    private Long productId;

    private Long productSeriesId;

    @Excel(exportName = "产品编号", exportFieldWidth = 30, exportConvertSign = 0, importConvertSign = 0)
    private String productCode;
    
    @Excel(exportName = "产品名称", exportFieldWidth = 30, exportConvertSign = 0, importConvertSign = 0)
    private String productName;

    private String productDesc;

    private String createTime;

    private String updateTime;
    
    private Long statusId;
    
    private List<ProductCategoryItem> itemList;
    
	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getProductSeriesId() {
		return productSeriesId;
	}

	public void setProductSeriesId(Long productSeriesId) {
		this.productSeriesId = productSeriesId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public List<ProductCategoryItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<ProductCategoryItem> itemList) { 
		this.itemList = itemList;
	}

}