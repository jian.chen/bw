/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:MemberChannelSummaryDayExp.java
 * Package Name:com.sage.scrm.module.report.member.model.exp
 * Date:2015年11月27日下午2:49:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.report.member.model.exp;

import com.bw.adv.module.report.member.model.MemberChannelSummaryDay;
import com.bw.adv.module.tools.Excel;

/**
 * ClassName:MemberChannelSummaryDayExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月27日 下午2:49:29 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class MemberChannelSummaryDayExp extends MemberChannelSummaryDay {

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "渠道名称", importConvertSign = 0)
	String channelName; // 渠道名称
	
	int memNum; //会员总数
	
	String startDate; //统计时间筛选字段
	
	String endDate;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "新增非会员粉丝", importConvertSign = 0)
	private Integer dailyAddNotActivaeNumber;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "新增会员", importConvertSign = 0)
	private Integer dailyAddActivaeNumber;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "新增粉丝总数", importConvertSign = 0)
	private Integer dailyAddNumber;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "会员总数", importConvertSign = 0)
	private Integer totalActivaeNumber;

	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "非会员粉丝总数", importConvertSign = 0)
    private Integer totalNotActivaeNumber;
	
	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "粉丝总数", importConvertSign = 0)
	private Integer totalNumber;

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public int getMemNum() {
		return memNum;
	}

	public void setMemNum(int memNum) {
		this.memNum = memNum;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getDailyAddNotActivaeNumber() {
		return dailyAddNotActivaeNumber;
	}

	public void setDailyAddNotActivaeNumber(Integer dailyAddNotActivaeNumber) {
		this.dailyAddNotActivaeNumber = dailyAddNotActivaeNumber;
	}

	public Integer getDailyAddNumber() {
		return dailyAddNumber;
	}

	public void setDailyAddNumber(Integer dailyAddNumber) {
		this.dailyAddNumber = dailyAddNumber;
	}

	public Integer getTotalActivaeNumber() {
		return totalActivaeNumber;
	}

	public void setTotalActivaeNumber(Integer totalActivaeNumber) {
		this.totalActivaeNumber = totalActivaeNumber;
	}

	public Integer getTotalNotActivaeNumber() {
		return totalNotActivaeNumber;
	}

	public void setTotalNotActivaeNumber(Integer totalNotActivaeNumber) {
		this.totalNotActivaeNumber = totalNotActivaeNumber;
	}

	public Integer getDailyAddActivaeNumber() {
		return dailyAddActivaeNumber;
	}

	public void setDailyAddActivaeNumber(Integer dailyAddActivaeNumber) {
		this.dailyAddActivaeNumber = dailyAddActivaeNumber;
	}

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}
	
	
}

