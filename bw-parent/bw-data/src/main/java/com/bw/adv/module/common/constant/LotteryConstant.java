package com.bw.adv.module.common.constant;

public class LotteryConstant {
	// 奖品常量
	public final static LotteryConstant LOTTERY_TYPE_AIR_CLEANER = new LotteryConstant(1,"LOTTERY_TYPE_AIR_CLEANER","空气净化器");
	public final static LotteryConstant LOTTERY_TYPE_AIQIYI_MONTH_MEMBER = new LotteryConstant(2,"LOTTERY_TYPE_AIQIYI_MEMBER","爱奇艺月会员");
	public final static LotteryConstant LOTTERY_TYPE_YOUKU_MONTH_MEMBER = new LotteryConstant(3,"LOTTERY_TYPE_YOUKU_MONTH_MEMBER","优酷月会员");
	public final static LotteryConstant LOTTERY_TYPE_INTELLIGENT_CLEANER_ROBOT = new LotteryConstant(4,"LOTTERY_TYPE_INTELLIGENT_CLEANER_ROBOT","智能扫地机器人");
	public final static LotteryConstant LOTTERY_TYPE_MEITUAN_PRESS = new LotteryConstant(5,"LOTTERY_TYPE_MEITUAN_PRESS","美团抵用券");
	public final static LotteryConstant LOTTERY_TYPE_XIECHENG_COUPON = new LotteryConstant(6,"LOTTERY_TYPE_XIECHENG_COUPON","携程抵用券");
	public final static LotteryConstant LOTTERY_TYPE_JINGDONG_COUPON = new LotteryConstant(7,"LOTTERY_TYPE_JINGDONG_COUPON","京东抵用券");
	public final static LotteryConstant LOTTERY_TYPE_STARBUCK_COUPON = new LotteryConstant(8,"LOTTERY_TYPE_STARBUCK_COUPON","星巴克抵用券");
	public final static LotteryConstant LOTTERY_TYPE_FILM_COUPON = new LotteryConstant(9,"LOTTERY_TYPE_FILM_COUPON","电影票");
	public final static LotteryConstant LOTTERY_TYPE_GIFT_FIVE_HUNDR = new LotteryConstant(10,"LOTTERY_TYPE_GIFT_FIVE_HUNDR","500元礼品卡");
	public final static LotteryConstant LOTTERY_TYPE_GIFT_ONE_THOUSAND = new LotteryConstant(11,"LOTTERY_TYPE_GIFT_ONE_THOUSAND","1000元礼品卡");
	
	public final static LotteryConstant LOTTERY_TYPE_WHEAT_HEAD = new LotteryConstant(12,"LOTTERY_TYPE_WHEAT_HEAD","麦芽");
	public final static LotteryConstant LOTTERY_TYPE_YAKIMA = new LotteryConstant(13,"LOTTERY_TYPE_YAKIMA","雅基玛酒花");
	public final static LotteryConstant LOTTERY_TYPE_BEECH = new LotteryConstant(14,"LOTTERY_TYPE_BEECH","榉木");
	public final static LotteryConstant LOTTERY_TYPE_WATER = new LotteryConstant(15,"LOTTERY_TYPE_WATER","纯净水");
	
	public LotteryConstant(int id, String code, String message) {
		super();
		this.id = id;
		this.code = code;
		this.message = message;
	}
	
	private int id;
	private String code;
	private String message;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
