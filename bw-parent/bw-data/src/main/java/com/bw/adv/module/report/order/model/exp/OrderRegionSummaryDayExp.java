package com.bw.adv.module.report.order.model.exp;

import com.bw.adv.module.report.order.model.OrderRegionSummaryDay;
import com.bw.adv.module.tools.Excel;

public class OrderRegionSummaryDayExp extends OrderRegionSummaryDay {
	@Excel(exportConvertSign = 0, exportFieldWidth = 20, exportName = "区域名称", importConvertSign = 0)
	private String orgName;

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "营业额", importConvertSign = 0)
	private String withTotalSales;

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "会员营业额占比", importConvertSign = 0)
	private String scale1;

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "会员TC", importConvertSign = 0)
	private String withTotalConsumption;

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "会员TC占比", importConvertSign = 0)
	private String scale2;

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "消费人数", importConvertSign = 0)
	private String withTotalConsumer;

	@Excel(exportConvertSign = 0, exportFieldWidth = 10, exportName = "消费人数占比", importConvertSign = 0)
	private String scale3;

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getWithTotalSales() {
		return withTotalSales;
	}

	public void setWithTotalSales(String withTotalSales) {
		this.withTotalSales = withTotalSales;
	}

	public String getWithTotalConsumer() {
		return withTotalConsumer;
	}

	public void setWithTotalConsumer(String withTotalConsumer) {
		this.withTotalConsumer = withTotalConsumer;
	}

	public String getWithTotalConsumption() {
		return withTotalConsumption;
	}

	public void setWithTotalConsumption(String withTotalConsumption) {
		this.withTotalConsumption = withTotalConsumption;
	}

	public String getScale1() {
		return scale1;
	}

	public void setScale1(String scale1) {
		this.scale1 = scale1;
	}

	public String getScale2() {
		return scale2;
	}

	public void setScale2(String scale2) {
		this.scale2 = scale2;
	}

	public String getScale3() {
		return scale3;
	}

	public void setScale3(String scale3) {
		this.scale3 = scale3;
	}

}