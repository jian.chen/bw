package com.bw.adv.module.sys.model.exp;

import com.bw.adv.module.sys.model.SysRoleDataType;

import java.util.List;

/**
 * Created by jeoy.zhou on 1/13/16.
 */
public class RoleDataConfigExp extends SysRoleDataType{

    /**
     * 页面不勾选的属性
     * 查看角色功能权限时使用
     */
    private List<String> unDisplayFields;

    /**
     * 数据权限的配置项
     */
    private List<String> configFields;

    public RoleDataConfigExp(){}

    public RoleDataConfigExp(SysRoleDataType sysRoleDataType) {
        super.setDataTypeId(sysRoleDataType.getDataTypeId());
        super.setClassName(sysRoleDataType.getClassName());
        super.setDataTypeName(sysRoleDataType.getDataTypeName());
        super.setFieldName(sysRoleDataType.getFieldName());
    }

    public List<String> getUnDisplayFields() {
        return unDisplayFields;
    }

    public void setUnDisplayFields(List<String> unDisplayFields) {
        this.unDisplayFields = unDisplayFields;
    }

    public List<String> getConfigFields() {
        return configFields;
    }

    public void setConfigFields(List<String> configFields) {
        this.configFields = configFields;
    }
}
