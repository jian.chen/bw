package com.bw.adv.module.component.rule.model;

public class ConditionType {
    private Long conditionTypeId;

    private String conditionTypeCode;

    private String conditionTypeName;

    private String isActive;

    public Long getConditionTypeId() {
        return conditionTypeId;
    }

    public void setConditionTypeId(Long conditionTypeId) {
        this.conditionTypeId = conditionTypeId;
    }

    public String getConditionTypeCode() {
        return conditionTypeCode;
    }

    public void setConditionTypeCode(String conditionTypeCode) {
        this.conditionTypeCode = conditionTypeCode;
    }

    public String getConditionTypeName() {
        return conditionTypeName;
    }

    public void setConditionTypeName(String conditionTypeName) {
        this.conditionTypeName = conditionTypeName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}