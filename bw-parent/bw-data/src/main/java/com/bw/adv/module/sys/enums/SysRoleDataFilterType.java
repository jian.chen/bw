package com.bw.adv.module.sys.enums;

/**
 * Created by jeoy.zhou on 1/4/16.
 */
public enum SysRoleDataFilterType {

    NOT_DISPLAY(1, "过滤属性"),
    DISPLAY(2, "显示属性");


    private Integer key;
    private String desc;

    private SysRoleDataFilterType(Integer key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public Integer getKey() {
        return key;
    }

    public String getDesc() {
        return desc;
    }
}
