package com.bw.adv.module.member.model.charts;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "gexf")
public class Gexf {

	private Graph graph;

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}
	
}
