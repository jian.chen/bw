/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:ComTaskExp.java
 * Package Name:com.sage.scrm.module.component.job.model.exp
 * Date:2015年9月5日下午3:15:41
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.component.job.model.exp;

import com.bw.adv.module.component.job.model.ComTask;

/**
 * ClassName:ComTaskExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月5日 下午3:15:41 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class ComTaskExp extends ComTask {
	
	private String taskTypeName;
	
	private String taskTypeClass;


	public String getTaskTypeName() {
		return taskTypeName;
	}

	public void setTaskTypeName(String taskTypeName) {
		this.taskTypeName = taskTypeName;
	}

	public String getTaskTypeClass() {
		return taskTypeClass;
	}

	public void setTaskTypeClass(String taskTypeClass) {
		this.taskTypeClass = taskTypeClass;
	}
	
	

}

