package com.bw.adv.module.activity.model;

import java.math.BigDecimal;

public class AwardsSetting {
	/**
     * 
     */
    private Long awardsSettingId;

    /**
     * 
     */
    private Long awardsId;

    /**
     * 
     */
    private Long cycle;

    /**
     * 
     */
    private String cycleValue;

    /**
     * 抽奖人数限制
     */
    private Integer persions;

    /**
     * 当前库存
     */
    private Integer nowPresions;

    /**
     * 概率
     */
    private BigDecimal probability;

    /**
     * common 表示正常的奖项;lucky 表示谢谢参与奖
     */
    private String awardsType;

    /**
     * 
     * @return AWARDS_SETTING_ID 
     */
    public Long getAwardsSettingId() {
        return awardsSettingId;
    }

    /**
     * 
     * @param awardsSettingId 
     */
    public void setAwardsSettingId(Long awardsSettingId) {
        this.awardsSettingId = awardsSettingId;
    }

    /**
     * 
     * @return AWARDS_ID 
     */
    public Long getAwardsId() {
        return awardsId;
    }

    /**
     * 
     * @param awardsId 
     */
    public void setAwardsId(Long awardsId) {
        this.awardsId = awardsId;
    }

    /**
     * 
     * @return CYCLE 
     */
    public Long getCycle() {
        return cycle;
    }

    /**
     * 
     * @param cycle 
     */
    public void setCycle(Long cycle) {
        this.cycle = cycle;
    }

    /**
     * 
     * @return CYCLE_VALUE 
     */
    public String getCycleValue() {
        return cycleValue;
    }

    /**
     * 
     * @param cycleValue 
     */
    public void setCycleValue(String cycleValue) {
        this.cycleValue = cycleValue == null ? null : cycleValue.trim();
    }

    /**
     * 抽奖人数限制
     * @return PERSIONS 抽奖人数限制
     */
    public Integer getPersions() {
        return persions;
    }

    /**
     * 抽奖人数限制
     * @param persions 抽奖人数限制
     */
    public void setPersions(Integer persions) {
        this.persions = persions;
    }

    /**
     * 当前库存
     * @return NOW_PRESIONS 当前库存
     */
    public Integer getNowPresions() {
        return nowPresions;
    }

    /**
     * 当前库存
     * @param nowPresions 当前库存
     */
    public void setNowPresions(Integer nowPresions) {
        this.nowPresions = nowPresions;
    }

    /**
     * 概率
     * @return PROBABILITY 概率
     */
    public BigDecimal getProbability() {
        return probability;
    }

    /**
     * 概率
     * @param probability 概率
     */
    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    /**
     * common 表示正常的奖项;lucky 表示谢谢参与奖
     * @return AWARDS_TYPE common 表示正常的奖项;lucky 表示谢谢参与奖
     */
    public String getAwardsType() {
        return awardsType;
    }

    /**
     * common 表示正常的奖项;lucky 表示谢谢参与奖
     * @param awardsType common 表示正常的奖项;lucky 表示谢谢参与奖
     */
    public void setAwardsType(String awardsType) {
        this.awardsType = awardsType == null ? null : awardsType.trim();
    }
}