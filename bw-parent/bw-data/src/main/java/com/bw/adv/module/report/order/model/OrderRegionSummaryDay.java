package com.bw.adv.module.report.order.model;

import java.math.BigDecimal;

public class OrderRegionSummaryDay {
    private Long orderRegionSummaryDayId;

    private Long orgId;

    private Integer summaryYear;

    private Integer summaryMonth;

    private Integer summaryDay;

    private String summaryDate;

    private BigDecimal totalSales;

    private Integer totalConsumption;

    private Integer totalConsumer;

    private String createTime;

    public Long getOrderRegionSummaryDayId() {
        return orderRegionSummaryDayId;
    }

    public void setOrderRegionSummaryDayId(Long orderRegionSummaryDayId) {
        this.orderRegionSummaryDayId = orderRegionSummaryDayId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Integer getSummaryYear() {
        return summaryYear;
    }

    public void setSummaryYear(Integer summaryYear) {
        this.summaryYear = summaryYear;
    }

    public Integer getSummaryMonth() {
        return summaryMonth;
    }

    public void setSummaryMonth(Integer summaryMonth) {
        this.summaryMonth = summaryMonth;
    }

    public Integer getSummaryDay() {
        return summaryDay;
    }

    public void setSummaryDay(Integer summaryDay) {
        this.summaryDay = summaryDay;
    }

    public String getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = summaryDate;
    }

    public BigDecimal getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(BigDecimal totalSales) {
        this.totalSales = totalSales;
    }

    public Integer getTotalConsumption() {
        return totalConsumption;
    }

    public void setTotalConsumption(Integer totalConsumption) {
        this.totalConsumption = totalConsumption;
    }

    public Integer getTotalConsumer() {
        return totalConsumer;
    }

    public void setTotalConsumer(Integer totalConsumer) {
        this.totalConsumer = totalConsumer;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}