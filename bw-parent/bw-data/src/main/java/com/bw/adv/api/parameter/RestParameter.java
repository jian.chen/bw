package com.bw.adv.api.parameter;


import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertyFilter;

import com.bw.adv.core.utils.MD5Utils;


public class RestParameter {
	private static final int PARAMETER_TYPE_MAP = 1;
	private static final int PARAMETER_TYPE_COLLECTION = 2;
	private static final int PARAMETER_TYPE_ARRAY = 3;
	private static final int PARAMETER_TYPE_OTHER = 4;
	
	private Object obj;
	private String signKey;
	private int parameterType;

	public RestParameter(Object obj) {
		this.obj = obj;
		if (obj instanceof Map) {
			this.parameterType = PARAMETER_TYPE_MAP;
		} else if (obj instanceof Collection) {
			this.parameterType = PARAMETER_TYPE_COLLECTION;
		} else if (obj.getClass().isArray()) {
			this.parameterType = PARAMETER_TYPE_ARRAY;
		} else {
			this.parameterType = PARAMETER_TYPE_OTHER;
		}
	}
	
	public RestParameter(Object obj, String signKey) {
		this.obj = obj;
		this.signKey = signKey;
		if (obj instanceof Map) {
			this.parameterType = PARAMETER_TYPE_MAP;
		} else if (obj instanceof Collection) {
			this.parameterType = PARAMETER_TYPE_COLLECTION;
		} else if (obj.getClass().isArray()) {
			this.parameterType = PARAMETER_TYPE_ARRAY;
		} else {
			this.parameterType = PARAMETER_TYPE_OTHER;
		}
	}

	/**
	 * 获取key=value;key2=value2类型字符串
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getMatrixString() {
		if (!(obj instanceof Map)) {
			return "";
		}
		Map<String, Object> parameters = (Map<String, Object>) obj;
		if (parameters.values().size() == 0) {
			return "";
		}
		StringBuffer buffer = new StringBuffer();
		for (Map.Entry<String, Object> entry : parameters.entrySet()) {
			buffer.append(";" + entry.getKey() + "=" + entry.getValue());
		}
		return buffer.toString();
	}

	/**
	 * 获取对象json字符串,不包括为null属性=
	 * 
	 * @return json
	 */
	public String getJsonString() {
		switch(parameterType){
			case PARAMETER_TYPE_COLLECTION : 
			case PARAMETER_TYPE_ARRAY : {
				JSONArray ja = JSONArray.fromObject(obj);
				return ja.toString();
			}
			default : {
				JSONObject jo = JSONObject.fromObject(obj, getJsonConfig());
				return jo.toString();
			}
		}
	}

	/**
	 * 获取属性自然顺序排序后字符串
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getSortString() {
		Set<String> treeSet = new TreeSet<String>();
		Collection<Object>  parameters = null;
		switch(parameterType){
			case PARAMETER_TYPE_MAP : {
				parameters = ((Map<String, Object>) obj).values();
				break;
			}
			case PARAMETER_TYPE_COLLECTION : {
				parameters = (Collection<Object>) obj;
				break;
			}
			case PARAMETER_TYPE_ARRAY : {
				parameters = Arrays.asList((Object[]) obj);
				break;
			}
			default : {
				parameters = JSONObject.fromObject(obj, getJsonConfig()).values();
			}
		}
		for (Object obj : parameters) {
			treeSet.add(obj.toString());
		}
		StringBuffer buffer = new StringBuffer();
		for (Object obj : treeSet) {
			buffer.append(obj);
		}
		return buffer.toString();
	}

	/**
	 * 获取校验码
	 * 
	 * @return
	 */
	public String getSignString() {
//		return MD5Utils.getMD5String(getSortString() + signKey);
		return "ok";
	}

	private JsonConfig getJsonConfig() {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setJsonPropertyFilter(new PropertyFilter() {
			@Override
			public boolean apply(Object source, String name, Object value) {
				if (value == null) {
					return true;
				}
				return false;
			}
		});
		return jsonConfig;
	}
}
