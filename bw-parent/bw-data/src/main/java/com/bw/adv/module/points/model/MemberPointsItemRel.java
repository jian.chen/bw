package com.bw.adv.module.points.model;

import java.math.BigDecimal;

public class MemberPointsItemRel {
    private Long pointsItemRelId;

    private Long pointsItemInId;

    private Long pointsItemOutId;

    private BigDecimal itemPointsNumber;

    private String itemRelTime;

    private Integer seq;

    public Long getPointsItemRelId() {
        return pointsItemRelId;
    }

    public void setPointsItemRelId(Long pointsItemRelId) {
        this.pointsItemRelId = pointsItemRelId;
    }

    public Long getPointsItemInId() {
        return pointsItemInId;
    }

    public void setPointsItemInId(Long pointsItemInId) {
        this.pointsItemInId = pointsItemInId;
    }

    public Long getPointsItemOutId() {
        return pointsItemOutId;
    }

    public void setPointsItemOutId(Long pointsItemOutId) {
        this.pointsItemOutId = pointsItemOutId;
    }

    public BigDecimal getItemPointsNumber() {
        return itemPointsNumber;
    }

    public void setItemPointsNumber(BigDecimal itemPointsNumber) {
        this.itemPointsNumber = itemPointsNumber;
    }

    public String getItemRelTime() {
        return itemRelTime;
    }

    public void setItemRelTime(String itemRelTime) {
        this.itemRelTime = itemRelTime;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }
}