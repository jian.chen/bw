package com.bw.adv.module.wechat.model;

public class WechatMsgQueue {
    private Long wechatMsgQueueId;

    private Long memberId;

    private Integer businessType;

    private String msgKey;

    private String createTime;

    private String sendTime;

    private String msgContent;

    private String isSend;//Y表示已发送 N表示未发送 E表示发送异常
    
    private String resultMsg;

    public Long getWechatMsgQueueId() {
        return wechatMsgQueueId;
    }

    public void setWechatMsgQueueId(Long wechatMsgQueueId) {
        this.wechatMsgQueueId = wechatMsgQueueId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public String getMsgKey() {
        return msgKey;
    }

    public void setMsgKey(String msgKey) {
        this.msgKey = msgKey;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getIsSend() {
        return isSend;
    }

    public void setIsSend(String isSend) {
        this.isSend = isSend;
    }

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
}