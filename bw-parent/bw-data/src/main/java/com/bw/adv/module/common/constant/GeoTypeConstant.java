package com.bw.adv.module.common.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.base.model.GeoType;

public class GeoTypeConstant extends AbstractConstant<GeoType> {

	public static final GeoTypeConstant GROUP = new GeoTypeConstant(1L, "GROUP", "国家集团");
	public static final GeoTypeConstant COUNTRY = new GeoTypeConstant(2L, "COUNTRY", "国家");
	public static final GeoTypeConstant STATE = new GeoTypeConstant(3L, "STATE", "州");
	public static final GeoTypeConstant PROVINCE = new GeoTypeConstant(4L, "PROVINCE", "省");
	public static final GeoTypeConstant CITY = new GeoTypeConstant(5L, "CITY", "城市");
	public static final GeoTypeConstant COUNTY_CITY = new GeoTypeConstant(6L, "COUNTY_CITY", "县级市");
	public static final GeoTypeConstant REGION = new GeoTypeConstant(7L, "REGION", "行政区");
	public static final GeoTypeConstant MARKET_TERRITORY = new GeoTypeConstant(8L, "MARKET_TERRITORY", "商圈");
	
	
	public GeoTypeConstant(Long id, String code, String name) {
		super(id, code, name);
	}

	@Override
	public GeoType getInstance() {
		GeoType geoType = new GeoType();
		geoType.setGeoTypeId(id);
		geoType.setGeoTypeCode(code);
		geoType.setGeoTypeName(name);
		return geoType;
	}

}
