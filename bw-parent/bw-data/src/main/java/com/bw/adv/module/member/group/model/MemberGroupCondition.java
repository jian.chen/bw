package com.bw.adv.module.member.group.model;

public class MemberGroupCondition {
    private Long memberGroupConditionId;

    private Long memberGroupId;

    private Long distinctConditionId;

    private String distinctConditionRule;

    private String distinctConditionValue;

    public Long getMemberGroupConditionId() {
        return memberGroupConditionId;
    }

    public void setMemberGroupConditionId(Long memberGroupConditionId) {
        this.memberGroupConditionId = memberGroupConditionId;
    }

    public Long getMemberGroupId() {
        return memberGroupId;
    }

    public void setMemberGroupId(Long memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public Long getDistinctConditionId() {
        return distinctConditionId;
    }

    public void setDistinctConditionId(Long distinctConditionId) {
        this.distinctConditionId = distinctConditionId;
    }

    public String getDistinctConditionRule() {
        return distinctConditionRule;
    }

    public void setDistinctConditionRule(String distinctConditionRule) {
        this.distinctConditionRule = distinctConditionRule;
    }

    public String getDistinctConditionValue() {
        return distinctConditionValue;
    }

    public void setDistinctConditionValue(String distinctConditionValue) {
        this.distinctConditionValue = distinctConditionValue;
    }
}