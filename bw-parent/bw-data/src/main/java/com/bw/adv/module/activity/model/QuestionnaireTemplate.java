package com.bw.adv.module.activity.model;

public class QuestionnaireTemplate {
    private Long questionnaireTemplateId;

    private Long questionnaireId;

    private String title;

    private Long type;

    private Long selectNum;

    public Long getQuestionnaireTemplateId() {
        return questionnaireTemplateId;
    }

    public void setQuestionnaireTemplateId(Long questionnaireTemplateId) {
        this.questionnaireTemplateId = questionnaireTemplateId;
    }

    public Long getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Long questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getSelectNum() {
        return selectNum;
    }

    public void setSelectNum(Long selectNum) {
        this.selectNum = selectNum;
    }
}