package com.bw.adv.module.activity.model;

public class ActivityInstanceRecord {
    private Long activityInstanceRecordId;

    private Long activityInstanceId;

    private Long memberId;

    private String createTime;

    private String remark;

    public Long getActivityInstanceRecordId() {
        return activityInstanceRecordId;
    }

    public void setActivityInstanceRecordId(Long activityInstanceRecordId) {
        this.activityInstanceRecordId = activityInstanceRecordId;
    }

    public Long getActivityInstanceId() {
        return activityInstanceId;
    }

    public void setActivityInstanceId(Long activityInstanceId) {
        this.activityInstanceId = activityInstanceId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}