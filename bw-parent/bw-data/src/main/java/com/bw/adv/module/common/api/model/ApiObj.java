package com.bw.adv.module.common.api.model;

public class ApiObj {
    private Long apiObjId;

    private String apiObjName;

    private String apiObjCode;

    private String apiObjClass;

    public Long getApiObjId() {
        return apiObjId;
    }

    public void setApiObjId(Long apiObjId) {
        this.apiObjId = apiObjId;
    }

    public String getApiObjName() {
        return apiObjName;
    }

    public void setApiObjName(String apiObjName) {
        this.apiObjName = apiObjName;
    }

    public String getApiObjCode() {
        return apiObjCode;
    }

    public void setApiObjCode(String apiObjCode) {
        this.apiObjCode = apiObjCode;
    }

    public String getApiObjClass() {
        return apiObjClass;
    }

    public void setApiObjClass(String apiObjClass) {
        this.apiObjClass = apiObjClass;
    }
}