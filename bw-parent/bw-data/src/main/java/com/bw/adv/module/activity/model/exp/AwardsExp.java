/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:AwardsExp.java
 * Package Name:com.sage.scrm.module.activity.model.exp
 * Date:2016年1月19日上午11:05:46
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.model.exp;

import java.math.BigDecimal;

import com.bw.adv.module.activity.model.Awards;

/**
 * ClassName:AwardsExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月19日 上午11:05:46 <br/>
 * scrmVersion 1.0
 * @author   Tab.Wang
 * @version  jdk1.7
 * @see 	 
 */
public class AwardsExp extends Awards {

    private Long cycle;

    private String cycleValue;

    private Integer persions;

    private BigDecimal probability;

    private String prizeName;

    private String prizeEn;

    private Integer totalQuantity;

    private Integer surplusQuantity;
    
    private Integer prizeType;
    
    private String prizeCode;	//奖品编码
    
    private Long prizeId;

    private String awardsType;
    
    private String equalScore; // 积分
    

	public String getEqualScore() {
		return equalScore;
	}

	public void setEqualScore(String equalScore) {
		this.equalScore = equalScore;
	}

	public Long getPrizeId() {
		return prizeId;
	}

	public void setPrizeId(Long prizeId) {
		this.prizeId = prizeId;
	}

	public String getAwardsType() {
		return awardsType;
	}

	public void setAwardsType(String awardsType) {
		this.awardsType = awardsType;
	}

	public Long getCycle() {
		return cycle;
	}

	public void setCycle(Long cycle) {
		this.cycle = cycle;
	}

	public String getCycleValue() {
		return cycleValue;
	}

	public void setCycleValue(String cycleValue) {
		this.cycleValue = cycleValue;
	}

	public Integer getPersions() {
		return persions;
	}

	public void setPersions(Integer persions) {
		this.persions = persions;
	}

	public BigDecimal getProbability() {
		return probability;
	}

	public void setProbability(BigDecimal probability) {
		this.probability = probability;
	}

	public String getPrizeName() {
		return prizeName;
	}

	public void setPrizeName(String prizeName) {
		this.prizeName = prizeName;
	}

	public String getPrizeEn() {
		return prizeEn;
	}

	public void setPrizeEn(String prizeEn) {
		this.prizeEn = prizeEn;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Integer getSurplusQuantity() {
		return surplusQuantity;
	}

	public void setSurplusQuantity(Integer surplusQuantity) {
		this.surplusQuantity = surplusQuantity;
	}

	public Integer getPrizeType() {
		return prizeType;
	}

	public void setPrizeType(Integer prizeType) {
		this.prizeType = prizeType;
	}

	public String getPrizeCode() {
		return prizeCode;
	}

	public void setPrizeCode(String prizeCode) {
		this.prizeCode = prizeCode;
	}
    
}

