package com.bw.adv.module.component.rule.model;

public class ConditionsValue {
    private Long conditionsValueId;

    private Long conditionsId;

    private Long activityInstanceId;

    private String conditionsValues;

    private String cnComments;

    private String enComments;

    private String fromTime;

    private String thruTime;

    private String isActive;

    public Long getConditionsValueId() {
        return conditionsValueId;
    }

    public void setConditionsValueId(Long conditionsValueId) {
        this.conditionsValueId = conditionsValueId;
    }

    public Long getConditionsId() {
        return conditionsId;
    }

    public void setConditionsId(Long conditionsId) {
        this.conditionsId = conditionsId;
    }

    public Long getActivityInstanceId() {
        return activityInstanceId;
    }

    public void setActivityInstanceId(Long activityInstanceId) {
        this.activityInstanceId = activityInstanceId;
    }

    public String getConditionsValues() {
        return conditionsValues;
    }

    public void setConditionsValues(String conditionsValues) {
        this.conditionsValues = conditionsValues;
    }

    public String getCnComments() {
        return cnComments;
    }

    public void setCnComments(String cnComments) {
        this.cnComments = cnComments;
    }

    public String getEnComments() {
        return enComments;
    }

    public void setEnComments(String enComments) {
        this.enComments = enComments;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getThruTime() {
        return thruTime;
    }

    public void setThruTime(String thruTime) {
        this.thruTime = thruTime;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}