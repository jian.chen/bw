package com.bw.adv.module.common.model;

public class DistinctType {
    private Long distinctTypeId;

    private String distinctTypeName;

    private String remark;

    private String createTime;

    private String updateTime;

    public Long getDistinctTypeId() {
        return distinctTypeId;
    }

    public void setDistinctTypeId(Long distinctTypeId) {
        this.distinctTypeId = distinctTypeId;
    }

    public String getDistinctTypeName() {
        return distinctTypeName;
    }

    public void setDistinctTypeName(String distinctTypeName) {
        this.distinctTypeName = distinctTypeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}