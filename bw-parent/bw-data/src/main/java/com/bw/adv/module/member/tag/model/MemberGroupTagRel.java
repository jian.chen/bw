package com.bw.adv.module.member.tag.model;

public class MemberGroupTagRel {
    private Long memberGroupTagRelId;

    private Long memberTagId;

    private Long conditionId;

    private Long otherConditionId;

    private String relationValue;

    public Long getMemberGroupTagRelId() {
        return memberGroupTagRelId;
    }

    public void setMemberGroupTagRelId(Long memberGroupTagRelId) {
        this.memberGroupTagRelId = memberGroupTagRelId;
    }

    public Long getMemberTagId() {
        return memberTagId;
    }

    public void setMemberTagId(Long memberTagId) {
        this.memberTagId = memberTagId;
    }

    public Long getConditionId() {
        return conditionId;
    }

    public void setConditionId(Long conditionId) {
        this.conditionId = conditionId;
    }

    public Long getOtherConditionId() {
        return otherConditionId;
    }

    public void setOtherConditionId(Long otherConditionId) {
        this.otherConditionId = otherConditionId;
    }

    public String getRelationValue() {
        return relationValue;
    }

    public void setRelationValue(String relationValue) {
        this.relationValue = relationValue;
    }
}