package com.bw.adv.module.common.api.model;

public class ApiObjMapping {
    private Long apiObjMappingId;

    private Long apiObjId;

    private Long extSystemId;

    private String mappingWay;

    private String fields;

    public Long getApiObjMappingId() {
        return apiObjMappingId;
    }

    public void setApiObjMappingId(Long apiObjMappingId) {
        this.apiObjMappingId = apiObjMappingId;
    }

    public Long getApiObjId() {
        return apiObjId;
    }

    public void setApiObjId(Long apiObjId) {
        this.apiObjId = apiObjId;
    }

    public Long getExtSystemId() {
        return extSystemId;
    }

    public void setExtSystemId(Long extSystemId) {
        this.extSystemId = extSystemId;
    }

    public String getMappingWay() {
        return mappingWay;
    }

    public void setMappingWay(String mappingWay) {
        this.mappingWay = mappingWay;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }
}