/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:SmsTempExp.java
 * Package Name:com.sage.scrm.module.component.msg.model.exp
 * Date:2015年8月26日下午6:36:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.component.msg.model.exp;

import com.bw.adv.module.component.msg.model.SmsTemp;

/**
 * ClassName:SmsTempExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月26日 下午6:36:18 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class SmsTempExp extends SmsTemp {

	private String smsTempTypeName;
	
	private String sysUserName;

	private String smsChannelName;
	
	private String cn;
	
	public String getSmsTempTypeName() {
		return smsTempTypeName;
	}

	public void setSmsTempTypeName(String smsTempTypeName) {
		this.smsTempTypeName = smsTempTypeName;
	}

	public String getSysUserName() {
		return sysUserName;
	}

	public void setSysUserName(String sysUserName) {
		this.sysUserName = sysUserName;
	}

	public String getSmsChannelName() {
		return smsChannelName;
	}

	public void setSmsChannelName(String smsChannelName) {
		this.smsChannelName = smsChannelName;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}
	
	
	
}

