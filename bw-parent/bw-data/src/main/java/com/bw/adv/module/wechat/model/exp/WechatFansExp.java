/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:WechatFansExp.java
 * Package Name:com.sage.scrm.module.wechat.model.exp
 * Date:2015年9月1日下午2:30:00
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.wechat.model.exp;

import com.bw.adv.module.wechat.model.WechatFans;

/**
 * ClassName:WechatFansExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月1日 下午2:30:00 <br/>
 * scrmVersion 1.0
 * @author   june
 * @version  jdk1.7
 * @see 	 
 */
public class WechatFansExp extends WechatFans{
	
	private Long wechatFansGroupId;
	private String wechatFansGroupName;
	private Long wechatFansGroupItemId;

	public Long getWechatFansGroupItemId() {
		return wechatFansGroupItemId;
	}

	public void setWechatFansGroupItemId(Long wechatFansGroupItemId) {
		this.wechatFansGroupItemId = wechatFansGroupItemId;
	}

	public String getWechatFansGroupName() {
		return wechatFansGroupName;
	}

	public void setWechatFansGroupName(String wechatFansGroupName) {
		this.wechatFansGroupName = wechatFansGroupName;
	}

	public Long getWechatFansGroupId() {
		return wechatFansGroupId;
	}

	public void setWechatFansGroupId(Long wechatFansGroupId) {
		this.wechatFansGroupId = wechatFansGroupId;
	}
	
}

