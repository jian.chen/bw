package com.bw.adv.module.sys.model;

public class ResourcesType {
    private Long resourcesTypeId;

    private String resourcesTypeCode;

    private String resourcesTypeName;

    private String description;

    public Long getResourcesTypeId() {
        return resourcesTypeId;
    }

    public void setResourcesTypeId(Long resourcesTypeId) {
        this.resourcesTypeId = resourcesTypeId;
    }

    public String getResourcesTypeCode() {
        return resourcesTypeCode;
    }

    public void setResourcesTypeCode(String resourcesTypeCode) {
        this.resourcesTypeCode = resourcesTypeCode;
    }

    public String getResourcesTypeName() {
        return resourcesTypeName;
    }

    public void setResourcesTypeName(String resourcesTypeName) {
        this.resourcesTypeName = resourcesTypeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}