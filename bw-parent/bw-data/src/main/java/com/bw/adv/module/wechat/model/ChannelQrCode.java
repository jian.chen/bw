package com.bw.adv.module.wechat.model;

public class ChannelQrCode {
    private Long cqcChannelqrcodeid;

    private String cqcActionname;

    private Long cqcSceneid;

    private String cqcScenestr;

    private String cqcTicket;

    private Long cqcCreatedby;

    private String cqcCreateddate;

    private Long cqcUpdatedby;

    private String cqcUpdateddate;

    private String cqcDeleted;

    private String cqcStatus;

    private String cqcDesc;

    public Long getCqcChannelqrcodeid() {
        return cqcChannelqrcodeid;
    }

    public void setCqcChannelqrcodeid(Long cqcChannelqrcodeid) {
        this.cqcChannelqrcodeid = cqcChannelqrcodeid;
    }

    public String getCqcActionname() {
        return cqcActionname;
    }

    public void setCqcActionname(String cqcActionname) {
        this.cqcActionname = cqcActionname;
    }

    public Long getCqcSceneid() {
        return cqcSceneid;
    }

    public void setCqcSceneid(Long cqcSceneid) {
        this.cqcSceneid = cqcSceneid;
    }

    public String getCqcScenestr() {
        return cqcScenestr;
    }

    public void setCqcScenestr(String cqcScenestr) {
        this.cqcScenestr = cqcScenestr;
    }

    public String getCqcTicket() {
        return cqcTicket;
    }

    public void setCqcTicket(String cqcTicket) {
        this.cqcTicket = cqcTicket;
    }

    public Long getCqcCreatedby() {
        return cqcCreatedby;
    }

    public void setCqcCreatedby(Long cqcCreatedby) {
        this.cqcCreatedby = cqcCreatedby;
    }

    public String getCqcCreateddate() {
        return cqcCreateddate;
    }

    public void setCqcCreateddate(String cqcCreateddate) {
        this.cqcCreateddate = cqcCreateddate;
    }

    public Long getCqcUpdatedby() {
        return cqcUpdatedby;
    }

    public void setCqcUpdatedby(Long cqcUpdatedby) {
        this.cqcUpdatedby = cqcUpdatedby;
    }

    public String getCqcUpdateddate() {
        return cqcUpdateddate;
    }

    public void setCqcUpdateddate(String cqcUpdateddate) {
        this.cqcUpdateddate = cqcUpdateddate;
    }

    public String getCqcDeleted() {
        return cqcDeleted;
    }

    public void setCqcDeleted(String cqcDeleted) {
        this.cqcDeleted = cqcDeleted;
    }

    public String getCqcStatus() {
        return cqcStatus;
    }

    public void setCqcStatus(String cqcStatus) {
        this.cqcStatus = cqcStatus;
    }

    public String getCqcDesc() {
        return cqcDesc;
    }

    public void setCqcDesc(String cqcDesc) {
        this.cqcDesc = cqcDesc;
    }
}