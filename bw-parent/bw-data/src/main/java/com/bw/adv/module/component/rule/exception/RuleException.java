package com.bw.adv.module.component.rule.exception;


public class RuleException extends RuntimeException {

	private static final long serialVersionUID = 4203482434232111182L;

	public RuleException() {
		super();
	}

	public RuleException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public RuleException(String arg0) {
		super(arg0);
	}
}
