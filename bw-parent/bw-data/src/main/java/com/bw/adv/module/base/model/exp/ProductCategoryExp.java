/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-web
 * File Name:CouponData.java
 * Package Name:com.sage.scrm.bk.masterdate.domain
 * Date:2015-12-15上午10:48:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.base.model.exp;

import java.util.ArrayList;
import java.util.List;

import com.bw.adv.module.base.model.Product;
import com.bw.adv.module.base.model.ProductCategory;


/**
 * ClassName:优惠券类别明细扩展类 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-12-15 上午10:48:04 <br/>
 * scrmVersion 1.0
 * 
 * @author mennan
 * @version jdk1.7
 * @see
 */
public class ProductCategoryExp extends ProductCategory{
	
	private List<Product> productList = new ArrayList<Product>();

	public void addProduct(Product product){
		productList.add(product);
	}
	
	public List<Product> getProductList() {
		return productList;
	}

	
}
