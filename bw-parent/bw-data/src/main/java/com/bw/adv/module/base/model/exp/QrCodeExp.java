/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:QrCode.java
 * Package Name:com.sage.scrm.module.base.model.exp
 * Date:2015年11月17日下午4:51:00
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.base.model.exp;

import com.bw.adv.module.base.model.QrCode;

public class QrCodeExp extends QrCode{
	
	private String orgName;

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	
}

