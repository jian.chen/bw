package com.bw.adv.module.wechat.model;

public class WechatFansGroup {
	
	/**
	 * 微信粉丝组
	 */
    private Long wechatFansGroupId;
    
    /**
     * 微信帐号Id
     */
    private Long wechatAccountId;
    
    /**
     * 分组名称
     */
    private String groupName;
    
    /**
     * 分组ID
     */
    private String groupId;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
	private String createTime;
	
	/**
	 * 修改人
	 */
	private Long updateBy;
	
	/**
	 * 修改时间
	 */
    private String updatedTime;
    
    /**
     * 删除标识
     */
    private String isDelete;
    
    /**
     * 状态
     */
    private Long statusId;

    public Long getCreateBy() {
        return createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getGroupId() {
		return groupId;
	}

    public String getGroupName() {
        return groupName;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public Long getStatusId() {
        return statusId;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public Long getWechatFansGroupId() {
        return wechatFansGroupId;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public void setWechatFansGroupId(Long wechatFansGroupId) {
        this.wechatFansGroupId = wechatFansGroupId;
    }
}