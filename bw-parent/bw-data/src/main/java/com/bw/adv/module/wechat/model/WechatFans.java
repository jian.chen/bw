package com.bw.adv.module.wechat.model;

/**
 * 粉丝信息
 * ClassName: WechatFans <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月10日 上午10:41:24 <br/>
 * scrmVersion 1.0
 * @author june
 * @version jdk1.7
 */
public class WechatFans {
	
	/**
	 * 粉丝信息主键
	 */
    private Long wechatFansId;
    
    private Long qrCodeId;
    
    private Long channelId;
    
    /**
     * openId
     */
    private String openid;
    
    /**
     * 用户的unionid
     */
    private String unionid;
    
    /**
     * 昵称
     */
    private String nickName;
    
    /**
     * 性别
     */
    private String sex;
    
    /**
     * 城市
     */
    private String cty;
    
    /**
     * 省份
     */
    private String province;
    
    /**
     * 国家
     */
    private String country;
    
    /**
     * 语言
     */
    private String language;
    
    /**
     * 头像url
     */
    private String headImgUrl;
    
    /**
     *  备注
     */
    private String remark;
    /**
     * 创建人 
     */
    private Long createBy;

	/**
     * 创建时间
     */
    private String createTime;

	/**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateTime;
    
    /**
     * 是否删除
     */
    private String isDelete;
    
    /**
     * 状态码
     */
    private Long statusId;
    
    /**
     * 关注时间
     */
    private String attentionTime;
    
    /**
     * 取消时间 
     */
    private String cancelAttentionTime;
    
    /**
     * 是否粉丝
     */
    private String isFans;
    
    /**
     * 公众号ID
     */
    private Long wfnPublicid;
    
    /**
     * 微信公众号
     */
    private String wfnPublicaccount;
    
    /**
     * 扫描渠道
     */
    private Long wfnTargetqr;
    
    /**
     * 微信分组ID
     */
    private Long wechatAccountId;
    
    /**
     * 会员ID
     */
    private Long memberId;
    
    /**
     * 二维码关注渠道
     * */
    private String orgName;
    
    
    public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Long getQrCodeId() {
		return qrCodeId;
	}

	public void setQrCodeId(Long qrCodeId) {
		this.qrCodeId = qrCodeId;
	}

	public Long getChannelId() {
		return channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	public String getAttentionTime() {
        return attentionTime;
    }
    
    public String getCancelAttentionTime() {
        return cancelAttentionTime;
    }

    public String getCountry() {
        return country;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getCty() {
        return cty;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public String getIsFans() {
        return isFans;
    }

    public String getLanguage() {
        return language;
    }

    public Long getMemberId() {
        return memberId;
    }

    public String getNickName() {
        return nickName;
    }

    public String getOpenid() {
        return openid;
    }

    public String getProvince() {
        return province;
    }

    public String getRemark() {
		return remark;
	}

    public String getSex() {
        return sex;
    }

    public Long getStatusId() {
        return statusId;
    }

    public String getUnionid() {
        return unionid;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public Long getWechatFansId() {
        return wechatFansId;
    }

    public String getWfnPublicaccount() {
        return wfnPublicaccount;
    }

    public Long getWfnPublicid() {
        return wfnPublicid;
    }

    public Long getWfnTargetqr() {
        return wfnTargetqr;
    }

    public void setAttentionTime(String attentionTime) {
        this.attentionTime = attentionTime;
    }

    public void setCancelAttentionTime(String cancelAttentionTime) {
        this.cancelAttentionTime = cancelAttentionTime;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setCty(String cty) {
        this.cty = cty;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public void setIsFans(String isFans) {
        this.isFans = isFans;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setRemark(String remark) {
		this.remark = remark;
	}

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public void setWechatFansId(Long wechatFansId) {
        this.wechatFansId = wechatFansId;
    }

    public void setWfnPublicaccount(String wfnPublicaccount) {
        this.wfnPublicaccount = wfnPublicaccount;
    }

    public void setWfnPublicid(Long wfnPublicid) {
        this.wfnPublicid = wfnPublicid;
    }

    public void setWfnTargetqr(Long wfnTargetqr) {
        this.wfnTargetqr = wfnTargetqr;
    }
}