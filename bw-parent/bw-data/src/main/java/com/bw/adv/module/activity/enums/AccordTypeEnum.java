package com.bw.adv.module.activity.enums;




public enum AccordTypeEnum {
	
	NO_CONDITION(1, "无条件"),
	SHARE_CONDITION(2, "需分享");
	
	private int id;
	private String desc;
	
	private AccordTypeEnum(int id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public int getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static AccordTypeEnum[] getArray() {
		return AccordTypeEnum.values();
	}
	
	public static String getDesc(int id){
			AccordTypeEnum[] values = AccordTypeEnum.values();
			for(AccordTypeEnum logNote : values){
				if(logNote.getId() == id){
					return logNote.getDesc();
				}
			}
		return "";
	}
}
