/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-web
 * File Name:CouponData.java
 * Package Name:com.sage.scrm.bk.masterdate.domain
 * Date:2015-12-15上午10:48:04
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */

package com.bw.adv.module.base.model.exp;

import com.bw.adv.module.base.model.ProductCategoryItem;


/**
 * ClassName:优惠券类别明细扩展类 <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015-12-15 上午10:48:04 <br/>
 * scrmVersion 1.0
 * 
 * @author mennan
 * @version jdk1.7
 * @see
 */
public class ProductCategoryItemExp extends ProductCategoryItem{

	private String productCode;
	private String productCategoryCode;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductCategoryCode() {
		return productCategoryCode;
	}

	public void setProductCategoryCode(String productCategoryCode) {
		this.productCategoryCode = productCategoryCode;
	}

}
