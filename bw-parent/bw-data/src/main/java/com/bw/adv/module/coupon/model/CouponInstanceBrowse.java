package com.bw.adv.module.coupon.model;

public class CouponInstanceBrowse {
    private Long couponInstanceBrowseId;

    private Long couponInstanceId;

    private String browseTime;

    private String pareOpenId;

    private String openId;

    private String browseIp;

    private String closeTime;

    private Integer timeLength;

    public Long getCouponInstanceBrowseId() {
        return couponInstanceBrowseId;
    }

    public void setCouponInstanceBrowseId(Long couponInstanceBrowseId) {
        this.couponInstanceBrowseId = couponInstanceBrowseId;
    }

    public Long getCouponInstanceId() {
        return couponInstanceId;
    }

    public void setCouponInstanceId(Long couponInstanceId) {
        this.couponInstanceId = couponInstanceId;
    }

    public String getBrowseTime() {
        return browseTime;
    }

    public void setBrowseTime(String browseTime) {
        this.browseTime = browseTime;
    }

    public String getPareOpenId() {
        return pareOpenId;
    }

    public void setPareOpenId(String pareOpenId) {
        this.pareOpenId = pareOpenId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getBrowseIp() {
        return browseIp;
    }

    public void setBrowseIp(String browseIp) {
        this.browseIp = browseIp;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public Integer getTimeLength() {
        return timeLength;
    }

    public void setTimeLength(Integer timeLength) {
        this.timeLength = timeLength;
    }
}