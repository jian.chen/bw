package com.bw.adv.module.activity.model;

public class UserAndWinning {
	
	private Long winningItemId;

    private Long memberId;

    private Long awardsId;

    private Long prizeId;

    private Long prizeInstanceId;

    private String prizeName;

    private String itemDate;

    private String itemTime;

    private Long activityId;
    
    private Long userId;

    private String userName;

    private Long activityTypeId;

    private String activityTypeName;

    private String createTime;

    private Integer creayeBy;

    private String ext1;

    private String ext2;

    private String mobile;

	public Long getWinningItemId() {
		return winningItemId;
	}

	public void setWinningItemId(Long winningItemId) {
		this.winningItemId = winningItemId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getAwardsId() {
		return awardsId;
	}

	public void setAwardsId(Long awardsId) {
		this.awardsId = awardsId;
	}

	public Long getPrizeId() {
		return prizeId;
	}

	public void setPrizeId(Long prizeId) {
		this.prizeId = prizeId;
	}

	public Long getPrizeInstanceId() {
		return prizeInstanceId;
	}

	public void setPrizeInstanceId(Long prizeInstanceId) {
		this.prizeInstanceId = prizeInstanceId;
	}

	public String getPrizeName() {
		return prizeName;
	}

	public void setPrizeName(String prizeName) {
		this.prizeName = prizeName;
	}

	public String getItemDate() {
		return itemDate;
	}

	public void setItemDate(String itemDate) {
		this.itemDate = itemDate;
	}

	public String getItemTime() {
		return itemTime;
	}

	public void setItemTime(String itemTime) {
		this.itemTime = itemTime;
	}

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getActivityTypeId() {
		return activityTypeId;
	}

	public void setActivityTypeId(Long activityTypeId) {
		this.activityTypeId = activityTypeId;
	}

	public String getActivityTypeName() {
		return activityTypeName;
	}

	public void setActivityTypeName(String activityTypeName) {
		this.activityTypeName = activityTypeName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getCreayeBy() {
		return creayeBy;
	}

	public void setCreayeBy(Integer creayeBy) {
		this.creayeBy = creayeBy;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
    
}
