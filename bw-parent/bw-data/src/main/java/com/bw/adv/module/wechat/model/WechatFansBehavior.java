package com.bw.adv.module.wechat.model;

public class WechatFansBehavior {
	
	/**
	 * 粉丝行为ID
	 */
    private Long wechatFansBehaviorId;
    
    /**
     * 微信粉丝ID
     */
    private Long wechatFansId;
    
    /**
     * 微信账号ID
     */
    private Long wechatAccountId;
    
    /**
     * 类型  1:关注 2:取消关注
     */
    private String behaviorType;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
    private String createTime;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateTime;
    
    /**
     * 删除标识
     */
    private String isDelete;

    public Long getWechatFansBehaviorId() {
        return wechatFansBehaviorId;
    }

    public void setWechatFansBehaviorId(Long wechatFansBehaviorId) {
        this.wechatFansBehaviorId = wechatFansBehaviorId;
    }

    public Long getWechatFansId() {
        return wechatFansId;
    }

    public void setWechatFansId(Long wechatFansId) {
        this.wechatFansId = wechatFansId;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public String getBehaviorType() {
        return behaviorType;
    }

    public void setBehaviorType(String behaviorType) {
        this.behaviorType = behaviorType;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }
}