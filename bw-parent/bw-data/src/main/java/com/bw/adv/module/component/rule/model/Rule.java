package com.bw.adv.module.component.rule.model;

public class Rule {
    private Long ruleId;

    private String ruleName;

    private String ruleDesc;

    private String ruleMergeExp;

    private String isActive;

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleDesc() {
        return ruleDesc;
    }

    public void setRuleDesc(String ruleDesc) {
        this.ruleDesc = ruleDesc;
    }

    public String getRuleMergeExp() {
        return ruleMergeExp;
    }

    public void setRuleMergeExp(String ruleMergeExp) {
        this.ruleMergeExp = ruleMergeExp;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}