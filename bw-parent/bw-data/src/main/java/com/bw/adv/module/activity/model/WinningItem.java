package com.bw.adv.module.activity.model;

public class WinningItem {
    private Long winningItemId;

    private Long memberId;

    private Long awardsId;

    private Long prizeId;

    private Long prizeInstanceId;

    private String prizeName;

    private String itemDate;

    private String itemTime;

    private Long activityId;
    
    private Long memberAddressId;
    
    private Long sceneGameInstanceId;
    
    private String larIsscore;
    
    private String larRemark;

    public Long getMemberAddressId() {
		return memberAddressId;
	}

	public void setMemberAddressId(Long memberAddressId) {
		this.memberAddressId = memberAddressId;
	}

	public Long getWinningItemId() {
        return winningItemId;
    }

    public void setWinningItemId(Long winningItemId) {
        this.winningItemId = winningItemId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getAwardsId() {
        return awardsId;
    }

    public void setAwardsId(Long awardsId) {
        this.awardsId = awardsId;
    }

    public Long getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(Long prizeId) {
        this.prizeId = prizeId;
    }

    public Long getPrizeInstanceId() {
        return prizeInstanceId;
    }

    public void setPrizeInstanceId(Long prizeInstanceId) {
        this.prizeInstanceId = prizeInstanceId;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public String getItemDate() {
        return itemDate;
    }

    public void setItemDate(String itemDate) {
        this.itemDate = itemDate;
    }

    public String getItemTime() {
        return itemTime;
    }

    public void setItemTime(String itemTime) {
        this.itemTime = itemTime;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

	public Long getSceneGameInstanceId() {
		return sceneGameInstanceId;
	}

	public void setSceneGameInstanceId(Long sceneGameInstanceId) {
		this.sceneGameInstanceId = sceneGameInstanceId;
	}

	public String getLarIsscore() {
		return larIsscore;
	}

	public void setLarIsscore(String larIsscore) {
		this.larIsscore = larIsscore;
	}

	public String getLarRemark() {
		return larRemark;
	}

	public void setLarRemark(String larRemark) {
		this.larRemark = larRemark;
	}
}