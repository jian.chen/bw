/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:GradeTypeEnums.java
 * Package Name:com.sage.scrm.module.member.enums
 * Date:2016年7月25日下午7:31:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.enums;
/**
 * ClassName:GradeTypeEnums <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年7月25日 下午7:31:34 <br/>
 * scrmVersion standard
 * @author   dong.d
 * @version  jdk1.7
 * @see 	 
 */
public enum GoodsShowGradeTypeEnums {

	No_1(1L,"蓝卡","金卡及以上仅需XX积分"),
	No_2(2L,"金卡","蓝卡需XX积分"),
	No_3(3L,"铂金卡","蓝卡需XX积分"),
	No_1_En(1L,"Blue","More Gold and only XX points"),
	No_2_En(2L,"Gold","Blue need XX points"),
	No_3_En(3L,"Platinum","Blue need XX points"),
	;
	
	
	private Long code;
	private String cardName;
	private String desc;
	
	GoodsShowGradeTypeEnums(Long code , String cardName , String desc){
		this.code = code;
		this.cardName= cardName;
		this.desc = desc;
	}
	
	public Long getCode(){
		return code;
	}
	
	public String getCardName(){
		return cardName;
	}
	
	public String getDesc(){
		return desc;
	}
	
}

