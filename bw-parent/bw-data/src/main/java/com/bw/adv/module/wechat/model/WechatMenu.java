package com.bw.adv.module.wechat.model;

public class WechatMenu {
    private Long wechatMenuId;

    private Long wechatAccountId;

    private Long messageTemplateId;

    private String wechatMenuCode;

    private String wechatMenuName;

    private String msgType;//1 表示click 2表示view

    private String menuKey;

    private Integer sort;

    private String url;

    private Long pareWechatMenuId;

    private Long createBy;

    private String createTime;

    private Long updateBy;

    private String updateTime;

    private String isDelete;

    private Long statusId;

    public Long getWechatMenuId() {
        return wechatMenuId;
    }

    public void setWechatMenuId(Long wechatMenuId) {
        this.wechatMenuId = wechatMenuId;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public Long getMessageTemplateId() {
        return messageTemplateId;
    }

    public void setMessageTemplateId(Long messageTemplateId) {
        this.messageTemplateId = messageTemplateId;
    }

    public String getWechatMenuCode() {
        return wechatMenuCode;
    }

    public void setWechatMenuCode(String wechatMenuCode) {
        this.wechatMenuCode = wechatMenuCode;
    }

    public String getWechatMenuName() {
        return wechatMenuName;
    }

    public void setWechatMenuName(String wechatMenuName) {
        this.wechatMenuName = wechatMenuName;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMenuKey() {
        return menuKey;
    }

    public void setMenuKey(String menuKey) {
        this.menuKey = menuKey;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getPareWechatMenuId() {
        return pareWechatMenuId;
    }

    public void setPareWechatMenuId(Long pareWechatMenuId) {
        this.pareWechatMenuId = pareWechatMenuId;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}