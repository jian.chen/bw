package com.bw.adv.module.points.model.exp;

import com.bw.adv.module.points.model.MemberPointsItem;

/**
 * Created by jeoy.zhou on 6/13/16.
 */
public class MemberPointsItemStoreExp extends MemberPointsItem {

    private String storeName;

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
}
