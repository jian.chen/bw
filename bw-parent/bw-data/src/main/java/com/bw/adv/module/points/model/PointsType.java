package com.bw.adv.module.points.model;

public class PointsType {
    private Long pointsTypeId;

    private String pointsTypeCode;

    private String pointsTypeName;

    private String pointsTypeDesc;

    public Long getPointsTypeId() {
        return pointsTypeId;
    }

    public void setPointsTypeId(Long pointsTypeId) {
        this.pointsTypeId = pointsTypeId;
    }

    public String getPointsTypeCode() {
        return pointsTypeCode;
    }

    public void setPointsTypeCode(String pointsTypeCode) {
        this.pointsTypeCode = pointsTypeCode;
    }

    public String getPointsTypeName() {
        return pointsTypeName;
    }

    public void setPointsTypeName(String pointsTypeName) {
        this.pointsTypeName = pointsTypeName;
    }

    public String getPointsTypeDesc() {
        return pointsTypeDesc;
    }

    public void setPointsTypeDesc(String pointsTypeDesc) {
        this.pointsTypeDesc = pointsTypeDesc;
    }
}