package com.bw.adv.module.sys.model;

public class ComVerification {
    private Long comVerificationId;

    private Long channelId;

    private String mobile;

    private String verificationCode;

    private String verificationIp;

    private String comments;

    private String createTime;

    private String expireTime;

    private String updateTime;

    private String isPass;

    private String isActive;

    private String isDelete;

    private String isComplement;

    private Long languageTypeId;

    private String field1;

    private String field2;

    private String field3;

    private String field4;

    private String field5;

    public Long getComVerificationId() {
        return comVerificationId;
    }

    public void setComVerificationId(Long comVerificationId) {
        this.comVerificationId = comVerificationId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getVerificationIp() {
        return verificationIp;
    }

    public void setVerificationIp(String verificationIp) {
        this.verificationIp = verificationIp;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsPass() {
        return isPass;
    }

    public void setIsPass(String isPass) {
        this.isPass = isPass;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getIsComplement() {
        return isComplement;
    }

    public void setIsComplement(String isComplement) {
        this.isComplement = isComplement;
    }

    public Long getLanguageTypeId() {
        return languageTypeId;
    }

    public void setLanguageTypeId(Long languageTypeId) {
        this.languageTypeId = languageTypeId;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public String getField4() {
        return field4;
    }

    public void setField4(String field4) {
        this.field4 = field4;
    }

    public String getField5() {
        return field5;
    }

    public void setField5(String field5) {
        this.field5 = field5;
    }
}