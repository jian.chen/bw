/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:ActivityDateTypeEnum.java
 * Package Name:com.sage.scrm.module.activity.enums
 * Date:2015年9月21日下午2:53:33
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.activity.enums;

import org.apache.commons.lang.StringUtils;

/**
 * ClassName:ActivityDateTypeEnum <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年9月21日 下午2:53:33 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public enum ActivityDateTypeEnum {
	
	FIXED("1", "固定日期"),
	PERIOD("2", "时间段"),
	CYCLE("3", "滚动日期"),
	MON("1","MON"),
	TUE("2","TUE"),
	WED("3","WED"),
	THU("4","THU"),
	FRI("5","FRI"),
	SAT("6","SAT"),
	SUN("7","SUN"),
	YEAR("1","year"),
	MONTH("2","mouth"),
	WEEK("3","week");
	
	private String id;
	private String desc;
	
	private ActivityDateTypeEnum(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	 public String getId() { 
        return id; 
    } 
   
	public String getDesc() {
		return desc;
	}
	
	public static ActivityValidityEnum[] getArray() {
		return ActivityValidityEnum.values();
	}
	
	public static String getDesc(String id){
		if(StringUtils.isNotEmpty(id)){
			ActivityValidityEnum[] values = ActivityValidityEnum.values();
			for(ActivityValidityEnum logNote : values){
				if(logNote.getId().equals(id)){
					return logNote.getDesc();
				}
			}
		}
		return "";
	}

}

