package com.bw.adv.module.activity.model;

public class ActivityInstance {
	
    private Long activityInstanceId;

    private Long activityId;

    private String activityInstanceName;

    private String validityType;

    private String validityValue;

    private String templateUrl;

    private String templateType;

    private String createTime;

    private String updateTime;

    private Long createBy;

    private Long updateBy;

    private String fromDate;

    private String thruDate;

    private String breakTime;

    private String activityInstanceDesc;

    private String remark;

    private Long statusId;

    public Long getActivityInstanceId() {
        return activityInstanceId;
    }

    public void setActivityInstanceId(Long activityInstanceId) {
        this.activityInstanceId = activityInstanceId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityInstanceName() {
        return activityInstanceName;
    }

    public void setActivityInstanceName(String activityInstanceName) {
        this.activityInstanceName = activityInstanceName;
    }

    public String getValidityType() {
        return validityType;
    }

    public void setValidityType(String validityType) {
        this.validityType = validityType;
    }

    public String getValidityValue() {
        return validityValue;
    }

    public void setValidityValue(String validityValue) {
        this.validityValue = validityValue;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getThruDate() {
        return thruDate;
    }

    public void setThruDate(String thruDate) {
        this.thruDate = thruDate;
    }

    public String getBreakTime() {
        return breakTime;
    }

    public void setBreakTime(String breakTime) {
        this.breakTime = breakTime;
    }

    public String getActivityInstanceDesc() {
        return activityInstanceDesc;
    }

    public void setActivityInstanceDesc(String activityInstanceDesc) {
        this.activityInstanceDesc = activityInstanceDesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}