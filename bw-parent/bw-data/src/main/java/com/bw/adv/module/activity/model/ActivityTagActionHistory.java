package com.bw.adv.module.activity.model;

import java.math.BigDecimal;

public class ActivityTagActionHistory {
    private Long tagActionId;

    private Long memberId;

    private String businessCode;

    private String activityCode;

    private String happenTime;

    private String createTime;

    private String countTime;

    private String isCount;

    private BigDecimal countAmount;

    private String langx;

    private String externalId;

    private String ruleOptParams;

    public Long getTagActionId() {
        return tagActionId;
    }

    public void setTagActionId(Long tagActionId) {
        this.tagActionId = tagActionId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getHappenTime() {
        return happenTime;
    }

    public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCountTime() {
        return countTime;
    }

    public void setCountTime(String countTime) {
        this.countTime = countTime;
    }

    public String getIsCount() {
        return isCount;
    }

    public void setIsCount(String isCount) {
        this.isCount = isCount;
    }

    public BigDecimal getCountAmount() {
        return countAmount;
    }

    public void setCountAmount(BigDecimal countAmount) {
        this.countAmount = countAmount;
    }

    public String getLangx() {
        return langx;
    }

    public void setLangx(String langx) {
        this.langx = langx;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getRuleOptParams() {
        return ruleOptParams;
    }

    public void setRuleOptParams(String ruleOptParams) {
        this.ruleOptParams = ruleOptParams;
    }
}