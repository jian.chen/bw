package com.bw.adv.module.wechat.model.exp;

import com.bw.adv.module.wechat.model.AccessTokenCache;
/**
 * 
 * @author roy.zhang
 *
 */
public class AccessTokenCacheExp extends AccessTokenCache{
	private String timeCondition;

	public String getTimeCondition() {
		return timeCondition;
	}

	public void setTimeCondition(String timeCondition) {
		this.timeCondition = timeCondition;
	}
	
	
}
