package com.bw.adv.module.coupon.model;

public class CouponInstanceReceive {
    private Long couponInstanceReceiveId;

    private Long couponInstanceId;

    private Long couponInstanceSrc;

    private Long couponInstanceGet;

    private String relayTime;

    private String getTime;

    private String relayDesc;

    public Long getCouponInstanceReceiveId() {
        return couponInstanceReceiveId;
    }

    public void setCouponInstanceReceiveId(Long couponInstanceReceiveId) {
        this.couponInstanceReceiveId = couponInstanceReceiveId;
    }

    public Long getCouponInstanceId() {
        return couponInstanceId;
    }

    public void setCouponInstanceId(Long couponInstanceId) {
        this.couponInstanceId = couponInstanceId;
    }

    public Long getCouponInstanceSrc() {
        return couponInstanceSrc;
    }

    public void setCouponInstanceSrc(Long couponInstanceSrc) {
        this.couponInstanceSrc = couponInstanceSrc;
    }

    public Long getCouponInstanceGet() {
        return couponInstanceGet;
    }

    public void setCouponInstanceGet(Long couponInstanceGet) {
        this.couponInstanceGet = couponInstanceGet;
    }

    public String getRelayTime() {
        return relayTime;
    }

    public void setRelayTime(String relayTime) {
        this.relayTime = relayTime;
    }

    public String getGetTime() {
        return getTime;
    }

    public void setGetTime(String getTime) {
        this.getTime = getTime;
    }

    public String getRelayDesc() {
        return relayDesc;
    }

    public void setRelayDesc(String relayDesc) {
        this.relayDesc = relayDesc;
    }
}