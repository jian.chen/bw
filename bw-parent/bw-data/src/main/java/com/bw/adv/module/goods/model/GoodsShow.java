package com.bw.adv.module.goods.model;

import java.math.BigDecimal;

public class GoodsShow {
    private Long goodsShowId;

    private Long couponId;

    private Long goodsId;

    private Long goodsShowCategoryId;

    private String goodsShowName;

    private String goodsShowDesc;

    private BigDecimal pointsNumber;

    private String saleBeginDate;

    private String saleEndDate;

    private Long createBy;

    private Long updateBy;

    private String createTime;

    private String updateTime;

    private Long statusId;

    private String remark;

    private Long requireLevel;
    
    public Long getGoodsShowId() {
        return goodsShowId;
    }

    public void setGoodsShowId(Long goodsShowId) {
        this.goodsShowId = goodsShowId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getGoodsShowCategoryId() {
        return goodsShowCategoryId;
    }

    public void setGoodsShowCategoryId(Long goodsShowCategoryId) {
        this.goodsShowCategoryId = goodsShowCategoryId;
    }

    public String getGoodsShowName() {
        return goodsShowName;
    }

    public void setGoodsShowName(String goodsShowName) {
        this.goodsShowName = goodsShowName;
    }

    public String getGoodsShowDesc() {
        return goodsShowDesc;
    }

    public void setGoodsShowDesc(String goodsShowDesc) {
        this.goodsShowDesc = goodsShowDesc;
    }

    public BigDecimal getPointsNumber() {
        return pointsNumber;
    }

    public void setPointsNumber(BigDecimal pointsNumber) {
        this.pointsNumber = pointsNumber;
    }

    public String getSaleBeginDate() {
        return saleBeginDate;
    }

    public void setSaleBeginDate(String saleBeginDate) {
        this.saleBeginDate = saleBeginDate;
    }

    public String getSaleEndDate() {
        return saleEndDate;
    }

    public void setSaleEndDate(String saleEndDate) {
        this.saleEndDate = saleEndDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

	public Long getRequireLevel() {
		return requireLevel;
	}

	public void setRequireLevel(Long requireLevel) {
		this.requireLevel = requireLevel;
	}
}