package com.bw.adv.module.component.job.model;



import com.bw.adv.module.common.constant.StatusConstant;


public class TaskResult {
	//任务ID
	private Long taskId;
	//任务名称
	private String taskName;
	//任务状态
	private Long status;
	//任务消息
	private String message;
	//开始执行时间
	private Long startTime;
	//结束执行时间
	private Long endTime;
	
	public TaskResult(){
		this.taskId = Thread.currentThread().getId();
		this.status = StatusConstant.JOB_NEW.getId();
	}

	public Long getTaskId() {
		return taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public Long getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public Long getStartTime() {
		return startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	
	public void setSucceed(String message){
		this.status = StatusConstant.JOB_SUCCEED.getId();
		this.message = message;
	}
	
	public void setFaied(String message){
		this.status = StatusConstant.JOB_FAILED.getId();
		this.message = message;
	}
	
	public boolean isSucceed(){
		return this.status == StatusConstant.JOB_SUCCEED.getId();
	}
	
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	/**
	 * 任务执行时间
	 * @return 执行时间毫秒数
	 */
	public Long getExecTime() {
		return this.endTime - this.startTime;
	}
	
	
	
	

}
