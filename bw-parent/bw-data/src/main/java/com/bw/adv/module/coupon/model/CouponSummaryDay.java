package com.bw.adv.module.coupon.model;

public class CouponSummaryDay {
    private Long summaryId;

    private Integer summaryYear;

    private Integer summaryMonth;

    private Integer summaryDay;

    private String summaryDate;

    private Integer dayNewNumber;

    private Integer weekNewNumber;

    private Integer monthNewNumber;

    public Long getSummaryId() {
        return summaryId;
    }

    public void setSummaryId(Long summaryId) {
        this.summaryId = summaryId;
    }

    public Integer getSummaryYear() {
        return summaryYear;
    }

    public void setSummaryYear(Integer summaryYear) {
        this.summaryYear = summaryYear;
    }

    public Integer getSummaryMonth() {
        return summaryMonth;
    }

    public void setSummaryMonth(Integer summaryMonth) {
        this.summaryMonth = summaryMonth;
    }

    public Integer getSummaryDay() {
        return summaryDay;
    }

    public void setSummaryDay(Integer summaryDay) {
        this.summaryDay = summaryDay;
    }

    public String getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = summaryDate;
    }

    public Integer getDayNewNumber() {
        return dayNewNumber;
    }

    public void setDayNewNumber(Integer dayNewNumber) {
        this.dayNewNumber = dayNewNumber;
    }

    public Integer getWeekNewNumber() {
        return weekNewNumber;
    }

    public void setWeekNewNumber(Integer weekNewNumber) {
        this.weekNewNumber = weekNewNumber;
    }

    public Integer getMonthNewNumber() {
        return monthNewNumber;
    }

    public void setMonthNewNumber(Integer monthNewNumber) {
        this.monthNewNumber = monthNewNumber;
    }
}