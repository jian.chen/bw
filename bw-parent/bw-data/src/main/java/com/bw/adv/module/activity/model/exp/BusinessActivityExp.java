package com.bw.adv.module.activity.model.exp;

import com.bw.adv.module.activity.model.Business;

public class BusinessActivityExp extends Business{
	
    private Long businessActivityId;
    private Long businessId;
    private Long activityId;
    
    private String activityCode;
    
    public Long getBusinessActivityId() {
        return businessActivityId;
    }

    public void setBusinessActivityId(Long businessActivityId) {
        this.businessActivityId = businessActivityId;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

	public String getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}

}