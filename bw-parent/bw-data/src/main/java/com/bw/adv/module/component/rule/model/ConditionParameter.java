package com.bw.adv.module.component.rule.model;

public class ConditionParameter {
    private Long conditionParameterId;

    private Long conditionId;

    private Long dataTypeId;

    private String parameterCn;

    private String parameterName;

    private Integer groupNo;

    private Integer seq;

    public Long getConditionParameterId() {
        return conditionParameterId;
    }

    public void setConditionParameterId(Long conditionParameterId) {
        this.conditionParameterId = conditionParameterId;
    }

    public Long getConditionId() {
        return conditionId;
    }

    public void setConditionId(Long conditionId) {
        this.conditionId = conditionId;
    }

    public Long getDataTypeId() {
        return dataTypeId;
    }

    public void setDataTypeId(Long dataTypeId) {
        this.dataTypeId = dataTypeId;
    }

    public String getParameterCn() {
        return parameterCn;
    }

    public void setParameterCn(String parameterCn) {
        this.parameterCn = parameterCn;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public Integer getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(Integer groupNo) {
        this.groupNo = groupNo;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }
}