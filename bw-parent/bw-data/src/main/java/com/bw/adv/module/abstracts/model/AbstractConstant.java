package com.bw.adv.module.abstracts.model;


import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public abstract class AbstractConstant<T> {
	protected Long id;
	protected String code;
	protected String name;

	protected AbstractConstant(){}
	
	protected AbstractConstant(Long id, String name){
		this.id = id;
		this.name = name;
	}
	protected AbstractConstant(Long id, String code, String name){
		this.id = id;
		this.code = code;
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
	public abstract T getInstance();
	
	/**
	 * 
	 * getConstantByCode:(通过反射获取子类定义的常量). <br/>
	 * @author Jimmy
	 * @param clazz
	 * @param code
	 */
	@SuppressWarnings("unchecked")
	public static <V> V getConstantByCode(Class<? extends AbstractConstant<V>> clazz, String code){
		Field[] fields = clazz.getFields();
		int mod = 0;
		AbstractConstant<V> constant = null;
		for(Field field : fields){
			field.setAccessible(true);
			try {
				if(field.get(null) instanceof AbstractConstant){
					constant = (AbstractConstant<V>)field.get(null);
					mod = field.getModifiers();
					if(Modifier.isPublic(mod) && Modifier.isFinal(mod) && Modifier.isStatic(mod)){
						if(code.equals(constant.getCode())){
							return constant.getInstance(); 
						}
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public static <V> V getConstantById(Class<? extends AbstractConstant<V>> clazz, Long id){
		Field[] fields = clazz.getFields();
		int mod = 0;
		AbstractConstant<V> constant = null;
		for(Field field : fields){
			field.setAccessible(true);
			try {
				if(field.get(null) instanceof AbstractConstant){
					constant = (AbstractConstant<V>)field.get(null);
					mod = field.getModifiers();
					if(Modifier.isPublic(mod) && Modifier.isFinal(mod) && Modifier.isStatic(mod)){
						if(id.equals(constant.getId())){
							return constant.getInstance(); 
						}
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
}
