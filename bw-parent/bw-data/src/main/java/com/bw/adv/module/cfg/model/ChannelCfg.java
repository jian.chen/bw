package com.bw.adv.module.cfg.model;

public class ChannelCfg {
    private Long channelCfgId;

    private Long channelId;

    private String cfgCode;

    private String enable;

    private String channelInstanceName;

    private String comments;

    private String validityValue;

    private String fromDate;

    private String thruDate;

    private String validityType;

    private String createTime;

    private String updateTime;

    private Long createBy;

    private Long updateBy;

    private Long statusId;

    private String breakTime;

    public Long getChannelCfgId() {
        return channelCfgId;
    }

    public void setChannelCfgId(Long channelCfgId) {
        this.channelCfgId = channelCfgId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getCfgCode() {
        return cfgCode;
    }

    public void setCfgCode(String cfgCode) {
        this.cfgCode = cfgCode == null ? null : cfgCode.trim();
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable == null ? null : enable.trim();
    }

    public String getChannelInstanceName() {
        return channelInstanceName;
    }

    public void setChannelInstanceName(String channelInstanceName) {
        this.channelInstanceName = channelInstanceName == null ? null : channelInstanceName.trim();
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    public String getValidityValue() {
        return validityValue;
    }

    public void setValidityValue(String validityValue) {
        this.validityValue = validityValue == null ? null : validityValue.trim();
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate == null ? null : fromDate.trim();
    }

    public String getThruDate() {
        return thruDate;
    }

    public void setThruDate(String thruDate) {
        this.thruDate = thruDate == null ? null : thruDate.trim();
    }

    public String getValidityType() {
        return validityType;
    }

    public void setValidityType(String validityType) {
        this.validityType = validityType == null ? null : validityType.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getBreakTime() {
        return breakTime;
    }

    public void setBreakTime(String breakTime) {
        this.breakTime = breakTime == null ? null : breakTime.trim();
    }
}