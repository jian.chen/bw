package com.bw.adv.module.member.model;

public class MemberPrismCondition {
    private Long conditionId;

    private Long prismId;

    private Long conditionType;

    private String conditionCode;

    private String condtionValue;

    private String ext1;

    private String ext2;

    public Long getConditionId() {
        return conditionId;
    }

    public void setConditionId(Long conditionId) {
        this.conditionId = conditionId;
    }

    public Long getPrismId() {
        return prismId;
    }

    public void setPrismId(Long prismId) {
        this.prismId = prismId;
    }

    public Long getConditionType() {
        return conditionType;
    }

    public void setConditionType(Long conditionType) {
        this.conditionType = conditionType;
    }

    public String getConditionCode() {
        return conditionCode;
    }

    public void setConditionCode(String conditionCode) {
        this.conditionCode = conditionCode;
    }

    public String getCondtionValue() {
        return condtionValue;
    }

    public void setCondtionValue(String condtionValue) {
        this.condtionValue = condtionValue;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1;
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2;
    }
}