/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-data
 * File Name:OrderHeaderExp.java
 * Package Name:com.sage.scrm.module.order.model.exp
 * Date:2015年8月31日下午2:42:36
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.order.model.exp;

import java.math.BigDecimal;

import com.bw.adv.module.order.model.OrderHeader;

/**
 * ClassName:OrderHeaderExp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月31日 下午2:42:36 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class OrderHeaderExp extends OrderHeader {
	
	private String memberCode;
	
	//订单日期筛选
	private String orderDateStart;
	//订单日期筛选
	private String orderDateEnd;
	//订单金额筛选
	private String orderAmountStart;
	//订单金额筛选
	private String orderAmountEnd;
	//订单金额筛选
	private String payAmountStart;
	//订单金额筛选
	private String payAmountEnd;
	//消费门店名称
	private String storeName;
	//订单渠道名称
	private String channelName;
	//使用优惠券名称
	private String couponName;
	//获得优惠券名称
	private String usedCoupon;
	//获得的积分数量
	private BigDecimal itemPointsNumber;
	//订单手机筛选
	private String mobile;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOrderDateStart() {
		return orderDateStart;
	}

	public void setOrderDateStart(String orderDateStart) {
		this.orderDateStart = orderDateStart;
	}

	public String getOrderDateEnd() {
		return orderDateEnd;
	}

	public void setOrderDateEnd(String orderDateEnd) {
		this.orderDateEnd = orderDateEnd;
	}

	public String getOrderAmountStart() {
		return orderAmountStart;
	}

	public void setOrderAmountStart(String orderAmountStart) {
		this.orderAmountStart = orderAmountStart;
	}

	public String getOrderAmountEnd() {
		return orderAmountEnd;
	}

	public void setOrderAmountEnd(String orderAmountEnd) {
		this.orderAmountEnd = orderAmountEnd;
	}

	public String getPayAmountStart() {
		return payAmountStart;
	}

	public void setPayAmountStart(String payAmountStart) {
		this.payAmountStart = payAmountStart;
	}

	public String getPayAmountEnd() {
		return payAmountEnd;
	}

	public void setPayAmountEnd(String payAmountEnd) {
		this.payAmountEnd = payAmountEnd;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getUsedCoupon() {
		return usedCoupon;
	}

	public void setUsedCoupon(String usedCoupon) {
		this.usedCoupon = usedCoupon;
	}

	public BigDecimal getItemPointsNumber() {
		return itemPointsNumber;
	}

	public void setItemPointsNumber(BigDecimal itemPointsNumber) {
		this.itemPointsNumber = itemPointsNumber;
	}
	
	

}

