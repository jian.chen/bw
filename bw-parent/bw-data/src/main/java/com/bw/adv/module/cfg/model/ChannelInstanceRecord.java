package com.bw.adv.module.cfg.model;

public class ChannelInstanceRecord {
    private Long channelInstanceRecordId;

    private Long channelCfgId;

    private Long memberId;

    private String createTime;

    private String remark;

    private String levels;

    private String points;

    private String coupons;

    public Long getChannelInstanceRecordId() {
        return channelInstanceRecordId;
    }

    public void setChannelInstanceRecordId(Long channelInstanceRecordId) {
        this.channelInstanceRecordId = channelInstanceRecordId;
    }

    public Long getChannelCfgId() {
        return channelCfgId;
    }

    public void setChannelCfgId(Long channelCfgId) {
        this.channelCfgId = channelCfgId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getLevels() {
        return levels;
    }

    public void setLevels(String levels) {
        this.levels = levels == null ? null : levels.trim();
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points == null ? null : points.trim();
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons == null ? null : coupons.trim();
    }
}