package com.bw.adv.module.points.constant;

import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.points.model.PointsType;

public class PointsTypeConstant extends AbstractConstant<PointsType>{
	
	public static final PointsTypeConstant POINTS_TYPE_ORDER = new PointsTypeConstant(1L,"POINTS_TYPE_ORDER", "消费积分");
	public static final PointsTypeConstant POINTS_TYPE_ACTIVITY = new PointsTypeConstant(2L,"POINTS_TYPE_ACTIVITY", "活动积分");
	public static final PointsTypeConstant POINTS_EXCHANGE = new PointsTypeConstant(3L,"POINTS_EXCHANGE", "积分兑换");
	public static final PointsTypeConstant POINTS_LOTTERY = new PointsTypeConstant(4L,"POINTS_LOTTERY", "每日签到");
	
	public PointsTypeConstant(Long id,String code, String name) {
		super(id, code,name);
	}

	@Override
	public PointsType getInstance() {
		PointsType pointsType = new PointsType();
		pointsType.setPointsTypeCode(code);
		pointsType.setPointsTypeId(id);
		pointsType.setPointsTypeName(name);
		return pointsType;
	}

	
}