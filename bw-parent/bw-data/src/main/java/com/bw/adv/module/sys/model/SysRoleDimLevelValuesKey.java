package com.bw.adv.module.sys.model;

public class SysRoleDimLevelValuesKey {
    private Long roleId;

    private String dimCode;

    private Long dimValue;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getDimCode() {
        return dimCode;
    }

    public void setDimCode(String dimCode) {
        this.dimCode = dimCode;
    }

    public Long getDimValue() {
        return dimValue;
    }

    public void setDimValue(Long dimValue) {
        this.dimValue = dimValue;
    }
}