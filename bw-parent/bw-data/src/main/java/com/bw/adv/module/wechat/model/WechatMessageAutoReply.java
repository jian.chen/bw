package com.bw.adv.module.wechat.model;

public class WechatMessageAutoReply {
	
	/**
	 * 编号
	 */
    private Long wechatMessageAutoReplyId;
    
    /**
     * 微信帐号
     */
    private Long wechatAccountId;

	/**
     * 消息模板ID
     */
    private Long wechatMessageTemplateId;

	/**
     * 自动回复类型
     */
    private String replytype;
    
    /**
     * 创建人
     */
    private Long createBy;
    
    /**
     * 创建时间
     */
    private String createDate;
    
    /**
     * 修改人
     */
    private Long updateBy;
    
    /**
     * 修改时间
     */
    private String updateDate;
    
    /**
     * 删除标识
     */
    private String isDeleted;
    
    public Long getCreateBy() {
        return createBy;
    }
    
    public String getCreateDate() {
        return createDate;
    }
    
    
    public String getIsDeleted() {
        return isDeleted;
    }

	public String getReplytype() {
        return replytype;
    }

	public Long getUpdateBy() {
        return updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public Long getWechatAccountId() {
        return wechatAccountId;
    }

    public Long getWechatMessageAutoReplyId() {
		return wechatMessageAutoReplyId;
	}

    public Long getWechatMessageTemplateId() {
        return wechatMessageTemplateId;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public void setReplytype(String replytype) {
        this.replytype = replytype;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public void setWechatAccountId(Long wechatAccountId) {
        this.wechatAccountId = wechatAccountId;
    }

    public void setWechatMessageAutoReplyId(Long wechatMessageAutoReplyId) {
		this.wechatMessageAutoReplyId = wechatMessageAutoReplyId;
	}

    public void setWechatMessageTemplateId(Long wechatMessageTemplateId) {
        this.wechatMessageTemplateId = wechatMessageTemplateId;
    }
    
    
}