package com.bw.adv.module.member.model;

public class ExtAccountBinding {
    private Long bindingId;

    private Long extAccountTypeId;

    private Long memberId;

    private String bindingAccount;

    private String bindingDate;

    private String unbindingDate;

    private String accessAccredit;

    private String loginAccredit;

    private Long statusId;

    public Long getBindingId() {
        return bindingId;
    }

    public void setBindingId(Long bindingId) {
        this.bindingId = bindingId;
    }

    public Long getExtAccountTypeId() {
        return extAccountTypeId;
    }

    public void setExtAccountTypeId(Long extAccountTypeId) {
        this.extAccountTypeId = extAccountTypeId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getBindingAccount() {
        return bindingAccount;
    }

    public void setBindingAccount(String bindingAccount) {
        this.bindingAccount = bindingAccount;
    }

    public String getBindingDate() {
        return bindingDate;
    }

    public void setBindingDate(String bindingDate) {
        this.bindingDate = bindingDate;
    }

    public String getUnbindingDate() {
        return unbindingDate;
    }

    public void setUnbindingDate(String unbindingDate) {
        this.unbindingDate = unbindingDate;
    }

    public String getAccessAccredit() {
        return accessAccredit;
    }

    public void setAccessAccredit(String accessAccredit) {
        this.accessAccredit = accessAccredit;
    }

    public String getLoginAccredit() {
        return loginAccredit;
    }

    public void setLoginAccredit(String loginAccredit) {
        this.loginAccredit = loginAccredit;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}