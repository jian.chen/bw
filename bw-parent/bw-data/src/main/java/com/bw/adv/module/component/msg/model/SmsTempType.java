package com.bw.adv.module.component.msg.model;

public class SmsTempType {
    private Long smsTempTypeId;

    private String smsTempTypeName;

    private String updateTime;

    private String createTime;

    private Long statusId;

    public Long getSmsTempTypeId() {
        return smsTempTypeId;
    }

    public void setSmsTempTypeId(Long smsTempTypeId) {
        this.smsTempTypeId = smsTempTypeId;
    }

    public String getSmsTempTypeName() {
        return smsTempTypeName;
    }

    public void setSmsTempTypeName(String smsTempTypeName) {
        this.smsTempTypeName = smsTempTypeName;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
}