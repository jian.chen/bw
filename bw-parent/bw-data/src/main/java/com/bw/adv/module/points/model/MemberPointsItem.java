package com.bw.adv.module.points.model;

import java.math.BigDecimal;

public class MemberPointsItem {
    private Long memberPointsItemId;

    private Long parentItemId;
    
    private Long memberId;

    private Long pointsRuleId;

    private Long activityInstanceId;

    private BigDecimal itemPointsNumber;

    private BigDecimal availablePointsNumber;

    private BigDecimal expirePointsNumber;

    private String happenTime;
    
    private Long createBy;

    private String createTime;
    
    private Long updateBy;
    
    private Long pointsTypeId;

    public Long getPointsTypeId() {
		return pointsTypeId;
	}

	public void setPointsTypeId(Long pointsTypeId) {
		this.pointsTypeId = pointsTypeId;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public Long getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Long updateBy) {
		this.updateBy = updateBy;
	}

	private String updateTime;

    private String invalidDate;

    private BigDecimal pointsBalance;

    private String pointsReason;

    private Long statusId;

    private Long orderId;

    private String externalId;

    public Long getMemberPointsItemId() {
        return memberPointsItemId;
    }

    public void setMemberPointsItemId(Long memberPointsItemId) {
        this.memberPointsItemId = memberPointsItemId;
    }

    public Long getParentItemId() {
		return parentItemId;
	}

	public void setParentItemId(Long parentItemId) {
		this.parentItemId = parentItemId;
	}

	public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getPointsRuleId() {
        return pointsRuleId;
    }

    public void setPointsRuleId(Long pointsRuleId) {
        this.pointsRuleId = pointsRuleId;
    }

    public Long getActivityInstanceId() {
        return activityInstanceId;
    }

    public void setActivityInstanceId(Long activityInstanceId) {
        this.activityInstanceId = activityInstanceId;
    }

    public BigDecimal getItemPointsNumber() {
        return itemPointsNumber;
    }

    public void setItemPointsNumber(BigDecimal itemPointsNumber) {
        this.itemPointsNumber = itemPointsNumber;
    }

    public BigDecimal getAvailablePointsNumber() {
        return availablePointsNumber;
    }

    public void setAvailablePointsNumber(BigDecimal availablePointsNumber) {
        this.availablePointsNumber = availablePointsNumber;
    }

    public BigDecimal getExpirePointsNumber() {
		return expirePointsNumber;
	}

	public void setExpirePointsNumber(BigDecimal expirePointsNumber) {
		this.expirePointsNumber = expirePointsNumber;
	}

	public String getHappenTime() {
        return happenTime;
    }

    public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getInvalidDate() {
        return invalidDate;
    }

    public void setInvalidDate(String invalidDate) {
        this.invalidDate = invalidDate;
    }

    public BigDecimal getPointsBalance() {
        return pointsBalance;
    }

    public void setPointsBalance(BigDecimal pointsBalance) {
        this.pointsBalance = pointsBalance;
    }

    public String getPointsReason() {
        return pointsReason;
    }

    public void setPointsReason(String pointsReason) {
        this.pointsReason = pointsReason;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}