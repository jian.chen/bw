package com.bw.adv.module.common.constant;


import com.bw.adv.module.abstracts.model.AbstractConstant;
import com.bw.adv.module.base.model.Status;

public class StatusConstant extends AbstractConstant<Status> {
	
	//任务状态
	public final static StatusConstant JOB_NEW = new StatusConstant(1001L, "JOB_NEW", "未执行");
	public final static StatusConstant JOB_RUN = new StatusConstant(1002L, "JOB_RUN", "执行中");
	public final static StatusConstant JOB_SUCCEED = new StatusConstant(1003L, "JOB_SUCCEED", "执行成功");
	public final static StatusConstant JOB_FAILED = new StatusConstant(1004L, "JOB_FAILED", "执行失败");
	public final static StatusConstant JOB_EXPIRED = new StatusConstant(1005L, "JOB_EXPIRED", "任务过期");
		
	//会员状态
	public final static StatusConstant MEMBER_NO_ACTIVATION = new StatusConstant(1101L, "MEMBER_NO_ACTIVATION", "未激活");
	public final static StatusConstant MEMBER_ACTIVATION = new StatusConstant(1102L, "MEMBER_ACTIVATION", "激活");
	public final static StatusConstant MEMBER_LOCKING = new StatusConstant(1103L, "MEMBER_LOCKING", "锁定");
	public final static StatusConstant MEMBER_DISABLED = new StatusConstant(1104L, "MEMBER_DISABLED", "停用");
	public final static StatusConstant MEMBER_REMOVE = new StatusConstant(1105L, "MEMBER_REMOVE", "删除");
	
	//外部账号状态
	public final static StatusConstant MEMBER_ACCOUNT_BINDING = new StatusConstant(1201L, "MEMBER_ACCOUNT_BINDING", "已绑定");
	public final static StatusConstant MEMBER_ACCOUNT_UNBINDING = new StatusConstant(1202L, "MEMBER_ACCOUNT_UNBINDING", "已解除");
	
	//会员卡状态
	public final static StatusConstant MEMBER_CARD_CREATED = new StatusConstant(1301L, "MEMBER_CARD_CREATED", "已创建");
	public final static StatusConstant MEMBER_CARD_NOT_ACTIVATION = new StatusConstant(1302L, "MEMBER_CARD_NOT_ACTIVATION", "未激活");
	public final static StatusConstant MEMBER_CARD_ENABLED = new StatusConstant(1303L, "MEMBER_CARD_ENABLED", "已启用");
	public final static StatusConstant MEMBER_CARD_DISABLED = new StatusConstant(1304L, "MEMBER_CARD_DISABLED", "已停用");
	public final static StatusConstant MEMBER_CARD_REMOVE = new StatusConstant(1305L, "MEMBER_CARD_REMOVE", "已挂失");
	
	public final static StatusConstant MEMBER_ENTITY_CARD_ENABLE = new StatusConstant(1306L, "MEMBER_ENTITY_CARD_ENABLE", "可使用");
	public final static StatusConstant MEMBER_ENTITY_CARD_DISABLED = new StatusConstant(1307L, "MEMBER_ENTITY_CARD_DISABLED", "已使用");
	public final static StatusConstant MEMBER_ENTITY_CARD_NO = new StatusConstant(1308L, "MEMBER_ENTITY_CARD_DISABLED", "未制卡");
	public final static StatusConstant MEMBER_ENTITY_CARD_YES = new StatusConstant(1309L, "MEMBER_ENTITY_CARD_DISABLED", "已制卡");
	
	
	//优惠券状态
	public final static StatusConstant COUPON_NEW = new StatusConstant(1401L, "COUPON_NEW", "已创建");
	public final static StatusConstant COUPON_ISSUED = new StatusConstant(1402L, "COUPON_ISSUED", "已下发");
	public final static StatusConstant COUPON_EXPIRE = new StatusConstant(1403L, "COUPON_EXPIRE", "已过期");
	public final static StatusConstant COUPON_DESTROY = new StatusConstant(1404L, "COUPON_DESTROY", "已作废");
	public final static StatusConstant COUPON_DELETE = new StatusConstant(1405L, "COUPON_DELETE", "已删除");
	public final static StatusConstant COUPON_ISSUEDING = new StatusConstant(1406L, "COUPON_ISSUEDING", "发放中");
	
	//优惠推送状态
	public final static StatusConstant COUPON_ISSUE_WAIT = new StatusConstant(1501L, "COUPON_ISSUE_WAIT", "待推送");
	public final static StatusConstant COUPON_ISSUE_COMPLETE = new StatusConstant(1502L, "COUPON_ISSUE_COMPLETE", "已推送");
	public final static StatusConstant COUPON_ISSUE_ISSUING = new StatusConstant(1503L, "COUPON_ISSUE_ISSUING", "推送中");
	public final static StatusConstant COUPON_ISSUE_NOGRANT = new StatusConstant(1504L, "COUPON_ISSUE_NOGRANT", "未发放");
	
	//优惠实例状态
	public final static StatusConstant COUPON_INSTANCE_CREATE = new StatusConstant(1601L, "COUPON_INSTANCE_CREATE", "已创建");
	public final static StatusConstant COUPON_INSTANCE_UNUSED = new StatusConstant(1602L, "COUPON_INSTANCE_UNUSED", "未使用");
	public final static StatusConstant COUPON_INSTANCE_USED = new StatusConstant(1603L, "COUPON_INSTANCE_USED", "已使用");
	public final static StatusConstant COUPON_INSTANCE_RECEIVE = new StatusConstant(1611L, "COUPON_INSTANCE_USED", "已领取");
	public final static StatusConstant COUPON_INSTANCE_EXPIRE = new StatusConstant(1604L, "COUPON_INSTANCE_EXPIRE", "已过期");
	public final static StatusConstant COUPON_INSTANCE_DESTROY = new StatusConstant(1605L, "COUPON_INSTANCE_DESTROY", "已作废");
	public final static StatusConstant COUPON_INSTANCE_USING = new StatusConstant(1606L, "COUPON_INSTANCE_USING", "使用中");
	public final static StatusConstant COUPON_INSTANCE_SHARE = new StatusConstant(1607L, "COUPON_INSTANCE_SHARE", "分享中");
	public final static StatusConstant COUPON_INSTANCE_FREEZE = new StatusConstant(1608L, "COUPON_INSTANCE_FREEZE", "已冻结");
	public final static StatusConstant COUPON_INSTANCE_FFING = new StatusConstant(1699L, "COUPON_INSTANCE_FFING", "正在进行");
	
	//积分状态
	public final static StatusConstant MEMBER_POINTS_ITEM_AVAILABLE = new StatusConstant(1701l, "MEMBER_POINTS_ITEM_AVAILABLE", "可使用");
	public final static StatusConstant MEMBER_POINTS_ITEM_UNAVAILABLE = new StatusConstant(1702l, "MEMBER_POINTS_ITEM_UNAVAILABLE", "不可使用");
	public final static StatusConstant MEMBER_POINTS_ITEM_USED = new StatusConstant(1703l, "MEMBER_POINTS_ITEM_USED", "已使用");
	public final static StatusConstant MEMBER_POINTS_ITEM_EXPIRE = new StatusConstant(1704l, "MEMBER_POINTS_ITEM_EXPIRE", "已过期");
	public final static StatusConstant MEMBER_POINTS_ITEM_FREEZE = new StatusConstant(1705l, "MEMBER_POINTS_ITEM_FREEZE", "已冻结");
	public final static StatusConstant MEMBER_POINTS_ITEM_CANCEL = new StatusConstant(1706l, "MEMBER_POINTS_ITEM_CANCEL", "已取消");
	
	//订单状态
	public final static StatusConstant ORDER_STATUS_SUCCESS = new StatusConstant(1801L, "ORDER_STATUS_SUCCESS", "成功");
	public final static StatusConstant ORDER_STATUS_CANCEL = new StatusConstant(1802L, "ORDER_STATUS_CANCEL", "取消");
	public final static StatusConstant ORDER_PAYMENT_STATUS_SUCCESS = new StatusConstant(1803L, "ORDER_PAYMENT_STATUS_SUCCESS", "已付款");
		
	//活动状态
	public final static StatusConstant ACTIVITY_DRAFT = new StatusConstant(1901l, "ACTIVITY_DRAFT", "已创建");
	public final static StatusConstant ACTIVITY_ENABLE = new StatusConstant(1902l, "ACTIVITY_ENABLE", "已启用");
	public final static StatusConstant ACTIVITY_EXPIRED = new StatusConstant(1903l, "ACTIVITY_EXPIRED", "已过期");
	public final static StatusConstant ACTIVITY_INVALID = new StatusConstant(1904l, "ACTIVITY_INVALID", "已作废");
	public final static StatusConstant ACTIVITY_DISABLE = new StatusConstant(1905l, "ACTIVITY_DISABLE", "已停用");
	
	//会员组状态
	public final static StatusConstant MEMBER_GROUP_AND_TAG_CREATED = new StatusConstant(2001L, "MEMBER_GROUP_AND_TAG_CREATED", "已创建");
	public final static StatusConstant MEMBER_GROUP_AND_TAG_USE = new StatusConstant(2002L, "MEMBER_GROUP_AND_TAG_USE", "已使用");
	public final static StatusConstant MEMBER_GROUP_AND_TAG_REMOVE = new StatusConstant(2003L, "MEMBER_GROUP_AND_TAG_REMOVE", "已删除");
	public final static StatusConstant MEMBER_TAG_SETTING = new StatusConstant(2004L, "MEMBER_TAG_SETTING", "正在执行");
	
	//活动实例状态
	public final static StatusConstant ACTIVITY_INSTANCE_DRAFT = new StatusConstant(2101l, "ACTIVITY_INSTANCE_DRAFT", "已创建");
	public final static StatusConstant ACTIVITY_INSTANCE_ENABLE = new StatusConstant(2102l, "ACTIVITY_INSTANCE_ENABLE", "已启用");
	public final static StatusConstant ACTIVITY_INSTANCE_EXPIRED = new StatusConstant(2103l, "ACTIVITY_INSTANCE_EXPIRED", "已过期");
	public final static StatusConstant ACTIVITY_INSTANCE_BREAK = new StatusConstant(2104l, "ACTIVITY_INSTANCE_BREAK", "已中止");
	
	//商品展示状态
	public final static StatusConstant GOODS_SHOW_REMOVE = new StatusConstant(2201l, "GOODS_SHOW_REMOVE", "已删除");
	public final static StatusConstant GOODS_SHOW_ENABLE = new StatusConstant(2202l, "GOODS_SHOW_ENABLE", "上架");
	public final static StatusConstant GOODS_SHOW_DISENABLE = new StatusConstant(2203l, "GOODS_SHOW_DISENABLE", "下架");
	
	//商品展示类型状态
	public final static StatusConstant GOODS_SHOW_CATEGORY_ENABLE = new StatusConstant(2301l, "GOODS_SHOW_CATEGORY_ENABLE", "有效");
	public final static StatusConstant GOODS_SHOW_CATEGORY_DISENABLE = new StatusConstant(2302l, "GOODS_SHOW_CATEGORY_DISENABLE", "无效");
	
	//优惠券类别明细状态
	public final static StatusConstant COUPON_CATEGORY_ITEM_REMOVE = new StatusConstant(2401l, "COUPON_CATEGORY_ITEM_REMOVE", "已删除");
	public final static StatusConstant COUPON_CATEGORY_ITEM_ENABLE = new StatusConstant(2402l, "COUPON_CATEGORY_ITEM_ENABLE", "上架");
	public final static StatusConstant COUPON_CATEGORY_ITEM_DISENABLE = new StatusConstant(2403l, "COUPON_CATEGORY_ITEM_DISENABLE", "下架");
	
	//优惠券类别状态
	public final static StatusConstant COUPON_CATEGORY_REMOVE = new StatusConstant(2501l, "COUPON_CATEGORY_REMOVE", "已删除");
	public final static StatusConstant COUPON_CATEGORY_ENABLE = new StatusConstant(2502l, "COUPON_CATEGORY_ENABLE", "上架");
	public final static StatusConstant COUPON_CATEGORY_DISENABLE = new StatusConstant(2503l, "COUPON_CATEGORY_DISENABLE", "下架");

	//微信状态
	public final static StatusConstant WECHAT_ENABLE= new StatusConstant(3001L, "WECHAT_ENABLE", "有效");
	public final static StatusConstant WECHAT_DISENABLE= new StatusConstant(3002L, "WECHAT_DISENABLE", "无效");

	//系统用户
	public final static StatusConstant SYS_USER_ACTIVE = new StatusConstant(4001L, "SYS_USER_ACTIVE", "已生效");
	public final static StatusConstant SYS_USER_REMOVE = new StatusConstant(4002L, "SYS_USER_REMOVE", "已删除");
	
	//短信模板
	public final static StatusConstant SMS_TEMP_IS_ACTIVE = new StatusConstant(4101L, "SMS_TEMP_IS_ACTIVE", "启用");
	public final static StatusConstant SMS_TEMP_IS_NOT_ACTIVE = new StatusConstant(4102L, "SMS_TEMP_IS_NOT_ACTIVE", "禁用");
	public final static StatusConstant SMS_TEMP_IS_REMOVE = new StatusConstant(4103L, "SMS_TEMP_IS_REMOVE", "删除");
	
	//短信模板类型
	public final static StatusConstant SMS_TEMP_TYPE_IS_ACTIVE = new StatusConstant(4201L, "SMS_TEMP_TYPE_IS_ACTIVE", "启用");
	public final static StatusConstant SMS_TEMP_TYPE_IS_NOT_ACTIVE = new StatusConstant(4202L, "SMS_TEMP_TYPE_IS_NOT_ACTIVE", "停用");
	
	//门店
	public final static StatusConstant STORE_ACTIVE = new StatusConstant(5001L, "STORE_ACTIVE", "正常");
	public final static StatusConstant STORE_REMOVE = new StatusConstant(5002L, "STORE_REMOVE", "已删除");

	//产品状态
	public final static StatusConstant PRODUCT_ACTIVE = new StatusConstant(5101L, "PRODUCT_ACTIVE", "正常");
	public final static StatusConstant PRODUCT_REMOVE = new StatusConstant(5102L, "PRODUCT_REMOVE", "已删除");

	//区域状态
	public final static StatusConstant ORG_ACTIVE = new StatusConstant(5201L, "Y", "正常");
	public final static StatusConstant ORG_REMOVE = new StatusConstant(5202L, "N", "已删除");
	
	//用户角色状态
	public final static StatusConstant SYS_ROLE_ACTIVE = new StatusConstant(5301L, "Y", "正常");
	public final static StatusConstant SYS_ROLE_REMOVE = new StatusConstant(5302L, "N", "已删除");
	
	//积分规则状态
	public final static StatusConstant POINTS_RULE_ACTIVE = new StatusConstant(5301L, "Y", "正常");
	public final static StatusConstant POINTS_RULE_REMOVE = new StatusConstant(5302L, "N", "已删除");
	
	//二维码状态
	public final static StatusConstant QR_CODE_ACTIVE = new StatusConstant(5401L, "Y", "正常");
	public final static StatusConstant QR_CODE_REMOVE = new StatusConstant(5402L, "N", "已删除");
	
	//奖品实例状态
	public final static StatusConstant PRIZE_INSTANCE_UNEXTRACT = new StatusConstant(6001L, "PRIZE_INSTANCE_UNEXTRACT", "奖品未抽取");
	public final static StatusConstant PRIZE_INSTANCE_EXTRACTED = new StatusConstant(6002L, "PRIZE_INSTANCE_EXTRACTED", "奖品已抽取");
	
	//会员地址状态
	public final static StatusConstant MEMBER_ADDRESS_ACTIVE = new StatusConstant(6001L, "MEMBER_ADDRESS_ACTIVE", "地址未删除");
	public final static StatusConstant MEMBER_ADDRESS_REMOVE = new StatusConstant(6002L, "MEMBER_ADDRESS_REMOVE", "地址已删除");
	//服务单状态
	public final static StatusConstant COMPLAIN_NOTREAD = new StatusConstant(7001L, "COMPLAIN_NOTREAD", "未读");
	public final static StatusConstant COMPLAIN_READED = new StatusConstant(7002L, "COMPLAIN_READED", "已读");
	public final static StatusConstant COMPLAIN_CLOSED = new StatusConstant(7003L, "COMPLAIN_CLOSED", "已解决");

	//微信返回值状态
	public final static StatusConstant WECHAT_ERRCODE_SUC = new StatusConstant(0L, "WECHAT_ERRCODE_SUC", "操作成功");
	
	public final static StatusConstant COUPON_JOIN = new StatusConstant(1L, "COUPON_JOIN", "入会礼");
	public final static StatusConstant COUPON_BRITH = new StatusConstant(2L, "COUPON_BRITH", "生日礼");
	
	
	//会员消息状态
	public final static StatusConstant MEMBERMESSAGE_NOTREAD = new StatusConstant(8001L, "MEMBERMESSAGE_NOTREAD", "未读");
	public final static StatusConstant MEMBERMESSAGE_READED = new StatusConstant(8002L, "MEMBERMESSAGE_READED", "已读");
	public final static StatusConstant MEMBERMESSAGE_SENDED = new StatusConstant(8003L, "MEMBERMESSAGE_SENDED", "已发送");
	public final static StatusConstant MEMBERMESSAGE_RECALL = new StatusConstant(8004L, "MEMBERMESSAGE_RECALL", "已撤回");
	public final static StatusConstant MEMBERMESSAGE_DELETED = new StatusConstant(8005L, "MEMBERMESSAGE_DELETED", "已删除");
	
	//食段
	public final static StatusConstant MEAL_ACTIVE = new StatusConstant(9001L, "MEAL_ACTIVE", "正常");
	public final static StatusConstant MEAL_REMOVE = new StatusConstant(9002L, "MEAL_REMOVE", "已删除");
	
	//积分调账时默认关联的积分规则ID:10
	public final static StatusConstant POINTS_REVISE_RULE_ID = new StatusConstant(0L, "POINTS_REVISE_RULE_ID", "积分调账");
		
	//积分调账时默认关联的活动实例ID:100
	public final static StatusConstant POINTS_REVISE_ACTIVITY_ID = new StatusConstant(0L, "POINTS_REVISE_ACTIVITY_ID", "积分调账");
	
	public final static StatusConstant MEMBER_EQUITY_ENABLE = new StatusConstant(7001L, "MEMBER_EQUITY_ENABLE", "有效");
	public final static StatusConstant MEMBER_EQUITY_DISENABLE = new StatusConstant(7002L, "MEMBER_EQUITY_DISENABLE", "无效");
	
	//场景游戏实例状态
	public final static StatusConstant SCENE_GAME_INSTANCE_SAVE = new StatusConstant(6101l, "SCENE_GAME_INSTANCE_SAVE", "已保存");
	public final static StatusConstant SCENE_GAME_INSTANCE_ENABLE = new StatusConstant(6102l, "SCENE_GAME_INSTANCE_ENABLE", "已激活");
	public final static StatusConstant SCENE_GAME_INSTANCE_EXPIRED = new StatusConstant(6103l, "SCENE_GAME_INSTANCE_EXPIRED", "已过期");
	public final static StatusConstant SCENE_GAME_INSTANCE_CLOSE = new StatusConstant(6104l, "SCENE_GAME_INSTANCE_CLOSE", "已关闭");
	
	public StatusConstant(Long id, String code, String name) {
		super(id, code, name);
	}	
	
	@Override
	public Status getInstance() {
		Status status = new Status();
		status.setStatusId(id);
		status.setStatusCode(code);
		status.setStatusName(name);
		return status;
	}

}
