package com.bw.adv.module.coupon.model;

public class CouponOrigin {
    private Integer couponOriginId;

    private String couponOriginName;

    private String isActive;

    public Integer getCouponOriginId() {
        return couponOriginId;
    }

    public void setCouponOriginId(Integer couponOriginId) {
        this.couponOriginId = couponOriginId;
    }

    public String getCouponOriginName() {
        return couponOriginName;
    }

    public void setCouponOriginName(String couponOriginName) {
        this.couponOriginName = couponOriginName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}