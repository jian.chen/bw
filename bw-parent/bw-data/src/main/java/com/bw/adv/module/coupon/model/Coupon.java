package com.bw.adv.module.coupon.model;

import java.math.BigDecimal;

public class Coupon {
	
	private String couponNameEn;
	
	private String commentsEn;
	
	private String remarkEn;
    
    private String extField1En;
	
    private Long couponId;

    private Long couponTypeId;
    
    private Long couponBusinessTypeId;

    private Long activityId;

    private Long orgId;

    private Long statusId;

    private String isCommon;

    private String couponCode;

    private String couponName;
    
    private String couponImg1;

    private String couponImg2;

    private String couponImg3;

    private BigDecimal amount;

    private BigDecimal faceAmount;

    private String isPromo;

    private BigDecimal discount;

    private Long seq;

    private String comments;

    private String fromDate;

    private String thruDate;

    private String createTime;

    private String createBy;

    private String firstPublishTime;
    
    private String lastPublishTime;

    private String publishBy;

    private String isActive;

    private String fromGrabTime;

    private String timePeriod;

    private String thruGrabTime;

    private String isGrabMember;

    private String isGrabGenericMember;

    private BigDecimal useSatisfyAmount;
    
    private String remark;
    
    private String webchatRemark;
    
    private String extField1;

    private String extField2;

    private String extField3;

    private String extField4;

    private String extField5;

    private String extField6;

    private String extField7;

    private String extField8;

    private String extField9;

    private String extField10;
    
    private String expireType;
    
    private String afterDate;
    
    private String expireValue;
    
    private Long totalQuantity;
    
    private Long issueQuantity;
    
    private Long usedQuantity;
    
    private Long availableQuantity;
    
    private String isReceive;
    
    private String isRemind;
    
    private String isOwnerUsed;
    private Integer originId;

    private Integer externalMerchantId;

	public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getCouponTypeId() {
        return couponTypeId;
    }

    public void setCouponTypeId(Long couponTypeId) {
        this.couponTypeId = couponTypeId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getIsCommon() {
        return isCommon;
    }

    public void setIsCommon(String isCommon) {
        this.isCommon = isCommon;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponImg1() {
        return couponImg1;
    }

    public void setCouponImg1(String couponImg1) {
        this.couponImg1 = couponImg1;
    }

    public String getCouponImg2() {
        return couponImg2;
    }

    public void setCouponImg2(String couponImg2) {
        this.couponImg2 = couponImg2;
    }

    public String getCouponImg3() {
        return couponImg3;
    }

    public void setCouponImg3(String couponImg3) {
        this.couponImg3 = couponImg3;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFaceAmount() {
        return faceAmount;
    }

    public void setFaceAmount(BigDecimal faceAmount) {
        this.faceAmount = faceAmount;
    }

    public String getIsPromo() {
        return isPromo;
    }

    public void setIsPromo(String isPromo) {
        this.isPromo = isPromo;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getThruDate() {
        return thruDate;
    }

    public void setThruDate(String thruDate) {
        this.thruDate = thruDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

	public String getFirstPublishTime() {
		return firstPublishTime;
	}

	public void setFirstPublishTime(String firstPublishTime) {
		this.firstPublishTime = firstPublishTime;
	}

	public String getLastPublishTime() {
		return lastPublishTime;
	}

	public void setLastPublishTime(String lastPublishTime) {
		this.lastPublishTime = lastPublishTime;
	}

	public String getPublishBy() {
        return publishBy;
    }

    public void setPublishBy(String publishBy) {
        this.publishBy = publishBy;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getFromGrabTime() {
        return fromGrabTime;
    }

    public void setFromGrabTime(String fromGrabTime) {
        this.fromGrabTime = fromGrabTime;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getExtField1() {
        return extField1;
    }

    public void setExtField1(String extField1) {
        this.extField1 = extField1;
    }

    public String getExtField2() {
        return extField2;
    }

    public void setExtField2(String extField2) {
        this.extField2 = extField2;
    }

    public String getExtField3() {
        return extField3;
    }

    public void setExtField3(String extField3) {
        this.extField3 = extField3;
    }

    public String getExtField4() {
        return extField4;
    }

    public void setExtField4(String extField4) {
        this.extField4 = extField4;
    }

    public String getExtField5() {
        return extField5;
    }

    public void setExtField5(String extField5) {
        this.extField5 = extField5;
    }

    public String getExtField6() {
        return extField6;
    }

    public void setExtField6(String extField6) {
        this.extField6 = extField6;
    }

    public String getExtField7() {
        return extField7;
    }

    public void setExtField7(String extField7) {
        this.extField7 = extField7;
    }

    public String getExtField8() {
        return extField8;
    }

    public void setExtField8(String extField8) {
        this.extField8 = extField8;
    }

    public String getExtField9() {
        return extField9;
    }

    public void setExtField9(String extField9) {
        this.extField9 = extField9;
    }

    public String getExtField10() {
        return extField10;
    }

    public void setExtField10(String extField10) {
        this.extField10 = extField10;
    }

    public String getThruGrabTime() {
        return thruGrabTime;
    }

    public void setThruGrabTime(String thruGrabTime) {
        this.thruGrabTime = thruGrabTime;
    }

    public String getIsGrabMember() {
        return isGrabMember;
    }

    public void setIsGrabMember(String isGrabMember) {
        this.isGrabMember = isGrabMember;
    }

    public String getIsGrabGenericMember() {
        return isGrabGenericMember;
    }

    public void setIsGrabGenericMember(String isGrabGenericMember) {
        this.isGrabGenericMember = isGrabGenericMember;
    }

    public BigDecimal getUseSatisfyAmount() {
        return useSatisfyAmount;
    }

    public void setUseSatisfyAmount(BigDecimal useSatisfyAmount) {
        this.useSatisfyAmount = useSatisfyAmount;
    }

	public String getRemark() {
		return remark;
	}
	
	public String getWebchatRemark() {
		return webchatRemark;
	}

	public void setWebchatRemark(String webchatRemark) {
		this.webchatRemark = webchatRemark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getExpireType() {
		return expireType;
	}

	public void setExpireType(String expireType) {
		this.expireType = expireType;
	}

	public String getAfterDate() {
		return afterDate;
	}

	public void setAfterDate(String afterDate) {
		this.afterDate = afterDate;
	}

	public String getExpireValue() {
		return expireValue;
	}

	public void setExpireValue(String expireValue) {
		this.expireValue = expireValue;
	}

	public Long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Long getIssueQuantity() {
		return issueQuantity;
	}

	public void setIssueQuantity(Long issueQuantity) {
		this.issueQuantity = issueQuantity;
	}

	public Long getUsedQuantity() {
		return usedQuantity;
	}

	public void setUsedQuantity(Long usedQuantity) {
		this.usedQuantity = usedQuantity;
	}

	public Long getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Long availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public Long getCouponBusinessTypeId() {
		return couponBusinessTypeId;
	}

	public void setCouponBusinessTypeId(Long couponBusinessTypeId) {
		this.couponBusinessTypeId = couponBusinessTypeId;
	}

	public String getIsReceive() {
		return isReceive;
	}

	public void setIsReceive(String isReceive) {
		this.isReceive = isReceive;
	}

	public String getIsRemind() {
		return isRemind;
	}

	public void setIsRemind(String isRemind) {
		this.isRemind = isRemind;
	}

	public String getIsOwnerUsed() {
		return isOwnerUsed;
	}

	public void setIsOwnerUsed(String isOwnerUsed) {
		this.isOwnerUsed = isOwnerUsed;
	}

	public String getCouponNameEn() {
		return couponNameEn;
	}

	public void setCouponNameEn(String couponNameEn) {
		this.couponNameEn = couponNameEn;
	}

	public String getCommentsEn() {
		return commentsEn;
	}

	public void setCommentsEn(String commentsEn) {
		this.commentsEn = commentsEn;
	}

	public String getRemarkEn() {
		return remarkEn;
	}

	public void setRemarkEn(String remarkEn) {
		this.remarkEn = remarkEn;
	}

	public String getExtField1En() {
		return extField1En;
	}

	public void setExtField1En(String extField1En) {
		this.extField1En = extField1En;
	}

    public Integer getOriginId() {
        return originId;
    }

    public void setOriginId(Integer originId) {
        this.originId = originId;
    }

    public Integer getExternalMerchantId() {
        return externalMerchantId;
    }

    public void setExternalMerchantId(Integer externalMerchantId) {
        this.externalMerchantId = externalMerchantId;
    }
}