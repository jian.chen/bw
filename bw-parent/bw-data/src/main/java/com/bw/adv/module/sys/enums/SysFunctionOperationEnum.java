package com.bw.adv.module.sys.enums;

/**
 * Created by jeoy.zhou on 12/31/15.
 */
public enum SysFunctionOperationEnum {

    BLOCK(0, "停用"),
    ACTIVE(1, "启用");


    private Integer key;
    private String desc;

    private SysFunctionOperationEnum(Integer key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public Integer getKey() {
        return this.key;
    }

    public String getDesc() {
        return this.desc;
    }
}
