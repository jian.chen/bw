package com.bw.adv.module.coupon.model.exp;

import com.bw.adv.module.coupon.model.CouponRange;

public class CouponRangeExp extends CouponRange {
	//区域ID
	private String orgIds;
	//区域名称
    private String orgName;

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

}