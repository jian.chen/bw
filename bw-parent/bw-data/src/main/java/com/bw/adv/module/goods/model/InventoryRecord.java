package com.bw.adv.module.goods.model;

public class InventoryRecord {
    private Long inventoryRecordId;

    private String inventoryRecordDesc;

    private Integer quantity;

    private Integer balanceQuantity;

    private Integer totalQuantity;

    private Long createBy;

    private String createTime;

    private String remark;

    public Long getInventoryRecordId() {
        return inventoryRecordId;
    }

    public void setInventoryRecordId(Long inventoryRecordId) {
        this.inventoryRecordId = inventoryRecordId;
    }

    public String getInventoryRecordDesc() {
        return inventoryRecordDesc;
    }

    public void setInventoryRecordDesc(String inventoryRecordDesc) {
        this.inventoryRecordDesc = inventoryRecordDesc;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getBalanceQuantity() {
        return balanceQuantity;
    }

    public void setBalanceQuantity(Integer balanceQuantity) {
        this.balanceQuantity = balanceQuantity;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}