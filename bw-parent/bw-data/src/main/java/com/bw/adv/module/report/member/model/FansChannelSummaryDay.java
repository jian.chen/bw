package com.bw.adv.module.report.member.model;

public class FansChannelSummaryDay {
    private Long fansChannelSummaryDayId;

    private Long channelId;
    
    private Long qrCodeId;

    private Integer summaryYear;

    private Integer summaryMonth;

    private Integer summaryDay;

    private String summaryDate;

    private Integer dailyAddNumber;

    private Integer totalNumber;

    private String createTime;

    public Long getFansChannelSummaryDayId() {
        return fansChannelSummaryDayId;
    }

    public void setFansChannelSummaryDayId(Long fansChannelSummaryDayId) {
        this.fansChannelSummaryDayId = fansChannelSummaryDayId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }
    
    public Long getQrCodeId() {
		return qrCodeId;
	}

	public void setQrCodeId(Long qrCodeId) {
		this.qrCodeId = qrCodeId;
	}

	public Integer getSummaryYear() {
        return summaryYear;
    }

    public void setSummaryYear(Integer summaryYear) {
        this.summaryYear = summaryYear;
    }

    public Integer getSummaryMonth() {
        return summaryMonth;
    }

    public void setSummaryMonth(Integer summaryMonth) {
        this.summaryMonth = summaryMonth;
    }

    public Integer getSummaryDay() {
        return summaryDay;
    }

    public void setSummaryDay(Integer summaryDay) {
        this.summaryDay = summaryDay;
    }

    public String getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = summaryDate;
    }

    public Integer getDailyAddNumber() {
        return dailyAddNumber;
    }

    public void setDailyAddNumber(Integer dailyAddNumber) {
        this.dailyAddNumber = dailyAddNumber;
    }

    public Integer getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Integer totalNumber) {
        this.totalNumber = totalNumber;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}