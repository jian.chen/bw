package com.bw.adv.module.wechat.model.exp;

import com.bw.adv.module.wechat.model.AccessTicketCache;
/**
 * 
 * @author roy.zhang
 *
 */
public class AccessTicketCacheExp extends AccessTicketCache{
	private String timeCondition;

	public String getTimeCondition() {
		return timeCondition;
	}

	public void setTimeCondition(String timeCondition) {
		this.timeCondition = timeCondition;
	}
	
	
}
