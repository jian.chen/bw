package com.bw.adv.module.member.model;

public class MemberAddress {
    private Long memberAddressId;

    private Long memberId;

    private Long provinceId;
    
    private String areaName;
    
	private String provinceName;

    private Long cityId;
    
    private String cityName;

    private Long districtId;
    
    private String districtName;

	private String personName;

    private String personMobile;

    private String phoneAreaCode;

    private String personPhone;
    
    private String personPhoneZone;
    
    private String personPhoneNum;

	private String addressDetail;

    private String isDefault;

    private String remark;

    private Long statusId;

    public Long getMemberAddressId() {
        return memberAddressId;
    }

    public void setMemberAddressId(Long memberAddressId) {
        this.memberAddressId = memberAddressId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonMobile() {
        return personMobile;
    }

    public void setPersonMobile(String personMobile) {
        this.personMobile = personMobile;
    }

    public String getPhoneAreaCode() {
        return phoneAreaCode;
    }

    public void setPhoneAreaCode(String phoneAreaCode) {
        this.phoneAreaCode = phoneAreaCode;
    }

    public String getPersonPhone() {
        return personPhone;
    }

    public void setPersonPhone(String personPhone) {
        this.personPhone = personPhone;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
    
    public String getPersonPhoneZone() {
		return personPhoneZone;
	}

	public void setPersonPhoneZone(String personPhoneZone) {
		this.personPhoneZone = personPhoneZone;
	}

	public String getPersonPhoneNum() {
		return personPhoneNum;
	}

	public void setPersonPhoneNum(String personPhoneNum) {
		this.personPhoneNum = personPhoneNum;
	}
	
	 public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	
	 public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
}