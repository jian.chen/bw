package com.bw.adv.module.member.model.exp;

import com.bw.adv.module.member.model.MemberGift;

public class MemberGiftExt extends MemberGift {
	private Integer memberGiftNum;

	public Integer getMemberGiftNum() {
		return memberGiftNum;
	}

	public void setMemberGiftNum(Integer memberGiftNum) {
		this.memberGiftNum = memberGiftNum;
	}
}