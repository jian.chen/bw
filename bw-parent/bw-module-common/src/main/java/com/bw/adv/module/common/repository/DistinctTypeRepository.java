package com.bw.adv.module.common.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.common.mapper.DistinctTypeMapper;
import com.bw.adv.module.common.model.DistinctType;


/**
 * 
 * ClassName: DistinctTypeRepository <br/>
 * date: 2015-8-12 下午3:51:27 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface DistinctTypeRepository extends BaseRepository<DistinctType, DistinctTypeMapper> {
	
}