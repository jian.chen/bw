/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-common
 * File Name:TreeUtil.java
 * Package Name:com.sage.scrm.module.common.utils
 * Date:2015年8月25日下午9:18:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.common.utils;

import java.util.ArrayList;
import java.util.List;

import com.bw.adv.module.common.model.TreeMenuModel;
import com.bw.adv.module.wechat.model.WechatButton;

/**
 * ClassName:TreeUtil <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月25日 下午9:18:51 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class TreeUtil {
	public static List<TreeMenuModel> dualWithTreeModel(List<TreeMenuModel> result) {
		List<TreeMenuModel> treeList = new ArrayList<TreeMenuModel>();  
		for(TreeMenuModel node1 : result){  
		    boolean mark = false;  
		    for(TreeMenuModel node2 : result){  
		        if(node1.getPid()!=null && node1.getPid().equals(node2.getId())){  
		            mark = true;  
		            if(node2.getChildren() == null)  
		                node2.setChildren(new ArrayList<TreeMenuModel>());  
		            node2.getChildren().add(node1);   
		            break;  
		        }  
		    }  
		    if(!mark){  
		    	treeList.add(node1);   
		    }  
		}
		return treeList;
	}
	
	public static List<WechatButton> dualWithWechatButton(List<WechatButton> result) {
		List<WechatButton> treeList = new ArrayList<WechatButton>();  
		for(WechatButton node1 : result){  
		    boolean mark = false;  
		    for(WechatButton node2 : result){  
		        if(node1.getPid()!=null && node1.getPid().equals(node2.getId())){  
		            mark = true;  
		            if(node2.getSub_button() == null)  
		                node2.setSub_button(new ArrayList<WechatButton>());  
		            node2.getSub_button().add(node1);   
		            break;  
		        }  
		    }  
		    if(!mark){  
		    	treeList.add(node1);   
		    }  
		}
		return treeList;
	}

}

