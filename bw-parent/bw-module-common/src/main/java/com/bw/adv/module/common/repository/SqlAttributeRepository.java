package com.bw.adv.module.common.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.common.mapper.SqlAttributeMapper;
import com.bw.adv.module.common.model.SqlAttribute;


/**
 * 
 * ClassName: SqlAttributeRepository <br/>
 * date: 2015-8-12 下午3:51:27 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface SqlAttributeRepository extends BaseRepository<SqlAttribute, SqlAttributeMapper> {

	List<SqlAttribute> findSqlAttrList(Long type);
	
}