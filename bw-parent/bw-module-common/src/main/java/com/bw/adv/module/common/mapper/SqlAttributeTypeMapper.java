package com.bw.adv.module.common.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.model.SqlAttribute;
import com.bw.adv.module.common.model.SqlAttributeType;

public interface SqlAttributeTypeMapper extends BaseMapper<SqlAttributeType>{
    int deleteByPrimaryKey(String typeName);

    int insert(SqlAttributeType record);

    int insertSelective(SqlAttributeType record);

    SqlAttributeType selectByPrimaryKey(String typeName);

    int updateByPrimaryKeySelective(SqlAttributeType record);

    int updateByPrimaryKey(SqlAttributeType record);
}