package com.bw.adv.module.common.api.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.api.model.ApiObjMapping;

public interface ApiObjMappingMapper extends BaseMapper<ApiObjMapping>{
	
}