package com.bw.adv.module.common.api.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.common.api.mapper.ApiObjMapper;
import com.bw.adv.module.common.api.mapper.ApiObjMappingMapper;
import com.bw.adv.module.common.api.model.ApiObjMapping;
import com.bw.adv.module.common.api.repository.ApiObjMappingRepository;
import com.bw.adv.module.common.api.repository.ApiObjRepository;


/**
 * 
 * ClassName: DistinctTypeRepository <br/>
 * date: 2015-8-12 下午3:51:27 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class ApiObjMappingRepositoryImpl extends BaseRepositoryImpl<ApiObjMapping, ApiObjMappingMapper> implements ApiObjMappingRepository{

	@Override
	protected Class<ApiObjMappingMapper> getMapperClass() {
		return ApiObjMappingMapper.class;
	}
	
}