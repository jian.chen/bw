package com.bw.adv.module.common.demo;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.component.msg.model.SmsInstance;

public interface mapperDemo extends BaseMapper<SmsInstance> {
	
	public List<SmsInstance> selectByMobile(String mobile);
	
	@Override
	public int insertBatch(List<SmsInstance> list);

	List<SmsInstance> selectByExample(Example example);
	
}