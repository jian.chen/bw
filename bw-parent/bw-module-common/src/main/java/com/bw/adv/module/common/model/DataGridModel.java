package com.bw.adv.module.common.model;

import java.util.ArrayList;
import java.util.List;

public class DataGridModel<T> extends JsonModel {

	private static final long serialVersionUID = -1682677776160623994L;

	private Long              total            = 0L;
	private List<T>           rows             = new ArrayList<T>();
	private Object obj;
	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	private int flag;
//	private List<T>           footer           = new ArrayList<T>();

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public DataGridModel() {
		super();
		this.setStatusSuccess();
	}

	public DataGridModel(String message) {
		this(message, false);
	}

	public DataGridModel(String message, boolean isSuccess) {
		if (isSuccess) {
			this.setStatusSuccess();
		}
		else {
			this.setStatusError();
		}
		this.message = message;
	}

	public DataGridModel(List<T> rows, Long total) {
		this.setStatusSuccess();
		this.total = total;
		this.rows = rows;
	}
	
	public DataGridModel(List<T> rows, Long total, int flag) {
		this.setStatusSuccess();
		this.total = total;
		this.rows = rows;
		this.flag=flag;
	}

	public Long getTotal() {
		return this.total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<T> getRows() {
		return this.rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

//	public List<T> getFooter() {
//		return this.footer;
//	}
//
//	public void setFooter(List<T> footer) {
//		this.footer = footer;
//	}

}
