package com.bw.adv.module.common.api.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.common.api.mapper.ApiObjMappingMapper;
import com.bw.adv.module.common.api.model.ApiObjMapping;


/**
 * 
 * ClassName: DistinctTypeRepository <br/>
 * date: 2015-8-12 下午3:51:27 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface ApiObjMappingRepository extends BaseRepository<ApiObjMapping, ApiObjMappingMapper> {
	
}