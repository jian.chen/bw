package com.bw.adv.module.common.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.common.mapper.SqlAttributeMapper;
import com.bw.adv.module.common.model.SqlAttribute;
import com.bw.adv.module.common.repository.SqlAttributeRepository;


@Repository
public class SqlAttributeRepositoryImpl extends BaseRepositoryImpl<SqlAttribute,SqlAttributeMapper> implements SqlAttributeRepository{

	@Override
	protected Class<SqlAttributeMapper> getMapperClass() {
		return SqlAttributeMapper.class;
	}

	@Override
	public List<SqlAttribute> findSqlAttrList(Long type) {
		
		return this.getMapper().selectByTypeId(type);
	}

}