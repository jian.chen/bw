package com.bw.adv.module.common.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.common.mapper.DistinctTypeMapper;
import com.bw.adv.module.common.model.DistinctType;
import com.bw.adv.module.common.repository.DistinctTypeRepository;


@Repository
public class DistinctTypeRepositoryImpl extends BaseRepositoryImpl<DistinctType,DistinctTypeMapper> implements DistinctTypeRepository{

	@Override
	protected Class<DistinctTypeMapper> getMapperClass() {
		return DistinctTypeMapper.class;
	}

}