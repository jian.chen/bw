package com.bw.adv.module.common.model;

import java.util.List;

public class TreeMenuModel extends JsonModel {

    private static final long serialVersionUID = -2877531505650975148L;

    public static final String State_Closed = "closed";
    public static final String State_Open = "open";

    public TreeMenuModel() {
        super();
        this.setStatusSuccess();
    }

    public TreeMenuModel(String message) {
        this(message, false);
    }

    public TreeMenuModel(String message, boolean isSuccess) {
        this.message = message;
        if (isSuccess) {
            this.setStatusSuccess();
        } else {
            this.setStatusError();
        }
    }

    private String id;
    private String pid;
    private String state;
    private String name;
    private Boolean open;
    private String type;

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private List<TreeMenuModel> children;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return this.pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<TreeMenuModel> getChildren() {
        return this.children;
    }

    public void setChildren(List<TreeMenuModel> children) {
        this.children = children;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
