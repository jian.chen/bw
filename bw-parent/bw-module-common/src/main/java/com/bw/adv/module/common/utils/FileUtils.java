package com.bw.adv.module.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bw.adv.module.tools.DateUtils;

public class FileUtils {
	/**
	 * 文件下载公用方法
	 * @param filename 文件名
	 * @param path 文件在服务器上存放的路劲
	 * @param response 
	 * @param request
	 * @return
	 */
	public static void download(String filename, String path, HttpServletResponse response) throws Exception{
		response.reset();
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-disposition","attachment; filename="+new String(filename.getBytes("utf-8"), "iso-8859-1"));
		InputStream fis = null;
		OutputStream toClient = null;
		try {
			fis = new BufferedInputStream(new FileInputStream(path));
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			
			toClient = new BufferedOutputStream(response.getOutputStream());
			toClient.write(buffer);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(fis != null){
					fis.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if(toClient != null){
					toClient.flush();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if(toClient != null){
					toClient.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
	}
	/**
	 * downloadUrl:下载跨域名文件 <br/>
	 * Date: 2016年1月26日 下午2:59:48 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param urlString
	 * @param filename
	 * @param response
	 * @throws Exception
	 */
	public static void downloadUrl(String urlString, String filename, HttpServletResponse response) throws Exception {
		response.reset();
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-disposition","attachment; filename="+new String(filename.getBytes("utf-8"), "iso-8859-1"));
	    // 输入流
		InputStream is = null;
		// 输出的文件流
		OutputStream os = null;
		try {
			// 构造URL
			URL url = new URL(urlString);
			// 打开连接
			URLConnection con = url.openConnection();
			is = con.getInputStream();
			// 1K的数据缓冲
			byte[] bs = new byte[1024];
			// 读取到的数据长度
			int len;
			os = new BufferedOutputStream(response.getOutputStream());
			// 开始读取
			while ((len = is.read(bs)) != -1) {
			  os.write(bs, 0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			// 完毕，关闭所有链接
			try {
				if(os!=null){
					os.flush();
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			try {
				if(os != null){
					os.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if(is != null){
					is.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}   
    public static String upload(MultipartHttpServletRequest multipartRequest) throws IOException{
		String result = "";
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		MultipartFile multipartFile =null;
		String fileName = null;
		//String ctxPath = MyConstants.FILE_ROOT + DateUtils.getCurrentTimeOfDb()+"/";
		String ctxPath = multipartRequest.getSession().getServletContext().getRealPath("/") + "/upload/"; //测试
		 
		File file = new File(ctxPath);
		if(!file.exists()) file.mkdirs();
		for(Map.Entry<String,MultipartFile > set:fileMap.entrySet()){
			multipartFile = set.getValue();//文件名
			fileName = multipartFile.getOriginalFilename();
            File uploadFile = new File(file.getPath() + "/"+fileName);
            try {  
                FileCopyUtils.copy(multipartFile.getBytes(), uploadFile); 
                //result =ctxPath+fileName;
               // result = MyConstants.MAPPING_FOLDER + result.replace(MyConstants.FILE_ROOT, "");
            }catch (Exception e) {
            	e.printStackTrace();
			}
		}
        //return result;
		return "/upload/" + fileName;
    }

    
    public static List<String> uploadImages(HttpServletRequest request) throws IOException{
    	// 返回的多图片路径 
    	List<String> resultLoadList =null;
    	resultLoadList =new ArrayList<String>();
    	
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		MultipartFile multipartFile =null;
		String fileName = null;
		String ctxPath =request.getSession().getServletContext().getRealPath("/")+MyConstants.MAPPING_FOLDER;
		File file = new File(ctxPath);
		if(!file.exists()) file.mkdirs();
		for(Map.Entry<String,MultipartFile > set:fileMap.entrySet()){
			String result ="";
			multipartFile = set.getValue();//文件名
			fileName = multipartFile.getOriginalFilename();
            File uploadFile = new File(ctxPath + "/"+fileName);
            try {   
                FileCopyUtils.copy(multipartFile.getBytes(), uploadFile); 
                result =ctxPath+fileName;
                result = MyConstants.MAPPING_FOLDER+result.replace(ctxPath, "");
                resultLoadList.add(result);
            }catch (Exception e) {
            	e.printStackTrace();
			}
		}
        return resultLoadList;
    }

}
