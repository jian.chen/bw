package com.bw.adv.module.common.api.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.api.model.ApiObj;

public interface ApiObjMapper extends BaseMapper<ApiObj>{
	
}