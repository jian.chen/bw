package com.bw.adv.module.common.utils;

public interface MyConstants {
	
	public static String FILE_ROOT = "D:/projects/";

	public static final String MAPPING_FOLDER = "/upload/";
	
	public abstract class EXT_ACCOUNT_TYPE {
		public static final int ACCOUNT_TYPE_WECHAT = 1;
		public static final int ACCOUNT_TYPE_QQ = 2;
		public static final int ACCOUNT_TYPE_MSN = 3;
		public static final int ACCOUNT_TYPE_SINA = 4;
		public static final int ACCOUNT_TYPE_ONLINE = 5;
		public static final int ACCOUNT_TYPE_CC = 6;
	}
	
	/*
	 *是否默认地址 
	 */
	public abstract class MEMBER_ADDRESS_IS_DEFAULT {
		public static final String YES = "1";
		public static final String NO = "0";
	}
	/*
	 * 分组是否包含全部会员
	 */
	public abstract class IS_ALL {
		public static final Long YES = 1L;
		public static final Long NO = 0L;
	}
	
	/*
	 * 会员分组类别 1.动态 2 静态
	 */
	public abstract class MEMBER_GROUP_TYPE {
		public static final String Dynamic = "1";
		public static final String Static = "2";
	}
	
	/*
	 * 会员标签类别 1.自动 2 手动
	 */
	public abstract class MEMBER_TAG_TYPE {
		public static final String Automatic = "1";
		public static final String Manual = "2";
	}
	
	/**
    public static final String client_credential_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
	
	public abstract class Wechat_Groups_Url {
		public static final String create = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=";
		public static final String get = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=";
		public static final String getid = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=";
		public static final String update = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=";
		public static final String members_update = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=";
		public static final String batchupdate = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=";
		public static final String delete = "https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=";

		public static final String getUsers = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=";
	}
	
	public static final String getBroker = "http://jjr.weizhi.in/Admin/Api/getBroker";
	public static final String getUsers = "http://api.weizhi.in/Admin/Service/getUsers";

	public abstract class Sys_Locked {

		public static final int Yes = 1;

		public static final int No = 2;

		public static final int Default_Value = Yes;

		public static final Map<Integer, String> valueMap = new HashMap<Integer, String>();

		static {
			valueMap.put(Yes, "锁定");
			valueMap.put(No, "未锁定");
		}

		public static String getStringAsValue(Integer value) {

			if (value == null) {
				return valueMap.get(No);
			}
			String result = valueMap.get(value);
			if (StringUtils.isBlank(result)) {
				return valueMap.get(No);
			}
			return result;
		}
	}
 */

	
}
