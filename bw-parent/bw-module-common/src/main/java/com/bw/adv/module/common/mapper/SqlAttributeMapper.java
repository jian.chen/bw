package com.bw.adv.module.common.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.model.SqlAttribute;

public interface SqlAttributeMapper extends BaseMapper<SqlAttribute>{
    int deleteByPrimaryKey(Long sqlAttributeId);

    int insert(SqlAttribute record);

    int insertSelective(SqlAttribute record);

    SqlAttribute selectByPrimaryKey(Long sqlAttributeId);

    int updateByPrimaryKeySelective(SqlAttribute record);

    int updateByPrimaryKey(SqlAttribute record);
    
    List<SqlAttribute> selectByTypeId(Long sqlAttributeTypeId);
}