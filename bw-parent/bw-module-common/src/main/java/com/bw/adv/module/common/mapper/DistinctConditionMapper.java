package com.bw.adv.module.common.mapper;

import com.bw.adv.module.common.model.DistinctCondition;

public interface DistinctConditionMapper {
    int deleteByPrimaryKey(Long distinctConditionId);

    int insert(DistinctCondition record);

    int insertSelective(DistinctCondition record);

    DistinctCondition selectByPrimaryKey(Long distinctConditionId);

    int updateByPrimaryKeySelective(DistinctCondition record);

    int updateByPrimaryKey(DistinctCondition record);
}