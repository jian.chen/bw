package com.bw.adv.module.common.init;



import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.common.api.model.ApiObj;
import com.bw.adv.module.common.api.model.ApiObjMapping;
import com.bw.adv.module.common.api.repository.ApiObjMappingRepository;
import com.bw.adv.module.common.api.repository.ApiObjRepository;
import com.bw.adv.module.tools.ReflectionUtils;

public class ApiObjMappingInit {

	private static final Logger logger = LoggerFactory.getLogger(ApiObjMappingInit.class);
	private static final Map<String, List<String>> INIT_DATA_MAP = new HashMap<String, List<String>>();

	private static final String MAPPING_WAY_INCLUDE = "1";
	private static final String MAPPING_WAY_EXCLUDE = "2";

	private ApiObjRepository apiObjRepository;
	private ApiObjMappingRepository apiObjMappingRepository;

	@PostConstruct
	public void init() {
		Map<Long, String> apiObjMap = new HashMap<Long, String>();
		List<ApiObj> apiObjs = apiObjRepository.findAll();
		List<ApiObjMapping> apiObjMappings = apiObjMappingRepository.findAll();
		
		for (ApiObj apiObj : apiObjs) {
			apiObjMap.put(apiObj.getApiObjId(), apiObj.getApiObjClass());
		}
		List<String> fieldNameList = null;
		String objClass = null;
		String key = null;
		for (ApiObjMapping apiObjMapping : apiObjMappings) {
			objClass = apiObjMap.get(apiObjMapping.getApiObjId());
			key = objClass + "-" + apiObjMapping.getExtSystemId();
			fieldNameList = INIT_DATA_MAP.get(objClass);
			if (fieldNameList == null) {
				fieldNameList = new ArrayList<String>();
				INIT_DATA_MAP.put(key, fieldNameList);
			}
			String mappingWay = apiObjMapping.getMappingWay();
			String[] fieldNames = apiObjMapping.getFields().split(",");
			//包含方式
			if(MAPPING_WAY_INCLUDE.equals(mappingWay)){
				fieldNameList.addAll(Arrays.asList(fieldNames));
			}
			//排除方式
			if(MAPPING_WAY_EXCLUDE.equals(mappingWay)){
				try {
					Class<?> cls = Class.forName(objClass);
					List<Field> fieldList = ReflectionUtils.getFieldList(cls, true);
					for(Field field : fieldList){
						fieldNameList.add(field.getName());
					}
					fieldNameList.removeAll(Arrays.asList(fieldNames));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					
				}
			}
		}
		logger.info("api返回值映射初始化完成......");

	}
	
	
	public static List<String> getApiObjFields(Class<?> cls, Long extSystemId){
		return INIT_DATA_MAP.get(cls.getName() + "-" + extSystemId);
	}


	@Autowired
	public void setApiObjRepository(ApiObjRepository apiObjRepository) {
		this.apiObjRepository = apiObjRepository;
	}

	
	@Autowired
	public void setApiObjMappingRepository(ApiObjMappingRepository apiObjMappingRepository) {
		this.apiObjMappingRepository = apiObjMappingRepository;
	}

}
