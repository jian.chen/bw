package com.bw.adv.module.common.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.model.DistinctType;

public interface DistinctTypeMapper extends BaseMapper<DistinctType> {
	
}