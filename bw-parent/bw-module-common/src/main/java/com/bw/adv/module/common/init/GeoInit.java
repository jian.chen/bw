/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-common
 * File Name:GeoInit.java
 * Package Name:com.sage.scrm.module.common.init
 * Date:2015年8月18日下午3:15:03
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.common.init;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.base.model.Geo;
import com.bw.adv.module.base.model.GeoAssoc;
import com.bw.adv.module.base.repository.GeoAssocRepository;
import com.bw.adv.module.base.repository.GeoRepository;
import com.bw.adv.module.common.constant.GeoTypeConstant;

/**
 * ClassName:GeoInit <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月18日 下午3:15:03 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public class GeoInit {
	public static final List<Geo> GEO_COUNTRY_LIST = new ArrayList<Geo>();
	public static final List<Geo> GEO_STATE_LIST = new ArrayList<Geo>();
    public static final List<Geo> GEO_PROVICE_LIST = new ArrayList<Geo>();//省(包括直辖市)
    public static final List<Geo> GEO_CITY_LIST = new ArrayList<Geo>();//市
    public static final List<Geo> GEO_COUNTY_CITY_LIST = new ArrayList<Geo>();//县&&行政区
    public static final List<Geo> GEO_REGION_LIST = new ArrayList<Geo>();
    public static final List<Geo> GEO_MARKET_TERRITORY_LIST = new ArrayList<Geo>();
    public static final List<GeoAssoc> GEO_ASSOC_LIST = new ArrayList<GeoAssoc>();
    public static final Map<Long,Geo> GEO_MAP = new HashMap<Long,Geo>();
    
    private GeoRepository geoRepository;
    private GeoAssocRepository geoAssocRepository;
    
    @PostConstruct
    private void init(){
    	List<GeoAssoc> geoAssocList = null;
    	List<Geo> geoList = null;
    	Geo geoTemp = null;
    	
    	geoAssocList = geoAssocRepository.findAll();
    	geoList = geoRepository.findAll();
    	for(int i=0;i<geoList.size();i++){
    		
    		geoTemp = geoList.get(i);
    		GEO_MAP.put(geoTemp.getGeoId(), geoTemp);
    		if(geoTemp.getGeoTypeId() == GeoTypeConstant.COUNTRY.getId()){
    			GEO_COUNTRY_LIST.add(geoTemp);
    		}else if(geoTemp.getGeoTypeId() == GeoTypeConstant.STATE.getId()){
    			GEO_STATE_LIST.add(geoTemp);
    		}else if(geoTemp.getGeoTypeId() == GeoTypeConstant.PROVINCE.getId()){
    			GEO_PROVICE_LIST.add(geoTemp);
    		}else if(geoTemp.getGeoTypeId() == GeoTypeConstant.CITY.getId()){
    			GEO_CITY_LIST.add(geoTemp);
    		}else if(geoTemp.getGeoTypeId() == GeoTypeConstant.COUNTY_CITY.getId()){
    			GEO_COUNTY_CITY_LIST.add(geoTemp);
    		}else if(geoTemp.getGeoTypeId() == GeoTypeConstant.REGION.getId()){
    			GEO_REGION_LIST.add(geoTemp);
    		}else if(geoTemp.getGeoTypeId() == GeoTypeConstant.MARKET_TERRITORY.getId()){
    			GEO_MARKET_TERRITORY_LIST.add(geoTemp);
    		}
    	}
    	for(int j=0;j<geoAssocList.size();j++){
    		GEO_ASSOC_LIST.add(geoAssocList.get(j));
    	}
    	//System.out.println(""+GEO_COUNTRY_LIST.get(0).getGeoName());

    }

    
    public static Geo getGeoByGeoId(Long geoId){
    	return GEO_MAP.get(geoId);
    }
    
    public static Geo getParentGeoByGeoId(Long geoId){
    	Geo geo = null;
    	Long temp = null;
    	
    	for(int i=0;i<GEO_ASSOC_LIST.size();i++){
    		temp = GEO_ASSOC_LIST.get(i).getToGeoId();
    		if(temp.equals(geoId)){
    			geo = GEO_MAP.get(GEO_ASSOC_LIST.get(i).getGeoId());
    			break;
    		}
    	}
    	
    	return geo;
    }
    
    
    @Autowired
	public void setGeoRepository(GeoRepository geoRepository) {
		this.geoRepository = geoRepository;
	}

    @Autowired
	public void setGeoAssocRepository(GeoAssocRepository geoAssocRepository) {
		this.geoAssocRepository = geoAssocRepository;
	}
    
    
}

