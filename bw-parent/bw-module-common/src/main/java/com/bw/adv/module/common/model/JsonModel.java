package com.bw.adv.module.common.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;


public class JsonModel implements Serializable {

	private static final long serialVersionUID = 1219760989503992746L;

	public JsonModel() {
	}
	public JsonModel(int status, String message) {
		this.status = status;
		this.message = message;
	}
	
	public static final int   Status_Success   = 200;
	public static final int   Status_Error     = 300;
	public static final int   Status_TimeOut   = 301;
	public static final int	  Status_Unauthorized = 401;

	protected int             status           = Status_Error;

	protected String          message;
	
	protected Object          obj;

	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	public void setStatusSuccess() {
		this.status = Status_Success;
	}

	public void setStatusError() {
		this.status = Status_Error;
	}

	public void setStatusTimeOut() {
		this.status = Status_TimeOut;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatus() {
		return this.status;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
