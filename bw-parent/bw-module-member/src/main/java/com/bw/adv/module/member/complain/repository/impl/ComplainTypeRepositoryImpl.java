/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:ComplainTypeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.complain.repository.impl
 * Date:2015年12月29日下午5:01:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.complain.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.complain.mapper.ComplainTypeMapper;
import com.bw.adv.module.member.complain.model.ComplainType;
import com.bw.adv.module.member.complain.repository.ComplainTypeRepository;

/**
 * ClassName:ComplainTypeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月29日 下午5:01:29 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ComplainTypeRepositoryImpl extends BaseRepositoryImpl<ComplainType, ComplainTypeMapper> implements ComplainTypeRepository{

	@Override
	protected Class<ComplainTypeMapper> getMapperClass() {
		return ComplainTypeMapper.class;
	}

	@Override
	public List<ComplainType> findAllComplainType() {
		return this.getMapper().selectAllComplainType();
	}

}

