package com.bw.adv.module.member.equity.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.EquityGradeRelation;

public interface EquityGradeRelationMapper extends BaseMapper<EquityGradeRelation> {
   
	List<EquityGradeRelation> selectByEquityId(Long equityId);
	
	void deleteByEquityId(Long equityid);
}