

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberTagItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.tag.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.tag.mapper.MemberTagItemMapper;
import com.bw.adv.module.member.tag.model.MemberTagItem;
import com.bw.adv.module.member.tag.repository.MemberTagItemRepository;
import com.bw.adv.module.member.tag.search.MemberTagItemSearch;

/**
 * ClassName:MemberTagItemRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberTagItemRepositoryImpl extends BaseRepositoryImpl<MemberTagItem, MemberTagItemMapper> implements MemberTagItemRepository{
	
	@Override
	protected Class<MemberTagItemMapper> getMapperClass() {
		return MemberTagItemMapper.class;
	}

	@Override
	public Long findCountSearchAuto(MemberTagItemSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<MemberTagItem> findListSearchAuto(MemberTagItemSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public MemberTagItem findEntitySearchAuto(MemberTagItemSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}

	@Override
	public void deleteTagItemByTagId(Long memberTagId) {
		
		this.getMapper().deleteTagItemByTagId(memberTagId);
		
	}
	
	@Override
	public void deleteTagItemByMemberId(Long memberTagId) {
		
		this.getMapper().deleteTagItemByMemberId(memberTagId);
		
	}

	@Override
	public void insertBatchMemberTagItem(List<MemberTagItem> addItemList) {
		
		this.getMapper().insertBatchMemberTagItem(addItemList);
	}

	@Override
	public void insertMemberTagItemIgnoreSame(List<MemberTagItem> addItemList) {
		this.getMapper().insertMemberTagItemIgnoreSame(addItemList);
	}

	
}