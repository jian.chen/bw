package com.bw.adv.module.member.mapper;

import com.bw.adv.module.member.model.LossReport;

public interface LossReportMapper {
    int deleteByPrimaryKey(Long lossReportId);

    int insert(LossReport record);

    int insertSelective(LossReport record);

    LossReport selectByPrimaryKey(Long lossReportId);

    int updateByPrimaryKeySelective(LossReport record);

    int updateByPrimaryKey(LossReport record);
}