/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:EquityGradeRelationRepository.java
 * Package Name:com.sage.scrm.module.member.equity.repository
 * Date:2016年1月14日下午9:54:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.equity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.equity.mapper.EquityGradeRelationMapper;
import com.bw.adv.module.member.model.EquityGradeRelation;

/**
 * ClassName:EquityGradeRelationRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 下午9:54:28 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
public interface EquityGradeRelationRepository extends BaseRepository<EquityGradeRelation, EquityGradeRelationMapper>{
   
	List<EquityGradeRelation> findGradeInfoById(Long equityId);
	
	void deleteByEquityId(Long equityId);
}

