/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberEquitySearch.java
 * Package Name:com.sage.scrm.module.member.equity.search
 * Date:2016年1月14日上午11:09:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.equity.search;

import com.bw.adv.module.member.BaseSearch;

/**
 * ClassName:MemberEquitySearch <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 上午11:09:40 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
public class MemberEquitySearch extends BaseSearch {

	private static final long serialVersionUID = 1L;

	public MemberEquitySearch() {
    	 super(Default_Rows, Default_Page);
	 }

	private String likeMemberEquityName;
	
	private String equalStatus;
	
	private String notEqualStatus;

	public String getLikeMemberEquityName() {
		return likeMemberEquityName;
	}

	public void setLikeMemberEquityName(String likeMemberEquityName) {
		this.likeMemberEquityName = likeMemberEquityName;
	}

	public String getEqualStatus() {
		return equalStatus;
	}

	public void setEqualStatus(String equalStatus) {
		this.equalStatus = equalStatus;
	}

	public String getNotEqualStatus() {
		return notEqualStatus;
	}

	public void setNotEqualStatus(String notEqualStatus) {
		this.notEqualStatus = notEqualStatus;
	}
	 
}

