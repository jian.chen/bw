package com.bw.adv.module.member.group.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.group.model.MemberGroupItem;
import com.bw.adv.module.member.group.search.MemberGroupItemSearch;


public interface MemberGroupItemMapper extends BaseMapper<MemberGroupItem>{
	 Long selectCountSearchAuto(@Param("MemberGroupItemSearch")MemberGroupItemSearch search);

   	List<MemberGroupItem> selectListSearchAuto(@Param("MemberGroupItemSearch")MemberGroupItemSearch search);
   	
   	MemberGroupItem selectEntitySearchAuto(@Param("MemberGroupItemSearch")MemberGroupItemSearch search);
   	
	void deleteGroupItemByGroupId(Long memberGroupId);
	
	void deleteGroupItemByMemberId(Long memberId);

	void insertBatchMemberGroupItem(@Param("list")List<MemberGroupItem> addItemList);

}