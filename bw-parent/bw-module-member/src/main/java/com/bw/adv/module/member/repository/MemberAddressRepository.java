
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberAddressRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.mapper.MemberAddressMapper;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.search.MemberAddressSearch;

/**
 * ClassName:MemberAddressRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:51:06 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public interface MemberAddressRepository extends BaseRepository<MemberAddress, MemberAddressMapper>{
	
	Long findCountSearchAuto(MemberAddressSearch search);
	
	List<MemberAddress> findListSearchAuto(MemberAddressSearch search);
	
	MemberAddress findEntitySearchAuto(MemberAddressSearch search);
	
	MemberAddress findMemberAddressDetail(Long memberAddressId);
	
	/**
	 * 
	 * updateStatusByMemberId:(根据会员ID,修改此会员所有的地址为非默认). <br/>
	 * Date: 2015年12月15日 上午10:52:52 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberId
	 */
	void updateStatusByMemberId(Long memberId,String isDefault);
	
	/**
	 * 
	 * findMemberDefault:(查询会员默认地址). <br/>
	 * Date: 2015年12月15日 下午12:00:56 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<MemberAddress> findMemberDefault(Long memberId);
	
	/**
	 * 
	 * findMemberAddressByMemberId:(根据memberId查询用户受奖地址). <br/>
	 * Date: 2015年12月17日 下午2:06:46 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberAddress findMemberAddressByMemberId(Long memberId);
}

