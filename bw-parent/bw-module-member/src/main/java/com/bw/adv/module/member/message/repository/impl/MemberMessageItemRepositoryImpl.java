

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.message.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.message.mapper.MemberMessageItemMapper;
import com.bw.adv.module.member.message.model.MemberMessageItem;
import com.bw.adv.module.member.message.model.NotReadMessage;
import com.bw.adv.module.member.message.repository.MemberMessageItemRepository;

/**
 * ClassName: MemberMessageRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年2月22日 下午2:50:39 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class MemberMessageItemRepositoryImpl extends BaseRepositoryImpl<MemberMessageItem, MemberMessageItemMapper> implements MemberMessageItemRepository{
	
	@Override
	protected Class<MemberMessageItemMapper> getMapperClass() {
		return MemberMessageItemMapper.class;
	}

	@Override
	public void deleteByMessageId(Long memberMessageId) {
		
		this.getMapper().deleteByMessageId(memberMessageId);
	}
	
	@Override
	public MemberMessageItem findByMessageId(Example example) {
		
		return this.getMapper().selectByMessageId(example);
	}

	@Override
	public NotReadMessage findByMemberId(Long memberId) {
		
		return this.getMapper().selectNotReadMessage(memberId);
	}

}