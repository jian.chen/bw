

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.group.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.group.mapper.MemberGroupMapper;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.repository.MemberGroupRepository;
import com.bw.adv.module.member.group.search.MemberGroupSearch;

/**
 * ClassName:MemberGroupRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberGroupRepositoryImpl extends BaseRepositoryImpl<MemberGroup, MemberGroupMapper> implements MemberGroupRepository{
	
	@Override
	protected Class<MemberGroupMapper> getMapperClass() {
		return MemberGroupMapper.class;
	}

	@Override
	public Long findCountSearchAuto(MemberGroupSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<MemberGroup> findListSearchAuto(MemberGroupSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public MemberGroup findEntitySearchAuto(MemberGroupSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}
}