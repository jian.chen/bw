/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberAddressRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.mapper.MemberAddressMapper;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.repository.MemberAddressRepository;
import com.bw.adv.module.member.search.MemberAddressSearch;

/**
 * ClassName:MemberAddressRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberAddressRepositoryImpl extends BaseRepositoryImpl<MemberAddress, MemberAddressMapper> implements MemberAddressRepository{
	
	@Override
	protected Class<MemberAddressMapper> getMapperClass() {
		return MemberAddressMapper.class;
	}

	@Override
	public Long findCountSearchAuto(MemberAddressSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<MemberAddress> findListSearchAuto(MemberAddressSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public MemberAddress findEntitySearchAuto(MemberAddressSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}

	@Override
	public MemberAddress findMemberAddressDetail(Long memberAddressId) {
		
		return this.getMapper().selectMemberAddressDetail(memberAddressId);
	}

	@Override
	public void updateStatusByMemberId(Long memberId,String isDefault) {
		
		this.getMapper().updateStatusByMemberId(memberId,isDefault);
	}

	@Override
	public List<MemberAddress> findMemberDefault(Long memberId) {

		return this.getMapper().selectMemberDefault(memberId);
	}

	@Override
	public MemberAddress findMemberAddressByMemberId(Long memberId) {
		return this.getMapper().selectMemberAddressByMemberId(memberId);
	}

}