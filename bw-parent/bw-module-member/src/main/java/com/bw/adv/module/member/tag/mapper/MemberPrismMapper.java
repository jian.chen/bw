package com.bw.adv.module.member.tag.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberPrism;

public interface MemberPrismMapper extends BaseMapper<MemberPrism>{
    int deleteByPrimaryKey(Long prismId);

    int insert(MemberPrism record);

    int insertSelective(MemberPrism record);

    MemberPrism selectByPrimaryKey(Long prismId);

    int updateByPrimaryKeySelective(MemberPrism record);

    int updateByPrimaryKey(MemberPrism record);
    
    /**
	 * 查询所有棱镜
	 * @param templatePage
	 * @return
	 * @author cuicd
	 */
	List<MemberPrism> selectAllPrism(Page<MemberPrism> templatePage);

}