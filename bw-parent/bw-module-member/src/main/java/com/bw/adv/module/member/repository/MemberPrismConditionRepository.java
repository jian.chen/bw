package com.bw.adv.module.member.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.model.MemberPrismCondition;
import com.bw.adv.module.member.tag.mapper.MemberPrismConditionMapper;

public interface MemberPrismConditionRepository extends BaseRepository<MemberPrismCondition, MemberPrismConditionMapper>{

}
