package com.bw.adv.module.member.tag.search;

import com.bw.adv.module.member.BaseSearch;


public class MemberTagItemSearch extends BaseSearch {

     public MemberTagItemSearch() {
		super(Default_Rows, Default_Page);
	}
	
	

	 private Long equalMemberTagItemId;
	 private Long equalMemberTagId;
	 private Long equalMemberId;
    

	public Long getEqualMemberTagItemId() {
		return equalMemberTagItemId;
	}

	public void setEqualMemberTagItemId(Long equalMemberTagItemId) {
		this.equalMemberTagItemId = equalMemberTagItemId;
	}
    

	public Long getEqualMemberTagId() {
		return equalMemberTagId;
	}

	public void setEqualMemberTagId(Long equalMemberTagId) {
		this.equalMemberTagId = equalMemberTagId;
	}
    

	public Long getEqualMemberId() {
		return equalMemberId;
	}

	public void setEqualMemberId(Long equalMemberId) {
		this.equalMemberId = equalMemberId;
	}
}
