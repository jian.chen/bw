
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.message.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.message.mapper.MemberMessageItemBrowseMapper;
import com.bw.adv.module.member.message.model.MemberMessageItemBrowse;


/**
 * ClassName: MemberMessageItemBrowseRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年3月4日 上午11:31:27 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface MemberMessageItemBrowseRepository extends BaseRepository<MemberMessageItemBrowse, MemberMessageItemBrowseMapper>{
	
}

