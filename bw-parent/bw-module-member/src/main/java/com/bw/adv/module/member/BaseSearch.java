/*
 * 作者		www.TheWk.cn.vc
 * 开发环境	Windows7 64位 MyEclipse8.6 JDK1.6.0_37
 * 开发日期	2013-8-10
 */
package com.bw.adv.module.member;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * <hr/>
 * 
 * @author www.TheWk.cn.vc
 * @version 1.0 2013-8-10
 * @class base.BaseSearch
 */ 
public abstract class BaseSearch implements Serializable {

	private static final long         serialVersionUID    = -5355781984903626639L;

	protected static final DateFormat yyyyMMdd_DF         = new SimpleDateFormat("yyyy-MM-dd");
	protected static final DateFormat yyyyMMddHHmmss_DF   = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static final String        Order_Type_Asc      = "asc";
	public static final String        Order_Type_Desc     = "desc";

	public static final Integer       YES                 = 1;
	public static final Integer       NO                  = null;

	protected static final int        Default_Rows        = Integer.MAX_VALUE;
	protected static final int        Default_Page        = 1;
	protected static final String     Default_Order       = Order_Type_Asc;
	protected static final String     Default_Sort        = "id";
	protected static final int        Default_Switch_Flag = 0;

	public static final int       IsNull_No           = 2;
	public static final int       IsNull_Yes          = 1;

	public static final int       IsNotNull_No        = 2;
	public static final int       IsNotNull_Yes       = 1;

	public static final int       query_no            = 0;
	public static final int       query_yes           = 1;
	public BaseSearch(int pageSize, int currentPage) {
		this.rows = pageSize;
		this.page = currentPage;
	}

	// 每页条数
	protected int    rows          = Default_Rows;
	// 当前页码
	protected int    page          = Default_Page;
	// 排序字段
	protected String sort          = null;
	// 排序类型
	protected String order         = null;
	/**
	 * 是否随机排序,1是,2否
	 */
	protected Integer    isRandomOrder = NO;
	/**
	 * 是否去除重复,1是,2否
	 */
	protected Integer    isDistinct    = NO;

	protected int    switchFlag    = Default_Switch_Flag;

	public Integer getIsRandomOrder() {
		return this.isRandomOrder;
	}

	public void setIsRandomOrder(Integer isRandomOrder) {
		this.isRandomOrder = isRandomOrder;
	}

	public Integer getIsDistinct() {
		return this.isDistinct;
	}

	public void setIsDistinct(Integer isDistinct) {
		this.isDistinct = isDistinct;
	}

	public int getSwitchFlag() {
		return this.switchFlag;
	}

	public void setSwitchFlag(int switchFlag) {
		this.switchFlag = switchFlag;
	}

	public int getBegin() {
		return (this.page - 1) * this.rows;
	}

	public int getEnd() {
		return this.page * this.rows;
	}

	public int getRows() {
		return this.rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getSort() {
		return this.sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return this.order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getDateFormatStringyyyyMMdd(Date date) {
		if (date != null) {
			return yyyyMMdd_DF.format(date);
		}
		return null;
	}

	public String getDateFormatStringyyyyMMddHHmmss(Date date) {
		if (date != null) {
			return yyyyMMddHHmmss_DF.format(date);
		}
		return null;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		return EqualsBuilder.reflectionEquals(obj, this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

}
