package com.bw.adv.module.member.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.exp.MemberCardExp;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberCardMapper extends BaseMapper<MemberCard>{
	
	List<MemberCard> selectByMemberId(@Param("memberId")Long memberId);

	
	/**
	 * selectByCardNo:(根据会员卡号查询会员卡). <br/>
	 * Date: 2015-11-16 下午5:44:28 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param cardNo
	 * @return
	 */
	MemberCard selectByCardNo(@Param("cardNo")String cardNo);


	/**
	 * selectExpByMemberId:(根据会员ID查询会员卡扩展信息). <br/>
	 * Date: 2015-12-17 上午10:25:47 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberCardExp selectExpByMemberId(Long memberId);

	int deleteCardByMemberId(Long memberId);
}