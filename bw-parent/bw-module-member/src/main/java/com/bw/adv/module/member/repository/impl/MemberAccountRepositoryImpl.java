

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberAccountRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.mapper.MemberAccountMapper;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.repository.MemberAccountRepository;
import com.bw.adv.module.member.search.MemberAccountSearch;

import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * ClassName:MemberAccountRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberAccountRepositoryImpl extends BaseRepositoryImpl<MemberAccount, MemberAccountMapper> implements MemberAccountRepository{
	
	@Override
	protected Class<MemberAccountMapper> getMapperClass() {
		return MemberAccountMapper.class;
	}

	@Override
	public Long findCountSearchAuto(MemberAccountSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<MemberAccount> findListSearchAuto(MemberAccountSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public MemberAccount findEntitySearchAuto(MemberAccountSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}

	@Override
	public MemberAccount findByMemberId(Long memberId) {
		List<MemberAccount> list = null;
		list = this.getMapper().selectByMemberId(memberId);
		if(list == null || list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}
	
	@Override
	public MemberAccount findByMemberIdForUpdate(Long memberId) {
		List<MemberAccount> list = null;
		list = this.getMapper().selectByMemberIdForUpdate(memberId);
		if(list == null || list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}

	/**
	 * TODO 积分清理，更新账户积分余额（可选）.
	 * @see com.bw.adv.module.member.repository.MemberAccountRepository#updateBatchByCleanPointsBalance(java.util.List)
	 */
	@Override
	public int updateBatchByCleanPointsBalance(List<MemberAccount> memberAccountList) {
		return this.getMapper().updateBatchByCleanPointsBalance(memberAccountList);
	}

	@Override
	public Long findMemberOrderCount(Long times1,Long times2,Long sort) {
		return this.getMapper().selectCountMemberOrder(times1,times2,sort);
	}

	@Override
	public int updateForAddPoints(Long memberId, BigDecimal points,String currentTime) {
		return this.getMapper().updateForAddPoints(memberId,points,currentTime);
	}

	@Override
	public int updateForSubtractPoints(Long memberId, BigDecimal points,String currentTime) {
		return this.getMapper().updateForSubtractPoints(memberId,points,currentTime);
	}
	
	@Override
	public int updateForSubtractPointsWithCancelOrder(Long memberId, BigDecimal points,String currentTime) {
		return this.getMapper().updateForSubtractPointsWithCancelOrder(memberId,points,currentTime);
	}

	
}