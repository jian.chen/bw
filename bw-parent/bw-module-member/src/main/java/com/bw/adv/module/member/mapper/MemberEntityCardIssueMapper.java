package com.bw.adv.module.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberEntityCardIssue;

public interface MemberEntityCardIssueMapper extends BaseMapper<MemberEntityCardIssue>{
    int deleteByPrimaryKey(Long issueId);

    int insert(MemberEntityCardIssue record);

    int insertSelective(MemberEntityCardIssue record);

    MemberEntityCardIssue selectByPrimaryKey(Long issueId);

    int updateByPrimaryKeySelective(MemberEntityCardIssue record);

    int updateByPrimaryKey(MemberEntityCardIssue record);
    
    List<MemberEntityCardIssue> selectMemeberCardByBatch(@Param("batch") String batch,Page<MemberEntityCardIssue> pageObj);
    
    int insertMemberCardGetId(MemberEntityCardIssue record);
}