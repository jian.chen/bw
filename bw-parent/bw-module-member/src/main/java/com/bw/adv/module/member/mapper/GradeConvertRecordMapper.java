package com.bw.adv.module.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.GradeConvertRecord;
import com.bw.adv.module.member.model.exp.GradeConvertRecordExp;
import com.bw.adv.module.member.search.GradeConvertRecordSearch;


public interface GradeConvertRecordMapper extends BaseMapper<GradeConvertRecord>{
	
    Long selectCountSearchAuto(@Param("GradeConvertRecordSearch")GradeConvertRecordSearch search);

	List<GradeConvertRecordExp> selectListSearchAuto(@Param("GradeConvertRecordSearch")GradeConvertRecordSearch search);
	
	GradeConvertRecord selectEntitySearchAuto(@Param("GradeConvertRecordSearch")GradeConvertRecordSearch search);
}