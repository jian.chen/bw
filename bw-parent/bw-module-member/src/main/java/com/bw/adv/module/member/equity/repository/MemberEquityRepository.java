/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberEquityRepository.java
 * Package Name:com.sage.scrm.module.member.equity.repository
 * Date:2016年1月14日上午10:59:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.equity.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.equity.mapper.MemberEquityMapper;
import com.bw.adv.module.member.equity.search.MemberEquitySearch;
import com.bw.adv.module.member.model.MemberEquity;
import com.bw.adv.module.member.model.exp.MemberEquityExp;

/**
 * ClassName:MemberEquityRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 上午10:59:28 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberEquityRepository extends BaseRepository<MemberEquity, MemberEquityMapper> {
   
	Long findCountSearchAuto(MemberEquitySearch search);
	
	List<MemberEquityExp> findListSearchAuto(MemberEquitySearch search);
	
	MemberEquity findInfoById(Long memberEquityid);
	
	int updateMemberEquity(MemberEquity memberEquity);
	
	int insertMemberEquity(MemberEquity memberEquity);
	
	int removeMemberEquity(MemberEquity memberEquity);
	
	List<MemberEquity> selectListByGradeId(Long gradeId,String nowDate);
}

