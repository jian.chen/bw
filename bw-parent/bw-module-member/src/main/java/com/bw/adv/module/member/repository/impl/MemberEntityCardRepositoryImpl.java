package com.bw.adv.module.member.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.mapper.MemberEntityCardMapper;
import com.bw.adv.module.member.model.MemberEntityCard;
import com.bw.adv.module.member.repository.MemberEntityCardRepository;

@Repository
public class MemberEntityCardRepositoryImpl extends BaseRepositoryImpl<MemberEntityCard, MemberEntityCardMapper> implements MemberEntityCardRepository {
	
	@Override
	protected Class<MemberEntityCardMapper> getMapperClass() {
		return MemberEntityCardMapper.class;
	}

	@Override
	public List<MemberEntityCard> selectMemberCardByIssueId(Long issueId,Page<MemberEntityCard> pageObj) {
		return this.getMapper().selectMemberCardByIssueId(issueId,pageObj);
	}
	public MemberEntityCard findCardByCardNo(String cardNo, Long statusId) {
		return this.getMapper().findCardByCardNo(cardNo, statusId);
	}

}
