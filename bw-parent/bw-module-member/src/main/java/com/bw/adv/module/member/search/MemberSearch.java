/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberSearch.java
 * Package Name:com.sage.scrm.module.member.model
 * Date:2015年8月10日下午4:52:27
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.search;

import java.math.BigDecimal;

import com.bw.adv.module.member.BaseSearch;

/**
 * ClassName:MemberSearch <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月10日 下午4:52:27 <br/>
 * scrmVersion 1.0
 * 
 * @author simon
 * @version jdk1.7
 * @see
 */
public class MemberSearch extends BaseSearch {

	public MemberSearch() {
		super(Default_Rows, Default_Page);
	}

	private Long memberGroupId;
	private String memberIds;

	private String memberGroupIds;
	private String memberTagIds;

	// 积分余额
	private Long greaterEqualPointsBalance;
	private Long lessEqualPointsBalance;
	// 积分发生时间
	private String happenTime;

	// 订单编号
	private String orderDate;

	// 订单日期
	private BigDecimal orderAmount;

	// 订单金额(优惠前)
	private BigDecimal payAmount;

	// 实际付款额(订单金额-优惠金额)
	private String orderCode;

	// 优惠劵使用时间
	private String usedTime;
	// 优惠劵名称
	private String couponName;
	// 优惠劵编码
	private String couponInstanceCode;

	// 状态
	private Long statusId;

	// 门店代码
	private String equalOrgCode;

	private String equalCardNo;

	public String getEqualCardNo() {
		return equalCardNo;
	}

	public void setEqualCardNo(String equalCardNo) {
		this.equalCardNo = equalCardNo;
	}

	private String[] inMemberGroupIds;
	private String[] inMemberTagIds;
	private Long equalMemberId;
	private Long equalChannelId;
	private String equalOrgId;
	private String[] inOrgId;
	private Long equalGeoId;

	public String[] getInOrgId() {
		return inOrgId;
	}

	public void setInOrgId(String[] inOrgId) {
		this.inOrgId = inOrgId;
	}

	private Long equalGradeId;

	private String equalMemberCode;
	private String likeMemberCode;
	private String greaterEqualMemberCode;
	private String lessEqualMemberCode;

	private String equalEmail;
	private String likeEmail;
	private String greaterEqualEmail;
	private String lessEqualEmail;

	private String equalEmailCheck;
	private String likeEmailCheck;
	private String greaterEqualEmailCheck;
	private String lessEqualEmailCheck;

	private String equalMobile;
	private String likeMobile;
	private String greaterEqualMobile;
	private String lessEqualMobile;

	private String equalMobileCheck;
	private String likeMobileCheck;
	private String greaterEqualMobileCheck;
	private String lessEqualMobileCheck;

	private String equalPerfectCheck;
	private String likePerfectCheck;
	private String greaterEqualPerfectCheck;
	private String lessEqualPerfectCheck;

	private Long equalPerfectPercentage;

	private String equalLoginPassword;
	private String likeLoginPassword;
	private String greaterEqualLoginPassword;
	private String lessEqualLoginPassword;

	private String equalMemberName;
	private String likeMemberName;
	private String greaterEqualMemberName;
	private String lessEqualMemberName;

	private Long equalGender;

	private String equalBirthday;
	private String likeBirthday;
	private String greaterEqualBirthday;
	private String lessEqualBirthday;

	private String equalRegisterTime;
	private String likeRegisterTime;
	private String greaterEqualRegisterTime;
	private String lessEqualRegisterTime;

	private String equalRegisterIp;
	private String likeRegisterIp;
	private String greaterEqualRegisterIp;
	private String lessEqualRegisterIp;

	private String equalLastLoginTime;
	private String likeLastLoginTime;
	private String greaterEqualLastLoginTime;
	private String lessEqualLastLoginTime;

	private String equalLastLoginIp;
	private String likeLastLoginIp;
	private String greaterEqualLastLoginIp;
	private String lessEqualLastLoginIp;

	private Long equalLastLoginCity;

	private String equalLastConsumptionTime;
	private String likeLastConsumptionTime;
	private String greaterEqualLastConsumptionTime;
	private String lessEqualLastConsumptionTime;

	private Long equalLastConsumptionCity;
	private Long equalStatusId;
	private Long equalActiveStatusId;
	private Long equalConsumptionStatusId;
	
	private String notEqualIsEmployee;
	
	private String equalIsEmployee;
	
	public Long getEqualMemberId() {
		return equalMemberId;
	}

	public void setEqualMemberId(Long equalMemberId) {
		this.equalMemberId = equalMemberId;
	}

	public Long getEqualChannelId() {
		return equalChannelId;
	}

	public void setEqualChannelId(Long equalChannelId) {
		this.equalChannelId = equalChannelId;
	}

	public String getEqualOrgId() {
		return equalOrgId;
	}

	public void setEqualOrgId(String equalOrgId) {
		this.equalOrgId = equalOrgId;
	}

	public Long getEqualGeoId() {
		return equalGeoId;
	}

	public void setEqualGeoId(Long equalGeoId) {
		this.equalGeoId = equalGeoId;
	}

	public Long getEqualGradeId() {
		return equalGradeId;
	}

	public void setEqualGradeId(Long equalGradeId) {
		this.equalGradeId = equalGradeId;
	}

	public String getEqualMemberCode() {
		return equalMemberCode;
	}

	public void setEqualMemberCode(String equalMemberCode) {
		this.equalMemberCode = equalMemberCode;
	}

	public String getLikeMemberCode() {
		return likeMemberCode;
	}

	public void setLikeMemberCode(String likeMemberCode) {
		this.likeMemberCode = likeMemberCode;
	}

	public String getGreaterEqualMemberCode() {
		return greaterEqualMemberCode;
	}

	public void setGreaterEqualMemberCode(String greaterEqualMemberCode) {
		this.greaterEqualMemberCode = greaterEqualMemberCode;
	}

	public String getLessEqualMemberCode() {
		return lessEqualMemberCode;
	}

	public void setLessEqualMemberCode(String lessEqualMemberCode) {
		this.lessEqualMemberCode = lessEqualMemberCode;
	}

	public String getEqualEmail() {
		return equalEmail;
	}

	public void setEqualEmail(String equalEmail) {
		this.equalEmail = equalEmail;
	}

	public String getLikeEmail() {
		return likeEmail;
	}

	public void setLikeEmail(String likeEmail) {
		this.likeEmail = likeEmail;
	}

	public String getGreaterEqualEmail() {
		return greaterEqualEmail;
	}

	public void setGreaterEqualEmail(String greaterEqualEmail) {
		this.greaterEqualEmail = greaterEqualEmail;
	}

	public String getLessEqualEmail() {
		return lessEqualEmail;
	}

	public void setLessEqualEmail(String lessEqualEmail) {
		this.lessEqualEmail = lessEqualEmail;
	}

	public String getEqualEmailCheck() {
		return equalEmailCheck;
	}

	public void setEqualEmailCheck(String equalEmailCheck) {
		this.equalEmailCheck = equalEmailCheck;
	}

	public String getLikeEmailCheck() {
		return likeEmailCheck;
	}

	public void setLikeEmailCheck(String likeEmailCheck) {
		this.likeEmailCheck = likeEmailCheck;
	}

	public String getGreaterEqualEmailCheck() {
		return greaterEqualEmailCheck;
	}

	public void setGreaterEqualEmailCheck(String greaterEqualEmailCheck) {
		this.greaterEqualEmailCheck = greaterEqualEmailCheck;
	}

	public String getLessEqualEmailCheck() {
		return lessEqualEmailCheck;
	}

	public void setLessEqualEmailCheck(String lessEqualEmailCheck) {
		this.lessEqualEmailCheck = lessEqualEmailCheck;
	}

	public String getEqualMobile() {
		return equalMobile;
	}

	public void setEqualMobile(String equalMobile) {
		this.equalMobile = equalMobile;
	}

	public String getLikeMobile() {
		return likeMobile;
	}

	public void setLikeMobile(String likeMobile) {
		this.likeMobile = likeMobile;
	}

	public String getGreaterEqualMobile() {
		return greaterEqualMobile;
	}

	public void setGreaterEqualMobile(String greaterEqualMobile) {
		this.greaterEqualMobile = greaterEqualMobile;
	}

	public String getLessEqualMobile() {
		return lessEqualMobile;
	}

	public void setLessEqualMobile(String lessEqualMobile) {
		this.lessEqualMobile = lessEqualMobile;
	}

	public String getEqualMobileCheck() {
		return equalMobileCheck;
	}

	public void setEqualMobileCheck(String equalMobileCheck) {
		this.equalMobileCheck = equalMobileCheck;
	}

	public String getLikeMobileCheck() {
		return likeMobileCheck;
	}

	public void setLikeMobileCheck(String likeMobileCheck) {
		this.likeMobileCheck = likeMobileCheck;
	}

	public String getGreaterEqualMobileCheck() {
		return greaterEqualMobileCheck;
	}

	public void setGreaterEqualMobileCheck(String greaterEqualMobileCheck) {
		this.greaterEqualMobileCheck = greaterEqualMobileCheck;
	}

	public String getLessEqualMobileCheck() {
		return lessEqualMobileCheck;
	}

	public void setLessEqualMobileCheck(String lessEqualMobileCheck) {
		this.lessEqualMobileCheck = lessEqualMobileCheck;
	}

	public String getEqualPerfectCheck() {
		return equalPerfectCheck;
	}

	public void setEqualPerfectCheck(String equalPerfectCheck) {
		this.equalPerfectCheck = equalPerfectCheck;
	}

	public String getLikePerfectCheck() {
		return likePerfectCheck;
	}

	public void setLikePerfectCheck(String likePerfectCheck) {
		this.likePerfectCheck = likePerfectCheck;
	}

	public String getGreaterEqualPerfectCheck() {
		return greaterEqualPerfectCheck;
	}

	public void setGreaterEqualPerfectCheck(String greaterEqualPerfectCheck) {
		this.greaterEqualPerfectCheck = greaterEqualPerfectCheck;
	}

	public String getLessEqualPerfectCheck() {
		return lessEqualPerfectCheck;
	}

	public void setLessEqualPerfectCheck(String lessEqualPerfectCheck) {
		this.lessEqualPerfectCheck = lessEqualPerfectCheck;
	}

	public Long getEqualPerfectPercentage() {
		return equalPerfectPercentage;
	}

	public void setEqualPerfectPercentage(Long equalPerfectPercentage) {
		this.equalPerfectPercentage = equalPerfectPercentage;
	}

	public String getEqualLoginPassword() {
		return equalLoginPassword;
	}

	public void setEqualLoginPassword(String equalLoginPassword) {
		this.equalLoginPassword = equalLoginPassword;
	}

	public String getLikeLoginPassword() {
		return likeLoginPassword;
	}

	public void setLikeLoginPassword(String likeLoginPassword) {
		this.likeLoginPassword = likeLoginPassword;
	}

	public String getGreaterEqualLoginPassword() {
		return greaterEqualLoginPassword;
	}

	public void setGreaterEqualLoginPassword(String greaterEqualLoginPassword) {
		this.greaterEqualLoginPassword = greaterEqualLoginPassword;
	}

	public String getLessEqualLoginPassword() {
		return lessEqualLoginPassword;
	}

	public void setLessEqualLoginPassword(String lessEqualLoginPassword) {
		this.lessEqualLoginPassword = lessEqualLoginPassword;
	}

	public String getEqualMemberName() {
		return equalMemberName;
	}

	public void setEqualMemberName(String equalMemberName) {
		this.equalMemberName = equalMemberName;
	}

	public String getLikeMemberName() {
		return likeMemberName;
	}

	public void setLikeMemberName(String likeMemberName) {
		this.likeMemberName = likeMemberName;
	}

	public String getGreaterEqualMemberName() {
		return greaterEqualMemberName;
	}

	public void setGreaterEqualMemberName(String greaterEqualMemberName) {
		this.greaterEqualMemberName = greaterEqualMemberName;
	}

	public String getLessEqualMemberName() {
		return lessEqualMemberName;
	}

	public void setLessEqualMemberName(String lessEqualMemberName) {
		this.lessEqualMemberName = lessEqualMemberName;
	}

	public Long getEqualGender() {
		return equalGender;
	}

	public void setEqualGender(Long equalGender) {
		this.equalGender = equalGender;
	}

	public String getEqualBirthday() {
		return equalBirthday;
	}

	public void setEqualBirthday(String equalBirthday) {
		this.equalBirthday = equalBirthday;
	}

	public String getLikeBirthday() {
		return likeBirthday;
	}

	public void setLikeBirthday(String likeBirthday) {
		this.likeBirthday = likeBirthday;
	}

	public String getGreaterEqualBirthday() {
		return greaterEqualBirthday;
	}

	public void setGreaterEqualBirthday(String greaterEqualBirthday) {
		this.greaterEqualBirthday = greaterEqualBirthday;
	}

	public String getLessEqualBirthday() {
		return lessEqualBirthday;
	}

	public void setLessEqualBirthday(String lessEqualBirthday) {
		this.lessEqualBirthday = lessEqualBirthday;
	}

	public String getEqualRegisterTime() {
		return equalRegisterTime;
	}

	public void setEqualRegisterTime(String equalRegisterTime) {
		this.equalRegisterTime = equalRegisterTime;
	}

	public String getLikeRegisterTime() {
		return likeRegisterTime;
	}

	public void setLikeRegisterTime(String likeRegisterTime) {
		this.likeRegisterTime = likeRegisterTime;
	}

	public String getGreaterEqualRegisterTime() {
		return greaterEqualRegisterTime;
	}

	public void setGreaterEqualRegisterTime(String greaterEqualRegisterTime) {
		this.greaterEqualRegisterTime = greaterEqualRegisterTime;
	}

	public String getLessEqualRegisterTime() {
		return lessEqualRegisterTime;
	}

	public void setLessEqualRegisterTime(String lessEqualRegisterTime) {
		this.lessEqualRegisterTime = lessEqualRegisterTime;
	}

	public String getEqualRegisterIp() {
		return equalRegisterIp;
	}

	public void setEqualRegisterIp(String equalRegisterIp) {
		this.equalRegisterIp = equalRegisterIp;
	}

	public String getLikeRegisterIp() {
		return likeRegisterIp;
	}

	public void setLikeRegisterIp(String likeRegisterIp) {
		this.likeRegisterIp = likeRegisterIp;
	}

	public String getGreaterEqualRegisterIp() {
		return greaterEqualRegisterIp;
	}

	public void setGreaterEqualRegisterIp(String greaterEqualRegisterIp) {
		this.greaterEqualRegisterIp = greaterEqualRegisterIp;
	}

	public String getLessEqualRegisterIp() {
		return lessEqualRegisterIp;
	}

	public void setLessEqualRegisterIp(String lessEqualRegisterIp) {
		this.lessEqualRegisterIp = lessEqualRegisterIp;
	}

	public String getEqualLastLoginTime() {
		return equalLastLoginTime;
	}

	public void setEqualLastLoginTime(String equalLastLoginTime) {
		this.equalLastLoginTime = equalLastLoginTime;
	}

	public String getLikeLastLoginTime() {
		return likeLastLoginTime;
	}

	public void setLikeLastLoginTime(String likeLastLoginTime) {
		this.likeLastLoginTime = likeLastLoginTime;
	}

	public String getGreaterEqualLastLoginTime() {
		return greaterEqualLastLoginTime;
	}

	public void setGreaterEqualLastLoginTime(String greaterEqualLastLoginTime) {
		this.greaterEqualLastLoginTime = greaterEqualLastLoginTime;
	}

	public String getLessEqualLastLoginTime() {
		return lessEqualLastLoginTime;
	}

	public void setLessEqualLastLoginTime(String lessEqualLastLoginTime) {
		this.lessEqualLastLoginTime = lessEqualLastLoginTime;
	}

	public String getEqualLastLoginIp() {
		return equalLastLoginIp;
	}

	public void setEqualLastLoginIp(String equalLastLoginIp) {
		this.equalLastLoginIp = equalLastLoginIp;
	}

	public String getLikeLastLoginIp() {
		return likeLastLoginIp;
	}

	public void setLikeLastLoginIp(String likeLastLoginIp) {
		this.likeLastLoginIp = likeLastLoginIp;
	}

	public String getGreaterEqualLastLoginIp() {
		return greaterEqualLastLoginIp;
	}

	public void setGreaterEqualLastLoginIp(String greaterEqualLastLoginIp) {
		this.greaterEqualLastLoginIp = greaterEqualLastLoginIp;
	}

	public String getLessEqualLastLoginIp() {
		return lessEqualLastLoginIp;
	}

	public void setLessEqualLastLoginIp(String lessEqualLastLoginIp) {
		this.lessEqualLastLoginIp = lessEqualLastLoginIp;
	}

	public Long getEqualLastLoginCity() {
		return equalLastLoginCity;
	}

	public void setEqualLastLoginCity(Long equalLastLoginCity) {
		this.equalLastLoginCity = equalLastLoginCity;
	}

	public String getEqualLastConsumptionTime() {
		return equalLastConsumptionTime;
	}

	public void setEqualLastConsumptionTime(String equalLastConsumptionTime) {
		this.equalLastConsumptionTime = equalLastConsumptionTime;
	}

	public String getLikeLastConsumptionTime() {
		return likeLastConsumptionTime;
	}

	public void setLikeLastConsumptionTime(String likeLastConsumptionTime) {
		this.likeLastConsumptionTime = likeLastConsumptionTime;
	}

	public String getGreaterEqualLastConsumptionTime() {
		return greaterEqualLastConsumptionTime;
	}

	public void setGreaterEqualLastConsumptionTime(String greaterEqualLastConsumptionTime) {
		this.greaterEqualLastConsumptionTime = greaterEqualLastConsumptionTime;
	}

	public String getLessEqualLastConsumptionTime() {
		return lessEqualLastConsumptionTime;
	}

	public void setLessEqualLastConsumptionTime(String lessEqualLastConsumptionTime) {
		this.lessEqualLastConsumptionTime = lessEqualLastConsumptionTime;
	}

	public Long getEqualLastConsumptionCity() {
		return equalLastConsumptionCity;
	}

	public void setEqualLastConsumptionCity(Long equalLastConsumptionCity) {
		this.equalLastConsumptionCity = equalLastConsumptionCity;
	}

	public Long getEqualStatusId() {
		return equalStatusId;
	}

	public void setEqualStatusId(Long equalStatusId) {
		this.equalStatusId = equalStatusId;
	}

	public Long getEqualActiveStatusId() {
		return equalActiveStatusId;
	}

	public void setEqualActiveStatusId(Long equalActiveStatusId) {
		this.equalActiveStatusId = equalActiveStatusId;
	}

	public Long getEqualConsumptionStatusId() {
		return equalConsumptionStatusId;
	}

	public void setEqualConsumptionStatusId(Long equalConsumptionStatusId) {
		this.equalConsumptionStatusId = equalConsumptionStatusId;
	}

	public Long getGreaterEqualPointsBalance() {
		return greaterEqualPointsBalance;
	}

	public void setGreaterEqualPointsBalance(Long greaterEqualPointsBalance) {
		this.greaterEqualPointsBalance = greaterEqualPointsBalance;
	}

	public Long getLessEqualPointsBalance() {
		return lessEqualPointsBalance;
	}

	public void setLessEqualPointsBalance(Long lessEqualPointsBalance) {
		this.lessEqualPointsBalance = lessEqualPointsBalance;
	}

	public String[] getInMemberGroupIds() {
		return inMemberGroupIds;
	}

	public void setInMemberGroupIds(String[] inMemberGroupIds) {
		this.inMemberGroupIds = inMemberGroupIds;
	}

	public String[] getInMemberTagIds() {
		return inMemberTagIds;
	}

	public void setInMemberTagIds(String[] inMemberTagIds) {
		this.inMemberTagIds = inMemberTagIds;
	}

	public String getEqualOrgCode() {
		return equalOrgCode;
	}

	public void setEqualOrgCode(String equalOrgCode) {
		this.equalOrgCode = equalOrgCode;
	}

	public String getHappenTime() {
		return happenTime;
	}

	public void setHappenTime(String happenTime) {
		this.happenTime = happenTime;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public BigDecimal getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(BigDecimal payAmount) {
		this.payAmount = payAmount;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(String usedTime) {
		this.usedTime = usedTime;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getCouponInstanceCode() {
		return couponInstanceCode;
	}

	public void setCouponInstanceCode(String couponInstanceCode) {
		this.couponInstanceCode = couponInstanceCode;
	}

	public String getMemberGroupIds() {
		return memberGroupIds;
	}

	public void setMemberGroupIds(String memberGroupIds) {
		this.memberGroupIds = memberGroupIds;
	}

	public String getMemberTagIds() {
		return memberTagIds;
	}

	public void setMemberTagIds(String memberTagIds) {
		this.memberTagIds = memberTagIds;
	}

	public Long getMemberGroupId() {
		return memberGroupId;
	}

	public void setMemberGroupId(Long memberGroupId) {
		this.memberGroupId = memberGroupId;
	}

	public String getMemberIds() {
		return memberIds;
	}

	public void setMemberIds(String memberIds) {
		this.memberIds = memberIds;
	}

	public String getNotEqualIsEmployee() {
		return notEqualIsEmployee;
	}

	public void setNotEqualIsEmployee(String notEqualIsEmployee) {
		this.notEqualIsEmployee = notEqualIsEmployee;
	}

	public String getEqualIsEmployee() {
		return equalIsEmployee;
	}

	public void setEqualIsEmployee(String equalIsEmployee) {
		this.equalIsEmployee = equalIsEmployee;
	}

}
