package com.bw.adv.module.member.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.member.mapper.MemberMapper;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.report.model.MemberAge;
import com.bw.adv.module.report.model.MemberRegion;
import com.bw.adv.module.wechat.model.WechatFansMessage;

public interface MemberRepository extends BaseRepository<Member, MemberMapper> {

	Long findCountSearchAuto(MemberSearch search);

	/**
	 * 查询所有会员数量 Date: 2016年5月5日 下午2:26:23 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @return
	 */
	Long queryActiveMemberCount();

	List<MemberExp> findListSearchAuto(MemberSearch search);
	
	/**
	 * 偏移量分批查询
	 * selectListSearchAutoByOffset: <br/>
	 * Date: 2016年5月11日 下午4:36:40 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param search
	 * @param offset
	 * @param lastId
	 * @return
	 */
	List<MemberExp> selectListSearchAutoByOffset(MemberSearch search,Long offset,Long lastId);
	
	List<MemberExp> selectListSearchAutoByOffsetNoCondition(MemberSearch search,Long offset,Long lastId);

	Member findEntitySearchAuto(MemberSearch search);

	List<MemberPointsItemExp> findMemberPointsByMemberId(MemberSearch search);

	List<OrderHeader> findOrderHeaderByMemberId(MemberSearch search);

	List<CouponInstance> findCouponInstanceByMemberId(MemberSearch search);

	List<WechatFansMessage> findWechatMessageByMemberId(MemberSearch search);

	List<SmsInstance> findSmsInstanceByMemberId(MemberSearch search);

	/**
	 * 
	 * findListBySql:根据sql查询会员id <br/>
	 * Date: 2015年8月13日 下午4:18:27 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author simon
	 * @version jdk1.7
	 * @param sql
	 * @return
	 */
	List<Long> findListBySql(String sql);

	Long findCountMemberPointsByMemberId(MemberSearch search);

	Long findCountOrderHeaderByMemberId(MemberSearch search);

	Long findCountCouponInstanceByMemberId(MemberSearch search);

	Long findCountWechatMessageByMemberId(MemberSearch search);

	Long findCountSmsInstanceByMemberId(MemberSearch search);

	/**
	 * findByMemberCode:(根据会员编号查询会员信息). <br/>
	 * Date: 2015-9-15 下午6:03:51 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param memberCode
	 * @return
	 */
	Member findByMemberCode(String memberCode);

	/**
	 * findByMemberCode:(根据手机号查询). <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param memberCode
	 * @return
	 */
	Member findByMobile(String mobile);

	/**
	 * findMemberExpByMemberId:(根据ID查询会员信息). <br/>
	 * Date: 2015-9-17 下午8:10:43 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberExp findMemberExpByMemberId(Long memberId);

	/**
	 * 根据手机号查询会员扩展信息
	 * @param code
	 * @param queryType
	 * @return
	 */
	MemberExp findMemberExpByCode(String code, String queryType);

	/**
	 * findPeriodPoints:(查询会员周期累计积分量). <br/>
	 * Date: 2015-9-17 下午4:12:33 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<MemberExp> findPeriodPoints(String registerDate, String beginDate, String endDate);
	
	/**
	 * 根据消费时间查询累积积分量
	 * @param consumeDate
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<MemberExp> findPeriodPointsByConsumeDate(String consumeDate, String beginDate, String endDate);

	/**
	 * findPeriodOrderAmountByConsumeDate:(根据消费时间查询会员周期累计订单金额). <br/>
	 * Date: 2016-6-14 下午12:18:55 <br/>
	 * scrmVersion standard
	 * @author mennan
	 * @version jdk1.7
	 * @param consumeDate
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<MemberExp> findPeriodOrderAmountByConsumeDate(String consumeDate, String beginDate, String endDate);
	
	/**
	 * findPeriodPointsByMemberId:(查询某个会员周期累计积分量). <br/>
	 * Date: 2015-9-17 下午4:15:10 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	MemberExp findPeriodPointsByMemberId(Long memberId, String beginDate, String endDate);

	/**
	 * findPeriodOrderAmount:(查询会员周期累计订单金额). <br/>
	 * Date: 2015-9-17 下午4:12:33 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<MemberExp> findPeriodOrderAmount(String registerDate, String beginDate, String endDate);

	/**
	 * findPeriodOrderAmountByMemberId:(查询某个会员周期累计订单金额). <br/>
	 * Date: 2015-9-17 下午4:16:00 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	MemberExp findPeriodOrderAmountByMemberId(Long memberId, String beginDate, String endDate);

	/**
	 * 根据会员id查询会员信息（微信）
	 * 
	 * @param memberId
	 * @return
	 */
	MemberExp selectMemberExpWechatByMemberId(Long memberId);

	/**
	 * 调用会员统计存储过程
	 * 
	 * @param dateStr
	 * @param weekStr
	 * @param monthStr
	 */
	void callMemberSummaryDay(String dateStr, String weekStr, String monthStr);

	/**
	 * 查询用户地区分部
	 * 
	 * @param page
	 * @return
	 */
	List<MemberRegion> findMemberRegion(Page<MemberRegion> page, String sort, String order);

	/**
	 * 查询当前所有会员数量
	 * 
	 * @return
	 */
	Long findAllMemberCount();

	/**
	 * 查询会员年龄日报
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<MemberAge> findMemberAge(String startDate, String endDate);

	/**
	 * findExpByMemberId:(根据会员ID，查询会员信息). <br/>
	 * Date: 2015-12-30 下午1:07:15 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberExp findExpByMemberId(Long memberId);

	/**
	 * findByAccountAndTypeId:(根据外部账号和外部账号类型查询会员信息). <br/>
	 * Date: 2016-4-21 下午5:03:20 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author mennan
	 * @version jdk1.7
	 * @param bindingAccount
	 * @param extAccountTypeId
	 * @return
	 */
	Member findByAccountAndTypeId(String bindingAccount, Long extAccountTypeId);
	
	/**
	 * 查询
	 * @param storeId
	 * @return
	 */
	Long queryMemberForStoreCount(Long storeId);

}
