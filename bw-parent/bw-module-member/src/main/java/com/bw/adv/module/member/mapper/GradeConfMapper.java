package com.bw.adv.module.member.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.GradeConf;

public interface GradeConfMapper extends BaseMapper<GradeConf>{
	
}