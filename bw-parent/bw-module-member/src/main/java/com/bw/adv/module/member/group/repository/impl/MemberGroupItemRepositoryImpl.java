

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.group.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.group.mapper.MemberGroupItemMapper;
import com.bw.adv.module.member.group.model.MemberGroupItem;
import com.bw.adv.module.member.group.repository.MemberGroupItemRepository;
import com.bw.adv.module.member.group.search.MemberGroupItemSearch;

/**
 * ClassName:MemberGroupItemRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberGroupItemRepositoryImpl extends BaseRepositoryImpl<MemberGroupItem, MemberGroupItemMapper> implements MemberGroupItemRepository{
	
	@Override
	protected Class<MemberGroupItemMapper> getMapperClass() {
		return MemberGroupItemMapper.class;
	}

	@Override
	public Long findCountSearchAuto(MemberGroupItemSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<MemberGroupItem> findListSearchAuto(MemberGroupItemSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public MemberGroupItem findEntitySearchAuto(MemberGroupItemSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}
	@Override
	public void deleteGroupItemByGroupId(Long memberGroupId) {
		
		this.getMapper().deleteGroupItemByGroupId(memberGroupId);
		
	}
	
	@Override
	public void deleteGroupItemByMemberId(Long memberId) {
		
		this.getMapper().deleteGroupItemByMemberId(memberId);
		
	}

	@Override
	public void insertBatchMemberGroupItem(List<MemberGroupItem> addItemList) {
		
		this.getMapper().insertBatchMemberGroupItem(addItemList);
	}
	
}