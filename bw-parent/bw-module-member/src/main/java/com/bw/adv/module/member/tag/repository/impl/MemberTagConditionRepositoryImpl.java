package com.bw.adv.module.member.tag.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.tag.mapper.MemberTagConditionMapper;
import com.bw.adv.module.member.tag.model.MemberTagCondition;
import com.bw.adv.module.member.tag.repository.MemberTagConditionRepository;

@Repository
public class MemberTagConditionRepositoryImpl extends BaseRepositoryImpl<MemberTagCondition, MemberTagConditionMapper> implements MemberTagConditionRepository{

	@Override
	protected Class<MemberTagConditionMapper> getMapperClass() {
		return MemberTagConditionMapper.class;
	}

	@Override
	public List<MemberTagCondition> findConditionByTagId(Long tagId) {
		return this.getMapper().selectConditionByTagId(tagId);
	}

	@Override
	@Transactional
	public List<Member> findMemberIdByConditionMember(Map<String,Object> tagCondition,List<Long> selectType) {
		Map<String,Object> resultMap = new HashMap<String, Object>();
		for (int i = 0,j = 1; i < selectType.size(); i++,j++) {
			resultMap.put(j+"", selectType.get(i));
		}
		
		if(resultMap.containsValue(10L)){
			this.getMapper().createTmpMember();
		}
		if(resultMap.containsValue(20L)){
			this.getMapper().createTmpPoints();
		}
		if(resultMap.containsValue(30L) || resultMap.containsValue(31L)){
			this.getMapper().createTmpCoupon();
		}
		if(resultMap.containsValue(40L) || resultMap.containsValue(41L) || resultMap.containsValue(42L) || resultMap.containsValue(43L)){
			this.getMapper().createTmpOrder();
		}
		
		//10.member 20.point 30.coupon0 31.coupon1 40.order0 41.order1 42.order2 43.order3
		if(resultMap.containsValue(10L)){
			this.getMapper().findMemberIdByConditionMember0(tagCondition);
		}
		if(resultMap.containsValue(20L)){
			this.getMapper().findMemberIdByConditionPoints0(tagCondition);
		}
		if(resultMap.containsValue(30L)){
			this.getMapper().findMemberIdByConditionCoupon0(tagCondition);
		}
		if(resultMap.containsValue(31L)){
			this.getMapper().findMemberIdByConditionCoupon1(tagCondition);
		}
		if(resultMap.containsValue(40L)){
			this.getMapper().findMemberIdByConditionOrder0(tagCondition);
		}
		if(resultMap.containsValue(41L)){
			this.getMapper().findMemberIdByConditionOrder1(tagCondition);
		}
		if(resultMap.containsValue(42L)){
			this.getMapper().findMemberIdByConditionOrder2(tagCondition);
		}
		if(resultMap.containsValue(43L)){
			this.getMapper().findMemberIdByConditionOrder3(tagCondition);
		}
		List<Member> result = this.getMapper().findMemberIdResult(resultMap);
		this.getMapper().delTmp(resultMap);
		return result;
	}

	@Override
	public void deleteConditionByTagId(Long memberTagId) {
		this.getMapper().deleteConditionByTagId(memberTagId);
	}
	
}
