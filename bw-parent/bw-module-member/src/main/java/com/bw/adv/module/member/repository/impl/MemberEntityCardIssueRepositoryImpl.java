package com.bw.adv.module.member.repository.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.mapper.MemberEntityCardIssueMapper;
import com.bw.adv.module.member.model.MemberEntityCardIssue;
import com.bw.adv.module.member.repository.MemberEntityCardIssueRepository;

@Repository
public class MemberEntityCardIssueRepositoryImpl extends BaseRepositoryImpl<MemberEntityCardIssue, MemberEntityCardIssueMapper> implements MemberEntityCardIssueRepository {
	
	@Override
	protected Class<MemberEntityCardIssueMapper> getMapperClass() {
		return MemberEntityCardIssueMapper.class;
	}

	@Override
	public List<MemberEntityCardIssue> selectMemerCardByBatch(String batch,Page<MemberEntityCardIssue> pageObj) {
		return this.getMapper().selectMemeberCardByBatch(batch,pageObj);
	}

	@Override
	public int insertMemberCardGetId(MemberEntityCardIssue record) {
		return this.getMapper().insertMemberCardGetId(record);
	}

}
