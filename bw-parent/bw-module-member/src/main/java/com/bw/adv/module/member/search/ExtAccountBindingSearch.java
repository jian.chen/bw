/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeConvertRecordSearch.java
 * Package Name:com.sage.scrm.module.member.search
 * Date:2015年8月11日下午1:44:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.search;

import com.bw.adv.module.member.BaseSearch;

/**
 * ClassName:GradeConvertRecordSearch <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:44:24 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class ExtAccountBindingSearch extends BaseSearch {
	public ExtAccountBindingSearch() {
		super(Default_Rows, Default_Page);
	}
	
	 private Long equalBindingId;
	 private Long equalExtAccountTypeId;
	 private Long equalMemberId;
    
     private String equalBindingAccount;
     private String likeBindingAccount;
     private String greaterEqualBindingAccount;
     private String lessEqualBindingAccount;
     
    
     private String equalBindingDate;
     private String likeBindingDate;
     private String greaterEqualBindingDate;
     private String lessEqualBindingDate;
     
    
     private String equalUnbindingDate;
     private String likeUnbindingDate;
     private String greaterEqualUnbindingDate;
     private String lessEqualUnbindingDate;
     
    
     private String equalAccessAccredit;
     private String likeAccessAccredit;
     private String greaterEqualAccessAccredit;
     private String lessEqualAccessAccredit;
     
    
     private String equalLoginAccredit;
     private String likeLoginAccredit;
     private String greaterEqualLoginAccredit;
     private String lessEqualLoginAccredit;
     
	 private Long equalStatusId;
    
    
    
    

	public Long getEqualBindingId() {
		return equalBindingId;
	}

	public void setEqualBindingId(Long equalBindingId) {
		this.equalBindingId = equalBindingId;
	}
    

	public Long getEqualExtAccountTypeId() {
		return equalExtAccountTypeId;
	}

	public void setEqualExtAccountTypeId(Long equalExtAccountTypeId) {
		this.equalExtAccountTypeId = equalExtAccountTypeId;
	}
    

	public Long getEqualMemberId() {
		return equalMemberId;
	}

	public void setEqualMemberId(Long equalMemberId) {
		this.equalMemberId = equalMemberId;
	}
    

    
    public String getEqualBindingAccount() {
		return equalBindingAccount;
	}

	public void setEqualBindingAccount(String equalBindingAccount) {
		this.equalBindingAccount = equalBindingAccount;
	}
    
    
    public String getLikeBindingAccount() {
		return likeBindingAccount;
	}

	public void setLikeBindingAccount(String likeBindingAccount) {
		this.likeBindingAccount = likeBindingAccount;
	}
    
   public String getGreaterEqualBindingAccount() {
		return greaterEqualBindingAccount;
	}

	public void setGreaterEqualBindingAccount(String greaterEqualBindingAccount) {
		this.greaterEqualBindingAccount = greaterEqualBindingAccount;
	}
    
     public String getLessEqualBindingAccount() {
		return lessEqualBindingAccount;
	}

	public void setLessEqualBindingAccount(String lessEqualBindingAccount) {
		this.lessEqualBindingAccount = lessEqualBindingAccount;
	}
    
    
     
    
    public String getEqualBindingDate() {
		return equalBindingDate;
	}

	public void setEqualBindingDate(String equalBindingDate) {
		this.equalBindingDate = equalBindingDate;
	}
    
    
    public String getLikeBindingDate() {
		return likeBindingDate;
	}

	public void setLikeBindingDate(String likeBindingDate) {
		this.likeBindingDate = likeBindingDate;
	}
    
   public String getGreaterEqualBindingDate() {
		return greaterEqualBindingDate;
	}

	public void setGreaterEqualBindingDate(String greaterEqualBindingDate) {
		this.greaterEqualBindingDate = greaterEqualBindingDate;
	}
    
     public String getLessEqualBindingDate() {
		return lessEqualBindingDate;
	}

	public void setLessEqualBindingDate(String lessEqualBindingDate) {
		this.lessEqualBindingDate = lessEqualBindingDate;
	}
    
    
     
    
    public String getEqualUnbindingDate() {
		return equalUnbindingDate;
	}

	public void setEqualUnbindingDate(String equalUnbindingDate) {
		this.equalUnbindingDate = equalUnbindingDate;
	}
    
    
    public String getLikeUnbindingDate() {
		return likeUnbindingDate;
	}

	public void setLikeUnbindingDate(String likeUnbindingDate) {
		this.likeUnbindingDate = likeUnbindingDate;
	}
    
   public String getGreaterEqualUnbindingDate() {
		return greaterEqualUnbindingDate;
	}

	public void setGreaterEqualUnbindingDate(String greaterEqualUnbindingDate) {
		this.greaterEqualUnbindingDate = greaterEqualUnbindingDate;
	}
    
     public String getLessEqualUnbindingDate() {
		return lessEqualUnbindingDate;
	}

	public void setLessEqualUnbindingDate(String lessEqualUnbindingDate) {
		this.lessEqualUnbindingDate = lessEqualUnbindingDate;
	}
    
    
     
    
    public String getEqualAccessAccredit() {
		return equalAccessAccredit;
	}

	public void setEqualAccessAccredit(String equalAccessAccredit) {
		this.equalAccessAccredit = equalAccessAccredit;
	}
    
    
    public String getLikeAccessAccredit() {
		return likeAccessAccredit;
	}

	public void setLikeAccessAccredit(String likeAccessAccredit) {
		this.likeAccessAccredit = likeAccessAccredit;
	}
    
   public String getGreaterEqualAccessAccredit() {
		return greaterEqualAccessAccredit;
	}

	public void setGreaterEqualAccessAccredit(String greaterEqualAccessAccredit) {
		this.greaterEqualAccessAccredit = greaterEqualAccessAccredit;
	}
    
     public String getLessEqualAccessAccredit() {
		return lessEqualAccessAccredit;
	}

	public void setLessEqualAccessAccredit(String lessEqualAccessAccredit) {
		this.lessEqualAccessAccredit = lessEqualAccessAccredit;
	}
    
    
     
    
    public String getEqualLoginAccredit() {
		return equalLoginAccredit;
	}

	public void setEqualLoginAccredit(String equalLoginAccredit) {
		this.equalLoginAccredit = equalLoginAccredit;
	}
    
    
    public String getLikeLoginAccredit() {
		return likeLoginAccredit;
	}

	public void setLikeLoginAccredit(String likeLoginAccredit) {
		this.likeLoginAccredit = likeLoginAccredit;
	}
    
   public String getGreaterEqualLoginAccredit() {
		return greaterEqualLoginAccredit;
	}

	public void setGreaterEqualLoginAccredit(String greaterEqualLoginAccredit) {
		this.greaterEqualLoginAccredit = greaterEqualLoginAccredit;
	}
    
     public String getLessEqualLoginAccredit() {
		return lessEqualLoginAccredit;
	}

	public void setLessEqualLoginAccredit(String lessEqualLoginAccredit) {
		this.lessEqualLoginAccredit = lessEqualLoginAccredit;
	}
    
    
     
	public Long getEqualStatusId() {
		return equalStatusId;
	}

	public void setEqualStatusId(Long equalStatusId) {
		this.equalStatusId = equalStatusId;
	}
}

