
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.api.grade.dto.GradeDto;
import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.member.mapper.GradeMapper;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.search.GradeSearch;

/**
 * ClassName: GradeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月6日 上午11:49:39 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface GradeRepository extends BaseRepository<Grade, GradeMapper>{
	
	Long findCountSearchAuto(GradeSearch search);

	List<Grade> findListSearchAuto(GradeSearch search);
	
	Grade findEntitySearchAuto(GradeSearch search);
	
	/**
	 * findList:查询列表 <br/>
	 * Date: 2015年9月6日 上午11:49:44 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	List<Grade> findList();
	
	/**
	 * findGradeDtoList:根据条件查询等级<br/>
	 * Date: 2015年9月14日 下午5:46:25 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<GradeDto> findGradeDtoList(@Param("example")Example example,Page<GradeDto> page);

	/**
	 * findGradeDtoList:根据条件查询等级<br/>
	 * Date: 2015年9月14日 下午5:46:25 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	GradeDto findGradeDtoList(@Param("example")Example example);
	
	List<Grade> queryGradeListForJS();
}

