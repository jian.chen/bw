

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.init;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.model.GradeConf;
import com.bw.adv.module.member.repository.GradeConfRepository;
import com.bw.adv.module.member.repository.GradeRepository;
import com.bw.adv.module.tools.BeanUtils;


/**
 * ClassName: GradeInit 等级初始化类<br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-17 上午11:17:48 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class GradeInit{
	
	private GradeRepository gradeRepository;
	private GradeConfRepository gradeConfRepository;
	
	private static Map<Long, Grade> GRADE_PK_MAP = new HashMap<Long, Grade>();
	private static Map<Integer, Grade> GRADE_LEVEL_MAP = new HashMap<Integer, Grade>();
	//等级的配置信息
	public static final GradeConf GRADE_CONF = new GradeConf();
	public static final List<Grade> GRADE_LIST = new ArrayList<Grade>();
	
	@PostConstruct
	public void init(){
		List<Grade> list = null;
		GradeConf gradeConf = null;
		gradeConf = gradeConfRepository.findByPk(1L);
		BeanUtils.copy(gradeConf, GRADE_CONF);
		list = gradeRepository.findList();
		for (Grade grade : list) {
			GRADE_LIST.add(grade);
			GRADE_PK_MAP.put(grade.getGradeId(), grade);
			GRADE_LEVEL_MAP.put(grade.getSeq(), grade);
		}
	}
	
	public void reLoad(){
		GRADE_PK_MAP.clear();
		GRADE_LEVEL_MAP.clear();
		GRADE_LIST.clear();
		init();
	}

	/**
	 * getGradeByPk:(根据主键获取等级信息). <br/>
	 * Date: 2015-9-17 下午1:54:15 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param gradeId
	 * @return
	 */
	public static final Grade getGradeByPk(Long gradeId){
		return GRADE_PK_MAP.get(gradeId);
	}
	
	/**
	 * getGradeBySeq:(根据级别获取等级信息). <br/>
	 * Date: 2015-9-17 下午1:54:15 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param gradeId
	 * @return
	 */
	public static final Grade getGradeBySeq(Integer level){
		return GRADE_LEVEL_MAP.get(level);
	}

	
	@Autowired
	public void setGradeRepository(GradeRepository gradeRepository) {
		this.gradeRepository = gradeRepository;
	}

	@Autowired
	public void setGradeConfRepository(GradeConfRepository gradeConfRepository) {
		this.gradeConfRepository = gradeConfRepository;
	}
	
	
}























