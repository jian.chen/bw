/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:ComplainTypeRepository.java
 * Package Name:com.sage.scrm.module.member.complain.repository
 * Date:2015年12月29日下午5:00:53
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.complain.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.complain.mapper.ComplainTypeMapper;
import com.bw.adv.module.member.complain.model.ComplainType;

/**
 * ClassName:ComplainTypeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月29日 下午5:00:53 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface ComplainTypeRepository extends BaseRepository<ComplainType, ComplainTypeMapper>{
	List<ComplainType> findAllComplainType();
}

