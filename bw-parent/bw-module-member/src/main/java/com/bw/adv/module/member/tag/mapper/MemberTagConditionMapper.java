package com.bw.adv.module.member.tag.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.tag.model.MemberTagCondition;
import com.bw.adv.module.member.tag.search.TagSqlProvider;

public interface MemberTagConditionMapper extends BaseMapper<MemberTagCondition>{
	
	/**
	 * 查询标签对应的条件
	 * @param tagId
	 * @return
	 */
	List<MemberTagCondition> selectConditionByTagId(@Param("tagId")Long tagId);
	
	/**
	 * 创建会员临时表
	 * @author cuicd
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "createTmpMember")
	void createTmpMember();
	
	/**
	 * 创建积分临时表
	 * @author cuicd
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "createTmpPoints")
	void createTmpPoints();
	
	/**
	 * 创建优惠券临时表
	 * @author cuicd
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "createTmpCoupon")
	void createTmpCoupon();
	
	/**
	 * 创建订单临时表
	 * @author cuicd
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "createTmpOrder")
	void createTmpOrder();
	
	/**
	 * 查询符合条件的会员
	 * @param tagCondition
	 * @return
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQuerySQLMember0")
	List<Member> findMemberIdByConditionMember0(Map<String,Object> tagCondition);
	
	/**
	 * 查询符合条件的会员
	 * @param tagCondition
	 * @return
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQuerySQLPoints0")
	List<Member> findMemberIdByConditionPoints0(Map<String,Object> tagCondition);
	
	/**
	 * 查询符合条件的会员
	 * @param tagCondition
	 * @return
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQuerySQLCoupon0")
	List<Member> findMemberIdByConditionCoupon0(Map<String,Object> tagCondition);
	
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQuerySQLCoupon1")
	List<Member> findMemberIdByConditionCoupon1(Map<String,Object> tagCondition);
	
	/**
	 * 查询符合条件的会员
	 * @param tagCondition
	 * @return
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQuerySQLOrder0")
	List<Member> findMemberIdByConditionOrder0(Map<String,Object> tagCondition);
	
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQuerySQLOrder1")
	List<Member> findMemberIdByConditionOrder1(Map<String,Object> tagCondition);
	
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQuerySQLOrder2")
	List<Member> findMemberIdByConditionOrder2(Map<String,Object> tagCondition);
	
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQuerySQLOrder3")
	List<Member> findMemberIdByConditionOrder3(Map<String,Object> tagCondition);
	
	/**
	 * 查询符合条件的会员
	 * @param tagCondition
	 * @return
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "getTagQueryResult")
	List<Member> findMemberIdResult(Map<String,Object> tagCondition);
	
	/**
	 * 删除临时表
	 * @param tagCondition
	 */
	@SelectProvider(type = TagSqlProvider.class,method = "delTmp")
	void delTmp(Map<String,Object> tagCondition);
	
	@Delete("delete from member_tag_condition where member_tag_id = #{memberTagId}")
	void deleteConditionByTagId(Long memberTagId);
}