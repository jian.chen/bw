
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.message.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.message.mapper.MemberMessageItemMapper;
import com.bw.adv.module.member.message.model.MemberMessageItem;
import com.bw.adv.module.member.message.model.NotReadMessage;


/**
 * ClassName: MemberMessageRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年2月22日 下午2:50:28 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface MemberMessageItemRepository extends BaseRepository<MemberMessageItem, MemberMessageItemMapper>{
	
	/**
	 * deleteByMessageId:撤回消息 <br/>
	 * Date: 2016年2月26日 下午3:59:07 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 */
	void deleteByMessageId(Long memberMessageId);
	
	/**
	 * findByMessageId:根据消息Id查询消息详情 <br/>
	 * Date: 2016年2月29日 下午9:55:50 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 * @return
	 */
	MemberMessageItem findByMessageId(Example example);
	
	/**
	 * findByMemberId:根据会员Id查询未读消息<br/>
	 * Date: 2016年3月2日 下午4:14:37 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	NotReadMessage findByMemberId(Long memberId);
}

