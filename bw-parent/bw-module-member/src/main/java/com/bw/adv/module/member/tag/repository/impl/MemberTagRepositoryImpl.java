/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberTagRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.tag.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.tag.mapper.MemberTagMapper;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.repository.MemberTagRepository;
import com.bw.adv.module.member.tag.search.MemberTagSearch;

/**
 * ClassName:MemberTagRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberTagRepositoryImpl extends BaseRepositoryImpl<MemberTag, MemberTagMapper> implements MemberTagRepository{
	
	@Override
	protected Class<MemberTagMapper> getMapperClass() {
		return MemberTagMapper.class;
	}

	@Override
	public Long findCountSearchAuto(MemberTagSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<MemberTag> findListSearchAuto(MemberTagSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public MemberTag findEntitySearchAuto(MemberTagSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}

	@Override
	public List<MemberTag> findTagByTypeAndNode(String type, String node) {
		return this.getMapper().selectTagByTypeAndNode(type, node);
	}

	@Override
	public List<MemberTag> findMemberTagByStatusId(Long statusId) {
		return this.getMapper().selectMemberTagByStatusId(statusId);
	}
	
	@Override
	public List<MemberTag> findTagByIdList(String[] memberTagIdList) {
		return this.getMapper().selectTagByIdList(memberTagIdList);
	}

}