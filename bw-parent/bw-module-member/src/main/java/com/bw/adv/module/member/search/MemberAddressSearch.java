package com.bw.adv.module.member.search;

import com.bw.adv.module.member.BaseSearch;


public class MemberAddressSearch extends BaseSearch {

     public MemberAddressSearch() {
    	 super(Default_Rows, Default_Page);
	}
	
	

	 private Long equalMemberAddressId;
	 private Long equalMemberId;
	 private Long equalProvinceId;
	 private Long equalCityId;
	 private Long equalDistrictId;
    
     private String equalPersonName;
     private String likePersonName;
     private String greaterEqualPersonName;
     private String lessEqualPersonName;
     
    
     private String equalPersonMobile;
     private String likePersonMobile;
     private String greaterEqualPersonMobile;
     private String lessEqualPersonMobile;
     
    
     private String equalPhoneAreaCode;
     private String likePhoneAreaCode;
     private String greaterEqualPhoneAreaCode;
     private String lessEqualPhoneAreaCode;
     
    
     private String equalPersonPhone;
     private String likePersonPhone;
     private String greaterEqualPersonPhone;
     private String lessEqualPersonPhone;
     
    
     private String equalAddressDetail;
     private String likeAddressDetail;
     private String greaterEqualAddressDetail;
     private String lessEqualAddressDetail;
     
    
     private String equalIsDefault;
     private String likeIsDefault;
     private String greaterEqualIsDefault;
     private String lessEqualIsDefault;
     
    
     private String equalRemark;
     private String likeRemark;
     private String greaterEqualRemark;
     private String lessEqualRemark;
     
	 private Long equalStatusId;
    
    
    
    

	public Long getEqualMemberAddressId() {
		return equalMemberAddressId;
	}

	public void setEqualMemberAddressId(Long equalMemberAddressId) {
		this.equalMemberAddressId = equalMemberAddressId;
	}
    

	public Long getEqualMemberId() {
		return equalMemberId;
	}

	public void setEqualMemberId(Long equalMemberId) {
		this.equalMemberId = equalMemberId;
	}
    

	public Long getEqualProvinceId() {
		return equalProvinceId;
	}

	public void setEqualProvinceId(Long equalProvinceId) {
		this.equalProvinceId = equalProvinceId;
	}
    

	public Long getEqualCityId() {
		return equalCityId;
	}

	public void setEqualCityId(Long equalCityId) {
		this.equalCityId = equalCityId;
	}
    

	public Long getEqualDistrictId() {
		return equalDistrictId;
	}

	public void setEqualDistrictId(Long equalDistrictId) {
		this.equalDistrictId = equalDistrictId;
	}
    

    
    public String getEqualPersonName() {
		return equalPersonName;
	}

	public void setEqualPersonName(String equalPersonName) {
		this.equalPersonName = equalPersonName;
	}
    
    
    public String getLikePersonName() {
		return likePersonName;
	}

	public void setLikePersonName(String likePersonName) {
		this.likePersonName = likePersonName;
	}
    
   public String getGreaterEqualPersonName() {
		return greaterEqualPersonName;
	}

	public void setGreaterEqualPersonName(String greaterEqualPersonName) {
		this.greaterEqualPersonName = greaterEqualPersonName;
	}
    
     public String getLessEqualPersonName() {
		return lessEqualPersonName;
	}

	public void setLessEqualPersonName(String lessEqualPersonName) {
		this.lessEqualPersonName = lessEqualPersonName;
	}
    
    
     
    
    public String getEqualPersonMobile() {
		return equalPersonMobile;
	}

	public void setEqualPersonMobile(String equalPersonMobile) {
		this.equalPersonMobile = equalPersonMobile;
	}
    
    
    public String getLikePersonMobile() {
		return likePersonMobile;
	}

	public void setLikePersonMobile(String likePersonMobile) {
		this.likePersonMobile = likePersonMobile;
	}
    
   public String getGreaterEqualPersonMobile() {
		return greaterEqualPersonMobile;
	}

	public void setGreaterEqualPersonMobile(String greaterEqualPersonMobile) {
		this.greaterEqualPersonMobile = greaterEqualPersonMobile;
	}
    
     public String getLessEqualPersonMobile() {
		return lessEqualPersonMobile;
	}

	public void setLessEqualPersonMobile(String lessEqualPersonMobile) {
		this.lessEqualPersonMobile = lessEqualPersonMobile;
	}
    
    
     
    
    public String getEqualPhoneAreaCode() {
		return equalPhoneAreaCode;
	}

	public void setEqualPhoneAreaCode(String equalPhoneAreaCode) {
		this.equalPhoneAreaCode = equalPhoneAreaCode;
	}
    
    
    public String getLikePhoneAreaCode() {
		return likePhoneAreaCode;
	}

	public void setLikePhoneAreaCode(String likePhoneAreaCode) {
		this.likePhoneAreaCode = likePhoneAreaCode;
	}
    
   public String getGreaterEqualPhoneAreaCode() {
		return greaterEqualPhoneAreaCode;
	}

	public void setGreaterEqualPhoneAreaCode(String greaterEqualPhoneAreaCode) {
		this.greaterEqualPhoneAreaCode = greaterEqualPhoneAreaCode;
	}
    
     public String getLessEqualPhoneAreaCode() {
		return lessEqualPhoneAreaCode;
	}

	public void setLessEqualPhoneAreaCode(String lessEqualPhoneAreaCode) {
		this.lessEqualPhoneAreaCode = lessEqualPhoneAreaCode;
	}
    
    
     
    
    public String getEqualPersonPhone() {
		return equalPersonPhone;
	}

	public void setEqualPersonPhone(String equalPersonPhone) {
		this.equalPersonPhone = equalPersonPhone;
	}
    
    
    public String getLikePersonPhone() {
		return likePersonPhone;
	}

	public void setLikePersonPhone(String likePersonPhone) {
		this.likePersonPhone = likePersonPhone;
	}
    
   public String getGreaterEqualPersonPhone() {
		return greaterEqualPersonPhone;
	}

	public void setGreaterEqualPersonPhone(String greaterEqualPersonPhone) {
		this.greaterEqualPersonPhone = greaterEqualPersonPhone;
	}
    
     public String getLessEqualPersonPhone() {
		return lessEqualPersonPhone;
	}

	public void setLessEqualPersonPhone(String lessEqualPersonPhone) {
		this.lessEqualPersonPhone = lessEqualPersonPhone;
	}
    
    
     
    
    public String getEqualAddressDetail() {
		return equalAddressDetail;
	}

	public void setEqualAddressDetail(String equalAddressDetail) {
		this.equalAddressDetail = equalAddressDetail;
	}
    
    
    public String getLikeAddressDetail() {
		return likeAddressDetail;
	}

	public void setLikeAddressDetail(String likeAddressDetail) {
		this.likeAddressDetail = likeAddressDetail;
	}
    
   public String getGreaterEqualAddressDetail() {
		return greaterEqualAddressDetail;
	}

	public void setGreaterEqualAddressDetail(String greaterEqualAddressDetail) {
		this.greaterEqualAddressDetail = greaterEqualAddressDetail;
	}
    
     public String getLessEqualAddressDetail() {
		return lessEqualAddressDetail;
	}

	public void setLessEqualAddressDetail(String lessEqualAddressDetail) {
		this.lessEqualAddressDetail = lessEqualAddressDetail;
	}
    
    
     
    
    public String getEqualIsDefault() {
		return equalIsDefault;
	}

	public void setEqualIsDefault(String equalIsDefault) {
		this.equalIsDefault = equalIsDefault;
	}
    
    
    public String getLikeIsDefault() {
		return likeIsDefault;
	}

	public void setLikeIsDefault(String likeIsDefault) {
		this.likeIsDefault = likeIsDefault;
	}
    
   public String getGreaterEqualIsDefault() {
		return greaterEqualIsDefault;
	}

	public void setGreaterEqualIsDefault(String greaterEqualIsDefault) {
		this.greaterEqualIsDefault = greaterEqualIsDefault;
	}
    
     public String getLessEqualIsDefault() {
		return lessEqualIsDefault;
	}

	public void setLessEqualIsDefault(String lessEqualIsDefault) {
		this.lessEqualIsDefault = lessEqualIsDefault;
	}
    
    
     
    
    public String getEqualRemark() {
		return equalRemark;
	}

	public void setEqualRemark(String equalRemark) {
		this.equalRemark = equalRemark;
	}
    
    
    public String getLikeRemark() {
		return likeRemark;
	}

	public void setLikeRemark(String likeRemark) {
		this.likeRemark = likeRemark;
	}
    
   public String getGreaterEqualRemark() {
		return greaterEqualRemark;
	}

	public void setGreaterEqualRemark(String greaterEqualRemark) {
		this.greaterEqualRemark = greaterEqualRemark;
	}
    
     public String getLessEqualRemark() {
		return lessEqualRemark;
	}

	public void setLessEqualRemark(String lessEqualRemark) {
		this.lessEqualRemark = lessEqualRemark;
	}
    
    
     
	public Long getEqualStatusId() {
		return equalStatusId;
	}

	public void setEqualStatusId(Long equalStatusId) {
		this.equalStatusId = equalStatusId;
	}
    

	
}
