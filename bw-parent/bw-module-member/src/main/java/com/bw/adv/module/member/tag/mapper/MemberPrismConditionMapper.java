package com.bw.adv.module.member.tag.mapper;

import com.bw.adv.module.member.model.MemberPrismCondition;

public interface MemberPrismConditionMapper {
    int deleteByPrimaryKey(Long conditionId);

    int insert(MemberPrismCondition record);

    int insertSelective(MemberPrismCondition record);

    MemberPrismCondition selectByPrimaryKey(Long conditionId);

    int updateByPrimaryKeySelective(MemberPrismCondition record);

    int updateByPrimaryKey(MemberPrismCondition record);
}