package com.bw.adv.module.member.mapper;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.MemberExtInfo;

public interface MemberExtInfoMapper extends BaseMapper<MemberExtInfo>{
}