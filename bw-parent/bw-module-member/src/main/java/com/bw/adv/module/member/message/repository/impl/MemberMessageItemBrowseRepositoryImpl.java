

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.message.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.message.mapper.MemberMessageItemBrowseMapper;
import com.bw.adv.module.member.message.model.MemberMessageItemBrowse;
import com.bw.adv.module.member.message.repository.MemberMessageItemBrowseRepository;

@Repository
public class MemberMessageItemBrowseRepositoryImpl extends BaseRepositoryImpl<MemberMessageItemBrowse, MemberMessageItemBrowseMapper> implements MemberMessageItemBrowseRepository{
	
	@Override
	protected Class<MemberMessageItemBrowseMapper> getMapperClass() {
		return MemberMessageItemBrowseMapper.class;
	}


}