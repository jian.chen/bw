package com.bw.adv.module.member.message.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.message.model.MemberMessage;
import com.bw.adv.module.member.message.model.exp.MemberMessageExp;

public interface MemberMessageMapper extends BaseMapper<MemberMessage>{
	
	/**
	 * selectList:查询消息中心列表<br/>
	 * Date: 2016年2月22日 下午2:54:03 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param Page
	 * @return
	 */
	List<MemberMessage> selectList(Page<MemberMessage> Page);
	
	/**
	 * updateStatus:修改消息中心状态为已删除<br/>
	 * Date: 2016年2月25日 下午4:36:49 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 */
	void updateStatus(Long memberMessageId);
	
	/**
	 * selectMessageByMemberId:根据会员Id查询会员消息<br/>
	 * Date: 2016年2月28日 下午11:30:44 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @param Page
	 * @return
	 */
	List<MemberMessageExp> selectMessageByMemberId(@Param("memberId")Long memberId,Page<MemberMessageExp> Page);
}