
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:ExtAccountBindingRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.mapper.ExtAccountBindingMapper;
import com.bw.adv.module.member.model.ExtAccountBinding;
import com.bw.adv.module.member.search.ExtAccountBindingSearch;

/**
 * ClassName:ExtAccountBindingRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:51:06 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface ExtAccountBindingRepository extends BaseRepository<ExtAccountBinding, ExtAccountBindingMapper>{
	
	Long findCountSearchAuto(ExtAccountBindingSearch search);

	List<ExtAccountBinding> findListSearchAuto(ExtAccountBindingSearch search);
	
	ExtAccountBinding findEntitySearchAuto(ExtAccountBindingSearch search);
	
	ExtAccountBinding findByAccountAndTypeId(String bindingAccount,Long extAccountTypeId);
	
	List<ExtAccountBinding> findByMemberIdAndTypeId(Long memberId,Long extAccountTypeId);
	
}

