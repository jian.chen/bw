package com.bw.adv.module.member.tag.repository;

import java.util.List;
import java.util.Map;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.tag.mapper.MemberTagConditionMapper;
import com.bw.adv.module.member.tag.model.MemberTagCondition;

public interface MemberTagConditionRepository extends BaseRepository<MemberTagCondition, MemberTagConditionMapper>{
	
	/**
	 * 查询标签对应的条件
	 * @param tagId
	 * @return
	 */
	List<MemberTagCondition> findConditionByTagId(Long tagId);
	
	/**
	 * 查询符合条件的会员
	 * @param tagCondition
	 * @return
	 */
	List<Member> findMemberIdByConditionMember(Map<String,Object> tagCondition,List<Long> selectType);

	/**
	 * 根据标签id删除条件
	 * @param memberTagId
	 */
	void deleteConditionByTagId(Long memberTagId);
	
}
