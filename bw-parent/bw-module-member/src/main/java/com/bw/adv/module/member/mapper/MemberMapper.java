package com.bw.adv.module.member.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.report.model.MemberAge;
import com.bw.adv.module.report.model.MemberRegion;
import com.bw.adv.module.wechat.model.WechatFansMessage;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberMapper extends BaseMapper<Member>{
    
	Long selectCountSearchAuto(@Param("MemberSearch")MemberSearch search);

	List<MemberExp> selectListSearchAuto(@Param("MemberSearch")MemberSearch search);
	
	/**
	 * 通过偏移量进行分页查询
	 * selectListSearchAutoByOffset: <br/>
	 * Date: 2016年5月11日 下午4:28:27 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @param search
	 * @param offset
	 * @return
	 */
	List<MemberExp> selectListSearchAutoByOffset(@Param("MemberSearch")MemberSearch search,@Param("offset")Long offset,@Param("lastId")Long lastId);
	
	List<MemberExp> selectListSearchAutoByOffsetNoCondition(@Param("MemberSearch")MemberSearch search,@Param("offset")Long offset,@Param("lastId")Long lastId);
	
	Member selectEntitySearchAuto(@Param("MemberSearch")MemberSearch search);

	List<MemberPointsItemExp> selectMemberPointsByMemberId(@Param("MemberSearch")MemberSearch search);

	List<OrderHeader> selectOrderHeaderByMemberId(@Param("MemberSearch")MemberSearch search);

	List<CouponInstance> selectCouponInstanceByMemberId(@Param("MemberSearch")MemberSearch search);

	List<WechatFansMessage> selectWechatMessageByMemberId(@Param("MemberSearch")MemberSearch search);

	List<SmsInstance> selectSmsInstanceByMemberId(@Param("MemberSearch")MemberSearch search);

	List<Long> selectListBySql(@Param("sql")String sql);

	Long selectCountMemberPointsByMemberId(@Param("MemberSearch")MemberSearch search);

	Long selectCountOrderHeaderByMemberId(@Param("MemberSearch")MemberSearch search);

	Long selectCountCouponInstanceByMemberId(@Param("MemberSearch")MemberSearch search);

	Long selectCountWechatMessageByMemberId(@Param("MemberSearch")MemberSearch search);

	Long selectCountSmsInstanceByMemberId(@Param("MemberSearch")MemberSearch search);

	/**
	 * selectByMemberCode:(根据会员编号查询会员信息). <br/>
	 * Date: 2015-9-15 下午6:04:43 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberCode
	 * @return
	 */
	Member selectByMemberCode(@Param("memberCode")String memberCode);

	/**
	 * selectPeriodPoints:(查询会员周期累计积分量). <br/>
	 * Date: 2015-9-17 下午4:22:20 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param registerDate
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<MemberExp> selectPeriodPoints(@Param("registerDate")String registerDate,@Param("beginTime")String beginTime, @Param("endTime")String endTime);

	/**
	 * selectPeriodPoints:(查询某个会员周期累计积分量). <br/>
	 * Date: 2015-9-17 下午4:22:56 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	MemberExp selectPeriodPointsByMemberId(@Param("memberId")Long memberId,@Param("beginTime")String beginTime, @Param("endTime")String endTime);

	/**
	 * selectPeriodOrderAmount:(查询会员周期累计订单金额). <br/>
	 * Date: 2015-9-17 下午4:23:56 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param registerDate
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<MemberExp> selectPeriodOrderAmount(@Param("registerDate")String registerDate,@Param("beginTime")String beginTime, @Param("endTime")String endTime);
	
	
	
	/**
	 * 根据消费时间查询会员周期累计订单金额
	 * @param consumeDate
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<MemberExp> selectPeriodOrderAmountByConsumeDate(@Param("consumeDate")String consumeDate,@Param("beginTime")String beginTime, @Param("endTime")String endTime);
	
	/**
	 * 根据消费时间查询会员的累积积分
	 * @param consumeDate
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<MemberExp> selectPeriodPointsByConsumeDate(@Param("consumeDate")String consumeDate,@Param("beginTime")String beginTime, @Param("endTime")String endTime);

	/**
	 * selectPeriodOrderAmount:(查询某个会员周期累计订单金额). <br/>
	 * Date: 2015-9-17 下午4:24:22 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	MemberExp selectPeriodOrderAmountByMemberId(@Param("memberId")Long memberId,@Param("beginTime")String beginTime, @Param("endTime")String endTime);

	/**
	 * selectMemberExpByMemberId:(根据ID查询会员信息). <br/>
	 * Date: 2015-9-17 下午8:12:33 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberExp selectMemberExpByMemberId(@Param("memberId")Long memberId);
	
	
	MemberExp selectMemberExpWechatByMemberId(@Param("memberId")Long memberId);
	
	void callMemberSummaryDay(@Param("dateStr")String dateStr, @Param("weekStr")String weekStr, @Param("monthStr")String monthStr);
	
	/**
     * 查询用户地区分部
     * @param page
     * @return
     */
    List<MemberRegion> findMemberRegion(Page<MemberRegion> page,@Param("sort")String sort,@Param("order")String order);
    
    /**
     * 查询所有会员数量
     * @return
     */
    Long selectCountMember();
    
    /**
     * 查询会员年龄日报
     * @param startDate
     * @param endDate
     * @return
     */
    List<MemberAge> findMemberAge(@Param("startDate")String startDate,@Param("endDate")String endDate);

	/**
	 * selectExpByMemberId:(根据会员ID，查询会员信息). <br/>
	 * Date: 2015-12-30 下午1:09:12 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberExp selectExpByMemberId(@Param("memberId")Long memberId);

	/**
	 * selectByMobile:(根据手机号查询). <br/>
	 * Date: 2016-4-8 上午11:30:44 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param mobile
	 * @return
	 */
	Member selectByMobile(String mobile);

	/**
	 * selectByAccountAndTypeId:(根据外部账号和外部账号类型查询会员信息). <br/>
	 * Date: 2016-4-21 下午5:05:22 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param bindingAccount
	 * @param extAccountTypeId
	 * @return
	 */
	Member selectByAccountAndTypeId(@Param("bindingAccount")String bindingAccount,@Param("extAccountTypeId")Long extAccountTypeId);
    
	/**
	 * 查询所有会员数量
	 * Date: 2016年5月5日 下午2:26:23 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @return
	 */
	Long queryActiveMemberCount();

	/**
	 * 根据手机号查询会员扩展信息
	 * @param code
	 * @param queryType
	 * @return
	 */
	MemberExp selectMemberExpByCode(@Param("code") String code, @Param("queryType") String queryType);
	
	/**
	 * 查询
	 * @param storeId
	 * @return
	 */
	Long queryMemberForStoreCount(@Param("storeId")Long storeId);

}
