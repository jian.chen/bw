

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.message.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.message.mapper.MemberMessageMapper;
import com.bw.adv.module.member.message.model.MemberMessage;
import com.bw.adv.module.member.message.model.exp.MemberMessageExp;
import com.bw.adv.module.member.message.repository.MemberMessageRepository;

/**
 * ClassName: MemberMessageRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年2月22日 下午2:50:39 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class MemberMessageRepositoryImpl extends BaseRepositoryImpl<MemberMessage, MemberMessageMapper> implements MemberMessageRepository{
	
	@Override
	protected Class<MemberMessageMapper> getMapperClass() {
		return MemberMessageMapper.class;
	}

	@Override
	public List<MemberMessage> findList(Page<MemberMessage> Page) {
		
		return this.getMapper().selectList(Page);
	}

	@Override
	public void updateStatus(Long memberMessageId) {
		
		this.getMapper().updateStatus(memberMessageId);
	}
	
	@Override
	public List<MemberMessageExp> findListByMemberId(Long memberId,
			Page<MemberMessageExp> Page) {
		
		return this.getMapper().selectMessageByMemberId(memberId, Page);
	}

}