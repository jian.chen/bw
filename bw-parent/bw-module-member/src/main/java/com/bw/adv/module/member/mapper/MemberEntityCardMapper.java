package com.bw.adv.module.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberEntityCard;

public interface MemberEntityCardMapper extends  BaseMapper<MemberEntityCard> {

    int deleteByPrimaryKey(Long memberEntityCardId);

    int insert(MemberEntityCard record);

    int insertSelective(MemberEntityCard record);

    MemberEntityCard selectByPrimaryKey(Long memberEntityCardId);

    int updateByPrimaryKeySelective(MemberEntityCard record);

    int updateByPrimaryKey(MemberEntityCard record);
    
    /**
     * 根据卡号和状态查询实体卡
     * @param cardNo
     * @return
     */
    MemberEntityCard findCardByCardNo(@Param("cardNo")String cardNo,@Param("statusId")Long statusId);
    
    
    List<MemberEntityCard> selectMemberCardByIssueId(@Param("issueId") Long issueId,Page<MemberEntityCard> pageObj);
}