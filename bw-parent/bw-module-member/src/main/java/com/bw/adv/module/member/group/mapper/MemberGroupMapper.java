package com.bw.adv.module.member.group.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.search.MemberGroupSearch;

public interface MemberGroupMapper extends BaseMapper<MemberGroup>{
    
    Long selectCountSearchAuto(@Param("MemberGroupSearch")MemberGroupSearch search);

   	List<MemberGroup> selectListSearchAuto(@Param("MemberGroupSearch")MemberGroupSearch search);
   	
   	MemberGroup selectEntitySearchAuto(@Param("MemberGroupSearch")MemberGroupSearch search);
    
}