/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberAddressRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.mapper.MemberCardMapper;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.exp.MemberCardExp;
import com.bw.adv.module.member.repository.MemberCardRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: MemberCardRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-24 下午4:54:56 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class MemberCardRepositoryImpl extends BaseRepositoryImpl<MemberCard, MemberCardMapper> implements MemberCardRepository{
	
	@Override
	protected Class<MemberCardMapper> getMapperClass() {
		return MemberCardMapper.class;
	}

	@Override
	public MemberCard findByMemberId(Long memberId) {
		List<MemberCard> list = this.getMapper().selectByMemberId(memberId);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public MemberCard findByCardNo(String cardNo) {
		return this.getMapper().selectByCardNo(cardNo);
	}

	@Override
	public MemberCardExp findExpByMemberId(Long memberId) {
		return this.getMapper().selectExpByMemberId(memberId);
	}

	@Override
	public int deleteCardByMemberId(Long memberId) {
		return this.getMapper().deleteCardByMemberId(memberId);
	}
}