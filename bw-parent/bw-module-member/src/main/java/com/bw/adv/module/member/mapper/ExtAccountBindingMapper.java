package com.bw.adv.module.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.ExtAccountBinding;
import com.bw.adv.module.member.search.ExtAccountBindingSearch;

public interface ExtAccountBindingMapper  extends BaseMapper<ExtAccountBinding>{
    
    Long selectCountSearchAuto(@Param("ExtAccountBindingSearch")ExtAccountBindingSearch search);

   	List<ExtAccountBinding> selectListSearchAuto(@Param("ExtAccountBindingSearch")ExtAccountBindingSearch search);
   	
   	ExtAccountBinding selectEntitySearchAuto(@Param("ExtAccountBindingSearch")ExtAccountBindingSearch search);

	ExtAccountBinding selectByAccountAndTypeId(@Param("bindingAccount")String bindingAccount,@Param("extAccountTypeId")Long extAccountTypeId);

	List<ExtAccountBinding> selectByMemberIdAndTypeId(@Param("memberId")Long memberId,@Param("extAccountTypeId")Long extAccountTypeId);
	
}