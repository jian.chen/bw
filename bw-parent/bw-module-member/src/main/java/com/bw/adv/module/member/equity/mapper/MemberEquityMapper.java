package com.bw.adv.module.member.equity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.equity.search.MemberEquitySearch;
import com.bw.adv.module.member.model.MemberEquity;
import com.bw.adv.module.member.model.exp.MemberEquityExp;

public interface MemberEquityMapper extends BaseMapper<MemberEquity> {
    
	Long selectCountSearchAuto(@Param("MemberEquitySearch")MemberEquitySearch search);
	
	List<MemberEquityExp> selectListSearchAuto(@Param("MemberEquitySearch")MemberEquitySearch search);    
    
	int updateEquityStatus(MemberEquity memberEquity);
	
	List<MemberEquity> selectListByGradeId(@Param("gradeId")Long gradeId,@Param("nowDate")String nowDate);
	
}