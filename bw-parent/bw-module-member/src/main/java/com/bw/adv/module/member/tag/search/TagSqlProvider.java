package com.bw.adv.module.member.tag.search;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.tools.DateUtils;

public class TagSqlProvider {
	
	private static final String create_tmp_member = "CREATE TEMPORARY TABLE tmp_member (memberId BIGINT(20),INDEX index_memberId(memberId)) ";
	
	private static final String create_tmp_points = "CREATE TEMPORARY TABLE tmp_points (memberId BIGINT(20),INDEX index_memberId(memberId)) ";
	
	private static final String create_tmp_coupon = "CREATE TEMPORARY TABLE tmp_coupon (memberId BIGINT(20),INDEX index_memberId(memberId)) ";
	
	private static final String create_tmp_order = "CREATE TEMPORARY TABLE tmp_order (memberId BIGINT(20),INDEX index_memberId(memberId)) ";

	private static final String select_begin_member0 = "INSERT INTO tmp_member (memberId) SELECT m.MEMBER_ID AS memberId FROM member m WHERE 1 = 1 ";
	
	private static final String select_begin_points0 = "INSERT INTO tmp_points (memberId) SELECT ma.MEMBER_ID AS memberId FROM member_account ma WHERE 1 = 1 ";
	
	private static final String select_begin_coupon0 = "INSERT INTO tmp_coupon (memberId) select ci.COUPON_INSTANCE_OWNER as memberId from coupon_instance ci ";
	
	private static final String select_begin_order0 = "INSERT INTO tmp_order (memberId) select oh.MEMBER_ID as memberId from order_header oh where 1=1 ";
	
	
	public final static Map<Long,String> resultMap = new HashMap<Long, String>();
	static {  
		resultMap.put(10L, "tmp_member");  
		resultMap.put(20L, "tmp_points");
		resultMap.put(30L, "tmp_coupon");
		resultMap.put(31L, "tmp_coupon");
		resultMap.put(40L, "tmp_order");
		resultMap.put(41L, "tmp_order");
		resultMap.put(42L, "tmp_order");
		resultMap.put(43L, "tmp_order");
	} 
	
	/**
	 * 创建会员临时表
	 * @return
	 * @author cuicd
	 */
	public String createTmpMember(){
        return create_tmp_member;
	}
	
	/**
	 * 创建积分临时表
	 * @return
	 * @author cuicd
	 */
	public String createTmpPoints(){
        return create_tmp_points;
	}
	
	/**
	 * 创建优惠券临时表
	 * @return
	 * @author cuicd
	 */
	public String createTmpCoupon(){
        return create_tmp_coupon;
	}
	
	/**
	 * 创建订单临时表
	 * @return
	 * @author cuicd
	 */
	public String createTmpOrder(){
        return create_tmp_order;
	}
	
	
	/**
	 * 构建标签条件查询sql
	 * @return
	 */
	public String getTagQuerySQLMember0(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
        sql.append(select_begin_member0);
    	if(tagCondition.containsKey("mobile")){
    		if(tagCondition.get("mobile").equals("1")){
    			sql.append(" and m.mobile is not null ");
    		}else{
    			sql.append(" and m.mobile is null ");
    		}
		}
		if(tagCondition.containsKey("gender")){
			sql.append(" and m.gender = ").append(tagCondition.get("gender").toString());
		}
		if(tagCondition.containsKey("age")){
			String nowDate = DateUtils.getYear();
			String start = tagCondition.get("age").toString().split(",")[0].equals("") ? "0" : tagCondition.get("age").toString().split(",")[0];
			String end = tagCondition.get("age").toString().split(",")[1].equals("") ? "200" : tagCondition.get("age").toString().split(",")[1];
			Long birthdayStart = Long.parseLong(nowDate) - Long.parseLong(end);
			Long birthdayEnd = Long.parseLong(nowDate) - Long.parseLong(start);
			sql.append(" and m.birthday >= ").append(birthdayStart+"0101");
			sql.append(" and m.birthday <= ").append(birthdayEnd+"1231");
		}
		if(tagCondition.containsKey("birthday")){
			sql.append(" and m.birthday >= ").append(tagCondition.get("birthday").toString().split(",")[0]);
			sql.append(" and m.birthday <= ").append(tagCondition.get("birthday").toString().split(",")[1]);
		}
		if(tagCondition.containsKey("register")){
			sql.append(" and m.register_time >= ").append(tagCondition.get("register").toString().split(",")[0]+"000000");
			sql.append(" and m.register_time <= ").append(tagCondition.get("register").toString().split(",")[1]+"235959");
		}
		if(tagCondition.containsKey("channel")){
			sql.append(" and m.channel_id = ").append(tagCondition.get("channel").toString());
		}
		if(tagCondition.containsKey("grade")){
			sql.append(" and m.grade_id = ").append(tagCondition.get("grade").toString());
		}
		if(tagCondition.containsKey("orderLast")){
			if(StringUtils.isNotBlank(tagCondition.get("orderLast").toString().split(",")[1])){
				String lastStart = DateUtils.addDays(0-Integer.parseInt(tagCondition.get("orderLast").toString().split(",")[0]));
    			sql.append(" and m.LAST_CONSUMPTION_TIME >= ").append(lastStart+"000000");
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("orderLast").toString().split(",")[0])){
    			String lastEnd = DateUtils.addDays(0-Integer.parseInt(tagCondition.get("orderLast").toString().split(",")[1]));
    			sql.append(" and m.LAST_CONSUMPTION_TIME <= ").append(lastEnd+"235959");
    		}
		}
		System.out.println("tagSqlMember-----"+sql.toString());
        return sql.toString();
	}
	
	public String getTagQuerySQLPoints0(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
        sql.append(select_begin_points0);
		if(tagCondition.containsKey("points")){
			if(StringUtils.isNotBlank(tagCondition.get("points").toString().split(",")[0])){
    			sql.append(" and ma.POINTS_BALANCE >= ").append(tagCondition.get("points").toString().split(",")[0]);
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("points").toString().split(",")[1])){
    			sql.append(" and ma.POINTS_BALANCE >= ").append(tagCondition.get("points").toString().split(",")[1]);
    		}
		}
        System.out.println("tagSqlPoints-----"+sql.toString());
        return sql.toString();
	}
	
	public String getTagQuerySQLCoupon0(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
        sql.append(select_begin_coupon0);
    	if(tagCondition.containsKey("couponFree")){
    		sql.append(" where ci.STATUS_ID = ").append(StatusConstant.COUPON_INSTANCE_UNUSED.getId());
    		sql.append(" group by ci.COUPON_INSTANCE_OWNER ");
            sql.append(" HAVING 1=1 ");
    		if(StringUtils.isNotBlank(tagCondition.get("couponFree").toString().split(",")[0])){
    			sql.append(" and COUNT(ci.COUPON_INSTANCE_OWNER) >= ").append(tagCondition.get("couponFree").toString().split(",")[0]);
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("couponFree").toString().split(",")[1])){
    			sql.append(" and COUNT(ci.COUPON_INSTANCE_OWNER) >= ").append(tagCondition.get("couponFree").toString().split(",")[1]);
    		}
		}
        System.out.println("tagSqlCoupon-----"+sql.toString());
        return sql.toString();
	}
	
	public String getTagQuerySQLCoupon1(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
        sql.append(select_begin_coupon0);
    	if(tagCondition.containsKey("couponUsed")){
			sql.append(" where ci.STATUS_ID = ").append(StatusConstant.COUPON_INSTANCE_USED.getId());
    		sql.append(" group by ci.COUPON_INSTANCE_OWNER ");
            sql.append(" HAVING 1=1 ");
			if(StringUtils.isNotBlank(tagCondition.get("couponUsed").toString().split(",")[0])){
    			sql.append(" and COUNT(ci.COUPON_INSTANCE_OWNER) >= ").append(tagCondition.get("couponUsed").toString().split(",")[0]);
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("couponUsed").toString().split(",")[1])){
    			sql.append(" and COUNT(ci.COUPON_INSTANCE_OWNER) >= ").append(tagCondition.get("couponUsed").toString().split(",")[1]);
    		}
		}
        System.out.println("tagSqlCoupon-----"+sql.toString());
        return sql.toString();
	}
	
	public String getTagQuerySQLOrder0(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
        sql.append(select_begin_order0);
        if(tagCondition.containsKey("orderStore")){
        	sql.append(" AND oh.STORE_ID in ( ").append(tagCondition.get("orderStore").toString()).append(" ) "); 
        }
        System.out.println("tagSqlOrder-----"+sql.toString());
        return sql.toString();
	}
	
	public String getTagQuerySQLOrder1(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
        sql.append(select_begin_order0);
        if(tagCondition.containsKey("orderCount")){
			if(StringUtils.isNotBlank(tagCondition.get("orderCount").toString().split(",")[0])){
    			sql.append(" AND oh.CREATE_TIME >= ").append(tagCondition.get("orderCount").toString().split(",")[0]+"000000");
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("orderCount").toString().split(",")[1])){
    			sql.append(" AND oh.CREATE_TIME <= ").append(tagCondition.get("orderCount").toString().split(",")[1]+"235959");
    		}
    		sql.append(" GROUP BY oh.MEMBER_ID HAVING 1=1 ");
    		if(StringUtils.isNotBlank(tagCondition.get("orderCount").toString().split(",")[2])){
    			sql.append(" AND COUNT(oh.MEMBER_ID) >= ").append(tagCondition.get("orderCount").toString().split(",")[2]);
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("orderCount").toString().split(",")[3])){
    			sql.append(" AND COUNT(oh.MEMBER_ID) <= ").append(tagCondition.get("orderCount").toString().split(",")[3]);
    		}
		}
        System.out.println("tagSqlOrder-----"+sql.toString());
        return sql.toString();
	}
	
	public String getTagQuerySQLOrder2(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
        sql.append(select_begin_order0);
        if(tagCondition.containsKey("orderGrand")){
			if(StringUtils.isNotBlank(tagCondition.get("orderGrand").toString().split(",")[0])){
    			sql.append(" AND oh.CREATE_TIME >= ").append(tagCondition.get("orderGrand").toString().split(",")[0]+"000000");
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("orderGrand").toString().split(",")[1])){
    			sql.append(" AND oh.CREATE_TIME <= ").append(tagCondition.get("orderGrand").toString().split(",")[1]+"235959");
    		}
    		sql.append(" GROUP BY oh.MEMBER_ID HAVING 1=1 ");
    		if(StringUtils.isNotBlank(tagCondition.get("orderGrand").toString().split(",")[2])){
    			sql.append(" AND SUM(oh.ORDER_AMOUNT) >= ").append(tagCondition.get("orderGrand").toString().split(",")[2]);
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("orderGrand").toString().split(",")[3])){
    			sql.append(" AND SUM(oh.ORDER_AMOUNT) <= ").append(tagCondition.get("orderGrand").toString().split(",")[3]);
    		}
		}
        System.out.println("tagSqlOrder-----"+sql.toString());
        return sql.toString();
	}
	
	public String getTagQuerySQLOrder3(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
        sql.append(select_begin_order0);
        if(tagCondition.containsKey("orderOne")){
			if(StringUtils.isNotBlank(tagCondition.get("orderOne").toString().split(",")[0])){
    			sql.append(" AND oh.CREATE_TIME >= ").append(tagCondition.get("orderOne").toString().split(",")[0]+"000000");
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("orderOne").toString().split(",")[1])){
    			sql.append(" AND oh.CREATE_TIME <= ").append(tagCondition.get("orderOne").toString().split(",")[1]+"235959");
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("orderOne").toString().split(",")[2])){
    			sql.append(" AND oh.ORDER_AMOUNT >= ").append(tagCondition.get("orderOne").toString().split(",")[2]);
    		}
    		if(StringUtils.isNotBlank(tagCondition.get("orderOne").toString().split(",")[3])){
    			sql.append(" AND oh.ORDER_AMOUNT <= ").append(tagCondition.get("orderOne").toString().split(",")[3]);
    		}
    		sql.append(" GROUP BY oh.MEMBER_ID ");
		}
        System.out.println("tagSqlOrder-----"+sql.toString());
        return sql.toString();
	}
	
	/**
	 * 临时表join
	 * @param tagCondition
	 * @return
	 */
	public String getTagQueryResult(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
		/*select tm.memberId from tmp_member tm
		inner join tmp_coupon tc on tm.memberId = tc.memberId
		inner join tmp_points tp on tm.memberId = tp.memberId
		inner join tmp_order td on tm.memberId = td.memberId;*/
		boolean oneFlag = false;
		boolean twoFlag = false;
		boolean threeFlag = false;		
		
		sql.append(" select ");
		if(tagCondition.containsValue(10L)){
			oneFlag = true;
			sql.append(" a.memberId from ").append(resultMap.get(10L)).append(" a ");
		}
		if(tagCondition.containsValue(20L) && oneFlag){
			twoFlag = true;
			sql.append(" inner join ").append(resultMap.get(20L)).append(" b on b.memberId = a.memberId");
		}else if(tagCondition.containsValue(20L) && !oneFlag){
			twoFlag = true;
			sql.append(" b.memberId from ").append(resultMap.get(20L)).append(" b ");
		}
		
		if((tagCondition.containsValue(30L) || tagCondition.containsValue(31L) && (oneFlag || twoFlag))){
			threeFlag = true;
			if(oneFlag){
				sql.append(" inner join ").append(resultMap.get(30L)).append(" c on c.memberId = a.memberId");
			}else if(twoFlag){
				sql.append(" inner join ").append(resultMap.get(30L)).append(" c on c.memberId = b.memberId");
			}
		}else if((tagCondition.containsValue(30L) || tagCondition.containsValue(31L) && !oneFlag && !twoFlag)){
			threeFlag = true;
			sql.append(" c.memberId from ").append(resultMap.get(30L)).append(" c ");
		}
		
		if((tagCondition.containsValue(40L) || tagCondition.containsValue(41L) || tagCondition.containsValue(42L) || tagCondition.containsValue(43L)) && (oneFlag || twoFlag || threeFlag)){
			if(oneFlag){
				sql.append(" inner join ").append(resultMap.get(40L)).append(" d on d.memberId = a.memberId");
			}else if(twoFlag){
				sql.append(" inner join ").append(resultMap.get(40L)).append(" d on d.memberId = b.memberId");
			}else if(threeFlag){
				sql.append(" inner join ").append(resultMap.get(40L)).append(" d on d.memberId = c.memberId");
			}
		}else if((tagCondition.containsValue(40L) || tagCondition.containsValue(41L) || tagCondition.containsValue(42L) || tagCondition.containsValue(43L)) && !oneFlag && !twoFlag && !threeFlag){
			sql.append(" d.memberId from ").append(resultMap.get(40L)).append(" d ");
		}
		
		return sql.toString();
	}
	
	/**
	 * 删除临时表sql
	 * @param tagCondition
	 * @return
	 */
	public String delTmp(Map<String,Object> tagCondition){
		StringBuffer sql = new StringBuffer();
		sql.append(" DROP TABLE if exists tmp_member,tmp_coupon,tmp_points,tmp_order ");
		return sql.toString();
	}
	
}
