package com.bw.adv.module.member.message.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.message.model.MemberMessageItemBrowse;

public interface MemberMessageItemBrowseMapper extends BaseMapper<MemberMessageItemBrowse>{

}