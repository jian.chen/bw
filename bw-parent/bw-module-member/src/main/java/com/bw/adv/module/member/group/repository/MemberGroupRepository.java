
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.group.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.group.mapper.MemberGroupMapper;
import com.bw.adv.module.member.group.model.MemberGroup;
import com.bw.adv.module.member.group.search.MemberGroupSearch;

/**
 * ClassName:MemberGroupRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:51:06 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberGroupRepository extends BaseRepository<MemberGroup, MemberGroupMapper>{
	
	Long findCountSearchAuto(MemberGroupSearch search);

	List<MemberGroup> findListSearchAuto(MemberGroupSearch search);
	
	MemberGroup findEntitySearchAuto(MemberGroupSearch search);
}

