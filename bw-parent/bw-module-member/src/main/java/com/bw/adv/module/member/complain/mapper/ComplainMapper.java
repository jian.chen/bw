package com.bw.adv.module.member.complain.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.complain.model.Complain;
import com.bw.adv.module.member.complain.model.exp.ComplainExp;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ComplainMapper extends BaseMapper<Complain>{
    int deleteByPrimaryKey(Long complainId);

    int insert(Complain record);

    int insertSelective(Complain record);

    Complain selectByPrimaryKey(Long complainId);

    int updateByPrimaryKeySelective(Complain record);

    int updateByPrimaryKey(Complain record);
    
    List<ComplainExp> selectByExa(@Param("example")Example example,Page<ComplainExp> page);

    ComplainExp selectDetailByPk(Long complainId);
}