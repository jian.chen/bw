

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.mapper.GradeConfMapper;
import com.bw.adv.module.member.model.GradeConf;
import com.bw.adv.module.member.repository.GradeConfRepository;

/**
 * ClassName: GradeConfRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月14日 下午7:34:04 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class GradeConfRepositoryImpl extends BaseRepositoryImpl<GradeConf, GradeConfMapper> implements GradeConfRepository{

	@Override
	protected Class<GradeConfMapper> getMapperClass() {
		
		return GradeConfMapper.class;
	}

	@Override
	public GradeConf findGradeConfByPk(Long gradeConfId) {
		return this.getMapper().selectByPrimaryKey(gradeConfId);
	}
	
}