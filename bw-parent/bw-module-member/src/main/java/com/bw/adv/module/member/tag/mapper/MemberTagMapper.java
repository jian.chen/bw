package com.bw.adv.module.member.tag.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.search.MemberTagSearch;

public interface MemberTagMapper extends BaseMapper<MemberTag>{
    
    Long selectCountSearchAuto(@Param("MemberTagSearch")MemberTagSearch search);

   	List<MemberTag> selectListSearchAuto(@Param("MemberTagSearch")MemberTagSearch search);
   	
   	MemberTag selectEntitySearchAuto(@Param("MemberTagSearch")MemberTagSearch search);
   	
   	/**
	 * 查询有自动更新节点的标签
	 * @param type
	 * @param node
	 * @return
	 */
	List<MemberTag> selectTagByTypeAndNode(@Param("updateType")String updateType,@Param("updateNode")String updateNode);
	
	/**
	 * 根据标签状态查询标签列表
	 * @param statusId
	 * @return
	 * @author cuicd
	 */
	List<MemberTag> selectMemberTagByStatusId(@Param("statusId")Long statusId);
	
	/**
	 * selectTagByIdList:根据标签Id查询标签. <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * Date: 2016年2月19日 上午10:53:07 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang(*^_^*)
	 * @version jdk1.7
	 * @param memberTagIdList
	 * @return
	 */
	List<MemberTag> selectTagByIdList(String[] memberTagIdList);
	
}