package com.bw.adv.module.member.complain.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.complain.model.ComplainType;

public interface ComplainTypeMapper extends BaseMapper<ComplainType>{
    List<ComplainType> selectAllComplainType();
}