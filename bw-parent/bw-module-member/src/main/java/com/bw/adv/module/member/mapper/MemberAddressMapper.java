package com.bw.adv.module.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.search.MemberAddressSearch;

public interface MemberAddressMapper extends BaseMapper<MemberAddress>{
    
    Long selectCountSearchAuto(@Param("MemberAddressSearch")MemberAddressSearch search);

	List<MemberAddress> selectListSearchAuto(@Param("MemberAddressSearch")MemberAddressSearch search);
	
	MemberAddress selectEntitySearchAuto(@Param("MemberAddressSearch")MemberAddressSearch search);

	MemberAddress selectMemberAddressDetail(@Param("memberAddressId")Long memberAddressId);
	
	/**
	 * 
	 * updateStatusByMemberId:(根据会员ID,修改此会员所有的地址为非默认). <br/>
	 * Date: 2015年12月15日 上午10:20:29 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	int updateStatusByMemberId(@Param("memberId")Long memberId,@Param("isDefault")String isDefault);
	
	/**
	 * 
	 * selectMemberDefault:(查询会员默认地址). <br/>
	 * Date: 2015年12月15日 上午11:52:54 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	List<MemberAddress> selectMemberDefault(@Param("memberId")Long memberId);
	
	/**
	 * selectMemberAddressByMemberId:(根据用户memberId查询用户受奖地址). <br/>
	 * Date: 2015年12月17日 下午2:08:35 <br/>
	 * scrmVersion 1.0
	 * @author june
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberAddress selectMemberAddressByMemberId(@Param("memberId")Long memberId);
}