/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeConvertRecordSearch.java
 * Package Name:com.sage.scrm.module.member.search
 * Date:2015年8月11日下午1:44:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.search;

import com.bw.adv.module.member.BaseSearch;

/**
 * ClassName:GradeConvertRecordSearch <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:44:24 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class SceneGameInstanceSearch extends BaseSearch {
	 public SceneGameInstanceSearch() {
		 super(Default_Rows, Default_Page);
	 }

	 private String likeInstanceName;
	 
     private String greaterEqualCreateTime;
     
     private String lessEqualCreateTime;

	public String getLikeInstanceName() {
		return likeInstanceName;
	}

	public void setLikeInstanceName(String likeInstanceName) {
		this.likeInstanceName = likeInstanceName;
	}

	public String getGreaterEqualCreateTime() {
		return greaterEqualCreateTime;
	}

	public void setGreaterEqualCreateTime(String greaterEqualCreateTime) {
		this.greaterEqualCreateTime = greaterEqualCreateTime;
	}

	public String getLessEqualCreateTime() {
		return lessEqualCreateTime;
	}

	public void setLessEqualCreateTime(String lessEqualCreateTime) {
		this.lessEqualCreateTime = lessEqualCreateTime;
	}
	    
}

