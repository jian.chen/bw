package com.bw.adv.module.member.search;

import com.bw.adv.module.member.BaseSearch;


public class MemberAccountSearch extends BaseSearch {

     public MemberAccountSearch() {
    	 super(Default_Rows, Default_Page);
	}
	
	

	 private Long equalMemberAccountId;
	 private Long equalMemberId;
	 private Long equalAccountBalance;
	 private Long equalPointsBalance;
	 private Long equalTotalConsumption;
	 private Long equalTotalAccount;
	 private Long equalTotalPoints;
    
     private String equalLastAccountTime;
     private String likeLastAccountTime;
     private String greaterEqualLastAccountTime;
     private String lessEqualLastAccountTime;
     
    
     private String equalLastPointsTime;
     private String likeLastPointsTime;
     private String greaterEqualLastPointsTime;
     private String lessEqualLastPointsTime;


	public Long getEqualMemberAccountId() {
		return equalMemberAccountId;
	}

	public void setEqualMemberAccountId(Long equalMemberAccountId) {
		this.equalMemberAccountId = equalMemberAccountId;
	}
    

	public Long getEqualMemberId() {
		return equalMemberId;
	}

	public void setEqualMemberId(Long equalMemberId) {
		this.equalMemberId = equalMemberId;
	}
    

	public Long getEqualAccountBalance() {
		return equalAccountBalance;
	}

	public void setEqualAccountBalance(Long equalAccountBalance) {
		this.equalAccountBalance = equalAccountBalance;
	}
    

	public Long getEqualPointsBalance() {
		return equalPointsBalance;
	}

	public void setEqualPointsBalance(Long equalPointsBalance) {
		this.equalPointsBalance = equalPointsBalance;
	}
    

	public Long getEqualTotalConsumption() {
		return equalTotalConsumption;
	}

	public void setEqualTotalConsumption(Long equalTotalConsumption) {
		this.equalTotalConsumption = equalTotalConsumption;
	}
    

	public Long getEqualTotalAccount() {
		return equalTotalAccount;
	}

	public void setEqualTotalAccount(Long equalTotalAccount) {
		this.equalTotalAccount = equalTotalAccount;
	}
    

	public Long getEqualTotalPoints() {
		return equalTotalPoints;
	}

	public void setEqualTotalPoints(Long equalTotalPoints) {
		this.equalTotalPoints = equalTotalPoints;
	}
    

    
    public String getEqualLastAccountTime() {
		return equalLastAccountTime;
	}

	public void setEqualLastAccountTime(String equalLastAccountTime) {
		this.equalLastAccountTime = equalLastAccountTime;
	}
    
    
    public String getLikeLastAccountTime() {
		return likeLastAccountTime;
	}

	public void setLikeLastAccountTime(String likeLastAccountTime) {
		this.likeLastAccountTime = likeLastAccountTime;
	}
    
   public String getGreaterEqualLastAccountTime() {
		return greaterEqualLastAccountTime;
	}

	public void setGreaterEqualLastAccountTime(String greaterEqualLastAccountTime) {
		this.greaterEqualLastAccountTime = greaterEqualLastAccountTime;
	}
    
     public String getLessEqualLastAccountTime() {
		return lessEqualLastAccountTime;
	}

	public void setLessEqualLastAccountTime(String lessEqualLastAccountTime) {
		this.lessEqualLastAccountTime = lessEqualLastAccountTime;
	}
    
    
     
    
    public String getEqualLastPointsTime() {
		return equalLastPointsTime;
	}

	public void setEqualLastPointsTime(String equalLastPointsTime) {
		this.equalLastPointsTime = equalLastPointsTime;
	}
    
    
    public String getLikeLastPointsTime() {
		return likeLastPointsTime;
	}

	public void setLikeLastPointsTime(String likeLastPointsTime) {
		this.likeLastPointsTime = likeLastPointsTime;
	}
    
   public String getGreaterEqualLastPointsTime() {
		return greaterEqualLastPointsTime;
	}

	public void setGreaterEqualLastPointsTime(String greaterEqualLastPointsTime) {
		this.greaterEqualLastPointsTime = greaterEqualLastPointsTime;
	}
    
     public String getLessEqualLastPointsTime() {
		return lessEqualLastPointsTime;
	}

	public void setLessEqualLastPointsTime(String lessEqualLastPointsTime) {
		this.lessEqualLastPointsTime = lessEqualLastPointsTime;
	}
    
    
     
	
}
