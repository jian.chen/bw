package com.bw.adv.module.member.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberPrism;
import com.bw.adv.module.member.repository.MemberPrismRepository;
import com.bw.adv.module.member.tag.mapper.MemberPrismMapper;

@Repository
public class MemberPrismRepositoryImpl extends BaseRepositoryImpl<MemberPrism, MemberPrismMapper> implements MemberPrismRepository{

	@Override
	protected Class<MemberPrismMapper> getMapperClass() {
		return MemberPrismMapper.class;
	}

	@Override
	public List<MemberPrism> selectAllPrism(Page<MemberPrism> templatePage) {
		return this.getMapper().selectAllPrism(templatePage);
	}

}
