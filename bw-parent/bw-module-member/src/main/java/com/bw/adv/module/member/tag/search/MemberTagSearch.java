package com.bw.adv.module.member.tag.search;

import com.bw.adv.module.member.BaseSearch;


public class MemberTagSearch extends BaseSearch {

     public MemberTagSearch() {
    	 super(Default_Rows, Default_Page);
	}
	
    private Long equalMemberId;

	private Long equalMemberTagId;
    
     private String equalMemberTagName;
     private String likeMemberTagName;
     private String greaterEqualMemberTagName;
     private String lessEqualMemberTagName;
     
    
     private String equalExecuteType;
     private String likeExecuteType;
     private String greaterEqualExecuteType;
     private String lessEqualExecuteType;
     
    
     private String equalConditionSql;
     private String likeConditionSql;
     private String greaterEqualConditionSql;
     private String lessEqualConditionSql;
     
    
     private String equalConditionRelation;
     private String likeConditionRelation;
     private String greaterEqualConditionRelation;
     private String lessEqualConditionRelation;
     
    
     private String equalRemark;
     private String likeRemark;
     private String greaterEqualRemark;
     private String lessEqualRemark;
     
    
     private String equalCreateTime;
     private String likeCreateTime;
     private String greaterEqualCreateTime;
     private String lessEqualCreateTime;
     
    
     private String equalUpdateTime;
     private String likeUpdateTime;
     private String greaterEqualUpdateTime;
     private String lessEqualUpdateTime;
     
	 private Long equalStatusId;
    
	 private Long notEqualStatusId;
    
    

	public Long getNotEqualStatusId() {
		return notEqualStatusId;
	}

	public void setNotEqualStatusId(Long notEqualStatusId) {
		this.notEqualStatusId = notEqualStatusId;
	}

	public Long getEqualMemberTagId() {
		return equalMemberTagId;
	}

	public void setEqualMemberTagId(Long equalMemberTagId) {
		this.equalMemberTagId = equalMemberTagId;
	}
    

    
    public String getEqualMemberTagName() {
		return equalMemberTagName;
	}

	public void setEqualMemberTagName(String equalMemberTagName) {
		this.equalMemberTagName = equalMemberTagName;
	}
    
    
    public String getLikeMemberTagName() {
		return likeMemberTagName;
	}

	public void setLikeMemberTagName(String likeMemberTagName) {
		this.likeMemberTagName = likeMemberTagName;
	}
    
   public String getGreaterEqualMemberTagName() {
		return greaterEqualMemberTagName;
	}

	public void setGreaterEqualMemberTagName(String greaterEqualMemberTagName) {
		this.greaterEqualMemberTagName = greaterEqualMemberTagName;
	}
    
     public String getLessEqualMemberTagName() {
		return lessEqualMemberTagName;
	}

	public void setLessEqualMemberTagName(String lessEqualMemberTagName) {
		this.lessEqualMemberTagName = lessEqualMemberTagName;
	}
    
    
     
    
    public String getEqualExecuteType() {
		return equalExecuteType;
	}

	public void setEqualExecuteType(String equalExecuteType) {
		this.equalExecuteType = equalExecuteType;
	}
    
    
    public String getLikeExecuteType() {
		return likeExecuteType;
	}

	public void setLikeExecuteType(String likeExecuteType) {
		this.likeExecuteType = likeExecuteType;
	}
    
   public String getGreaterEqualExecuteType() {
		return greaterEqualExecuteType;
	}

	public void setGreaterEqualExecuteType(String greaterEqualExecuteType) {
		this.greaterEqualExecuteType = greaterEqualExecuteType;
	}
    
     public String getLessEqualExecuteType() {
		return lessEqualExecuteType;
	}

	public void setLessEqualExecuteType(String lessEqualExecuteType) {
		this.lessEqualExecuteType = lessEqualExecuteType;
	}
    
    
     
    
    public String getEqualConditionSql() {
		return equalConditionSql;
	}

	public void setEqualConditionSql(String equalConditionSql) {
		this.equalConditionSql = equalConditionSql;
	}
    
    
    public String getLikeConditionSql() {
		return likeConditionSql;
	}

	public void setLikeConditionSql(String likeConditionSql) {
		this.likeConditionSql = likeConditionSql;
	}
    
   public String getGreaterEqualConditionSql() {
		return greaterEqualConditionSql;
	}

	public void setGreaterEqualConditionSql(String greaterEqualConditionSql) {
		this.greaterEqualConditionSql = greaterEqualConditionSql;
	}
    
     public String getLessEqualConditionSql() {
		return lessEqualConditionSql;
	}

	public void setLessEqualConditionSql(String lessEqualConditionSql) {
		this.lessEqualConditionSql = lessEqualConditionSql;
	}
    
    
     
    
    public String getEqualConditionRelation() {
		return equalConditionRelation;
	}

	public void setEqualConditionRelation(String equalConditionRelation) {
		this.equalConditionRelation = equalConditionRelation;
	}
    
    
    public String getLikeConditionRelation() {
		return likeConditionRelation;
	}

	public void setLikeConditionRelation(String likeConditionRelation) {
		this.likeConditionRelation = likeConditionRelation;
	}
    
   public String getGreaterEqualConditionRelation() {
		return greaterEqualConditionRelation;
	}

	public void setGreaterEqualConditionRelation(String greaterEqualConditionRelation) {
		this.greaterEqualConditionRelation = greaterEqualConditionRelation;
	}
    
     public String getLessEqualConditionRelation() {
		return lessEqualConditionRelation;
	}

	public void setLessEqualConditionRelation(String lessEqualConditionRelation) {
		this.lessEqualConditionRelation = lessEqualConditionRelation;
	}
    
    
     
    
    public String getEqualRemark() {
		return equalRemark;
	}

	public void setEqualRemark(String equalRemark) {
		this.equalRemark = equalRemark;
	}
    
    
    public String getLikeRemark() {
		return likeRemark;
	}

	public void setLikeRemark(String likeRemark) {
		this.likeRemark = likeRemark;
	}
    
   public String getGreaterEqualRemark() {
		return greaterEqualRemark;
	}

	public void setGreaterEqualRemark(String greaterEqualRemark) {
		this.greaterEqualRemark = greaterEqualRemark;
	}
    
     public String getLessEqualRemark() {
		return lessEqualRemark;
	}

	public void setLessEqualRemark(String lessEqualRemark) {
		this.lessEqualRemark = lessEqualRemark;
	}
    
    
     
    
    public String getEqualCreateTime() {
		return equalCreateTime;
	}

	public void setEqualCreateTime(String equalCreateTime) {
		this.equalCreateTime = equalCreateTime;
	}
    
    
    public String getLikeCreateTime() {
		return likeCreateTime;
	}

	public void setLikeCreateTime(String likeCreateTime) {
		this.likeCreateTime = likeCreateTime;
	}
    
   public String getGreaterEqualCreateTime() {
		return greaterEqualCreateTime;
	}

	public void setGreaterEqualCreateTime(String greaterEqualCreateTime) {
		this.greaterEqualCreateTime = greaterEqualCreateTime;
	}
    
     public String getLessEqualCreateTime() {
		return lessEqualCreateTime;
	}

	public void setLessEqualCreateTime(String lessEqualCreateTime) {
		this.lessEqualCreateTime = lessEqualCreateTime;
	}
    
    
     
    
    public String getEqualUpdateTime() {
		return equalUpdateTime;
	}

	public void setEqualUpdateTime(String equalUpdateTime) {
		this.equalUpdateTime = equalUpdateTime;
	}
    
    
    public String getLikeUpdateTime() {
		return likeUpdateTime;
	}

	public void setLikeUpdateTime(String likeUpdateTime) {
		this.likeUpdateTime = likeUpdateTime;
	}
    
   public String getGreaterEqualUpdateTime() {
		return greaterEqualUpdateTime;
	}

	public void setGreaterEqualUpdateTime(String greaterEqualUpdateTime) {
		this.greaterEqualUpdateTime = greaterEqualUpdateTime;
	}
    
     public String getLessEqualUpdateTime() {
		return lessEqualUpdateTime;
	}

	public void setLessEqualUpdateTime(String lessEqualUpdateTime) {
		this.lessEqualUpdateTime = lessEqualUpdateTime;
	}
    
    
     
	public Long getEqualStatusId() {
		return equalStatusId;
	}

	public void setEqualStatusId(Long equalStatusId) {
		this.equalStatusId = equalStatusId;
	}
    
	 public Long getEqualMemberId() {
		return equalMemberId;
	}

	public void setEqualMemberId(Long equalMemberId) {
		this.equalMemberId = equalMemberId;
	}

	
}
