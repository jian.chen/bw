/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberTagRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.tag.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.tag.mapper.MemberTagMapper;
import com.bw.adv.module.member.tag.model.MemberTag;
import com.bw.adv.module.member.tag.search.MemberTagSearch;

/**
 * ClassName:MemberTagRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:51:06 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberTagRepository extends BaseRepository<MemberTag, MemberTagMapper>{
	
	Long findCountSearchAuto(MemberTagSearch search);

	List<MemberTag> findListSearchAuto(MemberTagSearch search);
	
	MemberTag findEntitySearchAuto(MemberTagSearch search);
	
	/**
	 * 查询有自动更新节点的标签
	 * @param type
	 * @param node
	 * @return
	 */
	List<MemberTag> findTagByTypeAndNode(String type,String node);
	
	/**
	 * 根据标签状态查询标签列表
	 * @param statusId
	 * @return
	 * @author cuicd
	 */
	List<MemberTag> findMemberTagByStatusId(Long statusId);
	
	/**
	 * findTagByIdList:根据标签Id查询标签. <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * Date: 2016年2月19日 上午10:57:53 <br/>
	 * scrmVersion 1.0
	 * @author Tab.Wang(*^_^*)
	 * @version jdk1.7
	 * @param memberTagIdList
	 * @return
	 */
	List<MemberTag> findTagByIdList(String[] memberTagIdList);
	
}

