package com.bw.adv.module.member.mapper;

import com.bw.adv.module.member.model.MemberSummaryDay;

public interface MemberSummaryDayMapper {
    int deleteByPrimaryKey(Long summaryId);

    int insert(MemberSummaryDay record);

    int insertSelective(MemberSummaryDay record);

    MemberSummaryDay selectByPrimaryKey(Long summaryId);

    int updateByPrimaryKeySelective(MemberSummaryDay record);

    int updateByPrimaryKey(MemberSummaryDay record);
    
    /**
     * 根据日期查找统计结果
     * @param date
     * @return
     */
    MemberSummaryDay selectBySummaryDate(String date);
}