

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.api.grade.dto.GradeDto;
import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.mapper.GradeMapper;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.repository.GradeRepository;
import com.bw.adv.module.member.search.GradeSearch;

/**
 * ClassName: GradeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月14日 下午8:00:29 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class GradeRepositoryImpl extends BaseRepositoryImpl<Grade, GradeMapper> implements GradeRepository{
	
	@Override
	protected Class<GradeMapper> getMapperClass() {
		return GradeMapper.class;
	}

	@Override
	public Long findCountSearchAuto(GradeSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<Grade> findListSearchAuto(GradeSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public Grade findEntitySearchAuto(GradeSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}

	@Override
	public List<Grade> findList() {
		
		return this.getMapper().selectList();
	}

	@Override
	public List<GradeDto> findGradeDtoList(Example example,Page<GradeDto> page) {
		
		return this.getMapper().selectByDynamicCondition(example, page);
	}

	@Override
	public GradeDto findGradeDtoList(Example example) {
		
		return this.getMapper().selectGradeByDynamicCondition(example);
	}

	@Override
	public List<Grade> queryGradeListForJS() {
		return this.getMapper().queryGradeListForJS();
	}

}