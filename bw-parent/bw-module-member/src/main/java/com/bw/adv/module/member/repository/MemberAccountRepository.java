
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberAccountRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository;

import java.math.BigDecimal;
import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.mapper.MemberAccountMapper;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.search.MemberAccountSearch;

/**
 * ClassName:MemberAccountRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:51:06 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberAccountRepository extends BaseRepository<MemberAccount, MemberAccountMapper>{
	
	Long findCountSearchAuto(MemberAccountSearch search);

	List<MemberAccount> findListSearchAuto(MemberAccountSearch search);
	
	MemberAccount findEntitySearchAuto(MemberAccountSearch search);
	
	/**
	 * findByMemberId:(根据会员ID查询账户信息). <br/>
	 * Date: 2015-9-16 下午7:10:30 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	MemberAccount findByMemberId(Long memberId);
	
	
	/**
	 * findByMemberIdForUpdate:(根据会员ID查询账户信息). <br/>
	 * Date: 2015-9-16 下午7:10:30 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param search
	 * @return
	 */
	MemberAccount findByMemberIdForUpdate(Long memberId);

	/**
	 * updateBatchByCleanPointsBalance:(积分清理，更新账户积分余额). <br/>
	 * Date: 2015-10-9 下午8:25:39 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberAccountList
	 * @return
	 */
	int updateBatchByCleanPointsBalance(List<MemberAccount> memberAccountList);
	
	/**
     * 查询用户消费次数
     * @param times
     * @return
     */
    Long findMemberOrderCount(Long times1,Long times2,Long sort);
    
    
    /**
	 * updateForAddPoints:(账户新增积分). <br/>
	 * Date: 2016-1-6 下午4:14:16 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	int updateForAddPoints(Long memberId,BigDecimal points,String currentTime);
	
	/**
	 * updateForSubtractPoints:(账户扣减积分). <br/>
	 * Date: 2016-1-6 下午4:16:44 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param points
	 * @param currentTime
	 * @return
	 */
	int updateForSubtractPoints(Long memberId,BigDecimal points,String currentTime);
	
	/**
	 * updateForSubtractPointsWithCancelOrder:(取消单的扣减积分). <br/>
	 * Date: 2016-4-11 下午5:34:43 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param points
	 * @param currentTime
	 * @return
	 */
	int updateForSubtractPointsWithCancelOrder(Long memberId,BigDecimal points,String currentTime);
    
}

