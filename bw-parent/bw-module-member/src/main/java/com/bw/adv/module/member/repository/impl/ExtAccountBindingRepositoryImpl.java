

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:ExtAccountBindingRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.mapper.ExtAccountBindingMapper;
import com.bw.adv.module.member.model.ExtAccountBinding;
import com.bw.adv.module.member.repository.ExtAccountBindingRepository;
import com.bw.adv.module.member.search.ExtAccountBindingSearch;

/**
 * ClassName:ExtAccountBindingRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ExtAccountBindingRepositoryImpl extends BaseRepositoryImpl<ExtAccountBinding, ExtAccountBindingMapper> implements ExtAccountBindingRepository{
	
	@Override
	protected Class<ExtAccountBindingMapper> getMapperClass() {
		return ExtAccountBindingMapper.class;
	}

	@Override
	public Long findCountSearchAuto(ExtAccountBindingSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<ExtAccountBinding> findListSearchAuto(ExtAccountBindingSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public ExtAccountBinding findEntitySearchAuto(ExtAccountBindingSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}
	
	@Override
	public ExtAccountBinding findByAccountAndTypeId(String bindingAccount,Long extAccountTypeId) {
		return this.getMapper().selectByAccountAndTypeId(bindingAccount,extAccountTypeId);
	}
	
	@Override
	public List<ExtAccountBinding> findByMemberIdAndTypeId(Long memberId,Long extAccountTypeId){
		return this.getMapper().selectByMemberIdAndTypeId(memberId,extAccountTypeId);
	}

}