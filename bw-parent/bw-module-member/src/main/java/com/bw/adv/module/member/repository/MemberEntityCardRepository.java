package com.bw.adv.module.member.repository;


import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.mapper.MemberEntityCardMapper;
import com.bw.adv.module.member.model.MemberEntityCard;

public interface MemberEntityCardRepository extends BaseRepository<MemberEntityCard, MemberEntityCardMapper> {

	/**
	 * 根据Issued查询卡信息
	 * @param issueId
	 * @param pageObj
	 * @return
	 */
	List<MemberEntityCard> selectMemberCardByIssueId(Long issueId,Page<MemberEntityCard> pageObj);
	
	/**
     * 根据卡号和状态查询实体卡
     * @param cardNo
     * @return
     */
    MemberEntityCard findCardByCardNo(String cardNo,Long statusId);
    
}
