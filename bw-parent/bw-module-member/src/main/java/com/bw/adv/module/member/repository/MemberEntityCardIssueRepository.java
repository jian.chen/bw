package com.bw.adv.module.member.repository;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.mapper.MemberEntityCardIssueMapper;
import com.bw.adv.module.member.model.MemberEntityCardIssue;

public interface MemberEntityCardIssueRepository extends BaseRepository<MemberEntityCardIssue, MemberEntityCardIssueMapper> {

	List<MemberEntityCardIssue> selectMemerCardByBatch(String batch,Page<MemberEntityCardIssue> pageObj);
	
	int insertMemberCardGetId(MemberEntityCardIssue record);
	
}
