package com.bw.adv.module.member.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.mapper.MemberSummaryDayMapper;
import com.bw.adv.module.member.model.MemberSummaryDay;

public interface MemberSummaryRepository extends BaseRepository<MemberSummaryDay, MemberSummaryDayMapper>{
	

    /**
     * 根据日期查找统计结果
     * @param date
     * @return
     */
    MemberSummaryDay selectBySummaryDate(String date);

}
