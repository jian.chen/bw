/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeConvertRecordRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.mapper.GradeConvertRecordMapper;
import com.bw.adv.module.member.model.GradeConvertRecord;
import com.bw.adv.module.member.model.exp.GradeConvertRecordExp;
import com.bw.adv.module.member.search.GradeConvertRecordSearch;

/**
 * ClassName:GradeConvertRecordRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:51:06 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface GradeConvertRecordRepository extends BaseRepository<GradeConvertRecord, GradeConvertRecordMapper>{
	
	
	Long findCountSearchAuto(GradeConvertRecordSearch search);

	List<GradeConvertRecordExp> findListSearchAuto(GradeConvertRecordSearch search);
	
	GradeConvertRecord findEntitySearchAuto(GradeConvertRecordSearch search);
}

