
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberAddressRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.mapper.MemberCardMapper;
import com.bw.adv.module.member.model.MemberCard;
import com.bw.adv.module.member.model.exp.MemberCardExp;

/**
 * ClassName: MemberCardRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-9-24 下午4:54:05 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface MemberCardRepository extends BaseRepository<MemberCard, MemberCardMapper>{
	
	/**
	 * findByMemberId:(根据会员ID，查询会员卡信息). <br/>
	 * Date: 2015-12-17 上午10:14:18 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberCard findByMemberId(Long memberId);

	/**
	 * findMemberCardByMemberId:(根据会员ID，查询会员卡扩展信息). <br/>
	 * Date: 2015-12-17 上午10:14:45 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	MemberCardExp findExpByMemberId(Long memberId);
	
	/**
	 * findByCardNo:(根据卡号查询会员卡). <br/>
	 * Date: 2015-11-16 下午5:43:25 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param cardNo
	 * @return
	 */
	MemberCard findByCardNo(String cardNo);

	int deleteCardByMemberId(Long memberId);
	
}

