package com.bw.adv.module.member.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.model.MemberPrismCondition;
import com.bw.adv.module.member.repository.MemberPrismConditionRepository;
import com.bw.adv.module.member.tag.mapper.MemberPrismConditionMapper;

@Repository
public class MemberPrismConditionRepositoryImpl extends BaseRepositoryImpl<MemberPrismCondition, MemberPrismConditionMapper> implements MemberPrismConditionRepository{

	@Override
	protected Class<MemberPrismConditionMapper> getMapperClass() {
		return MemberPrismConditionMapper.class;
	}

}
