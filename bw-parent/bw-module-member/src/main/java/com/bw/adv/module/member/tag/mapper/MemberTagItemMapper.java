package com.bw.adv.module.member.tag.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.tag.model.MemberTagItem;
import com.bw.adv.module.member.tag.search.MemberTagItemSearch;


public interface MemberTagItemMapper extends BaseMapper<MemberTagItem> {
	
	Long selectCountSearchAuto(@Param("MemberTagItemSearch")MemberTagItemSearch search);

   	List<MemberTagItem> selectListSearchAuto(@Param("MemberTagItemSearch")MemberTagItemSearch search);
   	
   	MemberTagItem selectEntitySearchAuto(@Param("MemberTagItemSearch")MemberTagItemSearch search);

	void deleteTagItemByTagId(Long memberTagId);
	
	void deleteTagItemByMemberId(Long memberId);

	void insertBatchMemberTagItem(@Param("list")List<MemberTagItem> addItemList);
	
	/**
	 * 插入标签记录（无视重复记录）
	 * @param addItemList
	 * @author cuicd
	 */
	void insertMemberTagItemIgnoreSame(@Param("list")List<MemberTagItem> addItemList);
}