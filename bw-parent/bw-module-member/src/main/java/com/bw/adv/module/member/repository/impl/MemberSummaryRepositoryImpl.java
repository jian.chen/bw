package com.bw.adv.module.member.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.mapper.MemberSummaryDayMapper;
import com.bw.adv.module.member.model.MemberSummaryDay;
import com.bw.adv.module.member.repository.MemberSummaryRepository;

@Repository
public class MemberSummaryRepositoryImpl extends BaseRepositoryImpl<MemberSummaryDay, MemberSummaryDayMapper> implements MemberSummaryRepository{

	@Override
	public MemberSummaryDay selectBySummaryDate(String date) {
		return this.getMapper().selectBySummaryDate(date);
	}

	@Override
	protected Class<MemberSummaryDayMapper> getMapperClass() {
		return MemberSummaryDayMapper.class;
	}

}
