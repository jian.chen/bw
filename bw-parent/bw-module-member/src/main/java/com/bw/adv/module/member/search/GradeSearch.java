/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeConvertRecordSearch.java
 * Package Name:com.sage.scrm.module.member.search
 * Date:2015年8月11日下午1:44:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.search;

import com.bw.adv.module.member.BaseSearch;

/**
 * ClassName:GradeConvertRecordSearch <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:44:24 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class GradeSearch extends BaseSearch {
	 public GradeSearch() {
		 super(Default_Rows, Default_Page);
		}

		 private Long equalGradeId;
		 private Long equalOrgId;
	    
	     private String equalGradeCode;
	     private String likeGradeCode;
	     private String greaterEqualGradeCode;
	     private String lessEqualGradeCode;
	     
	    
	     private String equalGradeName;
	     private String likeGradeName;
	     private String greaterEqualGradeName;
	     private String lessEqualGradeName;
	     
	    
	     private String equalGradeTitle;
	     private String likeGradeTitle;
	     private String greaterEqualGradeTitle;
	     private String lessEqualGradeTitle;
	     
	    
	     private String equalRemark;
	     private String likeRemark;
	     private String greaterEqualRemark;
	     private String lessEqualRemark;
	     
		 private Long equalSeq;
	    
	     private String equalIsActive;
	     private String likeIsActive;
	     private String greaterEqualIsActive;
	     private String lessEqualIsActive;
	     
	    
	    
	    
	    

		public Long getEqualGradeId() {
			return equalGradeId;
		}

		public void setEqualGradeId(Long equalGradeId) {
			this.equalGradeId = equalGradeId;
		}
	    

		public Long getEqualOrgId() {
			return equalOrgId;
		}

		public void setEqualOrgId(Long equalOrgId) {
			this.equalOrgId = equalOrgId;
		}
	    

	    
	    public String getEqualGradeCode() {
			return equalGradeCode;
		}

		public void setEqualGradeCode(String equalGradeCode) {
			this.equalGradeCode = equalGradeCode;
		}
	    
	    
	    public String getLikeGradeCode() {
			return likeGradeCode;
		}

		public void setLikeGradeCode(String likeGradeCode) {
			this.likeGradeCode = likeGradeCode;
		}
	    
	   public String getGreaterEqualGradeCode() {
			return greaterEqualGradeCode;
		}

		public void setGreaterEqualGradeCode(String greaterEqualGradeCode) {
			this.greaterEqualGradeCode = greaterEqualGradeCode;
		}
	    
	     public String getLessEqualGradeCode() {
			return lessEqualGradeCode;
		}

		public void setLessEqualGradeCode(String lessEqualGradeCode) {
			this.lessEqualGradeCode = lessEqualGradeCode;
		}
	    
	    
	     
	    
	    public String getEqualGradeName() {
			return equalGradeName;
		}

		public void setEqualGradeName(String equalGradeName) {
			this.equalGradeName = equalGradeName;
		}
	    
	    
	    public String getLikeGradeName() {
			return likeGradeName;
		}

		public void setLikeGradeName(String likeGradeName) {
			this.likeGradeName = likeGradeName;
		}
	    
	   public String getGreaterEqualGradeName() {
			return greaterEqualGradeName;
		}

		public void setGreaterEqualGradeName(String greaterEqualGradeName) {
			this.greaterEqualGradeName = greaterEqualGradeName;
		}
	    
	     public String getLessEqualGradeName() {
			return lessEqualGradeName;
		}

		public void setLessEqualGradeName(String lessEqualGradeName) {
			this.lessEqualGradeName = lessEqualGradeName;
		}
	    
	    
	     
	    
	    public String getEqualGradeTitle() {
			return equalGradeTitle;
		}

		public void setEqualGradeTitle(String equalGradeTitle) {
			this.equalGradeTitle = equalGradeTitle;
		}
	    
	    
	    public String getLikeGradeTitle() {
			return likeGradeTitle;
		}

		public void setLikeGradeTitle(String likeGradeTitle) {
			this.likeGradeTitle = likeGradeTitle;
		}
	    
	   public String getGreaterEqualGradeTitle() {
			return greaterEqualGradeTitle;
		}

		public void setGreaterEqualGradeTitle(String greaterEqualGradeTitle) {
			this.greaterEqualGradeTitle = greaterEqualGradeTitle;
		}
	    
	     public String getLessEqualGradeTitle() {
			return lessEqualGradeTitle;
		}

		public void setLessEqualGradeTitle(String lessEqualGradeTitle) {
			this.lessEqualGradeTitle = lessEqualGradeTitle;
		}
	    
	    
	     
	    
	    public String getEqualRemark() {
			return equalRemark;
		}

		public void setEqualRemark(String equalRemark) {
			this.equalRemark = equalRemark;
		}
	    
	    
	    public String getLikeRemark() {
			return likeRemark;
		}

		public void setLikeRemark(String likeRemark) {
			this.likeRemark = likeRemark;
		}
	    
	   public String getGreaterEqualRemark() {
			return greaterEqualRemark;
		}

		public void setGreaterEqualRemark(String greaterEqualRemark) {
			this.greaterEqualRemark = greaterEqualRemark;
		}
	    
	     public String getLessEqualRemark() {
			return lessEqualRemark;
		}

		public void setLessEqualRemark(String lessEqualRemark) {
			this.lessEqualRemark = lessEqualRemark;
		}
	    
	    
	     
		public Long getEqualSeq() {
			return equalSeq;
		}

		public void setEqualSeq(Long equalSeq) {
			this.equalSeq = equalSeq;
		}
	    

	    
	    public String getEqualIsActive() {
			return equalIsActive;
		}

		public void setEqualIsActive(String equalIsActive) {
			this.equalIsActive = equalIsActive;
		}
	    
	    
	    public String getLikeIsActive() {
			return likeIsActive;
		}

		public void setLikeIsActive(String likeIsActive) {
			this.likeIsActive = likeIsActive;
		}
	    
	   public String getGreaterEqualIsActive() {
			return greaterEqualIsActive;
		}

		public void setGreaterEqualIsActive(String greaterEqualIsActive) {
			this.greaterEqualIsActive = greaterEqualIsActive;
		}
	    
	     public String getLessEqualIsActive() {
			return lessEqualIsActive;
		}

		public void setLessEqualIsActive(String lessEqualIsActive) {
			this.lessEqualIsActive = lessEqualIsActive;
		}
	    
}

