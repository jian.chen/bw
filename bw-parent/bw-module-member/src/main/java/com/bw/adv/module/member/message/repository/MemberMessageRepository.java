
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberGroupItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.message.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.message.mapper.MemberMessageMapper;
import com.bw.adv.module.member.message.model.MemberMessage;
import com.bw.adv.module.member.message.model.NotReadMessage;
import com.bw.adv.module.member.message.model.exp.MemberMessageExp;


/**
 * ClassName: MemberMessageRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2016年2月22日 下午2:50:28 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface MemberMessageRepository extends BaseRepository<MemberMessage, MemberMessageMapper>{
	
	/**
	 * findList:查询消息中心列表<br/>
	 * Date: 2016年2月22日 下午2:56:39 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param Page
	 * @return
	 */
	List<MemberMessage> findList(Page<MemberMessage> Page);
	
	/**
	 * updateStatus:修改会员消息状态为已删除<br/>
	 * Date: 2016年2月25日 下午4:38:56 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberMessageId
	 */
	void updateStatus(Long memberMessageId);
	
	/**
	 * findListByMemberId:根据会员Id查询会员消息 <br/>
	 * Date: 2016年2月28日 下午11:38:59 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param memberId
	 * @param Page
	 * @return
	 */
	List<MemberMessageExp> findListByMemberId(Long memberId,Page<MemberMessageExp> Page);
}

