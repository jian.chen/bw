package com.bw.adv.module.member.group.search;

import com.bw.adv.module.member.BaseSearch;


public class MemberGroupItemSearch extends BaseSearch {

     public MemberGroupItemSearch() {
    	 super(Default_Rows, Default_Page);
	}
	
	

	 private Long equalMemberGroupItemId;
	 private Long equalMemberGroupId;
	 private Long equalMemberId;
    
    
    
    

	public Long getEqualMemberGroupItemId() {
		return equalMemberGroupItemId;
	}

	public void setEqualMemberGroupItemId(Long equalMemberGroupItemId) {
		this.equalMemberGroupItemId = equalMemberGroupItemId;
	}
    

	public Long getEqualMemberGroupId() {
		return equalMemberGroupId;
	}

	public void setEqualMemberGroupId(Long equalMemberGroupId) {
		this.equalMemberGroupId = equalMemberGroupId;
	}
    

	public Long getEqualMemberId() {
		return equalMemberId;
	}

	public void setEqualMemberId(Long equalMemberId) {
		this.equalMemberId = equalMemberId;
	}

}
