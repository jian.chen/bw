package com.bw.adv.module.member.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.bw.adv.api.grade.dto.GradeDto;
import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.model.Grade;
import com.bw.adv.module.member.search.GradeSearch;


public interface GradeMapper extends BaseMapper<Grade> {
	
    Long selectCountSearchAuto(@Param("GradeSearch")GradeSearch search);

   	List<Grade> selectListSearchAuto(@Param("GradeSearch")GradeSearch search);
   	
   	Grade selectEntitySearchAuto(@Param("GradeSearch")GradeSearch search);
   	
   	/**
   	 * selectList:查询等级列表<br/>
   	 * Date: 2015年9月6日 上午11:48:57 <br/>
   	 * scrmVersion 1.0
   	 * @author liuyi.wang
   	 * @version jdk1.7
   	 * @return
   	 */
   	List<Grade> selectList();
   	
   	/**
   	 * selectByDynamicCondition:根据条件查询等级 <br/>
   	 * Date: 2015年9月14日 下午5:42:17 <br/>
   	 * scrmVersion 1.0
   	 * @author liuyi.wang
   	 * @version jdk1.7
   	 * @return
   	 */
   	List<GradeDto> selectByDynamicCondition(@Param("example")Example example,Page<GradeDto> page);
   	
   	/**
   	 * selectByDynamicCondition:根据条件查询等级<br/>
   	 * Date: 2015年9月14日 下午8:13:27 <br/>
   	 * scrmVersion 1.0
   	 * @author liuyi.wang
   	 * @version jdk1.7
   	 * @param example
   	 * @return
   	 */
   	List<GradeDto> selectByDynamicCondition(@Param("example")Example example);
   	
   	/**
   	 * selectGradeByDynamicCondition:根据条件查会员等级 <br/>
   	 * Date: 2015年9月15日 下午3:50:09 <br/>
   	 * scrmVersion 1.0
   	 * @author liuyi.wang
   	 * @version jdk1.7
   	 * @param example
   	 * @return
   	 */
   	GradeDto selectGradeByDynamicCondition(@Param("example")Example example);
   	
   	@Select("select seq as value,grade_name as text from grade order by seq")
   	List<Grade> queryGradeListForJS();
}