package com.bw.adv.module.member.message.mapper;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.message.model.MemberMessageItem;
import com.bw.adv.module.member.message.model.NotReadMessage;

public interface MemberMessageItemMapper extends BaseMapper<MemberMessageItem>{
	
	void deleteByMessageId(Long memberMessageId);
	
	MemberMessageItem selectByMessageId(@Param("example")Example example);
	
	NotReadMessage selectNotReadMessage(Long memberId);
}