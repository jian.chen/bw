package com.bw.adv.module.member.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.member.model.MemberAccount;
import com.bw.adv.module.member.model.MemberAddress;
import com.bw.adv.module.member.search.MemberAccountSearch;

public interface MemberAccountMapper extends BaseMapper<MemberAddress>{
   Long selectCountSearchAuto(@Param("MemberAccountSearch")MemberAccountSearch search);

	List<MemberAccount> selectListSearchAuto(@Param("MemberAccountSearch")MemberAccountSearch search);
		
	MemberAccount selectEntitySearchAuto(@Param("MemberAccountSearch")MemberAccountSearch search);
	
	/**
	 * selectListSearchAuto:(根据会员ID查询账户信息). <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<MemberAccount> selectByMemberId(@Param("memberId")Long memberId);
	
	/**
	 * selectListSearchAuto:(根据会员ID查询账户信息). <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<MemberAccount> selectByMemberIdForUpdate(@Param("memberId")Long memberId);

	/**
	 * updateByCleanPointsBalance:(积分清理，批量更新账户积分余额). <br/>
	 * Date: 2015-10-9 下午8:27:05 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberAccountList
	 * @return
	 */
	int updateBatchByCleanPointsBalance(List<MemberAccount> memberAccountList);
	
	/**
	 * 查询用户消费次数
	 * @param times
	 * @return
	 */
	Long selectCountMemberOrder(@Param("times1")Long times1,@Param("times2")Long times2,@Param("sort")Long sort);
	
	/**
	 * updateForAddPoints:(账户新增积分). <br/>
	 * Date: 2016-1-6 下午4:14:16 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	int updateForAddPoints(Long memberId,BigDecimal points,String currentTime);
	
	/**
	 * updateForSubtractPoints:(账户扣减积分). <br/>
	 * Date: 2016-1-6 下午4:16:44 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param points
	 * @param currentTime
	 * @return
	 */
	int updateForSubtractPoints(Long memberId,BigDecimal points,String currentTime);
	
	/**
	 * updateForSubtractPointsWithCancelOrder:(取消单的扣减积分). <br/>
	 * Date: 2016-4-11 下午5:35:37 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @param points
	 * @param currentTime
	 * @return
	 */
	int updateForSubtractPointsWithCancelOrder(Long memberId,BigDecimal points,String currentTime);
	
}