

/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberExtInfoRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.mapper.MemberExtInfoMapper;
import com.bw.adv.module.member.model.MemberExtInfo;
import com.bw.adv.module.member.repository.MemberExtInfoRepository;

/**
 * ClassName:MemberExtInfoRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberExtInfoRepositoryImpl extends BaseRepositoryImpl<MemberExtInfo, MemberExtInfoMapper> implements MemberExtInfoRepository{
	
	@Override
	protected Class<MemberExtInfoMapper> getMapperClass() {
		return MemberExtInfoMapper.class;
	}

}