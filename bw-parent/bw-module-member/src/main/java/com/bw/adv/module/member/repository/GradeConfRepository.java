
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.mapper.GradeConfMapper;
import com.bw.adv.module.member.model.GradeConf;

/**
 * ClassName: GradeConfRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年9月6日 上午11:47:34 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface GradeConfRepository extends BaseRepository<GradeConf, GradeConfMapper>{
	
	/**
	 * findGradeConfByPk:根据主键查询等级配置信息<br/>
	 * Date: 2015年9月6日 上午11:47:39 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param gradeConfId
	 * @return
	 */
	GradeConf findGradeConfByPk(Long gradeConfId);
}

