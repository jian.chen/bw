package com.bw.adv.module.member.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.component.msg.model.SmsInstance;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.member.exception.MemberException;
import com.bw.adv.module.member.mapper.MemberMapper;
import com.bw.adv.module.member.model.Member;
import com.bw.adv.module.member.model.exp.MemberExp;
import com.bw.adv.module.member.repository.MemberRepository;
import com.bw.adv.module.member.search.MemberSearch;
import com.bw.adv.module.order.model.OrderHeader;
import com.bw.adv.module.points.model.exp.MemberPointsItemExp;
import com.bw.adv.module.report.model.MemberAge;
import com.bw.adv.module.report.model.MemberRegion;
import com.bw.adv.module.wechat.model.WechatFansMessage;

@Repository
public class MemberRepositoryImpl extends BaseRepositoryImpl<Member, MemberMapper> implements MemberRepository {
	
	@Override
	protected Class<MemberMapper> getMapperClass() {
		return MemberMapper.class;
	}

	@Override
	public Long findCountSearchAuto(MemberSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<MemberExp> findListSearchAuto(MemberSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}
	
	@Override
	public List<MemberExp> selectListSearchAutoByOffset(MemberSearch search, Long offset, Long lastId) {
		return this.getMapper().selectListSearchAutoByOffset(search, offset, lastId);
	}
	
	@Override
	public List<MemberExp> selectListSearchAutoByOffsetNoCondition(MemberSearch search, Long offset, Long lastId) {
		return this.getMapper().selectListSearchAutoByOffsetNoCondition(search, offset, lastId);
	}

	@Override
	public Member findEntitySearchAuto(MemberSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}

	@Override
	public List<MemberPointsItemExp> findMemberPointsByMemberId(MemberSearch search) {

		return this.getMapper().selectMemberPointsByMemberId(search);
	}

	@Override
	public List<OrderHeader> findOrderHeaderByMemberId(MemberSearch search) {
		
		return this.getMapper().selectOrderHeaderByMemberId(search);
	}

	@Override
	public List<CouponInstance> findCouponInstanceByMemberId(MemberSearch search) {
		
		return this.getMapper().selectCouponInstanceByMemberId(search);
	}

	@Override
	public List<WechatFansMessage> findWechatMessageByMemberId(MemberSearch search) {
		
		return this.getMapper().selectWechatMessageByMemberId(search);
	}

	@Override
	public List<SmsInstance> findSmsInstanceByMemberId(MemberSearch search) {
		
		return this.getMapper().selectSmsInstanceByMemberId(search);
	}

	@Override
	public List<Long> findListBySql(String sql) {
		
		return this.getMapper().selectListBySql(sql);
	}

	@Override
	public Long findCountMemberPointsByMemberId(MemberSearch search) {

		return this.getMapper().selectCountMemberPointsByMemberId(search);
	}

	@Override
	public Long findCountOrderHeaderByMemberId(MemberSearch search) {
		
		return this.getMapper().selectCountOrderHeaderByMemberId(search);
	}

	@Override
	public Long findCountCouponInstanceByMemberId(MemberSearch search) {
		
		return this.getMapper().selectCountCouponInstanceByMemberId(search);
	}

	@Override
	public Long findCountWechatMessageByMemberId(MemberSearch search) {
		
		return this.getMapper().selectCountWechatMessageByMemberId(search);
	}

	@Override
	public Long findCountSmsInstanceByMemberId(MemberSearch search) {
		
		return this.getMapper().selectCountSmsInstanceByMemberId(search);
	}

	@Override
	public Member findByMemberCode(String memberCode) {
		return this.getMapper().selectByMemberCode(memberCode);
	}
	
	@Override
	public Member findByMobile(String mobile) {
		return this.getMapper().selectByMobile(mobile);
	}

	@Override
	public MemberExp findMemberExpByCode(String code, String queryType) {
		return this.getMapper().selectMemberExpByCode(code, queryType);
	}

	/**
	 * TODO 查询会员周期累计积分量（可选）.
	 * @see com.bw.adv.module.member.repository.MemberRepository#findPeriodPoints(java.lang.String, java.lang.String)
	 */
	@Override
	public List<MemberExp> findPeriodPoints(String registerDate,String beginDate, String endDate) {
		return this.getMapper().selectPeriodPoints(registerDate,beginDate, endDate) ;
	}
	
	/**
	 * TODO 根据消费时间查询会员的累积积分（可选）.
	 * @see com.bw.adv.module.member.repository.MemberRepository#findPeriodPoints(java.lang.String, java.lang.String)
	 */
	@Override
	public List<MemberExp> findPeriodPointsByConsumeDate(String consumeDate,String beginDate, String endDate) {
		return this.getMapper().selectPeriodPointsByConsumeDate(consumeDate,beginDate, endDate) ;
	}
	
	/**
	 * TODO 根据消费时间查询会员周期累计订单金额（可选）.
	 * @see com.bw.adv.module.member.repository.MemberRepository#findPeriodPoints(java.lang.String, java.lang.String)
	 */
	@Override
	public List<MemberExp> findPeriodOrderAmountByConsumeDate(String consumeDate,String beginDate, String endDate) {
		return this.getMapper().selectPeriodOrderAmountByConsumeDate(consumeDate,beginDate, endDate) ;
	}

	/**
	 * TODO 查询某个会员周期累计积分量（可选）.
	 * @see com.bw.adv.module.member.repository.MemberRepository#findPeriodPoints(java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public MemberExp findPeriodPointsByMemberId(Long memberId, String beginDate, String endDate) {
		return this.getMapper().selectPeriodPointsByMemberId(memberId,beginDate, endDate) ;
	}

	/**
	 * TODO 查询会员周期累计订单金额（可选）.
	 * @see com.bw.adv.module.member.repository.MemberRepository#findPeriodOrderAmount(java.lang.String, java.lang.String)
	 */
	@Override
	public List<MemberExp> findPeriodOrderAmount(String registerDate,String beginDate, String endDate) {
		return this.getMapper().selectPeriodOrderAmount(registerDate,beginDate, endDate) ;
	}

	/**
	 * TODO 查询某个会员周期累计订单金额（可选）.
	 * @see com.bw.adv.module.member.repository.MemberRepository#findPeriodOrderAmount(java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public MemberExp findPeriodOrderAmountByMemberId(Long memberId,String beginDate, String endDate) {
		return this.getMapper().selectPeriodOrderAmountByMemberId(memberId,beginDate, endDate) ;
	}
	
	@Override
	public MemberExp selectMemberExpWechatByMemberId(Long memberId) {
		return this.getMapper().selectMemberExpWechatByMemberId(memberId);
	}

	@Override
	public MemberExp findMemberExpByMemberId(Long memberId) {
		if(memberId == null){
			throw new MemberException("memberId不能为空");
		}
		return this.getMapper().selectMemberExpByMemberId(memberId);
	}

	@Override
	public void callMemberSummaryDay(String dateStr, String weekStr, String monthStr) {
		this.getMapper().callMemberSummaryDay(dateStr, weekStr, monthStr);
	}

	@Override
	public List<MemberRegion> findMemberRegion(Page<MemberRegion> page,String sort,String order) {
		return this.getMapper().findMemberRegion(page,sort,order);
	}
	
	@Override
	public MemberExp findExpByMemberId(Long memberId) {
		return this.getMapper().selectExpByMemberId(memberId);
	}

	@Override
	public Long findAllMemberCount() {
		return this.getMapper().selectCountMember();
	}

	@Override
	public List<MemberAge> findMemberAge(String startDate, String endDate) {
		return this.getMapper().findMemberAge(startDate, endDate);
	}
	
	@Override
	public Member findByAccountAndTypeId(String bindingAccount,Long extAccountTypeId){
		return this.getMapper().selectByAccountAndTypeId(bindingAccount, extAccountTypeId);
	}

	@Override
	public Long queryActiveMemberCount() {
		return this.getMapper().queryActiveMemberCount();
	}

	@Override
	public Long queryMemberForStoreCount(Long storeId) {
		return this.getMapper().queryMemberForStoreCount(storeId);
	}

}
