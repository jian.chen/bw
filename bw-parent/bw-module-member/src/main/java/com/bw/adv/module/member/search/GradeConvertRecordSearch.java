/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeConvertRecordSearch.java
 * Package Name:com.sage.scrm.module.member.search
 * Date:2015年8月11日下午1:44:24
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.search;

import com.bw.adv.module.member.BaseSearch;

/**
 * ClassName:GradeConvertRecordSearch <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:44:24 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public class GradeConvertRecordSearch extends BaseSearch {
	 public GradeConvertRecordSearch() {
		 super(Default_Rows, Default_Page);
		}

		 private Long equalGcRecordId;
		 private Long equalMemberId;
		 private Long equalSrcGradeId;
		 private Long equalTargetGradeId;
		 private Long equalGradeFactorsId;
	    
	     private String equalUpDownFlag;
	     private String likeUpDownFlag;
	     private String greaterEqualUpDownFlag;
	     private String lessEqualUpDownFlag;
		 private Long equalGradeFactorsValue;
	     private String equalGcDate;
	     private String likeGcDate;
	     private String greaterEqualGcDate;
	     private String lessEqualGcDate;
	     
		public Long getEqualGcRecordId() {
			return equalGcRecordId;
		}

		public void setEqualGcRecordId(Long equalGcRecordId) {
			this.equalGcRecordId = equalGcRecordId;
		}
	    

		public Long getEqualMemberId() {
			return equalMemberId;
		}

		public void setEqualMemberId(Long equalMemberId) {
			this.equalMemberId = equalMemberId;
		}
	    

		public Long getEqualSrcGradeId() {
			return equalSrcGradeId;
		}

		public void setEqualSrcGradeId(Long equalSrcGradeId) {
			this.equalSrcGradeId = equalSrcGradeId;
		}
	    

		public Long getEqualTargetGradeId() {
			return equalTargetGradeId;
		}

		public void setEqualTargetGradeId(Long equalTargetGradeId) {
			this.equalTargetGradeId = equalTargetGradeId;
		}
	    

		public Long getEqualGradeFactorsId() {
			return equalGradeFactorsId;
		}

		public void setEqualGradeFactorsId(Long equalGradeFactorsId) {
			this.equalGradeFactorsId = equalGradeFactorsId;
		}
	    

	    
	    public String getEqualUpDownFlag() {
			return equalUpDownFlag;
		}

		public void setEqualUpDownFlag(String equalUpDownFlag) {
			this.equalUpDownFlag = equalUpDownFlag;
		}
	    
	    
	    public String getLikeUpDownFlag() {
			return likeUpDownFlag;
		}

		public void setLikeUpDownFlag(String likeUpDownFlag) {
			this.likeUpDownFlag = likeUpDownFlag;
		}
	    
	   public String getGreaterEqualUpDownFlag() {
			return greaterEqualUpDownFlag;
		}

		public void setGreaterEqualUpDownFlag(String greaterEqualUpDownFlag) {
			this.greaterEqualUpDownFlag = greaterEqualUpDownFlag;
		}
	    
	     public String getLessEqualUpDownFlag() {
			return lessEqualUpDownFlag;
		}

		public void setLessEqualUpDownFlag(String lessEqualUpDownFlag) {
			this.lessEqualUpDownFlag = lessEqualUpDownFlag;
		}
	    
	    
	     
		public Long getEqualGradeFactorsValue() {
			return equalGradeFactorsValue;
		}

		public void setEqualGradeFactorsValue(Long equalGradeFactorsValue) {
			this.equalGradeFactorsValue = equalGradeFactorsValue;
		}
	    

	    
	    public String getEqualGcDate() {
			return equalGcDate;
		}

		public void setEqualGcDate(String equalGcDate) {
			this.equalGcDate = equalGcDate;
		}
	    
	    
	    public String getLikeGcDate() {
			return likeGcDate;
		}

		public void setLikeGcDate(String likeGcDate) {
			this.likeGcDate = likeGcDate;
		}
	    
	   public String getGreaterEqualGcDate() {
			return greaterEqualGcDate;
		}

		public void setGreaterEqualGcDate(String greaterEqualGcDate) {
			this.greaterEqualGcDate = greaterEqualGcDate;
		}
	    
	     public String getLessEqualGcDate() {
			return lessEqualGcDate;
		}

		public void setLessEqualGcDate(String lessEqualGcDate) {
			this.lessEqualGcDate = lessEqualGcDate;
		}
}

