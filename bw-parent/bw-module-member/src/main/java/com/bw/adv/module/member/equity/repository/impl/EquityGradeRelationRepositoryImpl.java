/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:EquityGradeRelationRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.equity.repository.impl
 * Date:2016年1月14日下午9:56:28
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.equity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.equity.mapper.EquityGradeRelationMapper;
import com.bw.adv.module.member.equity.repository.EquityGradeRelationRepository;
import com.bw.adv.module.member.model.EquityGradeRelation;

/**
 * ClassName:EquityGradeRelationRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 下午9:56:28 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class EquityGradeRelationRepositoryImpl extends BaseRepositoryImpl<EquityGradeRelation, EquityGradeRelationMapper> implements EquityGradeRelationRepository {
    
	@Override
	protected Class<EquityGradeRelationMapper> getMapperClass() {
		return EquityGradeRelationMapper.class;
	}
	
	@Override
	public List<EquityGradeRelation> findGradeInfoById(Long equityId) {
		return this.getMapper().selectByEquityId(equityId);
	}

	@Override
	public void deleteByEquityId(Long equityId) {
		this.getMapper().deleteByEquityId(equityId);
	}


}

