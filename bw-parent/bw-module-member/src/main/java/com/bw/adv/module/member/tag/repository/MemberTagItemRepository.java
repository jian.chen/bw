
/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberTagItemRepository.java
 * Package Name:com.sage.scrm.module.member.repository
 * Date:2015年8月11日下午1:51:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.tag.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.member.tag.mapper.MemberTagItemMapper;
import com.bw.adv.module.member.tag.model.MemberTagItem;
import com.bw.adv.module.member.tag.search.MemberTagItemSearch;

/**
 * ClassName:MemberTagItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午1:51:06 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
public interface MemberTagItemRepository extends BaseRepository<MemberTagItem, MemberTagItemMapper>{
	
	Long findCountSearchAuto(MemberTagItemSearch search);

	List<MemberTagItem> findListSearchAuto(MemberTagItemSearch search);
	
	MemberTagItem findEntitySearchAuto(MemberTagItemSearch search);

	void deleteTagItemByTagId(Long memberTagId);
	
	void deleteTagItemByMemberId(Long memberId);

	void insertBatchMemberTagItem(List<MemberTagItem> addItemList);
	
	/**
	 * 插入标签记录（无视重复记录）
	 * @param addItemList
	 * @author cuicd
	 */
	void insertMemberTagItemIgnoreSame(@Param("list")List<MemberTagItem> addItemList);
}

