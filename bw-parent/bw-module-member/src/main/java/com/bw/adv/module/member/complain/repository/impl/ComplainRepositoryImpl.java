/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:ComplainMapperRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.complain.repository.impl
 * Date:2015年12月29日下午3:41:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.complain.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.member.complain.mapper.ComplainMapper;
import com.bw.adv.module.member.complain.model.Complain;
import com.bw.adv.module.member.complain.model.exp.ComplainExp;
import com.bw.adv.module.member.complain.repository.ComplainRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:ComplainMapperRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月29日 下午3:41:35 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class ComplainRepositoryImpl extends BaseRepositoryImpl<Complain, ComplainMapper> implements ComplainRepository{

	@Override
	protected Class<ComplainMapper> getMapperClass() {
		return ComplainMapper.class;
	}

	@Override
	public List<ComplainExp> findByExa(Example example, Page<ComplainExp> page) {
		return this.getMapper().selectByExa(example, page);
	}

	@Override
	public ComplainExp findDetailByPk(Long complainId) {
		return this.getMapper().selectDetailByPk(complainId);
	}

}

