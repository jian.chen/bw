package com.bw.adv.module.member.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.member.model.MemberPrism;
import com.bw.adv.module.member.tag.mapper.MemberPrismMapper;

public interface MemberPrismRepository extends BaseRepository<MemberPrism, MemberPrismMapper>{
	
	/**
	 * 查询所有棱镜
	 * @param templatePage
	 * @return
	 * @author cuicd
	 */
	List<MemberPrism> selectAllPrism(Page<MemberPrism> templatePage);

}
