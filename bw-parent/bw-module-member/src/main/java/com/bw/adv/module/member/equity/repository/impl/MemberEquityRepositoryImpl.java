/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:MemberEquityRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.equity.repository.impl
 * Date:2016年1月14日上午11:01:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.equity.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.equity.mapper.MemberEquityMapper;
import com.bw.adv.module.member.equity.repository.MemberEquityRepository;
import com.bw.adv.module.member.equity.search.MemberEquitySearch;
import com.bw.adv.module.member.model.MemberEquity;
import com.bw.adv.module.member.model.exp.MemberEquityExp;

/**
 * ClassName:MemberEquityRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 上午11:01:11 <br/>
 * scrmVersion 1.0
 * @author   tait
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class MemberEquityRepositoryImpl extends BaseRepositoryImpl<MemberEquity, MemberEquityMapper> implements MemberEquityRepository {
    
	@Override
	protected Class<MemberEquityMapper> getMapperClass() {
		return MemberEquityMapper.class;
	}
	
	@Override
	public List<MemberEquityExp> findListSearchAuto(MemberEquitySearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public Long findCountSearchAuto(MemberEquitySearch search) {
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public MemberEquity findInfoById(Long memberEquityid) {
		return this.getMapper().selectByPrimaryKey(memberEquityid);
	}

	@Override
	public int updateMemberEquity(MemberEquity memberEquity) {
		return this.getMapper().updateByPrimaryKeySelective(memberEquity);
	}

	@Override
	public int insertMemberEquity(MemberEquity memberEquity) {
		return this.getMapper().insertSelective(memberEquity);
	}

	@Override
	public int removeMemberEquity(MemberEquity memberEquity) {		
		return this.getMapper().updateEquityStatus(memberEquity);
	}

	@Override
	public List<MemberEquity> selectListByGradeId(Long gradeId,String nowDate) {
		return this.getMapper().selectListByGradeId(gradeId,nowDate);
	}

	
}

