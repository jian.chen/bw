/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-member
 * File Name:GradeConvertRecordRepositoryImpl.java
 * Package Name:com.sage.scrm.module.member.repository.impl
 * Date:2015年8月11日下午2:00:26
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.member.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.member.mapper.GradeConvertRecordMapper;
import com.bw.adv.module.member.model.GradeConvertRecord;
import com.bw.adv.module.member.model.exp.GradeConvertRecordExp;
import com.bw.adv.module.member.repository.GradeConvertRecordRepository;
import com.bw.adv.module.member.search.GradeConvertRecordSearch;

/**
 * ClassName:GradeConvertRecordRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年8月11日 下午2:00:26 <br/>
 * scrmVersion 1.0
 * @author   simon
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class GradeConvertRecordRepositoryImpl extends BaseRepositoryImpl<GradeConvertRecord, GradeConvertRecordMapper> implements GradeConvertRecordRepository{
	
	@Override
	protected Class<GradeConvertRecordMapper> getMapperClass() {
		return GradeConvertRecordMapper.class;
	}

	@Override
	public Long findCountSearchAuto(GradeConvertRecordSearch search) {
		
		return this.getMapper().selectCountSearchAuto(search);
	}

	@Override
	public List<GradeConvertRecordExp> findListSearchAuto(GradeConvertRecordSearch search) {
		return this.getMapper().selectListSearchAuto(search);
	}

	@Override
	public GradeConvertRecord findEntitySearchAuto(GradeConvertRecordSearch search) {
		
		return this.getMapper().selectEntitySearchAuto(search);
	}

}