package com.bw.adv.module.tools;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Delete;
import org.apache.tools.ant.taskdefs.Expand;


/**
 * 文件工具类
 * 
 * 2010-1-6下午02:14:41
 */
public class FileUtil {
	
	private static final int BUFFER_SIZE = 4096;

    
    private FileUtil() {
    }
    
    public static void createFile(InputStream in, String filePath) {
        if (in == null)
            throw new RuntimeException("create file error: inputstream is null");
        int potPos = filePath.lastIndexOf('/') + 1;
        String folderPath = filePath.substring(0, potPos);
        createFolder(folderPath);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(filePath);
            byte[] by = new byte[1024];
            int c;
            while ((c = in.read(by)) != -1) {
                outputStream.write(by, 0, c);
            }
        }
        catch (IOException e) {
            e.getStackTrace().toString();
        }
        try {
            outputStream.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 是否是允许上传文件
     * 
     * @param ex
     * @return
     */
    public static boolean isAllowUp(String logoFileName) {
        logoFileName = logoFileName.toLowerCase();
        String allowTYpe = "gif,jpeg,jpg,bmp,swf,png,rar,doc,docx,xls,xlsx,pdf,zip,ico,txt";
        if (!logoFileName.trim().equals("") && logoFileName.length() > 0) {
            String ex = logoFileName.substring(logoFileName.lastIndexOf(".") + 1, logoFileName.length());
            // return allowTYpe.toString().indexOf(ex) >= 0;
            // lzf edit 20110717
            // 解决只认小写问题
            // 同时加入jpeg扩展名/png扩展名
            return allowTYpe.toUpperCase().indexOf(ex.toUpperCase()) >= 0;
        }
        else {
            return false;
        }
    }
    
    /**
     * 
     * @Title: isAllowUpForPicture
     * @author simple.sun
     * @Description: 图片允许导入格式
     * @param @param logoFileName
     * @param @return
     * @return boolean
     * @throws
     */
    public static boolean isAllowUpForPicture(String logoFileName) {
        logoFileName = logoFileName.toLowerCase();
        String allowTYpe = "gif,jpeg,jpg,bmp,png,";
        if (!logoFileName.trim().equals("") && logoFileName.length() > 0) {
            String ex = logoFileName.substring(logoFileName.lastIndexOf(".") + 1, logoFileName.length());
            // return allowTYpe.toString().indexOf(ex) >= 0;
            // lzf edit 20110717
            // 解决只认小写问题
            // 同时加入jpeg扩展名/png扩展名
            return allowTYpe.toUpperCase().indexOf(ex.toUpperCase()) >= 0;
        }
        else {
            return false;
        }
    }
    
    /**
     * 
     * @Title: isAllowUpForExcel
     * @author simple.sun
     * @Description: Excel允许导入格式
     * @param @param logoFileName
     * @param @return
     * @return boolean
     * @throws
     */
    public static boolean isAllowUpForExcel(String logoFileName) {
        logoFileName = logoFileName.toLowerCase();
        String allowTYpe = "xlsx";
        if (!logoFileName.trim().equals("") && logoFileName.length() > 0) {
            String ex = logoFileName.substring(logoFileName.lastIndexOf(".") + 1, logoFileName.length());
            // return allowTYpe.toString().indexOf(ex) >= 0;
            // lzf edit 20110717
            // 解决只认小写问题
            // 同时加入jpeg扩展名/png扩展名
            return allowTYpe.toUpperCase().indexOf(ex.toUpperCase()) >= 0;
        }
        else {
            return false;
        }
    }
    
    /**
     * 把内容写入文件
     * 
     * @param filePath
     * @param fileContent
     */
    public static void write(String filePath, String fileContent) {
        
        try {
            FileOutputStream fo = new FileOutputStream(filePath);
            OutputStreamWriter out = new OutputStreamWriter(fo, "UTF-8");
            
            out.write(fileContent);
            
            out.close();
        }
        catch (IOException ex) {
            
            System.err.println("Create File Error!");
            ex.printStackTrace();
        }
    }
    
    /**
     * 读取文件内容 默认是UTF-8编码
     * 
     * @param filePath
     * @return
     */
    public static String read(String filePath, String code) {
        if (code == null || code.equals("")) {
            code = "UTF-8";
        }
        String fileContent = "";
        File file = new File(filePath);
        try {
            InputStreamReader read = new InputStreamReader(new FileInputStream(file), code);
            BufferedReader reader = new BufferedReader(read);
            String line;
            while ((line = reader.readLine()) != null) {
                fileContent = fileContent + line + "\n";
            }
            read.close();
            read = null;
            reader.close();
            read = null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            fileContent = "";
        }
        return fileContent;
    }
    
    /**
     * 删除文件或文件夹
     * 
     * @param filePath
     */
    public static void delete(String filePath) {
        try {
            File file = new File(filePath);
            if (file.exists()) {
                if (file.isDirectory()) {
                    FileUtils.deleteDirectory(file);
                }
                else {
                    file.delete();
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static boolean exist(String filepath) {
        File file = new File(filepath);
        
        return file.exists();
    }
    
    /**
     * 创建文件夹
     * 
     * @param filePath
     */
    public static void createFolder(String filePath) {
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        catch (Exception ex) {
            System.err.println("Make Folder Error:" + ex.getMessage());
        }
    }
    
    /**
     * 重命名文件、文件夹
     * 
     * @param from
     * @param to
     */
    public static void renameFile(String from, String to) {
        try {
            File file = new File(from);
            if (file.exists()) {
                file.renameTo(new File(to));
            }
        }
        catch (Exception ex) {
            System.err.println("Rename File/Folder Error:" + ex.getMessage());
        }
    }
    
    /**
     * 得到文件的扩展名
     * 
     * @param fileName
     * @return
     */
    public static String getFileExt(String fileName) {
        
        int potPos = fileName.lastIndexOf('.') + 1;
        String type = fileName.substring(potPos, fileName.length());
        return type;
    }
    
    /**
     * 通过File对象创建文件
     * 
     * @param file
     * @param filePath
     */
    public static void createFile(File file, String filePath) {
        int potPos = filePath.lastIndexOf('/') + 1;
        String folderPath = filePath.substring(0, potPos);
        createFolder(folderPath);
        FileOutputStream outputStream = null;
        FileInputStream fileInputStream = null;
        try {
            outputStream = new FileOutputStream(filePath);
            fileInputStream = new FileInputStream(file);
            byte[] by = new byte[1024];
            int c;
            while ((c = fileInputStream.read(by)) != -1) {
                outputStream.write(by, 0, c);
            }
        }
        catch (IOException e) {
            e.getStackTrace().toString();
        }
        try {
            outputStream.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileInputStream.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static String readFile(String resource) {
        InputStream stream = getResourceAsStream(resource);
        String content = readStreamToString(stream);
        
        return content;
        
    }
    
    public static InputStream getResourceAsStream(String resource) {
        String stripped = resource.startsWith("/") ? resource.substring(1) : resource;
        
        InputStream stream = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader != null) {
            stream = classLoader.getResourceAsStream(stripped);
            
        }
        
        return stream;
    }
    
    public static String readStreamToString(InputStream stream) {
        String fileContent = "";
        
        try {
            InputStreamReader read = new InputStreamReader(stream, "utf-8");
            BufferedReader reader = new BufferedReader(read);
            String line;
            while ((line = reader.readLine()) != null) {
                fileContent = fileContent + line + "\n";
            }
            read.close();
            read = null;
            reader.close();
            read = null;
        }
        catch (Exception ex) {
            fileContent = "";
        }
        return fileContent;
    }
    
    /**
     * delete file folder
     * 
     * @param path
     */
    public static void removeFile(File path) {
        
        if (path.isDirectory()) {
            try {
                FileUtils.deleteDirectory(path);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        
    }
    
    public static void copyFile(String srcFile, String destFile) {
        try {
            if (FileUtil.exist(srcFile)) {
                FileUtils.copyFile(new File(srcFile), new File(destFile));
            }
        }
        catch (IOException e) {
            
            e.printStackTrace();
        }
    }
    
    public static void copyFolder(String sourceFolder, String destinationFolder) {
        // System.out.println("copy " + sourceFolder + " to " + destinationFolder);
        try {
            File sourceF = new File(sourceFolder);
            if (sourceF.exists())
                FileUtils.copyDirectory(new File(sourceFolder), new File(destinationFolder));
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("copy file error");
        }
        
    }
    
    /**
     * 拷贝源文件夹至目标文件夹
     * 只拷贝新文件
     * 
     * @param sourceFolder
     * @param targetFolder
     */
    public static void copyNewFile(String sourceFolder, String targetFolder) {
        try {
            File sourceF = new File(sourceFolder);
            
            if (!targetFolder.endsWith("/"))
                targetFolder = targetFolder + "/";
            
            if (sourceF.exists()) {
                File[] filelist = sourceF.listFiles();
                for (File f : filelist) {
                    File targetFile = new File(targetFolder + f.getName());
                    
                    if (f.isFile()) {
                        // 如果目标文件比较新，或源文件比较新，则拷贝，否则跳过
                        if (!targetFile.exists() || FileUtils.isFileNewer(f, targetFile)) {
                            FileUtils.copyFileToDirectory(f, new File(targetFolder));
                            // System.out.println("copy "+ f.getName());
                        }
                        else {
                            // System.out.println("skip  "+ f.getName());
                        }
                    }
                }
            }
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("copy file error");
        }
    }
    
    public static void unZip(String zipPath, String targetFolder, boolean cleanZip) {
        File folderFile = new File(targetFolder);
        File zipFile = new File(zipPath);
        Project prj = new Project();
        Expand expand = new Expand();
        expand.setEncoding("UTF-8");
        expand.setProject(prj);
        expand.setSrc(zipFile);
        expand.setOverwrite(true);
        expand.setDest(folderFile);
        expand.execute();
        
        if (cleanZip) {
            // 清除zip包
            Delete delete = new Delete();
            delete.setProject(prj);
            delete.setDir(zipFile);
            delete.execute();
        }
    }
    
    
    
    
    
    public static String generateTmpFileName(String fileName){
		String extension = getExtensionName(fileName);
		return System.currentTimeMillis() + "_" + UUID.randomUUID().toString() + extension;
	}
    
    /**
	 * Java从文件路径照哦个中获取文件名
	 * @param filePath
	 * @return
	 */
	public static String getPathFileName(String filePath){
		if ((filePath != null) && (filePath.length() > 0)) {
			filePath = filePath.replaceAll("\\\\", "/");
			int dot = filePath.lastIndexOf("/");
			if ((dot > -1) && (dot < (filePath.length()))) {
				return filePath.substring(dot + 1, filePath.length());
			}
		}
		return filePath;
	}
    
    /**
	 * Java文件操作 获取文件扩展名
	 * 
	 * @param fileName
	 * @return 带.的扩展名
	 */
	public static String getExtensionName(String fileName) {
		if ((fileName != null) && (fileName.length() > 0)) {
			int dot = fileName.lastIndexOf('.');
			if ((dot > -1) && (dot < (fileName.length() - 1))) {
				return fileName.substring(dot);
			}
		}
		return fileName;
	}
    
    /**
	 * 文件下载文件名编码
	 * @param request
	 * @param fileName
	 * @return
	 */
	public static String encodeFileName(HttpServletRequest request, String fileName){
		try {
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
				fileName = URLEncoder.encode(fileName, "UTF-8");//IE浏览器
			} else {
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			fileName = UUID.randomUUID().toString() + getExtensionName(fileName);
			e.printStackTrace();
		}
		return fileName;
	}
	
	
	/**
	 * 文件下载方法
	 * @param request 
	 * @param response
	 * @param filePath 文件存储的绝对路径包括文件名, 例如: Linux下: /tmp/upload/tmp.txt Windows下： F:/tmp/upload/tmp.txt
	 * @param fileName 真实的文件名称 如: ××××合同文件.xls
	 * @throws IOException
	 */
	public static void downloadFile(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException{
		File srcFile = new File(filePath);
		response.reset();
		response.setContentType("application/octet-stream");
		response.addHeader("Content-Length", "" + srcFile.length());
		response.addHeader("Content-Disposition", "attachment;filename=" + encodeFileName(request, fileName));
		response.setHeader("Connection", "close");
		OutputStream bos = new BufferedOutputStream(response.getOutputStream());
		FileInputStream fis = new FileInputStream(srcFile);
		byte[] buffer = new byte[BUFFER_SIZE];
		int len = 0;
		while((len = fis.read(buffer, 0, buffer.length)) != -1){
			bos.write(buffer, 0, len);
		}
		fis.close();
		bos.flush();
		bos.close();
	}
    
    /**
	 * 文件下载方法2
	 *     这个方法需要传入的是生成文件的输出流，不需要在本地系统做文件备份
	 * @param request
	 * @param response
	 * @param excelOutputStream 生成文件的输出流(excel文件特有的，转换成ByteArrayOutputStream对象)
	 * @param fileName 文件名需要带后缀，建议用xls
	 * @param excelContentType
	 * @throws IOException
	 */
	public static void downloadFile(HttpServletRequest request, HttpServletResponse response, ByteArrayOutputStream baos, String fileName, String excelContentType) throws IOException{
		response.reset();
		response.setContentType(excelContentType);
		response.addHeader("Content-Disposition", "attachment;filename=" + encodeFileName(request, fileName));
		OutputStream bos = new BufferedOutputStream(response.getOutputStream());
		ByteArrayInputStream ais = new ByteArrayInputStream(baos.toByteArray());	
		byte[] buffer = new byte[BUFFER_SIZE];
		int len = 0;
		while((len = ais.read(buffer, 0, buffer.length)) != -1){
			bos.write(buffer, 0, len);
		}
		bos.flush();
		bos.close();
	}
    
    public static void main(String arg[]) {
        
    }
    
}
