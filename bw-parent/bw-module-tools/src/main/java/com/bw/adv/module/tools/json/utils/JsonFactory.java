
package com.bw.adv.module.tools.json.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertyFilter;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * ClassName: JsonFactory <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-8-31 上午10:05:20 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public class JsonFactory {
	
	private final Map<String, String> xmlConfMap;
	private final static Map<Class<?>, List<String>> confMap = new HashMap<Class<?>, List<String>>();
	private final static ClassLoader classLoader = JsonFactory.class.getClassLoader();
	private final static String CLASS_NAME = "className";
	private final static String IS_EXCLUDE = "isExclude";
	
	
	public JsonFactory(Map<String, String> xmlPathMap) {
		this.xmlConfMap = xmlPathMap;
	}
	
	@PostConstruct
	public void init() {
		ParseBean parseBean = null;
		for (String key : xmlConfMap.keySet()) {
			parseBean = load(xmlConfMap.get(key));
			confMap.put(parseBean.getClazz(), parseBean.getParams());
		}
	}
	
	public static JSONObject getResult(Map<Class<?>, Object> mapParams) {
		JSONObject result = new JSONObject();
		Object obj = null;
		boolean flag = true;
		for (Class<?> cls : mapParams.keySet()) {
			if(!confMap.containsKey(cls)) {
				flag = false;
//				throw new IllegalArgumentException(cls.getName() + "：没有配置文件");
			}
			obj = mapParams.get(cls);
			if(obj instanceof List<?>) {
				result.put(cls.getName(), flag ? JSONArray.fromObject(obj, getJsonConfig(confMap.get(cls))) : JSONArray.fromObject(obj));
			} else {
				result.put(cls.getName(), flag ? JSONObject.fromObject(obj, getJsonConfig(confMap.get(cls))) : JSONObject.fromObject(obj));
			}
		}
		return result;
	}
	
	private static JsonConfig getJsonConfig(final List<String> params) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setJsonPropertyFilter(new PropertyFilter() {
			@Override
			public boolean apply(Object source, String name, Object value) {
				if(params.contains(name)) {
					return false;
				}
				return true;
			}
		});
		return jsonConfig;
	}
	
	private ParseBean load(String xmlPath) {
		SAXReader reader = new SAXReader();
		List<String> paramsList = new ArrayList<String>();
		Class<?> cls = null;
		try {
		    Document document = reader.read(classLoader.getResourceAsStream(xmlPath));
		    Element root = document.getRootElement();
		    cls = Class.forName(root.attribute(CLASS_NAME).getValue());
		    if(root.attribute(IS_EXCLUDE) == null 
		    		|| root.attribute(IS_EXCLUDE).getValue().equals("false")) {
		    	for (Object obj : root.elements()) {
		    		paramsList.add(((Element) obj).getName());
				}
		    	return new ParseBean(cls, paramsList);
		    }
		    Field[] fields = cls.getDeclaredFields();
		    List<String> temp = new ArrayList<String>();
		    for (Object obj : root.elements()) {
		    	temp.add(((Element) obj).getName());
			}
		    for (Field field : fields) {
		    	if(!temp.contains(field.getName())) {
		    		paramsList.add(field.getName());
		    	}
			}
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return new ParseBean(cls, paramsList);
	}
	
	class ParseBean {
		private Class<?> clazz;
		private List<String> params;
		private ParseBean(Class<?> clazz, List<String> params) {
			this.clazz = clazz;
			this.params = params;
		}
		public Class<?> getClazz() {
			return clazz;
		}
		public List<String> getParams() {
			return params;
		}
		
	}
}

