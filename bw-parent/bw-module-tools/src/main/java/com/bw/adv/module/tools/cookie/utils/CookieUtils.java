package com.bw.adv.module.tools.cookie.utils;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

public class CookieUtils {

	private final static int maxAge = 3600*24;//一周
	public static final String COOKIE_OPENID_KEY = "cookie_openid_key_bw";//cookie_openid_key_cd
	public static final String COOKIE_HEAD_IMG_URL = "cookie_head_img_url";//cookie_head_img_url
	public static final String COOKIE_NICK_NAME = "cookie_nick_name";//cookie_nick_name
	public static final String COOKIE_MEMBER_ID = "cookie_member_bw_id";//cookie_member_id
	
	
	/**
	 * 设置cookie
	 * @param response
	 * @param name  cookie名字
	 * @param value cookie倄1�7
	 */
	public static void addCookie(HttpServletResponse response,String name,String value){
		try {
			if(StringUtils.isBlank(value) || StringUtils.isBlank(name)){
				return;
			}
			Cookie cookie = new Cookie(name, URLEncoder.encode(value,"utf-8"));

			cookie.setPath("/");
			if(maxAge>0){
				cookie.setMaxAge(maxAge);
			}
			response.addCookie(cookie);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 清空cookie
	 * @param response
	 * @param name  cookie名字
	 * @param value cookie倄1�7
	 */
	public static void clearCookie(HttpServletResponse response,String name){
		try {
			Cookie cookie = new Cookie(name, null);
			cookie.setPath("/");
			if(maxAge>0){
				cookie.setMaxAge(maxAge);
			}
			response.addCookie(cookie);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	/**
	 * 根据名字获取cookie
	 * @param request
	 * @param name cookie名字
	 * @return
	 */
	public static String getCookieByName(HttpServletRequest request, String name){
	    Map<String,Cookie> cookieMap = ReadCookieMap(request);
	    if(cookieMap.containsKey(name)){
	        Cookie cookie = (Cookie)cookieMap.get(name);
	        if(cookie!=null){
	        	return cookie.getValue();
	        }
	        return null;
	    }else{
	        return null;
	    }
	}

	/**
	 * 将cookie封装到Map里面
	 * @param request
	 * @return
	 */
	private static Map<String,Cookie> ReadCookieMap(HttpServletRequest request){
		Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
		Cookie[] cookies = request.getCookies();
		if(null!=cookies){
			for(Cookie cookie : cookies){
				cookieMap.put(cookie.getName(), cookie);
			}
		}
		return cookieMap;
	}
}
