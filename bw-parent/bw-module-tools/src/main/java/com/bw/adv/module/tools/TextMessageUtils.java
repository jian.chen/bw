/**
 * Copyright (C), 2010-2016, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-tools
 * File Name:TextMessageUtils.java
 * Package Name:com.sage.scrm.module.tools
 * Date:2016年1月14日上午10:21:07
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.tools;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;

/**
 * ClassName:TextMessageUtils <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2016年1月14日 上午10:21:07 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei 短信发送接口与解析
 * @version  jdk1.7
 * @see 	 
 */
public class TextMessageUtils {
	
	/** 
     * Hex编码字符组
     */
    private static final char[] DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    
    @Value("${spid}")
    private static String spid;
    @Value("${sppassword}")
    private static String sppassword;
    @Value("${out_spid}")
    private static String outlandSpid ;
    @Value("${out_sppassword}")
    private static String outlandPwd ;
    @Value("${mtUrl}")
    private static String mtUrl ;
	
	/**
     * 单条下行示例
	 * @throws Exception 
     */
    public static HashMap testSingleMt(String phone,String content, boolean inland) throws Exception {
        //操作命令、SP编号、SP密码，必填参数
        String command = "MT_REQUEST";
        //sp服务代码，可选参数，默认为 00
        String spsc = "00";
        //源号码，可选参数
        //String sa = "10657109053657";
        //目标号码，必填参数
        //String da = phone;
        //下行内容以及编码格式，必填参数
        int dc = 15;
        String ecodeform = "GBK";
        String sm = new String(Hex.encodeHex(content.getBytes(ecodeform)));
        String tSpid = spid;
        String tPwd = sppassword;
        if (!inland){
            tSpid = outlandSpid;
            tPwd = outlandPwd;
        }
        //组成url字符串
        String smsUrl = mtUrl + "?command=" + command + "&spid=" + tSpid + "&sppassword=" + tPwd + "&spsc=" + spsc
        		 + "&da=" + phone + "&sm=" + sm + "&dc=" + dc;

        //发送http请求，并接收http响应
        String resStr = doGetRequest(smsUrl.toString());
        System.out.println(resStr);

        //解析响应字符串
        HashMap pp = parseResStr(resStr);
        /*System.out.println(pp.get("command"));
        System.out.println(pp.get("spid"));
        System.out.println(pp.get("mtmsgid"));
        System.out.println(pp.get("mtstat"));
        System.out.println(pp.get("mterrcode"));*/
		return pp;
    }
    
    
    
    /**
     * 将普通字符串转换成Hex编码字符串
     * 
     * @param dataCoding 编码格式，15表示GBK编码，8表示UnicodeBigUnmarked编码，0表示ISO8859-1编码
     * @param realStr 普通字符串
     * @return Hex编码字符串
     */
    public static String encodeHexStr(int dataCoding, String realStr) {
        String hexStr = null;

        if (realStr != null) {
            byte[] data = null;
            try {
                if (dataCoding == 15) {
                    data = realStr.getBytes("GBK");
                } else if ((dataCoding & 0x0C) == 0x08) {
                    data = realStr.getBytes("UnicodeBigUnmarked");
                } else {
                    data = realStr.getBytes("ISO8859-1");
                }
            } catch (UnsupportedEncodingException e) {
                System.out.println(e.toString());
            }

            if (data != null) {
                int len = data.length;
                char[] out = new char[len << 1];
                // two characters form the hex value.
                for (int i = 0, j = 0; i < len; i++) {
                    out[j++] = DIGITS[(0xF0 & data[i]) >>> 4];
                    out[j++] = DIGITS[0x0F & data[i]];
                }
                hexStr = new String(out);
            }
        }
        return hexStr;
    }
    
    /**
     * 发送http GET请求，并返回http响应字符串
     * 
     * @param urlstr 完整的请求url字符串
     * @return
     */
    public static String doGetRequest(String urlstr) {
        String res = null;
        try {
            URL url = new URL(urlstr);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setRequestMethod("GET");
            httpConn.setRequestProperty("Content-Type", "text/html; charset=GB2312");
            //System.setProperty("sun.net.client.defaultConnectTimeout", "5000");//jdk1.4换成这个,连接超时
            //System.setProperty("sun.net.client.defaultReadTimeout", "10000"); //jdk1.4换成这个,读操作超时
            httpConn.setConnectTimeout(5000);//jdk 1.5换成这个,连接超时
            httpConn.setReadTimeout(10000);//jdk 1.5换成这个,读操作超时
            httpConn.setDoInput(true);
            int rescode = httpConn.getResponseCode();
            if (rescode == 200) {
                BufferedReader bfw = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
                res = bfw.readLine();
            } else {
                res = "Http request error code :" + rescode;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return res;
    }
    
    /**
     * 将 短信下行 请求响应字符串解析到一个HashMap中
     * @param resStr
     * @return
     */
    public static HashMap parseResStr(String resStr) {
        HashMap pp = new HashMap();
        try {
            String[] ps = resStr.split("&");
            for (int i = 0; i < ps.length; i++) {
                int ix = ps[i].indexOf("=");
                if (ix != -1) {
                    pp.put(ps[i].substring(0, ix), ps[i].substring(ix + 1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return pp;
    }

}

