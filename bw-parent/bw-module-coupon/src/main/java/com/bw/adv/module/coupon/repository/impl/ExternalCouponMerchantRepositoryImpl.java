package com.bw.adv.module.coupon.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.ExternalCouponMerchantMapper;
import com.bw.adv.module.coupon.model.ExternalCouponMerchant;
import com.bw.adv.module.coupon.repository.ExternalCouponMerchantRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Ronald on 2016/11/11.
 */
@Repository
public class ExternalCouponMerchantRepositoryImpl extends BaseRepositoryImpl<ExternalCouponMerchant, ExternalCouponMerchantMapper> implements ExternalCouponMerchantRepository {
    @Override
    protected Class<ExternalCouponMerchantMapper> getMapperClass() {
        return ExternalCouponMerchantMapper.class;
    }

    @Override
    public List<ExternalCouponMerchant> queryExternalCouponMerchantList() {
        return this.getMapper().getExternalMerchants();
    }
    @Override
    public List<ExternalCouponMerchant> queryExternalCouponMerchantList(Page<ExternalCouponMerchant> page) {
        return this.getMapper().getExternalMerchantsPaged(page);
    }

    @Override
    public int saveExternalCouponMerchant(ExternalCouponMerchant merchant) {
        return this.getMapper().insert(merchant);
    }

    @Override
    public ExternalCouponMerchant findExternalMerchant(Integer id) {
        return this.getMapper().selectByPrimaryKey(id);
    }
    @Override
    public ExternalCouponMerchant getExternalMerchantByCode(String merchantCode) {
        return this.getMapper().selectByMerchantCode(merchantCode);
    }
}
