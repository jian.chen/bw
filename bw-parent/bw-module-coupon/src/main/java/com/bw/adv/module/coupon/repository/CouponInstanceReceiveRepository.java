/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceReceiveRepository.java
 * Package Name:com.sage.scrm.bk.coupon.repository
 * Date:2015年11月24日下午9:02:40
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.coupon.mapper.CouponInstanceReceiveMapper;
import com.bw.adv.module.coupon.model.CouponInstanceReceive;

/**
 * ClassName:CouponInstanceReceiveRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月24日 下午9:02:40 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponInstanceReceiveRepository extends BaseRepository<CouponInstanceReceive, CouponInstanceReceiveMapper>{
	
	/**
	 * 
	 * insertSelective:添加优惠券领取记录. <br/>
	 * Date: 2016年6月1日 下午6:03:10 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param record
	 * @return
	 */
	int insertSelective(CouponInstanceReceive record);
}

