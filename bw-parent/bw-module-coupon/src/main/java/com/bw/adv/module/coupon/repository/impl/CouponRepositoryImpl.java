package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.mapper.CouponMapper;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.exp.CouponExp;
import com.bw.adv.module.coupon.repository.CouponRepository;


/**
 * ClassName: CouponRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 上午11:25:29 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class CouponRepositoryImpl extends BaseRepositoryImpl<Coupon, CouponMapper> implements CouponRepository {
	
	@Override
	protected Class<CouponMapper> getMapperClass() {
		return CouponMapper.class;
	}


	@Override
	public List<CouponExp> findList(Page<Coupon> page) {
		return this.getMapper().selectCouList(page);
	}


	@Override
	public int insertCoupon(Coupon coupon) {
		return this.getMapper().insertSelective(coupon);
	}


	@Override
	public Coupon findByPk(Long couponId) {
		return this.getMapper().selectByPrimaryKey(couponId);
	}

	@Override
	public List<CouponExp> findCouponListByCategoryID(Example example){
		return this.getMapper().selectCouponByCategoryID(example);
	}

	@Override
	public int updateCoupon(Coupon coupon) {
		return this.getMapper().updateByPrimaryKeySelective(coupon);
	}


	@Override
	public List<CouponExp> findCouponListByName(Example example,Page<Coupon> page) {
		
		return this.getMapper().selectByName(example,page);
	}


	@Override
	public void removeCouponsByPk(List<Coupon> coupons) {
		
		this.getMapper().updateBatch(coupons);
		
	}


	@Override
	public int updateByCouponCode(String couponCode,String statusId) {
		
		return this.getMapper().updateByCouponCode(couponCode,statusId);
	}


	@Override
	public int findCouponNum(String couponName) {
		
		return this.getMapper().selectCount(couponName);
	}


	@Override
	public void callCouponSummaryDay(String dateStr, String weekStr, String monthStr) {
		this.getMapper().callCouponSummaryDay(dateStr, weekStr, monthStr);
	}


	@Override
	public List<Coupon> findListByIds(String couponIds) {
		
		return this.getMapper().selectListByIds(couponIds);
	}


	@Override
	public Coupon findCouponByCode(String code) {
		return this.getMapper().selectByCoulum(code);
	}


}
