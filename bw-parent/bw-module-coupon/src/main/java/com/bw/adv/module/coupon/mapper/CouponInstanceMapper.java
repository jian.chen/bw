package com.bw.adv.module.coupon.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.exp.CouponForUse;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.wechat.model.WechatMsgQueue;

/**
 * ClassName: CouponInstanceMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月12日 下午6:32:35 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponInstanceMapper extends BaseMapper<CouponInstance>{
    
	/**
	 * selectCouponListByNum:(这里用一句话描述这个方法的作用). <br/>
	 * Date: 2015年8月12日 下午6:32:41 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceNum
	 * @return
	 */
	List<CouponInstance> selectCouponListByNum(@Param("couponInstanceNum")Long couponInstanceNum);
	
	/**
	 * selectByDynamicCondition:根据条件查询优惠券实例 <br/>
	 * Date: 2015年8月26日 上午11:38:39 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param map
	 * @param page
	 * @return
	 */
	List<CouponInstanceExp> selectByDynamicCondition(@Param("map")Map<String, Object> map,Page<CouponInstanceExp> page);
	
	/**
	 * selectByDynamicCondition:根据条件查询优惠券实例 <br/>
	 * Date: 2015年9月21日 下午9:25:44 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponInstanceExp> selectByDynamicCondition(@Param("example")Example example,Page<CouponInstanceExp> page);
	
	
	/**
	 * selectByDynamicCondition:查询优惠券实例(优惠券实例、会员、优惠券、优惠券发布)<br/>
	 * Date: 2015年12月24日 下午3:16:05 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	List<CouponInstanceExp> selectByDynamicCondition(@Param("example")Example example);
	
	/**
	 * selectByCondition:根据条件查询优惠券实例  <br/>
	 * Date: 2015年9月21日 下午9:27:04 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponInstanceExp> selectByCondition(@Param("example")Example example,Page<CouponInstanceExp> page);
	
	/**
	 * 
	 * @param example
	 * @return
	 */
	List<CouponInstanceExp> selectByConditionNoPage(@Param("example")Example example);
	/**
	 * selectCountByDy:根据条件查询优惠券实例数量 <br/>
	 * Date: 2015年8月26日 上午11:39:14 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param map
	 * @return
	 */
	int selectCountByDy(Map<String, Object> map);
	
	/**
	 * selectByInstanceCode:根据优惠券实例编码查询实例 <br/>
	 * Date: 2015年8月27日 上午11:55:27 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	CouponInstance selectByInstanceCode(@Param("couponInstanceCode")String couponInstanceCode);
	
	/**
	 * selectExpByInstanceCode:根据实例查询优惠券信息<br/>
	 * Date: 2015年9月10日 下午3:15:28 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	CouponInstanceExp selectExpByExample(@Param("example")Example example);
	
	/**
	 * batchUpdateCouponInstance:批量修改优惠券实例<br/>
	 * Date: 2015年9月16日 下午7:18:20 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param map
	 */
	void batchUpdateCouponInstance(Map<String,Object> map);

	/**
	 * selectActiveListByMemberId:(查询会员可用的优惠券). <br/>
	 * Date: 2015-11-17 下午4:28:47 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<CouponInstanceExp> selectActiveListByMemberId(@Param("memberId")Long memberId,@Param("expireDate")String expireDate);

	/**
	 * selectExpByCode:(查询优惠券信息，根据优惠券编号). <br/>
	 * Date: 2015-11-17 下午5:09:21 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	CouponInstanceExp selectExpByCode(@Param("couponInstanceCode") String couponInstanceCode);
	
	/**
	 * insertCouMsgBatch:批量插入优惠券消息<br/>
	 * Date: 2015年12月10日 下午2:22:10 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param recordList
	 * @return
	 */
	int insertCouMsgBatch(@Param("list") List<WechatMsgQueue> list);

	/**
	 * selectListByCodes:(根据券编号查询相应的券信息). <br/>
	 * Date: 2015-12-18 上午1:10:34 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param codeList
	 * @return
	 */
	List<CouponInstance> selectListByCodes(@Param("codeList")List<String> codeList);
	
	
	/**
	 * 根据会员id和券状态查询实例数量
	 * @param memberId
	 * @param statusId
	 * @return
	 */
	@Select(" select COUNT(ci.COUPON_INSTANCE_OWNER) "
			+ " from coupon_instance ci "
			+ " where ci.COUPON_INSTANCE_OWNER = #{memberId}"
			+ " and ci.STATUS_ID = #{statusId}")
	Long selectInstanceCountByMemberIdAndStatusId(@Param("memberId")Long memberId,@Param("statusId")Long statusId);

	
	/**
	 * selectExpListByCodes:(根据券编号查询相应的券扩展信息). <br/>
	 * Date: 2016-1-5 上午10:57:31 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param codeList
	 * @return
	 */
	List<CouponInstanceExp> selectExpListByCodes(@Param("codeList")List<String> codeList);
	
	/**
	 * 更新优惠券发放中至未使用
	 * updateInstanceToOver: <br/>
	 * Date: 2016年5月12日 下午6:32:01 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 */
	void updateInstanceToOver();
	
	/**
	 * 删除所有发放中优惠券
	 * deleteInstanceWithIng: <br/>
	 * Date: 2016年5月12日 下午6:32:22 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 */
	void deleteInstanceWithIng();
	
	 /**
     * 查询优惠券使用情况
     * @param couponInstanceCode
     * @return
     */
    List<CouponForUse> queryCouponInstanceForUse(@Param("couponInstanceCode")String couponInstanceCode);

	/**
	 * 按照门店和日期查询核销记录用于air助手
	 * queryShopCheckout:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param storeCode
	 * @param useDate
	 * @return
	 */
	List<CouponInstanceExp> queryShopCheckout(@Param("storeCode")String storeCode,@Param("useDate")String useDate);


	/**
	 * 根据兑换码获取没有被分配的优惠券实例
	 * @return
	 */
	CouponInstance getCouponInstanceInfoByExchangeCode(@Param("exchangeCode") String exchangeCode);

	/**
	 *给实例券分配会员
	 * @param memberId
	 * @param exchangeCode
	 * @return
	 */
	int updateCouponInstanceByExchangeCode(@Param("memberId") Long memberId ,@Param("exchangeCode") String exchangeCode);

	/**
	 *给实例券分配会员
	 * @param memberId
	 * @param exchangeCode
	 * @return
	 */
	CouponInstance selectCouponInstanceExpByStatusByLimit(@Param("example")Example example);
}