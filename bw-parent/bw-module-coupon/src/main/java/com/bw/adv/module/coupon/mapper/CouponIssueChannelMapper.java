package com.bw.adv.module.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.coupon.model.CouponIssueChannel;

/**
 * ClassName: CouponIssueChannelMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月12日 下午6:32:09 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponIssueChannelMapper extends BaseMapper<CouponIssueChannel>{

	/**
     * selectChannelList:查询渠道列表 <br/>
     * Date: 2015年8月25日 下午2:56:23 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @return
     */
    List<Channel> selectChannelList(); 
    
    /**
     * selectChannelListByDc:根据优惠券类型查询渠道列表<br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param channelTypeId
     * @return
     */
    List<Channel> selectChannelListByDc(Long channelTypeId);
    
    /**
     * selectByPrimaryKeys:根据ids查询渠道<br/>
     * Date: 2015年11月18日 下午8:27:41 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param channelIds
     * @return
     */
    List<Channel> selectByPrimaryKeys(@Param("channelIds")String channelIds);
    
}