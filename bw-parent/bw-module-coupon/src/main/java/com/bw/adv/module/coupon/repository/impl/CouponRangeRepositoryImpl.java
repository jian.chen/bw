package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.coupon.mapper.CouponRangeMapper;
import com.bw.adv.module.coupon.model.CouponRange;
import com.bw.adv.module.coupon.model.exp.CouponRangeExp;
import com.bw.adv.module.coupon.repository.CouponRangeRepository;


/**
 * ClassName: CouponRangeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月12日 下午3:06:04 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class CouponRangeRepositoryImpl extends BaseRepositoryImpl<CouponRange, CouponRangeMapper> implements CouponRangeRepository {

	@Override
	protected Class<CouponRangeMapper> getMapperClass() {
		
		return CouponRangeMapper.class;
	}

	@Override
	public List<CouponRangeExp> findByCouponId(Long couponid) {
		
		return this.getMapper().selectByCouponId(couponid);
	}

	@Override
	public void deleteByCouponId(Long couponid) {
		
		this.getMapper().deleteByCouponId(couponid);
	}
    
	
}
