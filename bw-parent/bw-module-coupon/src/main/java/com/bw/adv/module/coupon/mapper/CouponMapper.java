package com.bw.adv.module.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.exp.CouponExp;
/**
 * ClassName: CouponMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 下午7:49:15 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponMapper extends BaseMapper<Coupon>{
    
    /**
     * selectCouList:查询优惠券列表 <br/>
     * Date: 2015年8月11日 下午7:49:19 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param page
     * @return
     */
    List<CouponExp> selectCouList(Page<Coupon> page);
    
    /**
     * selectByName:根据优惠券名称查询优惠券列表<br/>
     * Date: 2015年8月11日 下午7:49:44 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param couponName
     * @return
     */
    List<CouponExp> selectByName(@Param("example")Example example,Page<Coupon> page);
    
    /**
	 * 
	 * selectCouponByCategoryID:(我要领劵模块---根据劵类别ID查询优惠券). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponExp> selectCouponByCategoryID(@Param("example")Example example);
    /**
     * updateByCouponCode:根据优惠券码修改优惠券状态<br/>
     * Date: 2015年8月17日 下午5:19:22 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param couponCode
     * @return
     */
    int updateByCouponCode(@Param("couponCode")String couponCode,@Param("statusId")String statusId);
    
    /**
     * selectCount:查询优惠券的数量 <br/>
     * Date: 2015年8月20日 上午10:19:43 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @return
     */
    int selectCount(@Param("couponName")String couponName);
    
    /**
     * 
     * selectListByIds:(根据传入的id查询优惠券详情(格式"id1,id2,id3"),用于活动验证优惠券是否合法). <br/>
     * Date: 2016年1月25日 下午5:57:26 <br/>
     * scrmVersion 1.0
     * @author sherry.wei
     * @version jdk1.7
     * @param couponIds
     * @return
     */
    List<Coupon> selectListByIds(@Param("couponIds")String couponIds);
    
    void callCouponSummaryDay(@Param("dateStr")String dateStr, @Param("weekStr")String weekStr, @Param("monthStr")String monthStr);

	Coupon selectByCoulum(@Param("couponCode")String couponCode);

}