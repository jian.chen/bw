package com.bw.adv.module.coupon.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.model.ExternalCouponMerchant;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExternalCouponMerchantMapper extends BaseMapper<ExternalCouponMerchant>{
    int deleteByPrimaryKey(Integer externalMerchantId);

    int insert(ExternalCouponMerchant record);

    int insertSelective(ExternalCouponMerchant record);

    ExternalCouponMerchant selectByPrimaryKey(Integer externalMerchantId);

    int updateByPrimaryKeySelective(ExternalCouponMerchant record);

    int updateByPrimaryKey(ExternalCouponMerchant record);

    List<ExternalCouponMerchant> getExternalMerchants();
    List<ExternalCouponMerchant> getExternalMerchantsPaged(Page<ExternalCouponMerchant> page);
    ExternalCouponMerchant selectByMerchantCode(@Param("merchantCode") String merchantCode);
}