package com.bw.adv.module.coupon.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.coupon.model.CouponOrigin;

import java.util.List;

public interface CouponOriginMapper extends BaseMapper<CouponOrigin>{
    int deleteByPrimaryKey(Integer couponOriginId);

    int insert(CouponOrigin record);

    int insertSelective(CouponOrigin record);

    CouponOrigin selectByPrimaryKey(Integer couponOriginId);

    int updateByPrimaryKeySelective(CouponOrigin record);

    int updateByPrimaryKey(CouponOrigin record);

    List<CouponOrigin> getCouponOriginList();
}