package com.bw.adv.module.coupon.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.coupon.model.CouponType;

/**
 * ClassName: CouponTypeMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月12日 下午6:33:18 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponTypeMapper extends BaseMapper<CouponType>{
	
	/**
	 * selectAllActive:(查询所有有效的优惠券类型). <br/>
	 * Date: 2015-12-15 下午6:15:01 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<CouponType> selectAllActive();
	
}