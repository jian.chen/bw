package com.bw.adv.module.coupon.repository.impl;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.coupon.mapper.ExternalCouponMapper;
import com.bw.adv.module.coupon.model.ExternalCoupon;
import com.bw.adv.module.coupon.model.ExternalCouponMerchant;
import com.bw.adv.module.coupon.repository.ExternalCouponRepository;

import org.springframework.stereotype.Repository;

/**
 * Created by Ronald on 2016/11/9.
 */
@Repository
public class ExternalCouponRepositoryImpl extends BaseRepositoryImpl<ExternalCoupon, ExternalCouponMapper> implements ExternalCouponRepository{
    @Override
    public int bindCouponInstanceWithOneExternalCoupon(Long instanceId, Long couponId) {
        return this.getMapper().bindOneExternalCoupon(instanceId, couponId);
    }


    @Override
    protected Class<ExternalCouponMapper> getMapperClass() {

        return ExternalCouponMapper.class;
    }

    @Override
    public ExternalCoupon findSameCouponOfMerchant(Long couponId, String code) {
        return this.getMapper().selectByCouponCodeForCoupon(couponId, code);
    }
}
