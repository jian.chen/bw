package com.bw.adv.module.coupon.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.coupon.model.CouponInstanceReceive;

public interface CouponInstanceReceiveMapper extends BaseMapper<CouponInstanceReceive>{
    int deleteByPrimaryKey(Long couponInstanceReceiveId);

    int insert(CouponInstanceReceive record);

    int insertSelective(CouponInstanceReceive record);

    CouponInstanceReceive selectByPrimaryKey(Long couponInstanceReceiveId);

    int updateByPrimaryKeySelective(CouponInstanceReceive record);

    int updateByPrimaryKey(CouponInstanceReceive record);
}