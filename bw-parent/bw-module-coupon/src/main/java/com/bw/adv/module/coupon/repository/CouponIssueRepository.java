package com.bw.adv.module.coupon.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.CouponIssueMapper;
import com.bw.adv.module.coupon.model.CouponIssue;


/**
 * ClassName: CouponIssueRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月12日 下午6:33:47 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponIssueRepository extends BaseRepository<CouponIssue, CouponIssueMapper>{
	
	/**
	 * findCCouponIssueList:查询优惠券发布列表<br/>
	 * Date: 2015年8月13日 上午10:23:52 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	List<CouponIssue> findCouponIssueList(Long couponId,Page<CouponIssue> page);
	
	/**
	 * findCouIssByPk:根据主键查询优惠券发布记录<br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponIssueId
	 * @return
	 */
	CouponIssue findCouIssByPk(Long couponIssueId);
	
	/**
	 * findCouponIssueCount:查询优惠券发布数量 <br/>
	 * Date: 2015年8月25日 下午5:52:48 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	int findCouponIssueCount(Long couponid);
	
	/**
	 * 查询正在下发的优惠券数量
	 * queryCouponIsIssueing: <br/>
	 * Date: 2016年5月16日 下午6:42:46 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @return
	 */
	Long queryCouponIsIssueing();
	
}