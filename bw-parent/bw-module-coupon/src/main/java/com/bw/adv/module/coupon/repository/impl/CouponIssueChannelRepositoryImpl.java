package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.coupon.mapper.CouponIssueChannelMapper;
import com.bw.adv.module.coupon.model.CouponIssueChannel;
import com.bw.adv.module.coupon.repository.CouponIssueChannelRepository;


/**
 * ClassName: CouponRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 上午11:25:29 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class CouponIssueChannelRepositoryImpl extends BaseRepositoryImpl<CouponIssueChannel, CouponIssueChannelMapper> implements CouponIssueChannelRepository {
	
	@Override
	protected Class<CouponIssueChannelMapper> getMapperClass() {
		return CouponIssueChannelMapper.class;
	}

	@Override
	public int insertCouponIssueChannel(CouponIssueChannel couponIssueChannel) {
		
		return this.getMapper().insertSelective(couponIssueChannel);
	}

	@Override
	public List<Channel> findChannelList() {
		
		return this.getMapper().selectChannelList();
	}

	@Override
	public List<Channel> findChannelListByDc(Long typeId) {
		
		return this.getMapper().selectChannelListByDc(typeId);
	}

	@Override
	public List<Channel> findChannelListByIds(String ids) {
		
		return this.getMapper().selectByPrimaryKeys(ids);
	}


}
