/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-coupon
 * File Name:CouponCategoryItemRepository.java
 * Package Name:com.sage.scrm.module.coupon.repository
 * Date:2015年12月25日下午12:17:50
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.CouponCategoryItemMapper;
import com.bw.adv.module.coupon.model.CouponCategoryItem;
import com.bw.adv.module.coupon.model.exp.CouponCategoryItemExp;

/**
 * ClassName:CouponCategoryItemRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月25日 下午12:17:50 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponCategoryItemRepository extends BaseRepository<CouponCategoryItem, CouponCategoryItemMapper> {

	/**
	 * 
	 * findListByStatus:(查询优惠券类别明细List(所有未删除明细)). <br/>
	 * Date: 2015年12月28日 上午10:25:03 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param statusId
	 * @param page
	 * @return
	 */
	public List<CouponCategoryItemExp> findListByStatus(Long statusId,Page<CouponCategoryItemExp> page);
	
	/**
	 * findByCouponId:根据优惠券ID查询当季优惠券 <br/>
	 * Date: 2016年4月13日 下午2:46:04 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponId
	 * @return
	 */
	public List<CouponCategoryItemExp> findByCouponId(Long couponId);
	
	/**
	 * 
	 * findExpByPk:(根据id查询类别明细(扩展类)详情). <br/>
	 * Date: 2015年12月28日 上午10:27:05 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItem
	 * @return
	 */
	public CouponCategoryItemExp findExpByPk(Long couponCategoryItem);
	
	/**
	 * 
	 * removeList:(批量删除优惠券类别明细). <br/>
	 * Date: 2015年12月28日 下午1:18:42 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryItems
	 */
	public void removeList(Long statusId,String couponCategoryItems);
	
	/**
	 * 根据优惠券ID查询当季优惠
	 * @param couponId
	 * @return
	 */
	public List<CouponCategoryItemExp> queryListByCouponId(Long couponId);
	
}

