/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-coupon
 * File Name:CouponCategoryItemRepositoryImpl.java
 * Package Name:com.sage.scrm.module.coupon.repository.impl
 * Date:2015年12月25日下午12:18:29
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.CouponCategoryItemMapper;
import com.bw.adv.module.coupon.model.CouponCategoryItem;
import com.bw.adv.module.coupon.model.exp.CouponCategoryItemExp;
import com.bw.adv.module.coupon.repository.CouponCategoryItemRepository;
/**
 * ClassName:CouponCategoryItemRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月25日 下午12:18:29 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class CouponCategoryItemRepositoryImpl extends BaseRepositoryImpl<CouponCategoryItem, CouponCategoryItemMapper> 
	implements CouponCategoryItemRepository{

	@Override
	protected Class<CouponCategoryItemMapper> getMapperClass() {
		
		return CouponCategoryItemMapper.class;
	}

	@Override
	public List<CouponCategoryItemExp> findListByStatus(Long statusId,
			Page<CouponCategoryItemExp> page) {
		
		return this.getMapper().selectListByStatus(statusId, page);
	}

	@Override
	public List<CouponCategoryItemExp> findByCouponId(Long couponId) {
		
		return this.getMapper().selectListByCouponId(couponId);
	}
	
	@Override
	public CouponCategoryItemExp findExpByPk(Long couponCategoryItem) {
		
		return this.getMapper().selectExpByPK(couponCategoryItem);
	}

	@Override
	public void removeList(Long statusId,String couponCategoryItems) {
		
		this.getMapper().deleteList(statusId,couponCategoryItems);
	}

	@Override
	public List<CouponCategoryItemExp> queryListByCouponId(Long couponId) {
		return this.getMapper().selectListByCouponId(couponId);
	}

}

