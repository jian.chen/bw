package com.bw.adv.module.coupon.repository.impl;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.coupon.mapper.CouponOriginMapper;
import com.bw.adv.module.coupon.model.CouponOrigin;
import com.bw.adv.module.coupon.repository.CouponOriginRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Ronald on 2016/11/11.
 */
@Repository
public class CouponOriginRepositoryImpl extends BaseRepositoryImpl<CouponOrigin, CouponOriginMapper> implements CouponOriginRepository {
    @Override
    protected Class<CouponOriginMapper> getMapperClass() {
        return CouponOriginMapper.class;
    }

    @Override
    public List<CouponOrigin> getCouponOriginList() {
        return this.getMapper().getCouponOriginList();
    }
}
