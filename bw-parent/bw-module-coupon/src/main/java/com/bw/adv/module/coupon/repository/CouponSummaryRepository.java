package com.bw.adv.module.coupon.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.coupon.mapper.CouponSummaryDayMapper;
import com.bw.adv.module.coupon.model.CouponSummaryDay;

public interface CouponSummaryRepository extends BaseRepository<CouponSummaryDay, CouponSummaryDayMapper>{
	
	/**
     * 查询优惠券统计数据
     * @param date
     * @return
     */
    CouponSummaryDay selectBySummaryDate(String date);

}
