package com.bw.adv.module.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.coupon.model.CouponRange;
import com.bw.adv.module.coupon.model.exp.CouponRangeExp;

/**
 * ClassName: CouponRangeMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月12日 下午6:33:01 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponRangeMapper extends BaseMapper<CouponRange>{
	
	List<CouponRangeExp> selectByCouponId(@Param("couponid")Long couponid);
	
	void deleteByCouponId(@Param("couponid")Long couponid);
}