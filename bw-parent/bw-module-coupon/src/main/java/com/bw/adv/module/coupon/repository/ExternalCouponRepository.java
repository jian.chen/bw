package com.bw.adv.module.coupon.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.coupon.mapper.ExternalCouponMapper;
import com.bw.adv.module.coupon.model.ExternalCoupon;
import com.bw.adv.module.coupon.model.ExternalCouponMerchant;

/**
 * Created by Ronald on 2016/11/9.
 */
public interface ExternalCouponRepository extends BaseRepository<ExternalCoupon, ExternalCouponMapper> {
    int bindCouponInstanceWithOneExternalCoupon(Long instanceId, Long couponId);
    ExternalCoupon findSameCouponOfMerchant(Long couponId, String code);
}
