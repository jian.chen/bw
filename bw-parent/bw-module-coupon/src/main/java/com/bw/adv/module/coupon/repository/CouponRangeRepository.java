package com.bw.adv.module.coupon.repository;


import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.coupon.mapper.CouponInstanceMapper;
import com.bw.adv.module.coupon.mapper.CouponRangeMapper;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.CouponRange;
import com.bw.adv.module.coupon.model.exp.CouponRangeExp;


/**
 * ClassName: CouponRangeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月12日 下午6:33:32 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponRangeRepository extends BaseRepository<CouponRange, CouponRangeMapper>{
	
	List<CouponRangeExp> findByCouponId(Long couponid);
	
	void deleteByCouponId(Long couponid);
}