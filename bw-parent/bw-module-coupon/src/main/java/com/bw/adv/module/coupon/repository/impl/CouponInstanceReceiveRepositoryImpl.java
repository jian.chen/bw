/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceReceiveRepositoryImpl.java
 * Package Name:com.sage.scrm.bk.coupon.repository.impl
 * Date:2015年11月24日下午9:03:34
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.coupon.mapper.CouponInstanceReceiveMapper;
import com.bw.adv.module.coupon.model.CouponInstanceReceive;
import com.bw.adv.module.coupon.repository.CouponInstanceReceiveRepository;

/**
 * ClassName:CouponInstanceReceiveRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月24日 下午9:03:34 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class CouponInstanceReceiveRepositoryImpl extends BaseRepositoryImpl<CouponInstanceReceive, CouponInstanceReceiveMapper> implements CouponInstanceReceiveRepository{

	@Override
	protected Class<CouponInstanceReceiveMapper> getMapperClass() {
		return CouponInstanceReceiveMapper.class;
	}

	@Override
	public int insertSelective(CouponInstanceReceive record) {
		return this.getMapper().insertSelective(record);
	}

}

