package com.bw.adv.module.coupon.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.coupon.model.CouponInstanceUseRecord;

public interface CouponInstanceUseRecordMapper extends BaseMapper<CouponInstanceUseRecord>{
    int deleteByPrimaryKey(Long couponInstanceUseRecordId);

    int insert(CouponInstanceUseRecord record);

    int insertSelective(CouponInstanceUseRecord record);

    CouponInstanceUseRecord selectByPrimaryKey(Long couponInstanceUseRecordId);

    int updateByPrimaryKeySelective(CouponInstanceUseRecord record);

    int updateByPrimaryKey(CouponInstanceUseRecord record);
}