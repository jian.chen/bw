package com.bw.adv.module.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.model.CouponIssue;

/**
 * ClassName: CouponIssueMapper <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月12日 下午6:31:41 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponIssueMapper extends  BaseMapper<CouponIssue> {
    
    /**
     * selectList:查询优惠券发布列表<br/>
     * Date: 2015年8月12日 下午6:31:45 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @param page
     * @return
     */
    List<CouponIssue> selectList(@Param("couponId")Long couponId,Page<CouponIssue> page);
    
    /**
     * selectCount:查询优惠券发布数量<br/>
     * Date: 2015年8月25日 下午5:51:12 <br/>
     * scrmVersion 1.0
     * @author liuyi.wang
     * @version jdk1.7
     * @return
     */
    int selectCount(Long couponId);
    
    /**
	 * 查询正在下发的优惠券数量
	 * queryCouponIsIssueing: <br/>
	 * Date: 2016年5月16日 下午6:42:46 <br/>
	 * scrmVersion 1.0
	 * @author chengdi.cui
	 * @version jdk1.7
	 * @return
	 */
	Long queryCouponIsIssueing();
}