package com.bw.adv.module.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.model.CouponInstanceRelay;
import com.bw.adv.module.coupon.model.exp.CouponInstanceRelayExp;

public interface CouponInstanceRelayMapper extends BaseMapper<CouponInstanceRelay>{
    int deleteByPrimaryKey(Long couponInstanceRelayId);

    int insert(CouponInstanceRelay record);

    int insertSelective(CouponInstanceRelay record);

    CouponInstanceRelay selectByPrimaryKey(Long couponInstanceRelayId);

    int updateByPrimaryKeySelective(CouponInstanceRelay record);

    int updateByPrimaryKey(CouponInstanceRelay record);
    
    int querySeq(@Param("openId")String openId,@Param("couponInstanceId")Long couponInstanceId);//查询节点的层级
    int queryNumber(@Param("openId")String openId,@Param("couponInstanceId")Long couponInstanceId,@Param("seq")int seq);//查询链的序号
    
    /**
     * 
     * selectCouponInstanceRelayList:查询转发记录列表(转发中). <br/>
     * Date: 2016年5月31日 下午6:44:37 <br/>
     * scrmVersion 1.0
     * @author zhangqi
     * @version jdk1.7
     * @param relayExp
     * @return
     */
    List<CouponInstanceRelayExp> selectCouponInstanceRelayList(@Param("CouponInstanceRelayExp")CouponInstanceRelayExp relayExp,
    		Page<CouponInstanceRelayExp> pageObj);
    
    /**
     * 
     * selectCouponInstanceReceiveList:查询转发记录列表(已转发). <br/>
     * Date: 2016年6月1日 下午4:36:54 <br/>
     * scrmVersion 1.0
     * @author zhangqi
     * @version jdk1.7
     * @param relayExp
     * @param pageObj
     * @return
     */
    List<CouponInstanceRelayExp> selectCouponInstanceReceiveList(@Param("CouponInstanceRelayExp")CouponInstanceRelayExp relayExp,
    		Page<CouponInstanceRelayExp> pageObj);
    
    /**
     * 
     * selectCouponInstanceRollbackList:查询转发记录列表(已退回). <br/>
     * Date: 2016年6月1日 下午4:36:59 <br/>
     * scrmVersion 1.0
     * @author zhangqi
     * @version jdk1.7
     * @param relayExp
     * @param pageObj
     * @return
     */
    List<CouponInstanceRelayExp> selectCouponInstanceRollbackList(@Param("CouponInstanceRelayExp")CouponInstanceRelayExp relayExp,
    		Page<CouponInstanceRelayExp> pageObj);
    
    /**
     * 
     * selectCouponInstanceRelayCount:查询转发记录数量(转发中). <br/>
     * Date: 2016年6月2日 下午2:25:07 <br/>
     * scrmVersion 1.0
     * @author zhangqi
     * @version jdk1.7
     * @param relayExp
     * @return
     */
    public Long selectCouponInstanceRelayCount(@Param("CouponInstanceRelayExp")CouponInstanceRelayExp relayExp);
    
    /**
     * 
     * selectCouponInstanceReceiveCount:查询转发记录数量(已转发). <br/>
     * Date: 2016年6月2日 下午12:06:01 <br/>
     * scrmVersion 1.0
     * @author zhangqi
     * @version jdk1.7
     * @param relayExp
     * @return
     */
    public Long selectCouponInstanceReceiveCount(@Param("CouponInstanceRelayExp")CouponInstanceRelayExp relayExp);
    
    /**
     * 
     * selectCouponInstanceRollbackCount:查询转发记录数量(已退回). <br/>
     * Date: 2016年6月2日 下午12:07:14 <br/>
     * scrmVersion 1.0
     * @author zhangqi
     * @version jdk1.7
     * @param relayExp
     * @return
     */
    public Long selectCouponInstanceRollbackCount(@Param("CouponInstanceRelayExp")CouponInstanceRelayExp relayExp);
}