package com.bw.adv.module.coupon.mapper;

import com.bw.adv.module.coupon.model.CouponSummaryDay;

public interface CouponSummaryDayMapper {
    int deleteByPrimaryKey(Long summaryId);

    int insert(CouponSummaryDay record);

    int insertSelective(CouponSummaryDay record);

    CouponSummaryDay selectByPrimaryKey(Long summaryId);

    int updateByPrimaryKeySelective(CouponSummaryDay record);

    int updateByPrimaryKey(CouponSummaryDay record);
    
    /**
     * 查询优惠券统计数据
     * @param date
     * @return
     */
    CouponSummaryDay selectBySummaryDate(String date);
}