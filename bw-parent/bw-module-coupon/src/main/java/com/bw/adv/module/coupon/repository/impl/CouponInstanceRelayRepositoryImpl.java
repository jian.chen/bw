/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceRelayImpl.java
 * Package Name:com.sage.scrm.bk.coupon.repository.impl
 * Date:2015年11月19日下午5:35:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.CouponInstanceRelayMapper;
import com.bw.adv.module.coupon.model.CouponInstanceRelay;
import com.bw.adv.module.coupon.model.exp.CouponInstanceRelayExp;
import com.bw.adv.module.coupon.repository.CouponInstanceRelayRepository;

/**
 * ClassName:CouponInstanceRelayImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:35:54 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class CouponInstanceRelayRepositoryImpl extends BaseRepositoryImpl<CouponInstanceRelay, CouponInstanceRelayMapper> implements CouponInstanceRelayRepository{

	@Override
	protected Class<CouponInstanceRelayMapper> getMapperClass() {
		return CouponInstanceRelayMapper.class;
	}

	@Override
	public int querySeq(String openId, Long couponInstanceId) {
		return this.getMapper().querySeq(openId, couponInstanceId);
	}

	@Override
	public int queryNumber(String openId, Long couponInstanceId,int seq) {
		return this.getMapper().queryNumber(openId, couponInstanceId,seq);
	}

	@Override
	public List<CouponInstanceRelayExp> selectCouponInstanceRelayList(CouponInstanceRelayExp relayExp,
			Page<CouponInstanceRelayExp> pageObj) {
		return this.getMapper().selectCouponInstanceRelayList(relayExp, pageObj);
	}

	@Override
	public List<CouponInstanceRelayExp> selectCouponInstanceReceiveList(CouponInstanceRelayExp relayExp,
			Page<CouponInstanceRelayExp> pageObj) {
		return this.getMapper().selectCouponInstanceReceiveList(relayExp, pageObj);
	}

	@Override
	public List<CouponInstanceRelayExp> selectCouponInstanceRollbackList(CouponInstanceRelayExp relayExp,
			Page<CouponInstanceRelayExp> pageObj) {
		return this.getMapper().selectCouponInstanceRollbackList(relayExp, pageObj);
	}

	@Override
	public Long selectCouponInstanceReceiveCount(CouponInstanceRelayExp relayExp) {
		return this.getMapper().selectCouponInstanceReceiveCount(relayExp);
	}

	@Override
	public Long selectCouponInstanceRollbackCount(CouponInstanceRelayExp relayExp) {
		return this.getMapper().selectCouponInstanceRollbackCount(relayExp);
	}

	@Override
	public Long selectCouponInstanceRelayCount(CouponInstanceRelayExp relayExp) {
		return this.getMapper().selectCouponInstanceRelayCount(relayExp);
	}

}

