/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceUseRecordRepository.java
 * Package Name:com.sage.scrm.bk.coupon.repository
 * Date:2015年11月19日下午5:56:06
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.coupon.mapper.CouponInstanceUseRecordMapper;
import com.bw.adv.module.coupon.model.CouponInstanceUseRecord;

/**
 * ClassName:CouponInstanceUseRecordRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:56:06 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponInstanceUseRecordRepository extends BaseRepository<CouponInstanceUseRecord, CouponInstanceUseRecordMapper>{

}

