package com.bw.adv.module.coupon.repository;


import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.CouponBusinessTypeMapper;
import com.bw.adv.module.coupon.model.CouponBusinessType;



/**
 * ClassName: CouponBusinessTypeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年12月28日 下午3:12:18 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponBusinessTypeRepository extends BaseRepository<CouponBusinessType, CouponBusinessTypeMapper>{
	
	/**
	 * findList:查询业务类型列表<br/>
	 * Date: 2015年12月28日 下午3:12:27 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	List<CouponBusinessType> findList();
	
	/**
	 * findList:查询业务类型列表分页<br/>
	 * Date: 2015年12月28日 下午5:01:17 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	List<CouponBusinessType> findList(Page<CouponBusinessType> page);
}