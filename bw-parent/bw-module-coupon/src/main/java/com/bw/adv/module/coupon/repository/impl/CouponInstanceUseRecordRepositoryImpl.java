/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceUseRecordRepositoryImpl.java
 * Package Name:com.sage.scrm.bk.coupon.repository.impl
 * Date:2015年11月19日下午5:56:54
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.coupon.mapper.CouponInstanceUseRecordMapper;
import com.bw.adv.module.coupon.model.CouponInstanceUseRecord;
import com.bw.adv.module.coupon.repository.CouponInstanceUseRecordRepository;

/**
 * ClassName:CouponInstanceUseRecordRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:56:54 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class CouponInstanceUseRecordRepositoryImpl extends BaseRepositoryImpl<CouponInstanceUseRecord, CouponInstanceUseRecordMapper> implements CouponInstanceUseRecordRepository{

	@Override
	protected Class<CouponInstanceUseRecordMapper> getMapperClass() {
		return CouponInstanceUseRecordMapper.class;
	}

}

