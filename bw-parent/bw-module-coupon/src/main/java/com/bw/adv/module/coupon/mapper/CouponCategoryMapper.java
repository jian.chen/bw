package com.bw.adv.module.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.model.CouponCategory;

public interface CouponCategoryMapper extends BaseMapper<CouponCategory> {
	List<CouponCategory> selectCouponCategoryPage(@Param("example")Example example,Page<CouponCategory> page);
	
	List<CouponCategory> selectListByStatus(@Param("statusId")Long statusId,Page<CouponCategory> page);
	
	 void deleteList(@Param("statusId")Long statusId,@Param("couponCategoryIds")String couponCategoryIds);
    
}