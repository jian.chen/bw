package com.bw.adv.module.coupon.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.mapper.CouponMapper;
import com.bw.adv.module.coupon.model.Coupon;
import com.bw.adv.module.coupon.model.exp.CouponExp;

/**
 * ClassName: CouponRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 下午7:33:36 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponRepository extends BaseRepository<Coupon, CouponMapper>{
	
	/**
	 * findList:查询优惠券列表<br/>
	 * Date: 2015年8月11日 下午7:38:07 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param page
	 * @return
	 */
	List<CouponExp> findList(Page<Coupon> page);
	
	/**
	 * insertCoupon:新增优惠券 <br/>
	 * Date: 2015年8月11日 下午7:38:31 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param coupon
	 * @return
	 */
	int insertCoupon(Coupon coupon);
	
	/**
	 * removeCouponsByPk:批量删除 <br/>
	 * Date: 2015年8月13日 下午8:11:05 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param coupons
	 */
	void removeCouponsByPk(List<Coupon> coupons);
	
	/**
	 * findByPk:根据主键查询优惠券<br/>
	 * Date: 2015年8月11日 下午7:38:58 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param primaryKey
	 * @return
	 */
	Coupon findByPk(Long primaryKey);
	
	/**
	 * 
	 * findCouponListByCategoryID:(我要领劵模块--根据优惠券类别查询优惠券). <br/>
	 * scrmVersion 1.0
	 * @author yu.zhang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponExp> findCouponListByCategoryID(Example example);
	
	/**
	 * updateCoupon:跟新优惠券<br/>
	 * Date: 2015年8月11日 下午7:39:21 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param coupon
	 * @return
	 */
	int updateCoupon(Coupon coupon);
	
	/**
	 * findCouponListByName:根据优惠券名称查询优惠券列表<br/>
	 * Date: 2015年8月11日 下午7:39:37 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	List<CouponExp> findCouponListByName(Example example,Page<Coupon> page);
	
	/**
	 * updateByCouponCode:根据优惠券码修改优惠券状态 <br/>
	 * Date: 2015年8月17日 下午5:21:18 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponCode
	 * @return
	 */
	int updateByCouponCode(String couponCode,String statusId);
	
	/**
	 * findCouponNum:查询优惠券的数量<br/>
	 * Date: 2015年8月20日 上午10:20:42 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	int findCouponNum(String couponName);
	
	/**
	 * 
	 * findListByIds:(根据传入的id查询优惠券详情(格式"id1,id2,id3"),用于活动验证优惠券是否合法). <br/>
	 * Date: 2016年1月25日 下午6:01:20 <br/>
	 * scrmVersion 1.0
	 * @author sherry.wei
	 * @version jdk1.7
	 * @param couponIds
	 * @return
	 */
	List<Coupon> findListByIds(String couponIds);
	
	/**
	 * 调用优惠券统计存储过程
	 * @param dateStr
	 * @param weekStr
	 * @param monthStr
	 */
	void callCouponSummaryDay(String dateStr,String weekStr,String monthStr);

	/**
	 * 根据编号取值
	 * @param code
	 * @return
	 */
	Coupon findCouponByCode(String code);
	
}