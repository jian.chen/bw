package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.CouponBusinessTypeMapper;
import com.bw.adv.module.coupon.model.CouponBusinessType;
import com.bw.adv.module.coupon.repository.CouponBusinessTypeRepository;


@Repository
public class CouponBusinessRepositoryImpl extends BaseRepositoryImpl<CouponBusinessType, CouponBusinessTypeMapper> implements CouponBusinessTypeRepository {

	@Override
	public List<CouponBusinessType> findList() {
		
		return this.getMapper().selectList();
	}

	@Override
	protected Class<CouponBusinessTypeMapper> getMapperClass() {
		
		return CouponBusinessTypeMapper.class;
	}

	@Override
	public List<CouponBusinessType> findList(Page<CouponBusinessType> page) {
		
		return this.getMapper().selectList(page);
	}


}
