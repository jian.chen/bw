package com.bw.adv.module.coupon.repository;


import java.util.List;
import java.util.Map;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.mapper.CouponInstanceMapper;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.exp.CouponForUse;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;


/**
 * ClassName: CouponInstanceRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月13日 上午10:25:29 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponInstanceRepository extends BaseRepository<CouponInstance, CouponInstanceMapper>{
	
	/**
	 * findCouponInstanceListByNum:查询固定数量的优惠券实例列表<br/>
	 * Date: 2015年8月13日 上午10:25:37 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceNum
	 * @return
	 */
	List<CouponInstance> findCouponInstanceListByNum(Long couponInstanceNum);
	
	/**
	 * frinCouponInstanceListByDyc:根据条件筛选查询优惠券实例列表 <br/>
	 * Date: 2015年8月14日 下午2:34:57 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param map
	 * @return
	 */
	List<CouponInstanceExp> findCouponInstanceListByDyc(Map<String, Object> map,Page<CouponInstanceExp> page);
	
	/**
	 * findCouponInstanceListByDyc:根据条件筛选查询优惠券实例列表 <br/>
	 * Date: 2015年8月27日 上午11:56:35 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponInstanceExp> findCouponInstanceListByDyc(Example example,Page<CouponInstanceExp> page);
	
	/**
	 * findCouponInstanceListByDyc:查询优惠券实例(优惠券实例、会员、优惠券、优惠券发布) <br/>
	 * Date: 2015年12月24日 下午3:19:29 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @return
	 */
	List<CouponInstanceExp> findCouponInstanceListByDyc(Example example);
	/**
	 * findCouponInstanceList:根据条件筛选查询优惠券实例列表  <br/>
	 * Date: 2015年9月21日 下午9:30:18 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param example
	 * @param page
	 * @return
	 */
	List<CouponInstanceExp> findCouponInstanceList(Example example,Page<CouponInstanceExp> page);
	
	/**
	 * 重载上面的方法，获取不分页结果
	 * @param example
	 * @return
	 */
	List<CouponInstanceExp> findCouponInstanceList(Example example);
	
	/**
	 * findCountByDyc:根据条件查询优惠券实例数量 <br/>
	 * Date: 2015年8月26日 上午11:40:54 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param map
	 * @return
	 */
	int findCountByDyc(Map<String, Object> map);
	
	/**
	 * findCouponInstanceByInstanceCode:根据优惠券实例编码查询实例 <br/>
	 * Date: 2015年8月27日 上午11:57:48 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	CouponInstance findCouponInstanceByInstanceCode(String couponInstanceCode);

	/**
	 * findCouponInstanceExpByExample:根据优惠券实例编码查询实例 <br/>
	 * Date: 2015年9月10日 下午3:16:24 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	CouponInstanceExp findCouponInstanceExpByExample(Example example);
	
	/**
	 * batchUpdateCouponInstanceBatch:批量修改优惠券实例 <br/>
	 * Date: 2015年9月16日 下午7:21:33 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param map
	 */
	void batchUpdateCouponInstanceBatch(Map<String,Object> map);

	/**
	 * findActiveListByMemberId:(查询会员可用的优惠券). <br/>
	 * Date: 2015-11-17 下午4:28:07 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param memberId
	 * @return
	 */
	List<CouponInstanceExp> findActiveListByMemberId(Long memberId);

	/**
	 * findExpByCode:(根据优惠券编号查询优惠券信息). <br/>
	 * Date: 2015-11-17 下午5:08:06 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param couponInstanceCode
	 * @return
	 */
	CouponInstanceExp findExpByCode(String couponInstanceCode);
	
	/**
	 * findListByCodes:(根据券编号查询相应的券信息). <br/>
	 * Date: 2015-12-18 上午1:09:24 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param codeList
	 * @return
	 */
	List<CouponInstance> findListByCodes(List<String> codeList);
	
	
	/**
	 * findExpListByCodes:(根据券编号查询相应的券扩展信息). <br/>
	 * Date: 2016-1-5 上午10:53:57 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param codeList
	 * @return
	 */
	List<CouponInstanceExp> findExpListByCodes(List<String> codeList);
	
	/**
	 * 根据会员id和券状态查询实例数量
	 * @param memberId
	 * @param statusId
	 * @return
	 */
	Long selectInstanceCountByMemberIdAndStatusId(Long memberId,Long statusId);
	
	void updateInstanceToOver();
	
	void deleteInstanceWithIng();
	
	/**
	 * 
	 * updateInstance:修改优惠券实例. <br/>
	 * Date: 2016年6月1日 下午5:51:24 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param instance
	 * @return
	 */
	public int updateInstance(CouponInstance instance);

	/**
	 * 按照门店和日期查询核销记录用于air助手
	 * queryShopCheckout:(这里用一句话描述这个方法的作用). <br/>
	 * @author chengdi.cui
	 * @param storeCode
	 * @param useDate
	 * @return
	 */
	List<CouponInstanceExp> queryShopCheckout(String storeCode, String useDate);
	
	 /**
     * 查询优惠券使用情况
     * @param couponInstanceCode
     * @return
     */
    List<CouponForUse> queryCouponInstanceForUse(String couponInstanceCode);

	CouponInstance getCouponInstanceInfoByExchangeCode(String exchangeCode);
	int updateCouponInstanceByExchangeCode(Long memberId , String exchangeCode);

	CouponInstance findCouponInstanceExpByStatusByLimit(Example example);
}









