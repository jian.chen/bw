package com.bw.adv.module.coupon.repository.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.mapper.CouponInstanceMapper;
import com.bw.adv.module.coupon.model.CouponInstance;
import com.bw.adv.module.coupon.model.exp.CouponForUse;
import com.bw.adv.module.coupon.model.exp.CouponInstanceExp;
import com.bw.adv.module.coupon.repository.CouponInstanceRepository;
import com.bw.adv.module.tools.DateUtils;

/**
 * ClassName: CouponRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 上午11:25:29 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class CouponInstanceRepositoryImpl extends BaseRepositoryImpl<CouponInstance, CouponInstanceMapper> implements CouponInstanceRepository {

	@Override
	protected Class<CouponInstanceMapper> getMapperClass() {
		return CouponInstanceMapper.class;
	}

	@Override
	public List<CouponInstance> findCouponInstanceListByNum(Long couponInstanceNum) {
		
		return this.getMapper().selectCouponListByNum(couponInstanceNum);
	}

	@Override
	public List<CouponInstanceExp> findCouponInstanceListByDyc(Map<String, Object> map,Page<CouponInstanceExp> page) {
		return this.getMapper().selectByDynamicCondition(map,page);
	}
	@Override
	public List<CouponInstanceExp> findCouponInstanceListByDyc(Example example,Page<CouponInstanceExp> page) {
		
		return this.getMapper().selectByDynamicCondition(example,page);
	}
	@Override
	public List<CouponInstanceExp> findCouponInstanceListByDyc(Example example) {
		
		return this.getMapper().selectByDynamicCondition(example);
	}

	@Override
	public int findCountByDyc(Map<String, Object> map) {
		
		return this.getMapper().selectCountByDy(map);
	}

	@Override
	public CouponInstance findCouponInstanceByInstanceCode(String couponInstanceCode) {
		
		return this.getMapper().selectByInstanceCode(couponInstanceCode);
	}

	@Override
	public CouponInstanceExp findCouponInstanceExpByExample(Example example) {
								
		return this.getMapper().selectExpByExample(example);
	}
	
	@Override
	public CouponInstance findCouponInstanceExpByStatusByLimit(Example example) {
		
		return this.getMapper().selectCouponInstanceExpByStatusByLimit(example);
	}

	@Override
	public void batchUpdateCouponInstanceBatch(Map<String, Object> map) {
		
		this.getMapper().batchUpdateCouponInstance(map);
	}

	@Override
	public List<CouponInstanceExp> findCouponInstanceList(Example example,Page<CouponInstanceExp> page) {
		return this.getMapper().selectByCondition(example, page);
	}
	
	@Override
	public List<CouponInstanceExp> findCouponInstanceList(Example example) {
		return this.getMapper().selectByConditionNoPage(example);
	}
	
	@Override
	public List<CouponInstanceExp> findActiveListByMemberId(Long memberId) {
		String expireDate = DateUtils.getCurrentDateOfDb();
		return this.getMapper().selectActiveListByMemberId(memberId,expireDate);
	}

	@Override
	public CouponInstanceExp findExpByCode(String couponInstanceCode) {
		return this.getMapper().selectExpByCode(couponInstanceCode);
	}

	/**
	 * findListByCodes:(根据券编号查询相应的券信息). <br/>
	 * Date: 2015-12-18 上午1:09:24 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @param codeList
	 * @return
	 */
	@Override
	public List<CouponInstance> findListByCodes(List<String> codeList) {
		return this.getMapper().selectListByCodes(codeList);
	}

	@Override
	public Long selectInstanceCountByMemberIdAndStatusId(Long memberId, Long statusId) {
		return this.getMapper().selectInstanceCountByMemberIdAndStatusId(memberId, statusId);
	}

	@Override
	public List<CouponInstanceExp> findExpListByCodes(List<String> codeList) {
		return this.getMapper().selectExpListByCodes(codeList);
	}

	@Override
	public void updateInstanceToOver() {
		this.getMapper().updateInstanceToOver();
	}

	@Override
	public void deleteInstanceWithIng() {
		this.getMapper().deleteInstanceWithIng();
	}

	@Override
	public int updateInstance(CouponInstance instance) {
		return this.getMapper().updateByPrimaryKeySelective(instance);
	}

	@Override
	public List<CouponInstanceExp> queryShopCheckout(String storeCode, String useDate) {
		return this.getMapper().queryShopCheckout(storeCode, useDate);
	}
	
	@Override
	public List<CouponForUse> queryCouponInstanceForUse(String couponInstanceCode) {
		return this.getMapper().queryCouponInstanceForUse(couponInstanceCode);
	}

	@Override
	public CouponInstance getCouponInstanceInfoByExchangeCode(String exchangeCode) {
		return this.getMapper().getCouponInstanceInfoByExchangeCode(exchangeCode);
	}

	@Override
	public int updateCouponInstanceByExchangeCode(Long memberId, String exchangeCode) {
		return this.getMapper().updateCouponInstanceByExchangeCode(memberId, exchangeCode);
	}
}
