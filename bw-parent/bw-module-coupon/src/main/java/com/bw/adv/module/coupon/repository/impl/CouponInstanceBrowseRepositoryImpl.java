/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceBrowseRepositoryImpl.java
 * Package Name:com.sage.scrm.bk.coupon.repository.impl
 * Date:2015年11月19日下午5:16:11
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository.impl;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.coupon.mapper.CouponInstanceBrowseMapper;
import com.bw.adv.module.coupon.model.CouponInstanceBrowse;
import com.bw.adv.module.coupon.repository.CouponInstanceBrowseRepository;

/**
 * ClassName:CouponInstanceBrowseRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:16:11 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class CouponInstanceBrowseRepositoryImpl extends BaseRepositoryImpl<CouponInstanceBrowse, CouponInstanceBrowseMapper> implements CouponInstanceBrowseRepository{
	@Override
	protected Class<CouponInstanceBrowseMapper> getMapperClass() {
		return CouponInstanceBrowseMapper.class;
	}
}

