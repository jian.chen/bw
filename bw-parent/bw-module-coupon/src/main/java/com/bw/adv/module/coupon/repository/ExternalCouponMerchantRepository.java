package com.bw.adv.module.coupon.repository;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.ExternalCouponMerchantMapper;
import com.bw.adv.module.coupon.model.ExternalCouponMerchant;

import java.util.List;

/**
 * Created by Ronald on 2016/11/11.
 */
public interface ExternalCouponMerchantRepository extends BaseRepository<ExternalCouponMerchant, ExternalCouponMerchantMapper>{
    List<ExternalCouponMerchant> queryExternalCouponMerchantList();
    List<ExternalCouponMerchant> queryExternalCouponMerchantList(Page<ExternalCouponMerchant> page);
    int saveExternalCouponMerchant(ExternalCouponMerchant merchant);
    ExternalCouponMerchant findExternalMerchant(Integer id);
    ExternalCouponMerchant getExternalMerchantByCode(String merchantCode);
}
