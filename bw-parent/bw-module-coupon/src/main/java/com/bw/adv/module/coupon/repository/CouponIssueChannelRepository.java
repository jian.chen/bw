package com.bw.adv.module.coupon.repository;


import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.base.model.Channel;
import com.bw.adv.module.coupon.mapper.CouponIssueChannelMapper;
import com.bw.adv.module.coupon.model.CouponIssueChannel;



/**
 * ClassName: CouponIssueChannelRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月25日 下午3:04:35 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
public interface CouponIssueChannelRepository extends BaseRepository<CouponIssueChannel, CouponIssueChannelMapper>{
	
	/**
	 * insertCouponIssueChannel:插入优惠券渠道 <br/>
	 * Date: 2015年8月25日 下午3:04:39 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param couponIssueChannel
	 * @return
	 */
	int insertCouponIssueChannel(CouponIssueChannel couponIssueChannel);
	
	/**
	 * findChannelList:查询渠道列表<br/>
	 * Date: 2015年8月25日 下午3:04:57 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @return
	 */
	List<Channel> findChannelList();
	
	/**
	 * findChannelListByDy:根据优惠券类型查询渠道列表<br/>
	 * Date: 2015年11月18日 下午3:08:12 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param typeId
	 * @return
	 */
	List<Channel> findChannelListByDc(Long typeId);
	
	/**
	 * findChannelListByIds:根据ids查询渠道 <br/>
	 * Date: 2015年11月18日 下午8:29:33 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param ids
	 * @return
	 */
	List<Channel> findChannelListByIds(String ids);
	
	
	
}