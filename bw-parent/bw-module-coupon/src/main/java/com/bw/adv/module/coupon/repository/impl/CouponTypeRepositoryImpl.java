package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.module.coupon.mapper.CouponTypeMapper;
import com.bw.adv.module.coupon.model.CouponType;
import com.bw.adv.module.coupon.repository.CouponTypeRepository;


/**
 * ClassName: CouponTypeRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-12-15 下午3:43:38 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
@Repository
public class CouponTypeRepositoryImpl extends BaseRepositoryImpl<CouponType, CouponTypeMapper> implements CouponTypeRepository {
	
	@Override
	protected Class<CouponTypeMapper> getMapperClass() {
		return CouponTypeMapper.class;
	}

	@Override
	public List<CouponType> findAllActive() {
		return this.getMapper().selectAllActive();
	}

}
