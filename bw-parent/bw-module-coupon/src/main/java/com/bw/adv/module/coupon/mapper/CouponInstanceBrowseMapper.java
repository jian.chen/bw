package com.bw.adv.module.coupon.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.coupon.model.CouponInstanceBrowse;

public interface CouponInstanceBrowseMapper extends BaseMapper<CouponInstanceBrowse>{
    int deleteByPrimaryKey(Long couponInstanceBrowseId);

    int insert(CouponInstanceBrowse record);

    int insertSelective(CouponInstanceBrowse record);

    CouponInstanceBrowse selectByPrimaryKey(Long couponInstanceBrowseId);

    int updateByPrimaryKeySelective(CouponInstanceBrowse record);

    int updateByPrimaryKey(CouponInstanceBrowse record);
}