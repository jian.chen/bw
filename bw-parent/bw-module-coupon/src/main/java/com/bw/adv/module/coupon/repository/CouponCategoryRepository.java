/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-coupon
 * File Name:CouponCategoryRepository.java
 * Package Name:com.sage.scrm.module.coupon.repository
 * Date:2015年12月25日下午12:14:51
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.mapper.CouponCategoryMapper;
import com.bw.adv.module.coupon.model.CouponCategory;

/**
 * ClassName:CouponCategoryRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月25日 下午12:14:51 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponCategoryRepository extends BaseRepository<CouponCategory, CouponCategoryMapper> {
	public List<CouponCategory> findCouponCategoryPage(Example example,Page<CouponCategory> page);
	
	public List<CouponCategory> findListByStatus(Long statusId,Page<CouponCategory> page);
	
	/**
	 * 
	 * removeList:(批量删除优惠券类别(逻辑删除)). <br/>
	 * Date: 2015年12月28日 下午1:46:09 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param couponCategoryIds
	 */
	public void removeList(Long statusId,String couponCategoryIds);

}

