package com.bw.adv.module.coupon.repository;

import com.bw.adv.module.coupon.model.CouponOrigin;

import java.util.List;

/**
 * Created by Ronald on 2016/11/11.
 */
public interface CouponOriginRepository {
    List<CouponOrigin> getCouponOriginList();
}
