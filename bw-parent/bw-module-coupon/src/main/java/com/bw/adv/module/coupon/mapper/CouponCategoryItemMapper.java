package com.bw.adv.module.coupon.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.model.CouponCategoryItem;
import com.bw.adv.module.coupon.model.exp.CouponCategoryItemExp;

public interface CouponCategoryItemMapper extends BaseMapper<CouponCategoryItem> {
	
    List<CouponCategoryItemExp> selectListByStatus(@Param("statusId")Long statusId,Page<CouponCategoryItemExp> page);
    
    CouponCategoryItemExp selectExpByPK(@Param("couponCategoryItem")Long couponCategoryItem);
    
    void deleteList(@Param("statusId")Long statusId,@Param("couponCategoryItems")String couponCategoryItems);
    
    List<CouponCategoryItemExp> selectListByCouponId(@Param("couponId")Long couponId);
    
}