package com.bw.adv.module.coupon.mapper;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.module.coupon.model.ExternalCoupon;

import org.apache.ibatis.annotations.Param;

public interface ExternalCouponMapper extends BaseMapper<ExternalCoupon> {
    int deleteByPrimaryKey(Long externalCouponId);

    int insert(ExternalCoupon record);

    int insertSelective(ExternalCoupon record);

    ExternalCoupon selectByPrimaryKey(Long externalCouponId);

    int updateByPrimaryKeySelective(ExternalCoupon record);

    int updateByPrimaryKey(ExternalCoupon record);

    int bindOneExternalCoupon(@Param("couponInstanceId") Long couponInstanceId, @Param("couponId") Long couponId);

    ExternalCoupon selectByCouponCodeForCoupon(@Param("couponId")Long couponId, @Param("code")String code);
}