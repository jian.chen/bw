package com.bw.adv.module.coupon.repository;

import java.util.List;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.module.coupon.mapper.CouponTypeMapper;
import com.bw.adv.module.coupon.model.CouponType;

/**
 * ClassName: CouponTypeRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015-12-15 下午3:42:02 <br/>
 * scrmVersion 1.0
 * @author mennan
 * @version jdk1.7
 */
public interface CouponTypeRepository extends BaseRepository<CouponType, CouponTypeMapper>{
	
	
	/**
	 * findAllActive:(查询所有有效的优惠券类型). <br/>
	 * Date: 2015-12-15 下午6:15:01 <br/>
	 * scrmVersion 1.0
	 * @author mennan
	 * @version jdk1.7
	 * @return
	 */
	List<CouponType> findAllActive();
	
}