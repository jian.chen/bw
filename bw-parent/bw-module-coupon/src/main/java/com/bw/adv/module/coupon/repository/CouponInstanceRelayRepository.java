/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-bk-service
 * File Name:CouponInstanceRelayRepository.java
 * Package Name:com.sage.scrm.bk.coupon.repository
 * Date:2015年11月19日下午5:32:32
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bw.adv.core.repository.BaseRepository;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.CouponInstanceRelayMapper;
import com.bw.adv.module.coupon.model.CouponInstanceRelay;
import com.bw.adv.module.coupon.model.exp.CouponInstanceRelayExp;

/**
 * ClassName:CouponInstanceRelayRepository <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月19日 下午5:32:32 <br/>
 * scrmVersion 1.0
 * @author   yu.zhang
 * @version  jdk1.7
 * @see 	 
 */
public interface CouponInstanceRelayRepository extends BaseRepository<CouponInstanceRelay, CouponInstanceRelayMapper>{
	public int querySeq(String openId,Long couponInstanceId);//查询节点的层级
	public int queryNumber(String openId,Long couponInstanceId,int seq);//查询链的序号
	
	/**
	 * 
	 * selectCouponInstanceRelayList:查询转发记录列表(转发中). <br/>
	 * Date: 2016年5月31日 下午7:15:37 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param relayExp
	 * @param pageObj
	 * @return
	 */
	List<CouponInstanceRelayExp> selectCouponInstanceRelayList(CouponInstanceRelayExp relayExp, 
			Page<CouponInstanceRelayExp> pageObj);
	
	/**
	 * 
	 * selectCouponInstanceReceiveList:查询转发记录列表(已转发). <br/>
	 * Date: 2016年6月1日 下午4:40:11 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param relayExp
	 * @param pageObj
	 * @return
	 */
	List<CouponInstanceRelayExp> selectCouponInstanceReceiveList(CouponInstanceRelayExp relayExp, 
			Page<CouponInstanceRelayExp> pageObj);
	
	/**
	 * 
	 * selectCouponInstanceRollbackList:查询转发记录列表(已退回). <br/>
	 * Date: 2016年6月1日 下午4:40:16 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param relayExp
	 * @param pageObj
	 * @return
	 */
	List<CouponInstanceRelayExp> selectCouponInstanceRollbackList(CouponInstanceRelayExp relayExp, 
			Page<CouponInstanceRelayExp> pageObj);
	
	/**
	 * 
	 * selectCouponInstanceRelayCount:查询转发记录数量(转发中). <br/>
	 * Date: 2016年6月2日 下午2:26:23 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param relayExp
	 * @return
	 */
	Long selectCouponInstanceRelayCount(CouponInstanceRelayExp relayExp);
	
	/**
	 * 
	 * selectCouponInstanceReceiveCount:查询转发记录数量(已转发). <br/>
	 * Date: 2016年6月2日 下午12:08:35 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param relayExp
	 * @return
	 */
	Long selectCouponInstanceReceiveCount(CouponInstanceRelayExp relayExp);
	
	/**
	 * 
	 * selectCouponInstanceRollbackCount:查询转发记录数量(已退回). <br/>
	 * Date: 2016年6月2日 下午12:10:06 <br/>
	 * scrmVersion 1.0
	 * @author zhangqi
	 * @version jdk1.7
	 * @param relayExp
	 * @return
	 */
	Long selectCouponInstanceRollbackCount(CouponInstanceRelayExp relayExp);
}

