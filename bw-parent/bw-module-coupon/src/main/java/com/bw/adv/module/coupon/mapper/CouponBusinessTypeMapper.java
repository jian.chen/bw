package com.bw.adv.module.coupon.mapper;

import java.util.List;

import com.bw.adv.core.mapper.BaseMapper;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.model.CouponBusinessType;

public interface CouponBusinessTypeMapper extends BaseMapper<CouponBusinessType>{
    
    List<CouponBusinessType> selectList();
    
    List<CouponBusinessType> selectList(Page<CouponBusinessType> page);
}