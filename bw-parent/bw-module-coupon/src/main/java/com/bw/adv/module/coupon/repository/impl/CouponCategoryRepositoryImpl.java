/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-module-coupon
 * File Name:CouponCategoryRepositoryImpl.java
 * Package Name:com.sage.scrm.module.coupon.repository.impl
 * Date:2015年12月25日下午12:15:47
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.coupon.mapper.CouponCategoryMapper;
import com.bw.adv.module.coupon.model.CouponCategory;
import com.bw.adv.module.coupon.repository.CouponCategoryRepository;

/**
 * ClassName:CouponCategoryRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月25日 下午12:15:47 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Repository
public class CouponCategoryRepositoryImpl extends BaseRepositoryImpl<CouponCategory, CouponCategoryMapper> implements CouponCategoryRepository{

	@Override
	protected Class<CouponCategoryMapper> getMapperClass() {
		return CouponCategoryMapper.class;
	}

	@Override
	public List<CouponCategory> findCouponCategoryPage(Example example,Page<CouponCategory> page) {
		return this.getMapper().selectCouponCategoryPage(example,page);
	}

	@Override
	public List<CouponCategory> findListByStatus(Long statusId,
			Page<CouponCategory> page) {
		
		return this.getMapper().selectListByStatus(statusId, page);
	}

	@Override
	public void removeList(Long statusId, String couponCategoryIds) {
		
		this.getMapper().deleteList(statusId, couponCategoryIds);
	}

}

