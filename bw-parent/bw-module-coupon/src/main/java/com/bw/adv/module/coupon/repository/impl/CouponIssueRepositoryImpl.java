package com.bw.adv.module.coupon.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bw.adv.core.repository.impl.BaseRepositoryImpl;
import com.bw.adv.core.utils.Page;
import com.bw.adv.module.coupon.mapper.CouponIssueMapper;
import com.bw.adv.module.coupon.model.CouponIssue;
import com.bw.adv.module.coupon.repository.CouponIssueRepository;


/**
 * ClassName: CouponRepositoryImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * date: 2015年8月11日 上午11:25:29 <br/>
 * scrmVersion 1.0
 * @author liuyi.wang
 * @version jdk1.7
 */
@Repository
public class CouponIssueRepositoryImpl extends BaseRepositoryImpl<CouponIssue, CouponIssueMapper> implements CouponIssueRepository {

	@Override
	public List<CouponIssue> findCouponIssueList(Long couponId,Page<CouponIssue> page) {
		return this.getMapper().selectList(couponId,page);
	}

	@Override
	protected Class<CouponIssueMapper> getMapperClass() {
		return CouponIssueMapper.class;
	}

	@Override
	public CouponIssue findCouIssByPk(Long couponIssueId) {
		
		return this.getMapper().selectByPrimaryKey(couponIssueId);
	}


	@Override
	public int findCouponIssueCount(Long couponid) {
		
		return this.getMapper().selectCount(couponid);
	}

	@Override
	public Long queryCouponIsIssueing() {
		return this.getMapper().queryCouponIsIssueing();
	}

}
