/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-report
 * File Name:MemberChannelSummaryDayController.java
 * Package Name:com.sage.scrm.controller.report
 * Date:2015年11月26日下午5:12:35
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.report;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.report.member.model.exp.MemberChannelSummaryDayExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelExport;
import com.bw.adv.service.report.service.MemberChannelSummaryDayService;

/**
 * ClassName:MemberChannelSummaryDayController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月26日 下午5:12:35 <br/>
 * scrmVersion 1.0
 * 
 * @author chuanxue.wei
 * @version jdk1.7
 * @see
 */
@Controller
@RequestMapping("/MemberChannelSummaryDayController")
public class MemberChannelSummaryDayController {

	private MemberChannelSummaryDayService memberChannelSummaryDayService;

	/**
	 * 
	 * showListByExample:(查询会员渠道统计表列表数据). <br/>
	 * Date: 2015年11月26日 下午5:34:47 <br/>
	 * scrmVersion 1.0
	 * 
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param memberChannelSummaryDay
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showListByExample")
	@ResponseBody
	public Map<String, Object> showListByExample(MemberChannelSummaryDayExp memberChannelSummaryDayExp, int page,
			int rows) {
		Map<String, Object> map = null;
		List<MemberChannelSummaryDayExp> result = null;
		Page<MemberChannelSummaryDayExp> pageObj = null;
		Example example = null;
		Map<String, Integer> memNums = null;
		MemberChannelSummaryDayExp mcsde = null;

		try {

			map = new HashMap<String, Object>();

			pageObj = new Page<MemberChannelSummaryDayExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);

			example = new Example();
			Criteria temp = example.createCriteria();
			if (memberChannelSummaryDayExp != null) {
				if (!StringUtils.isBlank(memberChannelSummaryDayExp.getStartDate())) {
					temp.andGreaterThanOrEqualTo("mcsd.SUMMARY_DATE",
							DateUtils.formatString(memberChannelSummaryDayExp.getStartDate()));
				}
				if (!StringUtils.isBlank(memberChannelSummaryDayExp.getEndDate())) {
					temp.andLessThanOrEqualTo("mcsd.SUMMARY_DATE",
							DateUtils.formatString(memberChannelSummaryDayExp.getEndDate()));
				}
				// 默认查询前一天的粉丝记录
				if (StringUtils.isBlank(memberChannelSummaryDayExp.getStartDate())
						&& StringUtils.isBlank(memberChannelSummaryDayExp.getEndDate())) {
					temp.andEqualTo("mcsd.SUMMARY_DATE", DateUtils.addDays(-1));
				}
			} else {
				temp.andEqualTo("mcsd.SUMMARY_DATE", DateUtils.addDays(-1));
			}

			result = memberChannelSummaryDayService.queryExpByExample(example, pageObj);
			memNums = new HashMap<String, Integer>();

			if (result != null && result.size() != 0) {
				for (int i = 0; i < result.size(); i++) {
					mcsde = result.get(i);
					if (memNums.get(mcsde.getCreateTime()) == null) {
						memNums.put(mcsde.getCreateTime(), mcsde.getTotalNumber());
					} else {
						memNums.put(mcsde.getCreateTime(), memNums.get(mcsde.getCreateTime()) + mcsde.getTotalNumber());
					}
				}

				Set<String> s = memNums.keySet();// 获取KEY集合
				for (String str : s) {
					for (int i = 0; i < result.size(); i++) {
						if (str.equals(result.get(i).getCreateTime())) {
							result.get(i).setMemNum(memNums.get(str));
						}
					}
				}

			}

			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 会员来源渠道excel
	 * 
	 * @param request
	 *            请求
	 * @param response
	 *            响应
	 */
	@RequestMapping(value = "/leadToExcelMemberChannel", method = RequestMethod.GET)
	public void leadToExcelMemberChannel(@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, HttpServletRequest request, HttpServletResponse response)
					throws UnsupportedEncodingException {
		List<MemberChannelSummaryDayExp> list = null;
		try {
			String disposition = "attachment;filename=" + URLEncoder.encode("会员来源渠道.xlsx", "utf-8");
			response.setContentType("application/xlsx;charset=UTF-8");
			response.setHeader("Content-disposition", disposition);
			// excel导出sheetName
			String sheetName = "会员来源渠道";
			// excel导出数据
			list = getData(startDate, endDate);
			// 导出
			if (list == null || list.size() == 0) {
				System.out.println("null----");
			} else {
				OutputStream out = new FileOutputStream(disposition);
				ExcelExport.exportExcel(sheetName, MemberChannelSummaryDayExp.class, list, response.getOutputStream());
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<MemberChannelSummaryDayExp> getData(String startDate, String endDate) {
		List<MemberChannelSummaryDayExp> resultList = null;
		Example example = null;
		Map<String, Integer> memNums = null;
		MemberChannelSummaryDayExp mcsde = null;
		try {
			example = new Example();
			Criteria temp = example.createCriteria();
			if (StringUtils.isNotBlank(startDate) || StringUtils.isNotBlank(endDate)) {
				if (StringUtils.isNotBlank(startDate)) {
					temp.andGreaterThanOrEqualTo("mcsd.SUMMARY_DATE",
							DateUtils.formatString(startDate));
				}
				if (StringUtils.isNotBlank(endDate)) {
					temp.andLessThanOrEqualTo("mcsd.SUMMARY_DATE",
							DateUtils.formatString(endDate));
				}
			} else {
				temp.andEqualTo("mcsd.SUMMARY_DATE", DateUtils.addDays(-1));
			}

			resultList = memberChannelSummaryDayService.queryExpByExample(example, null);
			memNums = new HashMap<String, Integer>();

			if (resultList != null && resultList.size() != 0) {
				for (int i = 0; i < resultList.size(); i++) {
					mcsde = resultList.get(i);
					if (memNums.get(mcsde.getCreateTime()) == null) {
						memNums.put(mcsde.getCreateTime(), mcsde.getTotalNumber());
					} else {
						memNums.put(mcsde.getCreateTime(), memNums.get(mcsde.getCreateTime()) + mcsde.getTotalNumber());
					}
				}

				Set<String> s = memNums.keySet();// 获取KEY集合
				for (String str : s) {
					for (int i = 0; i < resultList.size(); i++) {
						if (str.equals(resultList.get(i).getCreateTime())) {
							resultList.get(i).setMemNum(memNums.get(str));
						}
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultList;
	}

	@Autowired
	public void setMemberChannelSummaryDayService(MemberChannelSummaryDayService memberChannelSummaryDayService) {
		this.memberChannelSummaryDayService = memberChannelSummaryDayService;
	}

}
