package com.bw.adv.controller.report;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.report.order.model.exp.OrderRegionSummaryDayExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelExport;
import com.bw.adv.service.order.service.OrderRegionSummaryDayService;

@Controller
@RequestMapping("/orderReport")
public class OrderRegionSummaryDayController {
	private OrderRegionSummaryDayService orderRegionSummaryDayService;
	/**
	 * showOrderList:查询订单统计列表 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/showOrderList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridModel<OrderRegionSummaryDayExp> showOrderList(String startDate,String endDate){
		DataGridModel<OrderRegionSummaryDayExp> result = null;
//		Page<OrderRegionSummaryDayExp> pageObj = null;
		List<OrderRegionSummaryDayExp> orderList = null;
		OrderRegionSummaryDayExp totalOrder = null;
		Long count = null;
		Example example = null;
		Criteria criteria = null;
		BigDecimal totalSales = null;
		Integer totalConsumption = null;
		Integer totalConsumer = null;
		try {
			example = new Example();
			criteria = example.createCriteria();
			if(!StringUtils.isBlank(startDate)){
				criteria.andGreaterThanOrEqualTo("orsd.SUMMARY_DATE", DateUtils.formatString(startDate));
			}
			if(!StringUtils.isBlank(endDate)){
				criteria.andLessThanOrEqualTo("orsd.SUMMARY_DATE", DateUtils.formatString(endDate));
			}
//			pageObj = new Page<OrderRegionSummaryDayExp>();  
//			pageObj.setPageNo(1);
//			pageObj.setPageSize(10);
			orderList = orderRegionSummaryDayService.queryByExp(example);
			totalSales = new BigDecimal("0");
			totalConsumption = 0;
			totalConsumer = 0;
			for(OrderRegionSummaryDayExp order:orderList){
				totalSales = totalSales.add(order.getTotalSales());
				totalConsumption += order.getTotalConsumption();
				totalConsumer += order.getTotalConsumer();
			}
			totalOrder = new OrderRegionSummaryDayExp();
			totalOrder.setOrgName("总计");
			totalOrder.setTotalSales(totalSales);
			totalOrder.setTotalConsumer(totalConsumer);
			totalOrder.setTotalConsumption(totalConsumption);
			orderList.add(totalOrder);
//			count = pageObj.getTotalRecord()+1;
			result = new DataGridModel<OrderRegionSummaryDayExp>(orderList, count);
			result.setObj(totalOrder);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 订单统计excel
	 * @param request 请求
	 * @param response 响应
	 */
	@RequestMapping(value="/leadToExcelOrderRegion",method=RequestMethod.GET)
	public void leadToExcelOrderRegion(@RequestParam("startDate")String startDate,@RequestParam("endDate")String endDate,HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException{
		List<OrderRegionSummaryDayExp> list = null;
		try {
			String orderRegion="attachment;filename="+URLEncoder.encode("订单统计.xlsx", "utf-8");
			response.setContentType("application/xlsx;charset=UTF-8");
			response.setHeader("Content-disposition", orderRegion);
			//excel导出sheetName
			String sheetName = "订单统计";
			//excel导出数据
			list = getData(startDate, endDate);
			//导出
			if(list == null || list.size() == 0){
				System.out.println("null --");
			}else{
				OutputStream out = new FileOutputStream(orderRegion);
				ExcelExport.exportExcel(sheetName, OrderRegionSummaryDayExp.class, list, response.getOutputStream());
				out.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<OrderRegionSummaryDayExp> getData(String startDate,String endDate){
		List<OrderRegionSummaryDayExp> orderList = null;
		OrderRegionSummaryDayExp totalOrder = null;
		Example example = null;
		Criteria criteria = null;
		BigDecimal totalSales = null;
		Integer totalConsumption = null;
		Integer totalConsumer = null;
		try {
			example = new Example();
			criteria = example.createCriteria();
			if(!StringUtils.isBlank(startDate)){
				criteria.andGreaterThanOrEqualTo("orsd.SUMMARY_DATE", DateUtils.formatString(startDate));
			}
			if(!StringUtils.isBlank(endDate)){
				criteria.andLessThanOrEqualTo("orsd.SUMMARY_DATE", DateUtils.formatString(endDate));
			}
			orderList = orderRegionSummaryDayService.queryByExp(example);
			totalSales = new BigDecimal("0");
			totalConsumption = 0;
			totalConsumer = 0;
			for(OrderRegionSummaryDayExp order:orderList){
				totalSales = totalSales.add(order.getTotalSales());
				totalConsumption += order.getTotalConsumption();
				totalConsumer += order.getTotalConsumer();
			}
			totalOrder = new OrderRegionSummaryDayExp();
			totalOrder.setOrgName("总计");
			totalOrder.setTotalSales(totalSales);
			totalOrder.setTotalConsumer(totalConsumer);
			totalOrder.setTotalConsumption(totalConsumption);
			for(OrderRegionSummaryDayExp order:orderList){
				double x = (Double.parseDouble(order.getWithTotalSales()) * 100.00 / totalSales.doubleValue());
				DecimalFormat df = new DecimalFormat("#0.00");
				order.setScale1(df.format(x) + "%");
					
				double y = (Double.parseDouble(order.getWithTotalConsumption()) * 100.00 / totalConsumption.doubleValue());
				order.setScale2(df.format(y) + "%");
				
				double z = (Double.parseDouble(order.getWithTotalConsumer()) * 100.00 / totalConsumer.doubleValue());
				order.setScale3(df.format(z) + "%");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return orderList;
	}
	
	@Autowired
	public void setOrderRegionSummaryDayService(
			OrderRegionSummaryDayService orderRegionSummaryDayService) {
		this.orderRegionSummaryDayService = orderRegionSummaryDayService;
	}
	
}
