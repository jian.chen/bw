package com.bw.adv.controller.report;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.bw.adv.module.common.constant.StatusConstant;
import com.bw.adv.module.common.model.DataGridModel;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.report.points.model.exp.PointsSummaryDayExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelExport;
import com.bw.adv.service.points.service.PointsSummaryDayService;

@Controller
@RequestMapping("/pointsReport")
public class PointsSummaryDayController {
	private PointsSummaryDayService pointsSummaryDayService;
	/**
	 * showOrderList:查询订单统计列表 <br/>
	 * scrmVersion 1.0
	 * @author liuyi.wang
	 * @version jdk1.7
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "/showPointsList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridModel<PointsSummaryDayExp> showPointsList(String startDate,String endDate){
		DataGridModel<PointsSummaryDayExp> result = null;
		List<PointsSummaryDayExp> pointsList = null;
		PointsSummaryDayExp pointsList1 = null;
		PointsSummaryDayExp pointsList2 = null;
		PointsSummaryDayExp pointsList3 = null;
		PointsSummaryDayExp totalPoints = null;
		Long count = null;
		Example example = null;
		Example example1 = null;
		Example example2 = null;
		Example example3 = null;
		BigDecimal totalInPoints = null;
		try {
			example = getExampleByStatusId(startDate, endDate, StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId());
			pointsList = pointsSummaryDayService.queryByExp(example);//可用积分
			example1 = getExampleByStatusId(startDate, endDate, StatusConstant.MEMBER_POINTS_ITEM_USED.getId());
			pointsList1 = pointsSummaryDayService.queryPointsByExp(example1);//兑换积分
			example2 = getExampleByStatusId(startDate, endDate, StatusConstant.MEMBER_POINTS_ITEM_EXPIRE.getId());
			pointsList2 = pointsSummaryDayService.queryPointsByExp(example2);//过期积分
			example3 = getExampleByStatusId(startDate, endDate, StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getId());
			pointsList3 = pointsSummaryDayService.queryPointsByExp(example3);//取消单积分
			
			totalPoints = new PointsSummaryDayExp();
			
			totalInPoints = new BigDecimal("0");
			for(PointsSummaryDayExp points:pointsList){
				totalInPoints = totalInPoints.add(points.getPointsInNumber());
			}
			totalPoints.setActivityInstanceName("总发放积分数量");
			totalPoints.setPointsInNumber(totalInPoints);
			if(pointsList1==null){
				pointsList1 = new PointsSummaryDayExp();
				pointsList1.setPointsInNumber(new BigDecimal(0));
			}
			if(pointsList2==null){
				pointsList2 = new PointsSummaryDayExp();
				pointsList2.setPointsInNumber(new BigDecimal(0));
			}
			if(pointsList3==null){
				pointsList3 = new PointsSummaryDayExp();
				pointsList3.setPointsInNumber(new BigDecimal(0));
			}
			pointsList1.setActivityInstanceName("总兑换积分");
			pointsList1.setWithPointsInNumber(null);
			pointsList2.setActivityInstanceName("总过期积分");
			pointsList2.setWithPointsInNumber(null);
			pointsList3.setActivityInstanceName("总取消单积分");
			pointsList3.setWithPointsInNumber(null);
			pointsList.add(pointsList1);
			pointsList.add(pointsList2);
			pointsList.add(pointsList3);
			pointsList.add(totalPoints);
			result = new DataGridModel<PointsSummaryDayExp>(pointsList, count);
			result.setObj(totalPoints);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	

	public Example getExampleByStatusId(String startDate,String endDate,Long statusId){
		Example example = null;
		Criteria criteria = null;
		example = new Example();
		criteria = example.createCriteria();
		if(!StringUtils.isBlank(startDate)){
			criteria.andGreaterThanOrEqualTo("psd.SUMMARY_DATE", DateUtils.formatString(startDate));
		}
		if(!StringUtils.isBlank(endDate)){
			criteria.andLessThanOrEqualTo("psd.SUMMARY_DATE", DateUtils.formatString(endDate));
		}
		if(statusId.equals(StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId())){
			criteria.andEqualTo("psd.STATUS_ID", StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId());
			
		}
		if(statusId.equals(StatusConstant.MEMBER_POINTS_ITEM_USED.getId())){
			criteria.andEqualTo("psd.STATUS_ID", StatusConstant.MEMBER_POINTS_ITEM_USED.getId());
			
		}
		if(statusId.equals(StatusConstant.MEMBER_POINTS_ITEM_EXPIRE.getId())){
			criteria.andEqualTo("psd.STATUS_ID", StatusConstant.MEMBER_POINTS_ITEM_EXPIRE.getId());
			
		}
		if(statusId.equals(StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getId())){
			criteria.andEqualTo("psd.STATUS_ID", StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getId());
			
		}
		return example;
	}
	
	/**
	 * 积分统计excel
	 * @param request 请求
	 * @param response 响应
	 */
	@RequestMapping(value="/leadToExcelPointsRegion",method=RequestMethod.GET)
	public void leadToExcelMemberRegional(@RequestParam("startDate")String startData,@RequestParam("endDate")String endDate,HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException{
		try {
			String disposition = "attachment;filename=" + URLEncoder.encode("积分统计.xlsx", "utf-8");
			response.setContentType("application/xlsx;charset=UTF-8");
			response.setHeader("Content-disposition", disposition);
			//excel导出sheetName
			String sheetName = "积分统计";
			//excel导出的数据
			List<PointsSummaryDayExp> list = getData(startData, endDate);
			//导出
			if( list == null || list.size() == 0){
				System.out.println("null====");
			}else{
				OutputStream out = new FileOutputStream(disposition);
				ExcelExport.exportExcel(sheetName, PointsSummaryDayExp.class, list, response.getOutputStream());
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取积分统计数据
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PointsSummaryDayExp> getData(String startDate,String endDate){
		List<PointsSummaryDayExp> pointsList = null;
		PointsSummaryDayExp pointsList1 = null;
		PointsSummaryDayExp pointsList2 = null;
		PointsSummaryDayExp pointsList3 = null;
		PointsSummaryDayExp totalPoints = null;
		Example example = null;
		Example example1 = null;
		Example example2 = null;
		Example example3 = null;
		BigDecimal totalInPoints = null;
		try {
			example = getExampleByStatusId(startDate, endDate, StatusConstant.MEMBER_POINTS_ITEM_AVAILABLE.getId());
			pointsList = pointsSummaryDayService.queryByExp(example);//可用积分
			example1 = getExampleByStatusId(startDate, endDate, StatusConstant.MEMBER_POINTS_ITEM_USED.getId());
			pointsList1 = pointsSummaryDayService.queryPointsByExp(example1);//兑换积分
			example2 = getExampleByStatusId(startDate, endDate, StatusConstant.MEMBER_POINTS_ITEM_EXPIRE.getId());
			pointsList2 = pointsSummaryDayService.queryPointsByExp(example2);//过期积分
			example3 = getExampleByStatusId(startDate, endDate, StatusConstant.MEMBER_POINTS_ITEM_CANCEL.getId());
			pointsList3 = pointsSummaryDayService.queryPointsByExp(example3);//取消单积分
			
			totalPoints = new PointsSummaryDayExp();
			
			totalInPoints = new BigDecimal("0");
			for(PointsSummaryDayExp points:pointsList){
				totalInPoints = totalInPoints.add(points.getPointsInNumber());
			}
			
			for (PointsSummaryDayExp pointsSummaryDayExp : pointsList) {
				double  x = pointsSummaryDayExp.getPointsInNumber().multiply(new BigDecimal(100)).divide(totalInPoints, 2, BigDecimal.
						ROUND_HALF_UP).doubleValue();
				pointsSummaryDayExp.setRate(x+"%");
			}
			
			totalPoints.setActivityInstanceName("总发放积分数量");
			totalPoints.setPointsInNumber(totalInPoints);
			totalPoints.setWithPointsInNumber(totalInPoints);
			if(pointsList1==null){
				pointsList1 = new PointsSummaryDayExp();
				pointsList1.setPointsInNumber(new BigDecimal(0));
			}
			if(pointsList2==null){
				pointsList2 = new PointsSummaryDayExp();
				pointsList2.setPointsInNumber(new BigDecimal(0));
			}
			if(pointsList3==null){
				pointsList3 = new PointsSummaryDayExp();
				pointsList3.setPointsInNumber(new BigDecimal(0));
			}
			pointsList1.setActivityInstanceName("总兑换积分");
			pointsList1.setWithPointsInNumber(pointsList1.getPointsInNumber());
			pointsList2.setActivityInstanceName("总过期积分");
			pointsList2.setWithPointsInNumber(pointsList2.getPointsInNumber());
			pointsList3.setActivityInstanceName("总取消单积分");
			pointsList3.setWithPointsInNumber(pointsList2.getPointsInNumber());
			pointsList.add(pointsList1);
			pointsList.add(pointsList2);
			pointsList.add(pointsList3);
			pointsList.add(totalPoints);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pointsList;
	}
	
	@Autowired
	public void setPointsSummaryDayService(
			PointsSummaryDayService pointsSummaryDayService) {
		this.pointsSummaryDayService = pointsSummaryDayService;
	}
	
	
}
