/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-report
 * File Name:FansChannelSummaryDayController.java
 * Package Name:com.sage.scrm.controller.report
 * Date:2015年11月30日上午10:11:18
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.report;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.report.member.model.exp.FansChannelSummaryDayExp;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelExport;
import com.bw.adv.service.report.service.FansChannelSummaryDayService;

/**
 * ClassName:FansChannelSummaryDayController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年11月30日 上午10:11:18 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/FansChannelSummaryDayController")
public class FansChannelSummaryDayController {
	
	private FansChannelSummaryDayService fansChannelSummaryDayService;
	
	/**
	 * 
	 * showListByExample:(查询粉丝渠道列表). <br/>
	 * Date: 2015年11月30日 上午10:16:20 <br/>
	 * scrmVersion 1.0
	 * @author chuanxue.wei
	 * @version jdk1.7
	 * @param fansChannelSummaryDayExp
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "showListByExample")
	@ResponseBody
	public Map<String, Object> showListByExample(FansChannelSummaryDayExp fansChannelSummaryDayExp,int page,int rows){
		Map<String, Object> map = null;
		List<FansChannelSummaryDayExp> result = null;
		Page<FansChannelSummaryDayExp> pageObj = null;
		Example example = null;
		int fansNum = 0;
		
		try {
			
			map = new HashMap<String, Object>();
			
			pageObj = new Page<FansChannelSummaryDayExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			example = new Example();
			Criteria temp = example.createCriteria();
			if(fansChannelSummaryDayExp != null){
				if(!StringUtils.isBlank(fansChannelSummaryDayExp.getStartDate())){
					temp.andGreaterThanOrEqualTo("fcsd.SUMMARY_DATE", DateUtils.formatString(fansChannelSummaryDayExp.getStartDate()));
				}
				if(!StringUtils.isBlank(fansChannelSummaryDayExp.getEndDate())){
					temp.andLessThanOrEqualTo("fcsd.SUMMARY_DATE", DateUtils.formatString(fansChannelSummaryDayExp.getEndDate()));
				}
				//默认查询前一天的粉丝记录
				if(StringUtils.isBlank(fansChannelSummaryDayExp.getStartDate()) && 
						StringUtils.isBlank(fansChannelSummaryDayExp.getEndDate())){
					temp.andEqualTo("fcsd.SUMMARY_DATE", DateUtils.addDays(-1));
				}
			}else{
				temp.andEqualTo("fcsd.SUMMARY_DATE", DateUtils.addDays(-1));
			}
			
			result = fansChannelSummaryDayService.queryListByExample(example, pageObj);
			
			if(result != null && result.size() != 0){
				for(int i=0;i<result.size();i++){
					if(fansNum == 0){
						fansNum = result.get(i).getTotalNumber();
					}else{
						fansNum += result.get(i).getTotalNumber();
					}
				}
				
				for(int i=0;i<result.size();i++){
					result.get(i).setFansNum(fansNum);
				}
				
			}
			
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 粉丝来源渠道excel
	 * @param request 请求
	 * @param resposne 响应
	 */
	@RequestMapping(value = "/leadToExcelFansChannel", method = RequestMethod.GET)
	public void leadToExcelFansChannel(@RequestParam("startDate")String startDate,@RequestParam("endDate")String endDate,HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		List<FansChannelSummaryDayExp> list = null;
		Example example = null;
		example = new Example();
		Criteria temp = example.createCriteria();
		try {
			String disposition = "attachment;filename=" + URLEncoder.encode("二维码粉丝渠道来源统计.xlsx", "utf-8");
			response.setContentType("application/xlsx;charset=UTF-8");
			response.setHeader("Content-disposition", disposition);
			// excel导出sheetName
			String sheetName = "二维码粉丝渠道来源统计";
			// excel导出数据
			if (StringUtils.isNotBlank(startDate) || StringUtils.isNotBlank(endDate)) {
				if (StringUtils.isNotBlank(startDate)) {
					temp.andGreaterThanOrEqualTo("fcsd.SUMMARY_DATE", DateUtils.formatString(startDate));
				}
				if (StringUtils.isNotBlank(endDate)) {
					temp.andLessThanOrEqualTo("fcsd.SUMMARY_DATE", DateUtils.formatString(endDate));
				}
			}else{
				temp.andEqualTo("fcsd.SUMMARY_DATE", DateUtils.addDays(-1));
			}
			
			list = fansChannelSummaryDayService.queryListByExample(example, null);
			for (FansChannelSummaryDayExp fansChannelSummaryDayExp : list) {
				if(StringUtils.isBlank(fansChannelSummaryDayExp.getQrCodeName())){
					fansChannelSummaryDayExp.setQrCodeName("微信");
				}
			}
			// 导出
			if (list == null || list.size() == 0) {
				System.out.println("null----");
			} else {
				OutputStream out = new FileOutputStream(disposition);
				ExcelExport.exportExcel(sheetName, FansChannelSummaryDayExp.class, list, response.getOutputStream());
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Autowired
	public void setFansChannelSummaryDayService(
			FansChannelSummaryDayService fansChannelSummaryDayService) {
		this.fansChannelSummaryDayService = fansChannelSummaryDayService;
	}
	
	

}

