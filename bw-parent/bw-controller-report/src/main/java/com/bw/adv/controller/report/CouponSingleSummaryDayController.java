/**
 * Copyright (C), 2010-2015, 赛捷软件（上海有限公司）.
 * Project Name:scrm-controller-report
 * File Name:CouponSingleSummaryDayController.java
 * Package Name:com.sage.scrm.controller.report
 * Date:2015年12月17日上午10:29:44
 * Description: //模块目的、功能描述      
 * History: //修改记录
*/

package com.bw.adv.controller.report;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.common.model.Example;
import com.bw.adv.module.common.model.Example.Criteria;
import com.bw.adv.module.report.coupon.model.exp.CouponSingleSummaryDayExp;
import com.bw.adv.module.tools.ExcelExport;
import com.bw.adv.service.report.service.CouponSingleSummaryDayService;

/**
 * ClassName:CouponSingleSummaryDayController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Date: 2015年12月17日 上午10:29:44 <br/>
 * scrmVersion 1.0
 * @author   chuanxue.wei
 * @version  jdk1.7
 * @see 	 
 */
@Controller
@RequestMapping("/CouponSingleSummaryDayController")
public class CouponSingleSummaryDayController {
	
	private CouponSingleSummaryDayService couponSingleSummaryDayService;
	
	@RequestMapping(method = RequestMethod.POST, value = "showListByMonth")
	@ResponseBody
	public Map<String, Object> showListByMonth(CouponSingleSummaryDayExp couponSingleSummaryDayExp,int page,int rows){
		Map<String, Object> map = null;
		List<CouponSingleSummaryDayExp> cssdeList = null;
		Page<CouponSingleSummaryDayExp> pageObj = null;
		Example example = null;
		
		try {
			map = new HashMap<String, Object>();
			
			pageObj = new Page<CouponSingleSummaryDayExp>();
			pageObj.setPageNo(page);
			pageObj.setPageSize(rows);
			
			example = new Example();
			Criteria temp = example.createCriteria();
			
			if(couponSingleSummaryDayExp != null){
				if(couponSingleSummaryDayExp.getSeachTime() != null){
					String time = couponSingleSummaryDayExp.getSeachTime();
					String[] str=time.split("-"); 
					temp.andEqualTo("cssd.SUMMARY_YEAR",str[0]);
					if(str[1].charAt(0) == '0'){
						temp.andEqualTo("cssd.SUMMARY_MONTH",str[1].charAt(1));
					}else{
						temp.andEqualTo("cssd.SUMMARY_MONTH",str[1]);
					}
					cssdeList = couponSingleSummaryDayService.queryExpByExample(example, pageObj);
					//cssdeList = couponSingleSummaryDayService.queryListByMonth(couponSingleSummaryDayExp.getSummaryMonth(), pageObj);
				}
				
			}
			map.put("total", pageObj.getTotalRecord());
			map.put("rows", pageObj.getResults());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 优惠券月统计excel
	 * @param request 请求
	 * @param response 响应
	 */
	@RequestMapping(value="/leadToExcelCouponSingle",method=RequestMethod.GET)
	public void leadToExcelCouponSingle(@RequestParam("startDate")String startDate,HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException{
		List<CouponSingleSummaryDayExp> list = null;
		try {
			String disposition="attachment;filename="+URLEncoder.encode("优惠券月统计.xlsx", "utf-8");
			response.setContentType("application/xlsx;charset=UTF-8");
			response.setHeader("Content-disposition", disposition);
			//excel导出sheetName
			String sheetName = "优惠券月统计";
			//excel导出数据
			list = getData(startDate);
			//导出
			if(list == null || list.size() == 0){
				System.out.println("null****");
			}else{
				OutputStream out = new FileOutputStream(disposition);
				ExcelExport.exportExcel(sheetName, CouponSingleSummaryDayExp.class, list, response.getOutputStream());
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<CouponSingleSummaryDayExp> getData(String startDate){
		List<CouponSingleSummaryDayExp> cssdeList = null;
		Page<CouponSingleSummaryDayExp> pageObj = null;
		Example example = null;
		
		try {
			
			
			example = new Example();
			Criteria temp = example.createCriteria();
			
			if(StringUtils.isNotBlank(startDate)){
				String time = startDate;
				String[] str=time.split("-"); 
				temp.andEqualTo("cssd.SUMMARY_YEAR",str[0]);
				if(str[1].charAt(0) == '0'){
					temp.andEqualTo("cssd.SUMMARY_MONTH",str[1].charAt(1));
				}else{
					temp.andEqualTo("cssd.SUMMARY_MONTH",str[1]);
				}
				cssdeList = couponSingleSummaryDayService.queryExpByExample(example, pageObj);
			}
				
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cssdeList;
	}
	
	@Autowired
	public void setCouponSingleSummaryDayService(
			CouponSingleSummaryDayService couponSingleSummaryDayService) {
		this.couponSingleSummaryDayService = couponSingleSummaryDayService;
	}
	

}

