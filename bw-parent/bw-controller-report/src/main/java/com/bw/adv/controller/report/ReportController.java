package com.bw.adv.controller.report;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bw.adv.core.utils.Page;
import com.bw.adv.module.report.model.MemberAge;
import com.bw.adv.module.report.model.MemberRegion;
import com.bw.adv.module.report.model.OrderRate;
import com.bw.adv.module.tools.DateUtils;
import com.bw.adv.module.tools.ExcelExport;
import com.bw.adv.service.member.service.MemberService;

@Controller
@RequestMapping("/ReportController")
public class ReportController {

	private MemberService memberService;

	/**
	 * 消费频次报表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/showOrderRate", method = RequestMethod.POST)
	@ResponseBody
	public List<OrderRate> showOrderRate() {
		OrderRate rate1 = null;
		OrderRate rate2 = null;
		OrderRate rate3 = null;
		OrderRate rate4 = null;
		List<OrderRate> rateList = null;
		try {
			rate1 = new OrderRate();
			rate2 = new OrderRate();
			rate3 = new OrderRate();
			rate4 = new OrderRate();
			rateList = new ArrayList<OrderRate>();

			Long count1 = memberService.findMemberOrderCount(0L, null, 0L);
			Long count2 = memberService.findMemberOrderCount(1L, null, 0L);
			Long count3 = memberService.findMemberOrderCount(1L, 5L, 3L);
			Long count4 = memberService.findMemberOrderCount(4L, null, 1L);
			Long sum = count1 + count2 + count3 + count4;

			rate1.setRate("0次");
			rate1.setMemberCount(count1);
			rate1.setSum(sum);

			rate2.setRate("1次");
			rate2.setMemberCount(count2);
			rate2.setSum(sum);

			rate3.setRate("2~4次");
			rate3.setMemberCount(count3);
			rate3.setSum(sum);

			rate4.setRate("4次以上");
			rate4.setMemberCount(count4);
			rate4.setSum(sum);

			rateList.add(rate1);
			rateList.add(rate2);
			rateList.add(rate3);
			rateList.add(rate4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rateList;
	}

	@RequestMapping(value = "/showMemberRegion", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> showMemberRegion(int page, int rows, HttpServletRequest request, String sort,
			String order) {
		Map<String, Object> map = null;
		Page<MemberRegion> templatePage = null;
		try {
			map = new HashMap<String, Object>();
			templatePage = new Page<MemberRegion>();
			templatePage.setPageNo(page);
			templatePage.setPageSize(rows);
			// 这里用member_id作为member_count的别名，否则分页插件会报错
			if (StringUtils.isNotBlank(sort) && sort.equals("memberId")) {
				sort = "MEMBER_ID";
			}
			Long allMemberAccount = this.memberService.findAllMemberCount();
			this.memberService.findMemberRegion(templatePage, sort, order);
			for (MemberRegion memberRegion : templatePage.getResults()) {
				memberRegion.setAllMemberAccount(allMemberAccount);
			}
			map.put("total", templatePage.getTotalRecord());
			map.put("rows", templatePage.getResults());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 会员地区分布excel
	 * @param request
	 *            请求
	 * @param resposne
	 *            响应
	 */
	@ResponseBody
	@RequestMapping(value = "/leadToExcelMemberRegion", method = RequestMethod.GET)
	public void leadToExcelMemberRegion(HttpServletRequest request, HttpServletResponse response) {
		List<MemberRegion> list = null;
		try {
			String disposition = "attachment;filename=" + URLEncoder.encode("会员地区分布.xlsx", "utf-8");
			response.setContentType("application/xlsx;charset=UTF-8");
			response.setHeader("Content-disposition", disposition);
			// excel导出sheetName
			String sheetName = "会员地区分布";
			// excel导出数据
			list = memberService.findMemberRegion(null, null, null);
			Long allMemberAccount = this.memberService.findAllMemberCount();
			for (MemberRegion memberRegion : list) {
				if(StringUtils.isBlank(memberRegion.getGeoName())){
					memberRegion.setGeoName("其他");
				}
				double x = (float)memberRegion.getMemberId() * 100/(float)allMemberAccount;
				NumberFormat ddf1=NumberFormat.getNumberInstance();
				ddf1.setMaximumFractionDigits(2); 
				memberRegion.setRate(ddf1.format(x)+"%");
			}
			// 导出
			if (list == null || list.size() == 0) {
				System.out.println("null----");
			} else {
				OutputStream out = new FileOutputStream(disposition);
				ExcelExport.exportExcel(sheetName, MemberRegion.class, list, response.getOutputStream());
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 消费频次报表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/showMemberAge", method = RequestMethod.POST)
	@ResponseBody
	public List<MemberAge> showMemberAge(String startDate, String endDate) {
		List<MemberAge> list = null;
		try {
			list = new ArrayList<MemberAge>();
			list = memberService.findMemberAge(DateUtils.formatString(startDate), DateUtils.formatString(endDate));
			Long all = 0L;
			for (MemberAge memberAge : list) {
				all += memberAge.getDayNewCount();
			}
			for (MemberAge memberAge : list) {
				memberAge.setAll(all);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * 会员年龄统计导出excel
	 * @param request 请求
	 * @param resposne 响应
	 */
	@RequestMapping(value = "/leadToExcelMemberAge", method = RequestMethod.GET)
	public void leadToExcelMemberAge(@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, HttpServletRequest request, HttpServletResponse response)
					throws UnsupportedEncodingException {
		try {
			String disposition = "attachment;filename=" + URLEncoder.encode("会员年龄统计.xlsx", "utf-8");
			response.setContentType("application/xlsx;charset=UTF-8");
			response.setHeader("Content-disposition", disposition);
			// excel的sheetName
			String sheetName = "会员年龄统计";
			// excel要导出的数据
			List<MemberAge> list = memberService.findMemberAge(DateUtils.formatString(startDate),
					DateUtils.formatString(endDate));
			Long all = 0L;
			for (MemberAge memberAge : list) {
				all = all + memberAge.getDayNewCount();
			}
			for (MemberAge memberAge : list) {
				memberAge.setAll(all);
			}
			// 导出
			if (list == null || list.size() == 0) {
				System.out.println("null--------");
			} else {
				OutputStream out = new FileOutputStream(disposition);
				ExcelExport.exportExcel(sheetName, MemberAge.class, list, response.getOutputStream());
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Autowired
	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

}
